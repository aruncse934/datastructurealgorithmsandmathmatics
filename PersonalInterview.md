career development


**# How do you confirm an email interview invitation?
    -------------------------------------------------
    Thanks for your email. I am confirming that I am fine with the date and time.
    Looking forward to talk to your teammate/engineer.
  or
  Thanks for your email. I am confirming that I am available to attend the next meeting.
   Looking forward to talking to your teammate.
   
   or
   
   Hi Rajalakshmi,
   Thanks for your email.
   Please find the below details:-
      Total & Current Experience: 3.7+ Years
      Current CTC: 6 LPA
      Expected CTC: 13 LPA
      Notice period: 45 Days
      Educational qualification: BE(CSE)
      Current company: Cognizant
      Date of birth: 02/04/1994
      Reason for change: looking for better Opportunity
      Contact no:+91- 9513187391
      Email id: arun.cse@outlook.com
   
   I have attached a copy of my resume that details my projects and experience in software development.
   
   Regards,
   Arun Kumar Gupta

or

Hi ,
Greeting of the day!
Thanks for your email.

Please find the below details:-
Total & Current Experience: 6+ Years
Current CTC: 22 LPA
Expected CTC: 30 LPA
Notice period: immediate joiner
Educational Qualification: BE(CSE)
Current company: Encora
Date of birth: 02/04/1994
Reason for change: looking for a better Opportunity
Contact no:+91- 9513187391
Email id: arun.cse@outlook.com

I have attached a copy of my resume detailing my projects and software development experience.

Thanks & Regards
Arun Kumar Gupta
arun.cse@outlook.com
+91-9513187391

1). Tell me about yourself or Introduce yourself? 
------------------------------------------------
Hi, Good Morning / Afternoon /evening
I'm Arun Kumar Gupta. and I'm a Java developer.
I am from Jharkhand. or I'm belongs to Garhwa, Jharkhand.
Regarding My Work Profile I am having a total of 4 years Experience in Software Development. or I have 4 years of experience in Backend Development.

I have been Working at Cognizant as Programmer Analyst(Backend Developer) from Feb 2019,
from where I got exposure to Complete Software life cycle starting from requirement gathering till
delivery with minimal time and team size, without Compromising quality through agile as well as scrum 
Methodologies.

MoreOver, I have Basic Knowledge in front Development using Javascript.

My Qualification is BE Graduation in CSE from RGPV University, Bhopal in 2016. or I have done B.E. in CSE From RGPV University.

I am working at cognizant.


My hobbies are cooking, Problem Solving, playing chess, etc.
My strength is Happiness, problem-solving.
My weakness is easily believe any person, English.
My short terms goal is Java Architect.
My long terms goal is Business-man.

Thank you for the opportunity time and concern to know about me.
That's all about MySelf or That's my Self.

Projects::- 
       Project name:
       Technology used::
       Purpose of project::

My Responsibilities in Project::
 -> Analyzing the design document.
 -> Involved in code enhancement, Defect-fixing.
 -> Used GitLab for the repository.
 -> Development code in java, Spring Boot, Spring Frameworks, Hibernate, and Restful.
 -> Coding of Business logic & DAO Layer.
 -> Code Review & Testing.
 -> Responsible for coding  of DAO Class using J2EE.
 **
 ->Involved Preparing Spring Configuration files.
 ->Involved in Developing Controller classes & Model class.
 ->Involved in adding Validations based on User login.
 ->Developed hibernate Configuration files to Communicate with database tables.
 ->Responsible for writing POJO , with help of annotations.
 ->Involved integration of Spring with hibernate configuration.
 
------------------------------------------
2. What are your strengths and weakness? or What is your greatest strength? or What is your greatest weakness?
------------------------------------------
My strength is Happiness, Problem Solving.
My weakness are English, esily belive someone.
------------------------------------------
3. Why should you be hired?  or Why should we hire you?
------------------------------------------
I have an ability to fulfill the work with full satisfaction.
------------------------------------------
I have 2.5 years experience in Backend Development.
------------------------------------------
4. How will you manage work pressure?
------------------------------------------
I handle pressure work to spritual
------------------------------------------
5. Why do you want to join us? or Why do you want to work here?
------------------------------------------
This is a Good company for make a future because I want to join this company.
------------------------------------------
6. What is your salary expectation?
------------------------------------------

------------------------------------------
7. Are you comfortable with rotational shifts?
------------------------------------------

------------------------------------------
8. Why are you looking for a job change?
------------------------------------------

------------------------------------------
9. What do you want to do?
------------------------------------------

------------------------------------------
10. What is your favorite programming language?
------------------------------------------

------------------------------------------
11. What is your work style?
------------------------------------------
Tell me about a time you showed leadership.
Tell me about a time you were successful on a team.
What would your co-workers say about you?
Why do you want to leave your current role?
Describe your most challenging project.
Tell me about something you’ve accomplished that you are proud of.
Can you explain your employment gap?
What are your salary expectations?
What do you like to do outside of work?
Tell me about a time you had to manage conflicting priorities.
Where do you see yourself in 5 years?
Describe your leadership style.
Tell me about a time you failed or made a mistake.
Tell me about a time you worked with a difficult person.
Tell me about a time you had to persuade someone.
Tell me about a time you disagreed with someone.
Tell me about a time you created a goal and achieved it.
Tell me about a time you surpassed people’s expectations.
Tell me about a time you had to handle pressure.
Tell me about a time you had to learn something quickly.
Do you have any questions for me?


12. What can you tell me about your experience?
13. Why do you want to work for this company?
15. Do you have any questions for me?
16. Why are you here?
17. Can you describe an environment or scenario where you would not thrive instantly?
18. If you could do anything, what would be your ideal job?
19. What makes you interested in this job?
20. Why do you think should we take you for this job?
21. What is your motivating factor for you at work?
22. Why do you want to leave your present job?
23. What is your greatest achievement?
24. What qualities would you for look, if you were recruiting someone for this position?
25.You seem to be working with the same company since a long time. - Why?
26.You seem to have switched many jobs-why?
27.You do not have all the experience we are seeking for this position.
28.How did you manage to attend this interview during your working hours?
29.Would you like to work in a team or on your own?
30.If your boss was present here, what do you think he would tell us about you?
31.Did you face any problems in your last role?
32.Did you think of changing your present job earlier? If yes, what do you think made you stay back?
33.Was your work ever criticized? What did you do?
34.You don’t seem to have lead in your last role. How would you be able to do it here?
35.Have you ever been in a situation where you were required to fire anyone? How did you handle it?
36.One of your team members is not able to meet his targets on a regular basis. You discussed it with him and tried all measures to improve his performance in that role but nothing works. What would you do as a team leader?
37.If you face a problem with your own performance, what would you do?
38.How was your performance measured?
39.How would you classify your style of management?
40.How would you describe your work style?
41.What would you do if your team does not perform as expected?
42.What will you do if you are offered a job with a salary higher than this?
43.Why are you applying for this job? (or)Why this role attract you?
44.Would you like to work overtime or odd hours?
45.What is more important to you: the money or the work?
46.What do you know about this organization?
47.Why did you leave your last job?
48.Assume you are hired, then how long would you expect to work for us?
49.How would you rate yourself on a scale of 1 to 10?
50.What is your objective in life?
51.What are your hobbies?
52.Explain, how would you be an asset to this organization?
53.Would you lie for the company?
54.Are you applying for other jobs also?
55.How do you get to know about our company?
56.What does success mean to you?
57.Describe yourself in one word?
58.What is the difference between confidence and overconfidence?
59.What is the difference between smart work and hard work?
60.Just imagine that you have enough money to retire right now. Would you?
61.Don't you think that you are overqualified for this position?
62.Do you have any blind spot?
63.How do you handle stress, pressure, and anxiety?
64.What is the disappointment in your life?
65.What makes you angry?
66.What was the most difficult decision you have made in your past life?
67.How did you know about this position/ vacancy?
68.What gets you up in the morning?
69.What is your favorite book?
70.As you said, internet surfing is your hobby. Which site do you surf mostly?
71.What was the biggest mistake of your life?
72.Do you have any reference?
73.What is your greatest fear?
74.Would you like to relocate or travel for the company?
75.What are your expectations from the company?
76.Tell me something about yourself in brief
77.Describe who you are. or Tell me about your background.
78.What are your strengths and weaknesses?
79.You have not done your PG yet. This is not a drawback, but don’t you think you should get a PG degree asap?
80.You have changed jobs/jumped ship too many times already, why so?
81.What are your strong points? or What are your strengths?
82.What is your greatest fear?
83.If I call up your current or previous reporting manager now, what will be their opinion about you? What will they say that you need to work on?
84.Do you have any serious medical issues?
85.Did you ever have a conflict with your current/previous boss or professor?
86.What do your friends/co-workers say about you?
87.What did you do in the last year to improve your knowledge?
88.Explain the difference between group and team. Are you a team player?
89.What is your ideal company or workplace?
90.Have you ever had to fire anyone? How did you feel about that?
91.What is the most difficult thing that you’ve ever accomplished? or What is the most difficult thing you have ever done?
92.What is the difference between hard work and smart work?
93.How do you feel about working weekends and night shifts?
94.Where do you see yourself 3 years from now? or Where do you see yourself in 5 years?
95.Give an example of a time you had to respond to an unhappy manager/ customer/ colleague/ professor/ friend.
96.How quickly do you adapt to new technology?
97.What software packages are you familiar with?
98.On a scale of 1 to 10 how would you rate yourself as a leader?
99.What makes you angry?
100.Are you open to take risks? or Do you like experimenting?
101.What are your future goals? Tell me about your short term and long-term goals.
102.What motivates you?
103.What are your hobbies? or What are you passionate about?
104.What are your biggest achievements till date?
105.What are you most proud of?
106.What has been your greatest failure?
107.What do you always regret? or Do you have any regrets?
108.How do you respond to change?
109.Are you demanding as a boss?
110.Are you an organized person?
111.Can you describe your time management skills?
112.What’s your absenteeism record like?
113.Are you reliable? or Can I trust you with responsibilities?
114.What are the three things that are most important for you in a job?
115.What was the toughest decision you ever had to make?
116.If you won a Rs.10-crore lottery, would you still work?
117.Give me an example of your creativity.
118.What makes you happy?
119.How do you work under pressure? Can you handle the pressure?
120.Are you willing to relocate or travel?
121.What do you know about us or our company?
122.How long do you think you will work for us after we hire you?
123.Are you applying for other jobs? Do you have any other offer in hand?
124.Why do you want to work for us or our company? or Why do you want this job?
125.Do you know anyone who works for us?
126.Why should we hire you? or Why should I hire you?
127.What are your salary expectations?
128.Do you have a good work ethic?
129.How do you deal with feedback and criticism?
130.Your interview is more or less coming to an end when the interviewer asks you, “Do you have any questions for me?”







