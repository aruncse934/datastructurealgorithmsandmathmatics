Data structure and coding concepts:->   Adobe
------------------------------------
How array lists work
How Strings work
Read contents from a file and count words in it.
Write a program to print a patters using *. For eg:
*
* *
* * *
* * * *
* * * * *
Write program to implement reversed linked list
Write program to do binary search in a list. What is complexity? Worst case scenarios.
Why strings are immutable?
Write program to add two numbers stored in 2 different linked lists.
Write program to find if second array is a rotated version of first array.
Puzzles:

Famous 3 eggs and 100 floors problem
Camel and banana problem
Apple and Oranges puzzle
3 bulb, 3 switch problem
Burning rope problem
Manual Testing

Test cases for a physical machine which counts number of cars passing through it
Test cases for grammar checker (this was difficult one. Search google for this)
Test cases of a mobile application which converts image clicked from camera to PDF
Test cases of a feature, which automatically copies contents of a local folder to a server location via ftp.
If an application or computer system is suddenly running very slow, what would you do to debug the problem?
Test cases on a LAN cable, which connects laptop to internet.
Computer fundamentals:

How does a system boot up? What happens during the same
Difference between RAM/ROM?
What is paging?
What happens when you google anything on google.com. On what basis, suggestions are shown to user for auto-complete.
What is cache memory?
How does disk defragmentation works?
Impact of anti-virus on debugging an application (i dont remember the question clearly)
Different HTTP status and their meaning. Like 404 internal server error etc.

------------------------------------------------------------------------------------------------------------------