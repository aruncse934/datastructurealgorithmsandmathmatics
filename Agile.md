Agile Model
The meaning of Agile is swift or versatile."Agile process model" refers to a software development approach based on iterative development. Agile methods break tasks into smaller iterations, or parts do not directly involve long term planning. The project scope and requirements are laid down at the beginning of the development process. Plans regarding the number of iterations, the duration and the scope of each iteration are clearly defined in advance.

Each iteration is considered as a short time "frame" in the Agile process model, which typically lasts from one to four weeks. The division of the entire project into smaller parts helps to minimize the project risk and to reduce the overall project delivery time requirements. Each iteration involves a team working through a full software development life cycle including planning, requirements analysis, design, coding, and testing before a working product is demonstrated to the client.

Agile Model
![alt text](https://static.javatpoint.com/tutorial/software-engineering/images/software-engineering-agile-model.png)

Phases of Agile Model:
Following are the phases in the Agile model are as follows:


Requirements gathering
Design the requirements
Construction/ iteration
Testing/ Quality assurance
Deployment
Feedback
1. Requirements gathering: In this phase, you must define the requirements. You should explain business opportunities and plan the time and effort needed to build the project. Based on this information, you can evaluate technical and economic feasibility.

2. Design the requirements: When you have identified the project, work with stakeholders to define requirements. You can use the user flow diagram or the high-level UML diagram to show the work of new features and show how it will apply to your existing system.

3. Construction/ iteration: When the team defines the requirements, the work begins. Designers and developers start working on their project, which aims to deploy a working product. The product will undergo various stages of improvement, so it includes simple, minimal functionality.

4. Testing: In this phase, the Quality Assurance team examines the product's performance and looks for the bug.

5. Deployment: In this phase, the team issues a product for the user's work environment.

6. Feedback: After releasing the product, the last step is feedback. In this, the team receives feedback about the product and works through the feedback.

Agile Testing Methods:
Scrum
Crystal
Dynamic Software Development Method(DSDM)
Feature Driven Development(FDD)
Lean Software Development
eXtreme Programming(XP)
Scrum
SCRUM is an agile development process focused primarily on ways to manage tasks in team-based development conditions.

There are three roles in it, and their responsibilities are:

Scrum Master: The scrum can set up the master team, arrange the meeting and remove obstacles for the process
Product owner: The product owner makes the product backlog, prioritizes the delay and is responsible for the distribution of functionality on each repetition.
Scrum Team: The team manages its work and organizes the work to complete the sprint or cycle.
eXtreme Programming(XP)
This type of methodology is used when customers are constantly changing demands or requirements, or when they are not sure about the system's performance.

Crystal:
There are three concepts of this method-

Chartering: Multi activities are involved in this phase such as making a development team, performing feasibility analysis, developing plans, etc.
Cyclic delivery: under this, two more cycles consist, these are:
Team updates the release plan.
Integrated product delivers to the users.
Wrap up: According to the user environment, this phase performs deployment, post-deployment.
Dynamic Software Development Method(DSDM):
DSDM is a rapid application development strategy for software development and gives an agile project distribution structure. The essential features of DSDM are that users must be actively connected, and teams have been given the right to make decisions. The techniques used in DSDM are:

Time Boxing
MoSCoW Rules
Prototyping
The DSDM project contains seven stages:

Pre-project
Feasibility Study
Business Study
Functional Model Iteration
Design and build Iteration
Implementation
Post-project
Feature Driven Development(FDD):
This method focuses on "Designing and Building" features. In contrast to other smart methods, FDD describes the small steps of the work that should be obtained separately per function.

Lean Software Development:
Lean software development methodology follows the principle "just in time production." The lean method indicates the increasing speed of software development and reducing costs. Lean development can be summarized in seven phases.

Eliminating Waste
Amplifying learning
Defer commitment (deciding as late as possible)
Early delivery
Empowering the team
Building Integrity
Optimize the whole
When to use the Agile Model?
When frequent changes are required.
When a highly qualified and experienced team is available.
When a customer is ready to have a meeting with a software team all the time.
When project size is small.
Advantage(Pros) of Agile Method:
Frequent Delivery
Face-to-Face Communication with clients.
Efficient design and fulfils the business requirement.
Anytime changes are acceptable.
It reduces total development time.
Disadvantages(Cons) of Agile Model:
Due to the shortage of formal documents, it creates confusion and crucial decisions taken throughout various phases can be misinterpreted at any time by different team members.
Due to the lack of proper documentation, once the project completes and the developers allotted to another project, maintenance of the finished project can become a difficulty.
