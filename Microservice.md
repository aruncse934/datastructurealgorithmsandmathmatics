#Spring Boot & Microservices
-------------------------------
->Domain Driven Design(DDD)
->Microservices Patterns
->Securing Microservices-Oauth2 & JWT
->Microservices Framework-Axon
->DataMangement Pattern-Event Sourcing & CQRS
->Dockerizing Microservices
->Deploying Microservices in AWS
->Integrating Microservices with ReactJS


->Configuraton Server
->Rest API & Batch API
->Zuul API Gateway
->Discovers Server
->Spring DAta JPA
->MongoDB
->Cloud Deploy
->OAuth 2.x
->Zipkin & Sleuth
->Hystrix
->Load Balancer
->Message Queue

====================================================================================================================================================================================
#Microservice::: 1.  https://microservices.io/patterns/microservices.html
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
https://microservices.io/patterns/index.html
====================================================================================================================================================================================
#Introduction to Spring Cloud Netflix – Eureka::
-----------------------------------------------------------------------------
-> Client-side service discovery allows services to find and communicate with each other without hard-coding hostname and port. 
-> The only ‘fixed point' in such an architecture consists of a service registry with which each service has to register.

-> A drawback is that all clients must implement a certain logic to interact with this fixed point. This assumes an additional network round trip before the actual request.
->With Netflix Eureka each client can simultaneously act as a server, to replicate its status to a connected peer. In other words, a client retrieves a list of all connected peers of a service registry and makes all further requests to any other services through a load-balancing algorithm.
-> To be informed about the presence of a client, they have to send a heartbeat signal to the registry.
-> To achieve the goal of this write-up, we'll implement three microservices:

  1. a service registry (Eureka Server),
  2. a REST service which registers itself at the registry (Eureka Client) and
  3. a web application, which is consuming the REST service as a registry-aware client (Spring Cloud Netflix Feign Client).

-----------------------------------------------------------------------------
#Introduction to Spring Cloud Rest Client with Netflix Ribbon::
----------------------------------------------------------------
Netflix Ribbon is an Inter Process Communication (IPC) cloud library. Ribbon primarily provides client-side load balancing algorithms.

Apart from the client-side load balancing algorithms, Ribbon provides also other features:

  -> Service Discovery Integration – Ribbon load balancers provide service discovery in dynamic environments like a cloud. Integration with Eureka and Netflix service discovery component is included in the ribbon library
  -> Fault Tolerance – the Ribbon API can dynamically determine whether the servers are up and running in a live environment and can detect those servers that are down
  -> Configurable load-balancing rules – Ribbon supports RoundRobinRule, AvailabilityFilteringRule, WeightedResponseTimeRule out of the box and also supports defining custom rules
Ribbon API works based on the concept called “Named Client”. While configuring Ribbon in our application configuration file we provide a name for the list of servers included for the load balancing.

Let's take it for a spin.

    @RequestMapping("/server-location")
    public String serverLocation() {
        return this.restTemplate.getForObject(
          "http://ping-server/locaus", String.class);
    }
 
    public static void main(String[] args) {
        SpringApplication.run(ServerLocationApp.class, args);
    }
}

2. Dependency Management
-------------------------
   The Netflix Ribbon API can be added to our project by adding the below dependency to our pom.xml:

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-ribbon</artifactId>
</dependency>

-------------------------------

3. Example Application
------------------------
In order to see the working of Ribbon API, we build a sample microservice application with Spring RestTemplate and we enhance it with Netflix Ribbon API along with Spring Cloud Netflix API.

We'll use one of Ribbon's load-balancing strategies, WeightedResponseTimeRule, to enable the client side load balancing between 2 servers, which are defined under a named client in the configuration file, in our application.
------------------------------------------------------------------------------------------------

4. Ribbon Configuration
------------------------
Ribbon API enables us to configure the following components of the load balancer:

Rule – Logic component which specifies the load balancing rule we are using in our application
Ping – A Component which specifies the mechanism we use to determine the server's availability in real-time
ServerList – can be dynamic or static. In our case, we are using a static list of servers and hence we are defining them in the application configuration file directly
Let write a simple configuration for the library:

public class RibbonConfiguration {
 
    @Autowired
    IClientConfig ribbonClientConfig;
 
    @Bean
    public IPing ribbonPing(IClientConfig config) {
        return new PingUrl();
    }
 
    @Bean
    public IRule ribbonRule(IClientConfig config) {
        return new WeightedResponseTimeRule();
    }
}
Notice how we used the WeightedResponseTimeRule rule to determine the server and PingUrl mechanism to determine the server's availability in real-time.

According to this rule, each server is given a weight according to its average response time, lesser the response time gives lesser the weight. This rule randomly selects a server where the possibility is determined by server's weight.

And the PingUrl will ping every URL to determine the server's availability.

----------------------------------------------------------------------------------

5. application.yml
----------------------------------------------------------
Below is the application.yml configuration file we created for this sample application:

spring:
  application:
    name: spring-cloud-ribbon
 
server:
  port: 8888
 
ping-server:
  ribbon:
    eureka:
      enabled: false
    listOfServers: localhost:9092,localhost:9999
    ServerListRefreshInterval: 15000
In the above file, we specified:

Application name
-----------------
Port number of the application
Named client for the list of servers: “ping-server”
Disabled Eureka service discovery component, by setting eureka: enabled to false
Defined the list of servers available for load balancing, in this case, 2 servers
Configured the server refresh rate with ServerListRefreshInterval
---------------------------------------------------------------------------------------------
6. RibbonClient
--------------------------------------------------------------------------------------------
Let's now set up the main application component snippet – where we use the RibbonClient to enable the load balancing instead of the plain RestTemplate:

@SpringBootApplication
@RestController
@RibbonClient(
  name = "ping-a-server",
  configuration = RibbonConfiguration.class)
public class ServerLocationApp {
 
    @LoadBalanced
    @Bean
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
 
    @Autowired
    RestTemplate restTemplate;
 
    @RequestMapping("/server-location")
    public String serverLocation() {
        return this.restTemplate.getForObject(
          "http://ping-server/locaus", String.class);
    }
 
    public static void main(String[] args) {
        SpringApplication.run(ServerLocationApp.class, args);
    }
}

We defined a controller class with the annotation @RestController; we also annotated the class with @RibbonClient with a name and a configuration class.

The configuration class we defined here is the same class that we defined before in which we provided the desired Ribbon API configuration for this application.

Notice we also annotated the RestTemplate with @LoadBalanced which suggests that we want this to be load balanced and in this case with Ribbon.
-----------------------------------------------------------------------------------------------------------------------------------------------
7. Failure Resiliency in Ribbon
As we discussed earlier in this article, Ribbon API not only provides client side load balancing algorithms but also it has built in failure resiliency.

As stated before, Ribbon API can determine the server's availability through the constant pinging of servers at regular intervals and has a capability of skipping the servers which are not live.

In addition to that, it also implements Circuit Breaker pattern to filter out the servers based on specified criteria.

The Circuit Breaker pattern minimizes the impact of a server failure on performance by swiftly rejecting a request to that server that is failing without waiting for a time-out. We can disable this Circuit Breaker feature by setting the property niws.loadbalancer.availabilityFilteringRule.filterCircuitTripped to false.

When all servers are down, thus no server is available to serve the request, the pingUrl() will fail and we receive an exception java.lang.IllegalStateException with a message “No instances are available to serve the request”.

#1. What Is Spring Cloud?
-------------------------
     Spring Cloud, in microservices, is a system that provides integration with external systems. 
     It is a short-lived framework that builds an application, fast. Being associated with the finite amount of data processing,
     it plays a very important role in microservice architectures.
    
    For typical use cases, Spring Cloud provides the out of the box experiences and a sets of extensive features mentioned below:
    
    Versioned and distributed configuration.
    Discovery of service registration.
    Service to service calls.
    Routing.
    Circuit breakers and load balancing.
    Cluster state and leadership election.
    Global locks and distributed messaging.

#2. What Is Spring Boot?
-----------------------
    Spring boot is a major topic under the umbrella of microservices interview questions.
    With the new functionalities that have been added, Spring keeps getting more complex. 
    Whenever you are starting a new project, it is mandatory to add a new build path or Maven dependencies.
    In short, you will need to do everything from scratch. Spring Boot is the solution that will help you to avoid all the code configurations.

3.How Do You Override a Spring Boot Project’s Default Properties?
-------------------------------------------------------------------
    This can be done by specifying the properties in the application.properties file.
    For example, in Spring MVC applications, you have to specify the suffix and prefix. This can be done by entering the properties mentioned 
    below in the application.properties file.
    
    For suffix – spring.mvc.view.suffix: .jsp
    
    For prefix – spring.mvc.view.prefix: /WEB-INF/
-------------------------------------------------------------------
4.Role of Actuator in Spring Boot
-------------------------------------------------------------------
    It is one of the most important features, which helps you to access the current state of an application that is running in a production environment. 
    There are multiple metrics which can be used to check the current
    state. They also provide endpoints for RESTful web services which can be simply used to check the different metrics.

5.How Is Spring Security Implemented In a Spring Boot Application?
------------------------------------------------------------------- 
    Minimal configuration is needed for implementation. All you need to do is add the spring-boot-starter-security starter in the pom.xml file.
    You will also need to create a Spring config class that will override the
    required method while extending the WebSecurityConfigurerAdapter to achieve security in the application. Here is some example code:

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
@Override
protected void configure(HttpSecurity http) throws Exception {
http.authorizeRequests()
.antMatchers("/welcome").permitAll()
.anyRequest().authenticated()
.and()
.formLogin()
.permitAll()
.and()
.logout()
.permitAll();
}
}

6. Which Embedded Containers Are Supported by Spring Boot?
Whenever you are creating a Java application, deployment can be done via two methods:

Using an application container that is external.
Embedding the container inside your jar file.
Spring Boot contains Jetty, Tomcat, and Undertow servers, all of which are embedded.

Jetty – Used in a wide number of projects, Eclipse Jetty can be embedded in framework, application servers, tools, and clusters.

Tomcat – Apache Tomcat is an open source JavaServer Pages implementation which works well with embedded systems.

Undertow – A flexible and prominent web server that uses small single handlers to develop a web server.

7.What Do You Mean by End-To-End Testing of Microservices?
------------------------------------------------------------------
End-to-end testing validates all the processes in the workflow to check if everything is working as expected. 
It also ensures that the system works in a unified manner, thereby satisfying the business requirement.

8.What Is Semantic Monitoring?
  It combines monitoring of the entire application along with automated tests. The primary benefit of 
  Semantic Monitoring is to find out the factors which are more profitable to your business.

Semantic monitoring along with service layer monitoring approaches monitoring of microservices from a business point of view.
 Once an issue is detected, they allow faster isolation and bug triaging, thereby reducing
the main time required to repair. It triages the service layer and transaction layer to figure out the transactions affected by availability or poor performance.

9. How Can You Set Up Service Discovery?
There are multiple ways to set up service discovery. I’ll choose the one that I think to be most efficient, Eureka by Netflix. It is a hassle free procedure that does not weigh much on the application. Plus,
it supports numerous types of web applications.

Eureka configuration involves two steps – client configuration and server configuration.
Client configuration can be done easily by using the property files. In the class path, Eureka searches for a eureka-client.properties file. It also searches for overrides caused by the environment in property files
which are environment specific.

For server configuration, you have to configure the client first. Once that is done, the server fires up a client which is used to find other servers. The Eureka server, by default, uses the Client configuration to
find the peer server.

10. Why Would You Opt for Microservices Architecture?
This is a very common microservices interview question which you should be ready for! There are plenty of pros that are offered by a microservices architecture. 
Here are a few of them:

Microservices can adapt easily to other frameworks or technologies.
Failure of a single process does not affect the entire system.
Provides support to big enterprises as well as small teams.
Can be deployed independently and in relatively less time.
11. Why Would You Need Reports and Dashboards in Microservices?
Reports and dashboards are mainly used to monitor and upkeep microservices. There are multiple tools that help to serve this purpose. Reports and dashboards can be used to:

Find out which microservices expose what resources.
Find out the services which are impacted whenever changes in a component occur.
Provide an easy point which can be accessed whenever documentation is required.
Versions of the components which are deployed.
To obtain a sense of maturity and compliance from the components.

12. Why Do People Hesitate to Use Microservices?
I have seen many devs fumble over this question. After all, they're getting asked this question when interviewing for a microservices architect role, so acknowledging its cons can be a little tricky.
Here are some good answers:

They require heavy investment – Microservices demand a great deal of collaboration. Since your teams are working independently, they should be able to synchronize well at all times.
They need heavy architecture set up – The system is distributed, the architecture is heavily involved.
They need excessive planning for handling operations overhead – You need to be ready for operations overhead if you are planning to use a microservices architecture.
They have autonomous staff selection – Skilled professionals are needed who can support microservices that are distributed heterogeneously.

13. How Does PACT Work?

PACT is an open source tool. It helps in testing the interactions between consumers and service providers. 
However, it is not included in the contract, increasing the reliability of the application.
The consumer service developer starts by writing a test which defines a mode of interaction with the service provider. 
The test includes the provider’s state, the request body, and the response that is expected.
Based on this, PACT creates a stub against which the test is executed. The output is stored in a JSON file.

14. Define Domain Driven Design
----------------------------------
The main focus is on the core domain logic. Complex designs are detected based on the domain’s model. 
This involves regular collaboration with domain experts to resolve issues related to the domain and improve
the model of the application. While answering this microservices interview question, you will also need to mention the core fundamentals of DDD. They are:

DDD focuses mostly on domain logic and the domain itself.
Complex designs are completely based on the domain’s model.
To improve the design of the model and fix any emerging issues, DDD constantly works in collaboration with domain experts.

15. What Are Coupling and Cohesion?
Coupling can be considered to be the measurement of strength between the dependencies of a component.
A good microservices application design always consists of low coupling and high cohesion.

Interviewers will often ask about cohesion. It is also another measurement unit. More like a degree to which the elements inside a module remain bonded together.

It is imperative to keep in mind that an important key to designing microservices is a composition of low coupling along with high cohesion. When loosely coupled, a service knows very little about other services.
 This keeps the services intact. In high cohesion, it becomes possible to keep all the related logic in a service. Otherwise, the services will try to communicate with each other, impacting the overall
 performance.

16. What Is OAuth?
Open Authorization Protocol, otherwise known as OAuth, helps to access client applications using third-party protocols like Facebook, GitHub, etc., via HTTP. You can also share resources between different sites
without the requirement of credentials.

OAuth allows the account information of the end user to be used by a third-party like Facebook while keeping it secure (without using or exposing the user’s password).
 It acts more like an intermediary on the user’s behalf while providing a token to the server for accessing the required information.

17. Why Do We Need Containers for Microservices?
To manage a microservice-based application, containers are the easiest alternative. It helps the user to individually deploy and develop. You can also use Docker to encapsulate microservices in the image of a container. Without any additional dependencies or effort, microservices can use these elements.

18. What Are the Ways to Access RESTful Microservices?
Another one of the frequently asked microservices interview questions is how to access RESTful microservices? You can do that via two methods:

Using a REST template that is load balanced.
Using multiple microservices.
19. What Are Some Major Roadblocks for Microservices Testing?
Talking about the cons, here is another one of the microservices interview questions you may be ready for, will be around the challenges faced while testing microservices.

Testers should have a thorough understanding of all the inbound and outbound processes before they start writing the test cases for integration testing.
When independent teams are working on different functionalities, collaboration can prove to be quite a struggling task. It can be tough to find an idle time-window to perform a complete round of regression testing.
With an increasing number of microservices, the complexity of the system also increases.
During the transition from monolithic architecture, testers must ensure that there is no disruption between the internal communication among the components.
20. Common Mistakes Made While Transitioning to Microservices
Not only on development, but mistakes also often occur on the process side. And any experienced interviewer will have this in the queue for microservices interview questions. Some of the common mistakes are:

Often the developer fails to outline the current challenges.
Rewriting the programs that are already existing.
Responsibilities, timeline, and boundaries not clearly defined.
Failing to implement and figure out the scope of automation from the very beginning.
21. What Are the Fundamentals of Microservices Design?
This is probably one of the most frequently asked microservices interview questions. Here is what you need to keep in mind while answering to it:

Define a scope.
Combine loose coupling with high cohesion.
Create a unique service which will act as an identifying source, much like a unique key in a database table.
Creating the correct API and taking special care during integration.
Restrict access to data and limit it to the required level.
Maintain a smooth flow between requests and response.
Automate most processes to reduce time complexity.
Keep the number of tables to a minimum level to reduce space complexity.
Monitor the architecture constantly and fix any flaw when detected.
Data stores should be separated for each microservice.
For each microservice, there should be an isolated build.
Deploy microservices into containers.
Servers should be treated as stateless.
You can also follow this article explaining 9 Fundamentals to a Successful Microservice Design.

22. Where Do We Use WebMVC Test Annotation?
WebMvcTest is used for unit testing Spring MVC applications. As the name suggests, it focuses entirely on Spring MVC components. For example,
@WebMvcTest(value = ToTestController.class, secure = false):
Here, the objective is to only launch ToTestController. Until the unit test has been executed, other mappings and controllers will not be launched.

23. What Do You Mean by Bounded Context?
A central pattern is usually seen in domain driven design. Bounded context is the main focus of the strategic design section of DDD. It is all about dealing with large teams and models.
DDD works with large models by disintegrating them into multiple bounded contexts. While it does that, it also explains the relationship between them explicitly.

24. What Are the Different Types of Two-Factor Authentication?
There are three types of credentials required for performing two-factor authentication.

A thing that you know – like password or pin or screen lock pattern.
A physical credential that you have – like OTP or phone or an ATM card, in other words, any kind of credential that you have in an external or third-party device.
Your physical identity – like voice authentication or biometric security, like a fingerprint or eye scanner.
25. What Is a Client Certificate?
This is a type of digital certificate usually used by client systems for making a request that is authenticated by a remote server. It plays an important role in authentication designs that are mutual and provides strong assurance of the identity of a requester. However, you should have a fully configured backend service for authenticating your client certificate.

26. What Is Conway's Law?
Conway’s Law states, “organizations which design systems … are constrained to produce designs which are copies of the communication structures of these organizations.”

The interviewer may ask a counter microservices interview question, like how is Conway's Law related to microservices. Well, some loosely coupled APIs form the architecture of microservices. The structure is well suited to how a small team is implementing components which are autonomous. This architecture makes an organization much more flexible in restructuring its work process.

27. How to Configure Spring Boot Application Logging?
Spring Boot comes with added support for Log4J2, Java Util Logging, and Logback. It is usually pre-configured as console output. They can be configured by only specifying logging.level in the application.properties file.

logging.level.spring.framework=Debug

28. How Would You Perform Security Testing on Microservices?
Before answering this microservices interview question, explain to the interviewer that microservices cannot be tested as a whole. You will need to test the pieces independently. There are three common procedures:

Code scanning – To ensure that any line of code is bug-free and can be replicated.

Flexibility – The security solution should be flexible so that it can be adjusted as per the requirements of the system.

Adaptability – The security protocols should be flexible and updated to cope up with the new threats by hackers or security breaches.

You can also check out this article explaining the influence of Microservices architecture on security.

29. What Is Idempotence and How Is it Used?
Idempotence refers to a scenario where you perform a task repetitively but the end result remains constant or similar.
----------------------------------------------------------------------------------------------------------------------
#Microservices Architecture::#Microservices Course
----------------------------------------------------
1.Evolution of Microservices
-----------------------------
Learning Objectives: In this Module, you will learn how Microservices have evolved over time and how different is Microservices from SOA.
In addition, you will get to know about different architectures and where does Microservices architecture fit.

Topics:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Monolithic Architecture:::
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
Distributed Architecture::::
----------------------------

----------------------------
Service oriented Architecture::
----------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------
Microservice and API Ecosystem::
---------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------
Microservices in nutshell::
----------------------------------------------------------------------------------------------------------------------------------
Point of considerations
SOA vs. Microservice
Microservice & API

Skills:
Architecture styles
Advantages of different architecture styles
Limitations of Architectures
What is Microservices
------------------------------------------------
2.Microservices Architecture
--------------------------------------------------
Learning Objectives: Learn the various principles of REST, the various characteristics of Microservices, the importance of messaging in Microservices architecture, and the concept of distributed transactions.

Topics:
REST Architecture principles:::
------------------------------
REST stands for REpresentational State Transfer and API stands for Application Program Interface.
REST is a software architectural style that defines the set of rules to be used for creating web services.
Web services which follow the REST architectural style are known as RESTful web services.
It allows requesting systems to access and manipulate web resources by using a uniform and predefined set of rules.
Interaction in REST based systems happen through Internet’s Hypertext Transfer Protocol (HTTP).

A Restful system consists of a:

-> client who requests for the resources.
-> server who has the resources.

It is important to create REST API according to industry standards which results in ease of development and increase client adoption.

Architectural Constraints of RESTful API:
------------------------------------------ There are six architectural constraints which makes any web service are listed below:

1.Uniform Interface
2.Stateless
3.Cacheable
4.Client-Server
5.Layered System
6.Code on Demand

The only optional constraint of REST architecture is code on demand. If a service violates any other constraint, it cannot strictly be referred to as RESTful.

1. Uniform Interface:
-------------------  It is a key constraint that differentiate between a REST API and Non-REST API.
It suggests that there should be an uniform way of interacting with a given server irrespective of device or type of application (website, mobile app).
There are four guidelines principle of Uniform Interface are:

Resource-Based:
-------------- Individual resources are identified in requests. For example: API/users.

Manipulation of Resources Through Representations:
-------------------------------------------------- Client has representation of resource and it contains enough information to modify or delete the resource on the server,
provided it has permission to do so. Example: Usually user get a user id when user request for a list of users and then use that id to delete or modify that particular user.

Self-descriptive Messages:
--------------------------Each message includes enough information to describe how to process the message so that server can easily analyses the request.

Hypermedia as the Engine of Application State (HATEOAS):
-------------------------------------------------------- It need to include links for each response so that client can discover other resources easily.

2.Stateless:
------------ It means that the necessary state to handle the request is contained within the request itself and server would not store anything related to the session.
In REST, the client must include all information for the server to fulfill the request whether as a part of query params, headers or URI.
Statelessness enables greater availability since the server does not have to maintain, update or communicate that session state.
There is a drawback when the client need to send too much data to the server so it reduces the scope of network optimization and requires more bandwidth.

3. Cacheable:
-------------- Every response should include whether the response is cacheable or not and for how much duration responses can be cached at the client side.
Client will return the data from its cache for any subsequent request and there would be no need to send the request again to the server.
A well-managed caching partially or completely eliminates some client–server interactions, further improving availability and performance.
But sometime there are chances that user may receive stale data.

4. Client-Server:
--------------REST application should have a client-server architecture. A Client is someone who is requesting resources and are not concerned with data storage,
which remains internal to each server, and server is someone who holds the resources and are not concerned with the user interface or user state. They can evolve independently.
Client doesn’t need to know anything about business logic and server doesn’t need to know anything about frontend UI.

5. Layered system:
-------------------An application architecture needs to be composed of multiple layers. Each layer doesn’t know any thing about any layer other than that of immediate layer and
there can be lot of intermediate servers between client and the end server. Intermediary servers may improve system availability by enabling load-balancing and by providing shared caches.

6. Code on demand:
------------------- It is an optional feature. According to this, servers can also provide executable code to the client. The examples of code on demand may include
the compiled components such as Java applets and client-side scripts such as JavaScript.

Rules of REST API:
------------------ There are certain rules which should be kept in mind while creating REST API endpoints.

-> REST is based on the resource or noun instead of action or verb based. It means that a URI of a REST API should always end with a noun.
Example: /api/users is a good example, but /api?type=users is a bad example of creating a REST API.
->HTTP verbs are used to identify the action. Some of the HTTP verbs are – GET, PUT, POST, DELETE, UPDATE, PATCH.
->A web application should be organized into resources like users and then uses HTTP verbs like – GET, PUT, POST, DELETE to modify those resources.
 And as a developer it should be clear that what needs to be done just by looking at the endpoint and HTTP method used.
->Always use plurals in URL to keep an API URI consistent throughout the application.
->Send a proper HTTP code to indicate a success or error status.

HTTP verbs: Some of the common HTTP methods/verbs are described below:
-----------
-> GET: Retrieves one or more resources identified by the request URI and it can cache the information receive.
-> POST: Create a resource from the submission of a request and response is not cacheable in this case.
       This method is unsafe if no security is applied to the endpoint as it would allow anyone to create a random resource by submission.
-> PUT: Update an existing resource on the server specified by the request URI.
-> DELETE: Delete an existing resource on the server specified by the request URI. It always return an appropriate HTTP status for every request.
-> GET, PUT, DELETE methods are also known as Idempotent methods. Applying an operation once or applying it multiple times has the same effect.
   Example: Delete any resource from the server and it succeeds with 200 OK and then try again to delete that resource than it will display an error message 410 GONE.

-----------------------------------------------------------------------------
Microservice Characteristics
------------------------------Microservices includes so many concepts that it is challenging to define it precisely.
However, all microservices architectures share some common characteristics, as the following figure illustrates:

1.Decentralized::
-------------– Microservices architectures are distributed systems with decentralized data management. They don’t rely on a unifying schema in a central database.
 Each microservice has its own view on data models. Microservices are also decentralized in the way they are developed, deployed, managed, and operated.

2.Independent::
-----------– Different components in a microservices architecture can be changed, upgraded, or replaced independently without affecting the functioning of other components.
Similarly, the teams responsible for different microservices are enabled to act independently from each other.

3.Do one thing well::
-----------------– Each microservice component is designed for a set of capabilities and focuses on a specific domain. If developers contribute so much code to a particular component
 of a service that the component reaches a certain level of complexity, then the service could be split into two or more services.

4.Polyglot::
----------– Microservices architectures don’t follow a “one size fits all” approach. Teams have the freedom to choose the best tool for their specific problems.
 As a consequence, microservices architectures take a heterogeneous approach to operating systems, programming languages, data stores, and tools.
 This approach is called polyglot persistence and programming.

5.Black box::
---------- – Individual microservice components are designed as black boxes, that is, they hide the details of their complexity from other components. Any communication between services happens via well- defined APIs to prevent implicit and hidden dependencies.

You build it; you run it:::
------------------------ – Typically, the team responsible for building a service is also responsible for operating and maintaining it in production.
 This principle is also known as DevOps. DevOps also helps bring developers into close contact with the actual users of their software and improves their understanding of the
 customers’ needs and expectations. The fact that DevOps is a key organizational principle for microservices shouldn’t be underestimated because according to Conway’s law,
 system design is largely influenced by the organizational structure of the teams that build the system.
-----------------------------------------------------------------------------
Inter-Process Communications
-----------------------------


-----------------------------
Microservice Transaction Management

Skills:
Considerations while building microservices
How the services communicate with each other
How the transaction management is done in microservice.
-----------------------------------------------------------
3.Microservices - Design
-------------------------
Learning Objectives: This Module gives you an insight into Domain Driven Design, the approach called Big Ball of Mud,
 the approaches and their strategies that can be used while moving from Monolithic to Microservices.

 Topics:
Domain Driven Design
Big Mud Ball to Sweet Gems
Untangling the Ball of MUD
Kill the MUD Ball growth
Repackaging/Refactoring
Decouple the User interface and Backend Business Logic
MUD Ball to Services
Microservice Design Patterns
Microservice Architecture Decisions

Hands-on:
Setting up the root project
Spring Boot - Hello World
Returning json entity as response
Spring Boot dev tools
Intro to Lombok
Adding Items to Mongo DB
Querying Mongo
Accessing an SQL database
Spring Data Rest and HATEOAS
Connecting to an Elasticsearch Server
Searching our Elasticsearch Server

Skills:
Architecture Decisions
Monolithic to Microservices redesign.
Learn to identify and design microservices.
-----------------------------------------------
4.Microservices - Security
------------------------------------------------
Learning Objectives: Know why security is an important factor to be considered in Microservices. Learn what are the various best practices in Microservice security design,
and what techniques can be used to implement security.

Topics:
Why is Security important?
Microservice Security Principles
Microservice Security techniques
Access Tokens
Oauth 2.0
How to secure a Microservice using OAuth 2.0

Hands-on:
Spring Boot Security Setup
Basic Spring security
Moving to Oauth2
Implementing Single Sign On
Implementing Authorization Server
Implementing Resource Server

Skills:
Oauth 2.0
Security tokens
Secure by design
------------------------------------------------------
5.Microservices - Testing
Learning Objectives: Learn the different testing strategies that can be implemented in Microservices, how Spring Boot features help in testing Microservices, and the various testing tools that are available to be used.


Topics:
Testing scenarios and strategy
Test at Different Levels
Testing Best Practice for Microservices

Skills:
Testing methodology
How to test Microservices
------------------------------------------------
6. Microservices Reference Architecture
Learning Objectives: Get an insight into Microservices reference architecture, what are the key Microservice enablers and how do DevOps and Microservice go hand in hand. In addition, know what features an API system provide to Microservices, and how Netflix has benefited by implementing Microservices.

Topics:
Reference Architecture
Microservice Enablerc
Microservices @ Netflix

Hands-on:
Reading properties in various ways
Implementing config server
Setting up Discovery Server
Setting up Discovery Client
Overview of Actuator Endpoints
API Gateway and Dynamic Routing
IDeclarative Rest Client
Hystrix Fault Tolerance
Distributed Caching
Distributed Sessions
Need for Event Driven Systems
Building Event Driven Systems
Implementing Distributed Tracing
Understanding Metrics
Monitoring Microservices
Spring Boot Admin

Skills:
Scalable Architecture
How Netflix uses Microservices
How cloud and DevOps enables Microservice architecture
----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------

Microservice:::
----------------
1. What is Microservices?
-------------------------
-> Microservices is an sculptural type subtype of service-oriented architecture (SOA) that constructs an application as a collection of services that are loosely coupled.
Services must be great-textured and guidelines must be compact in a microservice architecture. The advantage of breaking down an application into various smaller services is how it
enhances modifiability and makes it much easier to recognize, design and implement the application. This also develops mutability by allowing a small independent teams to separately create,
configure and measure their differing services. It also enables an individual service’s aesthetics to arise via constant modularizing. Architectures predicated on microservices allow continuous
integration and deployment.

2. What are Microservices and its architecture?
---------------------------------------------------
In software development, Microservices is an architectural trend used to comprise several of the small services. Furthermore, each microservice is limited to one single corporate app.
Together all these micro-services construct a larger or more complicated application that satisfies the overall industry requirement. The most obvious design elements in the microservices
include flexible or adaptable, scalable, loose coupling, composable, corporate, componenised, distributed, decentralized.

3. What are the advantages of Microservices Architecture?
----------------------------------------------------------
With the support of their individual functionalities and services, microservice architecture can be formed and dispatched autonomously in any application.
The program still works even if one of the application’s services stops working.
Various languages and advancements can be used to construct the different services for the same program.
Instead of leveling all the modules at once, individual modules can also be adjusted as per the needs.
-----------------------------------------------------------------------------------------------------------

4. What are some important characteristics of Microservices?
--------------------------------------------------------------------
It is possible to detach the services in the system application so that the entire application can be constructed, adjusted and modified as a whole.
Microservices modules are viewed separately and can therefore be readily switched and upgraded.
Microservices are incredibly easy because they demonstrate the ability of a single company.
Microservices give software developer teams sovereignty to operate independently from each other, that enables to accelerate.
It causes continual launches of computer software using deliberate mechanisation to create, measure and approve computer software.
Microservices treat software applications as responsible brands rather than concentrating on applications as projects.
Without reliing on interoperability, Microservices utilizes the right equipment.
Microservices enables rapid development and rejection of functionality.

5. What are the different features of Microservices application?
----------------------------------------------------------------
This is the fundamental question asked during the microservices interview. Below are the various features that Microservices supports:

Continuous Development: The two primary streams of continuous development that drive periodic launches and application deployment, include continuous integration and development.
Component-based: It is possible to easily upgrade, modify or supplant each microservice with the appropriate equivalent element.
Loosely Coupled: This allows the device to be constructed quickly and efficiently and helps to produce brands more quickly.
Scalability: Scaling the application for the amount of subscribers is simpler.

--------------------------------------
6. What is a Gateway in microservice?
-------------------------------------
Customers don’t call on microservices directly, they go via the Gateway. The Gateway calls the microservices and returns the answer to the customer.
The Gateway separates the customer’s microservices. It also offers authentication, logging, load balancing and so on.

------------------------------------------------
7. How do you monitor microservices?
----------------------------------------------
The Spring Boot Controller is a decent instrument monitor statistics for individual microservices. However, it is hard to track them independently when you have multiple microservices.
We could use opensource tools such as Prometheos, Kibana or Graphana for this purpose. Prometheous is a pull-based surveillance tool. It contains, shows and can activate notifications at
certain intervals. Kibana or Grafana are dashboard instruments for data visualization and surveillance. As there are many dependencies in microservices, AppDynamics, Dynatrace and New Relics
can be applied, which pull dependence between microservices.

----------------------------------------------------
8. How will you define the concept of Microservices?
-------------------------------------------------------
Microservices structures an application in an style of architecture as a collection of little self-contained services that fit into a corporation profession.
Usually you begin with just a little block and can create a bigger application together. The units are then organised in a sequence that later structures a tougher part of the bee hive.
So each unit is attached but autonomous and can be adjusted according to prerequisites. This implies that harm to a unit does not impact an another unitʻs productivity.
This allows destroyed unitʻs to be rapidly recreated and an software could do the same kind of.

-----------------------------------------------------------------------
9. When designing Microservices, what are the best practices to follow?
------------------------------------------------------------------------
These are the industry standards each programmer must follow in the design of Microservices:

Every microservice will often keep the information store separate.
When designing Microservices it is important that the code must be organised at the same intelligence level.
For each Microservice, a separate development should also be designed.
We should make sure that each development should be used in containers.
While designing the microservices it is always necessary to treat the server as stateless.

-----------------------------------------------------------------
10. Explain the major components of a Microservices Architecture
------------------------------------------------------------------
The following are the principle components of a Microservice architecture are discribed as follows:

Clients: Various subscribers send queries from various devices.
Identity Providers: It can configure user information and provide users with service tokens.
Static Content: It enables to store the entire system functionality for longer periods.
API Gateway: This gateway works along with several queries.
Management: It will balance Services over endpoints and missteps will also be identified.
CDNs: The content delivery network is a distributed network of proxy servers and their respective data centers.
Service Discovery: This will actually find the path of communication between the various Microservices.
Remote Services: It will allow remote access to network or other IT devices information.

--------------------------
11. What Is Spring Cloud?
--------------------------
Spring Cloud is a system for integrating with external systems in Microservices. It is a short – lived paradigm which quickly designs an application. It has a really vital role in
Microservice architectural design in connection with the limitless amount of information processing. Spring cloud offers a certain degree of actual experience in standard use cases as well as
a series of comprehensive functionalities below this:

Versioned and distributed configuration
Discovery of service registration
Service to service calls
Routing
Circuit breakers and load balancing
Cluster state and leadership election
Global locks and distributed messaging
Getting into micro-service implmentation

-------------------------
12. What Is Spring Boot?
-------------------------
Spring has become much more complicated with the additional features introduced. If you begin a new project, adding a new building path or Maven dependencies is necessary.
Simply put, you must do all from scratch. Spring Boot is the answer for preventing all code settings. The ingredients may be considered spring when you cook a dish.
When you cook a dish, it is fair to accept the ingredients as spring. The entire cooked dish is Spring Boot.

-----------------------------------------------------
13. Explain three types of Tests for Microservices
-----------------------------------------------------
In Microservices architecture tests are categorized into three wide types:

We can conduct a particular test such as quality and unit tests at the lower level test. Such tests are fully automated.
We can conduct exploratory tests at the mid – level, such as stress tests and accessibility tests.
We can perform acceptance tests at the highest level that are mainly less numerical. It enables investors to understand the various software characteristics.

------------------------------------------------------------------------------
14. What are the challenges you face while working Microservice Architectures?-
-----------------------------------------------------------------------------------
It is easy to develop a lot of smaller microservices however the obstacles they sometimes face were as follows:

Automate the Components: Hard to optimize as a variety of small modules are available. We will have to adopt the building, deployment and monitoring phases for each module.
Perceptibility: It is hard to deploy, preserve, supervise and address troubles to keep a huge number of modules together. All modules need to be highly perceptible.
Configuration Management: It is often difficult to maintain the configurations for modules in different environments.
Debugging: Every service is difficult to find for a mistake. Centralized logging and workflows are crucial for debugging issues.

------------------------------------------------------------------------------------------------
15. Shed light on the basic need of Microservices in today’s context of application development
------------------------------------------------------------------------------------------------
In the field of software development, micro services are also recognised as a new pattern. It has taken on importance because it can increase efficiency and speed. It is also able to efficiently
maintain software and services. Some people also call it a method and culture solution that gives greater business performance. It actually plays an extremely vital role in today’s software
development, as it is able to be deployed efficiently in the production of a monolithic application.

----------------------------------------------------------------
16. Which DB is preferred by Microservices based Architecture?
----------------------------------------------------------------
Database selection affects the form of microservice information to be stored in the database. So, Ephemeral, transactional, transitional and operational data are the main four types of data
that are stored. When choosing or selecting a database type, consider the data storage quantity and duration. Also assume the microservices ‘ quality and throughput requirements.

----------------------------------------------------------------
17. Why Would You Need Reports & Dashboards In Microservices?
---------------------------------------------------------------
Most of the reports and dashboards were used for monitoring and maintaining microservices. Multiple options are available to help support this motive. It is possible to use reports and dashboards
to:

Find out which resources are exposed to microservices.
Find out which services are affected whenever module alterations take place.
Provide an instant access point anytime information is needed.
Provides information about the versions of the deployed components or modules.
To give the components a level of self awareness and compliance.

-------------------------
18. How Does PACT Work?
-----------------------
PACT is an tool that is open source(freely available). It enables to exam customer and service provider relationships. It isn’t included in the lease, however, greatly increasing the
application’s reliability. The customer service developer begins by writing a test that characterizes how the service provider interacts. The test involves the status of the supplier,
the application body and the predicted response. PACT generates a photocopy against which the test is performed on the basis of it. Store the output in a JSON file.

-----------------------------------
19. What is Coupling and Cohesion?
----------------------------------
The coupling can be seen as the intensity calculation between the module reliance. A fine software design for Microservices often comprises of high cohesion and low coupling.
When you are asked questions of microservices, it is important to remember that a structure of low coupling alongside high cohesion is an essential component in designing microservices.
A service understands very little about another when it is loosely coupled. The services are therefore preserved. All pertaining reasoning can be maintained in a service in a higher level of
cohesion. In other cases, the services will attempt and influence overall quality to interact with each other.

---------------------------------
Advanced Microservices questions

20. How Would You Perform Security Testing of Microservices?
-------------------------------------------------------------
Explain to the interviewer before responding to this question about the microservices that microservices can not be fully tested. You have to test the parts on your own.
There are three popular methods:

Code scanning – To make sure that every code line is bug – free and replicable.

Flexibility – In order to be adaptable to system requirements, the security measure should be flexible.

Adaptability – In order to address the new problems from spammers or security breaches, safety procedures should also be adaptable and upgraded.

-------------------------------------------------------------
21. What Are Some Major Roadblocks For Microservices Testing?
--------------------------------------------------------------
Before beginning to type test cases for integration testing, testers ought to have a comprehensive knowledge of all incoming and outgoing processes.
When autonomous teams actually work on unique features, cooperation can be a very difficult task. A time – door for complete regression testing can be difficult to find.
The complexity of the system also increases with an increased number of microservices.
Testors should make sure no disturbance between the information exchange between the modules while transitioning from monolithic architecture.

22. Why Do People Hesitate In Using Microservices?
--------------------------------------------------
Requires heavy investment – A lot of cooperation is required for microservices. Your teams should be able to update too well at times as they operate independently.
Heavy architecture set up – The entire system is usually spread, the architectural style is strong.
Excessive planning for handling operations overhead – If you intend to use Microservices architecture, you must be prepared for overhead operations.
Autonomous staff selection – Qualified experts are required to provide full support for heterogeneous microservices.

--------------------------------
23. What Is Semantic Monitoring?
--------------------------------
The entire application is monitored and computerized tests are combined. Semantic Monitoring’s principal advantage is to discover out the qualities that make your business much more profitable.
After a problem has been detected, it allows for faster insulation and bug triage, reducing the main repair time. After a problem has been detected, it allows for quicker insulation and bug
emergency care, lowering the primary maintenance time. It tries to identify the transfers which are impacted by availability or lack of quality.

-------------------------------------------------------------------
24. How can we separate Deployment from Release of Microservices?
------------------------------------------------------------------
Two distinct MicroServices events are Deployment and Release. Blue / green deployment is one way to do this. Two variants of a Microservice were deployed simultaneously in this case.
However, only one version actually makes genuine demands. After we have examined the other version well though, we switch the versions. In order that the functionality runs properly in
freshly dispatched versions, we may run a smoke test suite. The new version could be published live based on the outcome of the smoke test.

-------------------------------------------------------------------
25.What is the difference between Authentication and Authorization?
-------------------------------------------------------------------
Authentication: Before disclosing confidential information, the authentication system defines the identifying of the user. It is very important to preserve the sensitive information for
the system or interfaces, where the user has the priority. The user then claims the identity of an person (his or her) or of an organisation. The username, password, fingerprint,
etc might be the credentials or claims. The application layer deals with authentication and non-repudiation, kind of problems. The inefficient method for authentication might greatly impact the service’s availability.

Authorization: The authorization method is being used to determine exactly which authorizations an authenticated user is granted. The authorisation is given when the identity of the user
is guaranteed before, then by checking up the entries stored in the tables and databases, the user access list will be established. The authorization is given when the identity of the new
user is guaranteed before, then by checking up the entries stashed in the tables and databases, the permissions list will be established.

26. What is Bounded Context?
Bounded context in the field of domain driven design is a central pattern. DDD’s tactical design segment focuses on the treatment of huge models and teams. DDD addresses huge models by
splitting them into different boundary contexts and making their interactions explicit. DDD deals with product design predicated on domain models. A model serves as an UbiquitousLanguage
to enable interaction among software engineers and field professionals. Also it behaves as the abstract base for the computer software itself and the way in which it is divided into
objects and functions. In order for a system to be efficient, it has to be unified-internally coherent to avoid contradictions.

27. What is a Consumer Driven Contract (CDC)?
Each consumer catches up with its supplier’s expectations in a separate contract in consumer-driven contracts. All these contracts are shared with the provider to provide an insight into
the obligations that each individual customer has to fulfill. To validate these obligations, the provider can create a test suite. To validate such obligations, the provider can develop
a test suite. This allows them to remain agile and to make changes which do not affect consumers, as well as to identify consumers who will suffer the necessary changes for further
planning and debate. Pacto helps to ensure that such commitments are fulfilled and decouples implementation obligations, so that customers and suppliers can be autonomously tested.
For their testing, suppliers can also decouple consumers. Pacto may use contracts for provider testing to visualize consumers.

28. How can we do cross-functional testing?
The verification of non-functional requirements is a cross-functional test. These prerequisites are system features which can not be enacted as normal. For instance.
Cross-functions testing relates to cross-functional requirements. The number of simultaneous system-endorsed users, site usability, etc. Commercial users often do not initially determine
cross-functional requirements. However, they expect these when software is finished and used for development. In the original stage of the project itself, it is always a good question to
ask companies about these cross-functional expectations.

29. Why do we use Correlation IDs in Microservices architecture?
The correlation ID is a single value connected to requests and messages that refer a specific transaction or event chain. A correlation ID is also known as a transit ID. A well publicized
pattern of enterprise integration depends upon the use of Correlation ID. A correlation ID forms part of Java Messaging Service (JMS) as the non-standard HTTP header. But again, it is
ambiguous to attach a correlation ID to a request. There’s not one you have to use. But you’re going to do enough to use a correlation identifier in your messages if you model a distributed
system that utilizes messages delays and asynchronous processing.

30. What is Confused Deputy Problem in security context?
Assume that the customer sends the server name of the input and output file. The server builds and stores the input file in the output. Assume, too, the customer has fewer privileges than
the server. Now also suppose there is a “restricted” file where the server doesn’t have authorization from the client. At that point, the server compiles the file and then write the server
into a “restricted” file that overwrites the preceding content if the user sends an absurd input and a “restricted” as output file. The client had no “restricted” authorization here, but
the server did. The server is thus an administrator who has been used for a manipulative action. Such problems are known as Confused Deputy Problem.

31. What is PACT?
The Pact is a testing tool for contracts. Contract testing provides services (such as an API provider and a customer) with the possibility of communicating with each other. The only way to
determine that services can interact is through the use of costly and fragile integration tests without contract testing.

32. What is Conway’s law?
Conway’s law shows that the design of every system is impacted substantially by the communications system of the organisation. The law is widely related to software development but applies
to systems and organisations.

33. What are Client certificates?
The certification of customer verification is used for a SSL handshake to authenticate customers. It authorize users who access a server through a customer authentication certificate
exchange. This ensures that the customer is who he claims to be. This ensures that the customer is who he claims to be. This deletes unverified entries in the user user profile of a
database when an Internet user authenticates the server.

33. How Do You Override a Spring Boot Project’s Default Properties?
We can do this by defining the  properties in the application.properties file. For instance, you should determine the suffix and prefix for Spring MVC applications. The following properties
can be entered in the application.properties file.

For suffix – spring.mvc.view.suffix: .jsp
For prefix – spring.mvc.view.prefix: /WEB-INF/

34. What is the Role of the Spring Boot Actuator?
This is one of the most essential features that enables you to obtain the present state of a development application. The current status can be monitored using multiple metrics. It also provides parameters for RESTful Web services that can be used simply to check the various statistics.

35. How Is Spring Security Implemented In a Spring Boot Application?
For implementation, minimum setup is necessary. Just add the jump-boot-starter-securitystarter to the pom.xml file. You also have to generate a Spring configuration class to bypass the necessary procedure while broadening the WebSecurityConfigurerAdapter for protection in the application.

36. Embedded Containers That are Supported by Spring Boot
Deployment can be carried out by two methods whenever you create a Java application:

Use an external application container.
Inside your jar file embedding the container.
The jetty, tomcat, and undertow servers are all included in Spring Boot.

Jetty – The Eclipse Jetty can be integrated into frames, application servers, methods and clusters in a broad variety of projects.
Tomcat — The JavaServer Pages free and open source is a very well – functioning JavaServer integration with embedded software.
Undertow – A durable and popular web server that develops a web server using tiny single handlers.

37. What Do You Mean by End-To-End Testing of Microservices?
End – to – end testing checks whether it all actually works as planned. all workflow procedures are validated. Also it guarantees that perhaps the system is working uniformly and thus fulfills the business objectives.

38. What Is OAuth?
OAuth enables easy access customer programs using third party service such as Facebook, GitHub, etc. via HTTP by open authorization protocol, otherwise regarded as OAuth.
Without the need for credentials you can share data among various sites. OAuth enables a third-party such as Facebook to use the account details from the end user while maintaining it
safe (never using or displaying the login information of the user). It behaves much more on behalf of the subscriber as an middleman while giving the server a token to access the data
requested.

39. Why Do We Need Containers for Microservices?
For handling a Microservices based application, containers are the simplest way to do it. The user can be deployed and developed individually. In the image of a container, you may also use
Docker to hide microservices. Microservices can use these features without any additional dependencies or effort.

40. What Are the Ways to Access RESTful Microservices?
One of the commonly asked question in the Microservices interview is how to access RESTful Microservices. There are two ways to do it which are as follows:

Use a REST template that is load balanced.
Use multiple microservices.

41. Common Mistakes Made While Transitioning to Microservices
Often errors happen on the manufacturing process side as well as on development. But any experienced questioner will also have interview questions in the queue for microservices.
Some common issues are:

The developer often fails to detail the difficulties at hand.
The rewriting of existing programs.
Not clearly delineated responsibilities, timeline and limits.
The scope of automation almost from the beginning has not been implemented and identified.
----------------------------------------------------------------------------------------------
1. What Is Spring Cloud?
 Spring Cloud, in microservices, is a system that provides integration with external systems. It is a short-lived framework that builds an application, fast. Being associated with the
 finite amount of data processing, it plays a very important role in microservice architectures.

For typical use cases, Spring Cloud provides the out of the box experiences and a sets of extensive features mentioned below:

Versioned and distributed configuration.
Discovery of service registration.
Service to service calls.
Routing.
Circuit breakers and load balancing.
Cluster state and leadership election.
Global locks and distributed messaging.

2. What Is Spring Boot?
Spring boot is a major topic under the umbrella of microservices interview questions.
With the new functionalities that have been added, Spring keeps getting more complex. Whenever you are starting a new project, it is mandatory to add a new build path or Maven dependencies.
In short, you will need to do everything from scratch. Spring Boot is the solution that will help you to avoid all the code configurations.

3. How Do You Override a Spring Boot Project’s Default Properties?
This can be done by specifying the properties in the application.properties file.
For example, in Spring MVC applications, you have to specify the suffix and prefix. This can be done by entering the properties mentioned below in the application.properties file.

For suffix – spring.mvc.view.suffix: .jsp

For prefix – spring.mvc.view.prefix: /WEB-INF/

4. Role of Actuator in Spring Boot
It is one of the most important features, which helps you to access the current state of an application that is running in a production environment. There are multiple metrics which can
be used to check the current state. They also provide endpoints for RESTful web services which can be simply used to check the different metrics.

5. How Is Spring Security Implemented In a Spring Boot Application?
Minimal configuration is needed for implementation. All you need to do is add thespring-boot-starter-securitystarter in the pom.xml file. You will also need to create a Spring config class
that will override the required method while extending the WebSecurityConfigurerAdapter to achieve security in the application. Here is some example code:

package com.gkatzioura.security.securityendpoints.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
@Override
protected void configure(HttpSecurity http) throws Exception {
http.authorizeRequests()
.antMatchers("/welcome").permitAll()
.anyRequest().authenticated()
.and()
.formLogin()
.permitAll()
.and()
.logout()
.permitAll();
}
}
6. Which Embedded Containers Are Supported by Spring Boot?
Whenever you are creating a Java application, deployment can be done via two methods:

Using an application container that is external.
Embedding the container inside your jar file.
Spring Boot contains Jetty, Tomcat, and Undertow servers, all of which are embedded.

Jetty – Used in a wide number of projects, Eclipse Jetty can be embedded in framework, application servers, tools, and clusters.

Tomcat – Apache Tomcat is an open source JavaServer Pages implementation which works well with embedded systems.

Undertow – A flexible and prominent web server that uses small single handlers to develop a web server.

7. What Do You Mean by End-To-End Testing of Microservices?
End-to-end testing validates all the processes in the workflow to check if everything is working as expected. It also ensures that the system works in a unified manner, thereby satisfying
the business requirement.

8. What Is Semantic Monitoring?
It combines monitoring of the entire application along with automated tests. The primary benefit of Semantic Monitoring is to find out the factors which are more profitable to your business.

Semantic monitoring along with service layer monitoring approaches monitoring of microservices from a business point of view. Once an issue is detected, they allow faster isolation and bug
triaging, thereby reducing the main time required to repair. It triages the service layer and transaction layer to figure out the transactions affected by availability or poor performance.

9. How Can You Set Up Service Discovery?
There are multiple ways to set up service discovery. I’ll choose the one that I think to be most efficient, Eureka by Netflix. It is a hassle free procedure that does not weigh much on the
application. Plus, it supports numerous types of web applications.

Eureka configuration involves two steps – client configuration and server configuration.
Client configuration can be done easily by using the property files. In the clas spath, Eureka searches for a eureka-client.properties file. It also searches for overrides caused by the
environment in property files which are environment specific.

For server configuration, you have to configure the client first. Once that is done, the server fires up a client which is used to find other servers. The Eureka server, by default, uses the Client configuration to find the peer server.

10. Why Would You Opt for Microservices Architecture?
This is a very common microservices interview question which you should be ready for! There are plenty of pros that are offered by a microservices architecture. Here are a few of them:

Microservices can adapt easily to other frameworks or technologies.
Failure of a single process does not affect the entire system.
Provides support to big enterprises as well as small teams.
Can be deployed independently and in relatively less time.
11. Why Would You Need Reports and Dashboards in Microservices?
Reports and dashboards are mainly used to monitor and upkeep microservices. There are multiple tools that help to serve this purpose. Reports and dashboards can be used to:

Find out which microservices expose what resources.
Find out the services which are impacted whenever changes in a component occur.
Provide an easy point which can be accessed whenever documentation is required.
Versions of the components which are deployed.
To obtain a sense of maturity and compliance from the components.
12. Why Do People Hesitate to Use Microservices?
I have seen many devs fumble over this question. After all, they're getting asked this question when interviewing for a microservices architect role, so acknowledging its cons can be a little tricky. Here are some good answers:

They require heavy investment – Microservices demand a great deal of collaboration. Since your teams are working independently, they should be able to synchronize well at all times.
They need heavy architecture set up – The system is distributed, the architecture is heavily involved.
They need excessive planning for handling operations overhead – You need to be ready for operations overhead if you are planning to use a microservices architecture.
They have autonomous staff selection – Skilled professionals are needed who can support microservices that are distributed heterogeneously.
13. How Does PACT Work?
PACT is an open source tool. It helps in testing the interactions between consumers and service providers. However, it is not included in the contract, increasing the reliability of the application. The consumer service developer starts by writing a test which defines a mode of interaction with the service provider. The test includes the provider’s state, the request body, and the response that is expected. Based on this, PACT creates a stub against which the test is executed. The output is stored in a JSON file.

14. Define Domain Driven Design
The main focus is on the core domain logic. Complex designs are detected based on the domain’s model. This involves regular collaboration with domain experts to resolve issues related to the domain and improve the model of the application. While answering this microservices interview question, you will also need to mention the core fundamentals of DDD. They are:

DDD focuses mostly on domain logic and the domain itself.
Complex designs are completely based on the domain’s model.
To improve the design of the model and fix any emerging issues, DDD constantly works in collaboration with domain experts.

15. What Are Coupling and Cohesion?
Coupling can be considered to be the measurement of strength between the dependencies of a component. A good microservices application design always consists of low coupling and high cohesion.

Interviewers will often ask about cohesion. It is also another measurement unit. More like a degree to which the elements inside a module remain bonded together.

It is imperative to keep in mind that an important key to designing microservices is a composition of low coupling along with high cohesion. When loosely coupled, a service knows very little about other services. This keeps the services intact. In high cohesion, it becomes possible to keep all the related logic in a service. Otherwise, the services will try to communicate with each other, impacting the overall performance.

16. What Is OAuth?
Open Authorization Protocol, otherwise known as OAuth, helps to access client applications using third-party protocols like Facebook, GitHub, etc., via HTTP. You can also share resources between different sites without the requirement of credentials.

OAuth allows the account information of the end user to be used by a third-party like Facebook while keeping it secure (without using or exposing the user’s password). It acts more like an intermediary on the user’s behalf while providing a token to the server for accessing the required information.

17. Why Do We Need Containers for Microservices?
To manage a microservice-based application, containers are the easiest alternative. It helps the user to individually deploy and develop. You can also use Docker to encapsulate microservices in the image of a container. Without any additional dependencies or effort, microservices can use these elements.

18. What Are the Ways to Access RESTful Microservices?
Another one of the frequently asked microservices interview questions is how to access RESTful microservices? You can do that via two methods:

Using a REST template that is load balanced.
Using multiple microservices.
19. What Are Some Major Roadblocks for Microservices Testing?
Talking about the cons, here is another one of the microservices interview questions you may be ready for, will be around the challenges faced while testing microservices.

Testers should have a thorough understanding of all the inbound and outbound processes before they start writing the test cases for integration testing.
When independent teams are working on different functionalities, collaboration can prove to be quite a struggling task. It can be tough to find an idle time-window to perform a complete round of regression testing.
With an increasing number of microservices, the complexity of the system also increases.
During the transition from monolithic architecture, testers must ensure that there is no disruption between the internal communication among the components.
20. Common Mistakes Made While Transitioning to Microservices
Not only on development, but mistakes also often occur on the process side. And any experienced interviewer will have this in the queue for microservices interview questions. Some of the common mistakes are:

Often the developer fails to outline the current challenges.
Rewriting the programs that are already existing.
Responsibilities, timeline, and boundaries not clearly defined.
Failing to implement and figure out the scope of automation from the very beginning.
21. What Are the Fundamentals of Microservices Design?
This is probably one of the most frequently asked microservices interview questions. Here is what you need to keep in mind while answering to it:

Define a scope.
Combine loose coupling with high cohesion.
Create a unique service which will act as an identifying source, much like a unique key in a database table.
Creating the correct API and taking special care during integration.
Restrict access to data and limit it to the required level.
Maintain a smooth flow between requests and response.
Automate most processes to reduce time complexity.
Keep the number of tables to a minimum level to reduce space complexity.
Monitor the architecture constantly and fix any flaw when detected.
Data stores should be separated for each microservice.
For each microservice, there should be an isolated build.
Deploy microservices into containers.
Servers should be treated as stateless.
You can also follow this article explaining 9 Fundamentals to a Successful Microservice Design.

22. Where Do We Use WebMVC Test Annotation?
WebMvcTest is used for unit testing Spring MVC applications. As the name suggests, it focuses entirely on Spring MVC components. For example,
@WebMvcTest(value = ToTestController.class, secure = false):
Here, the objective is to only launch ToTestController. Until the unit test has been executed, other mappings and controllers will not be launched.

23. What Do You Mean by Bounded Context?
A central pattern is usually seen in domain driven design. Bounded context is the main focus of the strategic design section of DDD. It is all about dealing with large teams and models. DDD works with large models by disintegrating them into multiple bounded contexts. While it does that, it also explains the relationship between them explicitly.

24. What Are the Different Types of Two-Factor Authentication?
There are three types of credentials required for performing two-factor authentication.

A thing that you know – like password or pin or screen lock pattern.
A physical credential that you have – like OTP or phone or an ATM card, in other words, any kind of credential that you have in an external or third-party device.
Your physical identity – like voice authentication or biometric security, like a fingerprint or eye scanner.
25. What Is a Client Certificate?
This is a type of digital certificate usually used by client systems for making a request that is authenticated by a remote server. It plays an important role in authentication designs that are mutual and provides strong assurance of the identity of a requester. However, you should have a fully configured backend service for authenticating your client certificate.

26. What Is Conway's Law?
Conway’s Law states, “organizations which design systems … are constrained to produce designs which are copies of the communication structures of these organizations.”

The interviewer may ask a counter microservices interview question, like how is Conway's Law related to microservices. Well, some loosely coupled APIs form the architecture of microservices. The structure is well suited to how a small team is implementing components which are autonomous. This architecture makes an organization much more flexible in restructuring its work process.

27. How to Configure Spring Boot Application Logging?
Spring Boot comes with added support for Log4J2, Java Util Logging, and Logback. It is usually pre-configured as console output. They can be configured by only specifying logging.level in the application.properties file.

logging.level.spring.framework=Debug

28. How Would You Perform Security Testing on Microservices?
Before answering this microservices interview question, explain to the interviewer that microservices cannot be tested as a whole. You will need to test the pieces independently. There are three common procedures:

Code scanning – To ensure that any line of code is bug-free and can be replicated.

Flexibility – The security solution should be flexible so that it can be adjusted as per the requirements of the system.

Adaptability – The security protocols should be flexible and updated to cope up with the new threats by hackers or security breaches.

You can also check out this article explaining the influence of Microservices architecture on security.

29. What Is Idempotence and How Is it Used?
Idempotence refers to a scenario where you perform a task repetitively but the end result remains constant or similar.

Idempotence is mostly used as a data source or a remote service in a way that when it receives more than one set of instructions, it processes only one set of instructions.


------------------------------------

1) Explain microservices architecture

Microservice Architecture is an architectural development style which builds an application as a collection of small autonomous services developed for a business domain.

2) Name three commonly used tools for Microservices

Wiremock, 2.) Docker and 3.) Hysrix are important Microservices tool.
3) What is Monolithic Architecture?

Monolithic architecture is like a big container in which all the software components of an application are clubbed inside a single package.

4) What are the advantages of microservices?

Here, are some significant advantages of using Microservices:

Technology diversity, e., Microservices can mix easily with other frameworks, libraries,  and databases
Fault isolation, e., a process failure should not bring the whole system down.
Greater support for smaller and parallel team
Independent deployment
Deployment time reduce
5) What is Spring Cloud?

Spring cloud is an Integration software that integrates with external systems. It allows microservices framework to build applications which perform restricted amounts of data processing.


6) Discuss uses of reports and dashboards in the environment of Microservices

Reports and dashboards help in monitoring and upkeep of Microservices. Tons of Application Monitoring Tools assist in this.

7) What are main differences between Microservices and Monolithic Architecture?


Microservices	Monolithic Architecture
Service Startup is fast	Service startup takes time
Microservices are loosely coupled architecture.	Monolithic architecture is mostly tightly coupled.
Changes done in a single data model does not affect other Microservices.	Any changes in the data model affect the entire database
Microservices  focuses  on products, not projects	Monolithic put emphasize over the whole project
8) What are the challenges faced while using Microservices?

Microservices always rely on each other. Therefore, they need to communicate with each other.
As it is distributed system, it is a heavily involved model.
If you are using Microservice architecture, you need to ready for operations overhead.
You need skilled professionals to support heterogeneously distributed microservices.
9) In which cases microservice architecture best suited?

Microservice architecture is best suited for desktop, web, mobile devices, Smart TVs, Wearable, etc.

10) Tell me the name of some famous companies which are using Microservice architecture

Most large-scale websites like Twitter, Netflix, Amazon, have advanced from a monolithic architecture to a microservices architecture.

11) What are the characteristics of Microservices?

Essential messaging frameworks
Decentralized Governance
Easy Infrastructure automation
Design for failure
Infrastructure automation
12) What is RESTful?


Representational State Transfer (REST)/RESTful web services is an architectural style that helps computer systems to communicate over the internet. These web services make microservices easier to understand and implement.

13) Explain three types of Tests for Microservices?

In Microservice architecture tests are divided into three broad categories:

At the bottom level test, we can perform a general test like performance and unit tests. These kinds of tests are entirely automated.
At the middle level, we can perform exploratory tests like the stress tests and usability tests.
At the top level, we can conduct acceptance tests which are mostly fewer in numbers. It also helps stakeholders to know about different software features.
14) What are Client certificates?

Client certificates is a digital certificate used to make authenticated requests to a remote server. It is termed as a client certificate.

15) Explain the use of PACT in Microservices architecture?

It is an open source tool which allows testing interactions between service providers and consumers. However, it is separated from the contract made. This increases the reliability of the Microservices applications.

16) What is the meaning of OAuth?

OAuth means open authorization protocol. This protocol allows you to access the client applications on HTTP for third-party providers GitHub, Facebook, etc. It helps you to share resources stored on one site with another site without the need for their credentials.

17) What is End to End Microservices Testing?

End-to-end testing validates every process in the workflow is functioning correctly. It also ensures that the system works together as a whole and satisfies all requirements.

18) Why are Container used in Microservices?

Containers are easiest and effective method to manage the microservice based application. It also helps you to develop and deploy individually. Docker also allows you to encapsulate your microservice in a container image along with its dependencies. Microservice can use these elements without additional efforts.

19) What is the meaning of Semantic monitoring in Microservices architecture?

Semantic monitoring combines automated tests with monitoring of the application. It allows you to find out reasons why your business is not getting more profits.

20) What is a CDC?

CDC is Consumer-Driven Contract. It is a pattern for developing Microservices so that external systems can use them.

21) What is the use of Docker?

Docker offers a container environment which can be used to host any application. This software application and the dependencies that support it which are tightly-packaged together.

22) What are Reactive Extensions in Microservices?

Reactive Extensions is also called Rx. It is a design pattern which allows collecting results by calling multiple services and then compile a combined response. Rx is a popular tool in distributed systems which works exactly opposite to legacy flows.

23) Explain the term ‘Continuous Monitoring.’

Continuous monitoring is a method which is used for searching compliance and risk issues associated with a company’s operational and financial environment. It contains human, processes, and working systems which support efficient and actual operations.

24) How independent micro-services communicate with each other?

It depends upon your project needs. However, in most cases, developers use HTTP/REST with JSON or Binary protocol. However, they can use any communication protocol.
=========================================================================================================================================================================================

Microservices Application Database Management:::-
-------------------------------------------------
-> Uses CAP theorem::
---------------------
1. Consistency
2. Availabilty
3. Partition

=====================================================================================================================================================================================


Q1: Define Microservice Architecture  Related To: Software Architecture
Add to PDF Junior 
Q2: List down the advantages of Microservices Architecture  
Add to PDF Junior 
Q3: Why Would You Opt For Microservices Architecture?  
Add to PDF Junior 
Q4: What are main differences between Microservices and Monolithic Architecture?  
 Add to PDF Mid 
Q5: What are the standard patterns of orchestrating microservices?  
 Add to PDF Mid 
Q6: Whether do you find GraphQL the right fit for designing microservice architecture?  Related To: GraphQL
 Add to PDF Mid 
Q7: What are smart endpoints and dumb pipes?  
 Add to PDF Mid 
Q8: What is the difference between a proxy server and a reverse proxy server?  
 Add to PDF Mid 
Q9: What is the difference between Monolithic, SOA and Microservices Architecture?  Related To: Software Architecture, SOA
 Add to PDF Mid 
Q10: What are the challenges you face while working Microservice Architectures?  
 Add to PDF Mid 
Q11: What are the features of Microservices?  
 Add to PDF Mid 
Q12: What Are The Fundamentals Of Microservices Design?  
 Add to PDF Mid 
Q13: How does Microservice Architecture work?  
 Add to PDF Mid 
Q14: How can we perform Cross-Functional testing?  
 Add to PDF Mid 
Q15: What do you understand by Contract Testing?  
 Add to PDF Senior 
Q16: How should the various services share a common DB Schema and code?  
 Add to PDF Senior 
Q17: Can we create State Machines out of Microservices?  
 Add to PDF Senior 
Q18: What are the pros and cons of Microservice Architecture?  
 Add to PDF Senior 
Q19: Explain what is the API Gateway pattern  Related To: API Design
 Add to PDF Senior 
Q20: What do you understand by Distributed Transaction?  
 Add to PDF Senior 
Q21: What is the role of an architect in Microservices architecture?  
 Add to PDF Senior 
Q22: Mention some benefits and drawbacks of an API Gateway  
 Add to PDF Senior 
Q23: What is Materialized View pattern and when will you use it?  
 Add to PDF Senior 
Q24: What is Idempotence?  
 Add to PDF Senior 
Q25: What does it mean that shifting to microservices creates a run-time problem?  
 Add to PDF Expert 
Q26: What is a Consumer-Driven Contract (CDC)?  
 Add to PDF Expert 
Q27: What is the most accepted transaction strategy for microservices?  Related To: Software Architecture
 Add to PDF Expert 
Q28: What is the difference between Cohesion and Coupling?  Related To: Software Architecture
 Add to PDF Expert 
Q29: What are Reactive Extensions in Microservices?  
 Add to PDF Expert 
Q30: Why would one use sagas over 2PC and vice versa?   
 Add to PDF Expert 
Q31: Provide an example of "smart pipes" and "dumb endpoint"  
 Add to PDF Expert 
Q32: Name the main differences between SOA and Microservices?  Related To: SOA
 Add to PDF Expert 
Q33: What Did The Law Stated By Melvin Conway Implied?  
 Add to PDF Expert 
Q34: How would you implement SSO for Microservice Architecture?  
--------------------------------------------------------------
#1.What is Microservice Architecture?
---------------------------------------
The microservice architecture is structured on the business domain and it's a collection of small autonomous services. In a microservice architecture, all the components are self-contained and wrap up around a single business capability.

Microservices, aka microservice architecture, is an architectural style that structures an application as a collection of small autonomous services, modeled around a business domain. In a Microservice Architecture, each service is self-contained and implements a single business capability.
Why do we need to consider the microservice architecture instead of using monolithic architecture? Below mentioned four main concepts that described the importance of microservice architecture over monolithic architecture.

1. Visibility is high - MSA provides better visibility to your services.
2. Improves resilience - Improves the resilience of our service network
3. Production time reduced - Reduce the delivery time from idea to final product.
4. Reduced cost - Reduce the overall cost of designing, implementing, and maintaining IT services.


#2. The principles used to design Microservices are as follows:
--------------------------------------------------------------
Independent & Autonomous Services
Scalability
Decentralization
Resilient Services
Real-Time Load Balancing
Availability
Continuous delivery through DevOps Integration
Seamless API Integration and Continuous Monitoring
Isolation from Failures
Auto -Provisioning
 
or
1. Scalability
2. Flexibility
3. Independent and autonomous
4. Decentralized governance
5. Resiliency
6. Failure isolation.
7. Continuous delivery through the DevOps


#1. Design Patterns of Microservices
------------------------------------
Aggregator
API Gateway
Chained or Chain of Responsibility
Asynchronous Messaging
Database or Shared Data
Event Sourcing
Branch
Command Query Responsibility Segregator
Circuit Breaker
Decomposition

1. Database per Microservice
   2. Event Sourcing
   3. CQRS
   4. Saga
   5. BFF
   6. API Gateway
   7. Strangler
   8. Circuit Breaker
   9. Externalized Configuration
   10. Consumer-Driven Contract Tracing


------------------------------------
1. Database per Microservice Pattern
   Database design is rapidly evolving, and there are numerous hurdles to overcome while developing a microservices-based solution. Database architecture is one of the most important aspects of microservices.

What is the best way to store data and where should it be stored?

There should are two main options for organizing the databases when using the microservice architecture.

Database per service
Shared database


1.1 Database per service.

The concept is straightforward. There is a data store for each microservice (whole schema or a table). Other services are unable to access data repositories that they do not control. A solution like this has a lot of advantages.

Individual data storage, on the other hand, is easy to scale. Furthermore, the microservice encapsulates the domain's data. As a result, understanding the service and its data as a whole is much easier. It's especially crucial for new development team members.

It will take them less time and effort to properly comprehend the area for which they are responsible. The main drawback of this database service is that there is a need for a failure protection mechanism in case the communication fails.

Database per Microservice Pattern Example







1.2 Shared Database
The use of a shared database is an anti-pattern. It is, however, questionable. The issue is that when microservices use a shared database, they lose their key features of scalability, robustness, and independence. As a result, Microservices rarely employ a shared database.

When a common database appears to be the best solution for a microservices project, we should reconsider if microservices are truly necessary. Perhaps the monolith is the better option. Let's have a look at how a shared database works.

Using a shared database with microservices isn't a frequent scenario. A temporary state could be created while moving a monolith to microservices. Transaction management is the fundamental advantage of a shared database versus a per-service database. There's no need to spread transactions out across services.







2. Event Sourcing Pattern
   The event sourcing is responsible for giving a new ordered sequence of events. The application state can be reconstructed using querying the data and in order to do this, we need to reimage every change to the state of the application. Event Sourcing is based on the idea that any change in an entity's state should be captured by the system.

The persistence of a business item is accomplished by storing a series of state-changing events. A new event is added to the sequence of events every time an object's state changes. It's essentially atomic because it's one action. By replaying the occurrences of an entity, its current state can be reconstructed.

An event store is used to keep track of all of your events. The event store serves as a message broker as well as a database of events. It gives services the ability to subscribe to events via an API. The event store sends all interested subscribers information about each event that is saved in the database. In an event-driven microservices architecture, the event store is the foundation.

This pattern can be used in the following scenarios,
It's important to keep the existing data storage.
There should be no changes to the existing data layer codebase.
Transactions are critical to the application's success.
So as from the above discussion, it is clearly indicated that the event sourcing addresses a challenge of implementing an event-driven architecture. Microservices with shared databases can't easily scale. The database will also be a single point of failure. Changes to the database could have an influence on a number of services.


Event Sourcing Pattern Example







3. Command Query Segmentation (CQRS)  Pattern
   In the above, we have discussed what is event sourcing. In this topic, we are going to discuss what is CQRS? We can divide the topic into two parts with commands and queries.

Commands - Change the state of the object or entity.
Queries -  Return the state of the entity and will not change anything.

In traditional data management systems, there are some issues,

1. Risk of data contention
2. Managing performance and security is complex as objects are exposed to both reading and writing applications.

So in order to solve these problems, the CQRS comes to the big picture. The CQRS is responsible for either change the state of the entity or return the result.

benefits of using the CQRS are discussed below.
1. The complexity of the system is reduced as the query models and commands are separated.
2. Can provide multiple views for query purposes.
3. Can optimize the read side of the system separately from the write side.

The write side of the model handles the event's persistence and acting as a source of information to the read side. The system's read model generates materialized views of the data, which are often highly denormalized views.


Command Query Segmentation (CQRS)  Pattern Example




4. SAGA
   SAGA is one of the best solutions to keep consistency with data in distributed architecture without having the ACID principles. SAGA is responsible for committing multiple commentary transactions by giving rollback opportunities.

There are two ways to achieve the saga's

1. Choreography
2. Orchestration.
   In this choreography saga, there is no central orchestration. Each service in the Saga carries out its transaction and publishes events. The other services respond to those occurrences and carry out their tasks. In addition, depending on the scenario, they may or may not publish additional events.

In the Orchestration saga, each service participating in the saga performs their transactions and publish events. The other services respond to those events and complete their tasks.

Advantage of using SAGA
1. Can be used to maintain the data consistency across multiple services without tight coupling.

The disadvantage of using SAGA
1. Complexity of the SAGA design pattern is high from the programmer's point of view and developers are not well accustomed to writing sagas as traditional transactions.





5. Backend For Frontend (BFF)
   This pattern is used to identify how the data is fetched between the server and clients. Ideally, the frontend team will be responsible for managing the BFF.

A single BFF is responsible for handling the single UI and it will help us to keep the frontend simple and see a unified view data through the backend.

Why BFF needs in our microservice application?
The goal of this architecture is to decouple the front-end apps from the backend architecture.
As a scenario, think about you have an application that consists of the mobile app, web app and needs to communicate with the backend services in a microservices architecture.

This can be done successfully but if you want to make a change to one of the frontend services, you need to deploy a new version instead of stick to updating the one service.

So here comes the microservice architecture and this is able to understand what our apps need and how to handle the services.

This is a big improvement in microservice architecture as this allows to isolate the backend of the application from the frontend. One other advantage that we can get from this BFF is that we can reuse the code as this allows all clients to use the code from the backend.

Between the client and other external APIs, services, and so on, BFF functions similarly to a proxy server. If the request must pass through another component, the latency will undoubtedly increase.


Backend For Frontend (BFF) Pattern for Microservices




6. API Gateway
   This microservice architecture pattern is really good for large applications with multiple client apps and it is responsible for giving a single entry point for a certain group of microservices.

API gateway sits between the client apps and the microservices and it serves as a reverse proxy, forwarding client requests to services. Authentication, SSL termination, and caching are some of the other cross-cutting services it can provide.

Why do we consider the API Gateway architecture instead of using direct client-to-microservice communication? We will discuss this with the following examples,

1. Security issues - All microservices must be exposed to the "external world" without a gateway, increasing the attack surface compared to hiding internal microservices that aren't directly accessed by client apps.

2. Cross-cutting concerns - Authorization and SSL must be handled by each publicly published microservice. Those problems might be addressed in a single tier in many cases, reducing the number of internal microservices.

3. Coupling - Client apps are tied to internal microservices without the API Gateway pattern. Client apps must understand how microservices decompose the application's various sections.

Last but not least, the microservices API gateway must be capable of handling partial failures. The failure of a single unresponsive microservice should not result in the failure of the entire request.

A microservices API gateway can deal with partial failures in a variety of ways, including:
Use data from a previous request that has been cached.
For time-sensitive data that is the request's major focus, return an error code.
Provide an empty value
Rely on hardware top 10 value.
API Gateway Microservice Pattern explained
API Gateway




7. Strangler
   The strangler design pattern is a popular design pattern to incrementally transform your monolithic application to microservices by replacing old functionality with a new service.  Once the new component is ready, the old component is strangled and a new one is put to use.

The facade interface, which serves as the primary interface between the legacy system and the other apps and systems that call it, is one of the most important components of the strangler pattern.

External apps and systems will be able to identify the code associated with a certain function, while the underlying historical system code will be obscured by the facade interface. The strangler design addresses this by requiring developers to provide a façade interface that allows them to expose individual services and functions when they break them free from the monolith.

You need to understand the quality and reliability of your system, whether you're working with legacy code, starting the process of "strangling" your old system, or running a newly containerized application. When anything goes wrong, you need to know how the system got there and why it went down that road.


Strangle Microservice Pattern explained
Moving from Monolithic to microservice architecture stages.



8. Circuit Breaker Pattern
   The circuit breaker is the solution for the failure of remote calls or the hang without a response until some timeout limit is reached. You can run out of critical resources if you having many callers with an unresponsive supplier and this will lead to failure across the multiple systems in the applications.

So here comes the circuit breaker pattern which is wrapping up a protected function call in a circuit breaker object which monitors for failure. When the number of failures reaches a specific level, the circuit breaker trips, and all subsequent calls to the circuit breaker result in an error or a different service or default message, rather than the protected call being made at all.

Different states in the circuit break pattern
Closed - When everything works well according to the normal way, the circuit breaker remains in this closed state.

Open -  When the number of failures in the system exceeds the maximum threshold, this will lead to open up the open state. This will give the error for calls without executing the function.

Open -Half - After having run the system several times, the circuit breaker will go on to the half-open state in order to check the underlying problems are still exist.


Here, we will have an example code that is built using the Netflix hystrix.

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@SpringBootApplication
public class StudentApplication {

    @RequestMapping(value = "/student")
    public String studentMethod(){
        return "Calling the studentMethod";
    }

    public static void main(String[] args) {
        SpringApplication.run(StudentApplication.class, args);
    }
}

So the client application code will call the studentMethod() and if the calling API, /student is not given any response back in time, then there is an alternative method calling the fallback. It is mentioned in the below code.

import java.net.URI;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class StudentService {

    private final RestTemplate restTemplate;

    public StudentService(RestTemplate rest) {
        this.restTemplate = rest;
    }

    @HystrixCommand(fallbackMethod = "reliable")
    public String studentMethodCalling() {
        URI uri = URI.create("http://localhost:8000/student");
        return this.restTemplate.getForObject(uri, String.class);
    }

    public String reliable() {
        return "This is calling if the studentMethod is falling to respond on time";
    }

}

So you can use the circuit breaker pattern to improve the fault tolerance and resilience of the microservice architecture and also prevent the cascading of failure to other microservices.




9. Externalized Configuration
   Often services need to be run in different environments. Environment-specific configuration is required, such as secret keys, database credentials, and so on. Changing the service for each environment has a number of drawbacks. So how we can enable a service to run in multiple environments without modification?

Here comes the Externalized configuration pattern as this enables the externalization of all application configurations including the database credentials and network location.

For example, the Spring Boot framework enables externalized configuration, which allows you to read configuration from many sources and potentially change previously specified configuration settings based on the reading order. FastAPI, thankfully, has built-in support for externalized configuration.


How to externalize Configuration in Microservice



Open up the ConfigServerApplication class and activate the discovery client and the configuration server by using the following annotation.
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class ConfigServerApplication {

}

Remove the application.properties file and create a new application.yml file with the following content.

server.port: 8001

spring:
application.name: config-server
cloud.config.server.git.uri:
https://github.com/alejandro-du/vaadin-microservices-demo-config.git

eureka:
client:
serviceUrl:
defaultZone: http://localhost:7001/eureka/
registryFetchIntervalSeconds: 1
instance:
leaseRenewalIntervalInSeconds: 1



This sets the port (8001) and name (config-server) of the application, as well as the URI
Spring Cloud Config should use to read the configuration from. On GitHub, we have a Git
repository. The configuration files for all of the example application's microservices can
be found in this repository. The admin-applicationmicroservic uses the admin-application.yml
file, for example.











10. Consumer-Driven Contract Tracing
    When a team is constructing multiple related services at the same time as part of a modernization effort, and your team knows the “domain language” of the bounded context but not the individual properties of each aggregate and event payload, the consumer driven contracts approach may be effective.
    Consumer-Driven Contract Tracing Pattern in Microservice


This microservice pattern is useful in legacy application which contains a large data model and existing service surface area. This design patterns will address the following issues,
1. How can you add to an API without breaking downstream clients.
2. How to find out who is using their service.
3. How to make short release cycles with the continuous delivery.

In an event driven architecture, many microservices expose two kinds of API's,
1. RESTful API over HTTP
2. HTTP and a message-based API The RESTful API allows for synchronous integration with these services as well as extensive querying capabilities for services that have received events from a service.
   In summary, a consumer-driven approach is sometimes used when breaking down a monolithic legacy
   application.


