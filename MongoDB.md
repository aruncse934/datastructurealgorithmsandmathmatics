#MongoDB:::--
-----------------------------------------------------------------------------------------------------------------------------------------------------
MongoDB is a cross-platform, document oriented database that provides, high performance, high availability, and easy scalability. MongoDB works on concept of collection and document.

Database
Database is a physical container for collections. Each database gets its own set of files on the file system. A single MongoDB server typically has multiple databases.

Collection
Collection is a group of MongoDB documents. It is the equivalent of an RDBMS table. A collection exists within a single database. Collections do not enforce a schema.
Documents within a collection can have different fields. Typically, all documents in a collection are of similar or related purpose.

Document
A document is a set of key-value pairs. Documents have dynamic schema. Dynamic schema means that documents in the same collection do not need to have the same set of fields or structure,
and common fields in a collection's documents may hold different types of data.

-> Uses CAP theorem::
---------------------
1. Consistency
2. Availabilty
3. Partition


The following table shows the relationship of RDBMS terminology with MongoDB.

RDBMS	                MongoDB
-------------------
Database	            Database
Table	                Collection
Tuple/Row	            Document
column	                Field
Table Join	            Embedded Documents
Primary Key	            Primary Key (Default key _id provided by mongodb itself)

Database Server and Client

Mysqld/Oracle	mongod
mysql/sqlplus	mongo


------------------------
Advantages of MongoDB over RDBMS
Schema less − MongoDB is a document database in which one collection holds different documents. Number of fields, content and size of the document can differ from one document to another.

Structure of a single object is clear.

No complex joins.

Deep query-ability. MongoDB supports dynamic queries on documents using a document-based query language that's nearly as powerful as SQL.

Tuning.

Ease of scale-out − MongoDB is easy to scale.

Conversion/mapping of application objects to database objects not needed.

Uses internal memory for storing the (windowed) working set, enabling faster access of data.

Why Use MongoDB?
Document Oriented Storage − Data is stored in the form of JSON style documents.

Index on any attribute

Replication and high availability

Auto-sharding

Rich queries

Fast in-place updates

Professional support by MongoDB

Where to Use MongoDB?
Big Data
Content Management and Delivery
Mobile and Social Infrastructure
User Data Management
Data Hub
--------------------

MongoDB - Datatypes::
----------------------
MongoDB supports many datatypes. Some of them are −

String − This is the most commonly used datatype to store the data. String in MongoDB must be UTF-8 valid.

Integer − This type is used to store a numerical value. Integer can be 32 bit or 64 bit depending upon your server.

Boolean − This type is used to store a boolean (true/ false) value.

Double − This type is used to store floating point values.

Min/ Max keys − This type is used to compare a value against the lowest and highest BSON elements.

Arrays − This type is used to store arrays or list or multiple values into one key.

Timestamp − ctimestamp. This can be handy for recording when a document has been modified or added.

Object − This datatype is used for embedded documents.

Null − This type is used to store a Null value.

Symbol − This datatype is used identically to a string; however, it's generally reserved for languages that use a specific symbol type.

Date − This datatype is used to store the current date or time in UNIX time format. You can specify your own date time by creating object of Date and passing day, month, year into it.

Object ID − This datatype is used to store the document’s ID.

Binary data − This datatype is used to store binary data.

Code − This datatype is used to store JavaScript code into the document.

Regular expression − This datatype is used to store regular expression.


------------------------------------------------------------------------------------------------------------------------------------
Types of Indexes in MongoDB
---------------------------
Index Type	Description

Single field index -> Used to create an index on a single field and it can be a user defined as well apart from the default _id one.

Compound index ->	MongoDB supports the user-defined indexes on multiple fields.

Multi key index -> 	MongoDB uses multi key indexes basically to store the arrays. MongoDB creates a separate index for each element in an array.
 MongoDB intelligently identifies to create a multi key index if the index contains elements from an array.

Geospatial index  -> Used to support the queries required for the geospatial coordinate data.

Text index  -> This index is used to search for a string content in a collection

Hashed index ->	Used for hash based Sharding

------------------------------------------------------------------------------------------------------------------------------------
Indexing in MongoDB::
---------------------
Default Index::
---------------
Mongodb provides a default index named _id which acts as a primary key to access any document in a collection. This _id index basically avoids the insertion of 2 documents with the
 same value for the _id field.

Creating an Index using createIndex()

To create an index in MongoDB, run the following command :

|-------------------------------------------------|
|db.collection_name.createIndex({field : value }) |
|-------------------------------------------------|
To create an index on the field regNo for a student collection, run the command
ex-  db.student.createIndex({regNo : 1})

Sorting in MongoDB::
--------------------
Sorting the data in any database is one of the vital operations in any database management system. MongoDB provides sort() function in order to sort the data in a collection.
Sort function in MongoDB accepts a list of values and an integer value 1 or -1 which states whether the collection to be sorted in ascending (1) or descending (-1) order.

Syntax for sort function:
|-----------------------------------------|
|db.collection_name.find().sort({KEY : 1})|
|-----------------------------------------|
ex:-
      db.student.find().sort({name : 1})

      db.student.find().sort({name : -1})

Aggregation in MongoDB::
------------------------
Aggregation in MongoDB is nothing but an operation used to process the data that returns the computed results.
Aggregation basically groups the data from multiple documents and operates in many ways on those grouped data in order to return one combined result. In sql count(*) and with group by
is an equivalent of MongoDB aggregation.

Aggregate function groups the records in a collection, and can be used to provide total number(sum), average, minimum, maximum etc out of the group selected.

In order to perform the aggregate function in MongoDB, aggregate () is the function to be used. Following is the syntax for aggregation :

|-------------------------------------------------|
|db.collection_name.aggregate(aggregate_operation)|
|-------------------------------------------------|

ex:-

    db.books.aggregate([{$group : {_id: "$type", category: {$sum : 1}}}])

Data Backup and Restoration in MongoDB::
----------------------------------------
Data Backup::
-------------
 ex:- mongodump

 Data Restore::
 --------------
 ex:- mongorestore

Relationships in MongoDB::
--------------------------
Relationships in MongoDB are used to specify how one or more documents are related to each other. In MongoDB, the relationships can be modelled either by Embedded way or by using the
Reference approach. These relationships can be of the following forms :

One to One
One to Many
Many to Many


Creating a Capped Collection:::
--------------------------------
We can create a capped collection using the following command.

ex:- db.createCollection("student", { capped : true, size : 5242880, max : 5000 } )

Drop a Collection::
-------------------
Any collection in a database in MongoDB can be dropped easily using the following command :

db.collection_name.drop()
drop() method will return true is the collection is dropped successfully, else it will return false.

====================================================================================================================================