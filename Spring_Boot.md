=============================================================================================================================================
#Spring Boot::
=============================================================================================================================================
----------------------------------------------------------------
1.What is Spring Boot? Why should you use it?
-----------------------------------------------------------------
     ->Spring Boot Framework is Auto-Dependency Resolution, Auto-Configuration, Management EndPoints, Embedded HTTP Servers(Jetty/Tomcat etc.) and Spring Boot CLI.
     ->Spring Boot = Auto-Dependency Resolution + Auto-Configuration + Management EndPoints + Embedded HTTP Servers(Jetty/Tomcat).
     ->Spring Boot Framework is Spring Boot Starter, Spring Boot Auto-Configurator, Spring Boot Actuator, Embedded HTTP Servers, and Groovy.
    Use:- 
       ->Spring boot makes it easier for you to create Spring application, it can save a lot of time and efforts.
       ->Spring Boot not only provides a lot of convenience by auto-configuration a lot of things for you but also improves the productivity because it lets you focus only on writing your
     business logic.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2.What are some important features of using Spring Boot?(Components)
--------------------------------------------------------------------------------------------------
     a.)Spring Boot Starter::: -> Spring Boot Starters are just JAR Files. They are used by Spring Boot Framework to provide “Auto-Dependency Resolution”.
     b.)Spring Boot AutoConfigurator::: -> Spring Boot AutoConfigurator is used by Spring Boot Framework to provide “Auto-Configuration”.
     c.)Spring Boot Actuator::: -> Spring Boot Actuator is used by Spring Boot Framework to provide “Management EndPoints” to see Application Internals, Metrics etc.
      ->  Spring Boot provides actuator to monitor and manage our application. Actuator is a tool which has HTTP endpoints. when application is pushed to production, you can choose to manage and monitor your application using HTTP endpoints.
     d.)Spring Boot CLI::: ->  Spring Boot CLI is Auto Dependency Resolution, Auto-Configuration, Management EndPoints, Embedded HTTP Servers(Jetty, Tomcat etc.) and (Groovy, Auto-Imports).
     -> Spring Boot CLI is Spring Boot Starter, Spring Boot Auto-Configurator, Spring Boot Actuator, Embedded HTTP Servers, and Groovy.
     -> With Spring Boot CLI::: No Semicolons, No Public and private access modifiers, No Imports(Most), No “return” statement, No setters and getters, No Application class with main() method(It takes care by SpringApplication class)., No Gradle/Maven builds. , No separate HTTP Servers.
     e.)Spring Boot Initilizr::: ->  Spring Boot Initilizr is a Spring Boot tool to bootstrap Spring Boot or Spring Applications very easily.
     
    Spring Boot Initilizr comes in the following forms:
    
    Spring Boot Initilizr With Web Interface
    Spring Boot Initilizr With IDEs/IDE Plugins
    Spring Boot Initilizr With Spring Boot CLI
    Spring Boot Initilizr With ThirdParty Tools
---------------------------------------------------------------------------------------------------------------------------------------
3.What is auto-configuration in Spring boot? how does it help? Why Spring Boot is called opinionated?
---------------------------------------------------------------------------------------------------------------------------------------------
    ->Spring Boot is its ability to automatically configure our application based the jar dependencies we are adding to our classpath. we will be covering Spring Boot Auto Configuration features with an understanding of how this can help in the development lifecycle.
    ->the point is auto-configuration does a lot of work for you with respect to configuring beans, controllers, view resolvers etc, hence it helps a lot in creating a Java application.
    ->Spring Boot also provides ways to override auto-configuration settings.
    It's also disabled by default and you need to use either @SpringBootApplication or @EnableAutoConfiguration annotations on the Main class to enable the auto-configuration feature.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5.What is the difference between @SpringBootApplication and @EnableAutoConfiguration annotation?
---------------------------------------------------------------------------------------------------------------------------------------------------
    -> Even though both are essential Spring Boot application and used in the Main class or Bootstrap class there is a subtle difference between them.
    The @EnableAutoConfiguration is used to enable auto-configuration but @SpringBootApplication does a lot more than that.
    -> It also combines @Configuration and @ComponentScan annotations to enable Java-based configuration and component scanning in your project.
    ->The @SpringBootApplication is in fact combination of @Configuration, @ComponentScan and @EnableAutoConfiguration annotations.
    -> Many Spring Boot developers like their apps to use auto-configuration, component scan and be able to define extra configuration on their "application class".
     A single @SpringBootApplication annotation can be used to enable those three features, that is:
    
    @EnableAutoConfiguration: enable Spring Boot’s auto-configuration mechanism
    @ComponentScan: enable @Component scan on the package where the application is located
    @Configuration: allow to register extra beans in the context or import additional configuration classes
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
6.What is Spring Initializer? why should you use it?
----------------------------------------------------------------------
    One of the difficult things to start with a framework is initial setup, particularly if you are starting from scratch and you don't have a reference setup or project. Spring Initializer addresses this problem in Spring Boot.
    
    It's nothing but a web application which helps you to create initial Spring boot project structure and provides Maven or Gradle build file to build your code.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7.What is actuator in Spring boot?
------------------------------------------------------------------------
    Spring boot actuator is one of the most important features of Spring boot. It is used to access current state of running application in production environment. There are various metrics which you can use to check current state of the application.
    
    Spring boot actuator provides restful web services end points which you can simply use and check various metrics.
    For example:
    /metrics : This restful end point will show you metrics such as free memory, processors, uptime and many more properties,
    
    Spring boot actuator will help you to monitor your application in production environment.Restful end points can be sensitive, it means it will have restricted access and will be shown only to authenticated users. You can change this property by overriding it in application.properties.
    
    Since Spring Boot is all about auto-configuration it makes debugging difficult and at some point in time, you want to know which beans are created in Spring's Application Context and how Controllers are mapped. Spring Actuator provides all that information.
    
    It provides several endpoints e.g. a REST endpoint to retrieve this kind of information over the web. It also provides a lot of insight and metrics about application health e.g. CPU and memory usage, number of threads etc.
------------------------------------------------------------------------------------------------------------------------------------------------------------------
8.What is Spring Boot CLI? What are its benefits?
-----------------------------------------------------
    Spring Boot CLI is a command line interface which allows you to create Spring-based Java application using Groovy. Since it's used Groovy, it allows you to create Spring Boot application from the command line without ceremony e.g. you don't need to define getter and setter method, or access modifiers, return statements etc.
    It's also very powerful and can auto-include a lot of library in Groovy's default package if you happen to use it.
    For example, if you use JdbcTempalte, it can automatically load that for you.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
9.Where do you define properties in Spring Boot application?
---------------------------------------------------------------------------------------------------
    You can define both application and Spring boot related properties into a file called application.properties. You can create this file manually or you can use Spring Initializer to create this file, albeit empty.
    
    You don't need to do any special configuration to instruct Spring Boot load this file. If it exists in classpath then Spring Boot automatically loads it and configure itself and application code according.
    For example, you can use to define a property to change the embedded server port in Spring Boot,
-----------------------------------------------------------------------------------------------------------------------------------------------
10.Can you change the port of Embedded Tomcat server in Spring boot? If Yes, How?
-----------------------------------------------------------------------------------------------------------------------------
    Yes, we can change the port of Embedded Tomcat Server in Spring Boot by adding a property called server.port in the application.properties file.
    this property file is automatically loaded by Spring Boot and can be used to configure both Spring Boot as well as application code.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11.What is the difference between an embedded container and a WAR?
---------------------------------------------------------------------
    The main difference between an embedded container and a WAR file is that you can Spring Boot application as a JAR from the command prompt without setting up a web server. But to run a WAR file, you need to first set up a web server like Tomcat which has Servlet container and then you need to deploy WAR there.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12.What embedded containers does Spring Boot support?
----------------------------------------------------------------------------------
    Spring Boot support three embedded containers: Tomcat, Jetty, and Undertow. By default, it uses Tomcat as embedded containers but you can change it to Jetty or Undertow.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13.What are some common Spring Boot annotations?
---------------------------------------------------------------------------------------
    Some of the most common Spring Boot annotations are @EnableAutoConfiguration, @SpringBootApplication, @SpringBootConfiguration, and @SpringBootTest.
    
    The @EnableAutoConfiguration is used to enable auto-configuration on Spring Boot application, while @SpringBootApplication is used on the Main class to allow it to run a JAR file.
     @SpringBootTest is used to run unit test on Spring Boot environment.
------------------------------------------------------------------------------------------------------------------------------------------------------
14.Can you name some common Spring Boot Starter POMs?
-------------------------------------------------------------------------------------------
    Some of the most common Spring Boot Start dependencies or POMs are spring-boot-starter, spring-boot-starter-web, spring-boot-starter-test.
    You can use spring-boot-starter-web to enable Spring MVC in Spring Boot application.
------------------------------------------------------------------------------------------------------------------------------
15.Can you control logging with Spring Boot? How?
----------------------------------------------------------------------------
    Yes, we can control logging with Spring Boot by specifying log levels on application.properties file. Spring Boot loads this file when it exists in the classpath and it can be used to
    configure both Spring Boot and application code.
    
    Spring Boot uses Commons Logging for all internal logging and you can change log levels by adding following lines in the application.properties file:
    logging.level.org.springframework=DEBUG
    logging.level.com.demo=INFO
------------------------------------------------------------------------------------------------------------------------------------------------------
16.What is DevTools in Spring boot?
------------------------------------------------------
    Spring boot comes with DevTools which is introduced to increase the productivity of developer. You don’t need to redeploy your application every time you make the changes.
    Developer can simply reload the changes without restart of the server. It avoids pain of redeploying application every time when you make any change.
    This module will be disabled in production environment.
-----------------------------------------------------------------------------------------------------------
17.How can you implement Spring security in Spring boot application?
-------------------------------------------------------------------------------------------------
    Implementation of Spring security in Spring boot application requires very little configuration. You need to add spring-boot-starter-security starter in pom.xml.You need to create
    Spring config class which will extend WebSecurityConfigurerAdapter and override required method to achieve security in Spring boot application
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18.How can you configure logging in Spring boot application?
---------------------------------------------------------------------------------------
    Spring Boot comes with support for Java Util Logging, Log4J2 and Logback and it will be pre-configured as Console output.
    Hence,You can simply specify logging.level in application.properties.
    logging.level.spring.framework=Debug
----------------------------------------------------------------------------------------------------------------------------------
19.Advantages of Spring Boot:
--------------------------------------------------------
    It is very easy to develop Spring Based applications with Java or Groovy.
    It reduces lots of development time and increases productivity.
    It avoids writing lots of boilerplate Code, Annotations and XML Configuration.
    It is very easy to integrate Spring Boot Application with its Spring Ecosystem like Spring JDBC, Spring ORM, Spring Data, Spring Security etc.
    It follows “Opinionated Defaults Configuration” Approach to reduce Developer effort
    It provides Embedded HTTP servers like Tomcat, Jetty etc. to develop and test our web applications very easily.
    It provides CLI (Command Line Interface) tool to develop and test Spring Boot(Java or Groovy) Applications from command prompt very easily and quickly.
    It provides lots of plugins to develop and test Spring Boot Applications very easily using Build Tools like Maven and Gradle
    It provides lots of plugins to work with embedded and in-memory Databases very easily.

20.What are disadvantages of Spring boot?
----------------------------------------------
    If you want to convert your old spring application to Spring boot application, it may not be straight forward and can be time consuming.
----------------------------------------------------------------------------------------------------------------------------------
