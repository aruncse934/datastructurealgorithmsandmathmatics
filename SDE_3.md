                                                    FAANG Interview ::-
                                                    ------------------
Interview Process
5 "onsite" virtual Hangouts interviews - 3 x ds/ago, 1 system design and 1 Googleyness and Leadership                                                     
      
#Online coding round (Hackerrank)::
----------------------------------------------------------------
1. Game of Book Cricket.
2. Simple string encoding-decoding problem.

                                                    
#Round 1::-DSA
--------------------------------------------------------------
(Coderpad + voice call): Two easy-medium level questions were discussed and you need to write the complete runnable call and pass all the test cases.

 1. Given a String and two words (which occur in the given string), find the minimum distance between two words. Distance between two words is defined as the number of characters between the given two words’ middle characters. The brute-force approach was already implemented but it had some logical bugs, and because of which sample test cases were failing. The objective was to find and fix those bugs and then to add some new test cases and write a code for those test cases as well.
 2. Minimum path sum
 3. Given two sorted arrays, find the median of the combined array. (I told the two pointer approach and they asked to optimize. I made the two pointer code and it was running fine along with all the boundary cases)
 4. Find cycle in the array. (Ex: 2 1 2 0 , return cycle length in this case it will be 2 as 2 1 2 make a cycle)


Note: Round 2 – 6 (Each round took around 60-70 minutes, all rounds were on the same day, Zoom Video Call + Coderpad):

#Round 2(DSA)::-
--------------------------------------------------------------
 1. Quick introduction
 2. Find the difference between two arrays: Two unsorted arrays are given, and you need to find (arr1 – arr2) and (arr2 – arr1). The difference between the two arrays is defined as all the elements from the first array which are not present in the second array, taking the number of occurrences into consideration.
        Example:
        arr1: [3, 5, 2, 7, 4, 2, 7] arr2: [1, 7, 5, 2, 2, 9]
        arr1 – arr2 = [3, 7, 4]
        arr2 – arr1 = [1, 7]
 3. Given an array of citations, calculate the researcher’s h-index.
 4. The Follow-up question was: h-index II
 5. Next follow-up question: What if we are getting a continuous stream of citations, and we need to calculate the h-index after each input?
 6. Sum of each level in a binary tree
 7. Given an array with duplicate elements, return teh array without any three consecutive array elements repeated. 

#Round 3 (DSA, Projects mentioned in the resume):
--------------------------------------------------------------

 1. A detailed discussion about the projects I have worked on and technologies and design patterns I have used.
 2. Given an imbalanced BST, return the balanced BST.
 3. Maximum Number of Events That Can Be Attended
 4. Puzzle: Given a 4-digit number ABCD, ABCD * 4 = DCBA (reversed number), find the values of A and D.
 5. Find the maximum value in a window ok k
 6. Find maximum sum path in a binary tree
 7. N queen problem (Only logic was asked)
 8. Max sum path in a binary tree
 9. Print three increasing elements in an array such that a[i] < a[j] < a[k] and i<j<k
10. Given an array and value of k , use the k values to make the array values equal such that the equal values are max.(Ex: 1 2 3 6 8 9 10 11 and k =6 , then we can increment 10 by 1, 9 by 2 , 8 by 3 and make each of these equal to 11 also now we have exhausted the k value so max 4 number of 11's we can get)
11. Given a long doc and a dictionary of words we need to tell the shortest window such that all the words of dictionary are atleast once contained in the window.
12. You entered a restaurant and given are the arrival time and the waiting time, find the avg. waiting time of all the customers?
[[0, 4], [0, 3], [4, 3], [20, 3]]
13. https://leetcode.com/problems/average-waiting-time/


#Round 4 (Java, Design):
--------------------------------------------------------------

 1. Introduction and technical discussion about my recent project
 2. OOPS questions
 3. HashMap internal working
 4. JVM architecture.
 5. How is Java different than other object-oriented programming languages?
 6. Detailed discussion on Garbage collector
 7. You need to design a relational database; how will you design it? Which data structures will you use?
 8. Puzzle: Find the top 3 horses puzzle.
 9. Project discussion
 10. Apache zookeeper internal working, why do we use zookeeper (as it was mentioned in my CV)?
 11. Rate limiter API problem design (LLD)
 12. ActiveMQ communication types, its uses, why do we need async communication (as it was mentioned in my CV)
 13. Expiry map discussion, how we can clean up the map, thead vs process, threapools, synchronization questions.
 14. What all design patterns i have used?
 15.  What is Sigleton pattern, how to create a singleton class?
 16. Suppose we need to create a pool of singleton objects, how will you manage it?
 17. Difference between @Service, @Repository, @Conponent annotations in spring.
 18.  When we use @Qualifier annotation?
 19. Lets say you have 10 spring boot instances, and you need to build your own monitoring health checkup, how would you do that?
20. How to create an Immutable class in java?
21. Why we use Spring, discussion on dependency injection
22. How do we make connection to mongoDB in spring application
23. @Repository annotation uses

24. What is singleton class? Write the code?
25. How will you make singleton class thread safe?
26. Synchronous vs volatile?
27. What are reflections in java? is it compile time or runtime?
28. How to make singleton class secure from reflections?

29. What are dependency injections in spring? What is the role of Autowired?
30. What is the difference between singleton class created by Java vs Spring ?


#Round 5 (Hiring Manager):
--------------------------------------------------------------

1. Quick introduction
2. If you are to design a garbage collector, how will you design it?
3. What is wrapper class and why do we need it?
4. What is type erasure and why do we need it?
5. Why do you want to leave the current organization?
6. Why GS?
7. He explained my role in the team.
8. Project overview
9. Challenges faced in the project
10. Given an array, create valid alphabet patterns using the same
[2,4,5,6,1,3]
24,5,6,1,3 -- XEFAC
2,4,5,6,1,3 -- BDEFAC
24,5,6,13 -- XEFM
2,4,5,6,13 -- BDEFM
11. Why do you want to change?

12. What is your current role?
13. Why change?
14. Why banking domain?
15. Any prior experience in finance domain?
16. A banking scenario discussed.
17. Expectations from the role
18. Future aspirations



