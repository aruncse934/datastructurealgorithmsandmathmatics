## Hbase , Scala, Spark, 
##Python/Java/Scala, Spark, Hadoop, Kafka, NoSQL, Hive, Cassandra, Hbase, Cloud etc.



#zookeeper start server command::-

zkServer start







/*
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

 

  //  public static List<String> getShrunkArray(List<String> inputArray, int burstLength) {
        int i = 0;
        while(i < inputArray.size()){
            if( i < 0) i = 0;

            int count = 1;
            int j = i+1;
            while(j < inputArray.size() && inputArray.get(j).equals(inputArray.get(i))){
                count++;
                j++;
            }
            if(i < inputArray.size() && count >= burstLength){
                while(count-- > 0){
                    inputArray.remove(i);
                }
                i -=3;
            }
            i++;
        }

        return inputArray;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int inputArrayCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> inputArray = IntStream.range(0, inputArrayCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
                .collect(toList());

        int burstLength = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> result = Result.getShrunkArray(inputArray, burstLength);

        bufferedWriter.write(
                result.stream()
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
*/

/*import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;


class Result {

    /*
     * Complete the 'findDamagedToy' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER N
     *  2. INTEGER T
     *  3. INTEGER D
     */

public static int findDamagedToy(int N, int T, int D) {

        int res = (D + T - 1 ) % N;
        return res == 0 ? T : res;
        }

        }
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int N = Integer.parseInt(bufferedReader.readLine().trim());

        int T = Integer.parseInt(bufferedReader.readLine().trim());

        int D = Integer.parseInt(bufferedReader.readLine().trim());

        int result = Result.findDamagedToy(N, T, D);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
*/