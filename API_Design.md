Q1: What REST stands for?  Related To: REST & RESTful
Add to PDF Entry 
Q2: What are the core components of a HTTP Request?  
Add to PDF Junior 
Q3: Define what is SOA  Related To: SOA
Add to PDF Junior 
Q4: What are advantages of REST web services?  Related To: REST & RESTful
Add to PDF Junior 
Q5: Mention some key characteristics of REST?  Related To: REST & RESTful
Add to PDF Junior 
Q6: Mention whether you can use GET request instead of PUT to create a resource?  
Add to PDF Junior 
Q7: What is cached response?  
Add to PDF Junior 
Q8: What are different types of Web Services?  
Add to PDF Junior 
Q9: Mention what are resources in a REST architecture?  Related To: REST & RESTful
Add to PDF Junior 
Q10: Explain the architectural style for creating web API?  
Add to PDF Junior 
Q11: What are the advantages of Web Services?  
Add to PDF Junior 
Q12: What is SOAP?  Related To: SOA
Add to PDF Junior 
Q13: What's the difference between REST & RESTful?  Related To: REST & RESTful
 Add to PDF Mid 
Q14: WebSockets vs Rest API for real time data? Which to choose?  Related To: Software Architecture, WebSockets, REST & RESTful
 Add to PDF Mid 
Q15: Mention what is the difference between PUT and POST?  
 Add to PDF Mid 
Q16: What are the best practices to create a standard URI for a web service?  
 Add to PDF Mid 
Q17: What is the use of Accept and Content-Type Headers in HTTP Request?  
 Add to PDF Mid 
Q18: What is statelessness in RESTful Webservices?  Related To: REST & RESTful
 Add to PDF Mid 
Q19:  What is Payload?  
 Add to PDF Mid 
Q20: Mention what is the difference between RPC or document style web services? How you determine to which one to choose?  
 Add to PDF Mid 
Q21: What are the best practices for caching?  
 Add to PDF Mid 
Q22: What are different ways to test web services?  
 Add to PDF Mid 
Q23: What are the best practices to design a resource representation?  
 Add to PDF Mid 
Q24: What is the purpose of HTTP Status Code?  
 Add to PDF Mid 
Q25: What are disadvantages of SOAP Web Services?  Related To: SOA
 Add to PDF Mid 
Q26: What is UDDI?  Related To: SOA
 Add to PDF Mid 
Q27: What are the disadvantages of statelessness in RESTful Webservices?  Related To: REST & RESTful
 Add to PDF Mid 
Q28: Mention what are the different application integration styles?  
 Add to PDF Mid 
Q29: How would you choose between SOAP and REST web services?  Related To: SOA
 Add to PDF Mid 
Q30: What are the primary security issues of web service?  
 Add to PDF Mid 
Q31: What are the core components of a HTTP response?  
 Add to PDF Mid 
Q32: What are disadvantages of REST web services?  Related To: REST & RESTful
 Add to PDF Mid 
Q33: Explain what is the API Gateway pattern  Related To: Microservices
 Add to PDF Senior 
Q34: Which header of HTTP response provides control over caching?  
 Add to PDF Senior 
Q35: What do you mean by idempotent operation?  
 Add to PDF Senior 
Q36: Explain element?  
 Add to PDF Senior 
Q37: What are the advantages of statelessness in RESTful Webservices?  Related To: REST & RESTful
 Add to PDF Senior 
Q38: What is difference between SOA and Web Services?  Related To: SOA
 Add to PDF Senior 
Q39: Explain Cache-control header  
 Add to PDF Senior 
Q40: Which type of Webservices methods are to be idempotent?  
 Add to PDF Senior 
Q41: What is difference between OData and REST web services?  Related To: REST & RESTful
 Add to PDF Expert 
Q42: Explain the difference between WCF, Web API, WCF REST and Web Service?  Related To: ASP.NET Web API, REST & RESTful
 Add to PDF Expert 
Q43: What are the best practices to be followed while designing a secure RESTful web service?  Related To: REST & RESTful
 Add to PDF Expert 
Q44:  Enlist some important constraints for RESTful web services  Related To: REST & RESTful
 Add to PDF Expert 
Q45: What is Open API Initiative?  
 Add to PDF Expert 
Q46: Name some best practices for better RESTful API design  Related To: REST & RESTful