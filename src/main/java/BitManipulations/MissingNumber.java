package BitManipulations;

import java.util.HashSet;
import java.util.Set;

public class MissingNumber {
    public static void main(String[] args) {
        int arr[] = {3,0,8,9,2,7,4,5,1};
        System.out.println(missingNumber(arr));
    }

    // Bit Manipulations
    public static int missingNumber(int arr[]){
        int missing = arr.length;
        for (int i = 0; i < arr.length; i++) {
            missing ^= i ^ arr[i]; //
            //missing = missing ^ i ^ arr[i];
        }
        return missing;
    }

    // Hash Set
    /*public static int missingNumber(int nums[]){
        Set<Integer> set = new HashSet<Integer>();
        for (int num : nums)
            set.add(num);

        int expectedNumCount = nums.length + 1;
        for (int number = 0; number < expectedNumCount; number++) {
            if (!set.contains(number)) {
                return number;
            }
        }
        return -1;
    }*/

    // Gauss' Formula
   /* public static int missingNumber(int[] nums) {
        int expectedSum = nums.length*(nums.length + 1)/2;
        int actualSum = 0;
        for (int num : nums) actualSum += num;
        return expectedSum - actualSum;
    }*/

}
