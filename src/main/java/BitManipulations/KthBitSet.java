package BitManipulations;

public class KthBitSet {
    public static void main(String[] args) {
       int n = 4;
       int k =1;
        System.out.println(kthBitSet(n,k));
    }
    /*public  static boolean kthBitSet(int n, int k){
        if((n & (1 << (k-1))) >= 1)//leftshift using
            return true;
        else
            return false;
    }*/

    public static boolean kthBitSet(int n,int k){
        if(((n >>(k-1))&1) >=1) // right shift
            return true;
        return false;
    }

}
