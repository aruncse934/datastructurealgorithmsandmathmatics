package BitManipulations;

public class FindPOowerOfNumber {
    public static void main(String[] args) {
        int x= 512;
        System.out.println( isPowOfNumber(x));
    }

    public static boolean isPowOfNumber(int x){
        /*if(x == 0){
            return false;
        }
        else {
            while (x%2 == 0)
                x = x/2;
            return (x == 1);
        }*/
         // x will check if x == 0 and !(x & (x - 1)) will check if x is a power of 2 or not
        return x != 0 && ((x & (x - 1)) == 0); //O(1)
        // return x>0 && (x&x-1)==0;

        //return x>0 && x==Math.pow(2, Math.round(Math.log(x)/Math.log(2)));
    }
}
