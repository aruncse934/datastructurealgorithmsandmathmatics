package BitManipulations;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
    public static void main(String[] args) {
        /*char set[] = {'a','b','c'};
        printSubsets(set);*/
        int nums[] = {1, 2, 3};
       // System.out.println(backtrack(nums));
    }
    /*public static void printSubsets(char set[]){
        int n = set.length;
        for(int i =0;i<(1 << n);i++){
            System.out.print("{");

            for(int j = 0; j<n;j++)
                if((i &(1 << j))> 0)
                    System.out.print(set[j] +" ");
            System.out.println("}");
        }
    }*/


    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
     //  Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }

    private static void backtrack(List<List<Integer>> list , List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }
}
