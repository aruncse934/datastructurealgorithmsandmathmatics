package BitManipulations;

public class LargestPower2 {
    public static void main(String[] args) {
       int n = 32;
        System.out.println(largestPower2(n));
    }
    public static int largestPower2(int n){
        int res =0 ;
        for (int i = n; i >=1;--i){
            if((i & (i-1)) == 0){
                res =i;
                break;
            }
        }
        return res;
    }
}
