package UnionFind;

import java.io.IOException;
import java.util.Arrays;

public class Test {

    public static void main(String[] args) throws IOException {
       /* BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getProperty("OUTPUT_FILE_PATH")));
        bufferedWriter.write("\n");
        bufferedWriter.close();
        bufferedWriter = new BufferedWriter(new FileWriter(System.getProperty("OUTPUT_FILE_PATH"),true));
        String T = bufferedReader.readLine();

        String W = bufferedReader.readLine();

      //  int outcome = solve(T,W);

    //    bufferedWriter.write(outcome + "\n");

        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();*/
        System.out.println(countAnagrams("forxxorfxdofr","for"));




    }

    public static int solve(String T, String W) {
       // int ch = 256;
        int n = T.length();
        int m = W.length();
        int count = 0;
        if (n < 0 || m < 0 || n < m) 
            return -1;

        char[] ar1 = new char[256];
        char[] ar2 = new char[256];

        for (int i = 0; i < n; i++) {
            ar1[256 - T.charAt(i)]++;
            ar2[256 - W.charAt(i)]++;
           // pArr[CHARACTERS - p.charAt(i)]++;

        }
        if (Arrays.equals(ar1, ar2))
            count += 1;
        for (int i = 0; i < m; i++) {
            ar1[256 - T.charAt(i)]++;
            ar2[256 - W.charAt(i - m)]--;

            if (Arrays.equals(ar1, ar2))
                count += 1;
        }
        return count; //return type "int".
    }
    public static int countAnagrams(String s, String p)
    {
        // change CHARACTERS to support range of supported
        // characters
        int CHARACTERS = 256;
        int sn = s.length();
        int pn = p.length();
        int count = 0;
        if (sn < 0 || pn < 0 || sn < pn)
            return 0;

        char[] pArr = new char[CHARACTERS];
        char[] sArr = new char[CHARACTERS];
        int i = 0;
        // till window size
        for (; i < pn; i++) {
            sArr[CHARACTERS - s.charAt(i)]++;
            pArr[CHARACTERS - p.charAt(i)]++;
        }
        if (Arrays.equals(pArr, sArr))
            count += 1;
        // next window
        for (; i < sn; i++) {
            sArr[CHARACTERS - s.charAt(i)]++;
            sArr[CHARACTERS - s.charAt(i - pn)]--;

            if (Arrays.equals(pArr, sArr))
                count += 1;
        }
        return count;
    }
}





