package OOD;

/*Description
Implement a Rectangle class which include the following attributes and methods:

Two public attributes width and height.
A constructor which expects two parameters width and height of type int.
A method getArea which would calculate the size of the rectangle and return.
Example
Example1:
Java:
    Rectangle rec = new Rectangle(3, 4);
    rec.getArea(); // should get 12，3*4=12

Example2:
Java:
    Rectangle rec = new Rectangle(4, 4);
    rec.getArea(); // should get 16，4*4=16*/
public class RectangleArea {

    /*
     * Define two public attributes width and height of type int.
     */
     private int width;
     private int height;

    /*
     * Define a constructor which expects two parameters width and height here.
     */
    public RectangleArea(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /*
     * Define a public method `getArea` which can calculate the area of the
     * rectangle and return.
     */
     public int getArea(){
         return width*height;
     }

    public static void main(String[] args) {
         RectangleArea rectangleArea = new RectangleArea(3,4);
        System.out.println("Reactagle Area is : "+ rectangleArea.getArea());

    }
}
