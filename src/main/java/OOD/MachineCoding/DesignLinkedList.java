package OOD.MachineCoding;



public class DesignLinkedList {
            Node head;
            int val;

    class Node{
        private int val;
        private Node next;

        Node(int val){
            this.val = val;
        }
    }
   public DesignLinkedList(){
             this.head = null;
             this.val = 0;
    }

    public int get(int index) {
        if(index < 0 || index >= this.val){ return -1; }
        else {
            int count = 0;
            Node curr = head;
            while (count < index){
                curr = curr.next;
                count++;
            }
            return curr.val;
        }
    }


    public void addAtHead(int val) {
       Node newNode = new Node(val);
       newNode.next = this.head;
       this.head = newNode;
       this.val++;
    }

    public void addAtTail(int val) {
         if(this.val == 0){
             addAtHead(val);
             return;
         }
         Node newNode  = new Node(val);
         Node temp  = this.head;
         while (temp.next != null){
             temp = temp.next;
         }
         temp.next = newNode;
         newNode.next = null;
         this.val++;
    }

    public void addAtIndex(int index, int val) {
          if(index > this.val){
              return;
          }
          if(index == 0){
              addAtHead(val);
              return;
          }
          if(index == this.val){
              addAtTail(val);
              return;
          }
          Node newNode = new Node(val);
          Node temp = this.head;
          int count = 0;

          while (count < index -1){
              temp = temp.next;
              count++;
          }
          newNode.next = temp.next;
          temp.next = newNode;
          this.val++;
    }


    public void deleteAtIndex(int index) {
        if(index < 0 || index >= this.val){
            return;
        }
        Node curr = this.head;
        int count = 0;

        if(index == 0){
            this.head = curr.next;
            this.val--;
        } else {
            Node pre = null;
            while (count < index){
                pre = curr;
                curr = curr.next;
                count++;
            }
            pre.next = curr.next;
            this.val--;
        }
    }

    public static void main(String[] args) {
        DesignLinkedList linkedList = new DesignLinkedList();
       /* int param_1 = list.get(index);
          list.addAtHead(val);
          list.addAtTail(val);
          list.addAtIndex(index,val);
          list.deleteAtIndex(index);*/
        linkedList.addAtHead(1);
        linkedList.addAtTail(3);
        linkedList.addAtIndex(1, 2);  // linked list becomes 1->2->3
        linkedList.get(1);            // returns 2
        linkedList.deleteAtIndex(1);  // now the linked list is 1->3
        linkedList.get(1);


    }
}
