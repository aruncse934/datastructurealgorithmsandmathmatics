package OOD.MachineCoding;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Common {

     public static final String SPACE = " ";
     public static final int ZERO = 0;
     public static final int TWO = 2;
     public static final int THREE = 3;
     public static final float PERCENTAGE = 0.02f;
 }

 @AllArgsConstructor
 @Getter
enum PassengerType {
    ADULT(200),
    SENIOR_CITIZEN(100),
    KID(50);
    private final int val;
 }

enum Station {
    CENTRAL,
    AIRPORT
}

@Getter
@Setter
@AllArgsConstructor
class MetroCard {         //BALANCE <METROCARD_NUMBER> <BALANCE_IN_THE_METROCARD>
    private final String mcNumber;
    private int balance;
}

@Getter
@AllArgsConstructor
class CheckIn {      //CHECK_IN <METROCARD_NUMBER>  <PASSENGER_TYPE> <FROM_STATION>

    private final String mcNumber;
    private final PassengerType passengerType;
    private final Station fromStation;

 /*   public CheckIn(String mcNumber, String passengerType, String fromStation) {
        this.mcNumber = mcNumber;
        this.passengerType = PassengerType.valueOf(passengerType);
        this.fromStation = Station.valueOf(fromStation);
    }*/
}


@Getter
@AllArgsConstructor
class PassengerCount implements Comparable<PassengerCount> {

    private final int count;
    private final PassengerType passengerType;

    @Override
    public int compareTo(PassengerCount o) {
        return o.getCount() - this.getCount() == 0 ?
                this.passengerType.name().charAt(0) - o.passengerType.name().charAt(0)
                : o.getCount() - this.getCount();
    }
}
interface CheckInService {

    /*
        Checks-in a passenger
        @CheckIn object
    */
    void checkInPassenger(CheckIn checkIn);

    /*
        return station type along with count
        @return map - amount left to pay
    */
    Map<Station, Map<PassengerType, Integer>> getStationTypeCountMap();

    /*
        returns station vs amount map
        @return map - amount paid mapped to station
    */
    Map<String, Integer> getStationAmountMap();

    /*
        returns station vs amount map
        @return map - amount paid mapped to station
    */
    Map<String, Integer> getStationDiscountMap();

}

interface MetroCardService {

    /*
        Adds new metro card from input, maintains a map of card_id mapped to card
        @param cardId - card number
        @param balance - balance amount in card
     */
    void addMetroCard(String mcNumber, int balance);

    /*
        Return list of all metro cards
        @return List - of metro cards
     */
    List<MetroCard> getCards();

    /*
        Performs a transaction on metro card from input
        @param cardId - card number
        @param amount - transaction amount
        @return int - amount left to pay
     */
    int transactCard(String mcNumber, int amount);
}

interface PrintSummaryService {

    /*
        prints the summary in desired format after all operations
     */
    void printSummary();
}


class CheckInServiceImpl implements CheckInService {

    Map<String, Integer> stationAmountMap;
    Map<String, Integer> stationDiscountMap;
    Map<Station, Map<PassengerType, Integer>> stationTypeCountMap;
    Map<String, Station> passengerMap;

    MetroCardService metroCardService;

    public CheckInServiceImpl(MetroCardService metroCardService) {
        this.metroCardService = metroCardService;
        stationAmountMap = new HashMap<>();
        stationDiscountMap = new HashMap<>();
        stationTypeCountMap = new HashMap<>();
        passengerMap = new HashMap<>();
        for (Station station : Station.values()) {
            stationAmountMap.put(station.name(), Common.ZERO);
            stationDiscountMap.put(station.name(), Common.ZERO);
            stationTypeCountMap.put(station, new HashMap<>());
        }

    }

    @Override
    public void checkInPassenger(CheckIn checkIn) {
        String mcNumber = checkIn.getMcNumber();

        if (passengerMap.containsKey(mcNumber)) {
            int amount = checkIn.getPassengerType().getVal() / Common.TWO;
            int collection = stationAmountMap.get(checkIn.getFromStation().name()) + amount;
            int discount = stationDiscountMap.get(checkIn.getFromStation().name()) + amount;
            stationDiscountMap.put(checkIn.getFromStation().name(), discount);

            int remaining = metroCardService.transactCard(mcNumber, amount);
            if (amount != Common.ZERO) {
                collection += remaining * Common.PERCENTAGE;
            }
            //            System.out.println(amount+" "+collection+" "+discount+" "+remaining);
            stationAmountMap.put(checkIn.getFromStation().name(), collection);
            passengerMap.remove(mcNumber);
        } else {
            int amount = checkIn.getPassengerType().getVal();
            int collection = stationAmountMap.get(checkIn.getFromStation().name()) + amount;

            int remaining = metroCardService.transactCard(mcNumber, amount);
            if (amount != Common.ZERO) {
                collection += remaining * Common.PERCENTAGE;
            }
            //            System.out.println(amount+" "+collection+" "+remaining);
            stationAmountMap.put(checkIn.getFromStation().name(), collection);
            passengerMap.put(mcNumber, checkIn.getFromStation());
        }

        updatePassengerCount(stationTypeCountMap.get(checkIn.getFromStation()), checkIn.getPassengerType(), checkIn.getFromStation());
    }

    private void updatePassengerCount(Map<PassengerType, Integer> tempMap, PassengerType passengerType, Station station) {
        if (tempMap.containsKey(passengerType)) {
            int val = tempMap.get(passengerType) + 1;
            tempMap.put(passengerType, val);
        } else {
            tempMap.put(passengerType, 1);
        }
        stationTypeCountMap.put(station, tempMap);
    }

    @Override
    public Map<Station, Map<PassengerType, Integer>> getStationTypeCountMap() {
        return this.stationTypeCountMap;
    }

    @Override
    public Map<String, Integer> getStationAmountMap() {
        return this.stationAmountMap;
    }

    @Override
    public Map<String, Integer> getStationDiscountMap() {
        return this.stationDiscountMap;
    }
}

class MetroCardServiceImpl implements MetroCardService {

    private final Map<String, MetroCard> metroCards = new HashMap<>();

    @Override
    public void addMetroCard(String mcNumber, int balance) {
        metroCards.put(mcNumber, new MetroCard(mcNumber, balance));
    }

    @Override
    public List<MetroCard> getCards() {
        List<MetroCard> list = new ArrayList<>();
        metroCards.forEach((k,v)->{list.add(v);});
        return list;
    }

    @Override
    public int transactCard(String mcNumber, int amount) {
        MetroCard metroCard = metroCards.get(mcNumber);
        int balance = metroCard.getBalance();
        int diff = balance - amount;
        if (diff < 0) {
            metroCard.setBalance(0);
            metroCards.put(mcNumber, metroCard);
            return Math.abs(balance - amount);
        }
        metroCard.setBalance(diff);
        metroCards.put(mcNumber, metroCard);
        return 0;
    }
}

class PrintSummaryServiceImpl implements PrintSummaryService {

    CheckInService checkInService;

    public PrintSummaryServiceImpl(CheckInService checkInService) {
        this.checkInService = checkInService;
    }

    @Override
    public void printSummary() {

        Map<String, Integer> stationAmountMap = checkInService.getStationAmountMap();
        Map<String, Integer> stationDiscountMap = checkInService.getStationDiscountMap();
        Map<Station, Map<PassengerType, Integer>> stationTypeCountMap = checkInService.getStationTypeCountMap();

        for (Station station : Station.values()) {
            System.out.println("TOTAL_COLLECTION " + station.name() + " " + stationAmountMap.get(station.name()) + " " + stationDiscountMap.get(station.name()));
            System.out.println("PASSENGER_TYPE_SUMMARY");
            PriorityQueue<PassengerCount> sortedCount = convertToQueue(stationTypeCountMap.get(station));
            while (!sortedCount.isEmpty()) {
                PassengerCount passengerCount = sortedCount.poll();
                System.out.println(passengerCount.getPassengerType().name() + " " + passengerCount.getCount());
            }
        }
    }

    private PriorityQueue<PassengerCount> convertToQueue(Map<PassengerType, Integer> map) {
        PriorityQueue<PassengerCount> priorityQueue = new PriorityQueue<>();
        for (Map.Entry<PassengerType, Integer> entry : map.entrySet()) {
            priorityQueue.add(new PassengerCount(entry.getValue(), entry.getKey()));
        }
        return priorityQueue;
    }
}

class MetroStationService {

    MetroCardService metroCardService = new MetroCardServiceImpl();
    CheckInService checkInService = new CheckInServiceImpl(metroCardService);
    PrintSummaryService printSummaryService = new PrintSummaryServiceImpl(checkInService);

    public void start(String[] args) throws IOException {

        try {
            // the file to be opened for reading
            FileInputStream fis = new FileInputStream(args[0]);
            Scanner sc = new Scanner(fis); // file to be scanned
            // returns true if there is another line to read
            while (sc.hasNextLine()) {
                String[] input = sc.nextLine().split(Common.SPACE, Common.TWO);
                switch (input[0]) {
                    case "BALANCE":
                        String[] cardProperties = input[1].split(Common.SPACE, Common.TWO);
//                        card number - cardProperties[0]
//                        card balance - Integer.parseInt(cardProperties[1])
                        metroCardService.addMetroCard(cardProperties[0], Integer.parseInt(cardProperties[1]));
                        break;
                    case "CHECK_IN":
                        String[] checkInDetails = input[1].split(Common.SPACE, Common.THREE);
//                        card number - checkInDetails[0]
//                        passenger type - checkInDetails[1]
//                        from station - checkInDetails[2]
                  //      checkInService.checkInPassenger(new CheckIn(checkInDetails[0], checkInDetails[1], checkInDetails[2]));
                        break;
                    case "PRINT_SUMMARY":
//                        action
                        printSummaryService.printSummary();
                        break;
                    default:
                        break;
                }

            }
            sc.close(); // closes the scanner
        } catch (IOException e) {
            throw new IOException("Error while reading input");
        }
    }
}



public class MetroCardSystem {



    @Test
    void testMetroCardSystem() {
        // Given
        List<String> inputs = Arrays.asList(
                "BALANCE MC1 600",
                "BALANCE MC2 500",
                "BALANCE MC3 50",
                "BALANCE MC4 50",
                "BALANCE MC5 200",
                "CHECK_IN MC1 ADULT CENTRAL",
                "CHECK_IN MC2 SENIOR_CITIZEN CENTRAL",
                "CHECK_IN MC1 ADULT AIRPORT",
                "CHECK_IN MC3 KID AIRPORT",
                "CHECK_IN MC4 ADULT AIRPORT",
                "CHECK_IN MC5 KID AIRPORT",
                "PRINT_SUMMARY"
        );

        StringBuilder output = new StringBuilder();
        // Stubbing System.out.println to capture the output
        System.setOut(new java.io.PrintStream(new java.io.ByteArrayOutputStream()) {
            public void println(String message) {
                output.append(message).append("\n");
            }
        });

        // When
        for (String input : inputs) {
            processInput(input);
        }

        // Then
        String expectedOutput = "TOTAL_COLLECTION CENTRAL 300 0\n" +
                "PASSENGER_TYPE_SUMMARY\n" +
                "ADULT 1\n" +
                "SENIOR_CITIZEN 1\n" +
                "TOTAL_COLLECTION AIRPORT  403 100\n" +
                "PASSENGER_TYPE_SUMMARY\n" +
                "ADULT 2\n" +
                "KID 2\n";
        assertEquals(expectedOutput, output.toString());
    }

    private  void processInput(String input) {

        MetroCardService metroCardService = new MetroCardServiceImpl();
        CheckInService checkInService = new CheckInServiceImpl(metroCardService);
        PrintSummaryService printSummaryService = new PrintSummaryServiceImpl(checkInService);

      /*  metroCardService.addCard("MC1", 600);
        checkInService.checkInPassenger(new CheckIn("MC1", "ADULT", "AIRPORT")); //CHECK_IN MC1 ADULT AIRPORT
        printSummaryService.printSummary();*/

        String[] inputs = input.split(" ");
        String command = inputs[0];

        switch (command) {
            case "BALANCE":
                String[] tokens = inputs[1].split(" ");
                metroCardService.addMetroCard(tokens[0], Integer.parseInt(tokens[1]));
                break;
            case "CHECK_IN":
                String[] checkInDetails = inputs[1].split(" ");
           //     checkInService.checkInPassenger(new CheckIn(checkInDetails[0], checkInDetails[1], checkInDetails[2]));
                break;
            case "PRINT_SUMMARY":
                printSummaryService.printSummary();
                break;
            default:
                break;
        }
    }


    public static void main(String[] args) throws IOException {
        MetroCardService metroCardService = new MetroCardServiceImpl();
        CheckInService checkInService = new CheckInServiceImpl(metroCardService);
        PrintSummaryService printSummaryService = new PrintSummaryServiceImpl(checkInService);

        metroCardService.addMetroCard("MC1", 600);
        metroCardService.addMetroCard("MC2", 500);
        metroCardService.addMetroCard("MC3", 50);
        metroCardService.addMetroCard("MC4", 50);
        metroCardService.addMetroCard("MC5", 200);

        checkInService.checkInPassenger(new CheckIn("MC1", PassengerType.ADULT, Station.CENTRAL));
        checkInService.checkInPassenger(new CheckIn("MC2", PassengerType.SENIOR_CITIZEN,  Station.CENTRAL));
        checkInService.checkInPassenger(new CheckIn("MC1", PassengerType.ADULT,  Station.AIRPORT));
        checkInService.checkInPassenger(new CheckIn("MC3", PassengerType.KID,  Station.AIRPORT));
        checkInService.checkInPassenger(new CheckIn("MC4", PassengerType.ADULT,  Station.AIRPORT));
        checkInService.checkInPassenger(new CheckIn("MC5", PassengerType.KID,  Station.AIRPORT));

        printSummaryService.printSummary();

        System.out.println("==================");



    }
    
}



/*
@Getter
@Setter
class Passenger {

    private String metroCardNumber;
    private Integer balanceInTheMetrocard;
    private Integer journeyCount;

    public Passenger(String metroCardNumber, Integer balanceInTheMetrocard) {
        super();
        this.metroCardNumber = metroCardNumber;
        this.balanceInTheMetrocard = balanceInTheMetrocard;
        this.journeyCount=0;
    }
}
@Getter
@Setter
@AllArgsConstructor
class EachJourneyCharge {

    private Integer discount;
    private Integer costOfJourney;
}

@Getter
@Setter
@AllArgsConstructor
class PassengerCheckIn {

    private String metroCardNumber;
    private String passengerType;
    private String fromStation;
    private Integer charge;
    private EachJourneyCharge journeyCharge;
}
@Getter
@Setter
@AllArgsConstructor
class PassengerSummary {

    private HashMap<String, Passenger> passengerMap;
    private List<PassengerCheckIn> checkInList;
    private Integer totalAmountAirport;
    private Integer totalAmountCentral;
    private Integer totalDiscountAirport;
    private Integer totalDiscountCentral;
    private List<String> orderByTypeAirport;
    private List<String> orderByTypeCentral;
}
@Getter
@Setter
@AllArgsConstructor
class StationStatistics {
    private Integer count;
    private String passengerType;
    private Integer totalCharge;
    private Integer totalDiscount;
    private Integer orderByType;
}*/



//main
/*  ***
 private static Map<String, Integer> mcBalances = new HashMap<>();
    private static Map<String, Integer> passengerTypeSummary = new HashMap<>();
    private static int totalCollectionCentral = 0;
    private static int totalCollectionAirport = 0;
    private static int totalCollectionAirportPassengers = 0;
     public static void main(String[] args) {

        MetroStationService metroStationService = new MetroStationService();
        metroStationService.start(args);
        processInput("BALANCEMC1 600");
        processInput("BALANCEMC2 500");
        processInput("BALANCEMC3 50");
        processInput("BALANCEMC4 50");
        processInput("BALANCEMC5 200");
        processInput("CHECK_INMC1 ADULT CENTRAL");
        processInput("CHECK_INMC2 SENIOR_CITIZEN CENTRAL");
        processInput("CHECK_INMC1 ADULT AIRPORT");
        processInput("CHECK_INMC3 KID AIRPORT");
        processInput("CHECK_INMC4 ADULT AIRPORT");
        processInput("CHECK_INMC5 KID AIRPORT");
        processInput("PRINT_SUMMARY");




private static void processInput(String input) {
        String[] tokens = input.split(" ");
        String command = tokens[0];

        switch (command) {
            case "BALANCEMC1":
            case "BALANCEMC2":
            case "BALANCEMC3":
            case "BALANCEMC4":
            case "BALANCEMC5":
                BALANCE(tokens[0], Integer.parseInt(tokens[1]));
                break;
            case "CHECK_INMC1":
            case "CHECK_INMC2":
            case "CHECK_INMC3":
            case "CHECK_INMC4":
            case "CHECK_INMC5":
                CHECK_IN(tokens[0], tokens[1], tokens[2]);
                break;
            case "PRINT_SUMMARY":
                printSummary();
                break;
        }
    }

    private static void BALANCE(String METROCARD_NUMBER, int BALANCE_IN_THE_METROCARD) {
        mcBalances.put(METROCARD_NUMBER, BALANCE_IN_THE_METROCARD);
    }

    private static void CHECK_IN(String METROCARD_NUMBER, String PASSENGER_TYPE, String FROM_STATION) {
        int fare = calculateFare(FROM_STATION);
        int balance = mcBalances.get(METROCARD_NUMBER);

        if (balance < fare) {
            rechargeMetroCard(METROCARD_NUMBER, fare - balance);
            balance = mcBalances.get(METROCARD_NUMBER);
        }

        int collectionAmount = fare;
        if (FROM_STATION.equals("AIRPORT")) {
            collectionAmount += (int) (fare * 0.02); // Add 2% service fee for airport travel
            totalCollectionAirport += collectionAmount;
            totalCollectionAirportPassengers++;
        } else {
            totalCollectionCentral += collectionAmount;
        }

        mcBalances.put(METROCARD_NUMBER, balance - fare);
        updatePassengerTypeSummary(PASSENGER_TYPE);
    }

    private static int calculateFare(String location) {
        // Implement fare calculation logic based on the location
        return 100; // Placeholder value for demonstration
    }

    private static void rechargeMetroCard(String mc, int rechargeAmount) {
        int balance = mcBalances.get(mc);
        int serviceFee = (int) (rechargeAmount * 0.02); // Calculate 2% service fee
        mcBalances.put(mc, balance + rechargeAmount + serviceFee);
    }

    private static void updatePassengerTypeSummary(String passengerType) {
        passengerTypeSummary.put(passengerType, passengerTypeSummary.getOrDefault(passengerType, 0) + 1);
    }

    private static void printSummary() {
        System.out.println(" TOTAL_COLLECTION CENTRAL" + totalCollectionCentral + " 0");
        System.out.println("PASSENGER_TYPE_SUMMARY");
        for (Map.Entry<String, Integer> entry : passengerTypeSummary.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("TOTAL_COLLECTION AIRPORT " + totalCollectionAirport + " " + totalCollectionAirportPassengers);
        System.out.println("PASSENGER_TYPE_SUMMARY");
        for (Map.Entry<String, Integer> entry : passengerTypeSummary.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
****/