package OOD.DesignMapsNavigatorClient;


public interface TransportBehavior {

        PathNode buildPath(Location A, Location B);
}

