package DesignPatterns.Behavioral.Strategy.Example1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StrategyPatternMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(StrategyPatternMain.class);

    public static void main(String[] args) {
        List<Message> messages = new ArrayList<>();
        messages.add(new Message(MessageType.TEXT,100,"This is test message"));
        messages.add(new Message(MessageType.XML,200,"How are you"));
        messages.add(new Message(MessageType.TEXT,300,"Does Strategy pattern follows OCP design principle?"));
        messages.add(new Message(MessageType.TEXT, 400, "Wrong Message, should be filtered"));
        messages = filter(messages, new FilterByType(MessageType.XML));
        messages = filter(messages, new FilterByKeyword("Wrong"));
        messages = filter(messages,new FilterBySize(200));

        System.out.println("Strategy Pattern:"+messages);


    }

    public static final List<Message> filter(List<Message> messageList, FilteringStrategy strategy) {
        Iterator<Message> iterator = messageList.iterator();
        while (iterator.hasNext()) {
            Message msg = iterator.next();
            if (strategy.isFilterable(msg)) {
                LOGGER.info(strategy.toString() + msg);
                iterator.remove();
            }
        }
        return messageList;
    }

}

