package DesignPatterns.Behavioral.Strategy.Example1;

public interface FilteringStrategy  {

    boolean isFilterable(Message msg);
}
