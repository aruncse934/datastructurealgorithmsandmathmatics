package DesignPatterns.Behavioral.Strategy.Example1;

public class Message {

    private MessageType type;
    private int size;
    private String content;

    public Message(MessageType type, int size, String content) {
        this.type = type;
        this.size = size;
        this.content = content;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", size=" + size +
                ", content='" + content + '\'' +
                '}';
    }

}
