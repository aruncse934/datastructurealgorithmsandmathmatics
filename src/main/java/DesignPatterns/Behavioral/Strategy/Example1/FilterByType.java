package DesignPatterns.Behavioral.Strategy.Example1;

public class FilterByType implements FilteringStrategy{
    private MessageType type;
    public FilterByType(MessageType type) {
        this.type =type;
    }

    @Override
    public boolean isFilterable(Message msg) {
        return type == msg.getType();
    }
    public String toString(){
        return "Filtering By type: " + type;
    }
}
