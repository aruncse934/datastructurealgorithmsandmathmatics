package DesignPatterns.Behavioral.CommandPattern.CommandPatternExample;

public class MenuOptions implements ActionListenerCommand {
    private Document doc;
    public MenuOptions(Document doc) {
        this.doc = doc;
    }
    @Override
    public void execute() {
        doc.save();
    }
} 

