package DesignPatterns.Behavioral.CommandPattern.CommandPatternExample;

public class AdapterPatternDemo {
    public static void main(String[] args) {
        Document doc = new Document();

        ActionListenerCommand clickOpen = new ActionOpen(doc);
        ActionListenerCommand clickSave = new ActionOpen(doc);

      //  MenuOptions menu = new MenuOptions(clickOpen, clickSave);

    }
}


        /*MenuOptions menu = new MenuOptions(clickOpen, clickSave);

        menu.clickOpen();
        menu.clickSave();
  */
