package DesignPatterns.Behavioral.CommandPattern.CommandPatternExample;

public interface ActionListenerCommand {
    public void execute();
}
