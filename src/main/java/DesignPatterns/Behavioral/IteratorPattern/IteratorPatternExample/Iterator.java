package DesignPatterns.Behavioral.IteratorPattern.IteratorPatternExample;

public interface Iterator {
    public boolean hasNext();
    public Object next();

}

