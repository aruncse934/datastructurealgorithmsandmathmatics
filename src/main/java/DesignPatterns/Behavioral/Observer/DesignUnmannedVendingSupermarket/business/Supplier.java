package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.Product;

public interface Supplier {

    void supplyProducts(Product product);
}
