package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.Product;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.enums.ProductType;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.service.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Market implements Notifier{
    private final Map<ProductType, List<Product>> productMap;

    private final Map<String, Integer> storage;

    private final List<Supplier> suppliers;

    public Market() {
        // --- write your code here ---

        productMap = new HashMap<>();
        storage = new HashMap<>();

        suppliers = new ArrayList<>();
    }

    public void init(List<Product> productList) {

        // --- write your code here ---

    }

    public void purchase(Product product) {

        // --- write your code here ---

    }

    public void sale(List<Request> requests) {

        // --- write your code here ---

    }

    public void sale(Request request) {

        Product product = getProduct(request);
        Integer pStorage = storage.get(request.getName());

        if (pStorage < request.getTotal()) {
            System.out.println("\n***************");
            System.out.println("Buy " + request.getName() + " Error: Out of stock, please reselect!");
            System.out.println("***************");
        } else {
            int totalPrice = product.getPrice() * request.getTotal();
            System.out.println("\n<---------------");
            System.out.println("Buy " + request.getTotal() + " " + request.getName() + ". Total price: " + totalPrice);
            System.out.println("<---------------");
            storage.put(request.getName(), storage.get(request.getName()) - request.getTotal());
        }

        // 判断库存是否<5 以进行进货
        // Determine if storage is less than 5 for purchasing
        // --- write your code here ---

    }

    private Product getProduct(Request request) {
        return null;
    }

    public Map<String, Integer> getStorage() {
        return storage;
    }

    public Map<ProductType, List<Product>> getProductMap() {
        return productMap;
    }

    @Override
    public void addSupplier(Supplier supplier) {

    }

    @Override
    public void notifySupplier(Product product) {

    }

    // You can add private methods you needed for calling.


}
