package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.Product;

public class ProductSupplier implements Supplier{

    // 请添加一个属性以注入一个 Market 对象
    // You need to inject a Market object. Please add param.

    private Market market;
    private Product purchase;

    public ProductSupplier(Market market) {
        // --- write your code here ---
    }

    @Override
    public void supplyProducts(Product product) {

        // 新建一个名为purchase的Product对象 并将传入的product属性赋给purchase
        // Create a new Product object named `purchase`
        // and assign the incoming `product` property to `purchase`
        // --- write your code here ---

        System.out.println("\n--------------->");
        System.out.println("Supply " + purchase.toString() + " to Market.");
        System.out.println("--------------->");
        System.out.println(purchase.getName() + " in Market Storage: " + market.getStorage().get(purchase.getName()));
        System.out.println("--------------->");

    }

}
