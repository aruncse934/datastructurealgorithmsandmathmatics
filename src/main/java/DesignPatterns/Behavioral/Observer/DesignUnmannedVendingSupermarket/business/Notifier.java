package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.Product;

public interface Notifier {

    // 添加供应商
    void addSupplier(Supplier supplier);
    // 通知供应商进行商品补货
    void notifySupplier(Product product);

}
