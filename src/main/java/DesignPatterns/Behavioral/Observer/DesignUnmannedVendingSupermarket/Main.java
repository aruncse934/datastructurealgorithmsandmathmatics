package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business.Market;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business.ProductSupplier;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business.Supplier;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.Product;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.enums.ProductType;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.service.CustomerService;
import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.service.Request;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Product cola = new Product(ProductType.DRINK, "Cola", 5, 3);
        Product chips = new Product(ProductType.SNACK, "Chips", 5, 6);
        Market market = new Market();
        List<Product> productList = new ArrayList<>();
        productList.add(cola);
        productList.add(chips);
        market.init(productList);

        Supplier productSupplier = new ProductSupplier(market);
        market.addSupplier(productSupplier);

        CustomerService customerService = new CustomerService(market);
        customerService.showProducts();
        Request colaRequest = new Request(ProductType.DRINK, "Cola", 3);
        customerService.buy(colaRequest);

        Request request1 = new Request(ProductType.SNACK, "Chips", 6);
        Request request2 = new Request(ProductType.DRINK, "Cola", 7);
        List<Request> requests = new ArrayList<>();
        requests.add(request1);
        requests.add(request2);
        System.out.println();
        customerService.buy(requests);
        System.out.println();
        customerService.showProducts();

    }

}
