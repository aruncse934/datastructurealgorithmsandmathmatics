package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.enums;

public enum  ProductType {
    DRINK, SNACK
}
