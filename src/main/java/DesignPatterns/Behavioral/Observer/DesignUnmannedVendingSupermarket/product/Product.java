package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.enums.ProductType;

public class Product {

    // --- write your code here ---
    // Add member variables, constructors and Getter&Setter needed.

    private ProductType type;
    private String name;
    private  int count;
    private int price;

    public Product(ProductType type, String name, int count, int price) {
        this.type = type;
        this.name = name;
        this.count = count;
        this.price = price;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}