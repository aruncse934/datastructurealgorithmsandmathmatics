package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.service;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.business.Market;

import java.util.List;

public class CustomerService {
    // 添加一个Market属性 并在构造时将其注入
    // Add a Market property and inject it in Constructor.
    // --- write your code here ---

    public CustomerService(Market market) {
        // --- write your code here ---

    }

    public void showProducts() {

        // --- write your code here ---

    }

    public void buy(Request request) {

        // --- write your code here ---

    }

    public void buy(List<Request> requests) {

        // --- write your code here ---

    }

}
