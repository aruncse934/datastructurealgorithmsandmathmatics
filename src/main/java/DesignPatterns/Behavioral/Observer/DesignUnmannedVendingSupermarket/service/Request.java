package DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.service;

import DesignPatterns.Behavioral.Observer.DesignUnmannedVendingSupermarket.product.enums.ProductType;

public class Request {
    private ProductType type;

    private String name;

    private Integer total;

    public Request(ProductType type, String name, Integer total) {
        this.type = type;
        this.name = name;
        this.total = total;
    }

    public ProductType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public Integer getTotal() {
        return total;
    }
}
