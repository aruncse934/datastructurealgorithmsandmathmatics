package DesignPatterns.Behavioral.Observer.observerExample1;

public interface WeatherObserver {
    void update(WeatherType currentWeather);

}
