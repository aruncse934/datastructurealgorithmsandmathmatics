package DesignPatterns.Behavioral.Observer.observerExample1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Hobbits implements WeatherObserver {
    private static final Logger LOGGER = LoggerFactory.getLogger(Hobbits.class);

    public void update(WeatherType currentWeather) {
        LOGGER.info("The hobbits are facing " + currentWeather.getDescription() + " wheather now");
    }

}
