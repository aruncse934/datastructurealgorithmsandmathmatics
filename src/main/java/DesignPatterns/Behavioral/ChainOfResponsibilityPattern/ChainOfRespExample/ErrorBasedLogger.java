package DesignPatterns.Behavioral.ChainOfResponsibilityPattern.ChainOfRespExample;

public class ErrorBasedLogger extends Logger {

    public ErrorBasedLogger(int levels) {
        this.levels = levels;
    }

    @Override
    protected void diplayLogInfo(String msg) {
        System.out.println("ERROR LOGGER INFO: "+msg);
    }
}
