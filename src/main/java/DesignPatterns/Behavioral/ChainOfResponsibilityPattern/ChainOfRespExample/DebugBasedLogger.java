package DesignPatterns.Behavioral.ChainOfResponsibilityPattern.ChainOfRespExample;

public class DebugBasedLogger extends Logger {
    public DebugBasedLogger(int levels) {
        this.levels = levels;
    }

    @Override
    protected void diplayLogInfo(String msg) {
        System.out.println("DEBUG LOGGER INFO: "+msg);
    }
}
