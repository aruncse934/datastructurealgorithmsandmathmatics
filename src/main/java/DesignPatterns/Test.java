package DesignPatterns;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

//Behavioral Design Patterns
//Template Method Design Pattern
 abstract class HouseTemplate{

        //template methods to be implemented by subclasses
        public abstract void buildWalls();
        public abstract void buildPillars();

        public final void buildHouse(){
         buildWalls();
         buildPillars();
         buildWindows();
        }

         //default implementation
         private void buildWindows() {
          System.out.println("Building Glass Windows");
         }
       }
       class WoodenHouse extends HouseTemplate {

        @Override
        public void buildWalls() {
         System.out.println("Building Wooden Walls");
        }
        @Override
        public void buildPillars() {
         System.out.println("Building Pillars with Wood coating");
        }
       }

       class GlassHouse extends HouseTemplate {

        @Override
        public void buildWalls() {
         System.out.println("Building Glass Walls\"");
        }

        @Override
        public void buildPillars() {
         System.out.println("Building Pillars with GlassHouse coating");
        }
}


//Strategy Design Pattern
interface PaymentStrategy {

     public void pay(int amount);
    }

    @AllArgsConstructor
    class CreditCardStrategy implements PaymentStrategy {

         private String HolderName;
         private String cardNumber;
         private String cvv;
         private String dateOfExpiry;

        @Override
        public void pay(int amount) {
            System.out.println(amount +" paid with credit/debit card");
        }
    }

    @AllArgsConstructor
    class PaypalStrategy implements PaymentStrategy {

        private String emailId;
        private String password;

        @Override
        public void pay(int amount) {
            System.out.println(amount + " paid using Paypal.");
        }
    }

    @AllArgsConstructor
    @Getter
     class Item {
        private String upcCode;
        private int price;
     }

    class ShoppingCart {
          List<Item> itemList;

          public ShoppingCart(){
              this.itemList = new ArrayList<>();
          }

         public void addItem(Item item){
               this.itemList.add(item);
          }

          public void  removeItem(Item item){
              this.itemList.remove(item);
          }
          public int calculateTotal(){
              int  sum = 0;
          /*    for(Item item : itemList){
                  sum += item.getPrice();
              }*/
              return (int) itemList.stream()
                      .mapToDouble(Item::getPrice)
                      .sum();
          }
          public void pay(PaymentStrategy paymentStrategy){
              int amount = calculateTotal();
              paymentStrategy.pay(amount);
          }

}

// State Design Pattern
interface State {
    public void doAction();
}
class TVStartState implements State {

    @Override
    public void doAction() {
        System.out.println("TV is turned ON");
    }
}
class TVStopState implements State {

    @Override
    public void doAction() {
        System.out.println("TV is turned OFF");
    }
}

@Getter@Setter
class TVContext implements State{
    private State tvState;
    @Override
    public void doAction() {
          this.tvState.doAction();
    }
}



//Mediator Design Pattern

@AllArgsConstructor
abstract class User {
    protected ChatMediator mediator;
    protected String name;

    public abstract void send(String msg);
    public abstract void receive(String msg);

}
interface ChatMediator {
     void sendMessage(String msg, User user);
     void addUser(User user);
}
class ChatMediatorImpl implements ChatMediator {
    private List<User> users;

    public ChatMediatorImpl() {
        this.users = new ArrayList<>();
    }

    @Override
    public void sendMessage(String msg, User user) {
         for(User u : this.users){
             if(u != user){
                 u.receive(msg);
             }
         }
    }

    @Override
    public void addUser(User user) {
          this.users.add(user);
    }
}

class UserImpl extends User {


    public UserImpl(ChatMediator mediator, String name) {
        super(mediator, name);
    }

    @Override
    public void send(String msg) {
        System.out.println(this.name+": Sending Message="+msg);
         mediator.sendMessage(msg,this);
    }

    @Override
    public void receive(String msg) {
        System.out.println(this.name+": Received Message:"+msg);
    }
}

//  Observer Design Pattern
interface Subject{
   //methods to register and unregister observers
   public void  register(ObserverInt obj);
   public void  unregister(ObserverInt obj);

   //method to  notify observers of change
    public void  notifyObservers();

    //method to get updates from subject
    public Object getUpdate(ObserverInt obj);

}

interface ObserverInt{
     //method to update the observer, used by subject
    public void update();

    //attach with subject to observe
    public void setSubject(Subject sub);

}
class MyTopic implements Subject {
      private List<ObserverInt> observerList;
      private String massage;
      private boolean changed;
      private final Object MUTEX = new Object();

      public MyTopic(){
         this.observerList = new ArrayList<>();
      }

    @Override
    public void register(ObserverInt obj) {
         if(obj == null)throw new NullPointerException("Null Observer");
         synchronized (MUTEX){
            if(!observerList.contains(obj))observerList.add(obj);
         }
    }
    
    @Override
    public void unregister(ObserverInt obj) {
        synchronized (MUTEX){
            observerList.remove(obj);
        }
    }
    

    @Override
    public void notifyObservers() {
        List<ObserverInt> observers = null;
        synchronized (MUTEX){
            if(!changed) return;
            observers = new ArrayList<>(this.observerList);
            this.changed = false;
        }
        for(ObserverInt obj : observers){
            obj.update();
        }
    }


    @Override
    public Object getUpdate(ObserverInt obj) {
        return this.massage;
    }

    //method to post message to the topic
    public void postMessage(String msg){
        System.out.println("Message Posted to Topic:"+msg);
        this.massage=msg;
        this.changed=true;
        notifyObservers();
    }

}

class MyTopicSubscriber implements ObserverInt {
    private String name;
    private Subject topic;

    public MyTopicSubscriber(String name){
        this.name = name;
    }

    @Override
    public void update() {
         String msg = (String) topic.getUpdate(this);
             if(msg ==null) System.out.println(name+":: No new message");
             else System.out.println(name+":: Consuming message::"+msg);
    }

    @Override
    public void setSubject(Subject sub) {
           this.topic =sub;
    }


}


    public class Test {

 public static void main(String[] args) {

  //using template method
     System.out.println("Template Method Design Pattern");
    HouseTemplate houseTemplate1 = new WoodenHouse();
    houseTemplate1.buildHouse();
    System.out.println("************");
    HouseTemplate houseTemplate2 = new GlassHouse();
    houseTemplate2.buildHouse();


     //Strategy Design Pattern
     System.out.println("************");
     System.out.println("Strategy Design Pattern");
     ShoppingCart cart = new ShoppingCart();
     Item item1 = new Item("12314",10);
     Item item2 = new Item("42314",20);

     cart.addItem(item1);
     cart.addItem(item2);
     cart.pay(new PaypalStrategy("arun@gmail.com","mypwd"));
     cart.pay(new CreditCardStrategy("arun","6734266288297","886","03/29"));

     //// State Design Pattern
     System.out.println("************");
     System.out.println("State Design Pattern");
     TVContext context = new TVContext();
     State tvStartState = new TVStartState();
     State tvStopState  = new TVStopState();
     context.setTvState(tvStartState);
     context.doAction();

     context.setTvState(tvStopState);
     context.doAction();


    //Mediator Design Pattern
     System.out.println("************");
     System.out.println("Mediator Design Pattern");
     ChatMediator chatMediator = new ChatMediatorImpl();
      User user1 = new UserImpl(chatMediator,"Arun");
      User user2 = new UserImpl(chatMediator,"Lisa");
      User user3 = new UserImpl(chatMediator,"shivam");
      User user4 = new UserImpl(chatMediator,"Satyam");
      chatMediator.addUser(user1);
      chatMediator.addUser(user2);
      chatMediator.addUser(user3);
      chatMediator.addUser(user4);
      user1.send("Hi All");


     //Observer Design Pattern
     System.out.println("************");
     System.out.println("Observer Design Pattern");
     MyTopic topic = new MyTopic();
     ObserverInt o1 = new MyTopicSubscriber("o1");
     ObserverInt o2 = new MyTopicSubscriber("o2");
     ObserverInt o3 = new MyTopicSubscriber("o3");
     topic.register(o1);
     topic.register(o2);
     topic.register(o3);
     o1.setSubject(topic);
     o2.setSubject(topic);
     o3.setSubject(topic);

     o1.update();

     topic.postMessage("new Massage");

 }
}                  