package DesignPatterns.Creational.BuilderDesignPattern.BuilderDPExample;

public abstract class CD implements Packing {
    public abstract String pack();
}