package DesignPatterns.Creational.BuilderDesignPattern.BuilderDPExample;

public class Sony extends Company{
    @Override
    public int price() {
        return 20;
    }

    @Override
    public String pack() {
        return "Sony CD";
    }
}