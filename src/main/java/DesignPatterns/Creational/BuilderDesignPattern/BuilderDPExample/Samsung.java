package DesignPatterns.Creational.BuilderDesignPattern.BuilderDPExample;

public class Samsung extends Company {
    @Override
    public int price() {
        return 15;
    }

    @Override
    public String pack() {
        return "Samsung CD";
    }
}
