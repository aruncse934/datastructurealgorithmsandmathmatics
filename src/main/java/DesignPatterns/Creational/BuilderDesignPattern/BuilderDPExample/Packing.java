package DesignPatterns.Creational.BuilderDesignPattern.BuilderDPExample;

public interface Packing {
    public String pack();
    public int price();
}