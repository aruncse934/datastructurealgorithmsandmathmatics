package DesignPatterns.Creational.BuilderDesignPattern.BuilderDPExample;

public abstract class Company extends CD {
    public abstract int price();
}
