package DesignPatterns.Creational.BuilderDesignPattern.PizzaColdDrinkBuilderExample;

public abstract class Pizza implements Item {
  public abstract float price();
}