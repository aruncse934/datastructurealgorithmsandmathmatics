package DesignPatterns.Creational.BuilderDesignPattern.PizzaColdDrinkBuilderExample;

public abstract class Coke extends ColdDrink {

    @Override
    public abstract String name();

    public abstract String size();

    public abstract float price();
}
