package DesignPatterns.Creational.BuilderDesignPattern.PizzaColdDrinkBuilderExample;

public interface Item {
    public String name();
    public String size();
    public float price();
}

