package DesignPatterns.Creational.BuilderDesignPattern.PizzaColdDrinkBuilderExample;

public abstract class ColdDrink implements Item {
    public abstract float price();
}
