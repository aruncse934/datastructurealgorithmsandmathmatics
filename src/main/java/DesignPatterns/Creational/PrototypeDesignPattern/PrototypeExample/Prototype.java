package DesignPatterns.Creational.PrototypeDesignPattern.PrototypeExample;

public interface Prototype {
    public Prototype getClone();
}
