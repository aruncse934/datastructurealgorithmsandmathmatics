package DesignPatterns.Creational.AbstractFactoryPattern.BankLoanPaymentExample;

//Create concrete classes that extend the loan abstract class
public class HomeLoan extends Loan {
    @Override
    void getInterestRate(double r) {
        rate =r;
    }
}