package DesignPatterns.Creational.AbstractFactoryPattern.BankLoanPaymentExample;
//Create a Bank interface
interface  Bank {
    String getBankName();
}

