package DesignPatterns.Creational.FactoryMethodPattern.CalculateElectricityBillExample;

//Create a Plan abstract class.
public abstract class Plan {
    protected double rate;
    abstract void getRate();

    public void calculateBill(int units){
        System.out.println( units*rate );
    }
}
