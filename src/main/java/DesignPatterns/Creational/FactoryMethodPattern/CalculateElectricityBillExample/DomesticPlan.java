package DesignPatterns.Creational.FactoryMethodPattern.CalculateElectricityBillExample;
//Create the concrete classes that extends Plan abstract class.
public class DomesticPlan extends Plan{
    @Override
   public void getRate() {
        rate = 3.50;
    }
}
