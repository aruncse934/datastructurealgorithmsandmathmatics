package DesignPatterns.Creational.FactoryMethodPattern.CalculateElectricityBillExample;

//Create a GetPlanFactory to generate Object of Concrete Classes based on given information.
public class GetPlanFactory {
    public Plan getPlan(String planType){
        if (planType == null){
            return null;
        }
        if(planType.equalsIgnoreCase("DOMESTICPLAN")){
            return new DomesticPlan();
        }
        else if(planType.equalsIgnoreCase("COMMERCIALPLAN")){
            return new CommercialPlan();
        }
        else if(planType.equalsIgnoreCase("INSTITUTIONALPLAN")){
            return new InstitutionalPlan();
        }
        return null;
    }
}
