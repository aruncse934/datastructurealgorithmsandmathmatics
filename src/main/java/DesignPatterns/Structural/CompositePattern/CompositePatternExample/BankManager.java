package DesignPatterns.Structural.CompositePattern.CompositePatternExample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//This is the BankManager class i.e. Composite.
public class BankManager implements Employee{
    private int id;
    private String name;
    private double salary;

    public BankManager(int id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    List<Employee> employeeList = new ArrayList<Employee>();


    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public void add(Employee employee) {

        employeeList.add(employee);
    }

    @Override
    public void remove(Employee employee) {

        employeeList.remove(employee);
    }

    @Override
    public Employee getChild(int i) {
        return employeeList.get(i);
    }
    @Override
    public void print() {
        System.out.println("==========================");
        System.out.println("Id ="+getId());
        System.out.println("Name ="+getName());
        System.out.println("Salary ="+getSalary());
        System.out.println("==========================");

        Iterator<Employee> it  =  employeeList.iterator();
           while (it.hasNext()){
               Employee employee = it.next();
               employee.print();
           }
    }
}
