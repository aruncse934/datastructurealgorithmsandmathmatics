package DesignPatterns.Structural.FacadePattern.FacadePatternExample;

public class Iphone implements MobileShop {
    @Override
    public void modelNo() {
        System.out.println("IPhone 7");
    }

    @Override
    public void price() {
        System.out.println(" RS 65000.00 ");
    }
}
