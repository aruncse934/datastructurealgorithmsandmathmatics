package DesignPatterns.Structural.FacadePattern.FacadePatternExample;

public interface MobileShop {
    public void modelNo();
    public void price();
}
