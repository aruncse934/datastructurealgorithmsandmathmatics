package DesignPatterns.Structural.FacadePattern.FacadePatternExample;

public class BlackBerry  implements MobileShop{
    @Override
    public void modelNo() {
        System.out.println(" BlackBerry Z10 ");
    }

    @Override
    public void price() {
        System.out.println(" 55000.00");
    }
}
