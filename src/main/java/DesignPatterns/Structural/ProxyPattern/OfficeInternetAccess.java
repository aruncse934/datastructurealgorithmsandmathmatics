package DesignPatterns.Structural.ProxyPattern;

public interface OfficeInternetAccess {
    public void grantInternetAccess();
}
