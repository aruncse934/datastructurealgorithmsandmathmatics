package DesignPatterns.Structural.ProxyPattern.ProxyPatternExample;

import DesignPatterns.Structural.ProxyPattern.OfficeInternetAccess;

public class ProxyInternetAccess implements OfficeInternetAccess {
    public ProxyInternetAccess(String employeeName) {
        this.employeeName = employeeName;
    }

    private String employeeName;
    private RealInternetAccess realInternetAccess;

    @Override
    public void grantInternetAccess() {
        if(getRole(employeeName) > 4){
            realInternetAccess = new RealInternetAccess(employeeName);
            realInternetAccess.grantInternetAccess();
        }else {
            System.out.println("No Internet Access Granted. your job level is below 5");
        }
    }

    private int getRole(String employeeName) {
        //Check role from the database based on Name and designation return job level or job designation.
        return 9;
    }
}
