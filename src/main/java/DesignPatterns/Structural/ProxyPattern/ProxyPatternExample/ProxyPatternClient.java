package DesignPatterns.Structural.ProxyPattern.ProxyPatternExample;

import DesignPatterns.Structural.ProxyPattern.OfficeInternetAccess;

public class ProxyPatternClient {
    public static void main(String[] args) {
        OfficeInternetAccess access = new ProxyInternetAccess("Arun Gupta");
        access.grantInternetAccess();
    }
}
