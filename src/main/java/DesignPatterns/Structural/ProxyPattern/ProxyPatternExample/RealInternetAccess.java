package DesignPatterns.Structural.ProxyPattern.ProxyPatternExample;

import DesignPatterns.Structural.ProxyPattern.OfficeInternetAccess;

public class RealInternetAccess implements OfficeInternetAccess {
    public RealInternetAccess(String employeeName) {
        this.employeeName = employeeName;
    }

    private String employeeName;

    @Override
    public void grantInternetAccess() {
        System.out.println("Internet Access granted for employee: "+ employeeName);
    }
}
