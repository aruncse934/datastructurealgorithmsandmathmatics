package DesignPatterns.Structural.DecoratorPattern.DecoratorPatternExample;

public interface Food {
    public String prepareFood();
    public double foodPrice();
}
