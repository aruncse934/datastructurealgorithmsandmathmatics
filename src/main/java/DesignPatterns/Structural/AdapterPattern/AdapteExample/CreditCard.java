package DesignPatterns.Structural.AdapterPattern.AdapteExample;

public interface CreditCard {
    public void giveBankDetails();
    public String getCreditCard();

}
