package DesignPatterns.Structural.BridgePattern.BridgeExample;

public class BridgePatternDemo {
    public static void main(String[] args) {
        QuestionFormat question = new QuestionFormat("Java Programming Language");
        question.q = new JavaQuestions();
        question.delete("What is class? ");
        question.display();

        question.newOne("what is inheritance?");
        question.newOne("how many types of inheritance are there in java?");
        question.displayAll();
    }
}
