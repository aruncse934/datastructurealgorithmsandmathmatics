package Z_InterviewQ;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

/*
i/p=
3
1 62
1 49
2 -1

 o/p = 49
*/
public class Test {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int Q = Integer.parseInt(br.readLine().trim());
        int[][] query = new int[Q][2];
        for(int i_query = 0; i_query < Q; i_query++)
        {
            String[] arr_query = br.readLine().split(" ");
            for(int j_query = 0; j_query < arr_query.length; j_query++)
            {
                query[i_query][j_query] = Integer.parseInt(arr_query[j_query]);
            }
        }

        int[] out_ = elements(Q, query);
        System.out.print(out_[0]);
        for(int i_out_ = 1; i_out_ < out_.length; i_out_++)
        {
            System.out.print(" " + out_[i_out_]);
        }

        wr.close();
        br.close();
    }
    private static class Node{
        int x,y,v;
        Node(int x, int y, int v){
            this.x = x; this.y = y; this.v = v;
        }
    }
    static int[] elements(int k, int[][] M){
        PriorityQueue<Node> q = new PriorityQueue<>(Comparator.comparing((Node n)->n.v ));
        int count = 0;
        int[] dx = {0,1}, dy = {1,0};
        int ans = 0;
        int n = M.length, m = M[0].length;
        boolean[][] visited = new boolean[n][m];
        q.add(new Node(0,0,M[0][0] ));
        visited[0][0] = true;
        while(count < k){
            Node p = q.poll();
            for(int i= 0; i<2;i++){
                int x = p.x+dx[i];
                int y = p.y+dy[i];
                if(x>=0 && x<n && y>=0 && y<m && !visited[x][y]){
                    q.offer(new Node(x, y, M[x][y]));
                    visited[x][y] =true;
                }
            }
            ans = p.v;
            count++;
        }
        int res[] = {0};
        return res;
    }
        }
   /* public int smallestDistancePair(int[] nums, int k) {
        // write your code here
        Arrays.sort(nums);
        int n = nums.length, low = 0, hi = nums[n-1] - nums[0];
        while (low < hi) {
            int cnt = 0, j = 0, mid = (low + hi)/2;
            for (int i = 0; i < n; ++i) {
                while (j < n && nums[j] - nums[i] <= mid) ++j;
                cnt += j - i-1;
            }
            if (cnt >= k)
                hi = mid;

            else low = mid + 1;
        }

        return low;
    }*/
