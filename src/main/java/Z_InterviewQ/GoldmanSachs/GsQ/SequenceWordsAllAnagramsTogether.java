package Z_InterviewQ.GoldmanSachs.GsQ;

import java.util.*;

//https://www.geeksforgeeks.org/given-a-sequence-of-words-print-all-anagrams-together/
//Or Group Anagrams
public class SequenceWordsAllAnagramsTogether {
     public static List<List<String>> groupAnagrams(String[] strs) {
        if (strs.length == 0) return new ArrayList();
        Map<String, List> ans = new HashMap<String, List>();
        for (String s : strs) {
            char[] ca = s.toCharArray();
            Arrays.sort(ca);
            String key = String.valueOf(ca);
            if (!ans.containsKey(key)) ans.put(key, new ArrayList());
            ans.get(key).add(s);
        }
        return new ArrayList(ans.values());
    }
    private static void printAnagrams(String arr[]){
        Map<String, List<String>> map = new HashMap<>();
        for(int i = 0; i< arr.length;i++){
            String word = arr[i];
            char[] letters = word.toCharArray();
            Arrays.sort(letters);
            String newWord = new String(letters);
            if(map.containsKey(newWord)){
                map.get(newWord).add(word);
            }else {
                List<String> words = new ArrayList<>();
                words.add(word);
                map.put(newWord,words);
            }
        }
        for(String s: map.keySet()){
            List<String> values = map.get(s);
            if (values.size() > 1){
                System.out.print(values);
            }
        }
    }

    public static void main(String[] args) {
        String arr[] = {"cat", "dog", "tac", "god", "act"};//
        printAnagrams(arr);//[["bat"],["nat","tan"],["ate","eat","tea"]]
        System.out.println("===============");
        System.out.println(groupAnagrams(arr));
    }
}

