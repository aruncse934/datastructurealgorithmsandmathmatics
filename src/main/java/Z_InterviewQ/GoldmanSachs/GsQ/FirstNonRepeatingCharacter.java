package Z_InterviewQ.GoldmanSachs.GsQ;

import java.util.HashMap;

public class FirstNonRepeatingCharacter {
    public static int firstUniqChar(String s) {
        HashMap<Character, Integer> count = new HashMap<Character, Integer>();
        int n = s.length();
        // build hash map : character and how often it appears
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            count.put(c, count.getOrDefault(c, 0) + 1);
        }

        // find the index
        for (int i = 0; i < n; i++) {
            if (count.get(s.charAt(i)) == 1)
                return i;
        }
        return -1;
    }
/*static final int noChars = 256;
static char count[] = new char[noChars];
static void getCharCountArray(String str){
    for(int i = 0; i < str.length(); i++)
        count[str.charAt(i)]++;
}
static int firstNonRepeating(String str){
    getCharCountArray(str);
    int index = -1, i;
    for(i = 0; i<str.length();i++){
        if(count[str.charAt(i)] == 1){
            index = i;
            break;
        }
    }
    return index;
}*/
public static void main(String[] args){
    String str = "loveleetcode";
    int index = firstUniqChar(str);
    System.out.println(index == -1
                    ? "Either all characters are repeating or string "
                    + "is empty"
                    : "First non-repeating character is "
                    + str.charAt(index));
}
}
/*
    public static void main(String[] args)
    {
        String str = "geeksforgeeks";
        int index = firstNonRepeating(str);

        System.out.println(
                index == -1
                        ? "Either all characters are repeating or string "
                        + "is empty"
                        : "First non-repeating character is "
                        + str.charAt(index));
    }
} */