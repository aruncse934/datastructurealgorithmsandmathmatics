package Z_InterviewQ.GoldmanSachs.GsQ;

import java.util.*;

public class SortElementsArrayFrequency {
    private static void sortArrayElementsByFrequency(int[] inputArray) {
        //Create LinkedHashMap with elements as keys and their occurrences as values
        //Remember LinkedHashMap maintains insertion order of elements

        Map<Integer, Integer> elementCountMap = new LinkedHashMap<>();

        //Check presence of each element in elementCountMap

        for (int i = 0; i < inputArray.length; i++) {
            if (elementCountMap.containsKey(inputArray[i])) {
                //If element is present in elementCountMap, increment its value by 1

                elementCountMap.put(inputArray[i], elementCountMap.get(inputArray[i]) + 1);
            } else {
                //If element is not present, insert this element with 1 as its value

                elementCountMap.put(inputArray[i], 1);
            }
        }

        //Create an ArrayList to hold sorted elements

        ArrayList<Integer> sortedElements = new ArrayList<>();

        //Java 8 code to sort elementCountMap by values in reverse order
        //and put keys into sortedElements list

        elementCountMap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .forEach(entry -> {
                    for (int i = 1; i <= entry.getValue(); i++)
                        sortedElements.add(entry.getKey());
                });

        //Printing sorted array elements in descending order of their frequency

        System.out.println("Input Array :" + Arrays.toString(inputArray));

        System.out.println("Sorted Array Elements In Descending Order Of their Frequency :");

        System.out.println(sortedElements);
    }

    public static void main(String[] args) {
        sortArrayElementsByFrequency(new int[]{9,9 ,9, 2 ,5});
        /*2
5
5 5 4 6 4
5
9 9 9 2 5*/
    }


    /*public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t > 0) {
            int n = sc.nextInt();
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            Arrays.sort(a);
            HashMap<Integer, Integer> hash = new HashMap<Integer, Integer>();
            for (int i = 0; i < n; i++) {
                if (hash.containsKey(a[i]))
                    hash.put(a[i], hash.get(a[i]) + 1);
                else
                    hash.put(a[i], 1);
            }
            Set<Integer> s = hash.keySet();
            int ne = s.size();
            Integer arr[] = new Integer[ne];
            arr = s.toArray(arr);
            int temp = 0;
            for (int j = 0; j < arr.length - 1; j++) {
                for (int i = 0; i < arr.length - 1; i++) {
                    if (hash.get(arr[i]) < hash.get(arr[i + 1])) {
                        temp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = temp;
                    }
                }
            }
            for (int i = 0; i < ne; i++) {
                for (int j = 0; j < hash.get(arr[i]); j++)
                    System.out.print(arr[i] + " ");
            }
            t--;
            System.out.println();
        }
    }*/

    public static StringBuffer sortByFrequency(int arr1[], int l1) {
        // Build a map of array elements to its count
        Map<Integer, Integer> countMap = getCountMap(arr1, l1);
        StringBuffer result = new StringBuffer();

        // Sort the map using a comparingByValue comparator
        // build the result by appending keys the count times to the result

        countMap.entrySet().stream()
                .sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                .forEach(e -> {
                    int key = e.getKey();
                    int val = e.getValue();
                    for (int i = 0; i < val; i++) {
                        result.append(key + " ");
                    }
                });

        return result;
    }

    // helper function to return the element count map
    private static Map<Integer, Integer> getCountMap(int[] arr, int l1) {
        Map<Integer, Integer> countMap = new LinkedHashMap<>();
        for (int i = 0; i < l1; i++) {
            if (countMap.containsKey(arr[i])) {
                countMap.put(arr[i], countMap.get(arr[i]) + 1);
            } else {
                countMap.put(arr[i], 1);
            }
        }
        return countMap;
    }

    // Driver program
   /* public static void main(String[] args) throws IOException {
            int a[] = { 7, 1, 3, 4, 7, 1, 7, 1, 4, 5, 1, 9, 3};//{8, 8, 8, 2, 2, 5, 5, 6}
            System.out.println(sortByFrequency(a, a.length));*/
      /*  Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t > 0) {
            int n = sc.nextInt();
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            System.out.println(sortByFrequency(a, a.length));
        }*/
    }
//}
    /*5 5 4 6 4
5
9 9 9 2 5

Output:
4 4 5 5 6
9 9 9 2 5*/

