package Z_InterviewQ.GoldmanSachs.GsQ;

public class RunLengthEncoding {
   /* public static void printRLE(String str){
        int n = str.length();
        for(int i = 0; i < n; i++){
            int count = 1;
            while (i<n-1 && str.charAt(i)==str.charAt(i+1)){
                count++;
                i++;
            }
            System.out.print(count);
            System.out.print(str.charAt(i));

        }

    }

    public static String encode(String s) {
       int i = 0;
       int j = 0;
       StringBuilder encoded = new StringBuilder();
       while (j < s.length()) {
           if (s.charAt(i) != s.charAt(j)) {
               encoded.append(j - i).append(s.charAt(i));
               i = j;
           }
           j++;
           if (j == s.length())
               encoded.append(j - i).append(s.charAt(i));
       }
       return encoded.toString();
   }
    public static String decode(String s) {
        StringBuilder decoded = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            int n = 0;
            while (Character.isDigit(ch)) {
                n = (n * 10) + Character.digit(ch, 10);
                ch = s.charAt(++i);
            }
            StringBuilder append = decoded.append(String.valueOf(ch).repeat(n));
        }
        return decoded.toString();
    }


    public static void main(String[] args) {
        String str = "WWWWWWWWWBBBBBBB";
        printRLE(str);
        System.out.println("+++++++");
        System.out.print(encode(str));
        System.out.print(encode("WWWWWWWWWBBBBBBB").equals("9W7B"));
        System.out.print(encode("WWWWWWWWWBBBBBBBR").equals("9W7B1R"));
        System.out.print(encode("W").equals("1W"));
        System.out.print(encode(""));
        System.out.println(decode("9W7B").equals("WWWWWWWWWBBBBBBB"));
        System.out.println(decode("1W7B").equals("WBBBBBBB"));
        System.out.println(decode("9W1B").equals("WWWWWWWWWB"));
        System.out.println(decode("1W1B").equals("WB"));
        System.out.println(decode("0W1B").equals("B"));
        System.out.println(decode("1W0B").equals("W"));
        System.out.println(decode("0W0B").equals(""));
        System.out.println(decode("1A1B1C").equals("ABC"));
        System.out.println(decode("1A1B").equals("AB"));
    }*/
}