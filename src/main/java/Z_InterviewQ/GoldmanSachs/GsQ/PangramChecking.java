package Z_InterviewQ.GoldmanSachs.GsQ;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PangramChecking {
    private static final int ALPHABET_COUNT = 26;
    public static boolean isPerfectPangram(String str) {
        if (str == null)
            return false;

        // filtered character stream
        String strUpper = str.toUpperCase();
        Stream<Character> filteredCharStream = strUpper.chars()
                .filter(item -> ((item >= 'A' && item <= 'Z')))
                .mapToObj(c -> (char) c);
        Map<Character, Long> alphabetFrequencyMap = filteredCharStream.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return (alphabetFrequencyMap.size() == ALPHABET_COUNT && alphabetFrequencyMap.values()
                .stream()
                .allMatch(item -> item == 1));
    }

   /* public static void main(String[] args) {
        String str = "The quick brown fox jumps over the lazy dog";

        if (isPerfectPangram(str) == true)
            System.out.print(str + " is a pangram.");
        else
            System.out.print(str + " is not a pangram.");

    }*/
    public static void main(String[] args){
        String str = "The quick brown fox jumps over the lazy dogs";
        if(checkPangram(str) == true)
            System.out.println(str + " is a pangram.");
        else
            System.out.println(str + " is not a pangram.");
    }
    public static boolean checkPangram(String str){
        boolean[] mark = new boolean[26];
        int index = 0;
        for(int i = 0; i< str.length();i++){
            if('A' <= str.charAt(i) && str.charAt(i) <= 'Z')
                index = str.charAt(i) - 'A';
            else if('a' <= str.charAt(i) && str.charAt(i) <= 'z')
                index = str.charAt(i) -'a';
            else
                continue;
            mark[index] = true;
        }
        for(int i = 0; i<= 25;i++)
            if(mark[i] == false)
                return (false);
            return (true);
    }
}