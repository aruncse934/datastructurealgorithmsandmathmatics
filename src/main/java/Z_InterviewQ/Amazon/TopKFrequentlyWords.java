package Z_InterviewQ.Amazon;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*Example 1:
Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
Output: ["i", "love"]
Explanation: "i" and "love" are the two most frequent words.
    Note that "i" comes before "love" due to a lower alphabetical order.
Example 2:
Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
Output: ["the", "is", "sunny", "day"]
Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
    with the number of occurrence being 4, 3, 2 and 1 respectively.*/
public class TopKFrequentlyWords {

    public List<String> topKFrequent(String[] words, int k) {

        List<String> result = new LinkedList<>();
        Map<String, Integer> map = new HashMap<>();
        for(int i=0; i<words.length; i++)
        {
            if(map.containsKey(words[i]))
                map.put(words[i], map.get(words[i])+1);
            else
                map.put(words[i], 1);
        }

        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(
                (a,b) -> a.getValue()==b.getValue() ? b.getKey().compareTo(a.getKey()) : a.getValue()-b.getValue()
        );

        for(Map.Entry<String, Integer> entry: map.entrySet())
        {
            pq.offer(entry);
            if(pq.size()>k)
                pq.poll();
        }

        while(!pq.isEmpty())
            result.add(0, pq.poll().getKey());

        return result;
    }
    //
    public static List<String> topMentioned(int k, List<String> keywords, List<String> reviews) {
        Pattern patt = Pattern.compile("\\b(:?" + String.join("|", keywords) + ")\\b", Pattern.CASE_INSENSITIVE);

        Map<String, Integer> counts = new HashMap<>();
        for (String review : reviews) {
            Matcher m = patt.matcher(review);
            HashSet<String> words = new HashSet<>();
            while (m.find())
                words.add(m.group(0).toLowerCase());
            for (String word : words)
                counts.merge(word, 1, Integer::sum);
        }
        PriorityQueue<Map.Entry<Integer, String>> queue = new PriorityQueue<>((a, b) -> {
            if (a.getKey() != b.getKey())
                return Integer.compare(a.getKey(), b.getKey());
            return -a.getValue().compareTo(b.getValue());
        });
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
        //    queue.offer(Map.entry(entry.getValue(), entry.getKey()));
            if (queue.size() > k)
                queue.poll();
        }
        ArrayList<String> res = new ArrayList<>();
        while (!queue.isEmpty())
            res.add(queue.poll().getValue());
        Collections.reverse(res);
        return res;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = Integer.parseInt(scanner.nextLine());
        int keywordsLength = Integer.parseInt(scanner.nextLine());
        List<String> keywords = new ArrayList<>();
        for (int i = 0; i < keywordsLength; i++) {
            keywords.add(scanner.nextLine());
        }
        int reviewsLength = Integer.parseInt(scanner.nextLine());
        List<String> reviews = new ArrayList<>();
        for (int i = 0; i < reviewsLength; i++) {
            reviews.add(scanner.nextLine());
        }
        scanner.close();
        List<String> res = topMentioned(k, keywords, reviews);
        System.out.println(String.join(" ", res));
    }
}
