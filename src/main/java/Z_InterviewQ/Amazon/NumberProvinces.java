package Z_InterviewQ.Amazon;

/*
There are n cities. Some of them are connected, while some are not. If city a is connected directly with city b, and city b is connected directly with city c, then city a is connected indirectly with city c.

A province is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth city are directly connected, and isConnected[i][j] = 0 otherwise.

Return the total number of provinces.


Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]]
Output: 2

Example 2:


Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]]
Output: 3
*/
public class NumberProvinces {
    public static void main(String[] args) {

        int [][]arr = {{1,1,0},{1,1,0},{0,0,1}};
        System.out.println(findCircleNum(arr));
    }

    public static void dfs(int[][] M, int[] visited, int i) {
        for (int j = 0; j < M.length; j++) {
            if (M[i][j] == 1 && visited[j] == 0) {
                visited[j] = 1;
                dfs(M, visited, j);
            }
        }
    }
    public static int findCircleNum(int[][] M) {
        int[] visited = new int[M.length];
        int count = 0;
        for (int i = 0; i < M.length; i++) {
            if (visited[i] == 0) {
                dfs(M, visited, i);
                count++;
            }
        }
        return count;
    }
}

/* public int findCircleNum(int[][] M) {
        int len = M.length;
        boolean[] set = new boolean[len];
        int count = 0;

        for(int i=0;i<len;i++){
            if(!set[i]){
                count++;
                dp(M,i,i,set);
            }
        }

        return count;
    }

    public void dp(int[][] M,int loopstart,int start,boolean[] set){
        for(int j=loopstart;j<M.length;j++){
            if(M[start][j]==1&&!set[j]){
                set[j] = true;
                dp(M,loopstart,j,set);
            }
        }
    }*/