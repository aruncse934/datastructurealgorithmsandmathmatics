package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/*Example 1:
Input:
items_prices = [2, 3, 1, 2, 4, 2]
Output:
8
2 5
Explanation:
The total cost is 1+2+1+0+2+2 = 8 units. And 2 and 5 indexes has no discount.
Examples
Example 2:
Input:
items_prices = [5, 1, 3, 4, 6, 2]
Output:
14
1 5
Examples
Example 3:
Input:
items_prices = [1, 3, 3, 2, 5]
Output:
9
0 3 4

public static List<List> shopkeeperSale(int[] items_prices){}*/
public class ShopkeeperSale {
    public static void finalPrice(int[] prices) {
        int total = 0;
        Stack<Integer> s = new Stack<>();
        for (int i = 0; i < prices.length; i++) {
            while (!s.isEmpty() && prices[s.peek()] >= prices[i]) {
                total += prices[s.pop()] - prices[i];
            }
            s.push(i);
        }
        List<Integer> res = new ArrayList<>();
        while (!s.isEmpty()) {
            int idx = s.pop();
            total += prices[idx];
            res.add(0, idx);
        }
        System.out.println(total);
        System.out.println(res);
    }
    public static void main(String[] args) {

        int[] prices = {5, 1, 3, 4, 6, 2};
        finalPrice(prices);
    }
}
