package Z_InterviewQ.Amazon;

import java.util.Deque;
import java.util.LinkedList;

public class KnightMoves {
    public static void main(String[] args) {
        System.out.println(knightMoves(new int[] {0,0}, 4));
    }

    private static int knightMoves(int[] startingPosition, int k){
        int[][] moves = {{1,2}, {2,1}, {1,-2}, {-1,2}, {-1,-2}, {-2,-1}, {2,-1}, {-2,1}};

        Deque<int[]> queue = new LinkedList<>();
        boolean[][] checked = new boolean[8][8];

        //Important to not mark first position as checked because next move can return to it and that will be a valid position.
        queue.add(startingPosition);
        int count = 0;

        while(k > 0){
            int size = queue.size();

            while(size-- > 0){
                int[] position = queue.removeFirst();

                for(int[] move : moves){
                    int x = position[0] + move[0];
                    int y = position[1] + move[1];

                    if(x < 0 || y < 0 || x > 7 || y > 7 || checked[x][y]){
                        continue;
                    }
                    count++;
                    checked[x][y] = true;
                    queue.addLast(new int[]{x,y});
                }
            }
            k--;
        }
        return count;
    }
}
