package Z_InterviewQ.Amazon;

import java.util.Stack;

/*Description
Given an array of integers A, find the sum of min(B), where B ranges over every (contiguous) subarray of A.

Since the answer may be large, return the answer modulo 10^9 + 7.

1 \leq A \leq 300001≤A≤30000
1 \leq A[i] \leq 300001≤A[i]≤30000
Example
Example 1:

Input: [3,1,2,4]
Output: 17
Explanation: Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4].
Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.  Sum is 17.
Example 2:

Input: [95,58,46,67,100]
Output: 859
Explanation: Subarrays are [95], [58], [46], [67], [100], [98,58], [58,46], [46,67], [67,100], [95,58,67],[46,67,100],[95,58,46,67],[58,46,67,100],[95,58,46,67,100].
Minimums are 95, 58, 46, 67, 100, 58, 46, 46, 67, 46,

Example 2:

Input: arr = [11,81,94,43,3]
Output: 444*/
public class SumSubarrayMinimums {
    public static void main(String[] args) {

        int []A = {11,81,94,43,3};
        System.out.println(sumSubarrayMins(A));
    }

    public static int sumSubarrayMins(int[] A) {
        // Write your code here.
        Stack<Integer> s = new Stack<>();
        int n = A.length;
        long res = 0, mod = (long) (1e9 + 7);
        for(int i = 0; i <= n; i++){
            while (!s.isEmpty() && A[s.peek()] > (i == n ? 0 : A[i]) ){
                int j = s.pop();
                int k = s.isEmpty() ? -1 : s.peek();
                res = (res + A[j] * (i - j) * (j - k)) % mod;
            }
            s.push(i);
        }
        return (int) res;
    }
}
