package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.List;

/*Example 1:

Input: arr = [1,2,3,4,5], k = 4, x = 3
Output: [1,2,3,4]
Example 2:

Input: arr = [1,2,3,4,5], k = 4, x = -1
Output: [1,2,3,4]

*/
/*Input: A = [1, 2, 3], target = 2, k = 3
Output: [2, 1, 3]
Example 2:

Input: A = [1, 4, 6, 8], target = 3, k = 3
Output: [4, 1, 6]
*/
public class Find_K_ClosestElements {

    //Binary Search To Find The Left Bound
    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        // Initialize binary search bounds
        int left = 0;
        int right = arr.length - k;

        // Binary search against the criteria described
        while (left < right) {
            int mid = (left + right) / 2;
            if (x - arr[mid] > arr[mid + k] - x) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        // Create output in correct format
        List<Integer> result = new ArrayList<Integer>();
        for (int i = left; i < left + k; i++) {
            result.add(arr[i]);
        }

        return result;
    }


    public int[] kClosestNumbers(int[] A, int target, int k) {
        int start = 0, end = A.length-1;
        while (start+1 < end) {
            int mid = (start+end)/2;
            if (A[mid] >= target)
                end = mid;
            else
                start = mid;
        }
        int[] result = new int[k];
        int left = start, right = end;
        for (int i = 0; i < k; i++) {
            if (left >= 0 && right < A.length) {
                if (Math.abs(A[left]-target) <= Math.abs(A[right]-target)) {
                    result[i] = A[left];
                    left--;
                }
                else {
                    result[i] = A[right];
                    right++;
                }
            } else if (left >= 0 && right >= A.length) {
                result[i] = A[left];
                left--;
            }
            else if (left < 0 && right < A.length){
                result[i] = A[right];
                right++;
            }
        }

        return result;
    }
}
