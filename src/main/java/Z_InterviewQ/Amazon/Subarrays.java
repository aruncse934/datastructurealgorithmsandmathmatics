package Z_InterviewQ.Amazon;

import java.util.*;

/*You are given an array A, create an another array B all or subarrays of A.

[ Note - Subarrays B are formed by removing zero or more elements from the front or/and back of the original array without changing the order of the remaining element ]

Do a Bitwise OR on the subarrays and get the Kth smallest element

Inputs :
A = [1, 2, 3]
K = 6

Output:
3

Explanation:

Given array A = [1, 2, 3], generate a Array B using A
B = [ 1, (1, 2), (1, 2, 3), 2, (2, 3), 3]

Now do Bitwise OR among the subarrays, which gives you
B = [1, 3, 3, 2, 3, 3]

Atlast get the Kth smallest element of B, if K = 6 , from sorted B we will get
B=[1, 2, 3, 3, 3, 3]
6th smallest element is 3*/
public class Subarrays {
    public static void main(String[] args) {

        int arr[] = {1, 2, 3};
        int k =6;
       // System.out.println(KthsmallestElement(arr,k));
        System.out.println(KthsmallestElement(new int[]{1, 2, 3} , 6));
    }

   // public static List<Integer> KthsmallestElement(int[] A, int K) {
        public static int KthsmallestElement(int[] input , int k){
            //first is value and second is index
            PriorityQueue<int[]> pq = new PriorityQueue<>( ( a, b) -> (a[0] - b[0]) );

            for(int i = 0 ; i < input.length ; i++){
                pq.add(new int[]{input[i]  , i});
            }

            while(k > 1 &&  !pq.isEmpty()){
                k--;
                int[] min = pq.remove();
                System.out.printf("%d , %d \n" , min[0] , min[1]);
                if(min[1] < input.length -1){
                    pq.add(new int[]{ min[0] | input[min[1] + 1] , min[1] +1});
                }
            }

            if(k > 1 || pq.isEmpty()) return -1;
            return pq.remove()[0];

        }
}
