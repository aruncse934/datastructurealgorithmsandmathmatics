package Z_InterviewQ.Amazon;

import java.util.*;
/*Example:
Input:
logData:

[
[345366 89921 45],
[029323 38239 23],
[38239 345366 15],
[029323 38239 77],
[345366 38239 23],
[029323 345366 13],
[38239 38239 23]
...
]
threshold: 3

Output: [345366 , 38239, 029323]
Explanation:
Given the following counts of userids, there are only 3 userids that meet or exceed the threshold of 3.

345366 -4 , 38239 -5, 029323-3, 89921-1*/
public class TransactionLogs {
    static List<String> processLogs(List<String> logs, int threshold) {
        Map<String, Integer> map = new HashMap<>();
        for (String logLine : logs) {
            String[] log = logLine.split(" ");
            map.put(log[0], map.getOrDefault(log[0], 0) + 1);
            if (log[0] != log[1]) {
                map.put(log[1], map.getOrDefault(log[1], 0) + 1);
            }
        }

        List<String> userIds = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() >= threshold) {
                userIds.add(entry.getKey());
            }
        }

        Collections.sort(userIds,new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return Integer.parseInt(s1) - Integer.parseInt(s2);
            }
        });

        return userIds;
    }


    public static void main(String[] args) {
        List<String> input = new ArrayList() {{
            add ("345366 89921 45");
            add ("029323 38239 23");
            add ("38239 345366 15");
            add ("029323 38239 77");
            add ("345366 38239 23");
            add ("029323 345366 13");
            add ("38239 38239 23");
        }};

        processLogs(input, 2).forEach(System.out::println);
    }

    //approach
    private static List<String> transactionLogs(List<String> logs, int threshold){
        Map<String, Integer> userFreqMap = new HashMap<>();

        for(String log : logs){
            String[] tokens = log.split("\\s+");

            String userID1 = tokens[0];
            String userID2 = tokens[1];

            userFreqMap.put(userID1, userFreqMap.getOrDefault(userID1, 0) + 1);

            if(!userID1.equalsIgnoreCase(userID2)){
                userFreqMap.put(userID2, userFreqMap.getOrDefault(userID2, 0) + 1);
            }
        }

        List<String> result = new ArrayList<>();

        for(Map.Entry<String, Integer> entry : userFreqMap.entrySet()){
            if(entry.getValue() >= threshold){
                result.add(entry.getKey());
            }
        }

        Collections.sort(result, (n1, n2) ->
                Integer.compare(Integer.parseInt(n1), Integer.parseInt(n2)));
        return result;
    }
}
