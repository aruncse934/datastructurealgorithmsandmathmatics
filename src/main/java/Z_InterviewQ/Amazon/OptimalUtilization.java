package Z_InterviewQ.Amazon;

import java.util.*;

/*Example 1:

Input:
a = [[1, 2], [2, 4], [3, 6]]
b = [[1, 2]]
target = 7

Output: [[2, 1]]

Explanation:
There are only three combinations [1, 1], [2, 1], and [3, 1], which have a total sum of 4, 6 and 8, respectively.
Since 6 is the largest sum that does not exceed 7, [2, 1] is the optimal pair.
Example 2:

Input:
a = [[1, 3], [2, 5], [3, 7], [4, 10]]
b = [[1, 2], [2, 3], [3, 4], [4, 5]]
target = 10

Output: [[2, 4], [3, 2]]

Explanation:
There are two pairs possible. Element with id = 2 from the list `a` has a value 5, and element with id = 4 from the list `b` also has a value 5.
Combined, they add up to 10. Similarily, element with id = 3 from `a` has a value 7, and element with id = 2 from `b` has a value 3.
These also add up to 10. Therefore, the optimal pairs are [2, 4] and [3, 2].
Example 3:

Input:
a = [[1, 8], [2, 7], [3, 14]]
b = [[1, 5], [2, 10], [3, 14]]
target = 20

Output: [[3, 1]]
Example 4:

Input:
a = [[1, 8], [2, 15], [3, 9]]
b = [[1, 8], [2, 11], [3, 12]]
target = 20

Output: [[1, 3], [3, 2]]*/

/*示例1

输入:
A = [1, 4, 6, 9], B = [1, 2, 3, 4], K = 9
输出:
[2, 2]
示例2:

输入:
A = [1, 4, 6, 8], B = [1, 2, 3, 5], K = 12
输出:
[2, 3]*/
public class OptimalUtilization {

    //Two pointer (Lintcode )
    public int[] optimalUtilization(int[] A, int[] B, int K) {
        // write your code here
        int[] indexPair = new int[2];
        int[] nullPair = new int[0];
        int Aindex = 0, Bindex = B.length - 1;
        int ALen = A.length, BLen = B.length, maxSum = -1;
        if(ALen == 0 || BLen == 0||A[0] + B[0] > K) return nullPair;
        while(Aindex < ALen && Bindex >= 0){
            System.out.println(indexPair[0] + ", " + indexPair[1]);
            if(A[Aindex] + B[Bindex] < K){
                if(A[Aindex] + B[Bindex] > maxSum){
                    while(Bindex != 0 && B[Bindex - 1] == B[Bindex]) Bindex--;
                    indexPair[0] = Aindex;
                    indexPair[1] = Bindex;
                    maxSum = A[Aindex] + B[Bindex];
                }
                Aindex++;
            }
            else if(A[Aindex] + B[Bindex] > K) Bindex--;
            else{
                indexPair[0] = Aindex;
                indexPair[1] = Bindex;
                return indexPair;
            }
        }
        return indexPair;
    }

    // "static void main" must be defined in a public class.
        public static void closestSum(List<List<Integer>> listA, List<List<Integer>> listB, int target) {
            List<List<Integer>> result = new ArrayList<List<Integer>>();
            int diff = Integer.MAX_VALUE;
            for (List<Integer> lista : listA) {
                int first = lista.get(1);
                for (List<Integer> listb : listB) {
                    //System.out.println(result);
                    int second = listb.get(1);
                    if ((first+second) <=target ) {
                        if (target - (first+second) <  diff) {
                            result.clear();
                            result.add(new ArrayList<Integer>(Arrays.asList(lista.get(0), listb.get(0))));
                            diff = target - (first+second);
                        } else if (target - (first+second) ==  diff) {
                            result.add(new ArrayList<Integer>(Arrays.asList(lista.get(0), listb.get(0))));
                        }
                    }
                }
            }
            System.out.println("Output: " + result);
        }

        // approach
    private static List<int[]> getPairs(List<int[]> a, List<int[]> b, int target) {
        Collections.sort(a, (i,j) -> i[1] - j[1]);
        Collections.sort(b, (i,j) -> i[1] - j[1]);
        List<int[]> result = new ArrayList<>();
        int max = Integer.MIN_VALUE;
        int m = a.size();
        int n = b.size();
        int i =0;
        int j =n-1;
        while(i<m && j >= 0) {
            int sum = a.get(i)[1] + b.get(j)[1];
            if(sum > target) {
                --j;
            } else {
                if(max <= sum) {
                    if(max < sum) {
                        max = sum;
                        result.clear();
                    }
                    result.add(new int[]{a.get(i)[0], b.get(j)[0]});
                    int index = j-1;
                    while(index >=0 && b.get(index)[1] == b.get(index+1)[1]) {
                        result.add(new int[]{a.get(i)[0], b.get(index--)[0]});
                    }
                }
                ++i;
            }
        }
        return result;
    }
    public static void main(String[] args) {
            List<List<Integer>> listA = new ArrayList<List<Integer>>();
            List<List<Integer>> listB = new ArrayList<List<Integer>>();
            listA.add(new ArrayList<Integer>(Arrays.asList(1, 8)));
            listA.add(new ArrayList<Integer>(Arrays.asList(2, 15)));
            listA.add(new ArrayList<Integer>(Arrays.asList(3, 9)));
            // listA.add(new ArrayList<Integer>(Arrays.asList(4, 10)));
            listB.add(new ArrayList<Integer>(Arrays.asList(1, 8)));
            listB.add(new ArrayList<Integer>(Arrays.asList(2, 11)));
            listB.add(new ArrayList<Integer>(Arrays.asList(3, 12)));
            // listB.add(new ArrayList<Integer>(Arrays.asList(4, 5)));
            System.out.println("a = " + listA);
            System.out.println("b = " + listB);
            int target = 20;
            System.out.println("target = " + target);
            closestSum(listA, listB, target);

        System.out.println("-----");
       // System.out.println(getPairs(listA,listB,target));
        }

}
/*public static void main(String[] args) {
        int[][] a = {{1, 2}, {2, 4}, {3, 6}};
        int[][] b = {{1, 2}};

        int[][] c = {{1, 3}, {9, 5}, {2, 5}, {3, 7}, {4, 10}};
        int[][] d = {{1, 2}, {2, 3}, {3, 4}, {4, 5}, {7, 5}};

        List<int[]> resultAB = getPairs(a, b, 7);
        List<int[]> resultCD = getPairs(c, d, 10);

        for(int[] pair : resultAB){
            System.out.print("[" + pair[0] + "," + pair[1] + "]");
            System.out.println(" ");
        }

        for(int[] pair : resultCD){
            System.out.print("[" + pair[0] + "," + pair[1] + "]");
        }
    }
    private static List<int[]> getPairs(int[][] a, int[][] b, int target) {
        Arrays.sort(a, (a1, a2) -> a1[1] - a2[1]);
        Arrays.sort(b, (b1, b2) -> b1[1] - b2[1]);

        List<int[]> result = new ArrayList<>();

        int left = 0, right = b.length-1;
        int maxInResult = Integer.MIN_VALUE;

        while(left < a.length && right >= 0){
            int sum = a[left][1] + b[right][1];
            //Need to think about handling duplicates key vs values?
            if(sum <= target){
                if(maxInResult < sum){
                    result.clear();
                    maxInResult = sum;
                }
                result.add(new int[] {a[left][0], b[right][0]});
                left++;
            }
            else{
                right--;
            }
        }
        return result;
    }*/