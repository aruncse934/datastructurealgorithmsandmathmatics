package Z_InterviewQ.Amazon.SecondRound;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

public class RemoveDuplicateSmallPrime {

    //Hashing Technique
    public static void removeDuplicateSmallPrime(Vector<Integer> vector){
        Set<Integer> hashSet = new HashSet<>(vector);
        vector.clear();
        vector.addAll(hashSet);

    }
    public static void main(String[] args) {
        Integer arr[]= {3,5,7,2,2,5,7,7};
        Vector<Integer> vector = new Vector<>(Arrays.asList(arr));
        removeDuplicateSmallPrime(vector);
        for(int i= 0;i<vector.size();i++){
            System.out.print(vector.get(i)+" ");
        }
        System.out.println("\nStream api");
        vector.stream().collect(Collectors.toSet()).forEach(System.out::println);

    }
}
