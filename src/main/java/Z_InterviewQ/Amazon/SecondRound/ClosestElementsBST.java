package Z_InterviewQ.Amazon.SecondRound;

import org.w3c.dom.Node;

// Recursive Java program to find key closest to k in given Binary Search Tree.
public class ClosestElementsBST {

    static int min_diff, min_diff_key;
    static class Node {

        int key;
        Node left, right;
    };

    public static Node newNode(int key) {

        Node node = new Node();
        node.key = key;
        node.left  = node.right = null;
        return (node);
    }
    static void maxDiffUtil(Node ptr, int k) {

        if(ptr == null)
            return;
        if(ptr.key == k) {

            min_diff_key = k;
            return;
        }
        if(min_diff > Math.abs(ptr.key - k)) {

        }
    }
    public static void main(String[] args) {

    }
}

/*
static void maxDiffUtil(Node ptr, int k)
{
        if (min_diff > Math.abs(ptr.key - k))
        {
        min_diff = Math.abs(ptr.key - k);
        min_diff_key = ptr.key;
        }

        // if k is less than ptr.key then move in
        // left subtree else in right subtree
        if (k < ptr.key)
        maxDiffUtil(ptr.left, k);
        else
        maxDiffUtil(ptr.right, k);
        }

// Wrapper over maxDiffUtil()
static int maxDiff(Node root, int k)
        {
        // Initialize minimum difference
        min_diff = 999999999; min_diff_key = -1;

        // Find value of min_diff_key (Closest key
        // in tree with k)
        maxDiffUtil(root, k);

        return min_diff_key;
        }

// Driver program to run the case
public static void main(String args[])
        {

        Node root = newnode(9);
        root.left = newnode(4);
        root.right = newnode(17);
        root.left.left = newnode(3);
        root.left.right = newnode(6);
        root.left.right.left = newnode(5);
        root.left.right.right = newnode(7);
        root.right.right = newnode(22);
        root.right.right.left = newnode(20);
        int k = 18;
        System.out.println( maxDiff(root, k));

        }
        }
        */