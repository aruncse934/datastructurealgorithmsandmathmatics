package Z_InterviewQ.Amazon;

import java.util.*;

public class CostEvaluationOrWarehouse {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public int costEvaluation(int n, int[][] connections) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        //Create Graph
        for(int[] connection : connections){
            graph.putIfAbsent(connection[0], new ArrayList<Integer>());
            graph.putIfAbsent(connection[1], new ArrayList<Integer>());

            graph.get(connection[0]).add(connection[1]);
            graph.get(connection[1]).add(connection[0]);
        }

        boolean[] checked = new boolean[n];
        List<List<Integer>> warehouses = new ArrayList<>();
        //Create list of Groups.
        for(int warehouse=0; warehouse<n; warehouse++){
            if(!checked[warehouse]){
                List<Integer> connectedWarehouses = new ArrayList<>();
                checkWarehouse(warehouse, graph, checked, connectedWarehouses);

                warehouses.add(connectedWarehouses);
            }
        }
        //Calculate Cost
        int cost = 0;
        for(List<Integer> group : warehouses){
            if(group.size() == 1){
                cost++;
            }
            else{
                cost += Math.ceil(Math.sqrt(group.size()));
            }
        }
        return cost;
    }
    //DFS
    private void checkWarehouse(int warehouse, Map<Integer, List<Integer>> graph, boolean[] checked, List<Integer> connectedWarehouses){
        if(checked[warehouse]){
            return;
        }
        checked[warehouse] = true;
        connectedWarehouses.add(warehouse);

        for(int nextWarehouse : graph.getOrDefault(warehouse, new ArrayList<Integer>())){
            checkWarehouse(nextWarehouse, graph, checked, connectedWarehouses);
        }
    }
}
