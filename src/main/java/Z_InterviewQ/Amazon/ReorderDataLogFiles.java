package Z_InterviewQ.Amazon;


import java.util.*;

/*Input: logs = ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"]
Output: ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]
Explanation:
The letter-log contents are all different, so their ordering is "art can", "art zero", "own kit dig".
The digit-logs have a relative order of "dig1 8 1 5 1", "dig2 3 6".
Example 2:

Input: logs = ["a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo"]
Output: ["g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"]*/
public class ReorderDataLogFiles {


        public static void main(String[] args) {
            String logs[] = {"dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"};
            System.out.println(Arrays.toString(reorderLogFiles(logs)));

            System.out.println(Arrays.toString(reorderLogFile(logs)));
        }

        public static String[] reorderLogFiles(String[] logs) {
            Comparator<String> myComp = new Comparator<String>() {
                @Override
                public int compare(String log1, String log2) {
                    // split each log into two parts: <identifier, content>
                    String[] split1 = log1.split(" ", 2);
                    String[] split2 = log2.split(" ", 2);

                    boolean isDigit1 = Character.isDigit(split1[1].charAt(0));
                    boolean isDigit2 = Character.isDigit(split2[1].charAt(0));

                    // case 1). both logs are letter-logs
                    if (!isDigit1 && !isDigit2) {
                        // first compare the content
                        int cmp = split1[1].compareTo(split2[1]);
                        if (cmp != 0)
                            return cmp;
                        // logs of same content, compare the identifiers
                        return split1[0].compareTo(split2[0]);
                    }

                    // case 2). one of logs is digit-log
                    if (!isDigit1 && isDigit2)
                        // the letter-log comes before digit-logs
                        return -1;
                    else if (isDigit1 && !isDigit2)
                        return 1;
                    else
                        // case 3). both logs are digit-log
                        return 0;
                }
            };

            Arrays.sort(logs, myComp);
            return logs;
        }

    public static String[] reorderLogFile(String[] logs) {

        Comparator<String> myComp = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                int s1SpaceIndex = s1.indexOf(' ');
                int s2SpaceIndex = s2.indexOf(' ');
                char s1FirstCharacter = s1.charAt(s1SpaceIndex+1);
                char s2FirstCharacter = s2.charAt(s2SpaceIndex+1);

                if (s1FirstCharacter <= '9') {
                    if (s2FirstCharacter <= '9') return 0;
                    else return 1;
                }
                if (s2FirstCharacter <= '9') return -1;

                int preCompute = s1.substring(s1SpaceIndex+1).compareTo(s2.substring(s2SpaceIndex+1));
                if (preCompute == 0) return s1.substring(0,s1SpaceIndex).compareTo(s2.substring(0,s2SpaceIndex));
                return preCompute;
            }
        };

        Arrays.sort(logs, myComp);
        return logs;
    }
    }

