package Z_InterviewQ.Amazon;

import java.util.HashSet;
import java.util.Set;

/*Example 1:

Input: s = "bedbathandbeyand", dict = ["bed", "bath", "bat", "and", "hand", "bey", "beyand"]
Output: ["bed", "bath", "and", "beyand"] or ["bed", "bat", "hand", "beyand"]

Example 2:

Input: s = "catsandog", dict = ["cats", "dog", "sand", "and", "cat"]
Output: []*/

/*
Input:
"CatMat"
["Cat", "Mat", "Ca", "tM", "at", "C", "Dog", "og", "Do"]

Output: 3
Explanation:
we can form 3 sentences, as follows:
"CatMat" = "Cat" + "Mat"
"CatMat" = "Ca" + "tM" + "at"
"CatMat" = "C" + "at" + "Mat"*/

public class WordBreak_III {
    public static void main(String[] args) {
        String s = "CatMat";

    }
    public int wordBreak3(String s, Set<String> dict) {
        if (s == null ||s.length() == 0 || dict == null || dict.size() == 0) {
            return 0;
        }

        s = s.toLowerCase();
        Set<String> set = new HashSet<String>();
        for (String word : dict) {
            String str = word.toLowerCase();
            set.add(str);
        }

        int len = s.length();
        int[] nums = new int[len + 1];
        nums[len] = 1;

        for (int i = len - 1; i >= 0; i--) {
            for (int j = i + 1; j <= len; j++) {
                String prefix = s.substring(i, j);

                if (set.contains(prefix)) {
                    nums[i] += nums[j];
                }
            }
        }
        return nums[0];
    }
}
