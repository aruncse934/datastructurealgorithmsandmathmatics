package Z_InterviewQ.Amazon;

import java.util.Comparator;
import java.util.PriorityQueue;

/*Example 1:

Input: classes = [[1,2],[3,5],[2,2]], extraStudents = 2
Output: 0.78333
Explanation: You can assign the two extra students to the first class. The average pass ratio will be equal to (3/4 + 3/5 + 2/2) / 3 = 0.78333.
Example 2:

Input: classes = [[2,4],[3,9],[4,5],[2,10]], extraStudents = 4
Output: 0.53485*/
public class MaximumAveragePassRatio { // Five Star Seller

    public static double maxAverageRatio(int[][] classes, int extraStudents) {
        PriorityQueue<double[]> maxHeap = new PriorityQueue<>(Comparator.comparingDouble(o -> -o[0])); // Max heap compared by first value in decreasing order.
        for (int[] c : classes) {
            double a = c[0], b = c[1];
            maxHeap.offer(new double[]{profit(a, b), a, b});
        }
        while (extraStudents-- > 0) {
            double[] top = maxHeap.poll();
            double a = top[1], b = top[2];
            maxHeap.offer(new double[]{profit(a+1, b+1), a+1, b+1});
        }
        double ans = 0.0d;
        while (!maxHeap.isEmpty()) {
            double[] top = maxHeap.poll();
            double a = top[1], b = top[2];
            ans += a/b;
        }
        return ans / classes.length;
    }
     static double profit(double a, double b) {
        return (a + 1) / (b + 1) - a / b;
    }

    public static void main(String[] args) {
        int [][] classes ={{1,2},{3,5},{2,2}}; //[[1,2],[3,5],[2,2]]
        int extraStudents = 2;
        System.out.println(maxAverageRatio(classes,extraStudents));
    }
}
