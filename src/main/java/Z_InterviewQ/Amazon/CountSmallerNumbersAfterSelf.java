package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.List;

/*Example 1:

Input: nums = [5,2,6,1]
Output: [2,1,1,0]
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
Example 2:

Input: nums = [-1]
Output: [0]
Example 3:

Input: nums = [-1,-1]
Output: [0,0]
*/
public class CountSmallerNumbersAfterSelf {
    public static void main(String[] args) {
        int ar[] = {5,2,6,1};
        System.out.println(countSmaller(ar));

    }
    private static int[] count;
    public static List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<>();
        if (nums == null || nums.length == 0) return result;
        int n = nums.length;
        count = new int[n];
        // initialize the corresponding indexes for the array
        int[] indexes = new int[n];
        for (int i = 0; i < n; i++) {
            indexes[i] = i;
        }
        sort(nums, indexes, new int[n], 0, n - 1);
        for (int i = 0; i < n; i ++) {
            result.add(count[i]);
        }
        return result;
    }
    private static void sort(int[] nums, int[] indexes, int[] aux, int low, int high){
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        sort(nums, indexes, aux, low, mid);
        sort(nums, indexes, aux, mid + 1, high);
        merge(nums, indexes, aux, low, mid, high);
    }

    private static void merge(int[] nums, int[] indexes, int[] aux, int low, int mid, int high){
        int left_index = low, right_index = mid + 1;
        int right_count = 0;
        // copy the original indexes to auxiliary array
        for (int k = low; k <= high; k++) {
            aux[k] = indexes[k];
        }
        for (int k = low; k <= high; k++) {
            if (left_index > mid) {
                indexes[k] = aux[right_index++];
            } else if (right_index > high) {
                indexes[k] = aux[left_index];
                count[aux[left_index++]] += right_count;
            } else if (nums[aux[right_index]] < nums[aux[left_index]]) {
                indexes[k] = aux[right_index++];
                right_count ++;
            } else {
                indexes[k] = aux[left_index];
                count[aux[left_index++]] += right_count;
            }
        }
    }
}
