package Z_InterviewQ.Amazon;

import java.util.*;

/*Examples 1
Input:
foregroundTasks = [1, 7, 2, 4, 5, 6]
backgroundTasks = [3, 1, 2]
K = 6

Output:
[(3, 2), (4, 1), (5,-1)]

Explaination:
Here we have 5 foreground tasks: task 0 uses 1 memeory. task 1 uses 7 memeory. task 2 uses 2 memeory..
And 5 background tasks: task 0 uses 3 memeory. task 1 uses 1 memeory. task 2 uses 2 memeory..
We need to find two tasks with total memory usage sum <= K.
Here we can return the foreground task 3 and background task 2, which total use 6 units of memory.
Or we can return the foreground task 4 and background task 1. Also use total 6 units of memory.
Or we can return the foreground task 5 only without any background task. Also use total 6 units of memory.*/
public class OptimizeMemoryUsage {
    public static void main(String[] args) {
        int foregroundTasks[] ={1, 7, 2, 4, 5, 6};
        int backgroundTasks[] ={3, 1, 2};
        int k =6;
        System.out.println( optimizeMemoryUsage(foregroundTasks,backgroundTasks,k));
    }

    public static List<int[]> optimizeMemoryUsage(int[] foregroundTasks, int[] backgroundTasks, int K) {
        List<int[]> result = new ArrayList<>();
        if (K == 0 || (foregroundTasks.length == 0 && backgroundTasks.length == 0))
            result.add(new int[]{-1, -1});

        List<int[]> foregroundTaskList = new ArrayList<>();
        List<int[]> backgroundTaskList = new ArrayList<>();

        for (int i = 0; i < foregroundTasks.length; i++) {
            foregroundTaskList.add(new int[]{i, foregroundTasks[i]});
        }

        for (int i = 0; i < backgroundTasks.length; i++) {
            backgroundTaskList.add(new int[]{i, backgroundTasks[i]});
        }

        foregroundTaskList.sort((p1, p2) -> p1[1] - p2[1]);
        backgroundTaskList.sort((p1, p2) -> p1[1] - p2[1]);

        int max = Integer.MIN_VALUE;
        for(int i = 0; i < foregroundTasks.length; i++){
            if (foregroundTaskList.get(i)[1] == K) {
                result.add(new int[]{foregroundTaskList.get(i)[0], -1});
                max = foregroundTaskList.get(i)[1];
            }
        }

        for(int i = backgroundTasks.length-1; i >= 0; i--){
            if (backgroundTaskList.get(i)[1] == K) {
                result.add(new int[]{-1, backgroundTaskList.get(i)[0]});
                max = backgroundTaskList.get(i)[1];
            }
        }

        if(foregroundTasks.length > 0 && backgroundTasks.length == 0){
            for (int i = 0; i < foregroundTasks.length; i++) {
                if (foregroundTaskList.get(i)[1] < K) {
                    result.add(new int[]{foregroundTaskList.get(i)[0], -1});
                }
            }

            return result;
        }

        if(backgroundTasks.length > 0 && foregroundTasks.length == 0){
            for (int i = backgroundTasks.length - 1; i >= 0; i--) {
                if (backgroundTaskList.get(i)[1] < K) {
                    result.add(new int[]{-1, backgroundTaskList.get(i)[0]});
                }
            }

            return result;
        }

        int i = 0;
        int j = backgroundTasks.length - 1;
        while (i < foregroundTasks.length && j >= 0) {
            int sum = foregroundTaskList.get(i)[1] + backgroundTaskList.get(j)[1];

            if (sum > K) {
                j = j - 1;
            } else {
                if (max <= sum) {
                    if (max < sum) {
                        max = sum;
                        result.clear();
                    }
                    result.add(new int[]{foregroundTaskList.get(i)[0], backgroundTaskList.get(j)[0]});
                    int index = j - 1;
                    while (index >= 0 &&
                            backgroundTaskList.get(index)[1] == backgroundTaskList.get(index + 1)[1]) {
                        result.add(new int[]{foregroundTaskList.get(i)[0], backgroundTaskList.get(index)[0]});
                        index--;
                    }
                }
                ++i;
            }
        }

        return result;
    }
}
/*public List<int[]> optimizeMemoryUsage(int[] foregroundTasks, int[] backgroundTasks, int K) {
		List<int[]> result = new ArrayList();
		TreeMap<Integer, List<Integer>> foregroundMemToIds = new TreeMap();

		for (int i = 0; i < foregroundTasks.length; ++i) {
		    int mem = foregroundTasks[i];
		    if (mem > K)
		        continue;
		    foregroundMemToIds.putIfAbsent(mem, new ArrayList());
		    foregroundMemToIds.get(mem).add(i);
		}
		foregroundMemToIds.put(0, new ArrayList());
		foregroundMemToIds.get(0).add(-1);

		int maxMem = foregroundMemToIds.lastKey();
		for (int foregroundId : foregroundMemToIds.get(maxMem)) {
		    result.add(new int[] { foregroundId,  -1 });
		}

		for (int i = 0; i < backgroundTasks.length; ++i) {
		    int backgroundMem = backgroundTasks[i];
		    Integer foregroundMem = foregroundMemToIds.floorKey(K - backgroundMem);
		    if (foregroundMem == null)
		        continue;
		    int sumMem = foregroundMem + backgroundMem;
		    if (sumMem > K || sumMem < maxMem)
		        continue;

		    if (sumMem > maxMem) {
		        result = new ArrayList();
		        maxMem = sumMem;
		    }

		    for (int foregroundId : foregroundMemToIds.get(foregroundMem))
		        result.add(new int[] { foregroundId,  i });
		}

		return result;
    }*/