package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*Example 1:

Input:
[[1,0,0,0,1],[0,0,0,0,0],[0,0,1,0,0]]
Output:
6

Explanation:
The point `(0,2)` is an ideal meeting point, as the total travel distance of `2 + 2 + 2 = 6` is minimal. So return `6`.
Example 2:

Input:
[[1,1,0,0,1],[1,0,1,0,0],[0,0,1,0,1]]
Output:
14
*/
/*Example 1
Input = [ [1,0], [1,1] ]
Output = 1

Example 2
Input = [ [-4,3], [-2,1], [1,0], [3,2] ]
Output = 14*/
public class BestMeetingPoint {

    public static void main(String[] args) {
        int[][] points1 = {{1,0}, {1,1}};
        int[][] points2 = {{-4,3}, {-2,1}, {1,0}, {3,2}};

        System.out.println("Output: " + minDist(points1));
        System.out.println("Output: " + minDist(points2));
    }
    private static int distance(ArrayList<Integer> tmp) {
        int n = tmp.size();
        int ret = 0;
        for (int i = 0, j = n - 1; i < j; i++, j--) {
            ret += tmp.get(j) - tmp.get(i);
        }
        return ret;
    }

    public static int minTotalDistance(int[][] grid) {
        ArrayList<Integer> x = new ArrayList();
        ArrayList<Integer> y = new ArrayList();
        int n = grid.length;
        int m = grid[0].length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                // 如果有人居住，记录下坐标位置
                if (grid[i][j] == 1) {
                    x.add(i);
                    y.add(j);
                }
            }
        }
        Collections.sort(y);
        return distance(x) + distance(y);
    }

    //
    private static int minDist(int[][] points) {
        List<Integer> xs = new ArrayList<>();
        List<Integer> ys = new ArrayList<>();
        for(int[] p : points) {
            xs.add(p[0]);
            ys.add(p[1]);
        }

        return getMD(xs) + getMD(ys);
    }
    static int getMD(List<Integer> lst) {
        Collections.sort(lst);
        int l=0, r=lst.size()-1, res=0;
        while(l < r) {
            res+= lst.get(r--) - lst.get(l++);
        }
        return res;
    }
}
