package Z_InterviewQ.Amazon;

import java.util.*;

/*A distributed caching service wants to build an algorithm to measure the efficiency of its network. The network is
represented as 	a number of nodes and a list of connected pairs. The efficiency of this network can be estimated
by first summing the cost of each isolated set of nodes where each individual node has a cost of 1.
To account for the increase in efficiency as mode nodes  are connected, update the cost of each isolated set to be the
ceiling of the square root of the original cost and return the final sum of all costs.


Example
n = 10 nodes
Edges = [[1,2], [1,3], [2,4], [3,5], [7,8]]

There are two isolated sets with more than one node, {1,2,3,4,5} and {7,8}. The ceilings of their square roots
are 5 ^ 1/2 = 2.236 and ceil(2.236) = 3, 2 ^ 1/2 = 1.414 and ceil (1.414) = 2. The other three isolated nodes
are separate and the square root of their weights is 1 ^ 1/2 = 1 respectively. The sum is 3 + 2 + (3 * 1) = 8.

Input has two parameters:
n - no of nodes
edges - list of edges, each edge is represented a string "1 2"

output:
int: an integer that denotes the sum of the values calculated.
*/
public class CloudFrontCaching {
    public static void main(String[] args) {
        System.out.println(connectedSum(10, Arrays.asList("1 2","1 3","2 4","3 5","7 8")));

        System.out.println(connectedSum(4, Arrays.asList("1 2","1 4")));

        System.out.println(connectedSum(0, Arrays.asList()));

        System.out.println(connectedSum(1, Arrays.asList()));


    }

    private static int connectedSum(int N, List<String> edges) {
        Map<Integer, Set<Integer>> graph = new HashMap<>();

        // building graph
        for (String edge: edges) {
            String[] parts = edge.trim().split(" ");
            Integer source = Integer.parseInt(parts[0]);
            Integer destination = Integer.parseInt(parts[1]);

            // adding edge: a -> b
            if(graph.containsKey(source)) {
                Set<Integer> children = graph.get(source);
                children.add(destination);
            } else {
                Set<Integer> children = new HashSet<>();
                children.add(destination);
                graph.put(source, children);
            }

            // adding edge: b -> a
            if(graph.containsKey(destination)) {
                Set<Integer> children = graph.get(destination);
                children.add(source);
            } else {
                Set<Integer> children = new HashSet<>();
                children.add(source);
                graph.put(destination, children);
            }
        }

        //System.out.println(graph.toString());

        // find groups
        int connectedSum = 0;

        HashSet<Integer> visited = new HashSet<>();
        for(int node=1; node <= N; node++) {
            Set<Integer> group = getGroup(node, graph, visited);
            //System.out.println(group.toString());
            if(group != null && group.size() > 0) {
                connectedSum += (int) (Math.ceil(Math.sqrt(group.size())));
            }
        }

        return connectedSum;
    }

    private static Set<Integer> getGroup(int node, Map<Integer, Set<Integer>> graph, Set<Integer> visited) {
        Set<Integer> group = new HashSet<>();
        if (visited.contains(node)) {
            return group;
        }

        Queue<Integer> nodesToVisit = new ArrayDeque<>();
        nodesToVisit.add(node);
        group.add(node);

        while(!nodesToVisit.isEmpty()) {
            Integer currentNode = nodesToVisit.remove();
            visited.add(currentNode);
            Set<Integer> children = graph.get(currentNode);
            if(children != null) {
                for(Integer child: children) {
                    if(!visited.contains(child)) {
                        group.add(child);
                        nodesToVisit.add(child);
                    }
                }
            }
        }

        return group;
    }
}
