package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.List;

/*Example 1:

Input: nums = [4,3,2,7,8,2,3,1]
Output: [2,3]
Example 2:

Input: nums = [1,1,2]
Output: [1]
Example 3:

Input: nums = [1]
Output: []*/
public class CountDuplicates { // Find All Duplicates in an Array

    //
    public List<Integer> countduplicates(List<Integer> nums) {
        // write your code here
        return null;
    }

    //
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < nums.length; ++i) {
            int index = Math.abs(nums[i])-1;
            if (nums[index] < 0)
                res.add(Math.abs(index+1));
            nums[index] = -nums[index];
        }
        return res;
    }
    //
    public List<Integer> findDuplicate(int[] nums) {
        List<Integer> newList = new ArrayList<Integer>();     // creating a new List
        for(int i=0;i<nums.length;i++){
            int index =Math.abs(nums[i]);             // Taking the absolute value to find index
            if(nums[index-1] >0){
                nums[index-1] = - nums[index-1];
            }else{
                // If it is not greater than 0 (i.e) negative then the number is a duplicate
                newList.add(Math.abs(nums[i]));
            }
        }
        return newList;
    }
}
