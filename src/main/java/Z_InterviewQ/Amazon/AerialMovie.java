package Z_InterviewQ.Amazon;

import java.util.*;
/*Example
Given t=87,dur=[20,25,19,37],return[20,37]

Explanation:
87-30=57
20+25=45,57-45=12
20+19=39,57-39=19
20+37=57,57-57=0
25+19=44,57-44=13
25+37=62,57<62
19+37=56,57-56=1
Givent=67,dur=[20,17,19,18],return[17,20]

Explanation:
67-30=37
17+20=37,18+19=37
The longest movie in the first group is 20，and 19 in the second grouo, so output[17,20]
Notice
30<t<=1000
dur[i]<=1000
1<=len(dur)<=100000*/
public class AerialMovie {

    public int[] aerial_Movies(int t, int[] dur) {
        // Write your code here
        if (t <= 30 || dur == null || dur.length == 0) {
            return null;
        }

        int maxDur = t - 30;
        Arrays.sort(dur);

        int i = 0;
        int j = dur.length - 1;

        int[] result = new int[2];
        int prev = 0;

        while (i < j) {
            int curDur = dur[i] + dur[j];
            if (curDur == maxDur) {
                result[0] = dur[i];
                result[1] = dur[j];
                return result;
            } else if (curDur > maxDur) {
                j--;
            } else {
                if (curDur > prev || curDur == prev && dur[j] > result[1]) {
                    prev = curDur;
                    result[0] = dur[i];
                    result[1] = dur[j];
                }
                i++;
            }
        }

        return result;
    }
    //
    public int find(int [] dur, int need, int pos) {
        int l = 0, r = pos;
        while(l <= r) {
            int mid = (l + r)  >> 1;
            if(dur[mid] <= need) {

                l = mid + 1;
            }
            else {

                r = mid - 1;
            }

        }
        if(r == -1) {
            return dur[0];
        }
        return dur[r];
    }
    public int[] aerial_Movie(int t, int[] dur) {
        // Write your code here
        t -= 30;
        int [] res = new int [2];
        int ans = 0;
        Arrays.sort(dur);
        for(int i = dur.length - 1 ; i >= 1 ; i--) {
            if(dur[i] < t) {
                int tmp = find(dur, t-dur[i], i-1);
                if(tmp <= t - dur[i] && tmp + dur[i] > ans) {
                    ans = tmp + dur[i];
                    res[0] = tmp;
                    res[1] = dur[i];

                }
            }
        }
        return res;
    }


}
