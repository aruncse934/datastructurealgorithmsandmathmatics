package Z_InterviewQ.Amazon;

import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

/*Description
In order to decorate your new house, you need to process some sticks with positive integer length.
You can connect any two sticks of lengths X and Y into one stick by paying a cost of X + Y. Due to the construction needs, you must connect all the bars into one.
Return the minimum cost of connecting all the given sticks into one stick in this way.
Please note that you can choose any order of sticks connection

1 \leq sticks.length \leq 10^41≤sticks.length≤10
4

1 \leq sticks[i] \leq 10^41≤sticks[i]≤10
4

Example
Example 1:

Input:
[2,4,3]
Output:
14
Explanation:
First connect 2 and 3 to 5 and cost 5; then connect 5 and 4 to 9; total cost is 14
Example 2:

Input:
 [1,8,3,5]
Output:
30*/
public class MinimumCostConnectSticks {
    public static void main(String[] args) {
        List<Integer> sticky = Arrays.asList(1, 8, 3, 5);
        System.out.println(MinimumCost(sticky));
    }

    //heap
    public static int MinimumCost(List<Integer> sticks) {
        if (sticks.size() < 2)
            return 0;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int num : sticks) {
            minHeap.add(num);
        }
        int res = 0;
        while (minHeap.size() > 1) {
            int merge = minHeap.poll() + minHeap.poll();
            res += merge;
            minHeap.add(merge);
        }
        return res;
    }
}