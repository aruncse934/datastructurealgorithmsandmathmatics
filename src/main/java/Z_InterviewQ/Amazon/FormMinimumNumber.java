package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.List;

/*Description
Given a pattern str containing only I and D. I for increasing and D for decreasing. Please design an algorithm to return the string that conforms to the pattern and has the smallest dictionary order. Digits from 1 to 9 and digits can’t repeat.

1<=|str|<=8



Example
Example 1:

Input:
"D"
Output:
"21"
Explanation:
2>1
Example 2:

Input:
"II"
Output:
"123"
Explanation:
1<2<3
Example 3:

Input:
"DIDI"
Output:
"21435"
Example 4:

Input:
"DDIDDIID"
Output:
"321654798"
*/
public class FormMinimumNumber {
    public static void main(String[] args) {
        String str = "DIDI";
        System.out.println(formMinimumNumber(str));
    }

    public static String formMinimumNumber(String str) {
        int length = str.length();
        String ans = new String();
        for(int i = 0; i <= length; i++){
            ans += ' ';
        }
        StringBuilder sb = new StringBuilder(ans);
        int count  = 1;
        for(int i = 0; i <= length;i++){
            if(i == length || str.charAt(i) == 'I'){
                for(int j = i - 1; j >= -1; j--){
                    sb.setCharAt(j + 1, (char) ('0' + count++));
                    if(j >= 0 && str.charAt(j) == 'I'){
                        break;
                    }
                }
            }
        }
        ans= sb.toString();
        return ans;
    }


}
