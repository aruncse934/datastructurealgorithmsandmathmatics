package Z_InterviewQ.Amazon.FirstRound;

import java.util.*;

/*here is a dictionary already implemented. Write a method, which takes input String without space, to prints
  all subsets of the input string which is present in dictionary.
Example: Dictionary – a*
………….Input- aaabaa
………….Output- a,a,a,aa,aa,aaa,a,a,aa
*/
public class StringWithoutSpace {

    public void findSubStringInDictionary(){
        String s = "aaabaa";
        Set<String> d = new HashSet<>(Arrays.asList("a","cdf","bcd","ef"));
        System.out.println(findSubString(s,d));
    }
    public List<String> findSubString(String s, Set<String> dictionary){
        List<String> retSet = new LinkedList<>();
        for(int i = 0; i<s.length();i++){
            for(int j = i;j<=s.length();j++){
                if(dictionary.contains(s.substring(i,j))){
                    retSet.add(s.substring(i,j));
                }
            }
        }
        return retSet;
    }


    public static void main(String[] args) {
        StringWithoutSpace space = new StringWithoutSpace();
        space.findSubStringInDictionary();

    }
}
