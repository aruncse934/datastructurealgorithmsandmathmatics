package Z_InterviewQ.Amazon;

/*Example 1:

Input:

matrix = [[3,4]]
target = 3
Output:

1
Explanation:

There is only one 3 in the matrix.

Example 2:

Input:

matrix = [
      [1, 3, 5, 7],
      [2, 4, 7, 8],
      [3, 5, 9, 10]
    ]
target = 3
Output:

2
Explanation:

There are two 3 in the matrix.

*/

public class Search2D_Matrix_II {
    public static void main(String[] args) {

    }

    public int searchMatrix(int[][] matrix, int target) {
        int x = matrix[0].length - 1, y = 0, count = 0;
        while (x >= 0 && y < matrix.length) {
            if (matrix[y][x] > target) x--;
            else if (matrix[y][x] < target) y++;
            else {
                x--;
                count++;
            }
        }
        return count;
    }
}
