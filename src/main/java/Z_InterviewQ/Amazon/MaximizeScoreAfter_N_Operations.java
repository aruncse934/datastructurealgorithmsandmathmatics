package Z_InterviewQ.Amazon;

/*Example 1:

Input: nums = [1,2]
Output: 1
Explanation: The optimal choice of operations is:
(1 * gcd(1, 2)) = 1
Example 2:

Input: nums = [3,4,6,8]
Output: 11
Explanation: The optimal choice of operations is:
(1 * gcd(3, 6)) + (2 * gcd(4, 8)) = 3 + 8 = 11
Example 3:

Input: nums = [1,2,3,4,5,6]
Output: 14
Explanation: The optimal choice of operations is:
(1 * gcd(1, 5)) + (2 * gcd(2, 4)) + (3 * gcd(3, 6)) = 1 + 4 + 9 = 14*/
public class MaximizeScoreAfter_N_Operations { // Number Game

   static int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
   static int dfs(int[] n, int[][] dp, int i, int mask) {
        if (i > n.length / 2)
            return 0;
        if (dp[i][mask] == 0)
            for (int j = 0; j < n.length; ++j)
                for (int k = j + 1; k < n.length; ++k) {
                    int new_mask = (1 << j) + (1 << k);
                    if ((mask & new_mask) == 0)
                        dp[i][mask] = Math.max(dp[i][mask], i * gcd(n[j], n[k]) + dfs(n, dp, i + 1, mask + new_mask));
                }
        return dp[i][mask];
    }
   static int maxScore(int[] n) {
        return dfs(n, new int[n.length / 2 + 1][1 << n.length], 1, 0);
    }

    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6};
        System.out.println(maxScore(arr));
    }
}
