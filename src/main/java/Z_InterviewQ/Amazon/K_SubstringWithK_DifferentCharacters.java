package Z_InterviewQ.Amazon;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*Description
Given a string S and an integer K.
Calculate the number of substrings of length K and containing K different characters
Example
String: "abcabc"
K: 3

Answer: 3
substrings:  ["abc", "bca", "cab"]
String: "abacab"
K: 3

Answer: 2
substrings: ["bac", "cab"]*/
public class K_SubstringWithK_DifferentCharacters {
    public static void main(String[] args) {
        String s = "abacab";
        int k = 3;
        System.out.println(KSubstringWithK(s,k));
    }

    public static int KSubstringWithK(String s, int k){
        if(s ==null || s.length() == 0 || k <= 0){
            return 0;
        }
        int count = 0;
        Map<Character, Integer> map = new HashMap<>();
        Set<String> set = new HashSet<>();
        int len = s.length();
        int j = 0;
        for(int i = 0; i < len; i++){
            while (j < len && map.size() < k){
                char c = s.charAt(j);
                if(map.containsKey(c)){
                    break;
                }
                map.put(c,1);
                j++;
                if(map.size() == k){
                    set.add(s.substring(i,j));
                }
            }
            map.remove(s.charAt(i));
        }
        return set.size();
    }
}
/*public int KSubstring(String stringIn, int K) {
        if (stringIn == null || stringIn.length() == 0 || K <= 0) {
            return 0;
        }
        int count = 0;
        HashMap<Character, Integer> charMap = new HashMap<>();
        HashSet<String> resultSet = new HashSet<String>();
        int len = stringIn.length();
        int j = 0;
        for (int i = 0; i < len; i++) {
            while (j < len && charMap.size() < K) {
                char c = stringIn.charAt(j);
                if (charMap.containsKey(c)) {
                    break;
                }
                charMap.put(c, 1);
                j++;
                if (charMap.size() == K) {
                    resultSet.add(stringIn.substring(i, j));
                }
            }
            charMap.remove(stringIn.charAt(i));
        }
        return resultSet.size();
    }*/