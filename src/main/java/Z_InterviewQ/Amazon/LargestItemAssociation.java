package Z_InterviewQ.Amazon;

import java.util.*;

/*Input
The itput to the function/method consists of an argument - itemAssociation, a list containing paris of string representing the items that are ordered together.

Output
Return a list of strings representing the largest association group sorted lexicographically.

Example
Input:
itemAssociation: [
[Item1, Item2],
[Item3, Item4],
[Item4, Item5]
]

Output:
[Item3, Item4, Item5]

Explanation:
There are two item association groups:
group1: [Item1, Item2]
group2: [Item3,Item4,Item5]
In the available associations, group2 has the largest association. So, the output is [Item3, Item4, Item5].

Helper Description in java
The following class is used to represent a Pair of strings and is already implemented in the default code (Do not write this definition again in your code):

class PairString
{
	String first;
	String second;

	PairString(String first, String second)
	{
		this.first = first;
		this.second = second;
	}
}
Method Signature to Implement in java
Java

List<String> LargestItemAssociation(List<PairString> itemAssociation)
{
}*/
public class LargestItemAssociation {
    public static void main(String[] args) {
        System.out.println(largestItemAssociation(Arrays.asList(
                new PairString("Item1", "Item2"),
                new PairString("Item3", "Item4"),
                new PairString("Item4", "Item5")
        )));

        System.out.println(largestItemAssociation(Arrays.asList(
                new PairString("Item1", "Item2"),
                new PairString("Item2", "Item8"),
                new PairString("Item2", "Item10"),
                new PairString("Item10", "Item12"),
                new PairString("Item10", "Item4"),
                new PairString("Item10", "Item3"),
                new PairString("Item3", "Item4"),
                new PairString("Item4", "Item5")
        )));
    }

    static List<String> largestItemAssociation(List<PairString> itemAssociation) {
        LinkedHashMap<String, LinkedHashSet<String>> map = new LinkedHashMap<>();
        for (PairString pairs : itemAssociation) {
            if (pairs.first.equals(pairs.second)) continue;
            map.computeIfAbsent(pairs.first, val -> new LinkedHashSet<>()).add(pairs.second);
            map.computeIfAbsent(pairs.second, val -> new LinkedHashSet<>()).add(pairs.first);
        }
        List<String> result = new ArrayList<>();
        for (String name : map.keySet()) {
            Set<String> visited = new HashSet<>();
            List<String> cur = dfs(name, map, visited);
            if (cur.size() > result.size()) {
                result = cur;
            }
        }
        return result;
    }

    private static List<String> dfs(String name, Map<String, LinkedHashSet<String>> map, Set<String> visited) {
        if (!visited.add(name)) return new ArrayList<>();
        List<String> cur = new ArrayList<>();
        for (String dep : map.get(name)) {
            List<String> next = dfs(dep, map, visited);
            if (next.size() > cur.size()) {
                cur = next;
            }
        }
        visited.remove(name);
        cur.add(0, name);
        return cur;
    }
}
/*static List<String> largestItemAssociation(List<PairString> itemAssociation) {
    Set<String> seen = new HashSet<>();
    Map<String, List<String>> graph = getGraph(itemAssociation);
    TreeSet<String> ansSet = new TreeSet<>();
    for (String vertex : graph.keySet()) {
        if (!seen.contains(vertex)) {
            TreeSet<String> path = new TreeSet<>((a,b) -> a.compareTo(b));
            dfs(vertex, graph, path,seen);
            if(ansSet.size() < path.size()){
                ansSet = path;
            }else if(ansSet.size() == path.size()){
                Iterator<String> outItr = ansSet.iterator();
                Iterator<String> pathItr = path.iterator();
                while(outItr.hasNext()){
                    String os = outItr.next();
                    String ps = pathItr.next();
                    if(os.compareTo(ps) > 0){
                        ansSet = path;
                    }
                }
            }
        }
    }
    return new ArrayList<>(ansSet);
}

private static void dfs(String key, Map<String, List<String>> graph, TreeSet<String> path,Set<String> seen) {
    if (!seen.contains(key)) {
        seen.add(key);
        path.add(key);
        for (String child : graph.get(key)) {
            dfs(child, graph, path,seen);
        }
    }
}

static Map<String, List<String>> getGraph(List<PairString> itemAssociation) {
    Map<String, List<String>> graph = new HashMap<>();
    for (PairString pair : itemAssociation) {
        List<String> child1;
        if (graph.containsKey(pair.first)) {
            child1 = graph.get(pair.first);
        } else {
            child1 = new ArrayList<>();
        }
        child1.add(pair.second);
        graph.put(pair.first, child1);

        List<String> child2;
        if (graph.containsKey(pair.second)) {
            child2 = graph.get(pair.second);
        } else {
            child2 = new ArrayList<>();
        }
        child2.add(pair.first);
        graph.put(pair.second, child2);
    }
    return graph;
}*/