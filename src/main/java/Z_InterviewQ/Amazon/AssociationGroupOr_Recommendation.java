package Z_InterviewQ.Amazon;


import java.util.*;

class PairString{
    String first;
    String second;

    PairString(String first, String second){
        this.first = first;
        this.second = second;
    }
}
public class AssociationGroupOr_Recommendation {

    public static void main(String[] args) {
        System.out.println(largestItemAssociation(Arrays.asList(
                new PairString("Item1", "Item2"),
                new PairString("Item3", "Item4"),
                new PairString("Item4", "Item5")
        )));

        System.out.println(largestItemAssociation(Arrays.asList(
                new PairString("A Farewell to Arms", "For Whom the Bell Tolls"),
                new PairString("Grapes of Wrath", "Of Mice and Men"),
                new PairString("Of Mice and Men", "East of Eden"),
                new PairString("Canary Row", "Sweet Thursday"),
                new PairString("On Writing", "Working Days"),
                new PairString("Sweet Thursday", "Tortilla Flat"),
                new PairString("The Big Sleep", "Farewell My Lovely"),
                new PairString("David Copperfield", "Pickwick Papers")
        )));
    }

    private static List<String> largestItemAssociation(List<PairString> itemAssociation){
        TreeSet<String> result = new TreeSet<>();
        Map<String, List<String>> graph = new HashMap<>();

        //Create a graph for all items (first items in a pair)
        for(PairString pair : itemAssociation){
            graph.putIfAbsent(pair.first, new ArrayList<String>());
            graph.get(pair.first).add(pair.second);
        }

        Set<String> processed = new HashSet<>();

        for(PairString item : itemAssociation){
            TreeSet<String> associationGroup = new TreeSet<>();

            if(!processed.contains(item.first)){
                getGroupies(item.first, graph, new HashSet<String>(), associationGroup);
                processed.add(item.first);
                result = updateResult(result, associationGroup);
            }
        }
        return new ArrayList<String>(result);
    }

    private static void getGroupies(String item, Map<String, List<String>> graph, Set<String> checked, TreeSet<String> associationGroup){
        if(checked.contains(item)) return;

        checked.add(item);
        associationGroup.add(item);

        for(String nextItem : graph.getOrDefault(item, new ArrayList<String>())){
            getGroupies(nextItem, graph, checked, associationGroup);
        }
    }

    private static TreeSet<String> updateResult(TreeSet<String> oldResult, TreeSet<String> newResult){
        if(newResult.size() > oldResult.size()){
            return newResult;
        }
        if(oldResult.size() == newResult.size()){
            if(oldResult.first().compareTo(newResult.first()) <= 0){
                return oldResult;
            }
            else{
                return newResult;
            }
        }
        return oldResult;
    }

}
