package Z_InterviewQ.Amazon;

import java.util.ArrayList;
import java.util.*;

/*Input
The input to the function/method consists of five arguments - numCompetitors, an integer representing the number of competitors for the Echo device;
topNCompetitors, is an integer representing the maximum number of competitors that Amazon wants to identify;
competitors, a list of strings representing the competitors;
numReviews, an integer representing the number of reviews from different websites that are identified by the automated webcrawler;
reviews, a list of string where each element is a string that consists of space-separated words representing user reviews.

Output
Return a list of strings representing Amazon's top N competitors in order of most frequently mentioned to least frequent.

Note
The comparison of strings is case-insensitive. If the value of topNCompetitors is more than the number of competitors discussed in the reviews then output the names of only the competitors mention.
If competitors have the same count (e.g. newshop=2, shopnow=2, mymarket=4), sort alphabetically. topNCompetitors=2, Output=[mymarket, newshop]

Example
Input:
numCompetitors=6
topNCompetitors = 2
competitors = [newshop, shopnow, afashion, fashionbeats, mymarket, tcellular]
numReviews = 6
reviews = [
"newshop is providing good services in the city; everyone should use newshop",
"best services by newshop",
"fashionbeats has great services in the city",
"I am proud to have fashionbeats",
"mymarket has awesome services",
"Thanks Newshop for the quick delivery"]

Output
["newshop", "fashionbeats"]

Explanation
"newshop" is occurring in 3 different reviews. "fashionbeats" is occuring in 2 different user reviews and "mymarket" is occurring in only 1 review.

*/
public class TopN_Competitors {
        public static void main(String[] args) {
            List<String> reviews = new ArrayList(
                    Arrays.asList(
                            "newshop is providing good services in the city; everyone should use newshop",
                            "best services by newshop's friendly staff",
                            "fashionbeats has great services in the city",
                            "I am proud to have fashionbeats's shoes.",
                            "mymarket has awesome services",
                            "Thanks newshop. You're better than fashionbeats."));

            List<String> competitors = new ArrayList<String>(
                    Arrays.asList("newshop", "shopnow", "afashion", "fashionbeats", "mymarket", "tcellular"));

            System.out.println(topNumCompetitors(6, 2, competitors, 6, reviews));
        }

        private static List<String> topNumCompetitors(int numCompetitors,int topNCompetitors,List<String> competitors,int numReviews, List<String> reviews){
            Set<String> competitorSet = new HashSet<>();
            competitorSet.addAll(competitors);

            Map<String, Integer> competitorOccurenceMap = new HashMap<>();
            for(String competitor : competitors){
                competitorOccurenceMap.put(competitor, 0);
            }

            for(String review : reviews){
                Set<String> repeatingNameSet = new HashSet<>();
                //review = review.replaceAll("[^a-zA-Z0-9]", " ");
                String[] words = review.split("\\W");
                for(String word : words){
                    if(competitorSet.contains(word) && !repeatingNameSet.contains(word)){
                        competitorOccurenceMap.put(word, competitorOccurenceMap.get(word)+1);
                        repeatingNameSet.add(word);
                    }
                }
            }
        /*
        for(Map.Entry<String, Integer> entry : competitorOccurenceMap.entrySet()){
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        */

            PriorityQueue<String> minHeap = new PriorityQueue<String>(
                    (c1, c2) -> competitorOccurenceMap.get(c1) == competitorOccurenceMap.get(c2) ? c2.compareTo(c1) : competitorOccurenceMap.get(c1) - competitorOccurenceMap.get(c2));

            for(String competitor : competitors){
                minHeap.offer(competitor);

                if(minHeap.size() > topNCompetitors){
                    minHeap.poll();
                }
            }

            List<String> result = new ArrayList<>();
            while(!minHeap.isEmpty()){
                result.add(0, minHeap.poll());
            }

            return result;
        }

}
