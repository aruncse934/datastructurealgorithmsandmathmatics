package Z_InterviewQ.Amazon;

/*Given a tree with n nodes, the father of the i-th node is fa[i-1], and the value is val[i-1].
In particular, 1 represents the root node, 2 represents the second node, and so on, and
it is guaranteed that the parent of the root node is -1, that is, fa[0] = -1.
 The average value of a subtree is the sum of all nodes val of the subtree divided by the number of nodes of the subtree.
 Find the maximum average value of the subtree of the tree, and return the root node number of the subtree.
 No more than 100,000 points If there are multiple subtree nodes with the most value, the one with the smallest number is output
 Sample Example 1: Input: fa=[-1,1,1,2,2,2,3,3], val=[100,120,80,40,50,60,50,70]
  Output: 1
  Explanation: -1 ------No.1 / \ No.2 ----1 1---------No.3 / | \ / \ 2 2 2 3 3
   The calculation result of No.1 node is (100+120+80+40+50+60+50+70) / 8 = 71.25
   The calculation result of No.2 node is (120 + 40 + 50 + 60) / 4 = 67.5 T
   he calculation result of No.3 node is (80+50+70) / 3 = 66.6667
   Then it will return 1 this node
   Example 2: Input: fa=[-1,1], val=[30,40]
   Output: 2
    Explanation: The calculation result of No.1 is (30+40)/2=35 The calculation result of No.2 is 40/1=40 Return node 2*/
public class TreeProblem {
    public int treeProblem(int[] fa, int[] val) {
        // Write your code here
        int [] totalval = new int [100500];
        int [] totalnum = new int [100500];
        totalval[0] = val[0];
        totalnum[0] = 1;
        for(int i = 1; i < val.length; i++) {
            totalval[i] = val[i];
            totalnum[i] = 1;
        }
        for(int i = 1; i < fa.length; i++) {
            int tmp = fa[i];
            while(tmp != -1) {
                totalnum[tmp-1]++;
                totalval[tmp-1] += val[i];
                tmp = fa[tmp-1];
            }
        }
        int tmp = totalval[0] / totalnum[0], res = 0;
        for(int i = 1; i < val.length; i++) {
            if(tmp < totalval[i] / totalnum[i]) {
                tmp = totalval[i] / totalnum[i];
                res = i;
            }
        }
        return res + 1;
    }

}
