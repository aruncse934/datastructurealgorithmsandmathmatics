package Z_InterviewQ.Amazon;

/*Input: root = [1,2,3,4,5]
Output: 3
Explanation: 3is the length of the path [4,2,1,3] or [5,2,1,3].
Example 2:

Input: root = [1,2]
Output: 1*/
public class DiameterBinaryTree {

    public int diameterOfBinaryTree(TreeNode root) {
        if (root == null) return 0;

        int left = diameterOfBinaryTree(root.left); //get left node diameter
        int right = diameterOfBinaryTree(root.right);//get right node diameter
        int rootDiameter = maxDepth(root.left) + maxDepth(root.right); //get root node diameter
        return Math.max(Math.max(left, right), rootDiameter); //get max of 3 diameters
    }

    //get max Depth
    public int maxDepth(TreeNode root){
        if (root == null) return 0;
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

}
/*  int max = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        maxDepth(root);
        return max;
    }

    private int maxDepth(TreeNode root) {
        if (root == null) return 0;

        int left = maxDepth(root.left);
        int right = maxDepth(root.right);

        max = Math.max(max, left + right);

        return Math.max(left, right) + 1;
    }*/