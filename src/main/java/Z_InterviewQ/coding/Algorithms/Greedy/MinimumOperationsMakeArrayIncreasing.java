package Z_InterviewQ.coding.Algorithms.Greedy;

public class MinimumOperationsMakeArrayIncreasing {

    public static int minOperations(int[] nums){
        int cnt = 0, prev =0;
        for(int curr : nums){
            if(curr <= prev){
                cnt += ++prev - curr;
            }else{
                prev = curr;
            }
        }
        return cnt;
    }
    public static void main(String[] args) {
     int[] nums = {1,5,2,4,1};
        System.out.println(minOperations(nums));
    }
}
