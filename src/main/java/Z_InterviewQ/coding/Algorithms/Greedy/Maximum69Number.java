package Z_InterviewQ.coding.Algorithms.Greedy;

public class Maximum69Number {


    public static int maximum69Num(int num){
        return Integer.parseInt((""+num).replaceFirst("6", "9"));
    }

    //One Pass
public static int maximum69Nums(int num){
        int add = 0, step =1, temp = num;
        while(temp > 0){
            int d = temp % 10;
            temp /= 10;
            if(d == 6){
                add = 3 * step;
            }
            step *= 10;
        }
        return num+add;
}
    public static void main(String[] args) {
      int num = 9999;
        System.out.println(maximum69Nums(num));
    }
}
/*public int maximum69Number (int num) {

        int add = 0;
        int step = 1;
        int tmp = num;

        while(tmp > 0) {
            int d = tmp%10;
            tmp /= 10;

            if (d==6) {
                add = 3*step;
            }
            step*=10;
        }
        return num+add;
    }*/