package Z_InterviewQ.coding.Algorithms.Greedy;

import java.util.Arrays;

public class DI_StringMatch {

    public static int[] diStringMatch(String s){
        int n = s.length();
        int lo = 0, hi = n;
        int[] ans = new int[n+1];
        for(int i = 0; i < n; ++i){
            if(s.charAt(i) == 'I')
                ans[i] = lo++;
            else
                ans[i] = hi--;
        }
        ans[n] = lo;
        return ans;
    }
    public static void main(String[] args) {
        String s = "IDID";
        System.out.println(Arrays.toString(diStringMatch(s)));
    }
}
/*public int[] diStringMatch(String S) {
        int N = S.length();
        int lo = 0, hi = N;
        int[] ans = new int[N + 1];
        for (int i = 0; i < N; ++i) {
            if (S.charAt(i) == 'I')
                ans[i] = lo++;
            else
                ans[i] = hi--;
        }

        ans[N] = lo;
        return ans;
    }*/