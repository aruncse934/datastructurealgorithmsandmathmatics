package Z_InterviewQ.coding.Algorithms.Recursion;

import java.util.Arrays;
import java.util.Stack;

public class ReverseString {

    //two pointer
    public static void reverseString(char[] s) {
        int i = 0, j = s.length - 1;
        while (i <= j) {
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            i++;
            j--;
        }

    }

    //recursion
    public static void reverString(char[] s){
        solve(s,0,s.length-1);
    }

    private static void solve(char[] s, int i, int j) {
        if(i >= j) return;
        char temp = s[i];
        s[i] = s[j];
        s[j] = temp;
        solve(s,++i, --j);
    }

    //stack
    public static void reverse(char[] s){
        Stack<Character> stack = new Stack<>();
        String str = new String(s);
        for(int i = 0; i < str.length(); i++){
            stack.push(s[i]);
        }

        char ans[] = new char[s.length];
        int i = 0;
        while(stack.size() > 0){
            s[i++] = stack.pop();
        }
        for(int j = 0; j < str.length();j++){
            ans[j] = str.charAt(j);
        }
    }

    public static void main(String[] args) {
        char[] ch = {'h', 'e', 'l', 'l', 'o'};
        reverse(ch);
        System.out.println(Arrays.toString(ch));
    }
}
