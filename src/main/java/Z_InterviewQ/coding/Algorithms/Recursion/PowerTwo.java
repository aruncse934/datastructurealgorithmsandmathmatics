package Z_InterviewQ.coding.Algorithms.Recursion;

public class PowerTwo {

    //iterative
    public static boolean isPowerTwo1(int n) {
    if(n <= 0)
        return false;
    while(n%2 == 0){
        n /=2;
    }
    return n == 1;
    }
    //recursive
    public static boolean isPowerTwo2(int n){
        return n > 0 && (n == 1 || (n%2 == 0 && isPowerTwo2(n/2)));
    }

    //Bit count
    public static boolean isPowerTwo3(int n){
        return n > 0 && Integer.bitCount(n) == 1;
    }

    //Bit operation
    public static boolean isPowerTwo(int n){
        return n>0 && (n & n -1) == 0;
    }

    public static void main(String[] args) {

        int n = 1;
        System.out.println(isPowerTwo3(n));
    }
}
