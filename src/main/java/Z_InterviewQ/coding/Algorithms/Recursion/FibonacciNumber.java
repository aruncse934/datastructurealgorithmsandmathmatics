package Z_InterviewQ.coding.Algorithms.Recursion;

public class FibonacciNumber {

    //Iterative tc- O(n), sc o(1)
    public static  int fib(int n){
        if(n <= 1)
            return n;
        int a = 0, b = 1;
        while(n-- > 1){
            int sum = a+b;
            a = b;
            b = sum;
        }
        return b;
    }

    //Recursion tc O(2^n)
    public static int fibRec(int n){
        if(n <=1){
            return n;
        }
        return fib(n-1)+fib(n-2);
    }

    //DP - Bottom Up Approach O(n)
    public static int fibBottom(int n){
        if (n <= 1)
            return n;

        int[] fib_cache = new int[n+1];
        fib_cache[1] = 1;
        for(int i = 2; i <= n; i++){
            fib_cache[i] = fib_cache[i-1]+fib_cache[i-2];
        }
        return fib_cache[n];
    }

    public static void main(String[] args) {
      int n = 1000;
        System.out.println(fib(n));
        System.out.println(fibRec(n));
        System.out.println(fibBottom(n));
    }
}

