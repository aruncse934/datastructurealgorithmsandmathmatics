package Z_InterviewQ.coding.Algorithms.Recursion;

class ListNode{
    int val;
    ListNode next;

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

}
public class ReverseLinkedList {

    //Iterative
    public static ListNode reverseLinkedList(ListNode head){
        ListNode newHead = null;
        while (head != null){
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }


    // // recursive solution
    public static ListNode reverseLists(ListNode head) {
        return reverseListInt(head, null);
    }
    private static ListNode reverseListInt(ListNode head, ListNode newHead) {
        if (head == null)
            return newHead;
        ListNode next = head.next;
        head.next = newHead;
        return reverseListInt(next, head);
    }

    public static void printList(ListNode head){
        ListNode ptr = head;
        while(ptr !=null){
            System.out.print(ptr.val+" -> " );
            ptr = ptr.next;
        }
        System.out.println("null");
    }

    public static void main(String[] args) {
        int[] keys = {1,2,3,4,5,6};
        ListNode head = null;
        for(int i = keys.length-1; i >= 0; i--){
            head = new ListNode(keys[i], head);
        }
        head = reverseLists(head);
        printList(head);
    }
}




