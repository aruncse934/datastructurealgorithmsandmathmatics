package Z_InterviewQ.coding.Algorithms.Dp;

import java.util.Arrays;

public class CountingBits {


    public static int[] countsBits(int num){
        int[] f = new int[num + 1];
        for(int i = 1; i <= num; i++)
            f[i] = f[i >> 1] + (i & 1);
        return f;
    }

    //dp
    public static int[] countBits(int num){
        int[] result = new int[num + 1];
        int offset = 1;
        for(int i = 1; i < num + 1; ++i){
            if(offset * 2 == i){
                offset *= 2;
            }
            result[i] = result[i - offset] + 1;
        }
        return result;
    }

    public static void main(String[] args) {
        int num = 5;
        System.out.println(Arrays.toString(countBits(num)));
    }
}
