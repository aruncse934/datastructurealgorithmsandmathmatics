package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

import java.util.PriorityQueue;

public class LastStoneWeight {

    public static int lastStoneWeight(int[] stones){
        PriorityQueue<Integer> pq = new PriorityQueue<>((a,b)-> b-a);
        for(int x : stones)
            pq.offer(x);
        while(pq.size() > 1)
            pq.offer(pq.poll() - pq.poll());
        return pq.poll();
    }

    public static void main(String[] args) {

        int[] stones = {2,7,4,1,8,1};
        System.out.println(lastStoneWeight(stones));
    }
}
