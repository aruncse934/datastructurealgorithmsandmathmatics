package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

import java.util.Arrays;
import java.util.PriorityQueue;

public class RelativeRanks {

    public static String[] findRelativeRanks(int[] score){
        int n = score.length;
        String[] res = new String[n];

        PriorityQueue<Integer> pq = new PriorityQueue<>((a,b) -> score[b]-score[a]);

        for(int i = 0; i < n; i++){
            pq.add(i);
        }

        int i =1;
        while (!pq.isEmpty()){
            int idx  = pq.poll();

            if(i > 3){
                res[idx] = Integer.toString(i);
            }else if(i == 1){
                res[idx] = "Gold Medal";
            }else if(i == 2){
                res[idx] = "Silver Medal";
            }else if(i == 3){
                res[idx] = "Bronze Medal";
            }
            i++;
        }
        return res;
    }


    //

    public static String[] findRelativeRank(int[] score){
        int[][] pair = new int[score.length][2];
        for(int i = 0; i < score.length; i++){
            pair[1][0] = score[i];
            pair[i][1] = i;
        }

        Arrays.sort(pair, (a,b)-> (b[0] - a[0]));
        String[] result = new String[score.length];

        for(int i = 0; i < score.length;i++){
            if(i == 0){
                result[pair[i][1]] = "Gold Medal";
            }else if(i == 1){
                result[pair[i][1]] = "Silver Medal";
            }else if(i == 2){
                result[pair[i][1]] = "Bronze Medal";
            }else {
                result[pair[i][1]] = (i+1)+"";
            }
        }
        return result;
    }
    public static void main(String[] args) {

        int[] score = {10,3,8,9,4};

        System.out.println(Arrays.toString(findRelativeRanks(score)));

    }
}
