package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

import java.util.PriorityQueue;

public class LargestNumberAfterDigitSwapsParity {

    public static int largestNumberAfterDigitSwapsParity(int num){
        PriorityQueue<Integer> pq1 = new PriorityQueue<>();
        PriorityQueue<Integer> pq2 = new PriorityQueue<>();

        int x = num;
        while (num > 0){
            int curr = num % 10;
            if(curr % 2 == 1){
                pq1.add(curr);
            }else {
                pq2.add(curr);
            }
            num /= 10;
        }

        StringBuilder sb = new StringBuilder();
        num = x;

        while (num > 0){
            int cur = num%10;
            if(cur%2 == 1)
                sb.insert(0,pq1.poll());
            else
                sb.insert(0,pq2.poll());

             num /= 10;
        }
        return Integer.parseInt(sb.toString());
    }

    //
    public static int largestNumberAfterDigitSwapsParity1(int num){
        char[] c = String.valueOf(num).toCharArray();
        for(int i =0; i < c.length; i++)
            for(int j = i+1; j < c.length; j++)
                if(c[j] > c[i] && (c[j] - c[i])%2 == 0){
                    char temp = c[i];
                    c[i] = c[j];
                    c[j] = temp;
                }
        return Integer.parseInt(new String(c));
    }
    public static void main(String[] args) {
        int num = 1234;

        System.out.println(largestNumberAfterDigitSwapsParity(num));
    }
}
