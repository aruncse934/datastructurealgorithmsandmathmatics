package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

public class HeapSort {

    //Heap sort
    public static void heapSort(int[] arr){
     int n = arr.length;

     for(int i = n/2-1; i >= 0; i--){
         heapify(arr, n, i);
     }

     for(int i = n - 1; i >= 0; i--){
         int temp = arr[0];
         arr[0] = arr[i];
         arr[i] = temp;

         heapify(arr, i, 0);
     }
    }
    public static void heapify(int[] arr, int n, int index){

        int largest = index;
        int left = 2 * index + 1;
        int right = 2 * index + 2;

        if(left < n && arr[left] > arr[largest])
            largest = left;
        if(right < n && arr[right] > arr[largest])
            largest = right;

        if(largest != index){
            int temp = arr[index];
            arr[index] = arr[largest];
            arr[largest] = temp;

            heapify(arr,n,largest);
        }

    }
    static void buildHeap(int[] arr) {
        int n = arr.length;
        int startIdx = (n / 2) - 1;
        for (int i = startIdx; i >= 0; i--) {
            heapify(arr, n, i);
        }
    }

    public static void printArray(int[] arr){
        int n = arr.length;
        for(int i = 0 ; i < n; i++){
            System.out.print(arr[i]+ " ");
        }
    }

    public static void main(String[] args) {

        int[] arr = {10,9,8,7,6,5,4,3,2,1};
        int n = args.length;
        heapSort(arr);
     //   buildHeap(arr);
        printArray(arr);
    }
}
