package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

import java.util.*;
import java.util.stream.Collectors;

public class Top3SmallestNnumber {

    public static List<Integer> heapTop3(List<Integer> arr){
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int x : arr){
            pq.offer(x);
        }
        List<Integer> res = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            res.add(pq.poll());
        }
        return res;
    }

    public static List<String> splitWords(String s){
        return s.isEmpty() ? List.of() : Arrays.asList(s.split(" "));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Integer> arr = splitWords(sc.nextLine()).stream().map(Integer :: parseInt).collect(Collectors.toList());
        sc.close();
        List<Integer> res = heapTop3(arr);
        System.out.println(res.stream().map(String::valueOf).collect(Collectors.joining(" ")));

    }
}
