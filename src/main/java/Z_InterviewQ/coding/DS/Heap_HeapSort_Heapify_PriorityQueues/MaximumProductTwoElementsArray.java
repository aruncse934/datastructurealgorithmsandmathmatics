package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

/*Given the array of integers nums, you will choose two different indices i and j of that array. Return the maximum value of (nums[i]-1)*(nums[j]-1).
 */
public class MaximumProductTwoElementsArray {

    public static int maximumProduct(int[] nums){
        int max1 = Integer.MIN_VALUE;
        int max2 = max1;

        for(int n : nums){
            if(n > max1){
                max2 = max1;
                max1 = n;
            }else if(n > max2){
                max2 = n;
            }
        }
        return (max1 - 1)*(max2 - 1);
    }

    public static void main(String[] args) {

        int[] nums = {3,4,5,2};
        System.out.println(maximumProduct(nums));

    }
}
