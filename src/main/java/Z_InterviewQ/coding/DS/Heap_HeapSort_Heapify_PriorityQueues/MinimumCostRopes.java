package Z_InterviewQ.coding.DS.Heap_HeapSort_Heapify_PriorityQueues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class MinimumCostRopes {
    static class FastReader{
        BufferedReader br;
        StringTokenizer st;

        public FastReader(){
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next(){
            while (st == null || !st.hasMoreElements()){
                try{ st = new StringTokenizer(br.readLine()); } catch (IOException e){ e.printStackTrace(); }
            }
            return st.nextToken();
        }

        String nextLine(){
            String str = "";
            try{ str = br.readLine(); } catch (IOException e) { e.printStackTrace(); }
            return str;
        }

        Integer nextInt(){
            return Integer.parseInt(next());
        }

        Long nextLong(){
            return Long.parseLong(next());
        }
    }
    //Function to return the minimum cost of connecting the ropes.
   public static long minCost(long arr[], int n)
    {
        PriorityQueue<Long> pq = new PriorityQueue<>();
        for(long x : arr){
            pq.offer(x);
        }
        long sum = 0;
        long res = 0;

        for(int i = 0; i< n-1; i++){
            sum = pq.remove() + pq.remove();
            res += sum;
            pq.offer(sum);
        }
        return res;
    }

    public static void main(String[] args) {

        long[] arr = {4, 3, 2, 6};
        int n = args.length;
        System.out.println(minCost(arr,n));
    }
}
/*Input:
n = 4
arr[] = {4, 3, 2, 6}
Output:
29*/