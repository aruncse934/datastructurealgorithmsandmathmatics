package Z_InterviewQ.coding.DS.arrays;

import java.util.HashSet;
import java.util.Set;

public class ContainsDuplicate {

    public static boolean containsDuplicate(int[] nums){
        Set<Integer> set = new HashSet<>();
        for(int i : nums)
            if (!set.add(i))
                return true;
            return false;
    }

    public static void main(String[] args) {

        int[] nums = {1,2,3,4};
        System.out.println(containsDuplicate(nums));
    }
}
/*

public boolean containsDuplicate(int[] nums) {
    Set<Integer> seen = new HashSet<>();
    return Arrays.stream(nums).anyMatch(num -> !seen.add(num));
}
*/