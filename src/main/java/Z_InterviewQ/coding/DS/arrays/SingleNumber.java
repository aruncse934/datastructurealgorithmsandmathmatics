package Z_InterviewQ.coding.DS.arrays;

public class SingleNumber {

    public static int singleNumber(int[] nums){
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
           result = result^nums[i];
        }
        return result;
    }
    public static void main(String[] args) {
        int[] nums = {2,2,1};
        System.out.println(singleNumber(nums));
    }
}
/*public int singleNumber(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            nums[0] ^= nums[i];
        }
        return nums[0];
    }*/