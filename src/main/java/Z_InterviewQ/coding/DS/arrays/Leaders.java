package Z_InterviewQ.coding.DS.arrays;

public class Leaders {

    public void findLeadersInArray(int arr[], int size){
        int max_from_right = arr[size];
        System.out.println(max_from_right + " ");
        for(int i = size-2; i >= 0; i--){
            if(max_from_right < arr[i]){
                max_from_right = arr[i];
                System.out.print(max_from_right + " ");
            }
        }
    }

    public static void main(String[] args) {
        Leaders leaders = new Leaders();
        int arr[] = new int[]{16, 17, 4, 3, 5, 2};
        int n = args.length;
        leaders.findLeadersInArray(arr,n);

    }
}
