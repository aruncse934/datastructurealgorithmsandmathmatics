package Z_InterviewQ.coding;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateString {

    public static void findDuplicateString(String s){
       char ch[] = s.toCharArray();
        Map<Character,Integer> charMap = new HashMap<>();
        for(Character c : ch){
           if(charMap.containsKey(c)){
               charMap.put(c,charMap.get(c)+1);
           }else {
               charMap.put(c,1);
           }
        }

        Set<Map.Entry<Character,Integer>> entrySet = charMap.entrySet();
        System.out.printf("List of duplicate characters in String '%s' %n", s);
        for(Map.Entry<Character,Integer> entry : entrySet){
            if(entry.getValue() >1){
                System.out.printf("%s : %d %n", entry.getKey(), entry.getValue());

            }
        }
    }
    public static void main(String[] args) {
        String s ="Programming";
        findDuplicateString(s);
    }
}

