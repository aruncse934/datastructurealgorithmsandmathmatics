package Z_InterviewQ.coding.String;

import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioning {

    //Backtracking:
    public static List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<List<String>>();
        dfs(0, result, new ArrayList<String>(), s);
        return result;
    }

    public static void dfs(int start, List<List<String>> result, List<String> currentList, String s) {
        if (start >= s.length())
            result.add(new ArrayList<String>(currentList));
        for (int end = start; end < s.length(); end++) {
            if (isPalindrome(s, start, end)) {
                currentList.add(s.substring(start, end + 1));
                dfs(end + 1, result, currentList, s);
                currentList.remove(currentList.size() - 1);
            }
        }
    }

    public static boolean isPalindrome(String s, int low, int high) {
        while (low < high) {
            if (s.charAt(low++) != s.charAt(high--))
                return false;

        }
        return true;
    }
   //Backtracking with Dynamic Programming
   public static List<List<String>> partitions(String s){
       int len = s.length();
       boolean[][] dp = new boolean[len][len];
       List<List<String>> result = new ArrayList<>();
       dfss(result,s,0, new ArrayList<>(), dp);
       return result;
   }
   public static void dfss(List<List<String>> result, String s, int start, List<String> currentList, boolean[][] dp){
        if(start >= s.length())
            result.add(new ArrayList<>(currentList));
        for(int end = start; end < s.length(); end++){
            if(s.charAt(start) == s.charAt(end) && (end - start <= 2 || dp[start + 1][end - 1])){
                dp[start][end] = true;
                currentList.add(s.substring(start, end + 1));
                dfss(result, s, end + 1, currentList, dp);
                currentList.remove(currentList.size()-1);
            }
        }
   }

    public static void main(String[] args) {
        String s = "aab";

        System.out.println(partition(s));
        System.out.println(partitions(s));
    }
}
