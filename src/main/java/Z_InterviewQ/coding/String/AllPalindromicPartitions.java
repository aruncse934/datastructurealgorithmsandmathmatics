package Z_InterviewQ.coding.String;

public class AllPalindromicPartitions {

    public static boolean isPalin(String input, int begin, int end){
        while(begin < end){
            if(input.charAt(begin) != input.charAt(end))
                return false;
            begin++;
            end--;
        }
        return true;
    }
    public static void printPart(String input, String output, int begin, int end){
        while(begin == end) {

            System.out.println(output);
            return;
        }
        int n = input.length();
        String delimiter = "-";
        for(int i = begin; i< end; i++){
            if(isPalin(input,begin,i)){
                if(i+1 == n){
                    delimiter = "";
                }
                printPart(input, output+ input.substring(begin, i+1)+delimiter, i+1, end);
            }
        }
    }

    public static void main(String[] args) {
        String input = "abcd";
        String output = "";
        int begin = 0;
        int end = input.length();
        printPart(input,output,begin,end);
    }
}
