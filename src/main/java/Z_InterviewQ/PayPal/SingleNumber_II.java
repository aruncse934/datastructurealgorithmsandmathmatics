package Z_InterviewQ.PayPal;

/*Example 1:

Input: nums = [2,2,3,2]
Output: 3
Example 2:

Input: nums = [0,1,0,1,0,1,99]
Output: 99*/
public class SingleNumber_II { // Find the element that appears once
    public static void main(String[] args) {
        int nums[] = {12, 1, 12, 3, 12, 1, 1, 2, 3, 2, 2, 3, 7};
        System.out.println(singleNumber(nums));
    }

    //Bitwise Operation
    public static int singleNumber(int[] nums) {
        int x1 = 0, x2 = 0, mask = 0;

        for (int i : nums) {
            x2 ^= x1 & i;
            x1 ^= i;
            mask = ~(x1 & x2);
            x2 &= mask;
            x1 &= mask;
        }

        return x1;
    }
}
