package Z_InterviewQ.PayPal;

/*For example:

Input 1:
    A = [1, 2, 3, 4, -10]

Output 1:
    10

Explanation 1:
    The subarray [1, 2, 3, 4] has the maximum possible sum of 10.

Input 2:
    A = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

Output 2:
    6

Explanation 2:
    The subarray [4,-1,2,1] has the maximum possible sum of 6.*/
public class MaximumSubarray { // Max Sum Contiguous Subarray
    public static void main(String[] args) {
        int ar[] = {-2 -3 -1 -4 -6};
        System.out.println(maxSubarray(ar));
    }
    //dp
    public static int maxSubarray(int A[]){
        int maxCurr = A[0];
        int maxEnd  = A[0];
        for(int i=1;i<A.length;i++){
            maxEnd = Math.max(maxEnd+A[i],A[i]);
            maxCurr = Math.max(maxCurr,maxEnd);
        }
        return maxCurr;
    }
}

/*public int maxSubArray(int[] A) {
        int n = A.length;
        int[] dp = new int[n];//dp[i] means the maximum subarray ending with A[i];
        dp[0] = A[0];
        int max = dp[0];

        for(int i = 1; i < n; i++){
            dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
            max = Math.max(max, dp[i]);
        }

        return max;
}*/

/* THe Max subarray
public static void main(String[] args) {
		final Scanner in = new Scanner(System.in);
		final int T = in.nextInt();
		for(int i = 0; i  < T; i++) {
			int maxSumElems = Integer.MIN_VALUE;
			int maxSumOfContiElems = Integer.MIN_VALUE;
			int sumOfPrevContiElems = 0;

			final int N = in.nextInt();
			for(int j = 0; j < N; j++) {
				final int elem = in.nextInt();

				if(sumOfPrevContiElems < 0) {
					sumOfPrevContiElems = 0;
				}

				sumOfPrevContiElems+= elem;

				if(sumOfPrevContiElems > maxSumOfContiElems) {
					maxSumOfContiElems = sumOfPrevContiElems;
				}

				if(elem > 0) {
					if(maxSumElems < 0) {
						maxSumElems = elem;
					} else {
						maxSumElems += elem;
					}
				} else {
					if(maxSumElems < 0) {
						maxSumElems = Math.max(elem, maxSumElems);
					}
				}

			}

			System.out.println(maxSumOfContiElems + " " + maxSumElems);
		}

		in.close();
	}*/