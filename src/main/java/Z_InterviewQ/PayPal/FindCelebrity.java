package Z_InterviewQ.PayPal;

/*Example1

Input:
2 // next n * (n - 1) lines
0 knows 1
1 does not know 0
Output: 1
Explanation:
Everyone knows 1,and 1 knows no one.
Example2

Input:
3 // next n * (n - 1) lines
0 does not know 1
0 does not know 2
1 knows 0
1 does not know 2
2 knows 0
2 knows 1
Output: 0
Explanation:
Everyone knows 0,and 0 knows no one.
0 does not know 1,and 1 knows 0.
2 knows everyone,but 1 does not know 2.*/
class Relation {
    boolean knows(int a, int b) {

        return false;
    }
}

public class FindCelebrity extends Relation {

    public int findCelebrity(int n) {
        if (n <= 0) {
            return -1;
        }
        if (n == 1) {
            return 0;
        }
        int candi = 0;
        for (int i = 1; i < n; ++i) {
            if (knows(candi, i)) {
                candi = i;
            }
        }
        // check candi according to the def of celebrity
        for (int i = 0; i < n; ++i) {
            if (i != candi && !knows(i, candi)) {
                return -1;
            }
            if (i != candi && knows(candi, i)) {
                return -1;
            }
        }
        return candi;
    }
}
