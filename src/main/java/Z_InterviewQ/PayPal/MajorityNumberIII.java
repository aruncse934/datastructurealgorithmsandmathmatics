package Z_InterviewQ.PayPal;

import java.util.*;

public class MajorityNumberIII {
    public static void main(String[] args) {
        List<Integer> nums =Arrays.asList(3, 4, 4, 5, 5, 5, 5);// {3,1,2,3,2,3,3,4,4,4};
        int k = 4;
        System.out.println(majorityNumber(nums,k));
    }
    public static int majorityNumber(List<Integer> nums, int k) {
        // write your code here
        int size = nums.size() / k;
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        for (Integer num : map.keySet()) {
            if (map.get(num) > size){
                return num;
            }
        }
        return 0;
    }

    // bf
    static void NDivKWithFreq(int arr[], int N, int K)
    {
        // Sort the array, arr[]
        Arrays.sort(arr);

        // Traverse the array
        for (int i = 0; i < N;) {

            // Stores frequency of arr[i]
            int cnt = 1;

            // Traverse array elements which
            // is equal to arr[i]
            while ((i + 1) < N
                    && arr[i] == arr[i + 1]) {

                // Update cnt
                cnt++;

                // Update i
                i++;
            }

            // If frequency of arr[i] is
            // greater than (N / K)
            if (cnt > (N / K)) {

                System.out.print(arr[i]+ " ");
            }
            i++;
        }
    }
}
