package Z_InterviewQ.PayPal;import java.util.*;

/*Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
Output: 6
Explanation: The maximal rectangle is shown in the above picture.
Example 2:

Input: matrix = []
Output: 0
Example 3:

Input: matrix = [["0"]]
Output: 0
Example 4:

Input: matrix = [["1"]]
Output: 1
Example 5:

Input: matrix = [["0","0"]]
Output: 0*/
public class MaximalRectangle {

    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0) return 0;
        int m = matrix.length, n = matrix[0].length, maxArea = 0;
        int[] left = new int[n];
        int[] right = new int[n];
        int[] height = new int[n];
        Arrays.fill(right, n - 1);
        for (int i = 0; i < m; i++) {
            int rB = n - 1;
            for (int j = n - 1; j >= 0; j--) {
                if (matrix[i][j] == '1') {
                    right[j] = Math.min(right[j], rB);
                } else {
                    right[j] = n - 1;
                    rB = j - 1;
                }
            }
            int lB = 0;
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    left[j] = Math.max(left[j], lB);
                    height[j]++;
                    maxArea = Math.max(maxArea, height[j] * (right[j] - left[j] + 1));
                } else {
                    height[j] = 0;
                    left[j] = 0;
                    lB = j + 1;
                }
            }
        }
        return maxArea;
    }

}

/*public int maximalRectangle(boolean[][] matrix) {
        // write your code here
        if(matrix == null || matrix.length == 0 ||
          matrix[0] == null || matrix[0].length == 0){
            return 0;
          }

        int m = matrix.length;
        int n = matrix[0].length;
        int max = 0;

        int[][] histogram = new int[m][n];

        for(int i = 0; i < m; i++){
          for(int j = 0; j < n; j++){
            if(matrix[i][j] == false){
              histogram[i][j] = 0;
            } else {
              histogram[i][j] = i == 0 ? 1 : histogram[i - 1][j] + 1;
            }
          }
        }

        for(int i = 0; i < m; i++){
          int local_max = largestHistogram(histogram[i]);

          max = Math.max(local_max, max);
        }

        return max;
    }

    private int largestHistogram(int[] height){
      Stack<Integer> stack = new Stack<>();
      int max = 0;

      for(int i = 0; i <= height.length; i++){
        int cur_h = i == height.length ? -1 : height[i];

        while(!stack.isEmpty() && cur_h <= height[stack.peek()]){
          int h = height[stack.pop()];
          int w = stack.isEmpty() ? i : i - stack.peek() - 1;

          max = Math.max(h * w, max);
        }

        stack.push(i);
      }

      return max;
    }*/
