package Z_InterviewQ.PayPal;

import java.util.*;

/*
Query on Queues
You're given an array containing N integers and you have to answer K queries. Each query contains an integer X which is the index of the i th ( 1 based index) element of the queue

Write a program to determine the following for each query

The number of segments containing the index X as the leftmost or the rightmost element. and the number at the index X is >= each element of the first segment.
Example
Segment formation : You have 3 numbers 1, 2 and 3
The possible segments for 3 are [3], [3,2], [3,2,1]
The possible segments for 2 are [2], [2,1]
Sample input 1
4 2
4 2 1 3
1
4

Sample output 1
4
3

Sample input 2
5 2
4 2 3 5 1
1
3

Sample output 2
3
2*/
public class QueryOnQueues {

    public int[] solve(int N, int[] A, int K, int[] Q)
    {
        int len = N;
        int[] left = new int[len];
        int[] right = new int[len];
        Stack<Integer> stack = new Stack<>();
        Arrays.fill( left, -1);
        Arrays.fill( right, -1);
        for( int i= len-1; i >= 0; i--) {
            if( !stack.isEmpty()) {
                while( !stack.isEmpty() && A[stack.peek()] <= A[i] ) {
                    stack.pop();
                }
            }
            right[i] = stack.isEmpty() ? -1 : stack.peek();
            stack.push(i);
        }
        stack.clear();
        for( int i= 0; i < len; i++) {
            if( !stack.isEmpty()) {
                while( !stack.isEmpty() && A[stack.peek()] <= A[i] ) {
                    stack.pop();
                }
            }
            left[i] = stack.isEmpty() ? -1 : stack.peek();
            stack.push(i);
        }
        int[] result = new int[K];
        for(int i=0; i<K; i++ )
        {
            int ans = 0;
            int index = Q[i]-1;
            if( left[index] != -1)
            {
                ans += Math.abs( index - left[index]) -1;
            }
            else
            {
                ans += Math.abs( index );
            }
            if( right[index] != -1)
            {
                ans += Math.abs( index - right[index]) -1;
            }
            else
            {
                ans += Math.abs(index - len)-1;
            }
            ans++;
            result[i] = ans;
        }
        return result;
    }

}
