package Z_InterviewQ.PayPal;

/*Example 1:

Input: nums = [2,2,1]
Output: 1
Example 2:

Input: nums = [4,1,2,1,2]
Output: 4
Example 3:

Input: nums = [1]
Output: 1*/
public class SingleNumber {
    public static void main(String[] args) {
        int nums[] = {4,1,2,1,2};
        System.out.println(singleNum(nums));
    }

    //Bit Manipulation
    public static int singleNum(int nums[]){
        int a =0;
        for(int i :nums){
            a ^= i;
        }
        return a;
    }
}
