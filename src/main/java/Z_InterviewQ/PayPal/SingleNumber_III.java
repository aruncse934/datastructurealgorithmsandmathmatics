package Z_InterviewQ.PayPal;

import java.util.Arrays;
import java.util.Collections;

/*Example 1:

Input: nums = [1,2,1,3,2,5]
Output: [3,5]
Explanation:  [5, 3] is also a valid answer.
Example 2:

Input: nums = [-1,0]
Output: [-1,0]
Example 3:

Input: nums = [0,1]
Output: [1,0]*/
public class SingleNumber_III {
    public static void main(String[] args) {
        int nums[] = {1,2,1,3,2,5};
        System.out.println(Arrays.toString(singleNumber(nums)));
    }
    public static int[] singleNumber(int[] nums){
        int diff =0;
        for(int num : nums){
            diff ^= num;
        }
        diff &= - diff;

        int[] rets = {0, 0}; // this array stores the two numbers we will return
        for (int num : nums)
        {
            if ((num & diff) == 0) // the bit is not set
            {
                rets[0] ^= num;
            }
            else // the bit is set
            {
                rets[1] ^= num;
            }
        }
        return rets;
    }
}
