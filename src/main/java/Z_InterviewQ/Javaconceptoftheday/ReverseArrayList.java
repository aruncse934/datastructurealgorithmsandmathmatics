package Z_InterviewQ.Javaconceptoftheday;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class ReverseArrayList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Diamond");
        list.add("Gold");
        list.add("Iron");
        list.add("Copper");
        list.add("Silver");
        list.add("Nickel");
        list.add("Cobalt");
        list.add("Zinc");
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);

        System.out.println("======================================");
        LinkedList<Integer> ll = new LinkedList<>();
        ll.add(56);

        ll.add(67);

        ll.add(81);

        ll.add(41);

        ll.add(63);

        ll.add(21);

        ll.add(96);
        System.out.println(ll);
        Collections.reverse(ll);
        System.out.println(ll);

    }
}
