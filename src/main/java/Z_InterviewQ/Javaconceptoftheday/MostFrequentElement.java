package Z_InterviewQ.Javaconceptoftheday;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MostFrequentElement {
    public static void getMostFrequentElement(int num[]){
        HashMap<Integer,Integer> hm = new HashMap<>();
        for( int i : num){
            if(hm.containsKey(i)){
                hm.put(i, hm.get(i) + 1);
            }else {
                hm.put(i,1);
            }
        }
        int element = 0;
        int frequency = 1;
        Set<Map.Entry<Integer,Integer>> entrySet = hm.entrySet();
        for(Map.Entry<Integer, Integer> entry : entrySet){
            if(entry.getValue() > frequency){
                element = entry.getKey();
                frequency = entry.getValue();
            }
        }
        if(frequency > 1){
            System.out.println("Input Array : "+ Arrays.toString(num));
            System.out.println("The most frequent element : "+element);
            System.out.println("Its frequency : "+ frequency);
            System.out.println("==============================");
        }else {
            System.out.println("Input Array : "+Arrays.toString(num));
            System.out.println("No frequent element. All elements are unique.");
            System.out.println("==================================");
        }
    }

    public static void main(String[] args) {
        getMostFrequentElement(new int[]{4, 5, 8, 7, 4, 7, 6,7});
    }

}
