package Z_InterviewQ.Javaconceptoftheday;

public class CaseConversionLettersStrings {
    public static void main(String[] args) {
        String s = "abcABC";
        System.out.println(alphabetConversion(s));
        System.out.println(alphabetConversions(s));
    }

    public static String alphabetConversion(String str) {
        char[] array = str.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if ((array[i] >= 'a') && (array[i] <= 'z')) {
                array[i] = Character.toUpperCase(array[i]);
            } else if ((array[i] >= 'A') && (array[i] <= 'Z')) {
                array[i] = Character.toLowerCase(array[i]);
            }
        }
        String string = new String(array);

        return string;
    }

    //
    public static String alphabetConversions(String str) {
        char[] array = str.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (Character.isUpperCase(array[i])) {
                array[i] = Character.toLowerCase(array[i]);
            } else if (Character.isLowerCase(array[i])) {
                array[i] = Character.toUpperCase(array[i]);
            }
        }

        String string = new String(array);

        return string;
    }
}
