package Z_InterviewQ.Javaconceptoftheday;

public class LeadersArray {
    public static void printLeaders(int arr[], int size){
        int n = arr[size-1];
        System.out.print(n+" ");
        for(int i = size-1;i>=0;i--){
            if(n < arr[i]){
                n = arr[i];
                System.out.print(n+" ");
            }
        }
    }

    public static void main(String[] args) {

        int arr[]={16,17,4,3,5,2};
        int n = arr.length;
        printLeaders(arr,n);

    }
}
