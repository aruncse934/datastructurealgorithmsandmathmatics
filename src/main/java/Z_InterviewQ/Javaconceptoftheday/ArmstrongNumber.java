package Z_InterviewQ.Javaconceptoftheday;

public class ArmstrongNumber {
    public static void checkArmStrongNum(int num){
        int c_num = num;
        int noDigit = String.valueOf(num).length();
        int sum = 0;
        while (c_num !=0){
            int lastDigit = c_num%10;
            int lastDigitToPowerNoOfDigit = 1;
            for(int i = 0; i < noDigit; i++){
                lastDigitToPowerNoOfDigit = lastDigitToPowerNoOfDigit*lastDigit;
            }
            sum = sum+lastDigitToPowerNoOfDigit;
            c_num = c_num /10;
        }
        if(sum == num){
            System.out.println(num+" is an armstrong Number");
        }else {
            System.out.println(num+" is not an armstrong number");
        }
    }

    public static void main(String[] args) {
        checkArmStrongNum(153);
    }
}