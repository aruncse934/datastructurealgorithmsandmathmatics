package Z_InterviewQ.Javaconceptoftheday;

import java.util.Arrays;

/*Sample 1

If this parameter is aaAA11@@ then the output after returning the result is.

The number of uppercase letters is: 2
The number of lowercase letters is: 2
The number of digits is: 2
The number of other characters is: 2
Sample Two

If this parameter is %a1B2#@ then the output after returning the result is.

The number of uppercase letters is: 1
The number of lowercase letters is: 1
The number of digits is: 2
The number of other characters is: 3*/
public class CountNumberDifferentCharactersString {

    public static void main(String[] args) {
        String s ="aaAA11@@";
        System.out.println(Arrays.toString(statistics(s)));
        System.out.println(Arrays.toString(statistics2(s)));
    }
    /**
     * @param str:   str represents the string passed in
     * @return:  return means to return an array with
     *          the number of upper and lower case letters after counting
     */
    public static int[] statistics(String str) {
        int capitalLetters = 0;
        int lowercaseLetters = 0;
        int digital = 0;
        int otherCharacters = 0;
        int[] number = new int[4];
        char[] array = str.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if ((array[i] >= 'A') && (array[i] <= 'Z')) {
                capitalLetters++;
            } else if ((array[i] >= 'a') && (array[i] <= 'z')) {
                lowercaseLetters++;
            } else if ((array[i] >= '0') && (array[i] <= '9')) {
                digital++;
            } else {
                otherCharacters++;
            }
        }

        number[0] = capitalLetters;
        number[1] = lowercaseLetters;
        number[2] = digital;
        number[3] = otherCharacters;

        return number;
    }

    //
    public int[] statistics1(String str) {
        int capitalLetters = 0;
        int lowercaseLetters = 0;
        int digital = 0;
        int otherCharacters = 0;
        int[] number = new int[4];
        char[] array = str.toCharArray();

        for (int i = 0; i < array.length; i++) { // 遍历整个字符串
            if (Character.isUpperCase(array[i])) { // 使用 if else 语句判断字符属于哪一类
                capitalLetters++;
            } else if (Character.isLowerCase(array[i])) {
                lowercaseLetters++;
            } else if (Character.isDigit(array[i])) {
                digital++;
            } else {
                otherCharacters++;
            }
        }

        number[0] = capitalLetters; // 最后统计答案
        number[1] = lowercaseLetters;
        number[2] = digital;
        number[3] = otherCharacters;

        return number;
    }

    //
    public static int[] statistics2(String str) {
        int[] number = new int[4];

        number[0] = str.replaceAll("[^A-Z]","").length(); // 最后统计答案
        number[1] = str.replaceAll("[^a-z]","").length();
        number[2] = str.replaceAll("[^0-9]","").length();
        number[3] = str.length() - number[0] - number[1] - number[2];

        return number;
    }

}
