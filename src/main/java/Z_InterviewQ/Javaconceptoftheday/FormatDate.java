package Z_InterviewQ.Javaconceptoftheday;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {
    public static void main(String[] args) {

        Date today = new Date();
        System.out.println("Today is : "+today);

        //Pattern 1 : dd/MM/yyyy (Ex : 10/09/2016)
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Today in dd/MM/yyyy format : "+simpleDateFormat.format(today));

        System.out.println("=================================================");

        //Pattern 2 : yyyy-MM-dd (Ex : 2016-09-10)
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("Today in yyyy-MM-dd format : "+ simpleDateFormat1.format(today));

        System.out.println("=================================================");
        //Pattern 3 : dd MMMM yyyy (Ex : 10 September 2016)
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd MMMM yyyy");
        System.out.println("Today in dd MMMM yyyy format : "+simpleDateFormat2.format(today));

        System.out.println("=================================================");
        //Pattern 4 : E, dd MMM yyyy (Ex : Sat, 10 Sep 2016)
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("E, dd MMM yyyy");
        System.out.println("Today in E, dd MMM yyyy format : "+simpleDateFormat3.format(today));

        System.out.println("=================================================");
        //Pattern 5 : dd-MMM-yyyy HH:mm:ss (Ex : 10-Sep-2016 18:40:47)
        SimpleDateFormat simpleDateFormat4 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        System.out.println("Today in dd-MMM-yyyy HH:mm:ss format : "+simpleDateFormat4.format(today));

        System.out.println("=================================================");
        //Pattern 6 : EEEE, MMM dd yyyy, hh:mm:ss a (Ex : Saturday, Sep 10 2016, 06:45:51 PM)
        SimpleDateFormat simpleDateFormat5 = new SimpleDateFormat("EEEE,MMM dd yyyy, hh:mm:ss a");
        System.out.println("Today in EEEE, MMM dd yyyy, hh:mm:ss a format : "+simpleDateFormat5.format(today));

        System.out.println("=================================================");
        //Pattern 7 : dd-MMM-yyyy HH:mm:ss z (Ex : 10-Sep-2016 18:49:53 IST)
        SimpleDateFormat simpleDateFormat6 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z");
        System.out.println("Today in dd-MMM-yyyy HH:mm:ss z format : "+simpleDateFormat6.format(today));

        System.out.println("=================================================");
        //Pattern 8 : dd-MMM-yyyy HH:mm:ss Z (Ex : 10-Sep-2016 19:01:39 +0530)
        SimpleDateFormat simpleDateFormat7 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss Z");
        System.out.println("Today in dd-MMM-yyyy HH:mm:ss Z format : "+simpleDateFormat7.format(today));

        
    }
}
/*  SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss Z");

        System.out.println("Today in dd-MMM-yyyy HH:mm:ss Z format : "+formatter.format(today));

    }
 */