package Z_InterviewQ.Javaconceptoftheday;

import java.util.Scanner;

public class NumberPatterns {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("How many rows you want in this Pattern? ");
        int rows = sc.nextInt();

        /*
        1
        1 2
        1 2 3
        1 2 3 4
        1 2 3 4 5
        1 2 3 4 5 6
        1 2 3 4 5 6 7
        * */
        System.out.println("Here is your pattern..");
        for(int i = 1; i<= rows ; i++){
            for(int j = 1; j<= i; j++){
                System.out.print(j+" ");
            }
            System.out.println();
        }

        System.out.println("============================================================");
        /*  1
            2 2
            3 3 3
            4 4 4 4
            5 5 5 5 5
            6 6 6 6 6 6
            7 7 7 7 7 7 7
         */

        System.out.println("Here is your pattern... ");
        for(int i = 1;i<= rows;i++){
            for(int j = 1; j<= i;j++){
                System.out.print(i+ " ");
            }
            System.out.println();
        }
        System.out.println("========================================================");
/*
1
1 2
1 2 3
1 2 3 4
1 2 3 4 5
1 2 3 4 5 6
1 2 3 4 5 6 7
1 2 3 4 5 6
1 2 3 4 5
1 2 3 4
1 2 3
1 2
1
*/
        System.out.println("Here is your pattern...");
        for(int i = 1; i<= rows ; i++){
            for(int j = 1; j<= i; j++){
                System.out.print(j+ " ");
            }
            System.out.println();
        }
        for(int i = rows-1; i <= 1; i--){
            for(int j = 1; j<= i; j++){
                System.out.print(j+" ");
            }
            System.out.println();
        }

      //  System.out.println("====================================================================");

    }
}
