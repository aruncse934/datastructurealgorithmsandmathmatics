package Z_InterviewQ.Javaconceptoftheday;

import java.util.Scanner;

public class DiamondPattern {
    public static void main(String[] args) {
        diamondPattern(5);

        Scanner sc =  new Scanner(System.in);
        System.out.println("How many Rows you want in your Diamond?");
        int noRow = sc.nextInt();
        int midRow = (noRow)/2;
        int row = 1;
        System.out.println("Here is your diamond : ");
        for(int i = midRow; i> 0; i--){
            for(int j = 1;j<= i;j++){
                System.out.print(" ");
            }
            for(int j =1; j <= row; j++){
                System.out.print(row+"* ");
            }
            System.out.println();
            row++;
        }
        for(int i = 0; i<= midRow;i++){
            for(int j = 1; j<= i; j++){
                System.out.print(" ");
            }
            for(int j = row ; j> 0 ; j--){
                System.out.print(row+"* ");
            }
            System.out.println();
            row--;
        }
    }

    public static void diamondPattern(int n){
     int sp = n/2, st=1;
     for(int i = 1; i <= n; i++){
         for(int j =1; j <=sp;j++)
             System.out.print(" ");
         int count = st/2 + 1;
         for(int k = 1; k <= st; k++){
             System.out.print(count);
             if(k <= st/2)
                 count--;
             else
                 count++;
         }
         System.out.println();
         if( i <= n/2){
             sp -= 1;
             st += 2;
         }
         else {
             sp += 1;
             st -= 2;
         }
      }
    }
}
