package Z_InterviewQ;

/* Save this in a file called Main.java to compile and test it */

/* Do not add a package declaration */

import java.io.PrintWriter;
import java.util.*;

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE */
/* You may add any imports here, if you wish, but only from the
   standard library */

/* Do not add a namespace declaration */

class Customers {
    public String customerName;
    public String productName;
    public Integer price;

    public Customers(String customerName, String productName, Integer price) {
        this.customerName = customerName;
        this.productName = productName;
        this.price = price;
    }
}

public class Test1 {
    public static List<String> processData(ArrayList<String> array) {
        /*
         * Modify this method to process `array` as indicated
         * in the question. At the end, return a List containing the
         * appropriate values
         *
         * Please create appropriate classes, and use appropriate
         * data structures as necessary.
         *
         * Do not print anything in this method.
         *
         * Submit this entire program (not just this method)
         * as your answer
         */
        Map<String,Integer> costMap = new HashMap<>();
        List<Customers> listData = new ArrayList<>();

        array.forEach(input->{
            String in [] = input.split(",");
            String cName = in[0].trim();
            String pName = in[3].trim();
            String price = in[4].trim();
            int price1 = Integer.parseInt(price.replace("Rs ","").trim());
            listData.add(new Customers(cName,pName,price1));
            if(costMap.containsKey(pName) && costMap.get(pName)<price1){
                costMap.put(pName,price1);
            }else if(costMap.containsKey(pName) && costMap.get(pName) >=price1){

            }else{
                costMap.put(pName,price1);
            }
        });

        Map<String,List<Boolean>> listMap = new HashMap<>();
        List<String> retVal = new ArrayList<String>();
        for(Customers c: listData){
            listMap.putIfAbsent(c.customerName, new ArrayList<>());
            if(costMap.get(c.productName) > c.price){
                listMap.get(c.customerName).add(true);
            }else {
                listMap.get(c.customerName).add(false);
            }
        }
        for (Map.Entry<String,List<Boolean>> e:listMap.entrySet()){
            if(!e.getValue().contains(false)){
                retVal.add(e.getKey());
            }
        }
        return retVal;
    }

    public static void main (String[] args) {
        ArrayList<String> inputData = new ArrayList<String>();
        String line;
        Scanner in = new Scanner(System.in);
        while(!in.hasNext("1"))
            inputData.add(in.nextLine());
        List<String> retVal = processData(inputData);
        PrintWriter output = new PrintWriter(System.out);
        for(String str: retVal)
            output.println(str);
        output.close();
    }
}
