package Z_InterviewQ.Google;
/*Example 1:
Input:
Gene1: "2T3G"
Gene2: "3T2G"
Output:
"4/5"
Explanation:
"TTTGG" and "TTGGG" have 4 position same gene, so "4/5"
Example 2:
Input:
Gene1 = "3T2G4A1C"
Gene2 = "6T1A2C1G"
Output:
"4/10"
Explanation:
"TTTGGAAAAC" and "TTTTTTACCG" hava 4 position gene same, so "4/10"*/
public class GeneSimilarity {
    public static void main(String[] args) {

    }

    public String GeneSimilarity(String Gene1, String Gene2) {

        return null;
    }
}
class Gene{
    private int index;
    private int count;
    private char[] gene;
    private char gene_base;

    public Gene() {
    }
    public Gene(String a){
       count = 0;
       index = 0;
       gene = a.toCharArray();
       gene_base = ' ';
    }



}
/**
class Gene{

    private int index;
    public int count;
    private char[] gene;
    private char gene_base;

    public Gene(){}
    public Gene(String a){
        count = 0;
        index = 0;
        gene = a.toCharArray();
        gene_base = ' ';
    }
    public void updateGene(){
        count = 0;
        gene_base = ' ';
        if(index >= gene.length){
            return;
        }

        while(index < gene.length && gene_base == ' '){
            if(gene[index] >= '0' && gene[index] <= '9'){
                count = count * 10 + (gene[index] - '0');
            }

            else{
                gene_base = gene[index];
            }
            index ++;
        }
    }
    public char get_base(){
        return gene_base;
    }
}


public class Solution {

public String GeneSimilarity(String Gene1, String Gene2) {
    // write your code here
    int same_time = 0, count_time = 0;

    Gene g1 = new Gene(Gene1);
    Gene g2 = new Gene(Gene2);

    g1.updateGene();
    g2.updateGene();

    count_time += g1.count;
    while(g1.get_base() != ' ' && g2.get_base() != ' '){
        if(g1.get_base() == g2.get_base()){
            same_time += Math.min(g1.count, g2.count);
        }

        if(g1.count > g2.count){
            g1.count -= g2.count;
            g2.updateGene();
        }
        else if(g1.count < g2.count){
            g2.count -= g1.count;
            g1.updateGene();
            count_time += g1.count;
        }
        else{
            g1.updateGene();
            g2.updateGene();
            count_time += g1.count;
        }
    }
    return String.valueOf(same_time) +  "/" + String.valueOf(count_time);
}
} */