package Z_InterviewQ.Google;

/*Input:
A = "99"
B = "111"
Output: "11010"
Explanation: because 9 + 1 = 10, 9 + 1 = 10, 0 + 1 = 1,connect them，so answer is "11010"

Input:
A = "2"
B = "321"
Output: "323"
Explanation: because 2 + 1 = 3, 2 + 0 = 2, 3 + 0 = 3, connect them，so answer is "323"*/
public class SumTwoStrings {
    public static void main(String[] args) {
        String A = "99";
        String B = "111";
        SumTwoStrings sumTwoStrings = new SumTwoStrings();
        System.out.println(sumTwoStrings.sumOfStr(A,B));
    }
    //
    public String sumOfStr(String A, String B){
        StringBuilder sb = new StringBuilder();
        if(A.length() > B.length()){
            sb.append(A.substring(0,A.length()- B.length()));
            A = A.substring(A.length() - B.length());
        }else {
            sb.append(B.substring(0,B.length() -A.length()));
            B = B.substring(B.length() - A.length());
        }
        for(int i = 0; i < A.length();i++){
            char digitA = A.charAt(i);
            char digitB = (i < B.length()) ? B.charAt(i) : '0';
            sb.append(getSum(digitA,digitB));
        }
        return sb.toString();
    }
    private String getSum(char digitA, char digitB){
        int sum = (digitA-'0') + (digitB - '0');
        return String.valueOf(sum);
    }

    //
    public String addStrings(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        for(int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0 || carry == 1; i--, j--){
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            sb.append((x + y + carry) % 10);
            carry = (x + y + carry) / 10;
        }
        return sb.reverse().toString();
    }
}
