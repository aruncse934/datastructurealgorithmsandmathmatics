package Z_InterviewQ.Collections.Sort;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
class Person{
    private long personId;
    private String name;
    private int age;
    private double salary;
    
}
public class ComparableNdComparator {

    public static void main(String[] args) {
       // List<Person> persons = new ArrayList<>();
       /* persons.add(new Person("Umesh Awasthi", 35));
        persons.add(new Person("Robert Hickle", 55));
        persons.add(new Person("John Smith", 40));
        persons.add(new Person("David", 23));
        persons.add(new Person("David", 63));*/

       List<Person> persons = Arrays.asList(
                new Person(3000, "David", 35, 25000),
                new Person(4000, "John", 55, 25000),
                new Person(5000, "Smith", 23, 25000),
                new Person(1000, "Umesh", 25, 30000),
                new Person(2000, "Robert", 30, 45000),
                new Person(6000, "John", 45, 25000));

        persons.sort(Comparator.comparing(Person::getName).thenComparingInt(Person::getAge));
        for(Person person : persons){
            System.out.println("Person Name Sorted By Name : " + person.getName());
        }

        persons.sort(Comparator.comparing(Person::getAge).thenComparing(Person::getName));
        for(Person person : persons){
            System.out.println("Person Name Sorted By Age:" + person.getName());
        }
        persons.stream().sorted(Comparator.comparing(Person::getPersonId).thenComparing(Person::getAge).thenComparing(Person::getSalary)).forEach(person -> System.out.println("Name : - " +person.getName()));

        // Creating comparator on fly:
        persons.stream().sorted((p1, p2) -> ((Long)p1.getPersonId()).compareTo(p2.getPersonId()))
                .forEach(person -> System.out.println("Person Name Sorted By Id:"+person.getName()));

        // Using Comparator.comparingLong() method(We have comparingDouble(), comparingInt() methods too):
        persons.stream().sorted(Comparator.comparingLong(Person::getPersonId)).forEach(person -> System.out.println(person.getName()));

        //Using Comparator.comparing() method(Generic method which compares based on the getter method provided):
        persons.stream().sorted(Comparator.comparing(Person::getPersonId)).forEach(person -> System.out.println(person.getName()));

        //We can do chaining too using thenComparing() method:
        persons.stream().sorted(Comparator.comparing(Person::getPersonId).thenComparing(Person::getAge)).forEach(person -> System.out.println(person.getName())); //Sorting by person id and then by age.
   }
}
