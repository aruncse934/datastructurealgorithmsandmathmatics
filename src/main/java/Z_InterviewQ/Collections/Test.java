package Z_InterviewQ.Collections;

import java.io.*;
import java.util.*;

class Test {
    public static void main(String args[] ) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int T = Integer.parseInt(br.readLine().trim());
        for(int t_i=0;t_i<T;t_i++)
        {
            String S1 = br.readLine();
            String S2 = br.readLine();
            String S3 = br.readLine();
            char C = S3.charAt(0);
            int I = Integer.parseInt(br.readLine().trim());
            solve(S1, S2, C, I);
        }
        wr.close();
        br.close();
    }
    static void solve(String S1, String S2, char C, int I) {
        //Solve and print your answer here
        Character ch = 'N';
        String substring = S1.substring(I);
        int fi = substring.indexOf(S2);
        if(fi == -1){
            System.out.println("Goodbye Watson.");
        }
        if(Character.isLetter(C)){
            System.out.println(I + fi);
        }
        String[] s = substring.split(" ");
        for(String w : s){
            if(S2.equals(w.trim())){
                System.out.println(I + substring.indexOf(S2));
            }
        }
        System.out.println("Goodbye Watson.");
    }
}