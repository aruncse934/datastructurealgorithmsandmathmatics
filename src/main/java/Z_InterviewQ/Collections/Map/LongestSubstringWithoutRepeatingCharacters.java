package Z_InterviewQ.Collections.Map;

import java.util.HashMap;
import java.util.Map;

/*Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.*/
public class LongestSubstringWithoutRepeatingCharacters {
    public static void main(String[] args) {
      String s = "pwwkew";
        System.out.println(lengthOfLongestSubstringWithoutRepeatingChar(s));
    }
    public static int lengthOfLongestSubstringWithoutRepeatingChar(String s){
        int n = s.length();
        int ans = 0;
        Map<Character,Integer> map = new HashMap<>();
        int i =0;
        for(int j =0; j<n;j++){
            if(map.containsKey(s.charAt(j))){
                i = Math.max(map.get(s.charAt(j)),i);
            }
            ans = Math.max(ans,j-i + 1);
            map.put(s.charAt(j),j+1);

        }
        return ans;
    }
}
