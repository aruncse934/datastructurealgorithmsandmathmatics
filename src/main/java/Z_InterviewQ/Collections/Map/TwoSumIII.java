package Z_InterviewQ.Collections.Map;

import java.util.*;

/*Example 1:

add(1); add(3); add(5);
find(4) // return true
find(7) // return false
*/
public class TwoSumIII {//two-sum-iii-data-structure-design

    public static void main(String[] args) {

    }

    private Map<Integer, Integer> map = new HashMap<>();

    // Add the number to an internal data structure. O(1)
    public void add(int number) {
        // Write your code here
        if (map.get(number) == null) {
            map.put(number, 1);
        } else {
            map.put(number, map.get(number) + 1);
        }
    }

    // Find if there exists any pair of numbers which sum is equal to the value. O(n)
    public boolean find(int value) {
        // Write your code here
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (map.get(value - entry.getKey()) != null) {
                if (value - entry.getKey() == entry.getKey()) {
                    return entry.getValue() > 1;
                }
                return true;
            }
        }
        return false;
    }
}
