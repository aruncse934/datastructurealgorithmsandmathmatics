package Z_InterviewQ.Collections;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {

    public static void main(String[] args) {

        Map<String,String> map = new HashMap<>();
        map.put("Name", "Arun");
        map.put("Domain","Arun.com");
        map.put("Location", "Bangalore");
        map.put("USA", "Washington");
        map.put("Australia", "Canberra");

        System.out.println("Arun Details : \n"+ map);

        //check if key is present  or Not
        if(map.containsKey("Location")){  //true or false
            System.out.println("yes");
        }
        else {
            System.out.println("No");
        }

        // check if key Spain is present
        if(!map.containsKey("Spain")) {
            // add entry if key already not present
            map.put("Spain", "Madrid");
        }

        System.out.println("Updated HashMap:\n" + map);
    }
}


