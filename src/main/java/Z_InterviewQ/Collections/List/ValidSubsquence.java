package Z_InterviewQ.Collections.List;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidSubsquence {
    public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {
        // Write your code here.
        int arrIdx = 0;
        int seqIdx = 0;
        while (arrIdx < array.size() && seqIdx < sequence.size()) {
            if (array.get(arrIdx).equals(sequence.get(seqIdx))) {
                seqIdx++;
            }
            arrIdx++;
        }
        return seqIdx == sequence.size();
    }
    public static boolean isValidSubsequence1(List<Integer> array, List<Integer> sequence) {
        // Write your code here.
        int arrIdx = 0;
        int seqIdx = 0;
        for(Integer value : array){
            if(seqIdx == sequence.size()){
                break;
            }
            if(sequence.get(seqIdx).equals(value)){
                seqIdx++;
            }
        }
        return seqIdx == sequence.size();
    }

}



class ProgramTest1 {

    @Test
    public void TestCase1() {
        List<Integer> array = Arrays.asList(5, 1, 22, 25, 6, -1, 8, 10);
        List<Integer> sequence = Arrays.asList(1, 6, -1, 10);
        assertEquals(true, ValidSubsquence.isValidSubsequence(array, sequence));
    }

    @Test
   // @DisplayName("Simple multiplication should work")
    public void TestCase2() {
        List<Integer> array = Arrays.asList(5, 1, 22, 25, 6, -1, 8, 10);
        List<Integer> sequence = Arrays.asList(1, 6, -1, 10);
        assertEquals(true, ValidSubsquence.isValidSubsequence1(array, sequence));
    }

    @Test
   // @DisplayName("Simple multiplication should work")
    public void TestCase3() {
        List<Integer> array = Arrays.asList(5, 1, 22, 25, 6, -1, 8, 10);
        List<Integer> sequence = Arrays.asList(1, 6, -1, 10, 11, 11, 11, 11);
        assertEquals(true, ValidSubsquence.isValidSubsequence1(array, sequence));
    }
}

