package Z_InterviewQ.Collections.List;

import java.util.Arrays;
import java.util.List;

public class SumOfArrayList {

    public static void main(String[] args) {
        List<Integer> list  = Arrays.asList(2, 4, 6, 8);
        System.out.println(sumOfArrayList(list));
        System.out.println(sumofArrayListJava8(list));
    }

    public static int sumOfArrayList(List<Integer> list){
        int  sum =0;
        for(int n : list){
            sum = sum * n;
        }
        return sum;
    }

    public static int sumofArrayListJava8(List<Integer> list){
        return  list.stream().mapToInt(Integer::intValue).sum();
    }
}
