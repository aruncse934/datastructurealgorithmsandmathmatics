package Z_InterviewQ.Collections.List;

import java.util.LinkedList;
import java.util.List;

/*Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]*/
public class LetterCombinationsPhoneNumber {

    public static void main(String[] args) {
        String s = "23";
        System.out.println(letterCombinations(s));

    }
    public static List<String> letterCombinations(String digits){
        LinkedList<String> ans = new LinkedList<>();
        if(digits.isEmpty())
            return ans;
        String[] mapping = new String[]{"0","1","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        ans.add("");
        for(int i = 0; i < digits.length();i++){
            int x =Character.getNumericValue(digits.charAt(i));
            while (ans.peek().length() ==i){
                String t = ans.remove();
                for(char s : mapping[x].toCharArray())
                    ans.add(t+s);
            }
        }
        return ans;
    }

}
/*public List<String> letterCombinations(String digits) {
		LinkedList<String> ans = new LinkedList<String>();
		if(digits.isEmpty()) return ans;
		String[] mapping = new String[] {"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		ans.add("");
		while(ans.peek().length()!=digits.length()){
			String remove = ans.remove();
			String map = mapping[digits.charAt(remove.length())-'0'];
			for(char c: map.toCharArray()){
				ans.addLast(remove+c);
			}
		}
		return ans;
	}*/