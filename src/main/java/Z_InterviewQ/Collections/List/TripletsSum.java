package Z_InterviewQ.Collections.List;

import java.util.*;

/*Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.



Example 1:

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
Example 2:

Input: nums = []
Output: []
Example 3:

Input: nums = [0]
Output: []*/
public class TripletsSum {
    public static void main(String[] args) {
        TripletsSum tripletsSum = new TripletsSum();
        int nums[] = {-1,0,1,2,-1,-4};
        System.out.println(tripletsSum.threeSums(nums));
        System.out.println(tripletsSum.threeSum(nums));
    }

    //LinkedList
    public List<List<Integer>> threeSums(int[] num) {
        Arrays.sort(num);
        List<List<Integer>> res = new LinkedList<>();
        for (int i = 0; i < num.length - 2; i++) {
            if (i == 0 || (i > 0 && num[i] != num[i - 1])) {
                int lo = i + 1, hi = num.length - 1, sum = 0 - num[i];
                while (lo < hi) {
                    if (num[lo] + num[hi] == sum) {
                        res.add(Arrays.asList(num[i], num[lo], num[hi]));
                        while (lo < hi && num[lo] == num[lo + 1]) lo++;
                        while (lo < hi && num[hi] == num[hi - 1]) hi--;
                        lo++;
                        hi--;
                    } else if (num[lo] + num[hi] < sum) lo++;
                    else hi--;
                }
            }
        }
        return res;
    }


    //ArrayList

    public List<List<Integer>> threeSum(int[] numbers){
        Arrays.sort(numbers);
        List<List<Integer>> res = new ArrayList<>();
        for(int i = 0; i< numbers.length; i++){
            if(i != 0 && numbers[i] == numbers[i-1]){
                continue;
            }
            findTwoSum(numbers,i,res);
        }
        return res;
    }
    private void findTwoSum(int[] numbers, int index, List<List<Integer>> res){
        int left = index +1;
        int right = numbers.length-1;
        int target = -numbers[index];
        while (left < right){
            int twoSum = numbers[left] + numbers[right];
            if(twoSum < target){
                left++;
            }else if(twoSum > target){
                right--;
            }else {
                List<Integer> triple = new ArrayList<>();
                triple.add(numbers[index]);
                triple.add(numbers[left]);
                triple.add(numbers[right]);
                res.add(triple);
                left++;
                right--;
                while (left < right && numbers[left] == numbers[left -1]){
                    left++;
                }
            }
        }
    }
}

