package AlgorithmicTrading.HighFrequencyTrading;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.*;

public class Test1 {
    /*//Write a function to find the longest common prefix string amongst an array of strings.
    //If there is no common prefix, return an empty string "".
    //
    //Example 1:
    //Input: strs = ["flower","flow","flight"]
    //Output: "fl"*/

    public static void commanString(String[] s){
        for(String str : s){
             char[] ch = str.toCharArray();
              for(int i = 0 ; i< ch.length; i++) {
                  for(int j = 0; j < ch.length; j++) {
                      if(ch[i] == ch[j]){
                          System.out.println(" ");
                      }
                  }
            }
            System.out.println(ch);
        }


    }
    public static void main(String[] args) {
      String[] s = {"flower","flow","flight"};
      commanString(s);
    }
    public static int findMinimumDays(List<Float> durations) {
        Collections.sort(durations, Collections.reverseOrder());

        int days = 0;
        int left = 0;
        int right = durations.size() - 1;

        // Greedy approach: Start from the longest and the shortest movies
        while (left <= right) {
            if (left == right) {
                // If there is only one movie remaining, watch it on a new day
                days++;
                break;
            }

            // Try to watch the longest and shortest movies together
            if (durations.get(left) + durations.get(right) <= 3.00) {
                left++;
            }

            // Watch the longest movie on a new day
            right--;
            days++;
        }

        return days;
    }

    @org.junit.jupiter.api.Test
    public void findMinimumDays() {
        List<Float> list = Arrays.asList(2.0f,1.01f,1.4f,2.4f,1.71f);
        System.out.println(findMinimumDays(list));
    }
}


@AllArgsConstructor
class Member{
    int id;
    List<Edge> edges;

    public Member(int id) {
        this.id =id;
        this.edges = new ArrayList<>();
    }
    /*public Member(int id){
        this.id =id;
        this.edges = new ArrayList<>();
    }*/
}
@AllArgsConstructor
class Edge{
    Member follower;
    Member following;
    int time;

   /* public Edge(Member follower, Member following, int time){
        this.follower = follower;
        this.following = following;
        this.time = time;
    }*/
}

class TestClass {

    public static int shortestTime(Member ninA, Member jsExB){
        PriorityQueue<Edge> pq = new PriorityQueue<>(Comparator.comparing(e-> e.time));
        Set<Member> set = new HashSet<>();
        pq.addAll(ninA.edges);

        while(!pq.isEmpty()){
            Edge currEdge = pq.poll();
            Member currMem = currEdge.following;
            if(set.contains(currMem)) continue;
            if(currMem == jsExB){
                return currEdge.time;
            }
            set.add(currMem);
            pq.addAll(currMem.edges);
        }
        return -1;

    }
    public static void main(String args[] ) throws Exception {

        Scanner scanner = new Scanner(System.in);

        int totalMembers = scanner.nextInt();
        List<Member> members = new ArrayList<>();

        for (int i = 0; i < totalMembers; i++) {
            members.add(new Member(scanner.nextInt()));
        }

        int totalEdges = scanner.nextInt();

        for (int i = 0; i < totalEdges; i++) {
            int followerId = scanner.nextInt() - 1; // Adjust for 0-based indexing
            int followingId = scanner.nextInt() - 1;
            int time = scanner.nextInt();

          //  Member follower = members.get(followerId);
          //  Member following = members.get(followingId);

          //  Edge edge = new Edge(follower, following, time);
          //  follower.edges.add(edge);
        }

        int ninjaAId = 7 - 1; // Adjust for 0-based indexing
        int jsExpertBId = 9 - 1; // Adjust for 0-based indexing

        Member ninjaA = members.get(ninjaAId);
        Member jsExpertB = members.get(jsExpertBId);

        int shortestTime = shortestTime(ninjaA, jsExpertB);
        System.out.println(shortestTime);

    }
}

/*4
2
5
7
9
4
2 9 2
7 2 3
7 9 7
9 5 1
7
9*/