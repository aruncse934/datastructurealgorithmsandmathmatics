package AlgorithmicTrading.HighFrequencyTrading;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Main {

    String[] solution(String[][] queries) {
        List<String> res = new ArrayList<>();
        for (String[] q : queries) {
            res.add(processQuery(q));
        }
        return res.toArray(new String[0]);
    }

    static Map<String, Account> accounts = new HashMap<>();

    public static String processQuery(String[] q) {
        String operation = q[0];
        switch (operation) {
            case "CREATE_ACCOUNT":
                return createAccount(q[2]);
            case "DEPOSIT":
                return deposit(q[2], Integer.parseInt(q[3]));
            case "PAY":
                return pay(q[2], Integer.parseInt(q[3]));
            default:
                return "";
        }
    }

    static String createAccount(String accountId) {
        if (accounts.containsKey(accountId)) {
            return "false";
        } else {
            accounts.put(accountId, new Account(accountId));
            return "true";
        }
    }

    static String deposit(String accountId, int amount) {
        Account account = accounts.get(accountId);
        if (account != null) {
            account.deposit(amount);
            return String.valueOf(account.getBalance());
        } else {
            return "";
        }
    }

    static String pay(String accountId, int amount) {
        Account account = accounts.get(accountId);
        if (account != null && account.pay(amount)) {
            return String.valueOf(account.getBalance());
        } else {
            return "";
        }
    }
}

class Account {
    private String accId;
    private int balance;

    public Account(String accountId) {
        this.accId = accountId;
        this.balance = 0;
    }

    public String getAccountId() {
        return accId;
    }

    public int getBalance() {
        return balance;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public boolean pay(int amount) {
        if (balance >= amount) {
            balance -= amount;
            return true;
        }
        return false;
    }
}



