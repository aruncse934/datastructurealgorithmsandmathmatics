package AlgorithmicTrading.HighFrequencyTrading;


import lombok.Getter;
import lombok.ToString;

import java.util.List;

enum Hobbies{
       CHESS,
       FOOTBALL,
       DANCE,
       TRAVELLING,
       CRICKET
}

/*this class is immutable*/
/*Create an immutable class Employee(id, name, list of interests) and create 3 employee objects and explain immutability.*/
@Getter
@ToString
public final class Employee {
    private final Integer id;
    private final String name;
    private final List<Hobbies> listOfinterest;

    public Employee(Integer id, String name, List<Hobbies> listOfinterest) {
        this.id = id;
        this.name = name;
        this.listOfinterest = listOfinterest;
    }

    


}
