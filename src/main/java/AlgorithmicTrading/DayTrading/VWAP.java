package AlgorithmicTrading.DayTrading;

import java.util.*;

public class VWAP {

    /*public double getVwap(double[] close, long[] volume){

        double vwap = 0.0;
        long totVol = 0;

        try{

            for(int i=close.length-2 ; i>(close.length-22); i--){
                double closePr = close[i];
                long vol = volume[i];
                totVol=totVol+vol;
                vwap = vwap+(closePr*vol);
            }
            vwap = vwap/totVol;

        }catch(Exception e){
            e.printStackTrace();
        }

        return vwap;
    }*/


       public static int[] pairOfEle(int[] arr, int val) {
           int n  = arr.length;
           for(int i = 0 ; i< n ; i++){
               for(int j = i+1; j <= n; j++){
                   if(arr[j]-arr[i] ==val ){
                       return new int[]{arr[i],arr[j]};
                   }
               }
           }
           return null;

       }

       public static int[] pairOEle(int[] arr, int val){     //Two   Hashing
           Map<Integer,Integer> map = new HashMap<>();
           for(int i = 0; i < arr.length; i++){
               map.put(i,map.getOrDefault(i,0) + 1);
           }
           for(int j = 0; j < arr.length; j++ ){
               int target = val - arr[j];
               if(map.containsKey(target) && map.get(target)  != j)  {
                   return new int[]{j, map.get(target)};
               }
           }
           return null;
       }

    public static int[] transformArray(int[] arr, int value) {
        List<Integer> transformedList = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            int diff = arr[i + 1] - arr[i];
            transformedList.add(arr[i]);
            transformedList.add(diff);
        }
        transformedList.add(arr[arr.length - 1]);

        int[] outputArray = transformedList.stream().mapToInt(Integer::intValue).toArray();
        return outputArray;
    }

    public static List<Pair> transformArray1(int[] arr, int value) {
        List<Pair> transformedList = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            int diff = arr[i + 1] - arr[i];
            transformedList.add(new Pair(arr[i], diff));
        }
        transformedList.add(new Pair(arr[arr.length - 1], value));

        return transformedList;
    }

    static class Pair {
        int first;
        int second;

        Pair(int first, int second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return "(" + first + "," + second + ")";
        }
    }



   
    public static void main(String[] args) {
       int[] arr = {1,3,4,1,2,3,4};
       int val = 2;
        System.out.println(Arrays.toString(pairOfEle(arr, val)));
    }
}

/*1,3,4,1,2,3,4
value. - 2
3-1=2
Nitish Kumar4:51 PM
4-2 = 2
Nitish Kumar4:53 PM
1,3
3,1
4,2
1,3
2,4*/