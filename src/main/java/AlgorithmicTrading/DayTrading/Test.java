package AlgorithmicTrading.DayTrading;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {
    //Find the occurance of letters in the sentence, print the maximum count of the letter and letter
    public static void main(String[] args) {    //a:3
        String s = "Haaacker";
        Map<String, Long> collect = Arrays.stream(s.split("\\s+"))
             //  .filter(i->i.startsWith("a"))
              //  .filter(i -> i.matches("[a-zA-Z]"))
                .filter(word -> word.startsWith("a"))
                .flatMap(word -> word.chars().mapToObj(c -> (char) c))
                .filter(Character::isLetter)
                .map(Character::toLowerCase)
                .map(Object::toString)
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));
        System.out.println(collect);
    }
}
