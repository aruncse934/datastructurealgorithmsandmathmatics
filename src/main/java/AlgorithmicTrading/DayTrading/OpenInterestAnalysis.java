package AlgorithmicTrading.DayTrading;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class OpenInterestAnalysis {

    private static final String API_KEY = "YOUR_API_KEY";
    private static final String SYMBOL = "AAPL"; // Example symbol

    public static void main(String[] args) {
        try {
            // Build the API URL
            String apiUrl = "https://www.nseindia.com/market-data/option-chain/query?function=TIME_SERIES_DAILY&symbol=" + SYMBOL + "&apikey=" + API_KEY;

            // Send HTTP request to the API and get the response
            URL url = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            // Read the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            // Parse the JSON response
            JSONObject json = new JSONObject(response.toString());
            JSONObject timeSeries = json.getJSONObject("Time Series (Daily)");

            // Get the latest two dates (current and previous)
            String currentDate = timeSeries.keys().next();
            String previousDate = timeSeries.keys().next();

            // Calculate the change in Open Interest
            double currentOpenInterest = timeSeries.getJSONObject(currentDate).getDouble("openInterest");
            double previousOpenInterest = timeSeries.getJSONObject(previousDate).getDouble("openInterest");
            double change = currentOpenInterest - previousOpenInterest;

            // Calculate the percentage change
            double percentageChange = (change / previousOpenInterest) * 100;

            // Print the results
            System.out.println("Current Open Interest: " + currentOpenInterest);
            System.out.println("Previous Open Interest: " + previousOpenInterest);
            System.out.println("Change: " + change);
            System.out.println("Percentage Change: " + percentageChange + "%");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
