package AlgorithmicTrading.OptionChain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

            public class OptionChainBankNifty {
                private static final String url = "https://www.nseindia.com/api/option-chain-indices?symbol=BANKNIFTY";
                private static final String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36 Edg/103.0.1264.37";
                private static final String acceptEncoding = "gzip, deflate, br";
                private static final String acceptLanguage = "en-GB,en;q=0.9,en-US;q=0.8";

                private static CookieManager cookieManager;

                static {
                    cookieManager = new CookieManager();
                    cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
                    CookieHandler.setDefault(cookieManager);
                }

                private static JSONObject getRequest(String urlString, String requestMethod, String userAgent, String acceptEncoding, String acceptLanguage) throws IOException {
                    URL url = new URL(urlString);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod(requestMethod);
                    connection.setRequestProperty("User-Agent", userAgent);
                    connection.setRequestProperty("Accept-Encoding", acceptEncoding);
                    connection.setRequestProperty("Accept-Language", acceptLanguage);

                    int responseCode = connection.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        StringBuilder response = new StringBuilder();
                        String line;
                        try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                            while ((line = reader.readLine()) != null) {
                                response.append(line);
                            }
                        }
                        return new JSONObject(response.toString());
                    } else {
                        throw new IOException("HTTP request failed. Response Code: " + responseCode);
                    }
                }

                private static JSONObject getOptionChainData() throws IOException {
                    JSONObject response = getRequest(url, "GET", userAgent, acceptEncoding, acceptLanguage);
                    return response.getJSONObject("records");
                }

                private static JSONArray getRawOptionData(JSONObject optionChainData) {
                    return optionChainData.getJSONArray("data");
                }

                private static JSONArray getOptionData(JSONArray rawOptionData) {
                    return rawOptionData.getJSONArray(Integer.parseInt("CE"));
                }

                private static void processData(JSONArray optionData) {
                    for (int i = 0; i < optionData.length(); i++) {
                        JSONObject option = optionData.getJSONObject(i);
                        double calloi, callcoi, cltp, putoi, putcoi, pltp;
                        double stp = option.getDouble("strikePrice");
                        if (option.isNull("CE")) {
                            calloi = callcoi = 0;
                        } else {
                            JSONObject ce = option.getJSONObject("CE");
                            calloi = ce.getDouble("openInterest");
                            callcoi = ce.getDouble("changeinOpenInterest");
                            cltp = ce.getDouble("lastPrice");
                        }
                        if (option.isNull("PE")) {
                            putoi = putcoi = 0;
                        } else {
                            JSONObject pe = option.getJSONObject("PE");
                            putoi = pe.getDouble("openInterest");
                            putcoi = pe.getDouble("changeinOpenInterest");
                            pltp = pe.getDouble("lastPrice");
                        }

                        // Process the option data here
                        // You can store it in a data structure or perform any desired operations
                    }
                }

                public static void main(String[] args) {
                    while (true) {
                        try {
                            JSONObject optionChainData = getOptionChainData();
                            JSONArray rawOptionData = getRawOptionData(optionChainData);
                            JSONArray optionData = getOptionData(rawOptionData);
                            processData(optionData);
                            Thread.sleep(50);
                        } catch (IOException | InterruptedException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
