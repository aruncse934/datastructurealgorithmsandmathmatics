package AlgorithmicTrading.FinancialMarket;


import java.util.ArrayList;
import java.util.*;

public class Test{
    public static void main(String[] args) {
        int[] arr1 = {0, 2, 5, 8};
        int[] arr2 = {1, 6, 7};

        List<Integer> mergedList = mergeArrays(arr1, arr2);

        List<Integer> listA = new ArrayList<>();
        List<Integer> listB = new ArrayList<>();

        // Separate mergedList into A and B
        for (int i = 0; i < mergedList.size(); i++) {
            if (i < arr1.length) {
                listA.add(mergedList.get(i));
            } else {
                listB.add(mergedList.get(i));
            }
        }

        // Convert lists to arrays if necessary
        int[] A = listA.stream().mapToInt(Integer::intValue).toArray();
        int[] B = listB.stream().mapToInt(Integer::intValue).toArray();

        // Print the result
        System.out.println("A=" + Arrays.toString(A));
        System.out.println("B=" + Arrays.toString(B));
    }

    private static List<Integer> mergeArrays(int[] arr1, int[] arr2) {
        List<Integer> mergedList = new ArrayList<>();

        int i = 0, j = 0;

        // Merge the arrays into mergedList
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedList.add(arr1[i]);
                i++;
            } else {
                mergedList.add(arr2[j]);
                j++;
            }
        }

        // Add remaining elements from arr1
        while (i < arr1.length) {
            mergedList.add(arr1[i]);
            i++;
        }

        // Add remaining elements from arr2
        while (j < arr2.length) {
            mergedList.add(arr2[j]);
            j++;
        }

        return mergedList;
    }


}
