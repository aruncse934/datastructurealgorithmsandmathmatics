package HckEndR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TheEndGame {
    public static void solve(int N, long s, int D, int[] power){

    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      //  PrintWriter wr = new PrintWriter(System.out);
        int T = Integer.parseInt(br.readLine().trim());
        for(int t_i = 0; t_i < T; t_i++){
            String[] line = br.readLine().split(" ");
            int N = Integer.parseInt(line[0]);
            int S = Integer.parseInt(line[1]);
            int D = Integer.parseInt(line[2]);
            String[] arr_power = br.readLine().split(" ");
            int[] power = new int[N];
            for(int i_power = 0;i_power<arr_power.length;i_power++){
                power[i_power] = Integer.parseInt(arr_power[i_power]);
            }
            solve(N,S,D,power);
        }
    //    wr.close();
        br.close();
    }
}
/*I/O
* 1
* 3 20 5
* 5 7 3
* O/p
* 5
* */