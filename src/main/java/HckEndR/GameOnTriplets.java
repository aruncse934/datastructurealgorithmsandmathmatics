package HckEndR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class GameOnTriplets {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        String[] line  = br.readLine().split(" ");
        int A = Integer.parseInt(line[0]);
        int B = Integer.parseInt(line[1]);
        int C= Integer.parseInt(line[2]);

        int res = calculateScore(A,C, B);
        System.out.println(res);

        wr.close();
        br.close();
    }

    /*
    * Input:= {4,12,20} out:= 3
    * Explanation (A+B)/2, (B+C)/2, (C+A)/2,
    * {8,16,12},{12,14,10},{13,12,11}
    * */

    public static int calculateScore(int A, int B, int C){

        return -1;
    }
}
