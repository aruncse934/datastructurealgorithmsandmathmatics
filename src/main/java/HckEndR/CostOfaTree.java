package HckEndR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CostOfaTree {
    public static void solve1(int N,int C, int u,int[][] edge){

    }
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //  PrintWriter wr = new PrintWriter(System.out);
        int T = Integer.parseInt(br.readLine().trim());
        for(int t_i = 0; t_i < T; t_i++){
            String[] line = br.readLine().split(" ");
            int N = Integer.parseInt(line[0]);
            int C = Integer.parseInt(line[1]);
            int u = Integer.parseInt(line[2]);

            int[][] edge = new int[N-1][3];
            for(int i_edge = 0;i_edge<N-1;i_edge++){
                String[] arr_edge = br.readLine().split(" ");
                for(int j_edge = 0; j_edge < arr_edge.length; j_edge++){
                    edge[i_edge][j_edge] = Integer.parseInt(arr_edge[j_edge]);
                }

            }
            solve1(N,C,u,edge);
        }
        //    wr.close();
        br.close();
    }
}
/*
* I/p
* 1
* 5 1 3
* 1 2 2
* 2 3 3
* 3 4 4
* 4 5 5
*
* o/p
* 13
* */