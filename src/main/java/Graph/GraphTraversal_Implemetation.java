package Graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class GraphTraversal_Implemetation {

    static class GraphTraversal {

        int vertex[];
        Map<Integer, LinkedList<Integer>> map = new HashMap<Integer, LinkedList<Integer>>();

        GraphTraversal(int vertex) {
            this.vertex = new int[vertex];
            for (int i = 0; i < vertex; i++) {
                this.vertex[i] = 0;
                this.map.put(i, new LinkedList<Integer>());
            }
        }

        public void addedge(int s, int d) {
            this.map.get(s).add(d);

        }
    }


    public static void main(String[] args) {

        GraphTraversal g = new GraphTraversal(5);
        g.addedge(0, 1);
        g.addedge(1, 2);
        g.addedge(1, 4);
        g.addedge(2, 4);
        g.addedge(3, 0);
        g.addedge(3, 4);

    }
}


