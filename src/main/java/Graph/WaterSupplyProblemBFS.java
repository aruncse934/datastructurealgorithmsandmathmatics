package Graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

public class WaterSupplyProblemBFS {

    public static int bfsUtil(int v[], boolean vis[], Vector<Integer> adj[], int src){
        vis[src] = true;
        Queue<Integer> q = new LinkedList<>();
        q.add(src);
        int count =0;
        while (!q.isEmpty()){
            int p = q.peek();
            for(int i = 0; i< adj[p].size();i++){
                if(!vis[adj[p].get(i)] && v[adj[p].get(i)] == 0)
                {
                    count++;
                    vis[adj[p].get(i)] = true;
                    q.add(adj[p].get(i));
                }else if(!vis[adj[p].get(i)] && v[adj[p].get(i)] ==1){
                    count++;
                }
            }
            q.remove();
        }
        return count+1;
    }
    public static int bfs(int N, int v[], Vector<Integer> adj[]){
        boolean []vis = new boolean[N+1];
        int max = 1,res;
        for(int i = 1;i<=N;i++)
            vis[i] = false;
        for(int i = 1;i<=N;i++){
            if(v[i] == 0 && !vis[i]){
                res = bfsUtil(v,vis,adj,i);
                if(res > max){
                    max =res;
                }
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int N = 4;
        Vector<Integer> adj[] = new Vector[N+1];
        for(int i =0; i< adj.length;i++)
            adj[i] = new Vector<>();
        int v[] = new int[N+1];
        adj[1].add(2);
        adj[2].add(1);
        adj[2].add(3);
        adj[3].add(2);
        adj[3].add(4);
        adj[4].add(3);

        v[1] = 0;
        v[2] = 1;
        v[3] = 1;
        v[4] = 0;

        System.out.println(bfs(N,v, adj));
    }
}


