package Graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

//counting pairs
public class Test {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int N = Integer.parseInt(br.readLine().trim());
        String[] arr_color = br.readLine().split(" ");
        int[] color = new int[N];

        for(int i_color = 0; i_color < arr_color.length; i_color++)
        {
            color[i_color] = Integer.parseInt(arr_color[i_color]);
        }
        int[][] edges = new int[N-1][2];
        for(int i_edges = 0; i_edges < N-1; i_edges++)
        {
            String[] arr_edges = br.readLine().split(" ");
            for(int j_edges = 0; j_edges < arr_edges.length; j_edges++)
            {
                edges[i_edges][j_edges] = Integer.parseInt(arr_edges[j_edges]);
            }
        }

        long out_ = CountPair(N, color, edges);
        System.out.println(out_);

        wr.close();
        br.close();
    }
    static long CountPair(int N, int[] color, int[][] edges){
        int count = 0;

       for(int i = 0; i< N-1;i++){
           for(int j = i+1;j<N-1;j++)
               if(color[i] -color[j]== edges[i][j] && color[j] - color[i] == edges[i][j])
                   count++;
       }


        return count;
    }
}
/*int count = 0;

        // Pick all elements one by one
        for (int i = 0; i < n; i++)
        {
            // See if there is a pair
            // of this picked element
            for (int j = i + 1; j < n; j++)
                if (arr[i] - arr[j] == k ||
                    arr[j] - arr[i] == k)
                    count++;
        }
        return count;
    }*/

/*Input:

5
0 1 0 1 0
1 2
2 3
3 4
4 5

output:

3

* */