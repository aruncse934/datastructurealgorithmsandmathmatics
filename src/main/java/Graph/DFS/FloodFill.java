package Graph.DFS;

public class FloodFill {


    // DFS
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor){
        if(image[sr][sc]== newColor) return image;
        fill(image,sr,sc, image[sr][sc],newColor);
        return image;
    }

    private void fill(int[][] image, int sr, int sc, int color, int newColor) {
        if(sr < 0 || sr >= image.length || sc < 0 || sc >= image[0].length || image[sr][sc] != color) return;
        image[sr][sc] = newColor;
        fill(image,sr + 1, sc, color,newColor);
        fill(image, sr - 1, sc, color, newColor);
        fill(image, sr, sc + 1, color, newColor);
        fill(image, sr, sc - 1, color, newColor);
    }

    public static void main(String[] args) {
        int[][] image = new int[][]{{1,1,1},{1,1,0},{1,0,1}};
        int sr = 1, sc = 1, color = 2;
        new FloodFill().floodFill(image,sr, sc, color);
        for(int i = 0; i < image.length; i++){
            for( int j = 0; j < image[0].length; j++){
                System.out.print(image[i][j] + " ");
            }
            System.out.println();
        }
    }
}
