package Graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

//depth-first search method to find cycle in a graph
public class DepthFirstSearch {
    int visited[];
    int vertex;
    Map<Integer, LinkedList<Integer>> map = new HashMap<Integer, LinkedList<Integer>>();

    DepthFirstSearch(int vertex) {
        this.vertex = vertex;
        this.visited = new int[vertex];
        for (int i = 0; i < vertex; i++) {
            this.visited[i] = 0;
            this.map.put(i, new LinkedList<Integer>());
        }
    }

    public void addEdge(int i, int j) {
        this.map.get(i).add(j);
    }

    Boolean DFS(int index, Boolean visited[], int parent) {

        visited[index] = true;
        int size = this.map.get(index).size();
        for (int i = 0; i < size; i++) {
            int val = this.map.get(index).get(i);
            if (visited[val] == false) {
                if (DFS(val, visited, index))
                    return true;
            } else if (i != parent)
                return true;
        }
        return false;
    }

    Boolean checkCycle() {
        Boolean visited[] = new Boolean[vertex];
        for (int i = 0; i < vertex; i++)
            visited[i] = false;

        for (int u = 0; u < vertex; u++)
            if (!visited[u])
                if (DFS(u, visited, -1))
                    return true;
        return false;
    }

    public static void main(String[] args) {

        DepthFirstSearch g1 = new DepthFirstSearch(5);
        g1.addEdge(1, 0);
        g1.addEdge(0, 2);
        g1.addEdge(2, 1);
        g1.addEdge(0, 3);
        g1.addEdge(3, 4);
        if (g1.checkCycle())
            System.out.println("Graph contains cycle");
        else
            System.out.println("Graph doesn't contains cycle");
    }

}
