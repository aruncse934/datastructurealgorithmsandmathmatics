package Graph;

import java.util.Iterator;
import java.util.LinkedList;

public class DfsMain {

    private Integer numVertices;
    private LinkedList<Integer> adjLists[];
    private Boolean visited[];

    DfsMain(Integer vertices){
        numVertices = vertices;
        adjLists = new LinkedList[vertices];
        visited = new Boolean[vertices];

        for(int i = 0; i<vertices;i++)
            adjLists[i]  = new LinkedList<>();
    }

    public void addEdge(Integer source, Integer destination){
        adjLists[source].add(destination);
    }

    public void dfsG(Integer vertex){
        visited[vertex] = true;
        System.out.print(vertex + " ");

        Iterator itr = adjLists[vertex].listIterator();
        while(itr.hasNext()){
            Integer adj_node = (Integer) itr.next();
            if(!visited[adj_node])
                dfsG(adj_node);
        }
    }

    public static void main(String[] args) {
        DfsMain d = new DfsMain(4);
        d.addEdge(0,1);
        d.addEdge(0,2);
        d.addEdge(1,2);
        d.addEdge(2,3);
        System.out.println("Following id Depth First Traversal: ");
       d.dfsG(2);
    }
}
/*class Graph
{

        while (itr.hasNext())
        {
            int adj_node = itr.next();
            if (!visited[adj_node])
                DFS(adj_node);
        }
    }


    public static void main(String args[])
    {
        Graph g = new Graph(4);

         g.addEdge(0, 1);
         g.addEdge(0, 2);
         g.addEdge(1, 2);
         g.addEdge(2, 3);

        System.out.println("Following is Depth First Traversal");

        g.DFS(2);
    }
}*/