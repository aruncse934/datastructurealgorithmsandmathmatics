package Graph;

import java.util.HashSet;
import java.util.Set;

public class LongestConsecutiveSequence {

    public static int lcs(int[] nums){
        Set<Integer> set = new HashSet<>();
        for(int num : nums){
            set.add(num);
        }
        int count = 0;
        for(int num : set){
            if(!set.contains(num-1)){
                int currentNum = num;
                int currentStreak = 1;
                while (set.contains(currentNum+1)){
                    currentNum += 1;
                    currentStreak += 1;
                }
                count = Math.max(count,currentStreak);
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[] nums = {100,4,200,1,3,2};

        System.out.println(lcs(nums));

        /*I
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.*/
    }
}
