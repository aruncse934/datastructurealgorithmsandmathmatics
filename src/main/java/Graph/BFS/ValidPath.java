package Graph.BFS;

import java.util.LinkedList;
import java.util.Queue;

public class ValidPath {

    int[][][] dirs = {{{0, -1}, {0, 1}}, {{-1, 0}, {1, 0}}, {{0, -1}, {1, 0}}, {{0, 1}, {1, 0}}, {{0, -1}, {-1, 0}}, {{0, 1}, {-1, 0}}};
    //the idea is you need to check port direction match, you can go to next cell and check whether you can come back.
    public boolean hasValidPath(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        Queue<int[]> q = new LinkedList<>();
        q.add(new int[]{0, 0});
        visited[0][0] = true;
        while (!q.isEmpty()) {
            int[] cur = q.poll();
            int x = cur[0], y = cur[1];
            int num = grid[x][y] - 1;
            for (int[] dir : dirs[num]) {
                int nx = x + dir[0], ny = y + dir[1];
                if (nx < 0 || nx >= m || ny < 0 || ny >= n || visited[nx][ny]) continue;
                //go to the next cell and come back to orign to see if port directions are same
                for (int[] backDir : dirs[grid[nx][ny] - 1])
                    if (nx + backDir[0] == x && ny + backDir[1] == y) {
                        visited[nx][ny] = true;
                        q.add(new int[]{nx, ny});
                    }
            }
        }
        return visited[m - 1][n - 1];
    }

    public static void main(String[] args) {

    }


}


/*public class Solution {
    public  String solve(int A, int B, int C, int D, ArrayList<Integer> E, ArrayList<Integer> F) {

        Graph g = new Graph((A+1)*(B+1));
        Map<Integer, pos> map = new HashMap<Integer, pos>();
        populateGraph(g, A, B, map);

        return bfs(g, getPosition(A, B, A), map, D, E, F) ? "YES" : "NO";
    }

     class Graph {
        private int V; // No. of vertices
        private LinkedList<Integer> adj[]; // Adjacency Lists

        // Constructor
        Graph(int v) {
            V = v;
            adj = new LinkedList[v];
            for (int i = 0; i < v; i++)
                adj[i] = new LinkedList();
        }

        // Function which adds an edge from v -> w
        void addEdge(int v, int w) {
            adj[v].add(w);
        }
    }

    private  void populateGraph(Graph g, int x, int y, Map<Integer, pos> map) {

        for(int j =0; j<= y; j++) {
            for(int i=0; i<= x; i++) {
                addNeighbours(g, i, j, x, y, map);
            }
        }

    }

     class pos {
        public int x; public int y;
        pos(int x, int y){
            this.x = x;
            this.y = y;
        }
    }

    private  void addNeighbours(Graph g, int i, int j, int x, int y, Map<Integer, pos> map) {
        map.put(getPosition(i, j, x), new pos(i, j));
        if(isNumberInGraph(i+1, j, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i+1, j, x));
        if(isNumberInGraph(i+1, j+1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i+1, j+1, x));
        if(isNumberInGraph(i, j+1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i, j+1, x));
        if(isNumberInGraph(i-1, j+1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i-1, j+1, x));
        if(isNumberInGraph(i-1, j, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i-1, j, x));
        if(isNumberInGraph(i-1, j-1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i-1, j-1, x));
        if(isNumberInGraph(i, j-1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i, j-1, x));
        if(isNumberInGraph(i+1, j-1, x, y)) g.addEdge(getPosition(i, j, x), getPosition(i+1, j-1, x));
    }

    private  boolean isNumberInGraph(int i, int j, int x, int y) {

        if(i < 0 || i > x) return false;
        if(j < 0 || j > y) return false;

        return true;
    }

    private  int getPosition(int i, int j, int x) {
        return j*(x+1) + i;
    }

    private  boolean bfs(Graph g, int z, Map<Integer, pos> map, int D, ArrayList<Integer> E, ArrayList<Integer> F) {
        Queue<Integer> q = new LinkedList<Integer>();
        q.add(0);
        boolean isVisited[] = new boolean[z+1];
        isVisited[0] = true;
        while(!q.isEmpty()) {
            int pop = q.remove();
            if(pop == z) {
                return true;
            }
            LinkedList l = g.adj[pop];
            Iterator<Integer> it = l.listIterator();
            while(it.hasNext()) {
                int n = it.next();
                if(isVisited[n]) {
                    continue;
                }
                isVisited[n] = true;
                if(doesPointLieInAnyCircles(n, D, E, F, map)) continue;
                q.add(n);
            }
        }

        return false;
    }

    private  boolean doesPointLieInAnyCircles(int k, int D, ArrayList<Integer> E, ArrayList<Integer> F, Map<Integer, pos> map) {

        for(int i=0; i< E.size(); i++) {
            int cx = E.get(i);
            int cy = F.get(i);
            pos p = map.get(k);

            if(doesPointLieInCircle(p.x, p.y, D, cx, cy)) return true;
        }

        return false;
    }

    private  boolean doesPointLieInCircle(int i, int j, int r, int cx, int cy) {

        if(distance(i, j, cx, cy) <= r) return true;

        return false;
    }

     private  double distance(int x1, int y1, int x2, int y2)
    {
        // Calculating distance
        return Math.sqrt(Math.pow(x2 - x1, 2) +
                Math.pow(y2 - y1, 2));
    }

}*/