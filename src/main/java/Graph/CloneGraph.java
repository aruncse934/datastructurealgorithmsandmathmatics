
package Graph;


import java.util.*;

class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
}

public class CloneGraph {
    /*public Node cloneGraph(Node node) {
        if (node == null)
            return null;
        //Creating a Queue for BFS
        Queue<Node> store = new LinkedList<Node>();
        //Adding the root node
        store.add(node);
        //Creating a hashmap of nodes and copies
        HashMap<Node, Node> hash = new HashMap<Node, Node>();
        hash.put(node, new Node(node.val));
        //Firing up the BFS!
        while (!store.isEmpty()) {
            Node cur = store.poll();
            for (Node neigh : cur.neighbors) {
                if (!hash.containsKey(neigh)) {
                    hash.put(neigh, new Node(neigh.val));
                    store.add(neigh);
                }
                //Adding the neighbours to the duplicate nodes
                hash.get(cur).neighbors.add(hash.get(neigh));
            }
        }
        return (hash.get(node));
    }
*/

    public Node cloneGraph(Node node) {
      if(node ==null)
          return null;
        Queue<Node> store = new LinkedList<Node>();
        store.add(node);
        Map<Node, Node> hash = new HashMap<>();
        hash.put(node, new Node(node.val));
        while(!store.isEmpty()){
            Node cur = store.poll();
            for(Node neighbor : cur.neighbors){
                if(!hash.containsKey(neighbor)){
                    hash.put(neighbor, new Node(neighbor.val));
                    store.add(neighbor);
                }
                hash.get(cur).neighbors.add(hash.get(neighbor));
            }
        }
        return (hash.get(node));
    }

    public static void main(String[] args) {

    }
}
/*Input: adjList = [[2,4],[1,3],[2,4],[1,3]]
Output: [[2,4],[1,3],[2,4],[1,3]]*/