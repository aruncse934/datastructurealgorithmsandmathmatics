package Graph.Topological_Sort_Problems;

import java.util.ArrayList;

public class TopologicalSortingImplementation {
    public static void main(String[] args) {
        ArrayList<Integer>[] graph = (ArrayList<Integer>[]) new ArrayList[6];
        for (int i = 0; i < graph.length; i++)
            graph[i] = new ArrayList<>();
        graph[5].add(0);
        graph[5].add(2);
        graph[4].add(0);
        graph[4].add(1);
        graph[2].add(3);
        graph[3].add(1);


        int[] ret = topoSort(graph, 6);

        for (int i = 0; i < ret.length; i++)
            System.out.print(ret[i] + " ");
    }

    public static int[] topoSort(ArrayList<Integer> graph[], int N) {
        int[] ret = new int[N];
        boolean[] visited = new boolean[N];
        int retIdx = N - 1;

        for (ArrayList<Integer> list : graph) {
            for (int x : list) {
                if (!visited[x]) {
                    retIdx = dfs(x, graph, ret, retIdx, visited);
                }
            }
        }

        return ret;
    }

    private static int dfs(int curr, ArrayList<Integer> graph[], int[] ret, int retIdx, boolean[] visited) {

        if (!visited[curr]) {

            visited[curr] = true;

            for (int adj : graph[curr]) {
                retIdx = dfs(adj, graph, ret, retIdx, visited);

            }

            ret[retIdx--] = curr;


        }

        return retIdx;
    }
}
