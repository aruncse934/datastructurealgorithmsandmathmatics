package IOStream.ReactiveX_Programming;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class ObservablesTester {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("hello ");
        Single<String> stringSingle = Single.just("Hello World"); //Create the observable

        Disposable disposable = stringSingle
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableSingleObserver<String>() {

                    @Override
                    public void onSuccess(String value) {
                        System.out.println(value);

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }
                });
        Thread.sleep(300);
        disposable.dispose();

    }

}
