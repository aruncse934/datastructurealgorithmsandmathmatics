package IOStream.ReactiveX_Programming.Operators;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;

public class FilteringObservables {
    public static void main(String[] args) {
        //distinct
        System.out.println("=======distinct=========");
        Observable.just(2,3,4,5,6,7)
                .distinct()
                .subscribe(System.out::println);

        //elementAt
        System.out.println("========elementAt========");
        Observable<Long> source = Observable.<Long , Long> generate(() ->1L,(state,emitter) ->{
            emitter.onNext(state);
            return state+1L;
        }).scan((product,x) -> product * x);
        @NonNull Maybe<Long> element = source.elementAt(5);
        element.subscribe(System.out::println);

        //filter
        System.out.println("======filter========");
        Observable.just(1,2,3,4,5,6)
                .filter( x -> x%2 ==0)
                .subscribe(System.out::println);


    }
}
/*Observable.just(1, 2, 3, 4, 5, 6)
        .filter(x -> x % 2 == 0)
        .subscribe(System.out::println);;*/