package IOStream.ReactiveX_Programming.Operators;

import io.reactivex.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.functions.Action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CreateObjectsExample {
    public static void main(String[] args) throws InterruptedException {


     //just
        System.out.println("===//just===");
        String str = "Hello, Welcome to Java Reactive Programming! ";
        Observable<String> o = Observable.just(str);
        o.subscribe(item -> System.out.println(item));
     //Or just
        System.out.println("===//Or just===");
        Observable<Object> o1 = Observable.just("1","A","3.2","def");
        o1.subscribe(item -> System.out.println(item),error -> error.printStackTrace(),() -> System.out.println());

    //fromIterable
        System.out.println("===  //fromIterable===");
    List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
    Observable<Integer> o2 = Observable.fromIterable(list);
    o2.subscribe(item -> System.out.println(item), error -> error.printStackTrace(),() -> System.out.println("Done"));


    //fromArray

        System.out.println("===  //fromArray===");
        Integer[] arr = new Integer[10];
        for(int i = 0; i<arr.length;i++){
            arr[i] = i;
        }
        Observable<Integer> o4 = Observable.fromArray(arr);
        o4.subscribe(item -> System.out.println(item), error -> error.printStackTrace(),() -> System.out.println("Done"));

        //fromCallable
        System.out.println("===//fromCallable====");
        Callable<String> c = ()->{
            System.out.println("Hello! Callable");
            return "Hello World!";
        };
        Observable<String> observable = Observable.fromCallable(c);

        observable.subscribe(item -> System.out.println(item),error -> error.printStackTrace(), () -> System.out.println("Done"));

        //fromAction
        System.out.println("===//fromAction====");
        Action action = () -> System.out.println("Hello fromAction Method!");
        Completable completable = Completable.fromAction(action);
        completable.subscribe(() -> System.out.println("Done"), error -> error.printStackTrace());


        //fromRunnable
        System.out.println("========fromRunnable==========");
        Runnable runnable = () -> System.out.println("Hello From Runnable method !");
        Completable complete = Completable.fromRunnable(runnable);
        complete.subscribe(() -> System.out.println("Done"), error -> error.printStackTrace());


        //fromFuture
        System.out.println("=====fromFuture=====");
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        Future<String> future =  executorService.schedule(() -> "Hello fromFuture", 1, TimeUnit.SECONDS);
        Observable<String> o5 = Observable.fromFuture(future);
        o5.subscribe(item -> System.out.println(item),error -> error.printStackTrace(), () -> System.out.println("done"));
        executorService.shutdown();


        //fromPublisher
        /*Flux<Integer> reactorFlux = Flux.fromCompletionStage(CompletableFuture.<Integer>completedFuture(1));

        Observable<Integer> observables = Observable.fromPublisher(reactorFlux);

        observables.subscribe(
                item -> System.out.println(item),
                error -> error.printStackTrace(),
                () -> System.out.println("Done"));*/

        //generate
        /*System.out.println("=====generate======");
        int startValue = 1;
        int incrementValue =1;
        Flowable<Integer> flowable = Flowable.generate(() -> startValue,(s,emitter) -> {
            int nextValue = s+incrementValue;
            emitter.onNext(nextValue);
            return nextValue;
        });
        flowable.subscribe(value -> System.out.println(value));*/

        //create
        System.out.println("=======Create========");

       /* ScheduledExecutorService executorService1 = Executors.newSingleThreadScheduledExecutor();
        ObservableOnSubscribe<String> handler = emitter -> {
            Future<Object> future1 = executorService1.schedule(() -> {
                emitter.onNext("Hello");
                emitter.onNext("World");
                emitter.onComplete();
                return null;
            }, 1,TimeUnit.SECONDS);
            emitter.setCancellable(() -> future1.cancel(false));
        };
        Observable<String> observable1 = Observable.create(handler);

        observable1.subscribe(item -> System.out.println(item), error -> error.printStackTrace(),
                () -> System.out.println("Done"));
        Thread.sleep(2000);
        executorService1.shutdown();*/

        System.out.println("=======defer========");
        Observable<Long> observable2 = Observable.defer(()->{
            long time = System.currentTimeMillis();
            return Observable.just(time);
        });
        observable2.subscribe(time -> System.out.println(time));
        Thread.sleep(1000);
        observable2.subscribe(time -> System.out.println(time));


        System.out.println("=========range========");
        String ss = "Hello World!";
        Observable<Integer> indexes =Observable.range(0,ss.length());

        Observable<Character> chars = indexes.map(indx -> ss.charAt(indx));
        chars.subscribe(character -> System.out.print(character),error -> error.printStackTrace(),()-> System.out.println());

        System.out.println("=========interval==========");
        /*Observable<Long> clock = Observable.interval(1,TimeUnit.SECONDS);
        clock.subscribe(time -> {
            if(time % 2 == 0){
                System.out.println("Tick");
            }else {
                System.out.println("Tock");
            }
        });*/


        System.out.println("============timer=============");
        Observable<Long> eggTimer = Observable.timer(1,TimeUnit.MINUTES);
        eggTimer.blockingSubscribe(v -> System.out.println("Veg is ready!"));

        System.out.println("==============empty=================");
        Observable<String> empty = Observable.empty();
        empty.subscribe(v -> System.out.println("This should never be printed!"),
                error -> System.out.println("Or this!"),
                () -> System.out.println("done will be printed."));


        System.out.println("=============never===========");
        Observable<String> never = Observable.never();
        never.subscribe(v -> System.out.println("this should never be printed!"),
                error -> System.out.println("Or this!"),
                ()-> System.out.println("This neither!"));

        System.out.println("===============error=================");
        Observable<String> error = Observable.error(new IOException());
        error.subscribe(v -> System.out.println("This should never be printed!"),
                e -> e.printStackTrace(),
                () -> System.out.println("This neither!"));

    }

}

/*

Observable<String> error = Observable.error(new IOException());

error.subscribe(
    v -> System.out.println("This should never be printed!"),
    e -> e.printStackTrace(),
    () -> System.out.println("This neither!"));

  */