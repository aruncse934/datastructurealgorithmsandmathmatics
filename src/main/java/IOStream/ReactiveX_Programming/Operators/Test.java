package IOStream.ReactiveX_Programming.Operators;

import io.reactivex.Observable;

public class Test {
    public static void main(String[] args) {
        String[] str = {"a", "b", "c", "d", "e", "f", "g"};
        final StringBuilder result = new StringBuilder();
        Observable<String> ob = Observable.fromArray(str);

        ob.map(String::toUpperCase)
                .subscribe(strs ->result.append(strs));
        System.out.println(result);

        System.out.println("===========================");

        String[] str1 = {"a", "b", "c", "d", "e", "f", "g"};
        final StringBuilder result1 = new StringBuilder();
        Observable<String> ob1 = Observable.fromArray(str1);
        ob1.take(2)
                .subscribe( le -> result1.append(le));
        System.out.println(result1);


        System.out.println("===========================");
        Integer[] num = { 1, 2, 3, 4, 5, 6};
        String[] strs = {"a", "b", "c", "d", "e", "f", "g"};
        final StringBuilder res = new StringBuilder();
        Observable<String> obs = Observable.fromArray(strs);
        Observable<Integer> obs1=Observable.fromArray(num);
        Observable.combineLatest(obs,obs1, (a,b) -> a + b)
                .subscribe(s ->res.append(s));
        System.out.println(res);

    }
}
/*
* */