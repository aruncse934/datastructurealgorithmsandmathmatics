package IOStream.ReactiveX_Programming;

import io.reactivex.Flowable;

public class HelloReactiveX {
    public static void main(String[] args) {
        Flowable.just("Welcome to Reactive Programming!").subscribe(System.out::println);
    }
}