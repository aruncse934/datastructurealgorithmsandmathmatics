package IOStream.SerializationNdDeserialization;

import java.io.*;

//Serialization and Deserialization of a objects.
public class Test implements Serializable {
    public int a;
    public String b;

    public Test(int a, String b){
        this.a = a;
        this.b =b;
    }

}

class Main{
    public static  void main(String[] args) throws FileNotFoundException {
        Test test = new Test(1,"javademo");
        String filename = "file.ser";

        //Serialization
        try{
            //saving of object in a file.
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out  = new ObjectOutputStream(file);

            //Method for serialization of object.
            out.writeObject(test);

            out.close();
            file.close();

            System.out.println("Object has beean serailized");

        }catch (IOException e){
           // e.printStackTrace();
            System.out.println("IOException is caught" );
        }
        Test test1 =null;
        //Deserialization

        try{
            //Reading the object from a file
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            //method for deserialization of object
            test1 = (Test) in.readObject();

            in.close();
            file.close();

            System.out.println("Object has been deserialized ");
            System.out.println("a = "+test1.a);
            System.out.println("b = "+test1.b);

        }catch (ClassNotFoundException | IOException e){
         //   e.printStackTrace();
            System.out.println("ClassNotFoundException is caught");
        }
    }
}
