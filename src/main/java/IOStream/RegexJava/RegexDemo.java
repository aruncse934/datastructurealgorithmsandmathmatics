package IOStream.RegexJava;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {
    private static String REGEX = "a*b";
    private static String INPUT = "aabfooaabfooabfoob";
    private static String REPLCE = "-";

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(INPUT);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()){
            matcher.appendReplacement(sb,REPLCE);
        }
        matcher.appendTail(sb);
        System.out.println(sb.toString());
    }
}

