package IOStream.RegexJava;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplceDemo {
    private static String REGEX = "dog";
    private static String INPUT = "The dog says meow. All dogs say meow.";
    private static String REPLCE = "cat";

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(INPUT);
        INPUT = matcher.replaceAll(REPLCE);
        System.out.println(INPUT);
    }
}
