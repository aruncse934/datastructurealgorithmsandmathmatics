package IOStream.RegexJava;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchingDigitsNon_DigitCharacters {
    public static void main(String[] args) {

        Regexs_Test tester = new Regexs_Test();
        String s = "([\\d]{2}\\D){2}[\\d]{4}";
        tester.checker(s); // Use \\ instead of using \

    }
}

class Regexs_Test {

    public void checker(String Regex_Pattern){

        Scanner Input = new Scanner(System.in);
        String Test_String = Input.nextLine();
        Pattern p = Pattern.compile(Regex_Pattern);
        Matcher m = p.matcher(Test_String);
        System.out.println(m.find());
    }

}

