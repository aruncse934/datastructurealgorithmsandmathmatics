package IOStream.RegexJava;

import java.util.regex.Pattern;

public class SplitDemo {

    private static final String ReGX = ":";
    private static final String InPUT = "ONE:TWO:THREE:FOUR:FIVE";

    public static void main(String[] args) {
        Pattern p = Pattern.compile(ReGX);
        String[] input = p.split(InPUT);
        for (String s : input) {
            System.out.println(s);
        }
    }
}
