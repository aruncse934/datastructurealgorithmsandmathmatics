package IOStream.RegexJava;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchingWord_NonWordCharacter {
    public static void main(String[] args) {

        Regex_Tests tester = new Regex_Tests();
        String s="\\w{3}\\\\W\\\\w{10}\\\\W\\\\w{3}";
        tester.checker(s); // Use \\ instead of using \

    }
}

class Regex_Tests {

    public void checker(String Regex_Pattern){

        Scanner Input = new Scanner(System.in);
        String Test_String = Input.nextLine();
        Pattern p = Pattern.compile(Regex_Pattern);
        Matcher m = p.matcher(Test_String);
        System.out.println(m.find());
    }
}
