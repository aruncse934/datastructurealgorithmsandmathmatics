package IOStream.Java8;

import java.util.Arrays;
import java.util.*;

public class Test {

    public static long mandragora(List<Integer> H) {
        long maxHeight = 0;
        long maxArea = 0;

        // Iterate over the heights
        for (int h : H) {
            // Update the max height if the current height is greater
            if (h > maxHeight) {
                maxHeight = h;
            }

            // Calculate the area of the current triangle
            long area = (h * maxHeight) / 2;

            // Update the max area if the current area is greater
            if (area > maxArea) {
                maxArea = area;
            }
        }

        // Return the max area
        return maxArea;
    }
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(3, 2, 2);
        System.out.println(mandragora(list));
        
    }
}
