package IOStream.Java8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConvertListToMap {

    public static void main(String[] args) {
        Map<String, String> m = new HashMap<String, String>();
        m.put("New_Hello", "World");
        m.put("New_Apple", "3.14");
        m.put("New_Another", "Element");

        List<String> list = new ArrayList<String>(m.keySet());

        List<String> list1 = new ArrayList<String>(m.values());

        System.out.println(list+" "+list1);

        List<String> values = m.values().stream().collect(Collectors.toList());
        System.out.println(values);


       /* Map<String, String> concept = m.stream()
                .collect(Collectors.toMap(concepts -> conceptq, concepts -> getRelatedData(conceptq)));*/

        String conceptId = "New_KG1317";

        System.out.println(getRelatedData(conceptId));
    }

    public static String getRelatedData(String conceptId) {
        String concepts = conceptId;
        if (concepts.startsWith("new_"))
            return concepts.substring(4);
        return conceptId;
    }
}
