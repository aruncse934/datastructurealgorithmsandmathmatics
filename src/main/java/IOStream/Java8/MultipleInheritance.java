package IOStream.Java8;

interface Poet{
    default void write(){
        System.out.println("Poet's default method");
    }
}
interface Writer{
    default void write(){
        System.out.println("Writer's default method");
    }
}

public class MultipleInheritance implements Poet, Writer {

    @Override
    public void write() {
        System.out.println("Writing stories now days");
    }

    public static void main(String[] args) {
        MultipleInheritance m = new MultipleInheritance();
        m.write();
    }
}
