package IOStream.Java8.Collections.Map;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HashMapDemo {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("a",new Integer(100));
        map.put("b",200);
        map.put("c",new Integer(300));
        map.put("d",new Integer(400));

        System.out.println(map);

        //Traversing through the map
        for(Map.Entry<String, Integer> me : map.entrySet()){
            System.out.print(me.getKey()+ ":");
            System.out.println(me.getValue());
        }
        // Using stream() in Java 8
        map.entrySet().stream().forEach( e -> {
            System.out.println(e.getKey()+":"+ e.getValue());
        });
       //
        Map<String, Integer> result = map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e-> e.getValue()));
        System.out.println(result);


        //
        Stream.of(map.entrySet().toString())
                .forEach(System.out::println);

        Stream.of(map.toString())
                .forEach(System.out::println);
    }
}
/*Map<String, List<String>> result = anotherHashMap
    .entrySet().stream()                    // Stream over entry set
    .collect(Collectors.toMap(              // Collect final result map
        Map.Entry::getKey,                  // Key mapping is the same
        e -> e.getValue().stream()          // Stream over list
            .sorted(Comparator.comparingLong(MyObject::getPriority)) // Sort by priority
            .map(MyObject::getName)         // Apply mapping to MyObject
            .collect(Collectors.toList()))  // Collect mapping into list
        );*/