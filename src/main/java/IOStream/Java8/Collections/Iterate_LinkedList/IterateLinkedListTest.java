package IOStream.Java8.Collections.Iterate_LinkedList;

import java.util.Iterator;
import java.util.LinkedList;

//Iterating The LinkedList
public class IterateLinkedListTest {
    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        //Inserting some Integer values to our Linked List using for loop
        linkedList.add(40);
        linkedList.add(44);
        linkedList.add(80);
        linkedList.add(9);
        iterateUsingForLoop(linkedList);

        LinkedList<Character> vowels = new LinkedList<>();
        vowels.add('a');
        vowels.add('e');
        vowels.add('i');
        vowels.add('o');
        vowels.add('u');
        iterateUsingWhileLoop(vowels);

        LinkedList<String> stringLinkedList = new LinkedList<>();
        stringLinkedList.add("Geeks");
        stringLinkedList.add("for");
        stringLinkedList.add("Geeks");
        iterateUsingEnhancedForLoop(stringLinkedList);

        LinkedList<Integer> integerLinkedList = new LinkedList<>();
        integerLinkedList.add(5);
        integerLinkedList.add(100);
        integerLinkedList.add(41);
        integerLinkedList.add(40);
        integerLinkedList.add(7);

        iterateUsingIterator((LinkedList<Integer>) integerLinkedList.stream());


        LinkedList<Integer> integers =new LinkedList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        iterateUsingForEach(integers);
    }

    //Using For Loop 1
    public static void iterateUsingForLoop(LinkedList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }

    //Using While Loop 2
    public static void iterateUsingWhileLoop(LinkedList<Character> list){
        int i =0;
        while (i<list.size()){
            System.out.print("\n"+list.get(i)+" ");
            i++;
        }
    }

    //Using Enhanced For Loop 3
    public static void iterateUsingEnhancedForLoop(LinkedList<String> stringLinkedList) {
        for (String str : stringLinkedList) {
            System.out.print("\n"+str);
        }
    }

    //Using Iterator 4
    public static void iterateUsingIterator(LinkedList<Integer> list){
        Iterator it =list.iterator();
        while (it.hasNext()){
            System.out.print("\n"+it.next()+" ");
        }
    }

    //Using ForEach Loop
    public static void iterateUsingForEach(LinkedList<Integer> list){
        list.forEach((element) ->{
            System.out.print(" \n" +element+ " ");
        });
    }
}
