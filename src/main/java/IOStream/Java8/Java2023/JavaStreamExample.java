package IOStream.Java8.Java2023;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@AllArgsConstructor
@Getter
@Setter
@ToString
class Employee {
    private int id;
    private String name;
    private int age;
    private long salary;
}

@AllArgsConstructor
@Getter
@Setter
@ToString
class Student {
    private String name;
    private int id;
    private String subject;
    private double percentage;
}
@AllArgsConstructor
@Getter
@Setter
@ToString
class EmpDepartment {
    private int id;
    private String name;
    private int age;
    private String gender;
    private String department;
    private int yearOfJoining;
    private double salary;

    }

    public class JavaStreamExample {
        public static void main(String[] args) {


            /*List<Employee> employees = new ArrayList< Employee >();
            employees.add(new Employee(10, "Ramesh", 30, 400000));
            employees.add(new Employee(20, "John", 29, 350000));
            employees.add(new Employee(30, "Tom", 30, 450000));
            employees.add(new Employee(40, "Pramod", 29, 500000));*/

            List<Student> students = Arrays.asList(
              new Student("Arun",101,"Java", 90.2),
                    new Student("ARjun",106,"Java", 50.2),
                    new Student("Suji",105,"Spring", 49.2),
                    new Student("Ravi",102,"Rest", 60),
                    new Student("Satyam",104,"DB", 95.6),
                    new Student("shivam",103,"DSA", 98.2)

            );

            //Given a list of students, write a Java 8 code to partition the students who got above 60% from those who didn’t?
            Map<Boolean, List<Student>> map = students.stream().collect(Collectors.partitioningBy(student -> student.getPercentage() > 60.0));
            System.out.println(map);

            // Given a list of students, write a Java 8 code to get the names of top 3 performing students?
            List<Student> top3Name = students.stream().sorted(Comparator.comparingDouble(Student::getPercentage).reversed()).limit(3).collect(Collectors.toList());
            System.out.println(top3Name);

            // Given a list of students, how do you get the name and percentage of each student?
            Map<String,Double> namePercentage = students.stream().collect(Collectors.toMap(Student::getName,Student::getPercentage));
            System.out.println(namePercentage);

             //Given a list of students, how do you get the subjects offered in the college?
            Set<String> subjects = students.stream().map(Student::getSubject).collect(Collectors.toSet());
            System.out.println(subjects);

           // Given a list of students, write a Java 8 code to get highest, lowest and average percentage of students?
            DoubleSummaryStatistics summaryStatistics = students.stream().collect(Collectors.summarizingDouble(Student::getPercentage));
            System.out.println("Highet : "+summaryStatistics.getMax()+ "\n"+"Lowest : "+summaryStatistics.getMin()+"\n"+"Average : "+summaryStatistics.getAverage());

            // How do you get total number of students from the given list of students?
            Long studentCount = students.stream().collect(Collectors.counting());
            System.out.println(studentCount);

            //How do you get the students grouped by subject from the given list of students?
            Map<String, List<Student>> groupSubjects = students.stream().collect(Collectors.groupingBy(Student::getSubject));
            System.out.println(groupSubjects);

            //


            List<EmpDepartment>  empDepartments = Arrays.asList(
                    new EmpDepartment(2001,"Siya",45,"Female","HR",2019,3000),
                    new EmpDepartment(2002,"Riya",43,"male","Manager",2021,6000),
                    new EmpDepartment(2003,"Tiya",41,"Female","Accounts",2019,4000),
                    new EmpDepartment(2004,"Kiya",42,"male","Admin",2023,5000),
                    new EmpDepartment(2005,"Miya",44,"Female","QA",2023,2000),
                    new EmpDepartment(2006,"Diya",39,"Female","HR",2022,1000)
            );
            //Given a list of employees, write a Java 8 code to count the number of employees in each department?
            Map<String, Long> countEachDepartMent = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.counting()));
            System.out.println(countEachDepartMent);

            //Given a list of employees, find out the average salary of male and female employees?
            Map<String, Double> avgMaleNdFemaleSalary = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getGender, Collectors.averagingDouble(EmpDepartment::getSalary)));
            System.out.println(avgMaleNdFemaleSalary);

            //Write a Java 8 code to get the details of highest paid employee in the organization from the given list of employees?
            Optional<EmpDepartment> highestEmpSala1 = empDepartments.stream().max(Comparator.comparingDouble(EmpDepartment::getSalary));
            System.out.println(highestEmpSala1);
            Optional<EmpDepartment> highestEmpSala2 = empDepartments.stream().collect(Collectors.maxBy(Comparator.comparingDouble(EmpDepartment::getSalary)));
            System.out.println(highestEmpSala2);

            //Write the Java 8 code to get the average age of each department in an organization?
            Map<String, Double> avgAgeDept = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.averagingInt(EmpDepartment::getAge)));
            System.out.println(avgAgeDept);

            //Given a list of employees, how do you find out who is the senior most employee in the organization?
            Optional<EmpDepartment> mostSeniorEmp = empDepartments.stream().sorted(Comparator.comparingInt(EmpDepartment::getYearOfJoining)).findFirst();
            System.out.println(mostSeniorEmp.get());

            //Given a list of employees, get the details of the most youngest employee in the organization?
            Optional<EmpDepartment> youngEmp1 = empDepartments.stream().collect(Collectors.minBy(Comparator.comparingInt(EmpDepartment::getAge)));
            Optional<EmpDepartment> youngEmp2 = empDepartments.stream().min(Comparator.comparingInt(EmpDepartment::getAge));
            System.out.println(youngEmp1+"\n"+youngEmp2);

            //How do you get the number of employees in each department if you have given a list of employees?
            Map<String, Long> eachDepartCou = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.counting()));
            System.out.println(eachDepartCou);

            //Given a list of employees, find out the number of male and female employees in the organization?
            Map<String, Long> countMndF = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getGender, Collectors.counting()));
            System.out.println(countMndF);
            //Print the name of all departments in the organization?
            empDepartments.stream().map(EmpDepartment::getDepartment).distinct().forEach(System.out::println);


            System.out.println(IntStream.range(1,6).sum());

             //StackOverFlowExxample
            //Java 8 Distinct by property  persons.stream().filter(distinctByKey(Person::getName))
            Collection<EmpDepartment> departments = empDepartments.stream().collect(Collectors.toMap(EmpDepartment::getName, p -> p, (p, q) -> p)).values();
            System.out.println(departments);

            List<Employee> list = Arrays.asList(
                    new Employee(10, "Ramesh", 30, 400000),
                    new Employee(20, "John", 29, 350000),
                    new Employee(30, "Tom", 30, 450000),
                    new Employee(40, "Pramod", 29, 500000)

            );

            //second highest salary
            Optional<Employee> secondHighestSalary = list.stream().sorted(Comparator.comparingLong(Employee::getSalary).reversed()).skip(1).findFirst();
            System.out.println("second highest salary: " + secondHighestSalary.get());

            // sort employee by salary in ascending order
            //1st Approach
            List<Employee> employeeList = list.stream().sorted(((a, b) -> (int) (a.getSalary() - b.getSalary()))).toList();
            System.out.println("sort emp salry asc:" + employeeList);

            //2nd Approach
            List<Employee> list1 = list.stream().sorted(Comparator.comparingLong(Employee::getSalary)).collect(Collectors.toList());
            System.out.println(list1);

            // sort employee by salary in descending order
            //1st approach
            List<Employee> list2 = list.stream().sorted((a, b) -> (int) (b.getSalary() - a.getSalary())).collect(Collectors.toList());
            System.out.println("sort emp salry desc:" + list2);

            //2nd approach
            List<Employee> list3 = list.stream().sorted(Comparator.comparingLong(Employee::getSalary).reversed()).collect(Collectors.toList());
            System.out.println(list3);


            //seconnd highest number
            Integer[] arr = {23, 45, 85, 10, 20, 5, 1};
            Integer integer = Arrays.stream(arr).sorted(Comparator.reverseOrder()).skip(1).findFirst().get();
            System.out.println("seconnd highest number:: " + integer);


            //Write a Java stream api to find the duplicate words and their number of occurrences in a string?
            String s = "This is a test a sentence. This is a another test This sentence.";
            Map<String, Long> wordOccurrences = Arrays.stream(s.split("\\s+"))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            wordOccurrences.entrySet().stream()
                    .filter(entry -> entry.getValue() > 1)
                    .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));


          //  Write a Java program to count the number of words in a string?
            String sentence = "This is a sample sentence.";
            long wordCount = Arrays.stream(sentence.split("\\s+")).count();
            System.out.println("Number of words: " + wordCount);
            //
            List<String> words = Arrays.asList(sentence.split(" "));
            long wc = words.stream().count();
            System.out.println("The number of words in the string is: " + wc);
            //
            String[] word = sentence.trim().split(" ");
            System.out.println("Number of words in the string = "+word.length);

            // Write a Java program to count the total number of occurrences of a given character in a string without using any loop?
            String sen= "This is a sample sentence.";
            char targetChar = 's';
            long charCount = sen.chars()
                    .filter(ch -> ch == targetChar)
                    .count();
            System.out.println("Total occurrences of '" + targetChar + "': " + charCount);
             //
            char ch = 's';
            long count = Stream.of(sen.split(""))
                    .filter(e -> e.equals(Character.toString(ch)))
                    .count();
            System.out.println("The number of occurrences of the character '" + ch + "' : " + count);
             //
            char c = 's';
            int coun = s.length() - s.replace("a", "").length();
            System.out.println("Number of occurances of 's' in : "+coun);

            int[] numbers = {5, 2, 8, 1, 6, 3, 9, 4, 7};
            Arrays.stream(numbers).filter(i->i%2== 0).boxed().sorted(Comparator.reverseOrder()).forEach(System.out::println);


            //  Using Java 8 how can I get count of consecutive characters in a string. like
            //String str = "aabbbccddbb"; // output: 2a3b2c2d2b.  Please note that count of b is not 5
            String input = "aabbbccddbb";
            String result = Arrays.stream(input.split("(?<=(.))(?!\\1)"))
                    .map(s1 -> s1.length() + Character.toString(s1.charAt(0)))
                    .collect(Collectors.joining());
            System.out.println(result);

            List<String> fruits = Arrays.asList("apple", "orange", "banana", "mango", "apricot", "kiwi");
            
          //  To count all the fruits whose names begin with "a" or "o"
            Map<String, Long> countMap = fruits.stream()
                    .filter(fruit -> fruit.startsWith("a") | fruit.startsWith("o"))
                    .collect(Collectors.groupingBy(fruit -> fruit, Collectors.counting()));

            System.out.println(countMap);


            //second highest salary
            Map<String, Long> map1 = new HashMap<>();
            map1.put("John", 5000L);
            map1.put("Jane", 6000L);
            map1.put("Alice", 6000L);
            map1.put("Bob", 7000L);
            map1.put("Eve", 5500L);

            List<Map.Entry<String, Long>> secondHighest = map1.entrySet().stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toList());

            System.out.println(secondHighest.get(1));

            int[] a = {1, 2, 1, 3, 4};

            List<String> pairs = new ArrayList<>();
            for (int i = 0; i < a.length - 1; i++) {
                String pair = "(" + a[i] + "," + a[i + 1] + ")";
                pairs.add(pair);
            }

            System.out.println(String.join(" ", pairs));

            //Count the Occurrences of Each Character
            String str = "ArunKumarGupta";
            Map<String, Long> res = Arrays.stream(str.split(""))
                    .map(String::toLowerCase)
                    .collect(Collectors.groupingBy(sa -> sa, Collectors.counting()));
            System.out.println(res);

            Pattern.compile(".").matcher(str).results().map(m -> m.group().toLowerCase()).
                    collect(Collectors.groupingBy(sa1 -> sa1, LinkedHashMap::new,
                            Collectors.counting())).forEach((k, v) -> System.out.println(k + " = " + v ));





        }
    }