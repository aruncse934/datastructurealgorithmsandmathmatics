package IOStream.Java8.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlattenStreamListsforEach {

    public static void main(String[] args) {

        List<Integer> a = Arrays.asList(1, 2);
        List<Integer> b = Arrays.asList(3, 4, 5, 6);
        List<Integer> c = Arrays.asList(7, 8, 9);

        List<List<Integer>> arr = new ArrayList<List<Integer>>();
        arr.add(a);
        arr.add(b);
        arr.add(c);

        List<Integer> flatList = flattenStreamInteger(arr).collect(Collectors.toList());
        System.out.println(flatList);

        System.out.println("=====================================================");
        List<Character> d = Arrays.asList('G', 'e', 'e', 'k', 's');
        List<Character> e = Arrays.asList('F', 'o', 'r');
        List<Character> f = Arrays.asList('G', 'e', 'e', 'k', 's');

        List<List<Character>> arrChar = new ArrayList<List<Character>>();
        arrChar.add(d);
        arrChar.add(e);
        arrChar.add(f);

        List<Character> flatListChar = flattenStreamCharacter(arrChar).collect(Collectors.toList());
        System.out.println(flatListChar);


    }

    // Using lists of integer.
    private static <T> Stream<T> flattenStreamInteger(List<List<T>> lists) {
        List<T> finalList = new ArrayList<>();
        for (List<T> list : lists) {
            list.stream().forEach(finalList::add);
        }

        return finalList.stream();
    }

    //Using lists of Characters.
    public static <T> Stream<T> flattenStreamCharacter(List<List<T>> lists) {
        List<T> finalList = new ArrayList<>();
        for (List<T> list : lists) {
            list.stream().forEach(finalList::add);
        }
        return finalList.stream();
    }

}

