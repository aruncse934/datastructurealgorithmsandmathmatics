package IOStream.Java8.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FlattenStreamArrayforEachLoop {

    // Using arrays of integer.
    public static <T> Stream<T> flattenStreamInteger(T[][] arr) {
        List<T> list = new ArrayList<>();
        for (T[] ar : arr) {
            Arrays.stream(ar).forEach(list::add);
        }
        return list.stream(); // Convert the list into Stream and return it
    }

    //Using arrays of character.
    public static <T> Stream<T> flattenStreamCharacter(T[][] arrChar) {
        List<T> list = new ArrayList<>();
        for (T[] arChar : arrChar) {
            Arrays.stream(arChar).forEach(list::add);
        }
        return list.stream();
    }

    public static void main(String[] args) {
        Integer[][] arr = {{1, 2}, {3, 4, 5, 6}, {7, 8, 9}};
        Integer[] flatArray = flattenStreamInteger(arr).toArray(Integer[]::new);
        System.out.println(Arrays.toString(flatArray));

        System.out.println("============================================");
        Character[][] arrChar = {{'G', 'e', 'e', 'k', 's'}, {'F', 'o', 'r'}, {'G', 'e', 'e', 'k', 's'}};
        Character[] flatArr = flattenStreamCharacter(arrChar).toArray(Character[]::new);
        System.out.println(Arrays.toString(flatArr));


    }

}
