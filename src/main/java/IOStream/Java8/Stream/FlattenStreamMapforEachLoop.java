package IOStream.Java8.Stream;

import java.util.*;

public class FlattenStreamMapforEachLoop {

    //
    public static <T> List<T> flattenStreamInteger(Collection<List<T>> lists){
        List<T> finalList = new ArrayList<>();
        for(List<T> list : lists){
            list.stream().forEach(finalList::add);
        }
        return finalList;
    }

    //Using lists of Characters.
    public static <T> List<T> flattenStreamCharacter(Collection<List<T>> lists){
        List<T> finalList = new ArrayList<>();
        for(List<T> list : lists){
            list.stream().forEach(finalList::add);
        }
        return finalList;
    }

    public static void main(String[] args){
        Map<Integer,List<Integer>> map = new HashMap<>();
        map.put(1, Arrays.asList(1,2));
        map.put(2,Arrays.asList(3,4,5,6));
        map.put(3,Arrays.asList(7,8,9));

        List<Integer> flatList = flattenStreamInteger(map.values());
        System.out.println(flatList);

        System.out.println("==================");
        Map<Integer, List<Character>> listMap = new HashMap<>();
        listMap.put(1,Arrays.asList('G','e','e','k','s'));
        listMap.put(2,Arrays.asList('F','o','r'));
        listMap.put(3,Arrays.asList('G','e','e','k','s'));

        List<Character> flatList1 = flattenStreamCharacter(listMap.values()) ;
        System.out.println(flatList1);
    }

}

