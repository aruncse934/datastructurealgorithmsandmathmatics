package IOStream.Java8.Stream;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class FilterStreamAPI {

    //Filter By Object Properties
    public static void filterByEvenElements() {
        Integer[] arr = new Integer[]{1, 4, 5, 7, 9, 10};
        Stream.of(arr).filter(x -> x % 2 == 0).forEach(System.out::println);
    }
    public static void filterByStartswith() {
        String[] str = new String[]{"stream", "is", "a", "sequence", "of", "elements", "like", "list"};
        Stream<String> stm = Stream.of(str);
        stm.filter(x -> x.startsWith("s")).forEach(System.out::println);
    }
    public static void filterByStartsWithVowelsRegex() {
        String[] str1 = "I am 24 years old and I want to be in Tier I company".split(" ");
        Stream<String> stm1 = Stream.of(str1);
        stm1.filter(x -> x.matches("(a|e|i|o|u)\\w*")).forEach(System.out::println);
    }

    //filter by Object Indices -> using AtomicInteger
    public static void filterByIndexUsingAtomicInteger(){
        String[] arr = new String[] {"stream", "is", "a","sequence", "of", "elements","like","list" };
        Stream<String> mst = Stream.of(arr);
        AtomicInteger i  = new AtomicInteger(0);
        mst.filter(x -> i.getAndIncrement() % 2 == 0).forEach(System.out::println);
    }

    public static void main(String[] args) {

        //Stream Filter API
        Stream<String> stream = Stream.of("Like", "and", "share", "https://www.google.com");
        stream.filter(x -> x.startsWith("https://")).forEach(System.out::println);

        //filters a stream by even elements
        System.out.println("=====================");
        filterByEvenElements();

        //filters a stream by starting string
        System.out.println("======================");
        filterByStartswith();

        //filters a stream by starting vowels
        System.out.println("======================");
        filterByStartsWithVowelsRegex();

        // filter by Object index
        System.out.println("======================");
       filterByIndexUsingAtomicInteger();


    }
}
/*

 */