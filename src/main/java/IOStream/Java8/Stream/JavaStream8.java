package IOStream.Java8.Stream;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JavaStream8 {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<String> res = new ArrayList<String>();
        Map<String, List<String>> map = new HashMap<>();
        for(String s : strs){
            char[] c = s.toCharArray();
            Arrays.sort(c);
            String str = String.valueOf(c);
            if(!map.containsKey(str)){
                map.put(str,res);
            }
            map.get(str).add(s);
        }

        return new ArrayList<>(map.values());

    }

    public static void main(String[] args) {
        String s = "ilovejavastreamprogram";

        //count the occurrence of each character in a string
        Map<String, Long> map = Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(map);

        //find all duplicate element from a given string
        List<String> mapduplicate = Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(x -> x.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        System.out.println(mapduplicate);

        //find all unique element from a given string
        List<String> uniqueelement = Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(x -> x.getValue() == 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        System.out.println(uniqueelement);

        //find first non-repeate element from a string

        String firstnorepeEle = Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(Function.identity(),LinkedHashMap::new, Collectors.counting()))
                .entrySet().stream()
                .filter(x -> x.getValue() == 1)
                .findFirst().get().getKey();

        System.out.println(firstnorepeEle);


        //find the second highest element in array integer
        int[] num = {4,7,9,2,8,11,5,17};
        Integer secondHightestNum = Arrays.stream(num).boxed()
                .sorted(Comparator.reverseOrder())
                .skip(1)
                .findFirst().get();
        System.out.println(secondHightestNum);

        //find longest string from given array
        String[] longestStrings = {"java","core Java","Microservices","Spring Boot","Spring Frameworks"};
        String longString = Arrays.stream(longestStrings)
                .reduce((a, b) -> a.length() > b.length() ? a : b)
                .get();
        System.out.println(longString);

         //find all elements from array who starts with 1
        int[] arr = {23,4,5,6,3,1,11,34,56,22,31};
        List<String> nuym = Arrays.stream(arr)
                .boxed()
                .map(i -> i + "")
                .filter(x -> x.startsWith("3"))
                .collect(Collectors.toList());
        System.out.println(nuym);

        
         //Reverse String words
        String str = "Hello World Java";
        String reversewords = Arrays.stream(str.split("\\s+"))
                .filter(word -> !word.isEmpty())  //.filter(word -> word.length() != 0)
                .reduce((word1, word2) -> word2 + " " + word1)
                .orElse("");
        System.out.println(reversewords);

        //first non-repeating character in the given string
        List<String> list = Arrays.asList("") ;
        
    }
}
