package IOStream.Java8.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Student {
    private String name;
    private int id;
    private String subjects;
    private double percentage;
}
