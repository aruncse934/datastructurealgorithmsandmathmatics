package IOStream.Java8.Stream;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConvertStringToListCharacters {

    //
    public static List<Character> convertStringToCharList(String str){
        List<Character> chars = new ArrayList<>();
        for(char ch : str.toCharArray()){
            chars.add(ch);
        }
        return chars;
    }

    //
    public static List<Character> convertStringToCharList1(String str){
        List<Character> chars = str.chars().mapToObj(e -> (char)e).collect(Collectors.toList());// Convert to String to IntStream ->  .chars() // Convert IntStream to Stream<Character> -> .mapToObj(e -> (char)e) // Collect the elements as a List Of Characters -> .collect(Collectors.toList());
        return chars;
    }

    //
    public static List<Character> convertStringToCharList2(String str){
        return new AbstractList<Character>(){
            @Override
            public Character get(int index) {
                return str.charAt(index);
            }

            @Override
            public int size() {
                return str.length();
            }
        };
    }
    public static void main(String[] args) {
        String str = "Geeks";
        List<Character> chars = convertStringToCharList(str);
        System.out.println(chars);

        System.out.println("===========================");
        String str1 = "Geek";
        List<Character> chars1 = convertStringToCharList1(str1);
        System.out.println(chars1);

        System.out.println("===========================");
        String str2 = "Facebook";
        List<Character> chars2 = convertStringToCharList2(str2);
        System.out.println(chars2);
    }
}
