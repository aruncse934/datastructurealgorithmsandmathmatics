package IOStream.Java8.Stream;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
class Employee{
    private String name;
    private Integer age;
    private Double salary;

}

public class EmployeeDetails {
}
