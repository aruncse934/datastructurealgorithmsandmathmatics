package IOStream.Java8.Stream;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;

//Java – Stream has already been operated upon or closed
public class Test8 {

    //first non-repeating character in the given string
   public static Character nonRepeating(String s){
       Map<Character, Integer> map = new HashMap<>();
       for(int i =0; i<s.length();i++){
           if(map.get(s.charAt(i)) == null){
               map.put(s.charAt(i),1);
           }
           else {
               return s.charAt(i);
           }
       }
       return null;
   }
    public static void main(String[] args) {


        String[] array = {"a", "b", "c", "d", "e"};

        System.out.println("============Reuse a stream=================");
        Supplier<Stream<String>> supplier = () -> Stream.of(array);
        supplier.get().forEach(x -> System.out.println(x));

        long count1 = supplier.get().filter(x -> "b".equals(x)).count();
        System.out.println(count1);


        System.out.println("============Stream has already been operated upon or closed=================");
        Stream<String> strm = Arrays.stream(array);
        strm.forEach(x -> System.out.println(x));

        long count = strm.filter(x -> "b".equals(x)).count();
        System.out.println(count);



        //
        String s = "HelloJava";
        System.out.println(nonRepeating(s));


    }
}
