package IOStream.Java8.Stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapExample {

    public static void main(String[] args) {
     Map<String, List<String>> people = new HashMap<>();
       people.put("John", Arrays.asList("555-1123","555-3389"));
       people.put("Mary", Arrays.asList("555-1113","555-3379"));
       people.put("Steve", Arrays.asList("555-1121","555-3369"));

        List<String> phones = people.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println("flatmap"+phones);

        List<Stream<String>> phones1 = people.values().stream().map(Collection::stream).collect(Collectors.toList());
        System.out.println(phones1);
    }
}

/*Map<String, List<String>> people = new HashMap<>();
people.put("John", Arrays.asList("555-1123", "555-3389"));
people.put("Mary", Arrays.asList("555-2243", "555-5264"));
people.put("Steve", Arrays.asList("555-6654", "555-3242"));

List<String> phones = people.values().stream()
  .flatMap(Collection::stream)
    .collect(Collectors.toList());*/