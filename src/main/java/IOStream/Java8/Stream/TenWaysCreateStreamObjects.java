package IOStream.Java8.Stream;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class TenWaysCreateStreamObjects {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Geeks");
        list.add("for");
        list.add("Geeks");
        getStream(list);

        System.out.println("\n-----------");

        getStream1();

        System.out.println("\n-----------");
        String[] str  = new String[] { "a", "b", "c" };
        getStream2(str);

        System.out.println("\n-----------");
        String[] sr = new String[]{ "a", "b", "c" };
        getStream3(sr);

        System.out.println("\n-----------");
        getStream4();

        System.out.println("\n-----------");
        getStream5();

        System.out.println("\n-----------");
        int seedValue = 2;
        int limitTerms = 5;
        getStream6(seedValue,limitTerms);

        System.out.println("\n-----------");
        int limitTerms2 = 5;
        getStream7(limitTerms2);

        System.out.println("\n-----------");
        List<String> stringList =Arrays.asList("Geeks",
                "For",
                "Geek",
                "GeeksForGeeks",
                "A Computer Portal");
        Pattern pattern = Pattern.compile("^G");
        getStream8(stringList,pattern);

        System.out.println("\n-----------");
        Iterator<String> stringIterator = Arrays.asList("a","b","c").iterator();
        getStream9(stringIterator);

        System.out.println("\n-----------");
        Iterable<String> iterable
                = Arrays.asList("a", "b", "c");
        getStream10(iterable);

    }

    //Using Collection Approach:
    private static <T> void getStream(List<T> list){
        Stream<T> stream = list.stream(); //Function convert a list into stream
        Iterator<T> it = stream.iterator();
        while (it.hasNext()){
            System.out.print(it.next()+" ");
        }
    }

    //Create a stream from specified values
    private static void getStream1(){
        Stream<Integer> stream = Stream.of(1,2,3,4,5,6,7,8,9);
        stream.forEach( p-> System.out.print(p+" "));
    }

    //Create stream from an array
    private static <T> void getStream2(T[] arr){
        Stream<T> streamOfArray = Arrays.stream(arr);
        Iterator<T> itr =streamOfArray.iterator();
        while (itr.hasNext()){
            System.out.print(itr.next()+ " ");
        }
    }

    //Create stream using Stream.of()
    private static <T> void getStream3(T[] arr){
        Stream<T> streamOfArray = Stream.of(arr);
        Iterator<T> itr = streamOfArray.iterator();
        while (itr.hasNext()){
            System.out.print(itr.next()+ " ");
        }
    }
    //Create an empty stream using Stream.empty()
    private static  void getStream4(){
        Stream<String> stream = Stream.empty();
        Iterator<String> it = stream.iterator();
        while (it.hasNext()){
            System.out.println(it.next()+" ");
        }
    }

    //Create a Stream using Stream.builder()
    private static <T> void getStream5(){
        Stream.Builder<String> builder = Stream.builder();
        Stream<String> stream = builder
                .add("a")
                .add("b")
                .add("c")
                .build();
        Iterator<String> itr = stream.iterator();
        while (itr.hasNext()){
            System.out.print(itr.next()+" ");
        }
    }

    //Create an infinite Stream using Stream.iterate()
    private static <T> void getStream6(int seedValue, int limitTerms){

        Stream.iterate(seedValue,n -> n*n)
                .limit(limitTerms)
                .forEach(System.out::println);

    }

    //Create an infinite Stream using Stream.generate() method
    private static <T> void getStream7(int limitTerms){
        Stream.generate(Math::random)
                .limit(limitTerms)
                .forEach(System.out::println);
    }
    //Create stream from a Pattern using Predicate
    private static void getStream8(List<String> list, Pattern p){
        list.stream()
            .filter(p.asPredicate())
            .forEach(System.out::println);
    }

    //Create stream from Iterator
    private static <T> void getStream9(Iterator<T> itr){
        Spliterator<T> split = Spliterators.spliteratorUnknownSize(itr,Spliterator.NONNULL);// Convert the iterator into a Spliterator
        Stream<T> tStream = StreamSupport.stream(split,false); // Convert spliterator into a sequential stream
        Iterator<T> iter = tStream.iterator();
        while (iter.hasNext()){
            System.out.println(iter.next()+" ");
        }
    }
    //
    private static <T> void getStream10(Iterable<T> iterable){
        Stream<T> stream = StreamSupport.stream(iterable.spliterator(),false);
        Iterator<T> it = stream.iterator();
        while (it.hasNext()){
            System.out.println(it.next()+" ");
        }
    }
 }
