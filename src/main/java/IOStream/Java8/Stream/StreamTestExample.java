package IOStream.Java8.Stream;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTestExample {
    public static void main(String[] args) {
        List<String> myList = Arrays.asList("a1","a2","b1","c2","c1");
        myList
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);

        System.out.println("=====================");

        Arrays.asList("a1","a2","a3","b1")
                .stream()
                .findFirst()
                .ifPresent(System.out::println);

        System.out.println("=====================");

        Stream.of("a","a1","a3","a4")
                .findFirst()
                .ifPresent(System.out::println);

        System.out.println("=====================");
        IntStream.range(1,10)
                .forEach(System.out::println);

        System.out.println("=====================");
        Arrays.stream(new int[]{1,2,3})
                .map(n->2*n+1)
                .average()
                .ifPresent(System.out::println);

        System.out.println("=====================");
        Stream.of("a1","a2","a3","a4")
                .map(s -> s.substring(1))
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(System.out::println);


        System.out.println("=====================");
        IntStream.range(1,4)
                .mapToObj(i -> "b"+i)
                .forEach(System.out::println);

        System.out.println("=====================");
        Stream.of(1.0,2.0,3.0)
                .mapToInt(Double::intValue)
                .mapToObj(i -> "a" + i)
                .forEach(System.out::println);

        System.out.println("=====================");
        Stream.of("d2","a2","b1","b3","c")
                .filter(s ->{
                    System.out.println("filter :"+s);
                    return true;
                });

        System.out.println("=====================");
        Stream.of("d2","a2","b1","b3","c")
                .filter(s -> {
                    System.out.println("filter : "+s);
                    return true;
                })
                .forEach(s -> System.out.println("forEach: "+s));

        System.out.println("=====================");
        Stream.of("d2","a2","b1","b3","c")
                .map(s -> {
                    System.out.println("map: "+ s);
                    return s.toUpperCase();
                })
                .anyMatch(s -> {
                    System.out.println("anyMatch: "+s);
                    return s.startsWith("A");
                });

        System.out.println("==========Parallel Streams===========");
        Arrays.asList("a1","a2","a3","b1","b2","c1","c2","c3")
                .parallelStream()
                .filter(s -> {
                    System.out.format("filter: %s [%s]\n",s,Thread.currentThread().getName());
                    return true;
                })

                .map( s -> {
                    System.out.format("forEach: %s [%s]\n", s, Thread.currentThread().getName());
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.format("forEach: %s [%s]\n",s,Thread.currentThread().getName()));
        }
    }



/*Arrays.asList("a1", "a2", "b1", "c2", "c1")
    .parallelStream()
    .filter(s -> {
        System.out.format("filter: %s [%s]\n",
            s, Thread.currentThread().getName());
        return true;
    })
    .map(s -> {
        System.out.format("map: %s [%s]\n",
            s, Thread.currentThread().getName());
        return s.toUpperCase();
    })
    .forEach(s -> System.out.format("forEach: %s [%s]\n",
        s, Thread.currentThread().getName())); */