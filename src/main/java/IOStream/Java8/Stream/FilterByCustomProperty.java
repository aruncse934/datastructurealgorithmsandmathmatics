package IOStream.Java8.Stream;

import java.util.Arrays;
import java.util.List;

public class FilterByCustomProperty {

    class Employee{

        String name;
        int age;

        public Employee(String name, int age) {
            this.name = name;
            this.age = age;
        }
        @Override
        public String toString() { //return "Employee [name=" + name + "]";
            return "Employee{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
        public  void filterByAge(){
           // List<Employee> list = Arrays.asList(new Employee("Arun",26),)
        }


    }

}
/*

	public static void filterByAge()
	{
		// create list of Employees
		List<Employee> myList = Arrays.asList(
			new GFG().new Employee("Ram", 25),
			new GFG().new Employee("Kumar", 40),
			new GFG().new Employee("Rakesh", 35));

		// create a stream on the list
		// filter by age of an employee
		myList.stream()
			.filter(x -> x.age >= 35)
			.forEach(System.out::println);
	}

	public static void main(String[] args)
	{
		filterByAge();
	}
}
*/