package IOStream.Java8.Stream;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ConvertArrayToStream {
    public static void main(String[] args) {

        //For object arrays, both Arrays.stream and Stream.of returns the same output.
        String[] array = {"a", "b", "c", "d", "e"};

        Stream<String> stm = Arrays.stream(array);
        stm.forEach(x -> System.out.println(x));

        Stream<String> stn = Stream.of(array);
        stn.forEach( x-> System.out.print(x));

        //For primitive array, the Arrays.stream and Stream.of will return different output.

        int arr[] = {1,2,3,4,5,6};
        IntStream sam = Arrays.stream(arr);
        sam.forEach(x-> System.out.println("\n"+x));

        Stream<int[]> temp = Stream.of(arr);
        IntStream intStream = temp.flatMapToInt( x -> Arrays.stream(x));
        intStream.forEach(x -> System.out.print(x));


    }
}
/*IntStream intStream1 = Arrays.stream(intArray);
        intStream1.forEach(x -> System.out.println(x));

        // 2. Stream.of -> Stream<int[]>
        Stream<int[]> temp = Stream.of(intArray);

        // Cant print Stream<int[]> directly, convert / flat it to IntStream
        IntStream intStream2 = temp.flatMapToInt(x -> Arrays.stream(x));
        intStream2.forEach(x -> System.out.println(x));*/