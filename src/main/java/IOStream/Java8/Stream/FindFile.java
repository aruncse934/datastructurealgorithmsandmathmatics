package IOStream.Java8.Stream;

import java.io.File;
import java.util.Scanner;

public class FindFile {

    public static void findFile(String name, File file){
        File[] files= file.listFiles();
        if(files != null)
            for (File file1 : files){
                if(file1.isDirectory()){
                    findFile( name,file );
                }
                else if (name.equalsIgnoreCase( file1.getName() )){
                    System.out.println(file1.getParentFile());
                }
            }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner( System.in );
        System.out.println("Enter the file to be searched...");
        String name = sc.next();
        System.out.println("Enter the Directory where to search ");
        String directory = sc.next();

        findFile( name,new File( directory ) );
    }
}
