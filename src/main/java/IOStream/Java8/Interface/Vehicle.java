package IOStream.Java8.Interface;

public interface Vehicle {

    void changeGear(int a);
    void speedUp(int b);
    void applyBrakes(int c);
}

class Bicycle implements Vehicle{

    int speed;
    int gear;

    @Override
    public void changeGear(int newgear) {

        gear = newgear;
    }

    @Override
    public void speedUp(int increament) {

        speed += increament;
    }

    @Override
    public void applyBrakes(int decrement) {

        speed -= decrement;
    }
    public void printStates() {
        System.out.println("speed: " + speed + " gear: " + gear);
    }
}
class Bike implements Vehicle{
    int speed;
    int gear;

    @Override
    public void changeGear(int newGear) {
        gear = newGear;
    }

    @Override
    public void speedUp(int increament) {

        speed += increament;
    }

    @Override
    public void applyBrakes(int decrement) {

        speed -= decrement;
    }

    public void printStates() {
        System.out.println("speed: " + speed + " gear: " + gear);
    }
}

class VehicleTest{
    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle();
        bicycle.changeGear(2);
        bicycle.speedUp(3);
        bicycle.applyBrakes(1);

        System.out.println("Bicycle present state :");
        bicycle.printStates();

        Bike bike = new Bike();
        bike.changeGear(1);
        bike.speedUp(4);
        bike.applyBrakes(3);

        System.out.println("Bike present state :");
        bike.printStates();
    }
}