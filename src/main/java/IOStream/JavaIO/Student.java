package IOStream.JavaIO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Student implements Serializable {
    public int id;
    public String name;
    public String address;

    public static void main(String[] args) {
        Student s = new Student();
        s.id = 101;
        s.name="arun";
        s.address="Bangalore";

        try{

            FileOutputStream fileOutputStream = new FileOutputStream("s.txt");
            //Seriallizing object
            ObjectOutputStream out =new ObjectOutputStream(fileOutputStream);
            out.writeObject(s);
            out.close();
            fileOutputStream.close();
            System.out.printf("Object Serialized");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
