                #Functional Programming 

###What is Functional Programming in Java?
Functional programming is a paradigm where the basic unit of computation is a function. Here functions are not the methods we write in programming. Those are called procedures where we write a list of instructions to tell the computer what to do. In functional programming, functions are treated like mathematical functions where we map inputs to outputs to produce a result. Eg. f(x) = 3xf(x)=3x.

In functional programming, the software is developed around the evaluation of functions. The functions are isolated and independent, and they depend only on the arguments passed to them and do not alter the program's state like modifying a global variable. Functions operate on immutable data and have no side effects on the program.

Functional programming is implemented using the concepts, First-Class Functions, Pure Functions, Immutability and Referential Transparency by avoiding shared states and side effects.

###Characteristics of Functional Programming Language
Functions are First Class Citizens
In Functional programming, functions are considered first-class citizens. A function is first-class if it can be stored in a variable and passed as an argument to a function like the other primitive types, and it is achieved via Functional Interface.

####What is a functional interface?
A functional interface is an interface that contains exactly one abstract method. It is also called Single Abstract Method (SAM) Interfaces. It can have any number of default methods. In Java, an interface is annotated with @FunctionalInterface to make it a functional interface. @FunctionalInterface annotation ensures that the interface can't have more than one abstract method. Functional interfaces are meant to be used with lambdas.

###Principles and Concepts of Functional Programming in Java

####First-Class and Higher-Order Functions
#####First-Class Functions
A function is said to be First-Class if treated like other variables in a programming language. A First-Class function satisfies the below criteria.

1. A function can be assigned to a variable.
2. A function can be passed as an argument to another function.
3. A function can be returned from another function.

#####Higher-Order Functions
A function is said to be Higher-Order if it satisfies any criteria below.

1. A function receives another function as an argument.
2. A function that returns a new function.

#####Pure Functions
A function is said to be pure if it satisfies the below criteria.

1. The return value of a function operates only on the arguments passed.
2. A function returns the same output, given the same input.
3. A function doesn't have any side effects (i.e.) modifying global or local state.

#####Immutability
Immutability means that an object cannot be modified once created. Java's immutability can be achieved using the final keyword and private access modifier. Immutable class is created in Java using the following rules.

1. The class must be declared as final, which prevents it from subclassing.
2. All fields must be private and final.
3. The class should have one or more public constructors for instantiation.
4. The class should have only getters and not setters.

#####Referential Transparency
An expression is referentially transparent if it can be replaced with its corresponding value without changing the program's behavior.

If all functions involved in the expression are pure functions, then the expression is referentially transparent. -- Wikipedia

###Functional Programming Techniques
####Function Composition
Function composition is the process of composing bigger functions by combining smaller functions. Function composition can be achieved in Java by Predicate and Function functional interfaces.

####Predicate
Predicate is a functional interface that accepts exactly one input and can return a boolean output. It provides built-in methods like and(), or(), negate(), and isEqual() methods to compose multiple functions. The composed function can be executed using the test() method. The syntax is Predicate<T> where T is the input type. Java also provides BiPredicate that accepts two inputs.

#####and()

Returns true only if all the predicates return true, else false. It behaves like logical AND.

predicateA.and(predicateB).and(predicateC)...test(value);

#####or()

Returns true if any of the predicates return true, else false. It behaves like logical OR.

predicateA.or(predicateB).or(predicateC)...test(value);

#####negate()

Returns true if the predicate returns false and vice versa. It behaves like logical NOT.

predicateA.negate()...test(value)

#####isEqual()

Returns a predicate that checks if two values are equal. The equality is decided via Object.equals() method.

Predicate.isEqual(value1).test(value2);

####Function
Function is also a functional interface that accepts exactly one input and can return any output. Function provides built-in methods like compose() and andThen() to compose multiple functions. The composed function can be executed using the apply() method. The syntax is Function<T, U> where T and U are the type of input and output, respectively. Java also provides BiFunction that accepts two inputs.

#####compose()

predicateA.compose(predicateY).test(value);
compose() method firt executes the predicateA with value and then executes predicateB with the result of predicateA. The above statement can be expanded as below.

temp = predicateA.apply(value);
result = predicateB.apply(temp);

#####andThen()

predicateA.andThen(predicateY).test(value);
andThen() method firt executes the predicateB with value and then executes predicateA with the result of predicateB. The above statement can be expanded as below.

temp = predicateB.apply(value);
result = predicateA.apply(temp);

Note:-> predicateA.compose(predicateB) is equivalent to predicateB.andThen(predicateA) and produces the same result.

####Monads
Monad is a technique which allows us to wrap a value and apply series of transformation to it. Mondas should follow the three laws, left identity, right identity and associativity. Optional is a good example of monad in Java.

####Currying
Currying is converting a function with multiple arguments into multiple functions with a single argument.

####Recursion
Recursion is the technique of making a function call itself. Since the function is called repeatedly, a base condition is required to break the loop.

###Why Functional Programming Matters?
Our ability to decompose a problem into parts depends directly on our ability to glue solutions together. To support modular programming, a language must provide good glue. Functional programming languages provide two new kinds of glue — higher-order functions and lazy evaluation.

#####-- Why functional programming matters by John Hughes

* Functional programming operates on functions, and a function is responsible for accomplishing a particular task. This encourages the concept of modularity.
* A program can be divided into several modules where each module is responsible for a specific task.
* Each module can be subdivided into several modules by delegating the responsibilities. In functional programming, we define a function as a module.

We can achieve the modularity by the two clues -- higher-order functions and lazy evaluation. By providing granular responsibilities, we can write code that is readable, maintainable, easily debuggable, and less error-prone. These are the factors of a successful programming paradigm.

###Functional Programming Paradigm vs. Object-Oriented Programming Paradigm

Functional Programming Paradigm
1. Functional Programming Paradigm	
2. Functional programming follows declarative programming paradigm.	
3. Functional programming uses immutable data.
4. Functional programming supports parallel programming.	
5. Functional programming operates on variables and functions.	
6. Functional programming uses recursion for iteration.	
7. Functional programming is stateless.

Object-Oriented Programming Paradigm	
1. Object-Oriented Programming Paradigm
2. Object-Oriented programming follows imperative programming paradigm.
3. Object-Oriented programming uses mutable data.
4. Object-Oriented programming doesn't support parallel programming.
5. Object-Oriented programming operates on objects and methods.
6. Object-Oriented programming uses loops for iteration.
7. Object-Oriented programming is stateful.

####Advantages of the Functional Programming
* Functional programming uses pure functions, which only depends on the input parameters passed to the function and always produces the same output for the given input.
* Functional programming deals with immutable data (i.e.) the state doesn't change, so it is easier to debug.
* Functional programming supports parallel programming because pure functions don't change the state and operate exclusively on the input parameters.
* Functional programming supports lazy evaluation, which avoids unwanted and repeated computations and saves time.
* Functional programming is thread-safe because pure functions don't modify the state of a program, and there is no shared state.
* In functional programming, functions can be stored in variables and passed to other functions. This enhances the readability of code.