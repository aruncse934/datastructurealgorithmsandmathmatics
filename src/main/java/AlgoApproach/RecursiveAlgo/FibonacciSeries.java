package AlgoApproach.RecursiveAlgo;

public class FibonacciSeries {
    public static void main(String[] args) {
        int n = 40;

        long arr = fib(n);
        for (int i = 0; i < n; i++) {
            System.out.print(fib(n)+ " ");
        }
    }


    public static long fib(int n){
        if(n == 2)
            return 1;
        else if(n == 1)
            return 0;
        else
            return fib( n-1)+fib( n-2 );

        /*//static int fib(int n) {
            double phi = (1 + Math.sqrt(5)) / 2;
            return  Math.round(Math.pow(phi, n)
                    / Math.sqrt(5));
*/
    }
/*
    long[] fib = new long[n];
    fib[0] = 1;
    fib[1] = 1;
        for (int i = 2; i < n; ++i) {
        fib[i] = fib[i - 2] + fib[i - 1];
    }
        return fib;*/
}
