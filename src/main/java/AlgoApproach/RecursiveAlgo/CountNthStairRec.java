package AlgoApproach.RecursiveAlgo;

public class CountNthStairRec {
    public static void main(String[] args) {
        int n = 3;
        System.out.println(countWayNthStair(n));
    }
    public static int countWayNthStair(int n){
        if(n==1){
            return 1;
        }

        else if (n==0)
            return 0;
        else
            return countWayNthStair( n-1 )+countWayNthStair( n-2 );
    }
}
