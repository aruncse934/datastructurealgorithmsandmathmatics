package AlgoApproach.Tree.BinarySearchTree;

public class MaximumSumBST_in_BinaryTree {

    public int max;
    public int maxSumBST(TreeNode root) {
        traverse(root);
        return max;
    }

    public int[] traverse(TreeNode root) {
        if(root == null) {
            return null;
        }
        if(root.left == null && root.right == null) {
            max = Math.max(max, root.val);
            return new int[]{root.val, root.val, root.val};
        }
        int[] res = new int[]{-1, Integer.MAX_VALUE, Integer.MIN_VALUE};
        int[] left = traverse(root.left);
        int[] right = traverse(root.right);
        if(left != null && right != null && root.val > left[2] && root.val < right[1] && isSubValid(left) && isSubValid(right)) {
            res[1] = left[1];
            res[2] = right[2];
            res[0] = root.val + left[0] + right[0];
            max = Math.max(max, res[0]);
        }else {
            if(left != null && root.val > left[2] && root.right == null && isSubValid(left)) {
                res[1] = left[1];
                res[2] = root.val;
                res[0] = root.val + left[0];
                max = Math.max(max, res[0]);
            }
            if(right != null && root.val < right[1] && root.left == null && isSubValid(right)) {
                res[2] = right[2];
                res[1] = root.val;
                res[0] = root.val + right[0];
                max = Math.max(max, res[0]);
            }
        }
        return res;
    }

    public boolean isSubValid(int[] nums) {
        return nums == null || nums[1] <= nums[2];
    }
}
/*Given a binary tree root, the task is to return the maximum sum of all keys of any sub-tree which is also a Binary Search Tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:



Input: root = [1,4,3,2,4,2,5,null,null,null,null,null,null,4,6]
Output: 20
Explanation: Maximum sum in a valid Binary search tree is obtained in root node with key equal to 3.
Example 2:



Input: root = [4,3,null,1,2]
Output: 2
Explanation: Maximum sum in a valid Binary search tree is obtained in a single root node with key equal to 2.
Example 3:

Input: root = [-4,-2,-5]
Output: 0
Explanation: All values are negatives. Return an empty BST.
Example 4:

Input: root = [2,1,3]
Output: 6
Example 5:

Input: root = [5,4,8,3,null,6,3]
Output: 7


Constraints:

Each tree has at most 40000 nodes..
Each node's value is between [-4 * 10^4 , 4 * 10^4].

*/