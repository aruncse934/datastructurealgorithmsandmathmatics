package AlgoApproach.Tree.BinarySearchTree;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

public class KthSmallestElementBST {
    TreeNode root;
    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(3);
        root.right = new TreeNode(6);
        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(4);
        root.left.left = new TreeNode(1);
        int k = 3;
        System.out.println(kthSmallestElementBST(root,k));
        System.out.println("----------------------------------");
        System.out.println(KthSmallest(root,k));
        System.out.println("==================");
        System.out.println(KTHSmallest(root,k));
    }
    public  static int  kthSmallestElementBST(TreeNode root, int k){
        Stack<TreeNode> stack = new Stack<>();
        TreeNode p = root;
        int res = 0;
        while(!stack.isEmpty() || p!= null){
            if(p!= null){
                stack.push(p);
                p = p.left;
            }else{
                TreeNode t = stack.pop();
                k--;
                if(k == 0)
                    res = t.val;
                p = t.right;
            }
        }
        return res;
    }
    //recursive method
    public static ArrayList<Integer> inorder(TreeNode root, ArrayList<Integer> arr){
        if(root == null)
            return arr;
        inorder(root.left,arr);
        arr.add(root.val);
        inorder(root.right,arr);
        return arr;
    }
    public static int KthSmallest(TreeNode root, int k){
        ArrayList<Integer> nums = inorder(root, new ArrayList<>());
        return nums.get(k-1);
    }

    //Iterative Method
    public static int KTHSmallest(TreeNode root, int k){
        LinkedList<TreeNode> ll = new LinkedList<>();
        while (true){
            while (root != null){
                ll.add(root);
                root = root.left;
            }
            root = ll.removeLast();
            if(--k ==0)
                return root.val;
            root = root.right;
        }
    }

    /*
      if (--k == 0) return root.val;
      root = root.right;
    }
  }
}
}*/
}
/*Kth Smallest Element in a BST
Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.

Note:
You may assume k is always valid, 1 ≤ k ≤ BST's total elements.

Example 1:

Input: root = [3,1,4,null,2], k = 1
   3
  / \
 1   4
  \
   2
Output: 1
Example 2:

Input: root = [5,3,6,2,4,null,null,1], k = 3
       5
      / \
     3   6
    / \
   2   4
  /
 1
Output: 3
Follow up:
What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? How would you optimize the kthSmallest routine?*/