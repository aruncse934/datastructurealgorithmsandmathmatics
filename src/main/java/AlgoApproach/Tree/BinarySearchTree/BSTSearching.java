package AlgoApproach.Tree.BinarySearchTree;
public class BSTSearching {
    TreeNode root;
    int val;


    public TreeNode search(TreeNode root, int val){
        if (root==null || root.val==val)
            return root;
        if (root.val > val)
            return search(root.left, val);
        return search(root.right, val);
    }

    public static void main(String[] args) {
        BSTSearching bst = new BSTSearching();
        bst.root = new TreeNode(1);
        bst.root.left = new TreeNode(2);
        bst.root.right = new TreeNode(3);
        bst.root.left.left = new TreeNode(4);
        bst.root.left.right = new TreeNode(5);
        bst.root.right.left = new TreeNode(6);
        bst.root.right.right = new TreeNode(7);
        int res = bst.search();

        System.out.println(res);
    }

    private int search() {
        search(root,val);
        return 0;
    }
}
