package AlgoApproach.Tree.BinarySearchTree;

import java.util.Stack;

public class ValidBST {
    TreeNode root;

    //recursive method
    public boolean  validBstUtil(TreeNode root, Integer lower, Integer upper){
        if(root == null)
            return true;
        int val = root.val;
        if(lower != null && val <= lower)
            return false;
        if(upper != null && val >= upper)
            return false;
        if(!validBstUtil(root.right, val, upper))
            return false;
        if(!validBstUtil(root.left,lower,val))
            return false;
        return true;
    }
    public boolean isValidBST(TreeNode root){
        return validBstUtil(root,null,null);
    }

    //Eifficent technq
    public boolean  validBstUtil2(TreeNode root, Integer lower, Integer upper) {
        if (root == null)
            return true;
        if (root.val < lower || root.val > upper)
            return false;
        return (validBstUtil(root.left, lower, root.val - 1) && validBstUtil(root.right, root.val + 1, upper));
    }
    public boolean isValidBST2(TreeNode root){
        return validBstUtil(root,Integer.MIN_VALUE,Integer.MAX_VALUE);
    }



    //InOrder Traversal techq
    public boolean isValidBSTs(TreeNode root){

        Stack<TreeNode> stack = new Stack<>();
        double inorder = - Double.MAX_VALUE;
        while (!stack.isEmpty() || root != null){
            while (root != null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if(root.val <= inorder)
                return false;
            inorder = root.val;
            root = root.right;
        }
        return true;
    }
    public static void main(String[] args) {
        ValidBST bst = new ValidBST();//[5,1,4,null,null,3,6]
       /* bst.root = new TreeNode(5);
        bst.root.left = new TreeNode(1);
        bst.root.right = new TreeNode(4);
        bst.root.right.left = new TreeNode(3);
        bst.root.right.right = new TreeNode(6);*/

        bst.root = new TreeNode(4);
        bst.root.left = new TreeNode(2);
        bst.root.right = new TreeNode(5);
        bst.root.left.left = new TreeNode(1);
        bst.root.left.right = new TreeNode(3);

        if(bst.isValidBST(bst.root))
            System.out.println("True");
        else
            System.out.println("False");

        System.out.println("---------------------------------------------------");

        if(bst.isValidBST2(bst.root))
            System.out.println("True");
        else
            System.out.println("False");
    }
}
/*Given a binary tree, determine if it is a valid binary search tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:

    2
   / \
  1   3

Input: [2,1,3]
Output: true
Example 2:

    5
   / \
  1   4
     / \
    3   6

Input: [5,1,4,null,null,3,6]
Output: false
Explanation: The root node's value is 5 but its right child's value is 4.*/