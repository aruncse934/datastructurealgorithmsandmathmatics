package AlgoApproach.Tree.BinarySearchTree;

public class BSTGreaterSumTree {

    TreeNode root;

    int pre = 0;

    private void bstToGst() {
        bstToGst(root);
    }
    public TreeNode bstToGst(TreeNode root) {
        if (root.right != null)
            bstToGst(root.right);
        pre = root.val = pre + root.val;

        if (root.left != null)
            bstToGst(root.left);
        return root;
    }
    public static void main(String[] args) {
       BSTGreaterSumTree tree = new BSTGreaterSumTree();
        tree.root = new TreeNode(4);
        tree.root.left = new TreeNode(1);
        tree.root.right = new TreeNode(6);
        tree.root.left.left = new TreeNode(0);
        tree.root.left.right = new TreeNode(2);
        tree.root.right.left = new TreeNode(5);
        tree.root.right.right = new TreeNode(7);
       // tree.root.right.right = new TreeNode(null);
       // tree.root.right.right = new TreeNode(null);
        //tree.root.right.right = new TreeNode(null);
        tree.root.right.right = new TreeNode(3);
        //tree.root.right.right = new TreeNode(null);
        //tree.root.right.right = new TreeNode(null);
        //tree.root.right.right = new TreeNode(null);
        tree.root.right.right = new TreeNode(8);
        tree.bstToGst();

    }


}


/*Given the root of a binary search tree with distinct values, modify it so that every node has a new value equal to the sum of the values of the original tree that are greater than or equal to node.val.

As a reminder, a binary search tree is a tree that satisfies these constraints:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:



Input: [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]


Note:

The number of nodes in the tree is between 1 and 100.
Each node will have value between 0 and 100.
The given tree is a binary search tree.
 */