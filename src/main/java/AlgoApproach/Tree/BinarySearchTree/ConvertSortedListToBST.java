package AlgoApproach.Tree.BinarySearchTree;

import java.util.ArrayList;
import java.util.List;

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
 }
public class ConvertSortedListToBST {
    public static void main(String[] args) {
        ConvertSortedListToBST bst = new ConvertSortedListToBST();
        List<Integer> list = new ArrayList<Integer>();

    }
    private ListNode findMiddleElement(ListNode head){
        ListNode prePtr = null;
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while(fastPtr != null && fastPtr.next != null){
            prePtr = slowPtr;
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;
        }
        if(prePtr != null){
            prePtr.next = null;
        }
        return slowPtr;
    }
    //recursion method
    public TreeNode covertSortedListToBST(ListNode head){

        if(head == null){
            return null;
        }
  ListNode mid = this.findMiddleElement(head);
        TreeNode node = new TreeNode(mid.val);
        if(head == mid){
            return node;
        }
        node.left = this.covertSortedListToBST(head);
        node.right = this.covertSortedListToBST(mid.next);
        return node;
    }
}
