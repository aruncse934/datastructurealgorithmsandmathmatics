package AlgoApproach.Tree.BinarySearchTree;

public class TwoSumBSTs {
    public boolean twoSumBSTs(TreeNode root1, TreeNode root2, int target) {
        // Base cases
        if(root1 == null || root2 == null) return false;
        if(root1.val + root2.val == target) return true;

        if(root1.val + root2.val < target) {
            // Go right in either tree if sum is smaller
            return twoSumBSTs(root1.right, root2, target) || twoSumBSTs(root1, root2.right, target);

        } else { // root1.val + root2.val > target
            // Go left in either tree if sum is larger
            return twoSumBSTs(root1.left, root2, target) || twoSumBSTs(root1, root2.left, target);
        }
    }

}
/*Given two binary search trees, return True if and only if there is a node in the first tree and a node in the second tree whose values sum up to a given integer target.



Example 1:



Input: root1 = [2,1,4], root2 = [1,0,3], target = 5
Output: true
Explanation: 2 and 3 sum up to 5.
Example 2:



Input: root1 = [0,-10,10], root2 = [5,1,7,0,2], target = 18
Output: false


Constraints:

Each tree has at most 5000 nodes.
-10^9 <= target, node.val <= 10^9
*/