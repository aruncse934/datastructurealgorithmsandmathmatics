package AlgoApproach.Tree.BinarySearchTree;

 public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(Integer x) {
            val = x;
        }
    }
