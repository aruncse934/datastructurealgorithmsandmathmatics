package AlgoApproach.Tree.BinarySearchTree;

public class ConvertSortedArrayToBST {
    static TreeNode root;
    public static void main(String[] args) {
        ConvertSortedArrayToBST bst = new ConvertSortedArrayToBST();
        int arr[] = new int[]{-10,-3, 0, 5, 9};
        int n = arr.length;
        root = bst.convertSortedArrayToBST(arr);
        System.out.println("Preorder traversal of constructed BST");
        bst.preOrder(root);
    }

    public TreeNode convertSortedArrayToBST(int[] num){
        if(num.length == 0){
            return null;
        }
        TreeNode head = helper(num, 0,num.length-1);
        return head;
    }
    public TreeNode helper(int[] num, int low, int high){
        if(low > high){
            return null;
        }
        //int mid = (low+high)/2;
        int mid = low + (high-low)/2;
        TreeNode node = new TreeNode(num[mid]);
        node.left = helper(num,low,mid-1);
        node.right = helper(num,mid+1,high);
        return node;
    }
    void preOrder(TreeNode node) {
        if (node == null) {
            return;
        }
        System.out.print(node.val + " ");
        preOrder(node.left);
        preOrder(node.right);
    }
}
