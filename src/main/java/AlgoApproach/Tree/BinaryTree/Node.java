package AlgoApproach.Tree.BinaryTree;

public class Node {
    int val;
    Node left;
    Node right;
    Node(Integer x) {
        val = x;
    }
}
