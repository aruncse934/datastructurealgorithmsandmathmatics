package AlgoApproach.Tree.BinaryTree;

public class LongestConsecutiveSequence {
    public static void main(String[] args) {

        Node root = new Node(1);
        root.right = new Node(3);
        root.right.left = new Node(2);
        root.right.right = new Node(4);
        root.right.right.right = new Node(5);
        System.out.println(longestConsecutiveSequence(root));
        System.out.println("--------------");
        System.out.println(LCS(root));
    }

    //dfs method
    public static int longestConsecutiveSequence(Node root){
           if(root == null) {
               return 0;
           }
           return dfs(root,1);
    }

    private static int dfs(Node root, int depth){
        if(root == null){
            return depth;
        }
        int leftDepth = (root.left != null && root.val + 1 == root.left.val)? dfs(root.left,depth+1) : dfs(root.left,1);
        int rightDepth = (root.right != null && root.val+1 == root.right.val)? dfs(root.right,depth+1): dfs(root.right,1);
        return Math.max(depth,Math.max(leftDepth,rightDepth));
    }

    //recursive method
    public static  int LCS(Node root){
        return recursiveHelper(root,null,0);
    }
    public static int recursiveHelper(Node curr,Node parent, int depth){
        if(curr == null){
            return 0;
        }
        int currDepth = 0;
        if(parent != null && parent.val + 1 == curr.val){
            currDepth = depth+1;
        }else {
            currDepth = 1;
        }
        return Math.max(currDepth, Math.max(recursiveHelper(curr.left,curr,currDepth),recursiveHelper(curr.right,curr,currDepth)));
    }
}
/*
Given a binary tree, find the length of the longest consecutive sequence path.

The path refers to any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The longest consecutive path need to be from parent to child (cannot be the reverse).

Have you met this question in a real interview?
Example
Example 1:

Input:
   1
    \
     3
    / \
   2   4
        \
         5
Output:3
Explanation:
Longest consecutive sequence path is 3-4-5, so return 3.
Example 2:

Input:
   2
    \
     3
    /
   2
  /
 1
Output:2
Explanation:
Longest consecutive sequence path is 2-3,not 3-2-1, so return 2.

*/