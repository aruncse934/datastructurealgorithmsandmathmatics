package AlgoApproach.Tree.BinaryTree;

public class BinaryTreeMaximumPathSum {

     private Node root;
     int max = Integer.MIN_VALUE;

    private int maxPathSum() {
        return maxPathSum(root);
    }
    public int maxPathSum(Node root){
        maxPathUtil(root);
        return max;
    }

    public int maxPathUtil(Node root){
        if(root == null){
            return 0;
        }
        int L = Math.max(maxPathUtil(root.left),0);
        int R = Math.max(maxPathUtil(root.right),0);
        max = Math.max(max,root.val + L + R);
        return root.val + Math.max(L,R);
    }


    public static void main(String[] args){
        BinaryTreeMaximumPathSum pathSum = new BinaryTreeMaximumPathSum();
        pathSum.root =new Node(-10);//[-10,9,20,null,null,15,7
        pathSum.root.left = new Node(9);
        pathSum.root.right = new Node(20);
        pathSum.root.right.left = new Node(15);
        pathSum.root.right.right = new Node(7);

        System.out.println("maximum path sum is : " + pathSum.maxPathSum());
    }

}
/*   5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1*/