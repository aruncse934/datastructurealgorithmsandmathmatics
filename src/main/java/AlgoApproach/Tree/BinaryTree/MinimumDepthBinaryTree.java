package AlgoApproach.Tree.BinaryTree;

public class MinimumDepthBinaryTree {

    Node root;


//recursive method

    private int minimumDepthBinaryTree() {
        return minimumDepthBinaryTree(root);
    }

    public int minimumDepthBinaryTree(Node root){

        if (root == null)
            return 0;
        int L = minimumDepthBinaryTree(root.left), R = minimumDepthBinaryTree(root.right);
        return L<R && L>0 || R<1 ? 1+L : 1+R;

        /*if(root == null) return 0;
        if(root.left == null || root.right == null) {
            return 1 + Math.max(minimumDepthBinaryTree(root.left), minimumDepthBinaryTree(root.right));
        }
        return 1 + Math.min(minimumDepthBinaryTree(root.left), minimumDepthBinaryTree(root.right));*/

        /*  public int minDepth(TreeNode root) {
              if (root == null) return 0;
            int L = minDepth(root.left), R = minDepth(root.right);
           return 1 + (Math.min(L, R) > 0 ? Math.min(L, R) : Math.max(L, R));
          }

        public int minDepth(TreeNode root) {
          if (root == null) return 0;
          int L = minDepth(root.left), R = minDepth(root.right), m = Math.min(L, R);
          return 1 + (m > 0 ? m : Math.max(L, R));
        }
       */
    }

    public static void main(String[] args) {

        MinimumDepthBinaryTree tree = new MinimumDepthBinaryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);

        System.out.println("The minimum depth of " + "binary tree is : " + tree.minimumDepthBinaryTree());
    }
}
