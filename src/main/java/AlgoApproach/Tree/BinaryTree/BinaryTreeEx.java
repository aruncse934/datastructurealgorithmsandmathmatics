package AlgoApproach.Tree.BinaryTree;

public class BinaryTreeEx {
    Node root = null;
    BinaryTreeEx(Integer val) {
        root = new Node(val);
    }
    BinaryTreeEx(){
        root = null;
    }

    public static void main(String[] args) {
        BinaryTreeEx treeEx = new BinaryTreeEx();
        treeEx.root = new Node(1);
        treeEx.root.left = new Node(2);
        treeEx.root.right = new Node(3);
        treeEx.root.left.left = new Node(2);
        treeEx.root.right.right = new Node(3);
    }
}
