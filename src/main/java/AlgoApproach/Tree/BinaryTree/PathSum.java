package AlgoApproach.Tree.BinaryTree;

public class PathSum {
    Node root;

    public int pathSum(Node root){
        if(root == null){
            return 0;
        }
        int L = Math.max(pathSum(root.left),0);
        int R = Math.max(pathSum(root.right),0);

        return 0;
    }

    public static void main(String[] args) {
        PathSum pathSum = new PathSum();
        pathSum.root = new Node(5);
        pathSum.root.left = new Node(4);
        pathSum.root.right = new Node(8);
        pathSum.root.left.left = new Node(11);
        pathSum.root.left.left.left = new Node(7);
        pathSum.root.left.left.right = new Node(2);
        pathSum.root.right.left = new Node(13);
        pathSum.root.right.right = new Node(4);
        pathSum.root.right.right.right = new Node(1);

    }
}
