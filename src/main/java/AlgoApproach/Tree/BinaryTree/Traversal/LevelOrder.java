package AlgoApproach.Tree.BinaryTree.Traversal;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LevelOrder {
    TreeNode root;

    public static void main(String[] args) {
        LevelOrder order = new LevelOrder();
        order.root = new TreeNode(1);
        order.root.left = new TreeNode(2);
        order.root.right = new TreeNode(3);
        order.root.left.left = new TreeNode(4);
        order.root.left.right = new TreeNode(5);
        order.levelOrder();
        System.out.println("\n===========================");
        order.levelOrderList();
    }

    private void levelOrderList() {
        levelOrderList(root);
    }

    public void levelOrder() {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode tempNode = queue.poll();
            System.out.print(tempNode.val + " ");
            if (tempNode.left != null) {
                queue.add(tempNode.left);
            }
            if (tempNode.right != null) {
                queue.add(tempNode.right);
            }
        }
    }
    public List<List<Integer>> levelOrderList(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> wrapList = new LinkedList<List<Integer>>();

        if (root == null)
            return wrapList;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int levelNum = queue.size();
            List<Integer> subList = new LinkedList<Integer>();
            for (int i = 0; i < levelNum; i++) {
                if (queue.peek().left != null)
                    queue.offer(queue.peek().left);
                if (queue.peek().right != null)
                    queue.offer(queue.peek().right);
                subList.add(queue.poll().val);
                System.out.println();
            }
            wrapList.add(subList);
        }
        return wrapList;
    }
}
