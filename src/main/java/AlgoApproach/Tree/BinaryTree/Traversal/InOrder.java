package AlgoApproach.Tree.BinaryTree.Traversal;

import java.util.ArrayList;
import java.util.List;

public class InOrder {
    TreeNode root;
    public static void main(String[] args) {
        InOrder order = new InOrder();
        order.root = new TreeNode(1);
        order.root.right = new TreeNode(2);
        order.root.right.left = new TreeNode(3);
        order.inOrder();
        System.out.println("\n=======");
        order.inOrderlist();

    }

    //recursive method
    private void inOrder() {
        inOrder(root);
    }
    public void inOrder(TreeNode root){
        if(root == null)
            return;
        inOrder(root.left);
        System.out.print(root.val+" ");
        inOrder(root.right);
    }


    //recursive method
    private void inOrderlist() {
        inOrderlist(root);
    }
    public List<Integer> inOrderlist(TreeNode root){
        List<Integer> res = new ArrayList<>();
        helper(root,res);
        return res;
    }
    public  void helper(TreeNode root, List<Integer> res){
        if(root != null){
            if(root.left != null){
                helper(root.left,res);
            }
            res.add(root.val);
            System.out.println(res);

            if(root.right != null){
                helper(root.right,res);
            }
        }
    }
}
