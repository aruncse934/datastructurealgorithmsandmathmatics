package AlgoApproach.Tree.BinaryTree.Traversal;

import java.sql.SQLOutput;
import java.util.*;

public class VerticalOrder {
    TreeNode root;
    public static void main(String[] args) {
        //  VerticalOrder order = new VerticalOrder();
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right= new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.left.right = new TreeNode(8);
        root.right.right.right = new TreeNode(9);
        printVerticalOrder(root);
        System.out.println("++++++++++++++++++++++++++++++++++++");
        TreeNode r = new TreeNode(3);
        r.left = new TreeNode(9);
        r.right = new TreeNode(20);
        r.right.left = new TreeNode(15);
        r.right.right = new TreeNode(7);
        printVerticalOrder(r);


    }
    //Map based method
    static void verticalOrders(TreeNode root, int hd, TreeMap<Integer, Vector<Integer>> m){

        if(root == null)
            return;
        Vector<Integer> get = m.get(hd);
        if(get == null){
            get = new Vector<>();
            get.add(root.val);
        }
        else
            get.add(root.val);
        m.put(hd,get);
        verticalOrders(root.left,hd-1,m);
        verticalOrders(root.right,hd+1,m);

    }
    static void printVerticalOrder(TreeNode root){
        TreeMap<Integer,Vector<Integer>> treeMap  = new TreeMap<>();
        int hd = 0;
        verticalOrders(root,hd,treeMap);

        for (Map.Entry<Integer, Vector<Integer>> entry : treeMap.entrySet())
        {
            System.out.println(entry.getValue());
        }
    }
}
/* private void verticalTraversal() {
        verticalTraversal(root);
    }

    List<Location> locations;
        public List<List<Integer>> verticalTraversal(TreeNode root) {
            // Each location is a node's x position, y position, and value
            locations = new ArrayList();
            dfs(root, 0, 0);
            Collections.sort(locations);

            List<List<Integer>> ans = new ArrayList();
            ans.add(new ArrayList<Integer>());

            int prev = locations.get(0).x;

            for (Location loc: locations) {
                // If the x value changed, it's part of a new report.
                if (loc.x != prev) {
                    prev = loc.x;
                    ans.add(new ArrayList<Integer>());
                }

                // We always add the node's value to the latest report.
                ans.get(ans.size() - 1).add(loc.val);
            }

            return ans;
        }

        public void dfs(TreeNode node, int x, int y) {
            if (node != null) {
                locations.add(new Location(x, y, node.val));
                dfs(node.left, x-1, y+1);
                dfs(node.right, x+1, y+1);
            }
        }
    }

    class Location implements Comparable<Location>{
        int x, y, val;
        Location(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        @Override
        public int compareTo(Location that) {
            if (this.x != that.x)
                return Integer.compare(this.x, that.x);
            else if (this.y != that.y)
                return Integer.compare(this.y, that.y);
            else
                return Integer.compare(this.val, that.val);
        }
    }*/