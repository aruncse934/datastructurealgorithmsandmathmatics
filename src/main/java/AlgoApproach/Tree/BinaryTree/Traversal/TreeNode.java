package AlgoApproach.Tree.BinaryTree.Traversal;

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(Integer x) {
        val = x;
    }
}
