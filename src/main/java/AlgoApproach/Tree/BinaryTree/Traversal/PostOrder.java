package AlgoApproach.Tree.BinaryTree.Traversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrder {
    TreeNode root;
    public static void main(String[] args) {
        PostOrder order = new PostOrder();
        order.root = new TreeNode(1);
        order.root.right = new TreeNode(2);
        order.root.right.left = new TreeNode(3);

        order.postOrder();
        System.out.println("\n==============================");
        order.postOrderList();

    }
    private void postOrder(){
        postOrder(root);
    }
    public void postOrder(TreeNode root){
        if(root == null)
            return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val+ " ");
    }
    private void postOrderList(){
        postOrderList(root);
    }

    public List<Integer> postOrderList(TreeNode root){
        List<Integer> res = new ArrayList<>();
        helper(root,res);
        return res;
    }
    public void helper(TreeNode root ,List<Integer> res){
        if(root != null){
            if(root.left != null){
                helper(root.left,res);
            }
            if(root.right != null){
                helper(root.right,res);
            }
            res.add(root.val);
            System.out.println(res);
        }
    }
}
