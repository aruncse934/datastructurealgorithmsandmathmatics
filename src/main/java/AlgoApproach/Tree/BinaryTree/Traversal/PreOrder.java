package AlgoApproach.Tree.BinaryTree.Traversal;

import java.util.ArrayList;
import java.util.List;

public class PreOrder {
    TreeNode root;
    public static void main(String[] args) {

        PreOrder order = new PreOrder();
        order.root = new TreeNode(1);
        order.root.right = new TreeNode(2);
        order.root.right.left = new TreeNode(3);
        order.preOrder();
        System.out.println("\n=========================================================================================================================================================");
        order.preOrderTraversal();

    }

    //recursive method
    private void preOrder() {
        preOrder(root);
    }
    public static void preOrder(TreeNode root){
        if(root == null)
            return;

        System.out.print(root.val+" ");
        preOrder(root.left);
        preOrder(root.right);

    }

    //recursive method
    private void preOrderTraversal() {
        preOrderTraversal(root);
    }
    public List<Integer> preOrderTraversal(TreeNode root) {
        List<Integer> res  = new ArrayList<>();
        helper(root,res);
        return res;
    }
    public void helper(TreeNode root, List<Integer> res){
        if(root != null){
            res.add(root.val);
            System.out.println(res);

            if(root.left != null){
                helper(root.left, res);
            }

            if(root.right != null){
                helper(root.right,res);
            }
        }
    }
}

