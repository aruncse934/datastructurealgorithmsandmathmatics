package AlgoApproach.Tree.BinaryTree.Traversal;

public class LevelOrder2 {
    TreeNode root;
    public static void main(String[] args) {
        LevelOrder2 order2 = new LevelOrder2();
        order2.root = new TreeNode(2);
        order2.root.left = new TreeNode(3);
        order2.root.right = new TreeNode(4);
        order2.root.left.left = new TreeNode(5);
        order2.root.left.right = new TreeNode(6);
        order2.root.right.left = new TreeNode(7);
        order2.root.right.right = new TreeNode(8);

        order2.levelOrder2();
    }

    private void levelOrder2() {
        levelOrder2(root);
    }

    public void levelOrder2(TreeNode root){
        if(root == null)
            return;

        levelOrder2(root.left);
        levelOrder2(root.right);
    }
}
