package AlgoApproach.Tree.BinaryTree;

import java.util.LinkedList;
import java.util.Queue;

public class InvertOrMirrorBinaryTree {

    Node root;

    //recursive method O(n) | this is best approach
    private void mirrorBinaryTree() {
         mirrorBinaryTree(root);
    }
    public Node mirrorBinaryTree(Node root){

        if(root == null){
            return null;
        }
        Node right = mirrorBinaryTree(root.right);
        Node left = mirrorBinaryTree(root.left);
        root.left = right;
        root.right = left;
        return root;
    }

    private void invertTree() {
        invertTree(root);
    }

    //Iterative method O(n)
    public Node invertTree(Node root){
        if(root == null){
            return null;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            Node current = queue.poll();

            Node temp = current.left;//swap
            current.left = current.right;
            current.right = temp;

            if(current.left != null)
                queue.add(current.left);
            if(current.right != null)
                queue.add(current.right);
        }
        return root;
    }
    private void inOrder(){
       inOrder(root);
    }

     public void inOrder(Node root){
        if(root == null)
            return;
        inOrder(root.left);
        System.out.print(root.val+" ");
        inOrder(root.right);
    }

    public static void main(String[] args) {

        InvertOrMirrorBinaryTree tree = new InvertOrMirrorBinaryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.right.right = new Node(5);

        tree.mirrorBinaryTree();
        tree.inOrder();

        System.out.println("-------------------------------------");
        tree.invertTree();
        tree.inOrder();
    }
}


/*Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1

*/