package AlgoApproach.Tree.DepthFirstSearch;

public class Node {

    int val;
    Node left;
    Node right;

    public Node(int x){
        val = x;
    }
}
