package AlgoApproach.Tree.DepthFirstSearch;

public class PathSum {
    public static void main(String[] args) {

        Node root = new Node(5);
        root.left = new Node(4);
        root.right = new Node(8);
        root.left.left = new Node(11);
        root.left.left.left = new Node(7);
        root.left.left.right = new Node(2);
        root.right.left = new Node(13);
        root.right.right = new Node(4);
        root.right.right.right = new Node(1);
        int sum =22;

        if (hasPathSum(root, sum))
            System.out.println("There is a root to leaf path with sum " + sum);
        else
            System.out.println("There is no root to leaf path with sum " + sum);

    }

    public static void printPathSum(Node root){
        if(root == null)
            return;
        printPathSum(root.left);
        System.out.println(root.val+" ");
        printPathSum(root.right);
    }
    public static boolean hasPathSum(Node root,int sum){

        if (root == null)
            return false;
        if(root.val == sum && (root.left == null && root.right == null)) return true;
        //if(root.left == null && root.right == null) return sum == root.val;//optimized
        return hasPathSum(root.left, sum-root.val) ||hasPathSum(root.right, sum-root.val);
    }

}

