package AlgoApproach.Tree.DepthFirstSearch;

public class ConstructBtInOrderNdPreOrder {
    public static void main(String[] args) {
        ConstructBtInOrderNdPreOrder bt = new ConstructBtInOrderNdPreOrder();
        int in[] = new int[] { 9,3,15,20,7 };
        int post[] = new int[] { 9,15,7,20,3};
      //  int n = in.length;
        Node root = bt.buildTree(in, post);
        System.out.println("Preorder of the constructed tree : ");
        bt.preOrder(root);
    }

    public Node buildTree(int[] inorder, int[] postOrder){
        return helper(inorder,postOrder,postOrder.length-1,0,inorder.length-1);
    }


    public Node helper(int[] inorder,  int[] postorder,  int ppos, int is, int ie){
        if (ppos >= postorder.length || is > ie)
            return null;
        Node node = new Node(postorder[ppos]);
        int pii = 0;
        for(int i =0;i<inorder.length;i++){
            if(inorder[i] == postorder[ppos])
                pii= i;
        }

        node.left = helper(inorder,postorder,ppos-1-ie+pii,is,pii-1);
        node.right = helper(inorder,postorder,ppos-1,pii+1,ie);
        return node;
    }
    void preOrder(Node node)
    {
        if (node == null)
            return;
        System.out.print(node.val + " ");
        preOrder(node.left);
        preOrder(node.right);
    }

}
