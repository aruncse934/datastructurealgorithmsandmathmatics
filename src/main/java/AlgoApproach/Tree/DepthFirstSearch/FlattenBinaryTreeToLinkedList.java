package AlgoApproach.Tree.DepthFirstSearch;

public class FlattenBinaryTreeToLinkedList {
    Node root;
    private static Node prev;

    public static void main(String[] args) {
        //FlattenBinaryTreeToLinkedList tree = new FlattenBinaryTreeToLinkedList();

        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(5);
        root.left.left = new Node(3);
        root.left.right = new Node(4);
        root.right.right = new Node(6);
        System.out.println("Original inorder traversal : ");
        printFBT(root);

        System.out.println("-----------------------------------");

        flattenBinaryTreeToLinkedlist(root);
        printFBT(root);

    }

    public static void flattenBinaryTreeToLinkedlist(Node root){

        if(root == null)
            return;
        flattenBinaryTreeToLinkedlist(root.right);
        flattenBinaryTreeToLinkedlist(root.left);
        root.right = prev;
        root.left = null;
        prev = root;
    }

    public static void printFBT(Node root){
        if(root == null)
            return;
        printFBT(root.left);
        System.out.println(root.val+" ");
        printFBT(root.right);
    }
}
