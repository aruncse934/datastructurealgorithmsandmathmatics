package AlgoApproach.Tree.DepthFirstSearch;

import java.util.HashMap;
import java.util.Map;

public class ContructBtPreorderNdInorder {
    public static void main(String[] args) {
        ContructBtPreorderNdInorder tree = new ContructBtPreorderNdInorder();

        int preorder[] = new int[]{3,9,20,15,7};
        int inorder[] = new int[]{9,3,15,20,7};
        Node root =tree.buildTre(preorder,inorder);
        System.out.println("PostOrder of the constructed tree :");
        tree.postorder(root);
    }

    public Node buildTre(int[] preorder, int[] inorder){
        Map<Integer,Integer> inMap = new HashMap<>();
        for(int i = 0;i<inorder.length;i++){
            inMap.put(inorder[i],i);
        }
        Node root = buildTree(preorder,0,preorder.length-1,inorder,0,inorder.length-1,inMap);
        return root;
    }

    public Node buildTree(int[] preorder,int prestart,int preEnd,int[] inorder, int instart, int inend, Map<Integer,Integer> inMap){
        if(prestart > preEnd || instart > inend)
            return null;
        Node root = new Node(preorder[prestart]);
        int inRoot = inMap.get(root.val);
        int numsleft = inRoot - instart;
        root.left = buildTree(preorder,prestart+1,prestart+numsleft,inorder,instart,inRoot-1,inMap);
        root.right = buildTree(preorder,prestart+numsleft+1,preEnd,inorder,inRoot+1,inend,inMap);

        return root;
    }

    public void postorder(Node root){
        if(root == null)
            return;
        postorder(root.left);
        postorder(root.right);
        System.out.print(root.val+" ");
    }
}
