package AlgoApproach.Tree.DepthFirstSearch;

import java.util.ArrayDeque;

public class SameTree {
    Node root1, root2;

    //recursive
    public boolean isSameTree(Node p, Node q) {
        if (p == null && q == null)
            return true;
        if (q == null || p == null)
            return false;
        if (p.val != q.val)
            return false;
        return isSameTree(p.right, q.right) && isSameTree(p.left, q.left);
    }

    //iterative
    public boolean check(Node p , Node q){
        if(p == null && q == null)
            return true;
        if(q == null || p == null)
            return false;
        if(p.val != q.val)
            return false;
        return true;
    }
    public boolean isSameTrees(Node p , Node q){
        if(p == null && q == null)
            return true;
        if(!check(p,q))
            return false;

        ArrayDeque<Node> nodes = new ArrayDeque<>();
        ArrayDeque<Node> nodes1 = new ArrayDeque<>();
        nodes.addLast(p);
        nodes1.addLast(q);

        while(!nodes.isEmpty()){
            p = nodes.removeFirst();
            q =nodes1.removeFirst();
        }

        if(!check(p,q))
            return false;
        if(p != null){
            if(!check(p.left,q.left))
                return false;
            if(p.left != null){
                nodes.addLast(p.left);
                nodes1.addLast(q.left);
            }
            if(!check(p.right, q.right))
                return false;
            if(p.right != null){
                nodes.addLast(p.right);
                nodes1.addLast(q.right);
            }
        }
        return true;
    }
    public static void main(String[] args) {
        SameTree tree = new SameTree();
        tree.root1 = new Node(1);
        tree.root1.left = new Node(2);
        tree.root1.right = new Node(3);

        tree.root2 = new Node(1);
        tree.root2.left = new Node(2);
        tree.root2.right = new Node(3);//[1,2],     [1,null,2]

        if (tree.isSameTree(tree.root1, tree.root2))
            System.out.println("same tree");
        else
            System.out.println("not same tree");

    }
}