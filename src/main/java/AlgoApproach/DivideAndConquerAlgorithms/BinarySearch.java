package AlgoApproach.DivideAndConquerAlgorithms;

public class BinarySearch {

        public static void main(String[] args) {
            int arr[] ={1,2, 3, 4, 10, 40};
            int x = 40;
            int n =arr.length;

            System.out.println(binarySearch1( arr,0,n-1,x));
            System.out.println(binarySearch2( arr,0,n-1,x));

            System.out.println(binarySearchs( arr,x ));
        }

        //recursion1 TC O(logn)
        public static int binarySearch1(int arr[],int left,int right,int x){
            if(left > right)
                return -1;
            int mid =(left+right)/2;

            if(x == arr[mid]) {
                return mid;
            }
            else if(x < arr[mid]) {
                return binarySearch1( arr, left, mid - 1, x );
            }
            else {
                return binarySearch1( arr, mid + 1, right, x );
            }
        }

        //Recursion2 TC O(logn)
        public static int binarySearch2(int arr[], int left,int right, int x){
            if( right >= left){
                int  mid = left + (right-left)/2;
                if(arr[mid] == x)
                    return mid;
                if(arr[mid] > x)
                    return binarySearch2( arr,left,mid-1,x );
                return binarySearch2( arr,mid+1,right,x);
            }
            return -1;
        }

        //Iterations TC O(logn)
        public static int binarySearchs(int arr[],int x){
            int left = 0;
            int right = arr.length-1;
            while (left <= right){
                int mid = (left + right)/2;
                if(x == arr[mid]){
                    return mid;
                }
                else if(x < arr[mid]){
                    right = mid-1;
                }
                else
                    left = mid+1;
            }
            return -1;
        }
    }


