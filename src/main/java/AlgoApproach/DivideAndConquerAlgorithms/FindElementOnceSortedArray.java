package AlgoApproach.DivideAndConquerAlgorithms;

public class FindElementOnceSortedArray {

    public static int singleNonDuplicate(int[] nums) {
        // corner case
        if(nums == null || nums.length == 0)
            return -1;

        int lo = 0;
        int hi = nums.length - 1;
        while(lo < hi){
            int mid = lo + (hi - lo)/2;
            // trick here int temp = mid % 2 == 0 ? mid + 1: mid - 1;
            int temp = mid ^ 1; // if even, mid + 1; if odd, mid - 1
            if(nums[mid] == nums[temp]){
                // if mid is even, then nums[mid] = nums[mid + 1], single number is >= mid + 2
                // if mid is odd, then nums[mid] = nums[mid - 1], single number is >= mid + 1
                // so we choose mid + 1
                lo = mid + 1;
            }else{
                // maybe nums[hi] is the single numer or
                // maybe the single number is to the left of nums[hi]
                // <= hi
                hi = mid;
            }
        }

        return nums[lo];
    }

        public static void search(int[] arr, int low, int high)
        {
            if(low > high)
                return;
            if(low == high)
            {
                System.out.println("The required element is "+arr[low]);
                return;
            }
            int mid = (low + high)/2;

            // If mid is even and element next to mid is
            // same as mid, then output element lies on
            // right side, else on left side
            if(mid % 2 == 0)
            {
                if(arr[mid] == arr[mid+1])
                    search(arr, mid+2, high);
                else
                    search(arr, low, mid);
            }
            // If mid is odd
            else if(mid % 2 == 1)
            {
                if(arr[mid] == arr[mid-1])
                    search(arr, mid+1, high);
                else
                    search(arr, low, mid-1);
            }
        }

    public static void main(String[] args) {
    int nums[] = {3,3,7,7,10,11,11};
        System.out.println(singleNonDuplicate( nums ));
        search(nums, 0, nums.length-1);
  }
}
