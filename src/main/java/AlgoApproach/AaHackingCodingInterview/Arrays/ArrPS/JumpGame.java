package AlgoApproach.AaHackingCodingInterview.Arrays.ArrPS;

import java.util.Scanner;

public class JumpGame {

        public static int fun(int arr[],int n)
        {
            int dp[] = new int[n];
            dp[0] = 0;
            for(int i=1;i<n;i++)
                dp[i] = Integer.MAX_VALUE;
            for(int i=1;i<n;i++)
            {
                for(int j=0;j<i;j++)
                {
                    if(j+arr[j] >= i)
                    {
                        if(dp[j] != Integer.MAX_VALUE)
                        {
                            dp[i] = Math.min(dp[i],dp[j]+1);
                        }
                    }
                }
            }
            if(dp[n-1] == Integer.MAX_VALUE)
                return -1;
            return dp[n-1];
        }
    public static int jump(int[] A) {
        int jumps = 0, curEnd = 0, curFarthest = 0;
        for (int i = 0; i < A.length - 1; i++) {
            curFarthest = Math.max(curFarthest, i + A[i]);
            if (i == curEnd) {
                jumps++;
                curEnd = curFarthest;
            }
        }
        return jumps;
    }
    private static int Jump(int[] num)
    {
        int i=0;
        int a = num[0];
        int b = num[0];
        int jump = 1;

        for (i = 1; i < num.length; i++)
        {
            if(i == num.length - 1)
                return jump;
            a--;
            b--;
            if(num[i] > b)
            {
                b = num[i];
            }
            if(a == 0)
            {
                jump++;
                a=b;
            }

        }
        return jump;
    }
    /*public int jump(int arr[]){
        int i =0;
        int a= arr[0];
        int b = arr[0];
        int jump =1;
        for(i=1;i<arr.length;i++) {
            if (i == arr.length - 1)
                return jump;
            a--;
            b--;
        }
            if(arr[i] > b){
                b = arr[i];
            }
            if(a==0){
                jump++;
                a=b;
            }
            return jump;
    }*/
    public static void main(String[] args) {
        /*Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-->0){
            int n = sc.nextInt();
            int arr[] = new int[n];
            for (int i =0;i<n;i++){
                arr[i] = sc.nextInt();
            }
            JumpGame game = new JumpGame();
            System.out.println(game.Jump(arr));

        }

        public static void main (String[] args) throws IOException {
		//code
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine().trim());
		for(int v=0;v<t;v++)
		{
		    int n = Integer.parseInt(br.readLine().trim());
		    int arr[] = new int[n];
		    String[] s = br.readLine().trim().split("\\s+");
		    for(int i=0;i<n;i++)
		        arr[i] = Integer.parseInt(s[i]);
		    System.out.println(fun(arr,n));

		}
	}
*/
        int num[] = { 3,2,1,0,4};

        System.out.println("Minimum number of jumps to reach end is : "
                + Jump(num));

    }
}
/*Input:
2
11
1 3 5 8 9 2 6 7 6 8 9 //2,3,1,1,4]
6
1 4 3 2 6 7 //3,2,1,0,4
Output:
3
2*/