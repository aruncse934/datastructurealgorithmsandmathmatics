package AlgoApproach.AaHackingCodingInterview.Arrays.ArrPS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/*Input :
2
5
0 2 1 2 0
3
0 1 0

Output:
0 0 1 2 2
0 0 1*/

public class SortArrayOf0s1sNd2s {
    public static void sort01(int[] array, int end){
        int start = 0, mid = 0;
        int pivot = 1;
        while (mid <= end){
            if (array[mid] < pivot){
                swap(array, start, mid);
                ++start;
                ++mid;
            }
            else if (array[mid] > pivot){
                swap(array, mid, end);
                --end;
            }
            else
                ++mid;
        }
    }

    private static void swap(int[] array , int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public void sortColors(int[] nums) {
        // 2-pass
        int count0 = 0, count1 = 0, count2 = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {count0++;}
            if (nums[i] == 1) {count1++;}
            if (nums[i] == 2) {count2++;}
        }
        for(int i = 0; i < nums.length; i++) {
            if (i < count0) {nums[i] = 0;}
            else if (i < count0 + count1) {nums[i] = 1;}
            else {nums[i] = 2;}
        }
    }
    void sort012(int a[], int arr_size){
        int lo = 0;
        int hi = arr_size - 1;
        int mid = 0, temp = 0;
        while (mid <= hi) {
            switch (a[mid]) {
                case 0: {
                    temp = a[lo];
                    a[lo] = a[mid];
                    a[mid] = temp;
                    lo++;
                    mid++;
                    break;
                }
                case 1:
                    mid++;
                    break;
                case 2: {
                    temp = a[mid];
                    a[mid] = a[hi];
                    a[hi] = temp;
                    hi--;
                    break;
                }
            }
        }
    }

    /* Utility function to print array arr[] */
    static void printArray(int arr[], int arr_size)
    {
        int i;
        for (i = 0; i < arr_size; i++)
            System.out.print(arr[i] + " ");
        System.out.println("");
    }

    public static void main(String [] args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t= Integer.parseInt(br.readLine());
        while(t-->0){
            int n= Integer.parseInt(br.readLine());
            int a[]=new int[n];
            String line = br.readLine();
            String[] strs = line.trim().split("\\s+");
            for(int i=0;i<n;i++){
                a[i]=Integer.parseInt(strs[i]);
            }
            sort01(a,n);
            System.out.println("Array after seggregation ");
            System.out.println(Arrays.toString(a));
          //  printArray(a,n);
            /*SortArrayOf0s1sNd2s zo=new SortArrayOf0s1sNd2s();
            //zo.sort012(a,n);
            zo.sortColors(a);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<n;i++)
            sb.append(a[i] + " ");
             System.out.println(sb);*/
        }
        br.close();

    }
}