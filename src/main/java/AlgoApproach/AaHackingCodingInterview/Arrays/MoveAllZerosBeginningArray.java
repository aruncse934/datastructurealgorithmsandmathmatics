package AlgoApproach.AaHackingCodingInterview.Arrays;

public class MoveAllZerosBeginningArray { //input {1,2,3,0,0,0,4,5} output {0,0,0,1,2,3,4,5}

    //method 1
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) return;

        int insertPos = 0;
        for (int num: nums) {
            if (num != 0) nums[insertPos++] = num;
        }

        while (insertPos < nums.length) {
            nums[insertPos++] = 0;
        }
    }

    // method 2
    static int[] sortBasedOnZero(int[] arr) { //Move All Zeros to the Beginning of the Array:
        int k=0;
        for(int i=0;i<arr.length;i++)
        {
            if(arr[i]==0)
            {
                int temp=arr[i];
                arr[i]=arr[k];
                arr[k]=temp;
                k++;
            }
        }
        return arr;
    }

    static void pushZerosToEnd(int arr[], int n) //Move All Zeros to the Ending of the Array:
    {
        int count = 0;  // Count of non-zero elements
        for (int i = 0; i < n; i++)
            if (arr[i] != 0)
                arr[count++] = arr[i];
        while (count < n)
            arr[count++] = 0;
    }


    public static void main (String[] args)
    {
        int arr[] = {0,1,0,3,12};
        int n = arr.length;

        sortBasedOnZero(arr);
        System.out.println("Move All Zeros to the Beginning of the Array: ");
        for (int i=0; i<n; i++) {
            System.out.print(arr[i] + " ");
        }

        pushZerosToEnd(arr,n);
        System.out.println("\nArray after pushing zeros to the back: ");
        for (int i=0; i<n; i++) {
            System.out.print(arr[i] + " ");
        }

    }

}

/*Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Example:

Input: [0,1,0,3,12]
Output: [1,3,12,0,0]
Note:

You must do this in-place without making a copy of the array.
Minimize the total number of operations.
*/
