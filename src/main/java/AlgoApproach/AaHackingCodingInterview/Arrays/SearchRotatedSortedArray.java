package AlgoApproach.AaHackingCodingInterview.Arrays;

public class SearchRotatedSortedArray {
    //binary Search
    public int search(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        while (start <= end){
            int mid = (start + end) / 2;
            if (nums[mid] == target)
                return mid;

            if (nums[start] <= nums[mid]){
                if (target < nums[mid] && target >= nums[start])
                    end = mid - 1;
                else
                    start = mid + 1;
            }

            if (nums[mid] <= nums[end]){
                if (target > nums[mid] && target <= nums[end])
                    start = mid + 1;
                else
                    end = mid - 1;
            }
        }
        return -1;
    }
    static int search1(int arr[], int l, int h, int key)
    {
        if (l > h)
            return -1;

        int mid = (l+h)/2;
        if (arr[mid] == key)
            return mid;
        if (arr[l] <= arr[mid]) {

            if (key >= arr[l] && key <= arr[mid])
                return search1(arr, l, mid-1, key);
            return search1(arr, mid+1, h, key);
        }
        if (key >= arr[mid] && key <= arr[h])
            return search1(arr, mid+1, h, key);

        return search1(arr, l, mid-1, key);
    }
    public static void main(String args[])
    {
        int arr[] = {4,5,6,7,0,1,2};
        int n = arr.length;
        int key = 0;
        int i = search1(arr, 0, n-1, key);
        if (i != -1)
            System.out.println("Index: " + i);
        else
            System.out.println("Key not found");
    }

}
/*Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Your algorithm's runtime complexity must be in the order of O(log n).

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
*/