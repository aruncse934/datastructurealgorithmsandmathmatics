package AlgoApproach.AaHackingCodingInterview.Arrays;

import java.util.*;

public class MergeArrayWithOverlappingIntervals {

    //Sorting appoach
        private class IntervalComparator implements Comparator<int[]> {
            @Override
            public int compare(int[] a, int[] b) {
                return a[0] < b[0] ? -1 : a[0] == b[0] ? 0 : 1;
            }
        }

        public int[][] merge(int[][] intervals) {
            Collections.sort(Arrays.asList(intervals), new IntervalComparator());

            LinkedList<int[]> merged = new LinkedList<>();
            for (int[] interval : intervals) {
                // if the list of merged intervals is empty or if the current
                // interval does not overlap with the previous, simply append it.
                if (merged.isEmpty() || merged.getLast()[1] < interval[0]) {
                    merged.add(interval);
                }
                // otherwise, there is overlap, so we merge the current and previous
                // intervals.
                else {
                    merged.getLast()[1] = Math.max(merged.getLast()[1], interval[1]);
                }
            }

            return merged.toArray(new int[merged.size()][]);
        }



        //efficient approach
        public static void mergeIntervals(Interval arr[]) {
            // Test if the given set has at least one interval
            if (arr.length <= 0)
                return;

            // Create an empty stack of intervals
            Stack<Interval> stack=new Stack<>();

            // sort the intervals in increasing order of start time
            Arrays.sort(arr,new Comparator<Interval>(){
                public int compare(Interval i1,Interval i2)
                {
                    return i1.start-i2.start;
                }
            });

            // push the first interval to stack
            stack.push(arr[0]);

            // Start from the next interval and merge if necessary
            for (int i = 1 ; i < arr.length; i++)
            {
                // get interval from stack top
                Interval top = stack.peek();

                // if current interval is not overlapping with stack top,
                // push it to the stack
                if (top.end < arr[i].start)
                    stack.push(arr[i]);

                    // Otherwise update the ending time of top if ending of current
                    // interval is more
                else if (top.end < arr[i].end)
                {
                    top.end = arr[i].end;
                    stack.pop();
                    stack.push(top);
                }
            }

            // Print contents of stack
            System.out.print("The Merged Intervals are: ");
            while (!stack.isEmpty())
            {
                Interval t = stack.pop();
                System.out.print("["+t.start+","+t.end+"] ");
            }
        }

        public static void main(String args[]) {
            Interval arr[]=new Interval[4];//[1,3],[2,6],[8,10],[15,18]
            arr[0]=new Interval(1,3);
            arr[1]=new Interval(2,6);
            arr[2]=new Interval(8,10);
            arr[3]=new Interval(15,18);
            mergeIntervals(arr);
        }
    }

class Interval
{
    int start,end;
    Interval(int start, int end)
    {
        this.start=start;
        this.end=end;
    }
}





/*Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
NOTE: input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.
*/
