package AlgoApproach.AaHackingCodingInterview.Strings;

import java.util.LinkedHashSet;
import java.util.Arrays;

public class RemoveDuplicatesFromString {

    //recursive greedy solution
    public String removeDuplicateLetters(String s) {
        int[] cnt = new int[26];
        int pos = 0; // the position for the smallest s[i]
        for (int i = 0; i < s.length(); i++) cnt[s.charAt(i) - 'a']++;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) < s.charAt(pos)) pos = i;
            if (--cnt[s.charAt(i) - 'a'] == 0) break;
        }
        return s.length() == 0 ? "" : s.charAt(pos) + removeDuplicateLetters(s.substring(pos + 1).replaceAll("" + s.charAt(pos), ""));
    }

    //Hashing
    static void removeDuplicates(String str){
        LinkedHashSet<Character> lhs = new LinkedHashSet<>();
        for(int i=0;i<str.length();i++)
            lhs.add(str.charAt(i));
        // print string after deleting duplicate elements
        for(Character ch : lhs)
            System.out.print(ch);
    }

    static char[] removeDuplicatesFromString(String string) {
        //table to keep track of visited characters
        int[] table = new int[256];
        char[] chars = string.toCharArray();
        //to keep track of end index of resultant string
        int endIndex = 0;
        for(int i = 0; i < chars.length; i++)
        {
            if(table[chars[i]] == 0)
            {
                table[chars[i]] = -1;
                chars[endIndex++] = chars[i];
            }
        }

        return Arrays.copyOfRange(chars, 0, endIndex);
    }


    public static void main(String args[])
    {
        String str = "cbacdcbc";

        removeDuplicates(str);
        System.out.println("\n");
        System.out.println(removeDuplicatesFromString(str));
    }
}
/*Given a string which contains only lowercase letters, remove duplicate letters so that every letter appears once and only once. You must make sure your result is the smallest in lexicographical order among all possible results.

Example 1:

Input: "bcabc"
Output: "abc"
Example 2:

Input: "cbacdcbc"
Output: "acdb"*/