package AlgoApproach.AaHackingCodingInterview.Strings;

import java.util.Scanner;

//Reverse words in a sentence.
public class ReverseWordsStringII {
    public static String reverseTheSentence(String inputString){
        String[] words = inputString.split(" ");

        String outputString = "";

        for (int i = words.length-1; i >= 0; i--)
        {
            outputString = outputString + words[i] + " ";
        }

        return outputString;
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter Input String :");

        String inputString = sc.nextLine();

        String outputString = reverseTheSentence(inputString);

        System.out.println("Input String : "+inputString);

        System.out.println("Output String : "+outputString);

        sc.close();
    }
    //
    public void reverseWords(char[] s) {
        int i=0;
        for(int j=0; j<s.length; j++){
            if(s[j]==' '){
                reverse(s, i, j-1);
                i=j+1;
            }
        }

        reverse(s, i, s.length-1);

        reverse(s, 0, s.length-1);
    }

    public void reverse(char[] s, int i, int j){
        while(i<j){
            char temp = s[i];
            s[i]=s[j];
            s[j]=temp;
            i++;
            j--;
        }
    }

}
/*
Given an input character array, reverse the array word by word. A word is defined as a sequence of non-space characters.

The input character array does not contain leading or trailing spaces and the words are always separated by a single space.

Have you met this question in a real interview?
Example
Example1

Input: s = "the sky is blue"
Output: "blue is sky the"
Example2

Input: "a b c"
Output: "c b a"
*/
