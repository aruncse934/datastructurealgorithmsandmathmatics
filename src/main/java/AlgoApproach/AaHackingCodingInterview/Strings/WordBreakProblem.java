package AlgoApproach.AaHackingCodingInterview.Strings;

import java.util.List;
import java.util.Arrays;
import java.util.Set;

public class WordBreakProblem {

    //DP
    public static boolean wordBreaks(List<String> dict, String s) {
        boolean[] f = new boolean[s.length() + 1];
        f[0] = true;
        /* First DP
        for(int i = 1; i <= s.length(); i++){
            for(String str: dict){
                if(str.length() <= i){
                    if(f[i - str.length()]){
                        if(s.substring(i-str.length(), i).equals(str)){
                            f[i] = true;
                            break;
                        }
                    }
                }
            }
        }*/

        //Second DP
        for(int i=1; i <= s.length(); i++){
            for(int j=0; j < i; j++){
                if(f[j] && dict.contains(s.substring(j, i))){
                    f[i] = true;
                    break;
                }
            }
        }

        return f[s.length()];
    }

    //
    public static boolean wordBreak(List<String> dict, String str) {
        if (str.length() == 0) {
            return true;
        }
        for (int i = 1; i <= str.length(); i++) {
            String prefix = str.substring(0, i);
            if (dict.contains(prefix) && wordBreak(dict, str.substring(i))) {
                return true;
            }
        }
        return false;
    }
    // main function
    public static void main(String[] args)
    {
        // List of Strings to represent dictionary
        List<String> dict = Arrays.asList("cats", "dog", "sand", "and", "cat");/*("this", "th", "is", "famous",
                "Word", "break", "b", "r", "e", "a", "k",
                "br", "bre", "brea", "ak", "problem");*/

        // input String
        String str = "catsandog";//"Wordbreakproblem";

        if (wordBreak(dict, str)) {
            System.out.print("True");
        } else {
            System.out.print("False");
        }

    }

}
/*Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

Note:

The same word in the dictionary may be reused multiple times in the segmentation.
You may assume the dictionary does not contain duplicate words.
Example 1:

Input: s = "leetcode", wordDict = ["leet", "code"]
Output: true
Explanation: Return true because "leetcode" can be segmented as "leet code".
Example 2:

Input: s = "applepenapple", wordDict = ["apple", "pen"]
Output: true
Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
             Note that you are allowed to reuse a dictionary word.
Example 3:

Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
Output: false
*/