package AlgoApproach.AaHackingCodingInterview.LinkedList;

public class IntersectionPointTwoLists {

    //Two Pointer
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        //boundary check
        if(headA == null || headB == null) return null;

        ListNode a = headA;
        ListNode b = headB;

        //if a & b have different len, then we will stop the loop after second iteration
        while( a != b){
            //for the end of first iteration, we just reset the pointer to the head of another linkedlist
            a = a == null? headB : a.next;
            b = b == null? headA : b.next;
        }

        return a;
    }

        /*public static LinkedListNode intersect(LinkedListNode head1, LinkedListNode head2) {

            LinkedListNode list1node = null;
            int list1length = get_length(head1);
            LinkedListNode list2node = null;
            int list2length = get_length(head2);

            int length_difference = 0;
            if(list1length >= list2length) {
                length_difference = list1length - list2length;
                list1node = head1;
                list2node = head2;
            } else {
                length_difference = list2length - list1length;
                list1node = head2;
                list2node = head1;
            }

            while(length_difference > 0) {
                list1node = list1node.next;
                length_difference--;
            }

            while(list1node != null) {
                if(list1node == list2node) {
                    return list1node;
                }

                list1node = list1node.next;
                list2node = list2node.next;
            }
            return null;
        }

        private static int get_length(
                LinkedListNode head) {
            int list_length = 0;
            while(head != null) {
                head = head.next;
                list_length++;
            }
            return list_length;
        }
        public static void main(String[] args) {
            int [] a1 = {13,4};
            int [] a2 = {29, 23, 82, 11};
            LinkedListNode list_head_1 = LinkedList.createLinkedList(a1);
            LinkedListNode list_head_2 = LinkedList.createLinkedList(a2);
            LinkedListNode node1 = new LinkedListNode(12);
            LinkedListNode node2 = new LinkedListNode(27);

            LinkedList.insertAtTail(list_head_1, node1);
            LinkedList.insertAtTail(list_head_1, node2);

            LinkedList.insertAtTail(list_head_2, node1);

            System.out.print("List 1: ");
            LinkedList.display(list_head_1);
            System.out.print("List 2: ");
            LinkedList.display(list_head_2);

            LinkedListNode intersect_node = intersect(list_head_1, list_head_2);
            System.out.println(String.format("Intersect at %d", intersect_node.data));
        }
    }*/

}

/*Write a program to find the node at which the intersection of two singly linked lists begins.

For example, the following two linked lists:


begin to intersect at node c1.



Example 1:


Input: intersectVal = 8, listA = [4,1,8,4,5], listB = [5,0,1,8,4,5], skipA = 2, skipB = 3
Output: Reference of the node with value = 8
Input Explanation: The intersected node's value is 8 (note that this must not be 0 if the two lists intersect). From the head of A, it reads as [4,1,8,4,5]. From the head of B, it reads as [5,0,1,8,4,5]. There are 2 nodes before the intersected node in A; There are 3 nodes before the intersected node in B.


Example 2:


Input: intersectVal = 2, listA = [0,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
Output: Reference of the node with value = 2
Input Explanation: The intersected node's value is 2 (note that this must not be 0 if the two lists intersect). From the head of A, it reads as [0,9,1,2,4]. From the head of B, it reads as [3,2,4]. There are 3 nodes before the intersected node in A; There are 1 node before the intersected node in B.


Example 3:


Input: intersectVal = 0, listA = [2,6,4], listB = [1,5], skipA = 3, skipB = 2
Output: null
Input Explanation: From the head of A, it reads as [2,6,4]. From the head of B, it reads as [1,5]. Since the two lists do not intersect, intersectVal must be 0, while skipA and skipB can be arbitrary values.
Explanation: The two lists do not intersect, so return null.


Notes:

If the two linked lists have no intersection at all, return null.
The linked lists must retain their original structure after the function returns.
You may assume there are no cycles anywhere in the entire linked structure.
Your code should preferably run in O(n) time and use only O(1) memory.*/