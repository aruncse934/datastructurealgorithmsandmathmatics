package AlgoApproach.AaHackingCodingInterview.LinkedList;

public class FindNthNodefromEndLinkedList {
        ListNode head; // head of the list

        void printNthFromLast(int n)
        {
            ListNode main_ptr = head;
            ListNode ref_ptr = head;

            int count = 0;
            if (head != null) {
                while (count < n) {
                    if (ref_ptr == null) {
                        System.out.println(n + " is greater than the no "
                                + " of nodes in the list");
                        return;
                    }
                    ref_ptr = ref_ptr.next;
                    count++;
                }
                while (ref_ptr != null) {
                    main_ptr = main_ptr.next;
                    ref_ptr = ref_ptr.next;
                }
                System.out.println("Node no. " + n + " from last is " + main_ptr.val);
            }
        }

        public void push(int new_data) {

            ListNode new_node = new ListNode(new_data);
            new_node.next = head;
            head = new_node;
        }

        /*Drier program to test above methods */
        public static void main(String[] args)
        {
            FindNthNodefromEndLinkedList llist = new FindNthNodefromEndLinkedList();
            llist.push(20);
            llist.push(4);
            llist.push(15);
            llist.push(35);
            llist.printNthFromLast(4);
        }
}
