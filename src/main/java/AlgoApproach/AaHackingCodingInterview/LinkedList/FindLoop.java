package AlgoApproach.AaHackingCodingInterview.LinkedList;

import java.util.LinkedList;

public class FindLoop {
    static  class LinkedList{
        int value;
        LinkedList next = null;
    }
    public static LinkedList findLoop(LinkedList head){ //Linked List Cycle II
        LinkedList slow = head;
        LinkedList fast = head;

        while (fast!=null && fast.next!=null){
            fast = fast.next.next;
            slow = slow.next;

            if (fast == slow){
                LinkedList slow2 = head;
                while (slow2 != slow){
                    slow = slow.next;
                    slow2 = slow2.next;
                }
                return slow;
            }
        }
        return null;
    }
}/* public static LinkedList findLoop(LinkedList head) {
        LinkedList first = head.next;
        LinkedList second = head.next.next;

        while (first != second) {
            first = first.next;
            second = second.next.next;
        }

        first = head;

        while (first != second) {
            first = first.next;
            second = second.next;
        }

        return first;
    }

    static class LinkedList {
        int value;
        LinkedList next = null;

        public LinkedList(int value) {
            this.value = value;
        }
    }*/
