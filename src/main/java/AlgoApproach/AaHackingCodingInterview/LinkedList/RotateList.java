package AlgoApproach.AaHackingCodingInterview.LinkedList;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RotateList {
    static class LinkedList{
        int value;
        LinkedList next;

        public LinkedList(int value, LinkedList next) {
            this.value = value;
            this.next = next;
        }


        public LinkedList(int i) {
            this.value=i;
        }
    }

    // O(n) time | O(1) space - where n is the number of nodes in the Linked List
    public static LinkedList shiftLinkedList(LinkedList head, int k){
        int listLength = 1;
        LinkedList listTail = head;
        while (listTail.next != null){
            listTail = listTail.next;
            listLength++;
        }
        int offset = Math.abs(k) % listLength;
        if(offset == 0)
            return head;
        int newTrailPosition = k > 0 ? listLength - offset : offset;
        LinkedList newTrail = head;
        for(int i = 1; i < newTrailPosition; i++ ){
            newTrail = newTrail.next;
        }
        LinkedList newHead = newTrail.next;
        newTrail.next = null;
        listTail.next = head;
        return newHead;
    }

   /* public static void main(String[] args) {


    }*/
}

class ProgramTest {
    public List<Integer> linkedListToArray(RotateList.LinkedList head) {
        var array = new ArrayList<Integer>();
        var current = head;
        while (current != null) {
            array.add(current.value);
            current = current.next;
        }
        return array;
    }

    @Test
    public void TestCase1() {
        var head = new RotateList.LinkedList(0);
        head.next = new RotateList.LinkedList(1);
        head.next.next = new RotateList.LinkedList(2);
        head.next.next.next = new RotateList.LinkedList(3);
        head.next.next.next.next = new RotateList.LinkedList(4);
        head.next.next.next.next.next = new RotateList.LinkedList(5);
        var result = RotateList.shiftLinkedList(head, 2);
        var array = this.linkedListToArray(result);

        var expected = Arrays.asList(new Integer[] {4, 5, 0, 1, 2, 3});
        assertTrue(expected.equals(array));
    }
}

