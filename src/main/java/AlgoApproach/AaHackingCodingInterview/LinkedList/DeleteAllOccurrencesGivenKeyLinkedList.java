package AlgoApproach.AaHackingCodingInterview.LinkedList;

public class DeleteAllOccurrencesGivenKeyLinkedList {//Remove Linked List Elements
    static ListNode head;
    //Recursive method
   /* public ListNode removeElements(ListNode head, int val) {
        if (head == null) return null;
        head.next = removeElements(head.next, val);
        return head.val == val ? head.next : head;
    }
*/
    public ListNode removeElements(ListNode head, int val) {
        ListNode fakeHead = new ListNode(-1);
        fakeHead.next = head;
        ListNode curr = head, prev = fakeHead;
        while (curr != null) {
            if (curr.val == val) {
                prev.next = curr.next;
            } else {
                prev = prev.next;
            }
            curr = curr.next;
        }
        return fakeHead.next;
    }

    public void push(int new_data)
    {
        ListNode new_node = new ListNode(new_data);
        new_node.next = head;
        head = new_node;
    }

    /* This function prints contents of linked list
    starting from the given node */
    public void printList()
    {
        ListNode tnode = head;
        while (tnode != null)
        {
            System.out.print(tnode.val + " ");
            tnode = tnode.next;
        }
    }

    // Driver Code
    public static void main(String[] args)
    {
        DeleteAllOccurrencesGivenKeyLinkedList llist = new DeleteAllOccurrencesGivenKeyLinkedList();

        llist.push(7);
        llist.push(2);
        llist.push(3);
        llist.push(2);
        llist.push(8);
        llist.push(1);
        llist.push(2);
        llist.push(2);

        int key = 2; // key to delete

        System.out.println("Created Linked list is:");
        llist.printList();

        llist.removeElements(head, key);

        System.out.println("\nLinked List after Deletion is:");
        llist.printList();
    }
}
/*Remove all elements from a linked list of integers that have value val.

Example:

Input:  1->2->6->3->4->5->6, val = 6
Output: 1->2->3->4->5

*/