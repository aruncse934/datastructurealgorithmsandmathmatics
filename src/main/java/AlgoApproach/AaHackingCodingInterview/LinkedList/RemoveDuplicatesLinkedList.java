package AlgoApproach.AaHackingCodingInterview.LinkedList;

public class RemoveDuplicatesLinkedList {

    //Straight-Forward Approach
    public static ListNode deleteDuplicates(ListNode head) {
        ListNode current = head;
        while (current != null && current.next != null) {
            if (current.next.val == current.val) {
                current.next = current.next.next;
            } else {
                current = current.next;
            }
        }
        return head;
    }

    /*//recursive Approach
    public ListNode deleteDuplicates(ListNode head) {
        if(head == null || head.next == null)return head;
        head.next = deleteDuplicates(head.next);
        return head.val == head.next.val ? head.next : head;
    }*/


    static ListNode push(ListNode head_ref,int new_data){
        /* allocate node */
        ListNode new_node = new ListNode();

        /* put in the data */
        new_node.val = new_data;

        /* link the old list off the new node */
        new_node.next = (head_ref);

        /* move the head to point to the new node */
        (head_ref) = new_node;
        return head_ref;
    }
    static void printList(ListNode node)
    {
        while (node != null)
        {
            System.out.print(" " + node.val);
            node = node.next;
        }
    }

    /* Driver code*/
    public static void main(String args[])
    {
        /* Start with the empty list */
        ListNode head = null;

    /* Let us create a sorted linked list
    to test the functions
    Created linked list will be 11.11.11.13.13.20 */
        head = push(head, 20);
        head = push(head, 13);
        head = push(head, 13);
        head = push(head, 11);
        head = push(head, 11);
        head = push(head, 11);

        System.out.println("Linked list before" +
                " duplicate removal ");
        printList(head);

        /* Remove duplicates from linked list */
        head = deleteDuplicates(head);

        System.out.println("\nLinked list after" +
                " duplicate removal ");
        printList(head);
    }
}

/*Given a sorted linked list, delete all duplicates such that each element appear only once.

Example 1:

Input: 1->1->2
Output: 1->2
Example 2:

Input: 1->1->2->3->3
Output: 1->2->3*/
