package AlgoApproach.Concurrency;
import java.io.*;
import java.util.*;
import java.util.function.IntConsumer;
import java.lang.*;
import java.util.concurrent.Semaphore;
import java.util.function.IntConsumer;
/*Input:
printNumber(1)
printNumber(2)
printCharacter('A')

Output:
When n = 1, your code should output:

12A
When n = 2, your code should output:

12A34B
*/
class Solution{ //Two threads interleavly print numbers and letters

    private int n;

    public Solution(int n) {
        this.n = n;
        // write your code

    }


    public void number(IntConsumer printNumber) throws Exception {
        // write your code

    }

    public void character(IntConsumer printCharacter) throws Exception {
        // write your code

    }
}

public class TwoThreadsNumbersLetters {
    private static String mainThreadName;
    private static PrintStream ps;
    public static void main(String[] args) {
        try {
            String inputPath = args[0];
            String outputPath = args[1];
            Scanner in = new Scanner(new FileReader(inputPath));
            ps = new PrintStream(outputPath);
            mainThreadName = Thread.currentThread().getName();
            Solution solution = new Solution(Integer.parseInt(in.nextLine()));
            IntConsumer printNumber = (int x) -> {
                try {
                    if (mainThreadName == Thread.currentThread().getName() || x < 0) {
                        throw new Exception("You should print numbers in a sub thread.");
                    }
                    ps.write(String.valueOf(x).getBytes("Utf-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            };
            IntConsumer printCharacter = (int x) -> {
                try {
                    if (mainThreadName == Thread.currentThread().getName() || x < 0 || x > 25) {
                        throw new Exception("You should print numbers in a sub thread.");
                    }
                    ps.write(String.valueOf((char)('A'+x)).getBytes("Utf-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            };
            Thread thread = new Thread(() -> {
                try {
                    solution.number(printNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            Thread thread1 = new Thread(() -> {
                try {
                    solution.character(printCharacter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            thread1.start();
            thread.join();
            thread1.join();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}