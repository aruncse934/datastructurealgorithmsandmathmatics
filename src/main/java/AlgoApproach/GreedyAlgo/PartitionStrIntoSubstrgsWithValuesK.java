package AlgoApproach.GreedyAlgo;

public class PartitionStrIntoSubstrgsWithValuesK {

    /*public int minimumPartition(String s, int k) {
        int count = 1;
        long val = 0;
        for (int i = 0; i < s.length(); i++) {
            int digit = s.charAt(i) - '0';
            val = val * 10 + digit;
            if (val > k) {
                count++;
                val = digit;
            }
            if (val > k) {
                return -1;
            }
        }

        return count;
    }
}*/
    public int minimumPartition(String s, int k) {
        long num = 0;
        int res = 1;
        for(int i = 0; i < s.length(); i++){
            num = num * 10 + s.charAt(i) - '0'; //keep adding digit to num
            if(num > k){ //new partition
                res++;
                num = s.charAt(i) - '0'; //reset num to current digit
            }
            if(num > k) //single digit > k? impossible to partition
                return -1;
        }
        return res;
    }
}