package AlgoApproach.TwoPointerAlgo;

/*Example 1:

Input: [1,2,3,4,5]
Output: Node 3 from this list (Serialization: [3,4,5])
The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).
Note that we returned a ListNode object ans, such that:
ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
Example 2:

Input: [1,2,3,4,5,6]
Output: Node 4 from this list (Serialization: [4,5,6])
Since the list has two middle nodes with values 3 and 4, we return the second one.*/
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class MiddleLinkedList {
    public static void main(String[] args) {
        MiddleLinkedList linkedList = new MiddleLinkedList();
       /* LinkedList.ListNode head = linkedList.head();
        linkedList.add( new LinkedList.ListNode("1"));
        linkedList.add( new LinkedList.ListNode("2"));
        linkedList.add( new LinkedList.ListNode("3"));
        linkedList.add( new LinkedList.ListNode("4"));*/


    }

    //Fast and Slow Pointer (Two Pointers)
    public ListNode middleNode(ListNode head) {
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
}
