package AlgoApproach.TwoPointerAlgo;

import java.util.Arrays;

/*Example 1:

Input: numbers = [2,7,11,15], target = 9
Output: [1,2]
Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
Example 2:

Input: numbers = [2,3,4], target = 6
Output: [1,3]
Example 3:

Input: numbers = [-1,0], target = -1
Output: [1,2]*/
public class TwoSumSorted {
    public static void main(String[] args) {
      int arr[] = {2,7,11,15};
      int target = 9;
        System.out.println(Arrays.toString(twoSum(arr,target)));
    }
    public static int[] twoSum(int[] num, int target) {
        int[] indice = new int[2];
        if (num == null || num.length < 2)
            return indice;
        int left = 0, right = num.length - 1;
        while (left < right) {
            int v = num[left] + num[right];
            if (v == target) {
                indice[0] = left + 1;
                indice[1] = right + 1;
                break;
            } else if (v > target) {
                right --;
            } else {
                left ++;
            }
        }
        return indice;
    }
   /* public int[] twoSums(int[] numbers, int target) {
        int n = numbers.length,i=0,j=n-1;
        while(i<j){
            int sum = numbers[i] + numbers[j];
            if(sum == target) return new int[]{i+1,j+1};
            if(sum > target) j--;
            else i++;
        }
        return null;
    }*/
}
