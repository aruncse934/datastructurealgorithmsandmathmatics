package AlgoApproach.TwoPointerAlgo;


/*Input:  x = 2, y = 3, p = 5
Output: 3
Explanation: 2^3 % 5 = 8 % 5 = 3.

Input:  x = 2, y = 5, p = 13
Output: 6
Explanation: 2^5 % 13 = 32 % 13 = 6.*/
/*Example 1
Input: n = 2, k = 3, m = 10

Output: 6

Explanation: 2^(2^3) % 10 == 2^8 % 10 == 256 % 10 == 6.

Example 2
Input: n = 2, k = 34, m = 21

Output: 16

*/
public class ModularExponentiation {
    static int power(int x, int y, int p)
    {
        int res = 1; // Initialize result

        x = x % p; // Update x if it is more than or
        // equal to p

        if (x == 0)
            return 0; // In case x is divisible by p;

        while (y > 0)
        {

            // If y is odd, multiply x with result
            if ((y & 1) != 0)
                res = (res * x) % p;

            // y must be even now
            y = y >> 1; // y = y/2
            x = (x * x) % p;
        }
        return res;
    }

    // Driver Code
    public static void main(String[] args)
    {
        int x = 2;
        int y = 34;
        int p = 10;
        System.out.print("Power is " + power(x, y, p));
    }
}
