package AlgoApproach.TwoPointerAlgo;

/*Example 1:

Input: s = "aDb", t = "adb"
Output: true
Example 2:

Input: s = "ab", t = "ab"
Output: false
Explanation:
s=t ,so they aren't one edit distance apart
*/
public class OneEditDistance {
    public static void main(String[] args) {

        String s = "ab";
        String t = "ab";
        System.out.println(isOneEditDistance(s,t));
    }

    public static boolean isOneEditDistance(String s, String t) {
        if(s == null || t == null){
            return s == t;
        }
        if(s.equals(t)){
            return false;
        }

        int n = s.length();
        int m = t.length();
        if(Math.abs(n-m)>1){
            return false;
        }
        int index = 0;
        int len = Math.min(m, n);
        for(int i = 0; i < len; i++){
            index = i + 1;
            if(s.charAt(i) != t.charAt(i)){
                return s.substring(index).equals(t.substring(index))
                        || s.substring(index).equals(t.substring(index-1))
                        || s.substring(index-1).equals(t.substring(index));
            }
        }

        return true;
    }
}
