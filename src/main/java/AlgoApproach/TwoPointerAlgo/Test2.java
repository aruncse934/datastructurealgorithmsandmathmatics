package AlgoApproach.TwoPointerAlgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/*
input:-
2
31 32
7 7
output:-
63
7

*/
public class Test2 {

        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter wr = new PrintWriter(System.out);
            int T = Integer.parseInt(br.readLine().trim());
            for(int t_i = 0; t_i < T; t_i++)
            {
                String[] str = br.readLine().split(" ");
                int l = Integer.parseInt(str[0]);
                int r = Integer.parseInt(str[1]);

                long out_ = solve(l, r);
                System.out.println(out_);

            }

            wr.close();
            br.close();
        }
        static long solve(int l, int r){
            int sum =0;
            Map<Integer,Boolean> map = new HashMap<>();
            for(int i = l; i<=r; i++ ){
                if(map.containsKey(i) && map.get(i) == true){
                    sum = sum+i;
                }else if( !map.containsKey(i) && isBeautNum(i,map)){
                    sum = sum+i;
                }
            }

            return sum;
        }

        static Boolean isBeautNum(int i, Map<Integer,Boolean> map){

            int curr = i;
           /* if(i == 0){
                return false;
            }
         */
            if(map.containsKey(i)){
                return map.get(i);
            }
            map.put(i, false);

            int temp = 0;
            while(i>0){
                int mod = i%10;
                temp = temp+(mod*mod);
                i = (i-mod)/10;
            }if(temp == 1){
                map.put(curr, true);
                return true;
            }

            Boolean b = isBeautNum(temp,map);
            map.put(curr, b);
            return b;
        }

}
