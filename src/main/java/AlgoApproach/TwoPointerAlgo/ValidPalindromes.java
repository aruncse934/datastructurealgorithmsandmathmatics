package AlgoApproach.TwoPointerAlgo;

public class ValidPalindromes {

    public static void main(String[] args) {
        String s = "A man, a plan, a canal: Panama";
        String s1 = "12121";
        boolean res = validPalindrome(s);
       System.out.println(res);

        System.out.println(isValidPalindrome(s1));
    }

    //Two Pointer
    public static boolean validPalindrome(String s){
        char[] ch =s.toCharArray();
        int i =0;
        int j = ch.length-1;
        //for(int i = 0,j= ch.length-1;i<j; ){
        while (i<j){
            if(!Character.isLetterOrDigit(ch[i]))
                i++;
            else if(!Character.isLetterOrDigit(ch[j]))
                j--;
            else if(Character.toLowerCase(ch[i++]) != Character.toLowerCase(ch[j--]))
                return false;
        }
        return true;
    }

    //Java Method using
    public static boolean isValidPalindrome(String s){
        String actual = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();
        String rev = new StringBuffer(actual).reverse().toString();
        return actual.equals(rev);
    }

}

/*   public boolean isPalindrome(String s) {
        if (s.isEmpty()) {
        	return true;
        }
        int head = 0, tail = s.length() - 1;
        char cHead, cTail;
        while(head <= tail) {
        	cHead = s.charAt(head);
        	cTail = s.charAt(tail);
        	if (!Character.isLetterOrDigit(cHead)) {
        		head++;
        	} else if(!Character.isLetterOrDigit(cTail)) {
        		tail--;
        	} else {
        		if (Character.toLowerCase(cHead) != Character.toLowerCase(cTail)) {
        			return false;
        		}
        		head++;
        		tail--;
        	}
        }

        return true;
    }*/