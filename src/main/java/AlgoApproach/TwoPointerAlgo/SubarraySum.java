package AlgoApproach.TwoPointerAlgo;

/*Given an array of integers and an integer target, find a subarray that sums to target and return the start and end indices of the subarray. It's guaranteed to have a unique solution.

Input:

1 -20 -3 30 5 4
7
Output: 1 4

Explanation: -20 - 3 + 30 = 7. The indices for subarray [-20,-3,30] is 1 and 4 (right exclusive).

*/
public class SubarraySum {
}
