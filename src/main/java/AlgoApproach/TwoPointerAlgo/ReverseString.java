package AlgoApproach.TwoPointerAlgo;


/*Example 1:

Input: s = ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
Example 2:

Input: s = ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]*/

/*Example 1：

Input："hello"
Output："olleh"
Example 2：

Input："hello world"
Output："dlrow olleh"
*/
public class ReverseString {

    public String reverseStrings(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        char[] chars = new char[s.length()];
        int left = 0;
        int right = chars.length - 1;
        while (left <= right) {
            chars[left] = s.charAt(right);
            chars[right] = s.charAt(left);
            left++;
            right--;
        }
        return new String(chars);
    }
    // two Pointer
    public void reverseString(char[] s) {
        int left = 0, right = s.length - 1;
        while (left < right) {
            char tmp = s[left];
            s[left++] = s[right];
            s[right--] = tmp;
        }
    }
}
