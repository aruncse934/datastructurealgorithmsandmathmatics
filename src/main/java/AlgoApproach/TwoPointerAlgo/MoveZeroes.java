package AlgoApproach.TwoPointerAlgo;

/*Example 1:

Input: nums = [0,1,0,3,12]
Output: [1,3,12,0,0]
Example 2:

Input: nums = [0]
Output: [0]*/
public class MoveZeroes {
    public static void main(String[] args) {
        int arr[]={0,1,0,3,12};
        int n = args.length;;
       // moveZeroes(arr);
        pushZerosToEnd(arr,n);
        for(int i =0; i< args.length;i++) {
            System.out.print(arr[i]+ " ");
        }
    }
    static void pushZerosToEnd(int arr[], int n)
    {
        int count = 0;  // Count of non-zero elements

        // Traverse the array. If element encountered is
        // non-zero, then replace the element at index 'count'
        // with this element
        for (int i = 0; i < n; i++)
            if (arr[i] != 0)
                arr[count++] = arr[i]; // here count is
        // incremented

        // Now all non-zero elements have been shifted to
        // front and 'count' is set as index of first 0.
        // Make all elements 0 from count to end.
        while (count < n)
            arr[count++] = 0;
    }
    // Shift non-zero values as far forward as possible
// Fill remaining space with zeros
    public static void moveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) return;

        int insertPos = 0;
        for (int num: nums) {
            if (num != 0) nums[insertPos++] = num;
        }

        while (insertPos < nums.length) {
            nums[insertPos++] = 0;
        }

    }
}
/*public void moveZeroes(int[] nums){
    		int index=0;
    		for (int i=0;i<nums.length;i++){
    			if (nums[i]!=0) nums[index++]=nums[i];
    		}
    		while(index<nums.length){
    			nums[index++]=0;
    		}
    	}
    */