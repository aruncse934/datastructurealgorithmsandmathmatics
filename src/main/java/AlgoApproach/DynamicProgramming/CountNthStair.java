package AlgoApproach.DynamicProgramming;

public class CountNthStair {
    public static void main(String[] args) {
        int n =10;

        System.out.println(nthStair( n ));
        System.out.println(nthStairs( n ));


    }
    public static int nthStair(int n) {
        if(n == 1){
            return 1;
        }
        int stair[] = new int[n+1];

        stair[1] = 1;
        stair[2] = 2;

        for(int i = 3; i<= n;i++){
            stair[i] = stair[i - 1] + stair[i-2];
        }
        return stair[n];
    }
    public static int nthStairs(int n) {
        double sqrt5 = Math.sqrt( 5 );
        double fibn = Math.pow( (1 + sqrt5) / 2, n + 1 ) - Math.pow( (1 - sqrt5) / 2, n + 1 );
        return (int) (fibn / sqrt5);
    }

}
