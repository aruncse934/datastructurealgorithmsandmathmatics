package AlgoApproach.DynamicProgramming;

//Largest Sum Contiguous Subarray
public class KadaneAlgorithms { //Maximum Subarray

    public static void main(String[] args) {
        int arr[] = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(MaxSumContinuousSubArray(arr));

        System.out.println(" ");
        System.out.println(maxSubArray(arr));
    }

    //Kadane Algorithms TC O(n)
    public static int MaxSumContinuousSubArray(int arr[]){
        int n = arr.length;
        int max_so_far = Integer.MIN_VALUE;
        int max_end_here = 0;
        for(int i = 0;i<n;i++){
            max_end_here = max_end_here + arr[i];
            if(max_so_far < max_end_here)
                max_so_far = max_end_here;
            if(max_end_here < 0)
                max_end_here = 0;
        }
        return max_so_far;
    }
    public static int maxSubArray(int[] A) {
        int n = A.length;
        int[] dp = new int[n];//dp[i] means the maximum subarray ending with A[i];
        dp[0] = A[0];
        int max = dp[0];
        for(int i = 1; i < n; i++){
            dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
            max = Math.max(max, dp[i]);
        }
        return max;
    }
}

/*Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
*/