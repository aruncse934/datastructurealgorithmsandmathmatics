package AlgoApproach.DynamicProgramming;

public class FibonacciSeries {
    public static void main(String[] args) {
        int n = 1000;
        System.out.println(fib(n));

       /* long getFib[] = generateFibonaccis( n );
        for (int i =0;i< n;i++){
            System.out.print(getFib[i]+" ");
        }*/
    }
    public static long fib(int n){
        long lasttwo[] = {0,1};
        long counter = 3;
        while (counter <= n){
            long nextFib = lasttwo[0]+lasttwo[1];
            lasttwo[0] =lasttwo[1];
            lasttwo[1]= nextFib;
            counter++;
        }
        return n>1 ? lasttwo[1]:lasttwo[0];

    }

    public static long[] generateFibonaccis(int n) {
        long[] fib = new long[n];
        fib[0] = 0;
        fib[1] = 1;
        for (int i = 2; i < n; ++i) {
            fib[i] = fib[i - 2] + fib[i - 1];
        }
        return fib;
    }
}
