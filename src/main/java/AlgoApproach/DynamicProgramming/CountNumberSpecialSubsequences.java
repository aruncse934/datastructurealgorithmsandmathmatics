package AlgoApproach.DynamicProgramming;

public class CountNumberSpecialSubsequences {
    public static int countSpecialSubsequences(int[] nums) {
        int n = nums.length;
        int arr[] = new int[n];
        for(int num : nums)
            arr[num] =((arr[num] + arr[num]) % 1000000007 + (num > 0 ? arr[num-1] : 1) ) % 1000000007;
        return arr[2];
    }

    public static void main(String[] args) {
        int arr[] = {0,1,2,2};
        System.out.println(countSpecialSubsequences(arr));
    }
}
