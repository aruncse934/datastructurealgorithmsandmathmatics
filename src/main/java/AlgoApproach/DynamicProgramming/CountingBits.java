package AlgoApproach.DynamicProgramming;

import java.util.Arrays;

public class CountingBits {

    public int[] countsBits(int n){
        int[] ans = new int[n+1];
        if(n == 0) return ans;

        for(int i = 0; i <=  n; i++){
            ans[i] = ans[i/2] + i%2;
        }
        return ans;
    }

    public static void main(String[] args) {

        int n = 5;
        System.out.println(Arrays.toString(new CountingBits().countsBits(n)));
    }
}
