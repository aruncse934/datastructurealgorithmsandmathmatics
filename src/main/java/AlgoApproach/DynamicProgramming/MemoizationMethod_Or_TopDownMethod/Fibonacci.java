package AlgoApproach.DynamicProgramming.MemoizationMethod_Or_TopDownMethod;

//Nth term of Fibonacci Series;
public class Fibonacci {

    //recursive method
    public static long fib(int n){
        if(n  <= 1)
            return n;
        return fib(n-1)+fib(n-2);
    }

    /*Fibonacci Series using memoized Recursion */
    static  int fib1(int n1){
        int []term = new int [1000];
        if(n1 <= 1)
            return n1;
        if(term[n1] != 0)
            return term[n1];
        else {
            term[n1] = fib1(n1 - 1)+fib1(n1- 2);
            return term[n1];
        }
    }
    public static void main(String[] args) {
        int n1 =10;
        int n= 10;
        System.out.println(fib(n));
        System.out.println(fib1(n1));
    }
}
