package MultithreadingConcurrency.ThreadPoolNdExecutors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//CachedThreadPool
public class MultipleTasksExecutorExample {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.execute(new CountDownClock("A"));
        pool.execute(new CountDownClock("B"));
        pool.execute(new CountDownClock("C"));
        pool.execute(new CountDownClock("D"));
    }
}