package Stack;

//Implementing Stack using Arrays
/*Java Program to implement basic stack operations */
public class StackOperations {
    static final int MAX=1000;
    int top;
    int a[] = new int[MAX]; //Maximum size of Stack

    boolean isEmpty(){
        return (top < 0);
    }
    StackOperations(){
        top = -1;
    }
    //push operation
    boolean push(int x){
       if(top >= (MAX - 1)){
           System.out.println("Stack Overflow");
           return false;
       }else {
            a[++top] = x;
           System.out.println(x+ " pushed into stack ");
           return true;
       }
    }
    //pop operation
    int pop(){
        if(top < 0){
            System.out.println("Stack underFlow");
            return 0;
        }else {
            int x = a[top--];
            return x;
        }
    }
    //peek operation
    int peek(){
        if(top < 0){
            System.out.println("Stack Underflow");
            return 0;
        }else {
            int x = a[top];
            return x;
        }
    }
}
//Driver code
class Main{
    public static void main(String[] args) {
        StackOperations s = new StackOperations();
        s.push(10);
        s.push(20);
        s.push(30);
        System.out.println(s.pop() + " Popped from stack ");
    }
}

