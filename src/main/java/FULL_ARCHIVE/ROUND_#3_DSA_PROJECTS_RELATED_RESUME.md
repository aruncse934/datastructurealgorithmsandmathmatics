
How to apply SOLID principle and what is dependency injection and why to use it and scenarios on where to use
----------------------
SOLID principles ensure maintainable and scalable code. Dependency Injection helps in achieving loose coupling and testability.
->SOLID principles are Single Responsibility, Open-Closed, Liskov Substitution, Interface Segregation, and Dependency Inversion
->Dependency Injection is a design pattern that allows objects to be loosely coupled and easily testable
->Use Dependency Injection to reduce tight coupling between classes and make them more modular
->Use SOLID principles to ensure maintainable and scalable code
->Example: Use Dependency Injection to inject a database connection into a repository class instead of hardcoding it
->Example: Use SOLID principles to separate concerns and make classes more cohesive
->Example: Use Interface Segregation to break down large interfaces into smaller ones that are more focused on specific tasks

What is dependency injection?
--------------------------
Dependency injection is a design pattern used to remove hard-coded dependencies and make code more flexible and testable.

Dependency injection involves injecting dependencies into a class rather than having the class create them itself.

This allows for easier testing and swapping out of dependencies.

There are three types of dependency injection: constructor injection, setter injection, and interface injection.

Example: Instead of creating a database connection within a class, the connection is injected into the class through a constructor or setter method.

Dependency injection frameworks like Spring and Guice can automate the process of injecting dependencies.

Spring IoC (Inversion of Control) vs Spring Dependency Injection
-----------------------------------------------------
Inversion of Control:
->Spring IoC Container is the core of Spring Framework. It creates the objects, configures and assembles their dependencies, manages their entire life cycle. ->	
->Spring helps in creating objects, managing objects, configurations, etc. because of IoC (Inversion of Control). 
->Spring IoC is achieved through Dependency Injection.	
->IoC is a design principle where the control flow of the program is inverted.	
->Aspect-Oriented Programming is one way to implement Inversion of Control.

Dependency Injection:
->Spring Dependency injection is a way to inject the dependency of a framework component by the following ways of spring: Constructor Injection and Setter Injection
->Spring framework helps in the creation of loosely-coupled applications because of Dependency Injection.
->Dependency Injection is the method of providing the dependencies and Inversion of Control is the end result of Dependency Injection.
->Dependency Injection is one of the subtypes of the IOC principle.  
->In case of any changes in business requirements, no code change is required

How To Create an Immutable Class in Java
-----------------------

To make a class immutable, its state should not be modifiable after creation.
->Make all fields private and final
->Do not provide any setters
->If mutable objects are used, return copies instead of references
->Ensure that the class cannot be extended
->Consider making the constructor private and providing a factory method
or
->Declare the class as final so it can’t be extended.
->Make all of the fields private so that direct access is not allowed.
->Don’t provide setter methods for variables.
->Make all mutable fields final so that a field’s value can be assigned only once.
->Initialize all fields using a constructor method performing deep copy.
->Perform cloning of objects in the getter methods to return a copy rather than returning the actual object reference.


Abstract classes can have implementation while interfaces cannot.
->Abstract classes can have constructors while interfaces cannot.
->A class can implement multiple interfaces but can only inherit from one abstract class.
->Abstract classes can have non-abstract methods while interfaces can only have abstract methods.
->Interfaces are used for full abstraction while abstract classes are used for partial abstraction.
->Example of abstract class: public abstract class Animal { public void eat() { System.out.println('Eating...'); } }
->Example of interface: public interface Animal { void eat(); }

