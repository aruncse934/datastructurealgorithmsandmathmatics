package FULL_ARCHIVE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SplitStrEveryNChars {


    //split method
    public static List<String> usingSplitMethod(String text, int n){
        String[] result  = text.split("(?<=\\G.{" +n+ "})");
        return Arrays.asList(result);
    }

    //substring method
    public static List<String> usingSubstringMethod(String text, int n){
        List<String> result = new ArrayList<>();
        int length = text.length();

        for(int i = 0; i < length; i += n){
            result.add(text.substring(i,Math.min(length, i+n)));
        }
        return result;
    }

    //Pattern Class method
    public static List<String> usingPattern(String text, int n){
        return Pattern.compile(".{1," +n+"}").matcher(text).results().map(MatchResult::group).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        String text = "abcdefgh123456";

        System.out.println(usingSplitMethod(text,4));

        for(String s : usingSubstringMethod(text, 4)) {
            System.out.println(s);
        }

        System.out.println(usingPattern(text,4));

    }
}
