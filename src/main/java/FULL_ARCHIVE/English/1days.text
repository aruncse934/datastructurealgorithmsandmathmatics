The Congress government in Himachal Pradesh managed to survive the immediate threat to its existence on Wednesday, passing the Budget for 2024-25 in the State Assembly after suspending 15 BJP lawmakers, including Leader of the Opposition Jairam Thakur.

The Budget Session was adjourned sine die (indefinitely) soon afterwards, a day before it was scheduled to conclude. Speaker Kuldeep Pathania reserved his order on a disqualification motion against six Congress MLAs who were absent for the vote passing the Budget.

In another twist to the current crisis, Minister Vikramditya Singh announced — and then took back — his resignation, bringing the leadership tussle in the party to the fore. Mr. Singh is the son of the former Chief Minister, the late Virbhadra Singh, and Himachal Pradesh Congress Committee chief Pratibha Singh. He withdrew his resignation within hours, after the party sent out mixed signals.

‘Weighing all options’

Chief Minister Sukhvinder Sukhu sounded conciliatory, making a “Vikramaditya is like my younger brother” reference in Shimla; in Delhi, however, the party only said that it was weighing all options.

Congress communications chief Jairam Ramesh asserted that the party would not hesitate to take tough actions to save its government and protect the mandate of the people of Himachal Pradesh.

The State was pushed into political turmoil on Tuesday, when Abhishek Singhvi, the ruling Congress party’s nominee for the State’s sole Rajya Sabha seat lost the election to the Bharatiya Janata Party’s Harsh Mahajan after six Congress MLAs voted in favour of the BJP, threatening the survival of their own government.

In damage control mode, Congress president Mallikarjun Kharge deputed Bhupesh Baghel, Bhupinder Singh Hooda, and D.K. Shivakumar as the All India Congress Committee’s (AICC) observers, asking them to submit a report after talking to all the party MLAs and finding out their grievances.

Mr. Kharge is also said to have spoken to former party chief Rahul Gandhi and party general secretary Priyanka Gandhi Vadra on the Himachal Pradesh developments.

The Chief Minister told presspersons in Shimla that the Budget had been passed in the Assembly, insisting that the government would complete its five years as the conspiracy to topple it has been foiled. Mr. Sukhu dismissed reports that he had quit the Chief Minister’s post. He added that a disqualification motion had been brought against the six Congress MLAs who had cross-voted in favour of the BJP in the Rajya Sabha polls, by being absent during the cut motion during the passage of the Budget in Assembly.

As the Assembly witnessed chaotic scenes, the Speaker, Mr. Pathania, suspended 15 of the 25 BJP MLAs amid an adjournment. The remaining 10 BJP MLAs staged a walkout and in the absence of any Opposition, the Budget was passed with a voice vote. The government would have technically lost the confidence of the House if it had failed to pass the Budget.

“They had to pass the Budget somehow, otherwise the government would have fallen. For this, they had only one way to reduce the number of BJP MLAs, and today 15 MLAs were suspended without any reason,” Mr. Thakur alleged.

Addressing a press conference at the AICC headquarters in Delhi, however, Mr. Ramesh accused the BJP of attempting “Operation Lotus” in the hill State to dislodge the Congress government. “The Congress will not hesitate to take some tough steps as the party is our priority

He also said that accountability would be fixed for the cross-voting in the Rajya Sabha poll that ensured the loss of the party’s nominee.

Party general secretary Priyanka Gandhi Vadra, who was one of the main campaigners in the December 2022 Assembly polls, alleged that the BJP, which had failed to stand with the people of the State when hit by a natural disaster, now wants to push it into a “political disaster”.

