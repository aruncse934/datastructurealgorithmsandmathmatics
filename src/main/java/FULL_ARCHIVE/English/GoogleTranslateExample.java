package FULL_ARCHIVE.English;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.util.List;
import java.util.stream.Collectors;

public class GoogleTranslateExample {

    public static void main(String[] args) {
        // Your Google Cloud Platform project ID
        String projectId = "your-project-id";

        // Initialize the Translate service
        Translate translate = TranslateOptions.newBuilder().setProjectId(projectId).build().getService();

        // Text to translate
        List<String> textsToTranslate = List.of("Hello", "How are you?", "Goodbye");

        // Source language code (optional, if not provided, will be detected automatically)
        String sourceLanguage = "en";

        // Target language code
        String targetLanguage = "fr"; // Translate to French in this example

        // Translate the texts using Java stream
        List<String> translatedTexts = textsToTranslate.stream()
                .map(text -> translateText(translate, text, sourceLanguage, targetLanguage))
                .collect(Collectors.toList());

        // Print the translations
        translatedTexts.forEach(System.out::println);
    }

    private static String translateText(Translate translate, String text, String sourceLanguage, String targetLanguage) {
        Translation translation = translate.translate(text,
                Translate.TranslateOption.sourceLanguage(sourceLanguage),
                Translate.TranslateOption.targetLanguage(targetLanguage));

        return translation.getTranslatedText();
    }
}
