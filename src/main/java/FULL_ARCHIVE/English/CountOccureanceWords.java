package FULL_ARCHIVE.English;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountOccureanceWords {
    public static void main(String[] args) throws FileNotFoundException {

        

        BufferedReader reader = new BufferedReader(new FileReader("src/main/java/FULL_ARCHIVE/English/2days.text"));
        Stream<String> words = reader.lines().flatMap(line -> Arrays.stream(line.split(" ")));

        // Count the occurrences of each word
        Map<String, Long> wordCounts = words.map(String::toLowerCase).collect(Collectors.groupingBy(Function.identity(),TreeMap::new, Collectors.counting()));

        // Print the word counts
        for (Map.Entry<String, Long> entry : wordCounts.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        

      
    }


}
