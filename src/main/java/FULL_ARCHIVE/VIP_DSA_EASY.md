1. Description Binary Tree Level Sum
Given a binary tree and an integer which is the depth of the target level.

Calculate the sum of the nodes in the target level.

Example
Example 1:

Input：{1,2,3,4,5,6,7,#,#,8,#,#,#,#,9},2
Output：5 
Explanation：
     1
   /   \
  2     3
 / \   / \
4   5 6   7
   /       \
  8         9
2+3=5

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: the root of the binary tree
     * @param level: the depth of the target level
     * @return: An integer
     */
    public int levelSum(TreeNode root, int level) {
        // write your code here
        if (root == null || level <1){
            return 0;
        }
        if (level ==1){
            return root.val;
        }
        return levelSum(root.left, level-1) + levelSum(root.right, level-1);
    }
    
    //
    public int levelSum(TreeNode root, int level) {
            if(root == null || level< 1) return 0;
            if(level == 1) return root.val;
            return (levelSum(root.left,level-1) + levelSum(root.right, level-1)); 
        }
    
    //Queue
     public int levelSum(TreeNode root, int level) {
            // write your code here
            if (root == null || level < 1) {
                return 0;
            }
            Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root);
            while (!queue.isEmpty()) {
                level--;
                int size = queue.size();
                int sum = 0;
                for (int i = 0; i < size; i++) {
                    TreeNode cur = queue.poll();
                    sum += cur.val;
                    if (cur.left != null) {
                        queue.offer(cur.left);
                    }
                    if (cur.right != null) {
                        queue.offer(cur.right);
                    }
                }
                if (level == 0) {
                    return sum;
                }
            }
            return 0;
        }
}

2.Guess Number Higher or Lower
Description
We are playing the Guess Game. The game is as follows:

I pick a number from 1 to n. You have to guess which number I picked.

Every time you guess wrong, I will tell you if this number is greater or less than the number you guessed.

You call a pre-defined API guess(int num) which returns 3 possible results (-1, 1, or 0):
-1 means this number is less than the number you guessed

1 means this number is greater than the number you guessed

0 means this number is equal to the number you guessed

Example
Example 1:

Input : n = 10, I pick 4 (but you don't know)
Output : 4

/* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */

public class Solution extends GuessGame {
    /**
     * @param n an integer
     * @return the number you guess
     */
     public int guessNumber(int n) {
            int left = 1, right = n;
            while(left <= right){
                int mid = left+(right-left)/2;
                int result = guess(mid);
              //  return (result == 0) ? mid : (result == -1) ? mid-1 : mid+1;  
              if(result == 0) return mid;
              if(result == -1){ right= mid-1;}
              else {
                left = mid+1;
              }
            }
            return -1;
        }
         
         //
    public int guessNumber(int n) {
        // Write your code here
	   int start = 1;
	   int end = n;
	   while(start + 1<end){
		   int mid = start + (end - start)/2;
		   int res = guess(mid);
		   if(res==-1){
			   end = mid;
		   }else{
			   start = mid;
		   }
	   }
	int   res = guess(end);
	   if(res == 0){
		   return end;
	   }
	  res = guess(start);
	   if(res == 0){
		   return start;
	   }
	   return -1;
    }
}

823 · Input Stream
Description
Give two input stream inputA and inputB, which have Backspace. If the final result of the two input streams are equal, output YES, otherwise output NO.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Input characters include only lowercase letters and '<'
The length of input stream does not exceed 10000.
Example
Example1

Input:  inputA = "abcde<<" and inputB = "abcd<e<"
Output: "YES"
Explanation:
The final result of inputA and inputB is "abc", so return "YES".

Double stack, when inputting non-backspace characters, pop up backspace characters.
Then compare whether the entered characters are equal, and judge whether they are the same input stream.
Time complexity O(n)
Space complexity O(n)

public class Solution {
    /**
     * @param inputA: Input stream A
     * @param inputB: Input stream B
     * @return: The answer
     */
    public String inputStream(String inputA, String inputB) {
        // 双栈，压入非退格字符，弹出退格字符。
        Stack<Character> stackA = new Stack<>();
        Stack<Character> stackB = new Stack<>();
        char key = '<';
        for(Character c : inputA.toCharArray()){
            if(c != key){
                stackA.push(c);
            }else if(!stackA.isEmpty()){
                stackA.pop();
            }
        }
        for(Character c : inputB.toCharArray()){
            if(c != key){
                stackB.push(c);
            }else if(!stackB.isEmpty()){
                stackB.pop();
            }
        }
        // 双栈弹出字符若不同，输入流结果则不同
        while(!stackA.isEmpty() && !stackB.isEmpty()){
            if(!(stackA.pop() == stackB.pop())){
                return "NO";
            }
        }
        return "YES";
    }
    
    
    // public String inputStream(String inputA, String inputB) {
              // Write your code here
              String A = check(inputA);
              String B = check(inputB);
              if (A.equals(B)) {
                  return "YES";
              } else {
                  return  "NO";
              }
          }
          
          private String check(String input) {
              StringBuilder sb = new StringBuilder();
              for(int i = 0; i < input.length(); i++) {
                  if(input.charAt(i) != '<') {
                      sb.append(input.charAt(i));
                  } else {
                      if (sb.length() > 0) {
                          sb.deleteCharAt(sb.length() - 1);
                      }
                  }
              }
              
              return sb.toString();
          }
}

1182 · Reverse String II

Description
Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the start of the string. If there are less than k characters left, reverse all of them. If there are less than 2k but greater than or equal to k characters, then reverse the first k characters and left the other as original.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The string consists of lower English letters only.
Length of the given string and k will in the range [1, 10000]

Example
Example 1:

Input: s = "abcdefg", k = 2
Output: "bacdfeg"


public class Solution {
    /**
     * @param s: the string
     * @param k: the integer k
     * @return: the answer
     */
    public String reverseStringII(String s, int k) {
        // Write your code here.
        char[] chars=s.toCharArray();
        for(int i=0;i<chars.length;i++){
            int j=(i+1)%(2*k);
            //刚好为2k的倍数时跳过
            if(j==0){
                continue;
            }
            //2k中前k个字符
            if(j<k){
                // System.out.println("-----"+i);
                //超过2k多少
                int x=(i+1)%(2*k);  
                // System.out.println("x"+x);
                //差多少到K值 
                int y=k-x;             
                if(y+i>=chars.length-1){
                    y=chars.length-i-1;
                }
                //逆序排序的下标index 
                int z=i+y-x+1;   
                //转换位置，保证替换的下标不会超过中位数
                if(i<z){
                char mid=chars[i];
                chars[i]=chars[z];
                chars[z]=mid;
                }
            }
        }
        return new String(chars);
    }
    }
    
    //
    public String reverseStringII(String s, int k) {
            if (s == null || s.length() <= 1 || k <= 0) {
                return s;
            }
            
            char[] ss = s.toCharArray();
            int n = ss.length;
            int l = 0, r = Math.min(k - 1, n - 1);
            
            while (l < n) {
                reverseRange(ss, l, r);
                l += 2 * k;
                r = Math.min(r + 2 * k, n - 1);
            }
            
            return new String(ss);
        }
        
        private void reverseRange(char[] ss, int begin, int end) {
            while (begin < end) {
                char tmp = ss[begin];
                ss[begin] = ss[end];
                ss[end] = tmp;
                begin++;
                end--;
            }
        }
        
 1368 · Same Number
 Description
 Given an array, If the same number exists in the array, and the minimum distance the same number is less than the given value k, output YES, otherwise output NO.
 
 Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)
 
 
 The length of the given array is n，and n <= 100000.
 The element is x，0 <= x <= 1e9.
 1 <= k < n。
 Example
 Example1
 
 Input:  array = [1,2,3,1,5,9,3] and k = 4
 Output: "YES"
 Explanation:
 The distance of 1 whose indexes are  3 and 0 is 3, which meet
 
 public class Solution {
     /**
      * @param nums: the arrays
      * @param k: the distance of the same number
      * @return: the ans of this question
      */
     public String sameNumber(int[] nums, int k) {
         if (nums == null || nums.length == 0) {
             return "NO";
         }
         Map<Integer, Integer> position = new HashMap<>();
         for (int i = 0; i < nums.length; i++) {
             if (position.containsKey(nums[i])) {
                 if (i - position.get(nums[i]) < k) {
                     return "YES";
                 }
             } 
             position.put(nums[i], i);
         }
         return "NO";
     }
 }  
 
  //
     public String sameNumber(int[] nums, int k) {
           if(k == 1 || nums.length == 0) return "NO";
           Map<Integer, Integer> numPos = new HashMap<>();
           for(int i = 0; i < nums.length; i++){
               Integer prevPos = numPos.put(nums[i], i);
               if(prevPos != null && i - prevPos < k) return "YES";
           }
           return "NO";
       }
       
       
1401 · Twitch Words

Description
A normal word never contains two or more consecutive letters. We call a substring is a twitch if three or more letters in the word are consecutive. Now given a word and output the start points and the end points of all the twitch from left to right.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The input string length is n, n <= 100000.
Example
Example1

Input: str = "whaaaaatttsup"
Output: [[2,6],[7,9]]
Explanation: 
"aaaa" and "ttt" are twitching letters, and output their starting and ending points.

public class Solution {
    /**
     * @param str: the origin string
     * @return: the start and end of every twitch words
     */
    public int[][] twitchWords(String str) {
        List<int[]> result = new ArrayList<>();
        int s = 0, e = 1;
        while(e < str.length()){
            if(str.charAt(e) != str.charAt(s)){
                if(e-s > 2) result.add(new int[]{s, e-1}); // e指向的字符不是重复的字符
                s = e;
            }
            e++;
        }
        if(e-s > 2) result.add(new int[]{s, e-1}); // 处理结尾处的重复
        return result.toArray(new int[0][]);
    }
}

//

 public int[][] twitchWords(String str) {
        char pointer = str.charAt(0);
        int count = 0,start = 0,end;
        char[] chars = str.toCharArray();
        List<int[]> list = new ArrayList<>();
        for(int i = 0; i< chars.length;i++){
            if(chars[i] != pointer){
                if(count > 2){
                    end = i-1;
                    int[] interval = {start,end};
                    list.add(interval);
                }
                start = i;
                pointer = chars[i];
                count = 0;
            }
            count ++;
        }
        if(count > 2){
            end = chars.length-1;
            int[] interval = {start,end};
            list.add(interval);
        }
        int[][] result = new int[list.size()][2];
        for(int i = 0; i < list.size(); i++){
            result[i] = list.get(i);
        }
        return result;
    }
    
1436 · Flipping an Image
Description
Given a binary matrix A, we want to flip the image horizontally, then invert it, and return the resulting image.

To flip an image horizontally means that each row of the image is reversed. For example, flipping [1, 1, 0] horizontally results in [0, 1, 1].

To invert an image means that each 0 is replaced by 1, and each 1 is replaced by 0. For example, inverting [0, 1, 1] results in [1, 0, 0].

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= A.length = A[0].length <= 20
0 <= A[i][j] <= 1
Example
Example 1:

Input: [[1,1,0],[1,0,1],[0,0,0]]
Output: [[1,0,0],[0,1,0],[1,1,1]]

Explanation: First reverse each row: [[0,1,1],[1,0,1],[0,0,0]].
Then, invert the image: [[1,0,0],[0,1,0],[1,1,1]]

Flip each row first, then iterate through each number, or negate the last 1

class Solution {
    public int[][] flipAndInvertImage(int[][] A) {
        int n = A.length;
        int m = A[0].length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= (m - 1) / 2; j++) {
                if ((A[i][j] ^ A[i][m - j - 1]) == 0) {
                    if (A[i][j] == 0) {
                        A[i][j] = 1;
                        A[i][m - j - 1] = 1;
                    }
                    else {
                        A[i][j] = 0;
                        A[i][m - 1 - j] = 0;
                    }
                } 
            }
        }
        return A;
    }
}

1505 · Find the Number

Description
Given an unsorted array of n elements, find if the element k is present in the array or not.
Complete the findnNumber function in the editor below.It has 2 parameters:
1 An array of integers, arr, denoting the elements in the array.
2 An integer, k, denoting the element to be searched in the array.
The function must return true or false denoting if the element is present in the array or not

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= n <= 10^5
1 <= arr[i] <= 10^9
Example
Example:
Input:
arr = [1, 2, 3, 4, 5]
k = 1
Output: true

public class Solution {
    /**
     * @param nums: a integer array
     * @param k: a integer
     * @return: return true or false denoting if the element is present in the array or not
     */
    public boolean findnNumber(int[] nums, int k) {
        // write your code here
        return Arrays.stream(nums).anyMatch(it -> it == k);
    }
}

//
 public boolean findnNumber(int[] nums, int k) {
        // write your code here
        Map<Integer, Boolean> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int a = nums[i];
            map.put(a, true);
        }
        if(map.get(k)!=null){
            return true;
        }
        return false;
    }
    // public boolean findnNumber(int[] nums, int k) {
      
              // write your code here
      		boolean flag =false;
      	   for (int i : nums) {
      		   if(i==k){
      			 flag=true;
      		   }
      	   }
      	   return flag;
      	   
          }

1621 · Cut Connection
Description
Given a matrix consists of 0 and 1, the first line is the roof.
Remove one of '1', the same column of the '1' that is not connected to the roof will drop and need to be set to '0'.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Roof will not be cut

Example
Example1

Input: 
matrix = 
[ 
     [1,1,1,1,1],
     [0,0,1,0,1],
     [0,0,1,0,1],
     [0,0,1,0,0]
]
Point = (1,2)
Output: 
matrix = 
[                  
     [1,1,1,1,1],
     [0,0,0,0,1],
     [0,0,0,0,1],
     [0,0,0,0,0]
]

public class Solution {
    /**
     * @param matrix: 
     * @param x: 
     * @param y: 
     * @return: return the matrix
     */
    public int[][] removeOne(int[][] matrix, int x, int y) {
        // Write your code here
         while(x < matrix.length){
            if(matrix[x][y] == 1)
                matrix[x][y] = 0;
            x++;
        }
        return matrix;
    }
}

1623 · Minimal Distance In The Array
Description
Given two integer arrays a and b,please find the number in a with the minimal distance between corresponding value in b (the distance between two numbers means the absolute value of two numbers), if there are several numbers in a have same distance between b[i],just output the smallest number.
Finally, you should output an array of length b.length to represent the answer.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1<=a.length,b.length<=100000

Example
Example1

Input: a = [5,1,2,3], b = [2,4,2]
Output: [2,3,2]
Explanation: 
b[0]=2,2 is the number who has the minimal distance to 2
b[1]=4,3 and 5 have the same distance to 4,output 3 because 3 is smaller
b[2]=2,as well as b[0]

public class Solution {
    /**
     * @param a: array a
     * @param b: the query array
     * @return: Output an array of length `b.length` to represent the answer
     */
    int[] ans;

    int lowerBound (int[] a, int val) {
        int l = 0, r = a.length;
        while (r - l > 1) {
            int mid = (l + r) / 2;
            if (a[mid] >= val) {
                r = mid;
            } else {
                l = mid;
            }
        }
        if (a[l] >= val) {
            return l;
        } else {
            return r;
        }
    }

    public int[] minimalDistance (int[] a, int[] b) {
        // Write your code here
        int n = a.length, m = b.length;
        Arrays.sort (a);
        ans = new int[m];
        for (int i = 0; i < m; i++) {
            int id = lowerBound (a, b[i]);
            if (id == 0) {
                ans[i] = a[id];
            } else if (id == n) {
                ans[i] = a[id - 1];
            } else {
                if (b[i] - a[id - 1] <= a[id] - b[i]) {
                    ans[i] = a[id - 1];
                } else {
                    ans[i] = a[id];
                }
            }
        }
        return ans;
    }
}

1632 · Count email groups
Description
Give you an array of n email addresses.
Find the number of email groups and each group should have more than one email address(the address can be duplicated). If two strings have the same value after being transformed, they are in the same group.

The rules of transforming are as follows:

Ignore all the characters '.' before the character '@'.
Ignore the substring which starts with the first '+'(included) and ends with the character '@'(exclude).
Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


a email group have at least two same email address(after transforming)

Example
Example1

Input: emails = ["abc.bc+c+d@jiuzhang.com", "abcbc+d@jiuzhang.com", "abc.bc.cd@jiuzhang.com"]
Output: 1
Explanation: 
"abc.bc+c+d@jiuzhang.com" transforms to "abcbc@jiuzhang.com"
"abcbc+d@jiuzhang.com" transforms to "abcbc@jiuzhang.com"
"abc.bc.cd@jiuzhang.com" transforms to "abcbccd@jiuzhang.com"
We can see that the first address and the second address are in the same group, and there is no address can transform to the same address as the third one. Therefore, there is only one group which meets the requrements.

public class Solution {
    public int countGroups(List<String> emails) {
        HashMap<String, Integer> map = new HashMap<>();
        int count = 0;
        for( String item : emails) {
            String name = item.substring(0, item.lastIndexOf('@'));
            String cp = item.substring(item.lastIndexOf('@'));
            if(name.indexOf('+') > -1) {
                name = name.substring(0, name.indexOf('+'));
            }
            name = name.replace(".", "");
            map.put(name+cp, map.getOrDefault(name+cp, 0) + 1);
        }
        for(Integer c : map.values()) {
            if(c > 1) count++;
        }
        return count;
    }
}

//
 public int countGroups(List<String> emails) {
        // Write your code here
        
        if (emails == null || emails.size() == 0) {
            return 0;
        }
        Map<String, Integer> map = new HashMap<>();
        
        for (String email : emails) {
            String[] parts = email.split("@");
            String[] local = parts[0].split("\\+");
            String item = local[0].replace(".", "") + "@" + parts[1];
            if (!map.containsKey(item)) {
                map.put(item, 1);
            } else {
                map.put(item, map.get(item) + 1);
            }
        }
        
        int cnt = 0;
        for (String key : map.keySet()) {
            if (map.get(key) > 1) {
                cnt++;
            }
        }
        
        return cnt;
    }
    
1633 · Strings That Satisfies The Condition

Description
Given a string target and a string array s, output all strings containing target(ie target is a subsequence of s[i]) in their original order in the array s

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


s.length<=1000
1<=the sum of strings’ length in s,target<=100000

Example
**Example1**
Input: target="google",s=["goooogle","abc","google","higoogle","googlg","gowogwle","gogle"]
Output: ["goooogle","google","higoogle","gowogwle"]


Explanation: 
**go**oo**ogle**
**google**
hi**google**
**go**w**og**w**le**

two pointer method, O(N) time and O(1) space, N being the number of input strings in source.

public class Solution {
    /**
     * @param target: the target string
     * @param s: 
     * @return: output all strings containing target  in s
     */
    public String[] getAns(String target, String[] s) {
        boolean [] take = new boolean [s.length];
        int count = 0;
        
        char [] ta = target.toCharArray();

        for (int i = 0; i < s.length; i++) {
            if (satisfy(s[i].toCharArray(), ta)) {
                count ++;
                take[i] = true;
            }
            else {
                take [i] = false;
            }
        }
        
        String [] result = new String [count];
        int idx = 0;
        for (int i = 0; i < s.length; i++) {
            if (take[i]) {
                result[idx] = s[i];
                idx++;
            }
        }
        return result;
    }
    
    private boolean satisfy (char[] source, char[] target) {
        
        int s = 0;
        int t = 0;
        
        while (s < source.length && t < target.length) {
            if (source[s] == target[t]) {
                s++;
                t++;
            }
            else {
                s++;
            }
        }

        return t == target.length;
    }

}
// public String[] getAns(String target, String[] s) {
          // Write your code here
          List<String> ans = new ArrayList<>();
          for (String str : s) {
              if (valid(target, str)) {
                  ans.add(str);
              }
          }
          String[] result = new String[ans.size()];
          for (int i = 0; i < result.length; i++) {
              result[i] = ans.get(i);
          }
          return result;
      }
      private boolean valid(String t, String s) {
          int left = 0;
          int right = 0;
          while (left < t.length() && right < s.length()) {
              if (t.charAt(left) == s.charAt(right)) {
                  left++;
                  right++;
              } else {
                  right++;
              }
          }
          if (left < t.length()) {
              return false;
          }
          return true;
      }

1698 · Delete Columns to Make Sorted
Description
We are given an array A of N lowercase letter strings, all of the same length.

Now, we may choose any set of deletion indices, and for each string, we delete all the characters in those indices.
The remaining string rows are read from top to bottom to form columns.

For example, if we have an array A = ["``abcdef``","uvwxyz"] and deletion indices {0, 2, 3}, then the final array after deletions is ["bef", "vyz"], and the remaining columns of A are ["b"``,"``v"], ["e","y"], and ["f","z"]. (Formally, the c-th column is [A[0][c], A[1][c], ..., A[A.length-1][c]].)

Suppose we chose a set of deletion indices D such that after deletions, each remaining column in A is in non-decreasing sorted order.

Return the minimum possible value of D.length.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= A.length <= 100
1 <= A[i].length <= 1000
Example
Example 1:

Input: ["cba","daf","ghi"]
Output: 1
Explanation: 
After choosing D = {1}, each column ["c","d","g"] and ["a","f","i"] are in non-decreasing sorted order.
If we chose D = {}, then a column ["b","a","h"] would not be i

public class Solution {
    /**
     * @param a: 
     * @return: nothing
     */
    public int minDeletionSize(String[] a) {
        // 最小要删除的列数。
        int cnt = 0;
        Boolean ok;
        // 枚举每一列。
        for (int j = 0; j < a[0].length(); ++j) {
            // 默认当前列满足条件，不需要删除。
            ok = true;
            // 判断本列的字符是否满足条件。
            for (int i = 1; i < a.length; ++i)
                // 出现一次逆序，则本列的字符不满足条件。 
                if (a[i].charAt(j) < a[i - 1].charAt(j)) {
                    ok = false;
                    break;
                }
            // 有了不满足条件的列，则本列需要被删除。
            if (!ok) ++cnt;
        }
        return cnt;
    }
}

//
 public int minDeletionSize(String[] a) {
        // 
        if (a.length == 0) {
            return 0;
        }
        int res = 0;
        for (int c = 0; c < a[0].length(); ++c) {
            for (int i = 0; i < a.length - 1; ++i) {
                if(a[i].charAt(c)>a[i+1].charAt(c)){
                    res++;
                    break;
                }
            }
        }
        return res;
    }
 
 1709 · Number of Recent Calls
 
 Description
 Write a class RecentCounter to count recent requests.
 
 It has only one method: ping(int t), where t represents some time in milliseconds.
 
 Return the number of pings that have been made from 3000 milliseconds ago until now.
 
 Any ping with time in [t - 3000, t] will count, including the current ping.
 
 It is guaranteed that every call to ping uses a strictly larger value of t than before.
 
 Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)
 
 
 Each test case will have at most 10000 calls to ping.
 Each test case will call ping with strictly increasing values of t.
 Each call to ping will have 1 <= t <= 10^9.
 Example
 Example 1:
 
 Input：["RecentCounter","ping","ping","ping","ping"],[[],[1],[100],[3001],[3002]]
 Output：[null,1,2,3,3]
 Explanation：
 The list of time of calls is [1,100,3001,3002].
 1.call "RecentCounter" gets null.
 2.call "ping",the range of time is[0,1],there is a call(t=1).
 3.call "ping",the range of time is[0,100],there are two calls(t=1,t=100).
 4.call "ping",the range of time is[1,3001],there are three calls(t=1,t=100,t=3001).
 5.call "ping",the range of time is[2,3002],there are three ca
 
 class RecentCounter {
     int[] vec = new int[10005];
     /* 定义快慢指针 */
     int slowIndex, fastIndex;
     /* 构造函数初始化 */
     public RecentCounter() {
         slowIndex = 0;
         fastIndex = 0;
     }
 
     public int ping(int t) {
         vec[fastIndex++] = t;
         while(vec[slowIndex] < t - 3000)
             slowIndex++;
         /* 返回数组的大小 = 发生的请求数 */
         return fastIndex - slowIndex;
     }
 }
 //
 
 class RecentCounter {
     Queue<Integer> queue;
 
     public RecentCounter() {
         queue = new ArrayDeque<Integer>();
     }
 
     public int ping(int t) {
         queue.offer(t);
         while (queue.peek() < t - 3000) {
             queue.poll();
         }
         return queue.size();
     }
 }
 
1779 · Shortest Duplicate Subarray       
Description
Given an array, return the shortest subarray length which has duplicate elements.

Returns -1 if the array has no subarray with duplicate elements.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


0 <= arr.length <= 10^6
Example
Example 1:

Input: [1,2,3,1,4,5,4,6,8]
Output: 3
Explanation: The the shortest subarray which has duplicate elements is [4,5,4].

public class Solution {
    /**
     * @param arr: The array you should find shortest subarray length which has duplicate elements.
     * @return: Return the shortest subarray length which has duplicate elements.
     */
    public int getLength(int[] arr) {
        // Java 中的哈希表为 HashMap 类型。
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        // 结果的初始值设为 -1。
        int res = -1;
        for (int i = 0; i < arr.length; ++i) {
            // 令 num 为当前的元素。
            int num = arr[i];
            // 在哈希表中查找当前元素。如果没找到，则在哈希表中插入当前元素，并记录当前元素在数组中的下标。
            if (!map.containsKey(num)) { 
                map.put(num, i);
            }
            // 哈希表中找到了当前元素。
            else {
                // 计算当前元素在数组中上个和当前一样值的元素的距离。
                int pos = i - map.get(num) + 1;
                // 如果距离比当前结果要小，则更新结果。
                if (res < 0 || pos < res)
                    res = pos;
                // 更新哈希表中当前元素的位置。
                map.put(num, i);
            }
        }
        // 返回结果。
        return res;
    }
}



1812 · Rotation Number
Description
Give you a positive integer n, Rotations Number is a number that rotates by 180° after each digit, and is not equal to its own number. And you also ensure that the number of rotations is less than or equal to n. You should find all the numbers that satisfy the definition of the Rotation Number in the range of 1 - n and return an array res. For example, 8069 -> 8096, 769 cannot be rotated because 7 is not a number after 180° rotation.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 =< n <= 10000
0 <= res[i] <= n
If the number x is greater than n after rotation, then x cannot be placed in res
The numbers are only 0, 6, 8, 9 and can be rotated, where 0->0, 6->9, 8->8, 9->6,

Example
Example 1:

Input:
n = 10
Output:
[6, 9]

public class Solution {
    /**
     * @param n: A Integer
     * @return: return the list of Rotation Number in 1-n
     */
     List<Integer> res = new ArrayList<>();
    public List<Integer> RotationNumber(int n) {
        // write your code here
        int count = helper(0, 0, n);
        System.out.println(count);
        return res;
    }

    int[][] pairs = new int[][] {{0, 0}, {6, 9}, {8, 8}, {9, 6}};
    private int helper(int num, int rotatedNum, int limit) {
        if (num > limit) return 0;
        int count = 0;
        if (num != rotatedNum && rotatedNum <= limit) {
            res.add(num);
            count += 1;
        }
        for (int[] p : pairs) {
            if (num == 0 && p[0] == 0) continue;
            count += helper(num * 10 + p[0], rotatedNum * 10 + p[1], limit);
        }
        return count;
    }
}

1859 · Minimum Amplitude
Description
Given an array A consisting of N integers. In one move, we can choose any element in this array and replace it with any value.
The amplitude of an array is the difference between the largest and the smallest values it contains.
Return the smallest amplitude of array A that we can achieve by performing at most three moves.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


N is an integer within the range: [2, 10000]
each element of array A is an integer within the range: [-50, 50]
Example
Example 1

Input:
A = [-9, 8, -1]
Output:
0
Explanation: 
We can replace -9 and 8 with -1 so that all element are equal to -1, and then the amplitude is 0

public class Solution {
    /**
     * @param A: a list of integer
     * @return: Return the smallest amplitude
     */
    public int MinimumAmplitude(int[] A) {
        if(A.length<=4){
            return 0;
        }
        Arrays.sort(A);
        int min=101;
        for (int i = 0; i < 4; i++) {
            min=Math.min(min, A[A.length-4+i]-A[i]);
        }
        return min;
    }
}
             

1867 · Most Frequent Word II
Description
Give a string which represents the content of the novel and return the most frequently occurring words in the string(If there are more than one words with the largest number of occurrences, return the one with the smallest number of lexicographical orders.).

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= paragraph.length <= 50000
The answer is unique, and written in lowercase (even if its occurrences in paragraph may have uppercase symbols, and even if it is a proper noun.)
paragraph only consists of letters, spaces, and the punctuation symbols !?',;.
Different words in paragraph are always separated by a space.
There are no hyphens or hyphenated words.
Words never apostrophes or other punctuation symbols.
Example
Example 1:

Input: 
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
Output: 
"hit"
Explanation: 
"hit" appears most often, at 3 times.

public class Solution {
    /**
     * @param paragraph: a string
     * @return: the most frequent word
     */
    public String mostCommonWord(String paragraph) {
        // write your code here
        String str = paragraph.replaceAll("[.?!';,]", " ");
        String[] parts = str.split(" ");
        Map<String, Integer> map = new HashMap<>();
        for (String p : parts) {
            if (!p.trim().isEmpty()) {
                map.put(p.toLowerCase(), map.getOrDefault(p.toLowerCase(), 0) + 1);
            }
        }
        int max = 0;
        String res = "";
        for (String key : map.keySet()) {
            if (map.get(key) > max) {
                res = key;
                max = map.get(key);
            }
            if (map.get(key) == max && key.compareTo(res) < 0) {
                res = key;
            }
        }
        return res;
    }
}

1916 · Longest lighting time
Description
You have 26 lamps in a row, and a list of operations that can switch lamps on or off.
All of lamps are off from the start, then the lamps will accept these operations one by one.
Each of operation[i] includes two integers, operation[i][0] and operation[i][1].
After these lamps accept an operation, the operation[i][0]-th lamp will turn on, and it will turn off at the operation[i][1]-th second.When this lamp turns off, the next operation will be accepted. That is, any time there is only one light is on.
The first operation will be sent at the 0-th second, and it will be accepted immediately.
You need to find which lamp has the longest continuously lighting time.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \le n \le 10^51≤n≤10 
5
 
0 \le keyTimes[i][0] \le 25 (0 \le i < n)0≤keyTimes[i][0]≤25(0≤i<n)
0 \le keyTimes[i][1] \le 10^8 (0 \le i < n)0≤keyTimes[i][1]≤10 
8
 (0≤i<n)
There will only be one key with the worst time.
keyTimes is sorted in ascending order of keyTimes[i][1].

Example
Example 1

Input：
[[0,2],[1,5],[0,9],[2,15]]
Output：
'c'
Explanation: 
operation = `[[0, 2], [1, 5], [0, 9], [2, 15]]`
At 0-th second, the lamps will accept the operation `[0, 2]`, so the lamp0 will turn on, and it will turn off at the 2-nd second.It will lighting `2-0 = 2` seconds.
At 2-nd second, the lamps will accept the operation `[1, 5]`, so the lamp1 will turn on, and it will turn off at the 5-th second.It will lighting `5-2 = 3` seconds.
At 5-th second, the lamps will accept the operation `[0, 9]`, so the lamp0 will turn on, and it will turn off at the 9-th second.It will lighting `9-5 = 4` seconds.
At 9-th second, the lamps will accept the operation `[2, 15]`, so the lamp2 will turn on, and it will turn off at the 15-th second.It will lighting `15-9 = 6` seconds.
So the longest continuously lighting time is `max(2, 3, 4, 6) = 6` seconds.

**You need to return a lowercase letter instead of a number, such as `'a' = 0, 'b' = 1, ..., 'z' = 25`.**
So the answer to the above example is 'c'.

public class Solution {
    /**
     * @param operation: A list of operations.
     * @return: The lamp has the longest liighting time.
     */
    public char longestLightingTime(List<List<Integer>> operation) {
        int maxTime = operation.get(0).get(1);
        char answer = (char)('a' + operation.get(0).get(0));
        int n = operation.size();

        for (int i = 0; i < n - 1; i++) {
            List<Integer> next = operation.get(i + 1);
            List<Integer> current = operation.get(i);
            int time = next.get(1) - current.get(1);
            if (time > maxTime) {
                maxTime = time;
                answer = (char)(next.get(0) + 'a');
            }
        }
        return answer;
    }
}

//

public char longestLightingTime(List<List<Integer>> operation) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int preofftime = 0;
        for (List<Integer> light : operation) {
            if(!map.containsKey(light.get(0))){
                map.put(light.get(0),map.getOrDefault(light.get(0), 0)+(light.get(1) - preofftime));
            }else{
                 map.put(light.get(0), Math.max(map.get(light.get(0)),(light.get(1) - preofftime)));
            }
            preofftime = light.get(1);
        }
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((a,b) -> { return map.get(b) - map.get(a);});
        for (int key : map.keySet()) {
            priorityQueue.offer(key);
        }

        return (char)(priorityQueue.poll() + 97);
    }
    

1240 · Path Sum III

Description
You are given a binary tree in which each node contains an integer value.

Find the number of paths that sum to a given value.

The path does not need to start or end at the root or a leaf, but it must go downwards (traveling only from parent nodes to child nodes).

The tree has no more than 1,000 nodes and the values are in the range -1,000,000 to 1,000,000.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Input：root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8
Output：3
Explanation：
      10
     /  \
    5   -3
   / \    \
  3   2   11
 / \   \
3  -2   1

Return 3. The paths that sum to 8 are:

1.  5 -> 3
2.  5 -> 2 -> 1
3. -3 -> 11


class Solution {

/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */
    public int pathSum(TreeNode root, int targetSum) {
        Map<Long, Integer> prefix = new HashMap<Long, Integer>();
        prefix.put(0L, 1);
        return dfs(root, prefix, 0, targetSum);
    }

    public int dfs(TreeNode root, Map<Long, Integer> prefix, long curr, int targetSum) {
        if (root == null) {
            return 0;
        }

        int ret = 0;
        curr += root.val;

        ret = prefix.getOrDefault(curr - targetSum, 0);
        prefix.put(curr, prefix.getOrDefault(curr, 0) + 1);
        ret += dfs(root.left, prefix, curr, targetSum);
        ret += dfs(root.right, prefix, curr, targetSum);
        prefix.put(curr, prefix.getOrDefault(curr, 0) - 1);

        return ret;
    }
}              