Please prepare for these concepts for your upcoming Interviews.



Time Complexity,
Space Complexity,
Separation of concerns,
Entity model
Entity design
Well defined API classes,
Feasibility of the code, Readability
Exception handling
Concurrency, Debugging Skills
Modularity and Extensibility
Functionalities,
Function correctness,
You should write all the test cases,
Concentrate on Edge cases and Corner cases,
Demoable code
Maintain good coding practices,
Object oriented principles,
Feel free to ask questions to the interviewers if you are facing any issues,
Do not jump into the solution without understanding the problem statement.
If the interviewer asks you to modify the code, pls do it, do not give up.
Utilize all the time given to you, do not come to the conclusion early,
Think out of the box with the help of the hints given by the panel's,
Be friendly with the panel with Good Attitude.
Implement all the features asked by the interviewer.
Prepare on how to solve the problem with optimized solution first and then recursive approach/Brute Force approach,
Panel's will check on how you will give ideas to approach the problem.
Data structures, Algorithms,
They will check on Strong hold of the programming language which you have chosen,
Debugging and fixing the issues discovered while running test cases
Good understanding of SDLC process and used structured mechanism for story estimation.  Good understanding of business metrics SLA breaches and escalation breaches.
Naming convention.
Proficient in the language constructs.Syntax lookups.
Keen to learn.
Good understanding of concepts
Communication Skills/ Correct way of using technical terms,
What is your self learning??
Reason for leaving??
Why are you interested in joining Flipkart??
Graph algorithms.
Clarify the doubts on asking a question and before jumping to a solution.
Be open to suggestions and should be able to work on the hints to come up with a better approach if the problem is difficult to solve.
List the variables and requirements first and then start the coding part.
Code should not be difficult to understand, please write the clean code.
How transactions work in RDBMS
 Interfaces.
Should be very strong in Stitching of flows.
Should be very careful in choosing the Database.
Relational Data entity Design and corresponding choices.
Honesty in questions
You should be able to explain what you are working on clearly.
Queues and SQL.
What would you do if you didn't agree with your manager's feedback?
Any pressurized situation ??
Tell me about a time you had a difficult working relationship with a colleague. What was the challenge?
What do you expect from a manager ??
What is your feedback from colleagues ??
How is your collaboration with different teams ??
Tell me about a time you had to make a difficult decision??
Why do you think you would be successful ??
What kind of innovations do you drive in your team ??


1
Domain services design
Messaging, Caching, Polling/streaming/Sockets, Latency & Throughput, Networks and Protocols
2
Building for scale
(Non functional requirements) - Endpoint protection, DB Proxy Sharding/Indexing, Load Balancing
3
Code extensibility
Separation of concern, right abstractions, re-usability and Modularisation
4
DB Design
Understanding of Normalization of tables, Understanding of indexes, choice of primary and foreign keys, Sharding/partitioning/replication of databases
5
Code Design
Choice of classes (Abstract, interface, final) and placement of objects, Usage/knowledge of design patterns
6
Storage Estimation
Scaling services, Number of requests per service, Read write ratio, Load balancer
7
High level API design
Setters, getters, Key APIs & their design

1
DB Design
Understanding of indexes, choice of primary and foreign keys, Sharding/partitioning of databases, SQL queries (Joins 2-3 tables, having group by)
2
Code design and LLD with 2+ modules
Choice of classes (Abstract, Interface, final) and placement of objects, Usage/Knowledge of design patterns,
3
Code extensibility
Separation of concern, right abstractions, re-usability and Modularisation
4
API/event contracts
Security of API’s, validations of data
5
Sync/Async communication
Sync/Async communication



Recently I have received a lot of queries regarding my preparation that landed me in FAANG/MAANG companies. Below are some of the resources that helped me a lot -

✅ 𝐂𝐨𝐝𝐢𝐧𝐠 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Solve LeetCode medium-level problems (at least more than 250+ covering different topics)
👉 I have created an xlsx on top/important 500 leetCode questions (https://lnkd.in/dWn73jXd) and a video on How to Crack The Coding Interview. (https://lnkd.in/dsn4cipq).
👉 Clement Mihailescu's AlgoExpert 195 handpicked questions (In case you want to prepare fast and only good questions)
👉 I watched my DSA playlist to revise concepts at 2x speed. (https://lnkd.in/dJfS9FkD)

✅ 𝐒𝐲𝐬𝐭𝐞𝐦 𝐃𝐞𝐬𝐢𝐠𝐧 / 𝐇𝐢𝐠𝐡-𝐋𝐞𝐯𝐞𝐥 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Grokking the System Design Interview - It has step-by-step discussion and good case studies on system design (https://lnkd.in/dXywra2R)
👉 Alex Xu's System Design Interview course on ByteByteGo (https://lnkd.in/d6yRpRvy) - The course covers all the content from his famous book (Vol 1 and Vol 2) System Design Interview
👉 Clement Mihailescu #SystemsExpert (https://lnkd.in/d-5mSpfw) videos to know how real-life System Design Interviews go

✅ 𝐎𝐛𝐣𝐞𝐜𝐭 𝐎𝐫𝐢𝐞𝐧𝐭𝐞𝐝 𝐃𝐞𝐬𝐢𝐠𝐧 / 𝐋𝐨𝐰-𝐋𝐞𝐯𝐞𝐥 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Grokking the Object Oriented Design Interview (https://lnkd.in/deuAXR-U) - A very detailed and step-by-step approach to various object-oriented design case studies.

✅ 𝐀𝐏𝐈 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Best Practices (https://lnkd.in/d_x39xkK), Implementation (https://lnkd.in/d5fhXZJ7), and Guidelines (https://lnkd.in/dr397Hy2) of API Design
👉 Look for use cases like - Stripe (https://lnkd.in/dsM7PpJt) and Twitter (https://lnkd.in/dxiMu8wr) API Documentation
👉 #SystemsExpert also has a few case studies on API design as well

✅ 𝐒𝐜𝐡𝐞𝐦𝐚 / 𝐃𝐚𝐭𝐚𝐛𝐚𝐬𝐞 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Grokking the Object Oriented Design Interview (https://lnkd.in/deuAXR-U) - Take the case studies and try to apply Object to Relational Mapping strategy

✅ 𝐁𝐞𝐡𝐚𝐯𝐢𝐨𝐫𝐚𝐥 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰
👉 Watch Jeff H Sipe, Corporate / Individual Consulting YouTube channel for behavioral questions (https://lnkd.in/d6rgxbnr)
👉 https://lnkd.in/dXBfNNK9

✅ 𝐈𝐦𝐩𝐨𝐫𝐭𝐚𝐧𝐭 𝐃𝐒𝐀 𝐭𝐨𝐩𝐢𝐜𝐬?
Array, Binary Search, Sliding Window, Matrix, Two Pointer, Intervals, Hash Map, String, Recursion, DP, Trees, Graph, Linked List, Stack, Queue & Heap

✅ 𝐂𝐫𝐚𝐜𝐤𝐢𝐧𝐠 𝐭𝐡𝐞 𝐆𝐀𝐌𝐀𝐌 𝐓𝐞𝐜𝐡𝐧𝐢𝐜𝐚𝐥 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰𝐬 - For all the preparation resources, strategies, tips, and roadmap that I followed, you can buy my ebook at - https://lnkd.in/d9xe8yfJ