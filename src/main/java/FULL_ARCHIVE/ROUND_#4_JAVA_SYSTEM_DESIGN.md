
#1. How would you build TinyURL?
#2. Design a web crawler
#3. Design a metrics and logging service.
#4. System design: timer
#5. Design a visual landmark recognition system.
#6. Design a cache controller.
#7. Design a service that supports uploading and tagging images to a travel site.
#8. Design a rate limiter.
#9. Design a load balancer


1. High Level System Design -> 
------------------------------
Design Uber like Service. Follow up 
->What would be your tech stack for designing such a service to make sure it could scale when needed.
-----------------------------------------------------------------------------------------------------

->Tech stack for designing a scalable Uber-like service.
->Use microservices architecture for scalability and fault tolerance.
->Choose a cloud provider with auto-scaling capabilities.
->Use a load balancer to distribute traffic across multiple instances.
->Use a NoSQL database for high availability and scalability.
->Use message queues for asynchronous communication between services.
->Use containerization for easy deployment and management.
->Use caching to improve performance.
->Use monitoring and logging tools to identify and troubleshoot issues.
->Examples: AWS, Kubernetes, MongoDB, RabbitMQ, Redis, Prometheus, Grafana.

Backend:
->Programming Language: Java is popular choices for building scalable backend services.
->Framework: Spring Boot (Java) can provide a solid foundation for developing the backend.
->Database: A combination of relational and NoSQL databases can be used. For example, PostgreSQL or MySQL for relational data and MongoDB or Cassandra for high scalability and flexibility.
->Caching: Distributed caching systems like Redis or Memcached can be used for caching frequently accessed data and improving response times.
->Messaging: Apache Kafka or RabbitMQ can be used for event-driven communication between different components of the system.
API Design: RESTful APIs can be designed using frameworks like Spring MVC (Java).

Geolocation and Mapping:
-> Geocoding: Services like Google Maps Geocoding API or OpenStreetMap Nominatim can be used for converting addresses into coordinates.
-> Routing and Directions: Google Maps Directions API or Mapbox Directions API can be used for generating optimal routes and directions.

Real-time Communication:
->Websockets: Technologies like WebSocket or Socket.IO can enable real-time communication between users, drivers, and the server.

Infrastructure and Scalability:
->Cloud Platform: Utilize a cloud provider like Amazon Web Services (AWS), Google Cloud Platform (GCP), or Microsoft Azure to benefit from their scalable infrastructure services.
->Load Balancing: Employ load balancers such as NGINX or AWS Elastic Load Balancer to distribute incoming traffic across multiple server instances.
->Auto Scaling: Leverage auto-scaling features provided by the cloud platform to dynamically adjust the number of server instances based on traffic load.
->Containerization: Docker can be used for containerization, allowing for easier deployment and scalability of microservices.
->Orchestration: Kubernetes can be used for container orchestration, managing and scaling multiple containers across a cluster.

Analytics and Monitoring:
->Logging: Tools like Elasticsearch, Logstash, and Kibana (ELK Stack) can be used for collecting and analyzing logs.
->Monitoring: Employ monitoring systems like Prometheus, Grafana, or New Relic for real-time monitoring of system performance, metrics, and alerting.

Frontend:

Web Application: React or Angular can be used to build a responsive and interactive web interface for users and drivers.
Mobile Application: Native development with Swift (iOS) and Kotlin (Android) or cross-platform frameworks like React Native or Flutter can be used to develop mobile apps.

--------------------------------------------------------------------------------------------------------------
Design tiny url service which should be scalable up to 10m requests a year. Derive the algorithm such as every request that the system generates is unique and read operation should be very efficient for obvious reasons.
----------
Design a scalable tiny URL service with unique requests and efficient read operation.
->Use a hashing algorithm to generate unique short URLs from long URLs
->Store the mappings in a distributed key-value store like Redis or Cassandra
->Use a load balancer to distribute requests across multiple servers
->Implement caching to improve read performance
->Consider using a CDN to serve frequently accessed URLs
->Implement rate limiting and throttling to prevent abuse
->Use HTTPS to ensure secure communication


--------------------------------------------
Design a website similar to bookmyshow.com for booking cinema tickets but it must be for a single location only which can have multiple theatres in it. In this he wanted me to design a basic rough GUI, relevant database tables and linking GUI to database and also showing the whole data flow in system. Site must also provide features like advance booking, user login, user registration, movie rating option, saving card details etc
----------------------------------------------------------


𝐆𝐨𝐥𝐝𝐞𝐧 𝐑𝐮𝐥𝐞𝐬 𝐭𝐨 𝐚𝐧𝐬𝐰𝐞𝐫 𝐢𝐧 𝐚 𝐒𝐲𝐬𝐭𝐞𝐦 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰

Sharing with you a pdf that has golden rules to answer in System Design Interviews. Save pdf for all such rules. Some of the rules are as follows -

1. If we are dealing with a read-heavy system, it's good to consider using a Cache.

2. If we need low latency in the system, it's good to consider using a Cache & CDN.

3. If we are dealing with a write-heavy system, it's good to use a Message Queue for Async processing

4. If we need a system to be an ACID complaint, we should go for RDBMS or SQL Database

5. If data is unstructured & doesn't require ACID properties, we should go for NO-SQL Database

6. If the system has complex data in the form of videos, images, files etc, we should go for Blob/Object storage

7. If the system requires complex pre-computation like a news feed, we should use a Message Queue & Cache

8. If the system requires searching data in high volume, we should consider using a search index, tries or a search engine like Elasticsearch

9. If the system requires to Scale SQL Database, we should consider using Database Sharding

10. If the system requires High Availability, Performance, & Throughput, we should consider using a Load Balancer

11. If the system requires faster data delivery globally, reliability, high availability, & performance, we should consider using a CDN

12. If the system has data with nodes, edges, and relationships like friend lists, & road connections, we should consider using a Graph Database

13. If the system needs scaling of various components like servers, databases, etc, we should consider using Horizontal Scaling

14. If the system requires high-performing database queries, we should use Database Indexes

15. If the system requires bulk job processing, we should consider using Batch Processing & Message Queues

16. If the system requires reducing server load and preventing DOS attacks, we should use a Rate Limiter

17. If the system has microservices, we should consider using an API Gateway (Authentication, SSL Termination, Routing etc)

18. If the system has a single point of failure, we should implement Redundancy in that component

19. If the system needs to be fault-tolerant, & durable, we should implement Data Replication (creating multiple copies of data on different servers)

20. If the system needs user-to-user communication (bi-directional) in a fast way, we should use Websockets

21. If the system needs the ability to detect failures in a distributed system, we should implement a Heartbeat

22. If the system needs to ensure data integrity, we should use Checksum Algorithm

23. If the system needs to scale servers with add/removal of nodes efficiently, with no hotspots, we should implement Consistent Hashing
    A System Design Interview usually lasts for 45-60 minutes. The following template will guide you on how to manage time duration in a System Design Interview -

✅ 𝐑𝐞𝐪𝐮𝐢𝐫𝐞𝐦𝐞𝐧𝐭 𝐂𝐥𝐚𝐫𝐢𝐟𝐢𝐜𝐚𝐭𝐢𝐨𝐧𝐬 - (3-5 𝐦𝐢𝐧)
Ask clarifying questions to understand the problem and expectations of the interviewer.
𝐚) 𝐅𝐮𝐧𝐜𝐭𝐢𝐨𝐧𝐚𝐥 𝐑𝐞𝐪𝐮𝐢𝐫𝐞𝐦𝐞𝐧𝐭𝐬
👉 Focussed use cases to cover (MVP)
👉 Use cases that will not be covered
👉 Who/How will use the system
👉 Total/Daily active users
𝐛) 𝐍𝐨𝐧 𝐅𝐮𝐧𝐜𝐭𝐢𝐨𝐧𝐚𝐥 𝐑𝐞𝐪𝐮𝐢𝐫𝐞𝐦𝐞𝐧𝐭𝐬
👉 Is the system Highly Available or Highly Consistent? CAP theorem?
👉 Does the system requires low latency?
👉 Does the system needs to be reliable?

✅ 𝐄𝐬𝐭𝐢𝐦𝐚𝐭𝐢𝐨𝐧𝐬 (3-5 𝐦𝐢𝐧)
👉 Latency/Throughput expectations
👉 QPS (Queries Per Second) Read/Write ratio
👉 Traffic estimates
👉 Storage estimates
👉 Memory estimates

✅ 𝐀𝐏𝐈 𝐃𝐞𝐬𝐢𝐠𝐧 (3-5 𝐦𝐢𝐧)
👉 Outline the different APIs for required scenarios

✅ 𝐃𝐚𝐭𝐚𝐛𝐚𝐬𝐞 𝐒𝐜𝐡𝐞𝐦𝐚 𝐃𝐞𝐬𝐢𝐠𝐧 (3-5 𝐦𝐢𝐧)
👉 Identify the type of database (SQL or NoSQL)
👉 Design schema like tables/columns and relationships with other tables (SQL)

✅ 𝐒𝐲𝐬𝐭𝐞𝐦'𝐬 𝐃𝐞𝐭𝐚𝐢𝐥𝐞𝐝 𝐃𝐞𝐬𝐢𝐠𝐧 (20 - 25 𝐦𝐢𝐧)
(a) Draw/Explain high-level components of the system involving below (if required) components -
👉 Client (Mobile, Browser)
👉 DNS
👉 CDN
👉 Load Balancers
👉 Web / Application Servers
👉 Microservices
👉 Blob/Object Storage
👉 Proxy/Reverse Proxy
👉 Database (SQL or NoSQL)
👉 Cache at various levels (Client side, CDN, Server side, Database side, Application level caching)
👉 Messaging Queues for asynchronous communication

(b) Identification of algorithm/data structures and ways to scale them
(c) Scaling individual components - Horizontal & Vertical Scaling
(d) Database Partitioning -
i) Methods
👉 Horizontal Partitioning
👉 Vertical Partitioning
👉 Directory-Based Partitioning
ii) Criteria    
👉 Range-Based Partitioning
👉 Hash-Based Partitioning (Consistent Hashing)
👉 Round Robin
(e) Replication & Redundancy -
👉 Redundancy - Primary & Secondary Server
👉 Replication - Data replication from active to mirrored database     
(f) Databases
👉 SQL - Sharding, Indexes, master-slave, master-master, Denormalization
👉 NoSQL - Key-Value, Document, Wide-Column, Graph
(g) Communication Protocols and standards like - IP, TCP, UDP, HTTP/S, RPC, REST, Web Sockets

✅ 𝐑𝐞𝐬𝐨𝐥𝐯𝐞 𝐛𝐨𝐭𝐭𝐥𝐞𝐧𝐞𝐜𝐤𝐬 𝐚𝐧𝐝 𝐟𝐨𝐥𝐥𝐨𝐰-𝐮𝐩 𝐪𝐮𝐞𝐬𝐭𝐢𝐨𝐧𝐬 (2-3 𝐦𝐢𝐧𝐮𝐭𝐞𝐬)

![img.png](img.png)

𝐇𝐚𝐯𝐞 𝐲𝐨𝐮 𝐞𝐯𝐞𝐫 𝐰𝐨𝐫𝐤𝐞𝐝 𝐨𝐫 𝐛𝐮𝐢𝐥𝐭 𝐚 12-𝐟𝐚𝐜𝐭𝐨𝐫 𝐚𝐩𝐩?
The 12-factor app methodology is a set of best practices for building modern software applications that are scalable, maintainable, and easily deployable. It was originally introduced by Heroku and has since become a widely adopted standard in the software development community. The 12 factors are as follows:

1. Codebase: A single codebase tracked in a version control system, with multiple deployments.
2. Dependencies: Explicitly declare and isolate dependencies. Use dependency management tools.
3. Config: Store configuration in the environment, separate from the code.
4. Backing services: Treat backing services (databases, caches, etc.) as attached resources that can be easily swapped.
5. Build, release, run: Strictly separate the build, release, and run stages of the application lifecycle.
6. Processes: Run the application as one or more stateless processes.
7. Port binding: Export services via port binding. Applications should be self-contained and not rely on runtime injection of a web server.
8. Concurrency: Scale out via the process model. Applications should be able to scale horizontally.
9. Disposability: Maximize robustness with fast startup and graceful shutdown. Processes should be able to start and stop quickly.
10. Dev/Prod parity: Keep development, staging, and production environments as similar as possible.
11. Logs: Treat logs as event streams and provide centralized logging. Application should not concern itself with routing or storage of its output stream.
12. Admin processes: Run admin/management tasks as one-off processes. They should be run in the same environment as the app, with access to the same config and code.
![img_1.png](img_1.png)

![img_2.png](img_2.png)

The template covers following points -
- Load Balancing
- API Gateway
- Communication Protocols
- Content Delivery Network (CDN)
- Database
- Cache
- Message Queue
- Unique ID Generation
- Scalability
- Availability
- Performance
- Security
- Fault Tolerance and Resilience
- and many more

# 𝐌𝐮𝐬𝐭-𝐤𝐧𝐨𝐰 𝐄𝐬𝐭𝐢𝐦𝐚𝐭𝐢𝐨𝐧𝐬 𝐟𝐨𝐫 𝐚 𝐒𝐲𝐬𝐭𝐞𝐦 𝐃𝐞𝐬𝐢𝐠𝐧 𝐈𝐧𝐭𝐞𝐫𝐯𝐢𝐞𝐰 🔥

In a system design interview, the chances are high that you will be asked to estimate QPS (Queries per second) and Storage of the System using a back-of-the-envelope estimation.

There are various scales and numbers, and anyone appearing for System Design Interview must know them. Save the attached pdf and refer to it for the Back-of-the-envelope estimations.

# An Object Oriented Design Interview lasts for 45-60 mins. The following template will guide you on the approach-

✅Requirement Gathering (3-5 mins)

The OOD questions are often abstract & vague. Always ask clarifying questions to understand the exact scope of the problem that the interviewer has in mind.
👉Use cases to cover (MVP)
👉Who will use the system (Actors)

Let's design an online shopping site. Few requirements would be-
Users should be able to-
👉search products
👉view/buy products
👉add/remove product items in their shopping cart
👉place an order
👉get notifications about order
👉pay through diff modes

✅Use Cases (3-6 mins)

You are expected to find possible actors of the system & write down different use cases system will support.

👉Actors - Customer,Admin,System

Some of the use cases could be -
👉Search products
👉Add/remove products in the cart
👉Check out to buy items in the cart
👉Make payment to place an order
👉Send notification

✅Identify the Core classes (3-6 mins)

After gathering requirements & drafting a few use cases, our understanding of the system becomes clear. Now we should consider what could be the main classes of the system. You are expected to draw a class diagram or write down class names.

👉Nouns in the requirements are possible candidates for Classes like-

👉Product
👉Item
👉User
👉ShoppingCart
👉Order
👉Payment
👉Notification

✅Identify the fields of each class (5-10 mins)

Once we know the core classes of the system, it is expected to draw a class diagram along with class fields. Take each class & add a few imp fields which drive the use cases.
eg.
👉Product
-name
-desc
-price

👉User
-name
-email
-phone
etc

✅Identify the Relationship between the classes (5-10 mins)

Once we know the core classes of the system, it is expected to draw what is the relationship between the classes. The different types of relationships to draw/write are-

👉Inheritance (is-a)
👉Association (has-a) one-one, one-many, many-many

eg.
👉Customer & Admin inherit from User
👉Customer has One Shopping Cart
👉Shopping Cart has Many Items
etc

✅Identify the Actions of the classes (5-10 mins)

Once we are clear with the requirements, use cases & possible design of the system, it is time to identify the different actions classes will perform based on their relationship. Verbs in the requirements are possible candidates for action. They are methods of the classes like-
Customer can -
👉add the item to the cart- addItemToCart(Item item)
👉place an order- placeOrder(Order order)
etc

✅ Code (5-8 mins)

The class diagram will give you an idea about the class's name, fields, & methods. You are expected to write code for the methods which fulfill the use case interviewer wants or any algo/data structure which handles certain use cases.

✅Resolve bottlenecks and Follow-up questions (3-4 mins)



