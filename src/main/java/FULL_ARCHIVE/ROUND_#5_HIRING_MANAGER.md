

##Behavioral ::::: Technical Program Manager


#1.Tell me about a time when you worked on a project with a tight deadline.

#2. If you have to work on 5 different projects, how do you prioritize?
I will use below 4 pillars to rate and prioritize those 5 projects:

Analyze the impact of those 5 projects, to our team and to the whole company maybe
Find out if those 5 projects have any dependency between each other
Clarify the timeline of each project
Identity the status of resources to work on those 5 projects and also the status of their dependencies

or
 
 I will start by putting projects in different buckets:
 
 Urgency bucket - rate them based on urgency of the users waiting on them
 Importance bucket - criticality of each project
 Budget status - which projects are at what stage of budget approval
 Cost vs Benefit - measure business value, ROI etc.,
 Strategic alignment - rank projects in terms of their alignment with organisational vision
 Putting the project in such buckets will most often give out clear winner or priority order. If there is still some conflict, then start adding weightage to each criteria and use the weighted average mechanism to understand the value of each project. This should resolve any conflict.
 
#3. Tell me about a time you were wrong.