Q2. What is a wireless in AWS Services?

Q3. Which tool using in software testing ?

Q5. How would you describe AWS to a customer?

Q6. What are the different internet security vulnerabilities known to you

Q7. Migration to cloud

Q9. How to Secure a web server

Q10. Tell me a time when you solved a complex problem


             HTTP Status Codes At one Place 

1xx Informational
100 Continue: Server agrees to continue with the client's request.
101 Switching Protocols: Server switches protocols as requested by the client.
102 Processing: Request received, and the server is continuing processing.

2xx Success
200 OK: The request was successful.
201 Created: Request resulted in a new resource created.
202 Accepted: Request accepted, but processing is not complete.
203 Non-Authoritative Information: Request processed, but from a third-party source.
204 No Content: Request processed successfully with no additional content.
205 Reset Content: Clear input fields for user form.

3xx Redirection
300 Multiple Choices: Multiple options available; user-agent can select one.
301 Moved Permanently: Resource moved permanently to a new location.
302 Found (or 303 See Other): Resource temporarily at a different location.
304 Not Modified: Resource not modified since last requested.
307 Temporary Redirect: Requested page temporarily resides under a different URL.

4xx Client Errors
400 Bad Request: Server cannot process the request due to client error.
401 Unauthorized: Authentication required for access.
402 Payment Required: Payment needed for further access.
403 Forbidden: Server understands the request but refuses to authorize.
404 Not Found: Requested resource not found.
405 Method Not Allowed: Request method not supported.
406 Not Acceptable: Requested resource cannot generate the response meeting the list of acceptable values.
407 Proxy Authentication Required: Proxy needs authentication.
408 Request Timeout: Server timed out while waiting for a request.
409 Conflict: Request conflicts with the current state of the server.
410 Gone: Requested resource is no longer available.
411 Length Required: Content-Length not defined.
412 Precondition Failed: Precondition in the request header evaluated to false.
413 Payload Too Large: Request entity too large.
414 URI Too Long: URI requested is too long.
415 Unsupported Media Type: Request entity has a media type not supported by the server.
416 Range Not Satisfiable: Requested range not available.
417 Expectation Failed: Expectation specified in the Expect header cannot be met.

5xx Server Errors
500 Internal Server Error: Generic error message for an unexpected condition.
501 Not Implemented: Server does not support the functionality.
502 Bad Gateway: Server acting as a gateway received an invalid response.
503 Service Unavailable: Server temporarily unable to handle the request.
504 Gateway Timeout: Server acting as a gateway did not receive a timely response.
505 HTTP Version Not Supported: Server does not support the HTTP protocol version used.