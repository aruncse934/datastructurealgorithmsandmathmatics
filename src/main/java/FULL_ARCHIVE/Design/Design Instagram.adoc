
= **Day 4 of System Design Case Studies Series : **Design Instagram, Robinhood, Swiggy, CashApp, Google Duo, Audible, Pokemon Go, Calm

image:https://miro.medium.com/v2/resize:fit:1400/1*KvaWR_vh6MvhUSYsP_P5lw.png[image]

== System Design = Object Oriented Design  and Products/ API Design

== What is Instagram?

image:https://miro.medium.com/v2/resize:fit:1200/0*aFdOyZLQHgAzP3ey[image]

Instagram is a social networking platform where users can —

. [#568f]#Upload and share Photos and videos#
. [#70ea]#Follow other users#
. [#233e]#Chat with other people#
. [#f01f]#Can control the visibility of their content by making it public ( accessible to everyone) or private ( accessible to the people who follow)#
. [#ad89]#Create stories#
. [#bcd3]#Tag another user/location in the post ( photo/video)#
. [#ad42]#Watch the feed of other users they follow.#

Some key components of the Instagram design would include:

. [#1de7]#_User accounts: Users would need to be able to create accounts and log in to the application. This would involve designing a registration and login system, as well as a way to securely store user information such as passwords._#
. [#a880]#_Profile creation: Users would need to be able to create and edit their own profiles, which would include things like a profile picture, bio, and contact information._#
. [#a820]#_Feed: The main feature of Instagram is the feed, where users can view posts from the people they follow. The feed would need to be designed to show posts in reverse chronological order and include features like pagination to handle a large number of posts._#
. [#0e2f]#_Posting: Users would need to be able to create and post new content, including photos and videos, as well as add captions, tags, and locations._#
. [#ee2a]#_Search: A search feature would need to be implemented so that users can find other users and posts by searching for keywords or hashtags._#
. [#ef3f]#_Notifications: Users would need to be notified of new posts from the people they follow, comments on their posts, and other interactions on the platform._#
. [#69c2]#_Direct messaging: Users would need to be able to send direct messages to other users and view their message history._#
. [#c35b]#_Scalability: Instagram needs to be able to handle a large number of users and high traffic loads. This would involve designing the application to be scalable, including using technologies such as load balancers and distributed systems._#
. [#64f7]#_Security: Instagram would need to be designed with security in mind, to protect user data and prevent unauthorized access._#
. [#2448]#_Mobile and web: Instagram should be designed for web and mobile platforms so that it can be used on any device._#
. [#5800]#_Analytics: The Instagram platform would need to be designed to track usage and engagement metrics, including user engagement, post engagement, and other key performance indicators._#

== 1. Important Features

We will pick most important features based on the functionality of the instagram.

____
*Upload Images*

*Follow other instagram users*

*Like and comments*

*Generate feed for the users*
____

== 2. Scaling Requirements

Let’s say, we have —

No of photos uploaded by each user/month = 3

No of active users/month = 30 million

Size of each Photo : 10 MB

*Total = 30⁷ * 10 * 3 = 900 TB of storage is what we need every month*

Storage space ( for photos) needed for 5 years :

*900 * 12 * 5 = 54 PB*

_Assumption : I have taken a very small scale just to keep things simple ( in reality insta has more than 2 billion users)._

For example, if the current Instagram system can handle 10 million users and the expected growth rate is 10% per month, the scaling requirements can be calculated as follows:

* [#758e]#*_After 1 month: 11 million users_*#
* [#09ba]#*_After 2 months: 12.1 million users_*#
* [#e609]#*_After 3 months: 13.31 million users_*#
* [#d5ad]#*_After 1 year: 19.1 million users_*#

== 3. Data Model-Entity Relationship

We will be using NoSQL databases to store the photo data in the form of a key value store as well capture the relationship between the different entities. We need high reliability wrt data.

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Users

*Fields -*

____
Username : String

Email : String

Password : String
____

*Functionality —*

Users can post photos and location to the photos.

Users can follow other people and have followers.

Users can get the feed of other people they are following

Users can like and comment photos

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Follow

____
User_id1 : Int

User_id2 : Int
____

=== Functionality —

User 1 can follow user2

User 2 can follow user1

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Post — Photos

Fields —

____
PostId : Int

User_id : Int ( Foreign key from users table)

Photo Url : String

Caption: String

Post_Timestamp: DateTime

Location : String
____

Functionality —

Post — Photos are used to generate feed

Post — Photos can be captioned and timestamped

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Likes

____
Like_id : Int

User_id:Int

Post_id: Int

Timestamp: DateTime
____

*Functionality —*

Users can like the post-photos

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Comment

____
Comment_id : Int

Post_id: Int

User_id: Int

Caption_text : String

Timestamp: DateTime
____

*Functionality —*

Users can comment on the post-photos

Users can also reply to the prior comments

image::https://miro.medium.com/v2/resize:fit:1400/1*v44ZDyC0WXbEzZjPY6-qCA.png[Data Model,width=700,height=1360]


== 4. High Level Design

=== Assumptions

. [#70e7]#There will be more reads than writes so we need to design read heavy system — More Slaves replicas ( where we can perform read operations fast)#
. [#9ba2]#We will be scaling horizontally (scale — out).#
. [#9649]#Services should be highly available.#
. [#23c2]#Latency should be ~350ms for the feed generation.#
. [#9dd4]#Consistency vs Availability vs Reliability: Availability and Reliability are more important than consistency in this case.#
. [#de85]#The system is read heavy ( people see photos more than posting)#

image::https://miro.medium.com/v2/resize:fit:1400/1*LdqbbAH_H376b3EskE-HMA.png[Pic copyright and credits : Naina Chaturvedi,width=700,height=546]

— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —

=== Main Components —

Mobile Client : These are users accessing instagram

Application Servers : Read, write and notification server

Load Balancer : Route and direct the requests to the required server performing designated service.

Cache (Memcache) : It’s a massive system to serve million of users fast. We will be caching the data based on LRU.

CDN : To improve the latency and throughput.

Database : To store the data based on the Data model above and fetch data from the Storage using the required path.

Storage (HDFS or Amazon S3) : To upload and store the photos.

=== *Here is a high-level overview of how Instagram works :*

. [#16df]#_Front-end: The front-end of Instagram is built using React Native and is responsible for handling user interactions and displaying the user interface. It communicates with the back-end through APIs to request data or perform actions such as posting a new photo._#
. [#b7cd]#_Back-end: The back-end of Instagram is built using a combination of technologies including Python, Django, and PostgreSQL. It acts as the server-side component that receives requests from the front-end and processes them._#
. [#c828]#_Database: Instagram uses a relational database management system, such as PostgreSQL, to store user data, including user profiles, photos, and captions. The database is optimized for fast read and write operations to handle the large number of requests made by users._#
. [#616d]#_Content Delivery Network (CDN): Instagram uses a CDN to store and distribute media files, such as photos and videos, to users. The CDN is used to reduce the load on the back-end servers and improve the overall performance and speed of the system._#
. [#d507]#_Image Processing: Instagram uses image processing algorithms to handle the processing of photos uploaded by users. This includes tasks such as resizing, cropping, and filtering images._#
. [#154b]#_Notifications: Instagram uses a notification system to inform users of various events, such as when someone likes their photo or starts following them. The notifications are sent in real-time and can be received on the front-end through push notifications or through email._#
. [#9a03]#_Search: Instagram provides a search feature that allows users to find other users, photos, and hashtags. The search feature is powered by a combination of algorithms that consider various factors, such as the relevance and popularity of the content._#
