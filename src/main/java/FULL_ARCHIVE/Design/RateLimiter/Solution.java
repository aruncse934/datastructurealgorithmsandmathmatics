package FULL_ARCHIVE.Design.RateLimiter;

import java.util.*;

/*Description
Implement a rate limiter, provide one method: is_ratelimited(timestamp, event, rate, increment).

timestamp: The current timestamp, which is an integer and in second unit.
event: The string to distinct different event. for example, "login" or "signup".
rate: The rate of the limit. 1/s (1 time per second), 2/m (2 times per minute), 10/h (10 times per hour), 100/d (100 times per day). The format is [integer]/[s/m/h/d].
increment: Whether we should increase the counter. (or take this call as a hit of the given event)、
The method should return true or false to indicate the event is limited or not.
Note: Login failure is not recorded in login times.

Example
Example 1

Input:
is_ratelimited(1, "login", "3/m", true)
is_ratelimited(11, "login", "3/m", true)
is_ratelimited(21, "login", "3/m", true)
is_ratelimited(30, "login", "3/m", true)
is_ratelimited(65, "login", "3/m", true)
is_ratelimited(300, "login", "3/m", true)

Output:
false
false
false
true
false
false*/

public class Solution {
    /*
     * @param timestamp: the current timestamp
     * @param event: the string to distinct different event
     * @param rate: the format is [integer]/[s/m/h/d]
     * @param increment: whether we should increase the counter
     * @return: true or false to indicate the event is limited or not
     */
    private HashMap<String, List<Integer>> hashmap;
    private HashMap<String, Integer> timeMapping;

    public Solution () {
        hashmap = new HashMap<String, List<Integer>>();
        timeMapping = new HashMap<String, Integer>();
        timeMapping.put("s", 1);
        timeMapping.put("m", 60);
        timeMapping.put("h", 3600);
        timeMapping.put("d", 86400);
    }

    public boolean isRatelimited(int timestamp, String event, String rate, boolean increment) {
        if(rate == null || rate.length() == 0 || timestamp <= 0) {
            return false;
        }
        String[] splits = rate.split("/");
        int times = Integer.parseInt(splits[0]);
        int timelimit = timeMapping.get(splits[1]);

        hashmap.putIfAbsent(event, new ArrayList<Integer>());
        int startTime = timestamp - timelimit + 1;

        // count how many elements >= startTime;
        int count = countTimes(hashmap.get(event), startTime);

        boolean islimited = count >= times;
        if(increment && !islimited) {
            hashmap.get(event).add(timestamp);
        }
        return islimited;
    }

    private int countTimes(List<Integer> list, int target) {
        if(list.size() == 0 || list.get(list.size() -1) < target) {
            return 0;
        }
        int start = 0; int end = list.size() - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(list.get(mid) < target) {
                start = mid;
            } else {
                end = mid;
            }
        }

        if(list.get(start) >= target) {
            return list.size() - start;
        } else {
            return list.size() - end;
        }
    }
}