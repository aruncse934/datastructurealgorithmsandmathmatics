package FULL_ARCHIVE.Design.RateLimiter;
  //standard behavioral questions like why Cisco? , why are you looking for change?, How can I trust you as you are switching in a very short time ? etc.,
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

 class RatelimiterServic {   // RateLimiterService
    public static void main(String[] args) {
        RatelimiterServic service = new RatelimiterServic();
     int limit = 5;
     RateLimit rateLimit = new RateLimit(limit);
              new RateLimitHelper("User A :",rateLimit).start();
              new RateLimitHelper("User b :",rateLimit).start();
    }
}
 class RateLimit{
    private int rateLimit;
     Map<String, LinkedList<Request>> userReqMap = new ConcurrentHashMap<>();

    public RateLimit(int rateLimit) {
        this.rateLimit = rateLimit;
    }


     public synchronized boolean hit(String user, Instant ts) {
        if(!userReqMap.containsKey(user)){
            return addNewUser(user);
        } else {
            if(getTotalElpasedReqs(user) < rateLimit){
                LinkedList<Request> requests = userReqMap.get(user);
                requests.add(new Request(ts,1));
                userReqMap.put(user,requests);
                return true;
            } else {
                boolean actionTaken = false;
                for(int i = 0; i < userReqMap.get(user).size(); i++){
                    Duration duration = Duration.between(userReqMap.get(user).get(i).getTimestamp(), ts);
                    if(duration.getSeconds() >= 60){
                        userReqMap.get(user).remove(i);
                        actionTaken = true;
                    } else {
                        break;
                    }
                }
                if(actionTaken){
                  LinkedList<Request> requests = userReqMap.get(user);
                  requests.add(new Request(ts,1));
                  userReqMap.put(user,requests);
                  return true;
                }
                return false;
            }
        }
     }

     public boolean addNewUser(String user){
        LinkedList<Request> requests = new LinkedList<>();
        requests.add(new Request(Instant.now(),1));
        userReqMap.put(user,requests);
         System.out.println("New User added !! "+user);
         return true;
     }


     public Integer getTotalElpasedReqs(String user){
         return userReqMap.get(user).stream().mapToInt(Request::getCount).sum();
     }

 }
class RateLimitHelper extends Thread{
    RateLimit rateLimit;

    public RateLimitHelper(String user, RateLimit rateLimit) {
        super(user);
        this.rateLimit = rateLimit;
    }

    @Override
    public void run() {
       for(int i =1; i <= 65; i++){
           System.out.println("Thread Name - "+getName()+", Time - "+ i + ", rate Limit : "+hit(getName(), Instant.now()));
           try{
               Thread.sleep(1000);

           }   catch (InterruptedException e){
               e.printStackTrace();
           }
           System.out.println("DONE! : " +getName());
       }
    }

    private boolean hit(String user, Instant ts){
        return rateLimit.hit(user, ts);
    }


}
@Getter
@Setter
class Request{
    private Instant timestamp;
    private Integer count;

    public Request(Instant timestamp, Integer count) {
        this.timestamp = timestamp;
        this.count = count;
    }
}






