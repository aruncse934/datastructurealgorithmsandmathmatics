package FULL_ARCHIVE;

import java.util.Arrays;

public class ConvertStrToStrArray {

    //regex
    public static String[] singleChars(String s){
        return s.split("(?!^)");
    }

    public static void main(String[] args) {
        String str = "Hi there, nice to meet you!";

        //re
        String[] strArray = str.split("");
        System.out.println(Arrays.toString(strArray));

        //re
        String[] myArray = str.split("[-,.!;?]\\s*");
        System.out.println(Arrays.toString(myArray));

        //re
        String[] array = str.split("\\W+");
        System.out.println(Arrays.toString(array));

        //method call reg
        System.out.println(Arrays.toString(singleChars(str)));

        //example
        String FLIGHT_INPUT = "20221018LH720FRAPEK";
        String dateStr = FLIGHT_INPUT.substring(0,8);
        String flightNo = FLIGHT_INPUT.substring(8,FLIGHT_INPUT.length()-6);
        int airportStart = dateStr.length() + flightNo.length();
        String from = FLIGHT_INPUT.substring(airportStart,airportStart+3);
        String to = FLIGHT_INPUT.substring(airportStart+3);

        String[] arr = new String[]{dateStr, flightNo, from, to};
        System.out.println(Arrays.toString(arr));
    }
}
