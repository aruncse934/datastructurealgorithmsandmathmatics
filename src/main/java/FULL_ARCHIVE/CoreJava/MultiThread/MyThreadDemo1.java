package FULL_ARCHIVE.CoreJava.MultiThread;


// Thread creation by extending the thread class
 class MyThreadDemo1 extends Thread {
    public  void run(){
        try{
            System.out.println("Thread"+ Thread.currentThread().getName()+ "is Running");

        }   catch (Exception e){
            System.out.println("Exception is caught");
        }
    }
    public static void main(String[] args) {
         int n = 10;
         for(int i = 0; i<n;i++){
             MyThreadDemo1 demo1=new MyThreadDemo1();
             demo1.start();
         }
    }
}


// Thread creation  by implementing the Runnable interface

class MultiThreadDemo2 implements Runnable{

    @Override
    public void run() {
         try{
             System.out.println("Runnable Thread"+ Thread.currentThread().getName()+"is Running");

         }   catch (Exception e){
             System.out.println("Exception is caught");
         }
    }
    public static void main(String[] args) {
         int n =10;
         for(int i =0; i<n; i++){
             Thread t = new Thread(new MultiThreadDemo2());
             t.start();
         }
    }

}

// Thread states Example
class TestThread implements Runnable{

    @Override
    public void run() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e){
              e.printStackTrace();
        }
        System.out.println("State of thread while it called join() method on thread2 - "+Test.thread1.getState());

        try{
            Thread.sleep(200);
        }  catch (InterruptedException e){
            e.printStackTrace();
        }

    }

}

class Test implements Runnable{
     public static Thread thread1;
     public static Test obj;
    @Override
    public void run() {
         TestThread thread = new TestThread();
         Thread t  = new Thread(thread);

         // thread1 created and is currently in the new state
        System.out.println("State of thread2 after creating it - " +t.getState());
        t.start();

        // thread 2 moved to Ruunable state
        System.out.println("  state of thread2 after calling .start() method on it  -"+ t.getState());
        
        // moving thread1 to  timed waiting state

        try {
            Thread.sleep(200);
        }   catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println("state of thread 2 after caling  .sleep method on it -"+ t.getState());

        try{
            t.join();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        System.out.println("State of thread2 when it has finished it's excution -"+ t.getState());
    }


    public static void main(String[] args) {
        obj = new Test();
        thread1 = new Thread(obj);
        // thread1 created and is currently in the new state
        System.out.println("state of thread1 after creating it-"+ thread1.getState());
        thread1.start();

        //thread1 moved to runnable state
        System.out.println("State of thread1 after calling .start() method on it -"+thread1.getState());


    }
}

//  Priorities in Multithreading via help of getPriority and setPriority() method
class ThreadDemo extends Thread{
     
}



/*// Java Program to Illustrate Priorities in Multithreading
// via help of getPriority() and setPriority() method

// Importing required classes
import java.lang.*;

// Main class
class ThreadDemo extends Thread {

	// Method 1
	// run() method for the thread that is called
	// as soon as start() is invoked for thread in main()
	public void run()
	{
		// Print statement
		System.out.println("Inside run method");
	}

	// Main driver method
	public static void main(String[] args)
	{
		// Creating random threads
		// with the help of above class
		ThreadDemo t1 = new ThreadDemo();
		ThreadDemo t2 = new ThreadDemo();
		ThreadDemo t3 = new ThreadDemo();

		// Thread 1
		// Display the priority of above thread
		// using getPriority() method
		System.out.println("t1 thread priority : "
						+ t1.getPriority());

		// Thread 1
		// Display the priority of above thread
		System.out.println("t2 thread priority : "
						+ t2.getPriority());

		// Thread 3
		System.out.println("t3 thread priority : "
						+ t3.getPriority());

		// Setting priorities of above threads by
		// passing integer arguments
		t1.setPriority(2);
		t2.setPriority(5);
		t3.setPriority(8);

		// t3.setPriority(21); will throw
		// IllegalArgumentException

		// 2
		System.out.println("t1 thread priority : "
						+ t1.getPriority());

		// 5
		System.out.println("t2 thread priority : "
						+ t2.getPriority());

		// 8
		System.out.println("t3 thread priority : "
						+ t3.getPriority());

		// Main thread

		// Displays the name of
		// currently executing Thread
		System.out.println(
			"Currently Executing Thread : "
			+ Thread.currentThread().getName());

		System.out.println(
			"Main thread priority : "
			+ Thread.currentThread().getPriority());

		// Main thread priority is set to 10
		Thread.currentThread().setPriority(10);

		System.out.println(
			"Main thread priority : "
			+ Thread.currentThread().getPriority());
	}
}
*/