package FULL_ARCHIVE.CoreJava;

import java.util.Arrays;
import java.util.List;

class Java17Features {

     //Text Blocks
     private static void  jsonBlock(){
         String text = """
                 {
                 "name" : " John Doe",
                 "age"  : 45,
                 "address" : " HSR Layout, 31/5, JavaTown"
                 }
                 """;
         System.out.println(text);
     }

     //
     private static void sqlStatement() {
         String sql = """
    SELECT id, firstName, lastName\s\
    FROM Employee
    WHERE departmentId = "IT" \
    ORDER BY lastName, firstName""";
         System.out.println(sql);
     }

     //Improved Switch Statements
     /* private static void improvedSwitch(Fruit fruit){
         String text = switch (fruit){
             case APPLE,PEAR -> {
                 System.out.println("the given fruit was:" + fruit);
                 yield "Common fruit";
             }
             case ORAANGE, AVOCADO -> "Exotic fruit";
             default -> "Undefined fruit";
         };
          System.out.println(text);
      }
*/
    public static void main(String[] args) {
          jsonBlock();
          sqlStatement();

          List<String> list = Arrays.asList("Hello", "world");
          list.forEach(System.out::println);
    }

     Person person = new Person("Arun", 30);


 }
record Person(String name, int age){

}


 sealed class Shape permits Circle, Squares, Triangle {
    // class body
}

sealed interface Shapes permits Circles, Rectangles, Triangles {}

final class Circles implements Shapes {}

final class Rectangles implements Shapes {
}

final class Triangles implements Shapes {
}






/*
-------------------------------------------
Pattern Matching for Switch (Preview Feature):
Allows the use of patterns in switch statements, enabling more concise and readable code.

record Point(int x, int y) {}

Point point = new Point(10, 20);
int result = switch (point) {
    case Point p && (p.x() == 0 || p.y() == 0) -> 0;
    case Point p && (p.x() == p.y()) -> 1;
    case Point p -> 2;
    default -> -1;
};
 -------------------------------------------

Records:
Provides a concise syntax for declaring classes that are transparent holders for immutable data.

record Person(String name, int age) {}

Person person = new Person("Alice", 30);

  -------------------------------------------
Sealed Classes (Second Preview):
Allows the restriction of the subclasses that can extend a class or implement an interface.

sealed interface Shape permits Circle, Rectangle, Triangle {}

final class Circle implements Shape {}

-------------------------------------------

Local Variable Type Inference Enhancements:
Extends the var keyword to lambda expressions, anonymous classes, and more.

var list = new ArrayList<String>();


-------------------------------------------

 Text Blocks:
Introduces a multi-line string literal to make it easier to write and read multiline strings.

java
Copy code
String html = """
              <html>
                  <body>
                      <p>Hello, world!</p>
                  </body>
              </html>
              """;


*/






/*private static void improvedSwitch(Fruit fruit) {
    String text = switch (fruit) {
        case APPLE, PEAR -> {
            System.out.println("the given fruit was: " + fruit);
            yield "Common fruit";
        }
        case ORANGE, AVOCADO -> "Exotic fruit";
        default -> "Undefined fruit";
    };
    System.out.println(text);
}
*/
