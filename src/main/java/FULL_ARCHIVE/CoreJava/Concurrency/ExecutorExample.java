package FULL_ARCHIVE.CoreJava.Concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ExecutorExample {
     public static void main(String[] args) {
         System.out.println("Executors Example");
     }
}

class ObjectNewFixedThreadPoolExample{
    public static void main(String[] args) {
      //creating a fixed-size thread pool with 3 threads
      ExecutorService executorService = Executors.newFixedThreadPool(3);

      //submitting tasks to the executor
        executorService.submit(()-> System.out.println("Task 1 executed"));
        executorService.submit(() -> System.out.println("Task 2 executed"));
        executorService.submit(() -> System.out.println("Task 3 executed"));

        //shutting down the executor
        executorService.shutdown();
    }
    
}

class ObjectNewFixedThreadPoolExample1{
    public static void main(String[] args) {
        //creating a fixed-size thread pool with 3 threads
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        //submitting tasks to the executor
        for(int i = 0; i < 5; i++) {
            int taskId = i;
            executorService.execute(() -> System.out.println("Task " +taskId + " executed by  thread "+Thread.currentThread().getName()));

        }

        //shutting down the executor
        executorService.shutdown();
    }

}
class ObjectNewSingleThreadExecutorExample{
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(()-> System.out.println("Task 1 Executed"));
        executorService.submit(()-> System.out.println("Task 2 Executed"));
        executorService.submit(()-> System.out.println("Task 3 Executed"));
        executorService.shutdown();
    }
}

class ObjectNewCachedThreadPoolExample{
    public static void main(String[] args) {
        //creating aa cached thread pool
        ExecutorService executors = Executors.newCachedThreadPool();

        //submitting tasks to the executor
        for(int i = 0; i< 10;i++){
            final int tasknumber = i;
            executors.submit(()-> System.out.println("Task "+ tasknumber + " executed by thread " + Thread.currentThread().getName()));
        }

        //shutting down the executor
        executors.shutdown();
    }
}

