package FULL_ARCHIVE.CoreJava.Concurrency;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureExample {
    public static void main(String[] args) {
        //creating a thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        //submitting a task that returns a future
        Future<Integer> future = executorService.submit(() ->{
            Thread.sleep(2000);
            return 289;
        });

        //do other work while waiting for the computation to complete
        System.out.println("Doing something else while waiting...");

        //Retrieving the result of the computation

        try {
            Integer result = future.get();
            System.out.println("Result: "+result);
        }catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
        }

        //shutting down the executor
        executorService.shutdown();
    }
    
}
