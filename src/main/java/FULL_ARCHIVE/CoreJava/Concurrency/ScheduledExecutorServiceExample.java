package FULL_ARCHIVE.CoreJava.Concurrency;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorServiceExample {
    public static void main(String[] args) {
        System.out.println("ScheduledExecutorService Example");
    }
}

class ObjectNewSingleThreadScheduledExecutorExample{
    public static void main(String[] args) {
        //creating a scheduledExecutorServices with a single thread
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        //scheduling aa task to run after a delay
        executorService.schedule(() -> System.out.println("Delayed task executed"), 5, TimeUnit.SECONDS);
        
        //scheduling  a task to run periodically
        executorService.scheduleAtFixedRate(() -> System.out.println("Periodic task executed"), 1,3,TimeUnit.SECONDS);

        //shutting down the executor when all tasks are done
        executorService.shutdown();
    }

}
