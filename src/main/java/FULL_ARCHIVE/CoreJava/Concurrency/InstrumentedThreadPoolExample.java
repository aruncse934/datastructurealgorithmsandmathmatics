package FULL_ARCHIVE.CoreJava.Concurrency;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class InstrumentedThreadPoolExample {
    public static void main(String[] args) throws InterruptedException {
        // Start time of the thread pool
        Instant startTime = Instant.now();

        // Using an AtomicLong to track total service time and total pool time
        AtomicLong totalServiceTime = new AtomicLong(0);
        AtomicLong totalPoolTime = new AtomicLong(0);

        // AtomicIntegers to track the number of requests and completed requests
        AtomicInteger numberOfRequests = new AtomicInteger(0);
        AtomicInteger numberOfRequestsRetired = new AtomicInteger(0);

        // To track inter-request arrival time
        AtomicLong lastRequestTime = new AtomicLong(System.nanoTime());
        AtomicLong aggregateInterRequestArrivalTime = new AtomicLong(0);

        // Create a thread pool with 5 threads
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        // A list to hold Future objects for tracking task completion
        List<Future<?>> futures = new ArrayList<>();

        // Create a runnable task that simulates some work
        Runnable task = () -> {
            long start = System.nanoTime();
            try {
                // Simulate a task taking 1 second to complete
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } finally {
                long end = System.nanoTime();
                // Add the task's service time to the total service time
                totalServiceTime.addAndGet(end - start);
                // Increment the number of requests retired
                numberOfRequestsRetired.incrementAndGet();
            }
        };

        // Submit 10 tasks to the executor service
        for (int i = 0; i < 10; i++) {
            // Track inter-request arrival time
            long currentTime = System.nanoTime();
            aggregateInterRequestArrivalTime.addAndGet(currentTime - lastRequestTime.get());
            lastRequestTime.set(currentTime);

            // Increment the number of requests
            numberOfRequests.incrementAndGet();

            // Submit the task and add the Future to the list
            futures.add(executorService.submit(task));
        }

        // Wait for all tasks to complete
        for (Future<?> future : futures) {
            try {
                future.get();
            } catch (ExecutionException e) {
                System.out.println("Task encountered an exception: " + e.getMessage());
            }
        }

        // Shutdown the executor service
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        // Calculate total pool time
        totalPoolTime.addAndGet(System.nanoTime() - startTime.toEpochMilli() * 1_000_000);

        // Display the metrics
        System.out.println("Start time: " + startTime);
        System.out.println("Total service time (ns): " + totalServiceTime.get());
        System.out.println("Total pool time (ns): " + totalPoolTime.get());
        System.out.println("Number of requests: " + numberOfRequests.get());
        System.out.println("Number of requests retired: " + numberOfRequestsRetired.get());
        System.out.println("Aggregate inter-request arrival time (ns): " + aggregateInterRequestArrivalTime.get());
    }
}
