package FULL_ARCHIVE.CoreJava.Concurrency;

/*The same instance of Foo will be passed to three different threads. Thread A will call first(), thread B will call second(), and thread C will call third(). Design a mechanism and modify the program to ensure that second() is executed after first(), and third() is executed after second().*/
// Using Semaphore
 class Foo{

    public Foo(){

    }
}

 class Print_In_Order {
    public static void main(String[] args) {
      Foo foo = new Foo();
    }

}

