package FULL_ARCHIVE.CoreJava.Concurrency;

import java.util.concurrent.*;

public class TimeTrackingThreadPoolExecutor extends ThreadPoolExecutor {
    // Map to track when tasks were submitted
    private final ConcurrentHashMap<Runnable, Long> timeOfRequest = new ConcurrentHashMap<>();
    private final ThreadLocal<Long> startTime = new ThreadLocal<>();

    public TimeTrackingThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    protected void beforeExecute(Thread worker, Runnable task) {
        super.beforeExecute(worker, task);

        // Set the start time before task execution
        startTime.set(System.nanoTime());
    }

    @Override
    protected void afterExecute(Runnable task, Throwable throwable) {
        super.afterExecute(task, throwable);  // Call the base class's method

        Long requestTime = timeOfRequest.remove(task);  // Retrieve and remove the request time
        if (requestTime != null) {
            long executionTime = System.nanoTime() - requestTime;
            System.out.println("Task executed in " + executionTime + " ns");
        }

        if (throwable != null) {
            System.err.println("Task encountered an error: " + throwable.getMessage());
        }
    }

    @Override
    public void execute(Runnable task) {
        // Store the request time when a task is submitted
        timeOfRequest.put(task, System.nanoTime());
        super.execute(task);  // Submit the task to the thread pool
    }


}

