What Is a Functional Interface? What Are the Rules of Defining a Functional Interface?
----------------------------------------------------
A functional interface is an interface that has only one abstract method. This abstract method is known as the functional method or the single abstract method (SAM).

Functional interfaces are used to implement functional programming concepts in Java, such as lambdas and streams. They allow for more concise and expressive code by eliminating the need for anonymous inner classes.

The rules for defining a functional interface in Java are:

->The interface must have only one abstract method.
->The interface can have any number of default methods and static methods.
->The interface can extend other interfaces, even if they are not functional interfaces, as long as it still has only one abstract method.
->The @FunctionalInterface annotation can be used to enforce the rule that the interface must have only one abstract method. This annotation is optional, but it helps to prevent accidental additions of abstract methods to the interface

 ----------------------------------------------------
Explain the major project you worked and any special contribution made in it

Tell us briefly about the solution design of the project?

Which was your most challenging project?

How ConcurrentHashMap works internally in Java

Define the advantages and disadvantages of the heap compared to a stack?

Write a program to print first non-repeating character in the given string using java8

What is the difference between Authentication and Authorization. How you will implement in Spring Boot application.

What are the challenges you faced while migrating the application from Monolithic to Microservices

Why Functional interface is introduced in java8 if abstract class is already available for alternative solution

Write a Java Program to open all links of gmail.com.

When you will going to use comparable and comparator? explain with real time scenario, have you used in your project

What is the difference between CrudRepository and JPARepository in Spring Boot.

How to handle exception in Spring Boot.

Circuit breaker pattern in Microservices

Do you know how the memory is affected by signed and unsigned numbers?

Given a LinkedList(LL) Node, How would you determine, Whether the given Node is, Single LL, Double LL or Circular LL?

Explain whether a linked list is considered as a linear or non-linear data structure?

Explain how encryption algorithm works?

How @SpringBootApplication annotation works internally, explain the architecture flow

How you will manage response time between inter communication microservices

How you will integrate your spring boot application with Cl/CD pipeline of Jenkins

What is the difference between Authentication and Authorization. How you will implement in Spring Boot application.

Which sorting algorithm is considered the fastest? Why?

How you will manage million of request at a time in microservices

Why and when should I use Stack or Queue data structures instead of Arrays/Lists?

How you would aggregate logs in distributed micro service architecture?

how to override the default configuration in springboot

What is MetaSpace in Java8? How does it differ from PermGen Space

What Is the Difference Between Intermediate and Terminal Operations?

What is double checked locking in Singleton?

How APIs are authenticated in Spring framework?

What’s the difference between @Component, @Controller, @Repository & @Service annotations in Spring?

How to handle exceptions in Spring MVC Framework?

How you would aggregate logs in distributed micro service architecture?
ans: Use a centralized logging service that aggregates logs from each service instance. The users can search and analyze the logs. They can configure alerts that are triggered when certain messages appear in the logs. Example For Centralized logging we can use E[ElasticSearch]L[LogStash]K[Kibana] stack as well

Why do you need the Eureka Server? (Microservices architecture)
Eureka Server is an application that holds the information about all client-service applications. Every Micro service will register into the Eureka server and Eureka server knows all the client applications running on each port and IP address. Eureka Server is also known as Discovery Server.

Tree Data Structure: Depth First v/s Breadth First process 

@all please share below pointers to IG_Ashwini - Sr Java Shubhank (Test Candidates)

Overall test was 1 hour 20 mins

There were two task:- Both were scenario based questions; Task 1 & Task 2

Task 2 - Java Based - Reservation system - (Basically Need to check whether continuous four seats are available or not) - Need to code this.

Depends person to person how we are good in coding.

Overall it depends upon compilation & correctness - if you are facing issue in task 2 in compiling, try to compile it in different platform (leetcode)

Also a suggestion while compiling by default import statements were commented there, we have to the remove import statements, have to  uncomment that after that it should compile properly.