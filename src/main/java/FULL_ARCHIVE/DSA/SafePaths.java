package FULL_ARCHIVE.DSA;

import java.util.ArrayList;
import java.util.List;

public class SafePaths {
    static int count = 0;


    public static void dfs(List<Integer>[] adjList, int[] trees, boolean[] visited, int curr) {
        visited[curr] = true;

        if(trees[curr] == 1) {
            return;
        }

        boolean treeFound = false;

        for(int neighbor : adjList[curr]) {
            if(!visited[neighbor]) {
                dfs(adjList, trees, visited, neighbor);

                if(trees[neighbor] == 1) {
                    treeFound = true;
                }
            }
        }

        if(!treeFound) {
            count++;
        }
    }

    public static int countTreelessCities(List<Integer>[] adjList, int[] trees, int n) {
        boolean[] visited = new boolean[n + 1];
        dfs(adjList, trees, visited, 1);
        return count;
    }

    public static void main(String[] args) {
        int n = 5;
        int k = 2;
        int[] trees = new int[n + 1];
        trees[2] = 1;
        trees[4] = 1;

        List<Integer>[] adjList = new List[n + 1];
        for(int i = 1; i <= n; i++) {
            adjList[i] = new ArrayList<>();
        }

        adjList[1].add(2);
        adjList[2].add(1);

        adjList[1].add(3);
        adjList[3].add(1);

        adjList[3].add(4);
        adjList[4].add(3);

        adjList[3].add(5);
        adjList[5].add(3);

        int treelessCities = countTreelessCities(adjList, trees, n);
        System.out.println("Number of treeless cities that can be visited from city 1: " + treelessCities);
    }

}
