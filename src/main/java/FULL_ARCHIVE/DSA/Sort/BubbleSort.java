package FULL_ARCHIVE.DSA.Sort;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort {

    public static void sortBubble(int[] arr){
        int n = arr.length;
        for(int i = 0; i< n; i++){
            for(int j = 0; j < n;j++){
                if(arr[i] < arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
    public static List<Integer> sortArray(int[] nums) {
        List<Integer> list = new ArrayList<>();
        sortBubble(nums);
        for (int i : nums){
            list.add(i);
        }
        return list;

    }

    public static void main(String[] args) {
        int[] arr = {2,0,2,1,1,0};
        System.out.println(sortArray(arr));
        for(int i = 0; i<arr.length; i++){
            sortBubble(arr);
            System.out.print(arr[i]+" ");
        }

    }
}
