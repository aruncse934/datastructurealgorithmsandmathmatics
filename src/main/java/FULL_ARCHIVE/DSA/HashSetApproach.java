package FULL_ARCHIVE.DSA;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashSetApproach {

    public HashSetApproach() {
     //   map = new HashMap<>();
    }

    public void add(int key) {

    }

    public void remove(int key) {

    }

    public boolean contains(int key) {

        return false;
    }


    // Find Duplicates By Hash Set
   public static boolean findDuplicates(List<Type> keys) {
        // Replace Type with actual type of your key
        Set<Type> hashset = new HashSet<>();
        for (Type key : keys) {
            if (hashset.contains(key)) {
                return true;
            }
            hashset.add(key);
        }
        return false;
    }
       //Contains Duplicate
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int n : nums){
            //if(set.contains(n)) return true;
                //set.add(n);
            if(!set.add(n)) return true;
        }

        return false;
    }
    public static void main(String[] args) {
        System.out.println("**Design HashSet");
        HashSetApproach obj = new HashSetApproach();
         int key =0;
         obj.add(key);
         obj.remove(key);
         boolean param_3 = obj.contains(key);

        System.out.println("** Hash Set - Usage ***");
        // 1. initialize the hash set
        Set<Integer> hashSet = new HashSet<>();
        // 2. add a new key
        hashSet.add(3);
        hashSet.add(2);
        hashSet.add(1);
        // 3. remove the key
        hashSet.remove(2);
        // 4. check if the key is in the hash set
        if (!hashSet.contains(2)) {
            System.out.println("Key 2 is not in the hash set.");
        }
        // 5. get the size of the hash set
        System.out.println("The size of has set is: " + hashSet.size());
        // 6. iterate the hash set
        for (Integer i : hashSet) {
            System.out.print(i + " ");
        }
        System.out.println("are in the hash set.");
        // 7. clear the hash set
        hashSet.clear();
        // 8. check if the hash set is empty
        if (hashSet.isEmpty()) {
            System.out.println("hash set is empty now!");
        }

        System.out.println("Contains Duplicate");
    }
}
