package FULL_ARCHIVE.DSA;

import org.junit.jupiter.api.Test;
import org.testng.internal.collections.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NinjaConding {
   // Two sum
    public static ArrayList<Pair<Integer, Integer>> twoSum(ArrayList<Integer> arr, int target, int n) {
        ArrayList<Pair<Integer, Integer>> result = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < n; i++) {
            int req = target - arr.get(i);
            if(map.getOrDefault(req, -1) != -1) {
                result.add(new Pair(req, arr.get(i)));
                map.put(arr.get(i), -1);
            } else {
                map.put(arr.get(i), i);
            }
        }

        if(result.isEmpty()) {
            result.add(new Pair(-1, -1));
        }

        return result;
    }

    // Maximize Sum
    public static int calculateMaximisedSum(int[] arr1, int[] arr2, int n) {
        /*int maxSum = 0;
        for(int i = 0; i<n;i++){
            maxSum += Math.abs(arr1[i]-arr2[i]);
            if(i < n-1){
                maxSum += Math.abs(arr1[i+1]-arr2[i]);
            }
        }
        return maxSum;*/
        int maxSum = 0;
        for(int i = 0; i<n;i++){
            if(arr1[i] > arr2[i]){
                maxSum += arr1[i];//Math.abs(arr1[i]-arr2[i]);
            }else{
                maxSum += arr2[i];//Math.abs(arr1[i+1]-arr2[i]);
            }
        }
        return maxSum;


    }
    			


    public static void main(String[] args) {

        //// Maximize Sum
        int[] arr1 = {2,3,4,1};
        int[] arr2 = {2,3,1,1};

        System.out.println(calculateMaximisedSum(arr1,arr2, 4));
    }

    @Test
    void testTwoSum() {
        ArrayList<Integer> arr1 = new ArrayList<>(Arrays.asList(4, 9));
        ArrayList<Integer> arr2 = new ArrayList<>(Arrays.asList(2, 7, 11, 13));
        ArrayList<Integer> arr3 = new ArrayList<>(Arrays.asList(2));

        ArrayList<Pair<Integer, Integer>> result1 = twoSum(arr1, 3, arr1.size());
        ArrayList<Pair<Integer, Integer>> result2 = twoSum(arr2, 1, arr2.size());
        ArrayList<Pair<Integer, Integer>> result3 = twoSum(arr3, 4, arr3.size());

        assertEquals(0, result1.size()); // No pairs found for test case 1
        assertEquals(2, result2.size()); // Two pairs found for test case 2
        assertEquals(0, result3.size()); // No pairs found for test case 3
    }
}
