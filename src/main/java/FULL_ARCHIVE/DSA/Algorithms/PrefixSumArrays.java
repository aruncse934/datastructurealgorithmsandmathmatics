package FULL_ARCHIVE.DSA.Algorithms;

public class PrefixSumArrays {

    public static int equilibriumPoint(long arr[], int n) {
        if(n==0) return -1;
        int totalSum = 0;
        int leftSum = 0;
        for(long num : arr){
            totalSum += num;
        }
        for(int i = 0; i< n; i++){
            totalSum -=arr[i];
            if(leftSum == totalSum){
                return i+1;
            }
            leftSum += arr[i];
        }
        return -1;
    }

    //n = 5
    //A[] = {1,3,5,2,2}
    //Output:
    //3 
}
