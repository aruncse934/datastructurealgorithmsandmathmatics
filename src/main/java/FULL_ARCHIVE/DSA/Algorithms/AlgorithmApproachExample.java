package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmApproachExample {

    // Contains Duplicate
    //Approach 1: Brute Force
    public boolean containsDuplicate(int[] nums){
        for(int i = 0; i< nums.length-1;i++){
            for(int j = i+1; j < nums.length;i++){
                if(nums[i] == nums[j]) return true;
            }
        }
        return false;
    }

    //Approach 2: Using Hash Set
    public boolean containsDuplicateHashSet(int[] nums){
        Set<Integer> set = new HashSet<>();
        for(int i : nums){
           if(!set.add(i))
               return true;
        }
        return false;
    }

    //Sort
    public boolean containsDuplicateUsingSort(int[] nums){
        Arrays.sort(nums);
        for(int i =0; i < nums.length-1; i++){
            if(nums[i] == nums[i+1])
              return true;
        }
        return false;
    }
    @Test
    public void containsDuplicateTest(){
        int[] nums = {1,2,3,4,5};
        assertFalse(containsDuplicateUsingSort(nums));
    }
    @Test
    public void containsDuplicateTest1(){
        int[] nums = {1, 1, 1, 1};
        assertTrue(containsDuplicateUsingSort(nums));
    }
    //Given a string sentence containing English letters (lower- or upper-case), return true if sentence is a Pangram, or false otherwise.
    // using HashSet
    public boolean isPangram(String s){
        Set<Character> set = new HashSet<>();
        for(char c : s.toLowerCase().toCharArray()){
            if(Character.isLetter(c)){
                set.add(c);
            }
        }
           return set.size() == 26;
    }

    @Test
    public void isPangramTest(){
        String s= "TheQuickBrownFoxJumpsOverTheLazyDog";
        assertTrue(isPangram(s));
    }

    @Test
    public void isPangramTest1(){
        String s= "This is not a pangram";
        assertEquals(false,isPangram(s));
    }

    //Given a non-negative integer x, return the square root of x rounded down to the nearest integer. The returned integer should be non-negative as well.
    //Binary Search approach
    public int sqrt(int n){
        if(n < 2) return n;
          int left = 2, right = n/2;
        return right;
    }

    @Test
    public void  sqrtTest(){
        int n = 8;
        assertEquals(2,sqrt(n));
        System.out.println(sqrt(n));
    }
}

/*
1. Two Pointers
        2. Island (Matrix Traversal) Pattern
        3. Fast & Slow Pointers
        4. Sliding Window
        5. Merge Intervals
        6. Cyclic Sort
        7. In-place Reversal of a Linked List
        8. Tree Breadth First Search
        9. Tree Depth First Search
        10. Two Heaps
        11. Subsets
        12. Modified Binary Search
        13. Bitwise XOR
        14. Top 'K' Elements
        15. K-way Merge
        16. Topological Sort
        17. Trie
        18. Backtracking
        19. Monotonic Stack
        20. 0/1 Knapsack (Dynamic Programming)*/