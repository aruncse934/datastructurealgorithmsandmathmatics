package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import static Arrays.Sort.MergeSort.sort;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

//If you are a beginner solve these problems which makes concepts clear for future coding:
//
//Two Sum
//Roman to Integer
//Palindrome Number
//Maximum Subarray
//Remove Element
//Contains Duplicate
//Add Two Numbers
//Majority Element
//Remove Duplicates from Sorted Array
// Valid Anagram
// Group Anagrams







public class TwoPointer {


    //Remove Duplicates from Sorted Array
    public  int removeDuplicates(int[] nums) {
        int j = 1;
        int n = nums.length;
        for(int i =1; i<n; i++){
            if(nums[i]!= nums[i-1]){
                nums[j] = nums[i];
                j++;
            }
        }
        return j;
    }
    @Test
    public void removeDuplicatesTestCase1() {
       int[] nums = {1,1,2}; // Input array
        int[] expectedNums = {1,2}; // The expected answer with correct length
        int k = removeDuplicates(nums); // Calls your implementation

        assert k == expectedNums.length;
        for (int i = 0; i < k; i++) {
            assert nums[i] == expectedNums[i];
        }
    }

    //
    public static int removeElement(int[] nums, int val) {
            int j =0;
            for(int i =0; i< nums.length; i++){
                if(nums[i] != val){
                    nums[j] = nums[i];
                    j++;
                }
            }
            return j;
    }

    @Test
    public void removeElementTestCase1() {
        int[] nums = {3,2,2,3}; // Input array
        int val = 3; // Value to remove
        int[] expectedNums = {2,2}; // The expected answer with correct length.
        // It is sorted with no values equaling val.
        int k = removeElement(nums, val); // Calls your implementation
        assert k == expectedNums.length;
        sort(nums, 0, k); // Sort the first k elements of nums
        for (int i = 0; i < k; i++) {
            assert nums[i] == expectedNums[i];
        }
    }


    //Concatenation of Array
    public int[] getConcatenation(int[] nums) {
       int[] res = new int[nums.length*2];
       for(int i =0; i< nums.length;i++){
           res[i] = nums[i];
           res[i+nums.length] = nums[i];
       }
       return res;
    }
    @Test
    public void getConcatenationTestCase1(){
        int[] nums  ={1,3,2,1};
        int[] expNums = {1,3,2,1,1,3,2,1};
        int[] result = getConcatenation(nums);
        assertArrayEquals(expNums, result);
    }



    public int trapRainWater(int[] arr){
        int res = 0;

        int left_max = 0, right_max = 0;
        int i = 0, j = arr.length-1;
        while(i < j){
            left_max = Math.max(left_max,arr[i]);
            right_max = Math.max(right_max,arr[j]);
            if(left_max< right_max) {
                res += left_max - arr[i];
                i++;
            }
            else {
                res += right_max - arr[j];
                j--;
            }
        }
        return res;
    }

    @Test
    public void  trapRainWaterTest(){
        int[] arr = {0,1,0,2,1,0,1,3,2,1,2,1};
        int trappedWater = trapRainWater(arr);
        // The expected trapped rainwater for this input is 6.
        assertEquals(6, trappedWater);

    }

}


