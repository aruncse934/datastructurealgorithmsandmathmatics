package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.*;

class Graph2 {
    private int vertices; // Number of vertices
    private LinkedList<Integer>[] adjacencyList; // Adjacency list

    // Constructor
    public Graph2(int vertices) {
        this.vertices = vertices;
        this.adjacencyList = new LinkedList[vertices];

        for (int i = 0; i < vertices; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    // Add an edge to the graph
    public void addEdge(int source, int destination) {
        adjacencyList[source].add(destination);
    }

    // Depth-First Search traversal starting from a given source vertex
    public void dfs(int source) {
        boolean[] visited = new boolean[vertices]; // Mark all vertices as not visited
        Stack<Integer> stack = new Stack<>();
        visited[source] = true;
        stack.push(source);
        while (!stack.isEmpty()) {
            int currentVertex = stack.pop();
            System.out.print(currentVertex + " ");
            // Visit all adjacent vertices
            for (int neighbor : adjacencyList[currentVertex]) {
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    stack.push(neighbor);
                }
            }
        }
    }
}

public class DepthFirstSearch {
    @Test
     public void DepthFirstSearchTest(){
    Graph2 graph = new Graph2(6);

    // Add edges
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);

    // Perform DFS starting from vertex 0
        System.out.println("Depth-First Search starting from vertex 0:");
        graph.dfs(0);
   }

   //Binary Tree Inorder Traversal
   public List<Integer> inorderTraversal(TreeNode root) {
       List<Integer> list = new ArrayList<>();
       Stack<TreeNode> stack = new Stack<>();
       TreeNode curr = root;
       while(curr != null || !stack.isEmpty()){
           while (curr != null){
               stack.push(curr);
               curr = curr.left;
           }
           curr = stack.pop();
           list.add(curr.val);
           curr = curr.right;
       }
       return list;
   }
   @Test
   public void inorderTraversalTest(){
       TreeNode root = new TreeNode(1);
       root.left = new TreeNode(2);
       root.right = new TreeNode(3);
       root.left.left = new TreeNode(4);
       root.left.right = new TreeNode(5);
       System.out.println("Inorder Traversal (Iterative):");
       System.out.println(inorderTraversal(root));
   }
}
 

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
}


class TreeDFS{

    TreeNode root;

    private void visit(int value) {
        System.out.print(" " + value);
    }

    //Preorder Traversal recursive DFS
    public void traversePreOrder(TreeNode node) {
        if (node != null) {
            visit(node.val);
            traversePreOrder(node.left);
            traversePreOrder(node.right);
        }
    }

    //preorder traversal iterative DFS
    public void traversePreOrderWithoutRecursion() {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode current = root;
        stack.push(root);
        while(!stack.isEmpty()) {
            current = stack.pop();
            visit(current.val);

            if(current.right != null) {
                stack.push(current.right);
            }
            if(current.left != null) {
                stack.push(current.left);
            }
        }
    }

    //
    public void traverseInOrder(TreeNode node) {
        if (node != null) {
            traverseInOrder(node.left);
            visit(node.val);
            traverseInOrder(node.right);
        }
    }
    //
    public void traverseInOrderWithoutRecursion() {
        Stack stack = new Stack<>();
        TreeNode current = root;

        while (current != null || !stack.isEmpty()) {
            while (current != null) {
                stack.push(current);
                current = current.left;
            }

            TreeNode top = (TreeNode) stack.pop();
            visit(top.val);
            current = top.right;
        }
    }

    //

    public void traversePostOrder(TreeNode node) {
        if (node != null) {
            traversePostOrder(node.left);
            traversePostOrder(node.right);
            visit(node.val);
        }
    }

    //
    public void traversePostOrderWithoutRecursion() {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode prev = root;
        TreeNode current = root;
        stack.push(root);

        while (!stack.isEmpty()) {
            current = stack.peek();
            boolean hasChild = (current.left != null || current.right != null);
            boolean isPrevLastChild = (prev == current.right ||
                    (prev == current.left && current.right == null));

            if (!hasChild || isPrevLastChild) {
                current = stack.pop();
                visit(current.val);
                prev = current;
            } else {
                if (current.right != null) {
                    stack.push(current.right);
                }
                if (current.left != null) {
                    stack.push(current.left);
                }
            }
        }
    }


}

class GraphDFS {


    private Map<Integer, List<Integer>> adjVertices;

    public GraphDFS() {
        this.adjVertices = new HashMap<Integer, List<Integer>>();
    }

    public void addVertex(int vertex) {
        adjVertices.putIfAbsent(vertex, new ArrayList<>());
    }

    public void addEdge(int src, int dest) {
        adjVertices.get(src).add(dest);
    }

    private void visit(int value) {
        System.out.print(" " + value);
    }


    // Graph DFS with Recursion
    public boolean[] dfs(int start) {
        boolean[] isVisited = new boolean[adjVertices.size()];
        return dfsRecursive(start, isVisited);
    }

    private boolean[] dfsRecursive(int current, boolean[] isVisited) {
        isVisited[current] = true;
        visit(current);
        for (int dest : adjVertices.get(current)) {
            if (!isVisited[dest])
                dfsRecursive(dest, isVisited);
        }
        return isVisited;
    }

    //
    public void dfsWithoutRecursion(int start) {
        Stack<Integer> stack = new Stack<Integer>();
        boolean[] isVisited = new boolean[adjVertices.size()];
        stack.push(start);
        while (!stack.isEmpty()) {
            int current = stack.pop();
            if (!isVisited[current]) {
                isVisited[current] = true;
                visit(current);
                for (int dest : adjVertices.get(current)) {
                    if (!isVisited[dest])
                        stack.push(dest);
                }
            }
            //  return isVisited;
        }


        // Topological Sort
      /*  public List<Integer> topologicalSort(int start) {
            LinkedList<Integer> result = new LinkedList<Integer>();
            boolean[] isVisited = new boolean[adjVertices.size()];
            topologicalSortRecursive(start, isVisited, result);
            return result;
        }

        private void topologicalSortRecursive(int current, boolean[] isVisited, LinkedList<Integer> result) {
            isVisited[current] = true;
            for (int dest : adjVertices.get(current)) {
                if (!isVisited[dest])
                    topologicalSortRecursive(dest, isVisited, result);
            }
            result.addFirst(current);
        }*/
    }
}