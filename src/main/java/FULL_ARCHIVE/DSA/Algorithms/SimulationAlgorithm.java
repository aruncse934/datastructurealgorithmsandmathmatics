package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

public class SimulationAlgorithm {

    public static long fibonacci(int N) {
        if (N == 1) {
            return 0;
        } else if (N == 2) {
            return 1;
        } else {
            long a = 0;
            long b = 1;
            for (int i = 3; i <= N; i++) {
                long temp = a + b;
                a = b;
                b = temp;
            }
            return b;
        }
    }
        @Test
        public void fibonacciTest(){
            System.out.println(fibonacci(10));
        }
}
