package FULL_ARCHIVE.DSA.Algorithms;

import FULL_ARCHIVE.DSA.ArrayWithHashing.TwoSum;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class HashTable {
    /*Hash Table is a data structure which organizes data using hash functions in order to
     support quick insertion and search.*/


    public int romanToInt(String s) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        int res = 0;
        int prev = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            int currValue = map.get(s.charAt(i));
            if (currValue < prev) res -= currValue;
            else res += currValue;

            prev = currValue;
        }
        return res;

    }

    @Test
    public void romanToIntTest() {
        String s = "III";
        int res = romanToInt(s);
        assertEquals(3, res);
    }

    //290. Word Pattern
    public boolean wordPattern(String pattern, String s) {
        String[] words = s.split(" ");
        if (words.length != pattern.length()) return false;
        Map<Character, String> map = new HashMap<>();
        for (int i = 0; i < pattern.length(); i++) {
            char ch = pattern.charAt(i);
            String word = words[i];
            if (map.containsKey(ch)) {
                if (!map.get(ch).equals(word)) return false;
            } else if (map.containsValue(word)) return false;
            map.put(ch, word);
        }
        return true;
    }

    @Test
    public void wordPatternTest() {
        String pattern = "abba";
        String s = "dog cat cat dog";
        System.out.println(wordPattern(pattern, s));

    }

    public boolean containsNearByDuplicate(int[] nums, int k) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (i > k) set.remove(nums[i - k - 1]);
            if (!set.add(nums[i])) return true;
        }
        return false;
    }

    @Test
    public void containsNearByDuplicateTest() {
        int[] nums = {1, 2, 3, 1};
        int k = 3;
        System.out.println(containsNearByDuplicate(nums, k));
    }

    //128. Longest Consecutive Sequence
    public int longestConsecutive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int n : nums) {
            set.add(n);
        }
        int count = 0;
        for (int n : set) {
            if (!set.contains(n - 1)) {
                int m = n + 1;
                while (set.contains(m)) m++;
                count = Math.max(count, m - n);
            }
        }
        return count;
    }

    @Test
    public void longestConsecutiveTest() {
        int[] nums = {100, 4, 200, 1, 3, 2};
        System.out.println(longestConsecutive(nums));
    }

}

 /*
    Design HashSet
    Design a HashSet without using any built-in hash table libraries.

    Implement MyHashSet class:

    void add(key) Inserts the value key into the HashSet.
    bool contains(key) Returns whether the value key exists in the HashSet or not.
    void remove(key) Removes the value key in the HashSet. If key does not exist in the HashSet, do nothing.

    */

class DesignHashSet {

    private boolean[] set = new boolean[1000001];

    public DesignHashSet() {
        set = new boolean[1000001];
    }

    public void add(int key) {
        set[key] = true;
    }

    public void remove(int key) {
        set[key] = false;
    }

    public boolean contains(int key) {
        return set[key];
    }

    @Test
    public void DesignHashSetTest() {
        DesignHashSet set = new DesignHashSet();
        // ["MyHashSet"]
        // Output: [null]
        System.out.println(set);

        // ["add", 1]
        // Output: [null]
        set.add(1);

        // ["add", 2]
        // Output: [null]
        set.add(2);

        // ["contains", 1]
        // Output: [true]
        System.out.println(set.contains(1));

        // ["contains", 3]
        // Output: [false]
        System.out.println(set.contains(3));

        // ["add", 2]
        // Output: [null]
        set.add(2);

        // ["contains", 2]
        // Output: [true]
        System.out.println(set.contains(2));

        // ["remove", 2]
        // Output: [null]
        set.remove(2);

        // ["contains", 2]
        // Output: [false]
        System.out.println(set.contains(2));
    }

}

class HashSetUsage {

    @Test
    public void HashSetUsageTest() {
        // 1. initialize the hash set
        Set<Integer> hashSet = new HashSet<>();
        // 2. add a new key
        hashSet.add(3);
        hashSet.add(2);
        hashSet.add(1);
        // 3. remove the key
        hashSet.remove(2);
        // 4. check if the key is in the hash set
        if (!hashSet.contains(2)) {
            System.out.println("Key 2 is not in the hash set.");
        }
        // 5. get the size of the hash set
        System.out.println("The size of has set is: " + hashSet.size());
        // 6. iterate the hash set
        for (Integer i : hashSet) {
            System.out.print(i + " ");
        }
        System.out.println("are in the hash set.");
        // 7. clear the hash set
        hashSet.clear();
        // 8. check if the hash set is empty
        if (hashSet.isEmpty()) {
            System.out.println("hash set is empty now!");
        }
    }
}

class HashSetPracticeExample {

    //Contains Duplicate
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i : nums) {
            if (!set.add(i)) return true;
        }
        return false;
    }

    @Test
    public void containsDuplicateTest() {
        int[] arr = {1, 2, 3, 1};
        System.out.println(containsDuplicate(arr));

        int[] arr1 = {1, 2, 3, 4};
        System.out.println(containsDuplicate(arr1));

        int[] arr2 = {1, 1, 1, 3, 3, 4, 3, 2, 4, 2};
        System.out.println(containsDuplicate(arr2));
    }

    //Single Number
    public int singleNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                set.remove(num);
            } else {
                set.add(num);
            }
        }
        return set.iterator().next();
    }

    @Test
    public void singleNumberTest() {
        int[] arr = {2, 2, 1};
        System.out.println(singleNumber(arr));

        int[] arr1 = {4, 1, 2, 1, 2};
        System.out.println(singleNumber(arr1));

        int[] arr2 = {1};
        System.out.println(singleNumber(arr2));
    }

    //Intersection of Two Arrays
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        for (int i : nums1) {
            set1.add(i);
        }
        Set<Integer> set2 = new HashSet<>();
        for (int j : nums2) {
            if (set1.contains(j)) {
                set2.add(j);
            }
        }
        // set1.retainAll(set2);
        int[] res = new int[set2.size()];
        int idx = 0;
        for (int s : set2) res[idx++] = s;
        return res;

    }

    @Test
    public void intersectionTest() {
        int[] arr1 = {1, 2, 2, 1};
        int[] arr2 = {2, 2};
        System.out.println(Arrays.toString(intersection(arr1, arr2)));

        int[] arr3 = {4, 9, 5};
        int[] arr4 = {9, 4, 9, 8, 4};
        System.out.println(Arrays.toString(intersection(arr3, arr4)));

    }

    //Happy Number
    public boolean isHappy(int n) {
        Set<Integer> seen = new HashSet<>();
        while (n != 1 && !seen.contains(n)) {
            seen.add(n);
            n = getNext(n);
        }
        return n == 1;
    }

    private int getNext(int num) {
        int sum = 0;
        while (num > 0) {
            int rem = num % 10;
            sum += rem * rem;
            num /= 10;
        }
        return sum;
    }

    @Test
    public void isHappyTest() {
        int n = 19;
        System.out.println(isHappy(n));

        int n1 = 2;
        System.out.println(isHappy(n1));
    }
}

class DesignHashMap {
    public DesignHashMap() {

    }

    public void put(int key, int value) {

    }

    public int get(int key) {
      return 1;
    }

    public void remove(int key) {

    }
}

class HashMapUsage{
    @Test
    public void HashMapUsageTest(){
        // 1. initialize a hash map
        Map<Integer, Integer> hashmap = new HashMap<>();
        // 2. insert a new (key, value) pair
        hashmap.putIfAbsent(0, 0);
        hashmap.putIfAbsent(2, 3);
        // 3. insert a new (key, value) pair or update the value of existed key
        hashmap.put(1, 1);
        hashmap.put(1, 2);
        // 4. get the value of specific key
        System.out.println("The value of key 1 is: " + hashmap.get(1));
        // 5. delete a key
        hashmap.remove(2);
        // 6. check if a key is in the hash map
        if (!hashmap.containsKey(2)) {
            System.out.println("Key 2 is not in the hash map.");
        }
        // 7. get the size of the hash map
        System.out.println("The size of hash map is: " + hashmap.size());
        // 8. iterate the hash map
        for (Map.Entry<Integer, Integer> entry : hashmap.entrySet()) {
            System.out.print("(" + entry.getKey() + "," + entry.getValue() + ") ");
        }
        System.out.println("are in the hash map.");
        // 9. clear the hash map
        hashmap.clear();
        // 10. check if the hash map is empty
        if (hashmap.isEmpty()) {
            System.out.println("hash map is empty now!");
        }
    }
}
class HashMapPracticeExample {

     //Two Sum
     public int[] twoSum(int[] nums, int target) {
         Map<Integer, Integer> map = new HashMap<>();
         for (int i = 0; i < nums.length; i++) {
             int diff = target - nums[i];
             if (map.containsKey(diff)) {
                 return new int[] { map.get(diff), i };
             }
             map.put(nums[i], i);
         }
         return null;
     }

     @Test
    public void twoSumTest(){
         int[] nums = {2, 7, 11, 15};
         int target = 9;
         System.out.println(Arrays.toString(new TwoSum().twoSum(nums, target)));
     }

     //  Isomorphic Strings
    public static boolean isIsomorphic(String s, String t){
         if(s.length() != t.length()) return false;
         Map<Character, Character> map = new HashMap<>();
         for(int i = 0; i<s.length(); i++){
             char charS = s.charAt(i);
             char charT = t.charAt(i);
             if(!map.containsKey(charS)){
                 if(!map.containsValue(charT))
                     map.put(charS,charT);
                 else
                     return false;
             }
             else {
                 if(map.get(charS) !=charT) return false;
             }
         }
         return true;
    }

    @Test
    public void isIsomorphicTest(){
       String s = "egg", t = "add";
        System.out.println(isIsomorphic(s,t));
        String s1 = "foo", t1 = "bar";
        System.out.println(isIsomorphic(s1,t1));
        String s2 = "paper", t2 = "title";
        System.out.println(isIsomorphic(s2,t2));
    }

    //


    /*public String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        List<String> res = new ArrayList<>();
        int leastIndSum = Integer.MAX_VALUE;
        for(int i = 0; i <list1.length;i++ ){
            map.put(list1[i], i);
        }
        for(int j =0; j <list2.length;j++){
            if(map.containsKey(list2[j])){
                int indSum = j+ map.get(list2[j]);
                if(indSum < leastIndSum){
                    res.clear();
                    res.add(list2[j]);
                    leastIndSum = indSum;
                }
                else if(indSum == leastIndSum){
                    res.add(list2[j]);
                }
            }
        }
        return res.toArray(new String[0]);
    }*/


    public int firstUniqChar(String s) {
        Map<Character,Integer> map = new HashMap<>();
        for(char c : s.toCharArray()){
            map.put(c,map.getOrDefault(c,0)+1);
        }
        for(int i =0; i < s.length(); i++) {
            if (map.get(s.charAt(i)) == 1) return i;
        }
        return -1;
    }


    //Logger Rate Limiter
}


class LoggerRateLimiter{

    Map<String,Integer> logs;

    public LoggerRateLimiter() {
        this.logs = new HashMap<>();
    }
    public boolean shouldPrintMessage(int timestamp, String message) {

        if (!logs.containsKey(message) || timestamp - logs.get(message) >= 10) {
            logs.put(message, timestamp);
            return true;
        }
        else{
            return false;
        }
    }
    @Test
    public void LoggerRateLimiterTest() {
    //[[1, foo],[2, bar],[3, foo],[8, bar],[10, foo],[11, foo]]
        LoggerRateLimiter logger = new LoggerRateLimiter();
        System.out.println(logger.shouldPrintMessage(1, "foo"));
        System.out.println(logger.shouldPrintMessage(2, "bar"));
        System.out.println(logger.shouldPrintMessage(3, "foo"));
        System.out.println(logger.shouldPrintMessage(8, "bar"));
        System.out.println(logger.shouldPrintMessage(10, "foo"));
        System.out.println(logger.shouldPrintMessage(11, "foo"));
        }
        
    }

    // public boolean shouldPrintMessage(int timestamp, String message) {
    //        if (timestamp - lastTime.getOrDefault(message, -100) < 10)
    //            return false;
    //        lastTime.put(message, timestamp);
    //        return true;
    //    }




 class NumberFrequencyInRange {

     public static Map<Integer, Integer> countFrequencyInRange(int[] nums, int l, int r) {
         int mod = 1000000007;
         Map<Integer, Integer> frequencyMap = new HashMap<>();

         // Count the frequency of each number in the given range
         for (int num : nums) {
             if (num >= l && num <= r) {
                 int i = frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1) % mod;
             }
         }

         return frequencyMap;
     }

     public static void main(String[] args) {
         // Example usage:
         int[] nums = {1, 2, 3, 2, 1, 4, 5, 1};
         int l = 2;
         int r = 4;

         Map<Integer, Integer> frequencyMap = countFrequencyInRange(nums, l, r);

         // Print the frequency of each number in the range [l, r]
         for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
             System.out.println("Number: " + entry.getKey() + ", Frequency: " + entry.getValue());
         }
     }

     public void countSubMultisets(List<Integer> nums, int l, int r) {
         int mod = 1000000007;


         long[] prefixSum = new long[nums.size() + 1];
         for (int i = 1; i <= nums.size(); i++) {
             prefixSum[i] = (prefixSum[i - 1] + nums.get(i - 1)) % mod;
         }

         Map<Integer, Integer> map = new HashMap<>();
         for (int num : nums) {
             if (num >= l && num <= r) {
                 map.put(num, map.getOrDefault(num, 0) + 1);
             }
         }
         int count = 1;
         for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
             int num = entry.getKey();
             int freq = entry.getValue();

             System.out.println(num + "=" + freq);
             int tempResult = 0;
             for (int i = 0; i <= freq; i++) {
                 long currentSum = 1L * num * i;
                 if (l - currentSum <= r - currentSum) {
                     // Include sub-multisets where the sum is in the range [l, r]
                     tempResult = (tempResult + count) % mod;
                 }
             }

             count = tempResult;
         }
         System.out.println(count);
     }

     @Test
     public void test() {
         List<Integer> nums = Arrays.asList(2, 1, 4, 2, 7);
         int l = 1, r = 5;
         //     countSubMultisets(nums,l,r);

         List<Integer> nums1 = Arrays.asList(1, 2, 1, 3, 5, 2);
         int l1 = 3;
         int r1 = 5;
         System.out.println(countSubMultisetss(nums1, l1, r1));
     }

     public static int countSubMultisetss(List<Integer> nums, int l, int r) {
         int mod = 1000000007;

         // Prefix sum array

         long[] prefixSum = new long[nums.size() + 1];
         for (int i = 1; i <= nums.size(); i++) {
             prefixSum[i] = (prefixSum[i] + nums.get(i - 1)) % mod;
         }

         // Count the sub-multisets using prefix sums and a HashMap
         Map<Long, Integer> countMap = new HashMap<>();
         int totalCount = 1;

         for (long sum : nums) {
             // Count the number of valid sub-multisets with sum in the range [l, r]
             for (long target = l; target <= r; target++) {

                 totalCount = (totalCount + countMap.getOrDefault(sum - target, 0)) % mod;
             }

             // Update the countMap with the current prefix sum
             countMap.put(sum, countMap.getOrDefault(sum, 0) + 1);
         }
         System.out.println(countMap);

         return totalCount;
     }

     public static int countSubsetsSumInRange(int[] nums, int l, int r) {
         int mod = 1000000007;
         int totalCount = 0;
         int currentSum = 0;

         // Map to store the count of prefix sums
         Map<Integer, Integer> countMap = new HashMap<>();
         countMap.put(0, 1); // Initialize with 0 to cover subsets starting from the beginning

         for (int num : nums) {
             currentSum += num;

             // Count the number of valid sub-multisets with sum in the range [l, r]
             for (int target = l; target <= r; target++) {
                 totalCount = (totalCount + countMap.getOrDefault(currentSum - target, 0)) % mod;
             }

             // Update the countMap with the current prefix sum
             countMap.put(currentSum, countMap.getOrDefault(currentSum, 0) + 1);
         }

         return totalCount;
     }

     //
     public String isSubset(long a1[], long a2[]) {
         Map<Long, Long> map = new HashMap<>();
         for (Long n : a1) {
             map.put(n, map.getOrDefault(n, 0L) + 1);
         }
         for (Long n : a2) {
             if (map.containsKey(n) && map.get(n) > 0) {
                 map.put(n, map.get(n) - 1);
             } else {
                 return "No";
             }
         }
         return "Yes";
     }

     public String isSubset1(long[] a1, long[] a2) {
         Map<Long, Long> map = Arrays.stream(a1)
                 .boxed()
                 .collect(HashMap::new, (m, n) -> m.merge(n, 1L, Long::sum), HashMap::putAll);

         return Arrays.stream(a2)
                 .boxed()
                 .allMatch(n -> map.containsKey(n) && map.get(n) > 0)
                 ? "Yes"
                 : "No";
     }

     public String isSubset2( long a1[], long a2[], long n, long m) {
         Map<Long, Long> map = Arrays.stream(a1)
                 .boxed()
                 .collect(HashMap::new, (m1, v) -> m1.merge(v, 1L, Long::sum), HashMap::putAll);

         return Arrays.stream(a2)
                 .allMatch(x -> map.getOrDefault(x, 0L) > 0) ? "Yes" : "No";
     }

     @Test
     public void isSubSetTest() {
         long[] a1 = {11, 7, 1, 13, 21, 3, 7, 3};
         long[] a2 = {11, 3, 7, 1, 7};
         System.out.println(isSubset2(a1,a2,8,5));
     }

 }