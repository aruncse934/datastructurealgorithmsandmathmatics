package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

public class SimpleJavaInterviewProgram {

       //Write a java program to reverse a string?

       public void reverseStringUsingStringBuilder(String s ){
           StringBuilder sb = new StringBuilder(s);
           System.out.println(sb.reverse());
       }
       @Test
      public void   reverseStringUsingStringBuilderTest(){
           String s = "hello";
           reverseStringUsingStringBuilder(s);
       }

    public void reverseStringUsingIterative(String s ){
              char[] c = s.toCharArray();
           for(int i = c.length -1; i >= 0; i--) {
               System.out.print(c[i]+"");
           }

    }
    @Test
    public void  reverseStringUsingIterativeTest(){
        String s = "hello";
        reverseStringUsingIterative(s);
    }

    public String reverseStringUsingRecursive(String s ){

         if(s==null|| s.length()<=1) return s;
        return reverseStringUsingRecursive(s.substring(1))+s.charAt(0);
    }
    @Test
    public void  reverseStringUsingRecursiveeTest(){
        String s = "hello";
        System.out.println( reverseStringUsingRecursive(s));;
    }
}
