package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

public class Mathematical {

    //202. Happy Number
   /* public boolean isHappy(int n) {
        int count = 0;
        while(n > 1){
            int sqSum = 0;
            while(n > 0){
                int rem = n%10;
                sqSum += rem*rem;
                n /=10;
            }
            count++;
            if(count == 10) return false;
            n = sqSum;
        }
        return true;
    }*/

    public boolean isHappy(int n){
        int count = 0;
        while (n > 1){
            int sum = 0;
            while (n > 0){
                int temp = n%10;
                sum += temp*temp;
                n /= 10;
            }
            count++;
            if(count == 10) return false;
            n = sum;
        }
        return true;
    }
    @Test
    public void isHappyTest(){
        int n = 10;
        System.out.println(isHappy(n));
        System.out.println(isHappy(2));
    }
}
class Fundamentals {

}



class NumberTheory{

}

class Combinatorics{

}

class Algebra{

}

class Geometry{

}

class Probability{
    
}

class LinearAlgebraFoundations{

}

class Divisibility_Large_Numbers{

    public  boolean divisibleBy3(String s){
        int digitSum = 0;
        for(int i = 0; i<s.length(); i++){
            digitSum += (s.charAt(i) -'0');
        }
        return digitSum % 3 == 0;
    }

    public boolean divisibleBy3(String n, int k){
       // bool check(string &n, long long k) {
            int rem = 0;
            for (int i=0; i< n.length();i++)
                rem = ((rem * 10) + i - '0') % k;
            return rem == 0;
    }

    @Test
    public void divisibaleBy3Test(){
        String s = "1332";
        System.out.println(divisibleBy3(s));
        
        String s1 = "123456758933312";
        System.out.println(divisibleBy3(s1,3));

        String s2 = "3635883959606670431112222";
        System.out.println(divisibleBy3(s2));

        System.out.println("=====");
        int n = 1332;
       
    }

}
