package FULL_ARCHIVE.DSA.Algorithms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AllArgsConstructor
@Getter
@Setter
@ToString
class Employee {
    private int id;
    private String name;
    private int age;
    private long salary;
}

@AllArgsConstructor
@Getter
@Setter
@ToString
class Student {
    private String name;
    private int id;
    private String subject;
    private double percentage;
}
@AllArgsConstructor
@Getter
@Setter
@ToString
class EmpDepartment {
    private int id;
    private String name;
    private int age;
    private String gender;
    private String department;
    private int yearOfJoining;
    private double salary;

    }

    public class JavaStreamExample {
        public static void main(String[] args) {


            /*List<Employee> employees = new ArrayList< Employee >();
            employees.add(new Employee(10, "Ramesh", 30, 400000));
            employees.add(new Employee(20, "John", 29, 350000));
            employees.add(new Employee(30, "Tom", 30, 450000));
            employees.add(new Employee(40, "Pramod", 29, 500000));*/

            List<Student> students = Arrays.asList(
                    new Student("Arun", 101, "Java", 90.2),
                    new Student("ARjun", 106, "Java", 50.2),
                    new Student("Suji", 105, "Spring", 49.2),
                    new Student("Ravi", 102, "Rest", 60),
                    new Student("Satyam", 104, "DB", 95.6),
                    new Student("shivam", 103, "DSA", 98.2)

            );

            //Given a list of students, write a Java 8 code to partition the students who got above 60% from those who didn’t?
            Map<Boolean, List<Student>> map = students.stream().collect(Collectors.partitioningBy(student -> student.getPercentage() > 60.0));
            System.out.println(map);

            // Given a list of students, write a Java 8 code to get the names of top 3 performing students?
            List<Student> top3Name = students.stream().sorted(Comparator.comparingDouble(Student::getPercentage).reversed()).limit(3).collect(Collectors.toList());
            System.out.println(top3Name);

            // Given a list of students, how do you get the name and percentage of each student?
            Map<String, Double> namePercentage = students.stream().collect(Collectors.toMap(Student::getName, Student::getPercentage));
            System.out.println(namePercentage);

            //Given a list of students, how do you get the subjects offered in the college?
            Set<String> subjects = students.stream().map(Student::getSubject).collect(Collectors.toSet());
            System.out.println(subjects);

            // Given a list of students, write a Java 8 code to get highest, lowest and average percentage of students?
            DoubleSummaryStatistics summaryStatistics = students.stream().collect(Collectors.summarizingDouble(Student::getPercentage));
            System.out.println("Highet : " + summaryStatistics.getMax() + "\n" + "Lowest : " + summaryStatistics.getMin() + "\n" + "Average : " + summaryStatistics.getAverage());

            // How do you get total number of students from the given list of students?
            Long studentCount = students.stream().collect(Collectors.counting());
            System.out.println(studentCount);

            //How do you get the students grouped by subject from the given list of students?
            Map<String, List<Student>> groupSubjects = students.stream().collect(Collectors.groupingBy(Student::getSubject));
            System.out.println(groupSubjects);

            //


            List<EmpDepartment> empDepartments = Arrays.asList(
                    new EmpDepartment(2001, "Siya", 45, "Female", "HR", 2019, 3000),
                    new EmpDepartment(2002, "Riya", 43, "male", "Manager", 2021, 6000),
                    new EmpDepartment(2003, "Tiya", 41, "Female", "Accounts", 2019, 4000),
                    new EmpDepartment(2004, "Kiya", 42, "male", "Admin", 2023, 5000),
                    new EmpDepartment(2005, "Miya", 44, "Female", "QA", 2023, 2000),
                    new EmpDepartment(2006, "Diya", 39, "Female", "HR", 2022, 1000)
            );
            //Given a list of employees, write a Java 8 code to count the number of employees in each department?
            Map<String, Long> countEachDepartMent = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.counting()));
            System.out.println(countEachDepartMent);

            //Given a list of employees, find out the average salary of male and female employees?
            Map<String, Double> avgMaleNdFemaleSalary = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getGender, Collectors.averagingDouble(EmpDepartment::getSalary)));
            System.out.println(avgMaleNdFemaleSalary);

            //Write a Java 8 code to get the details of highest paid employee in the organization from the given list of employees?
            Optional<EmpDepartment> highestEmpSala1 = empDepartments.stream().max(Comparator.comparingDouble(EmpDepartment::getSalary));
            System.out.println(highestEmpSala1);
            Optional<EmpDepartment> highestEmpSala2 = empDepartments.stream().collect(Collectors.maxBy(Comparator.comparingDouble(EmpDepartment::getSalary)));
            System.out.println(highestEmpSala2);

            //Write the Java 8 code to get the average age of each department in an organization?
            Map<String, Double> avgAgeDept = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.averagingInt(EmpDepartment::getAge)));
            System.out.println(avgAgeDept);

            //Given a list of employees, how do you find out who is the senior most employee in the organization?
            Optional<EmpDepartment> mostSeniorEmp = empDepartments.stream().sorted(Comparator.comparingInt(EmpDepartment::getYearOfJoining)).findFirst();
            System.out.println(mostSeniorEmp.get());

            //Given a list of employees, get the details of the most youngest employee in the organization?
            Optional<EmpDepartment> youngEmp1 = empDepartments.stream().collect(Collectors.minBy(Comparator.comparingInt(EmpDepartment::getAge)));
            Optional<EmpDepartment> youngEmp2 = empDepartments.stream().min(Comparator.comparingInt(EmpDepartment::getAge));
            System.out.println(youngEmp1 + "\n" + youngEmp2);

            //How do you get the number of employees in each department if you have given a list of employees?
            Map<String, Long> eachDepartCou = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getDepartment, Collectors.counting()));
            System.out.println(eachDepartCou);

            //Given a list of employees, find out the number of male and female employees in the organization?
            Map<String, Long> countMndF = empDepartments.stream().collect(Collectors.groupingBy(EmpDepartment::getGender, Collectors.counting()));
            System.out.println(countMndF);
            //Print the name of all departments in the organization?
            empDepartments.stream().map(EmpDepartment::getDepartment).distinct().forEach(System.out::println);


            System.out.println(IntStream.range(1, 6).sum());

            //StackOverFlowExxample
            //Java 8 Distinct by property  persons.stream().filter(distinctByKey(Person::getName))
            Collection<EmpDepartment> departments = empDepartments.stream().collect(Collectors.toMap(EmpDepartment::getName, p -> p, (p, q) -> p)).values();
            System.out.println(departments);

            List<Employee> list = Arrays.asList(
                    new Employee(10, "Ramesh", 30, 400000),
                    new Employee(20, "John", 29, 350000),
                    new Employee(30, "Tom", 30, 450000),
                    new Employee(40, "Pramod", 29, 500000)

            );

            //second highest salary
            Optional<Employee> secondHighestSalary = list.stream().sorted(Comparator.comparingLong(Employee::getSalary).reversed()).skip(1).findFirst();
            System.out.println("second highest salary: " + secondHighestSalary.get());

            // sort employee by salary in ascending order
            //1st Approach
            List<Employee> employeeList = list.stream().sorted(((a, b) -> (int) (a.getSalary() - b.getSalary()))).toList();
            System.out.println("sort emp salry asc:" + employeeList);

            //2nd Approach
            List<Employee> list1 = list.stream().sorted(Comparator.comparingLong(Employee::getSalary)).collect(Collectors.toList());
            System.out.println(list1);

            // sort employee by salary in descending order
            //1st approach
            List<Employee> list2 = list.stream().sorted((a, b) -> (int) (b.getSalary() - a.getSalary())).collect(Collectors.toList());
            System.out.println("sort emp salry desc:" + list2);

            //2nd approach
            List<Employee> list3 = list.stream().sorted(Comparator.comparingLong(Employee::getSalary).reversed()).collect(Collectors.toList());
            System.out.println(list3);


            //seconnd highest number
            Integer[] arr = {23, 45, 85, 10, 20, 5, 1};
            Integer integer = Arrays.stream(arr).sorted(Comparator.reverseOrder()).skip(1).findFirst().get();
            System.out.println("seconnd highest number:: " + integer);


            //Write a Java stream api to find the duplicate words and their number of occurrences in a string?
            String s = "This is a test a sentence. This is a another test This sentence.";
            Map<String, Long> wordOccurrences = Arrays.stream(s.split("\\s+"))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            wordOccurrences.entrySet().stream()
                    .filter(entry -> entry.getValue() > 1)
                    .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));


            //  Write a Java program to count the number of words in a string?
            String sentence = "This is a sample sentence.";
            long wordCount = Arrays.stream(sentence.split("\\s+")).count();
            System.out.println("Number of words: " + wordCount);
            //
            List<String> words = Arrays.asList(sentence.split(" "));
            long wc = words.stream().count();
            System.out.println("The number of words in the string is: " + wc);
            //
            String[] word = sentence.trim().split(" ");
            System.out.println("Number of words in the string = " + word.length);

            // Write a Java program to count the total number of occurrences of a given character in a string without using any loop?
            String sen = "This is a sample sentence.";
            char targetChar = 's';
            long charCount = sen.chars()
                    .filter(ch -> ch == targetChar)
                    .count();
            System.out.println("Total occurrences of '" + targetChar + "': " + charCount);
            //
            char ch = 's';
            long count = Stream.of(sen.split(""))
                    .filter(e -> e.equals(Character.toString(ch)))
                    .count();
            System.out.println("The number of occurrences of the character '" + ch + "' : " + count);
            //
            char c = 's';
            int coun = s.length() - s.replace("a", "").length();
            System.out.println("Number of occurances of 's' in : " + coun);

            int[] numbers = {5, 2, 8, 1, 6, 3, 9, 4, 7};
            Arrays.stream(numbers).filter(i -> i % 2 == 0).boxed().sorted(Comparator.reverseOrder()).forEach(System.out::println);


            //  Using Java 8 how can I get count of consecutive characters in a string. like
            //String str = "aabbbccddbb"; // output: 2a3b2c2d2b.  Please note that count of b is not 5
            String input = "aabbbccddbb";
            String result = Arrays.stream(input.split("(?<=(.))(?!\\1)"))
                    .map(s1 -> s1.length() + Character.toString(s1.charAt(0)))
                    .collect(Collectors.joining());
            System.out.println(result);

            List<String> fruits = Arrays.asList("apple", "orange", "banana", "mango", "apricot", "kiwi");

            //  To count all the fruits whose names begin with "a" or "o"
            Map<String, Long> countMap = fruits.stream()
                    .filter(fruit -> fruit.startsWith("a") | fruit.startsWith("o"))
                    .collect(Collectors.groupingBy(fruit -> fruit, Collectors.counting()));

            System.out.println(countMap);


            //second highest salary
            Map<String, Long> map1 = new HashMap<>();
            map1.put("John", 5000L);
            map1.put("Jane", 6000L);
            map1.put("Alice", 6000L);
            map1.put("Bob", 7000L);
            map1.put("Eve", 5500L);

            List<Map.Entry<String, Long>> secondHighest = map1.entrySet().stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toList());

            System.out.println(secondHighest.get(1));

            int[] a = {1, 2, 1, 3, 4};

            List<String> pairs = new ArrayList<>();
            for (int i = 0; i < a.length - 1; i++) {
                String pair = "(" + a[i] + "," + a[i + 1] + ")";
                pairs.add(pair);
            }

            System.out.println(String.join(" ", pairs));

            //Count the Occurrences of Each Character
            String str = "ArunKumarGupta";
            Map<String, Long> res = Arrays.stream(str.split(""))
                    .map(String::toLowerCase)
                    .collect(Collectors.groupingBy(sa -> sa, Collectors.counting()));
            System.out.println(res);

            Pattern.compile(".").matcher(str).results().map(m -> m.group().toLowerCase()).
                    collect(Collectors.groupingBy(sa1 -> sa1, LinkedHashMap::new,
                            Collectors.counting())).forEach((k, v) -> System.out.println(k + " = " + v));


        }

        //Count occurrence of a given character in a string using Stream API in Java
        public static long countOcce(String s, char ch) {
            //  return  s.chars().filter(c->c == ch).count();
            return Collections.frequency(Arrays.asList(s.split("")), String.valueOf(ch));

        }

        @Test
        public void countOcceTest() {
            String s = "abccdefgaa";
            char ch = 'a';
            System.out.println(countOcce(s, ch));
            assertEquals(3, countOcce(s, ch));
        }

        //**********How to calculate a frequency map in Java 8+

        //Frequency map of list elements
        public static Map<String, Long> frequMap(List<String> elements) {
            return elements.stream().collect(Collectors.groupingBy(Function.identity(), HashMap::new, Collectors.counting()));
            // return elements.stream().map(String ::toLowerCase).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));

        }

        @Test
        public void frequMapTest() {
            List<String> list = Arrays.asList("hello", "hello", "mighty");
            //  List<String> list = Arrays.asList("Sam", "james", "Selena", "JAMes", "Joe", "sam", "JamES");
            System.out.println(frequMap(list));
        }

        //Frequency map of a character array
        public static Map<Character, Long> freqCharMap(List<Character> elements) {
            return elements.stream().collect(Collectors.groupingBy(Function.identity(), HashMap::new, Collectors.counting()));
        }

        @Test
        public void freqCharMapTest() {
            Character[] ch = {'a', 'b', 'b', 'c', 'c', 'c'};
            System.out.println(freqCharMap(List.of(ch)));
        }

        //Frequency map of characters in a string
        public static Map<Character, Long> freqStringCharMap(String s) {
            return s.chars().mapToObj(c -> (char) c).collect(Collectors.groupingBy(Function.identity(), HashMap::new, Collectors.counting()));
        }

        @Test
        public void freqStringCharMapTest() {
            String s = "abcb";
            System.out.println(freqStringCharMap(s));
        }

        //Frequency map of an integer array or unboxed type
        public static Map<Integer, Long> freqIntegerMap(int[] arr) {
            return Arrays.stream(arr).boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }

        @Test
        public void freqIntegerMapTest() {
            int[] arr = {1, 2, 2, 2, 3, 3};
            System.out.println(freqIntegerMap(arr));
        }

        // How to find the duplicate elements from a Collection
        public Set<String> findDuplicateElem(List<String> list) {

            //   return list.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            //         .entrySet().stream().filter(m -> m.getValue() > 1).map(Map.Entry::getKey).collect(Collectors.toSet());

            Set<String> set = new HashSet<>();
            return list.stream().filter(i -> !set.add(i)).collect(Collectors.toSet());

        }

        @Test
        public void findDuplicateElemTest() {
            List<String> list = Arrays.asList("ant", "1", "cat", "ant", "we", "java", "cat");
            System.out.println(findDuplicateElem(list));
        }

        //java 8 stream program to sort by each string length from list in descending order
        public List<String> sortEachStringLen(List<String> list) {
            return list.stream().sorted((s1, s2) -> Integer.compare(s2.length(), s1.length())).collect(Collectors.toList());
        }

        @Test
        public void sortEachStringLenTest() {
            List<String> stringList = Arrays.asList("apple", "banana", "cherry", "date", "fig");
            System.out.println(sortEachStringLen(stringList));
        }

        //Given a list of integers, find out all the even numbers that exist in the list using Stream functions?
        public void evenNum(List<Integer> list) {
            list.stream().filter(i -> i % 2 == 0).forEach(System.out::println);
        }

        @Test
        public void evenNumTest() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 32);
            evenNum(list);
        }

        //Given a list of integers, find out all the numbers starting with 1 using Stream functions?
        public void findFirstStartNum(List<Integer> list) {
            list.stream().map(s -> s + "").filter(s -> s.startsWith("1")).forEach(System.out::println);
        }

        @Test
        public void findFirstStartNumTest() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 32);
            findFirstStartNum(list);
        }

        // How to find duplicate elements in a given integers list in java using Stream functions?
        public void duplicateNum(List<Integer> list) {
            Set<Integer> set = new HashSet<>();
            list.stream().filter(i -> !set.add(i)).forEach(System.out::println);
        }

        @Test
        public void duplicateNumTest() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            duplicateNum(list);
        }

        //Given the list of integers, find the first element of the list using Stream functions?
        public void findFirstElem(List<Integer> list) {
            list.stream().findFirst().ifPresent(System.out::println);
        }

        @Test
        public void findFistElem() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            findFirstElem(list);
        }

        //Given a list of integers, find the total number of elements present in the list using Stream functions?
        public void findAllElem(List<Integer> list) {
            System.out.println(list.stream().count());
        }

        @Test
        public void findAllElemTest() {
            List<Integer> integerList = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            findAllElem(integerList);
        }

        //Given a list of integers, find the maximum value element present in it using Stream functions?
        public int FindMaxmumValue(List<Integer> list) {
            return list.stream().max(Integer::compare).get();
        }

        @Test
        public void FindMaxmumValueTest() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            System.out.println(FindMaxmumValue(list));
        }

        //Given a String, find the first non-repeated character in it using Stream functions?
        public char findFirstNonRepeatedChar(String s) {
            return s.chars().mapToObj(i -> Character.toLowerCase((char) i))
                    .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .filter(i -> i.getValue() == 1)
                    .map(Map.Entry::getKey)
                    .findFirst().get();
        }

        @Test
        public void findFirstNonRepeatedCharTest() {
            String s = "Java articles are Awesome";
            System.out.println(findFirstNonRepeatedChar(s));
        }

        // Given a String, find the first repeated character in it using Stream functions?
        public char findfirstRepeatedChar(String s) {
            return s.chars().mapToObj(i -> (char) i)
                    .collect(Collectors.groupingBy(o -> o, LinkedHashMap::new, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .filter(i -> i.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .findFirst().get();
        }

        @Test
        public void findfirstRepeatedCharTest() {
            String s = "Java articles are Awesome";
            System.out.println(findfirstRepeatedChar(s));
        }

        //Given a list of integers, sort all the values present in it using Stream functions?
        public void sortAll(List<Integer> list) {
            list.stream().sorted().forEach(System.out::println);
        }

        @Test
        public void sortAllTest() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            sortAll(list);
        }

        //Given a list of integers, sort all the values present in it in descending order using Stream functions?
        public void sortAllDesc(List<Integer> list) {
            list.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
        }

        @Test
        public void sortAllDesc() {
            List<Integer> list = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
            sortAllDesc(list);
        }

        // Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

        public boolean containsDuplicate(int[] nums) {
            List<Integer> list = Arrays.stream(nums).boxed().toList();
            Set<Integer> set = new HashSet<>(list);
            if (set.size() == list.size())
                return false;
            return true;
        }

        @Test
        public void containsDuplicateTest() {
            int[] nums = {1, 2, 3, 4};
            System.out.println(containsDuplicate(nums));
        }

        //Write a program to print the count of each character in a String?
        public void countEachCharInStr(String s) {
            Map<String, Long> counteachchar = Arrays.stream(s.split("")).map(String::toLowerCase).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            System.out.println(counteachchar);
        }

        @Test
        public void countEachCharInStrTest() {
            String s = "string data to count each character";
            countEachCharInStr(s);
        }

        // Write a Program to find the Maximum element in an array?
        public void findMaxEle(int[] arr) {
            int asInt = Arrays.stream(arr).max().getAsInt();
            System.out.println(asInt);
        }

        @Test
        public void findMaxEleTest() {
            int[] arr = {3, 78, 56, 88, 90, 1, 5, 0};
            findMaxEle(arr);
        }

        //How to find only duplicate elements with its count from the String ArrayList in Java8?
        public void findDuplicateCount(List<String> lis) {
            Map<String, Long> count = lis.stream().filter(x -> Collections.frequency(lis, x) > 1).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            System.out.println(count);
        }

        @Test
        public void findDuplicateCountTest() {
            List<String> list = Arrays.asList("AA", "BB", "AA", "CC");
            findDuplicateCount(list);
        }

        // Java 8 Map Example
        public void findValue(Map<String, String> map) {
            Map<String, List<String>> output = map.entrySet().stream().collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.mapping(Map.Entry::getKey, Collectors.toList())));
            //System.out.println(output);
            for (Map.Entry<String, List<String>> listEntry : output.entrySet()) {
                System.out.println(listEntry.getKey() + "-" + String.join(",", listEntry.getValue()));
            }
        }

        @Test
        public void findValueTest() {
            Map<String, String> map = new HashMap<>();
            map.put("key1", "value1");
            map.put("key2", "value1");
            map.put("key3", "value2");
            map.put("key4", "value2");

            findValue(map);
        }

        public void findByGroupCounting(Map<String, String> grades) {
            Map<String, Long> groupGrades = grades.values().stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            System.out.println(groupGrades);
        }

        @Test
        public void findByGroupCountingTest() {
            Map<String, String> grades = new HashMap();
            grades.put("100", "A");
            grades.put("101", "B");
            grades.put("102", "A");
            grades.put("103", "C");
            grades.put("104", "D");
            grades.put("105", "B");
            grades.put("106", "B");
            grades.put("107", "C");
            findByGroupCounting(grades);
        }

        public void reverseEachWord(String s) {
            List<StringBuilder> reverseWord = Arrays.stream(s.split(" ")).map(i -> new StringBuilder(i).reverse()).collect(Collectors.toList());
            System.out.println(reverseWord);
        }

        @Test
        public void reverseEachWordTest() {
            String s = "arun kumar gupta";
            reverseEachWord(s);
        }

        public void evenOddCountEachWord(String str) {
            long evenCount = Arrays.stream(str.split(" "))
                    .filter(i -> i.length() % 2 == 0)
                    .count();
            long OddCount = Arrays.stream(str.split(" "))
                    .filter(i -> i.length() % 2 != 0)
                    .count();
            System.out.println("Even word:" + evenCount + "\n" + "Odd word:" + OddCount);
        }

        @Test
        public void evenOddCountEachWordTest() {
            String str = "Today is my best day so far";
            evenOddCountEachWord(str);
        }
        //  System.out.println(evenOddCount);

        /*1. find second highest or maximum salary of Employee
-----------------------------------------------------
Ans- Optional<Employee> emp = employees.stream().sorted(Comparator.comparingDouble(Employee::getSalary).reversed()).skip(1).findFirst();
     System.out.println(emp.get());
----------------------------------------------------------------------

List<Employee> sortedList = employees.stream().sorted(Comparator.comparingDouble(Employee::getSalary).reversed()).collect(Collectors.toList());
System.out.println(sortedList);*/

        public int secondHighest(int[] nums) {
            int first = Integer.MIN_VALUE;
            int second = Integer.MIN_VALUE;
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] > first) {
                    second = first;
                    first = nums[i];
                } else if (nums[i] > second) {
                    second = nums[i];
                }
            }
            return second;
        }

        //  Integer integer1 = list.stream().sorted(Comparator.reverseOrder()).skip(1).findFirst().get();
        @org.junit.jupiter.api.Test
        public void secondHighestTest() {
            int[] arr = {5, 1, 7, 3, 10, 9, 13, 14};
            int res = secondHighest(arr);
            assertEquals(13, res);
        }

        public void findList(List<String> list) {
            List<String> modifiedList = list.stream()
                    .filter(str -> str != null && (str.equals("Australia") || str.equals("Africa")))
                    .map(str -> str + "AA")
                    .collect(Collectors.toList());
            System.out.println(modifiedList);
        }

        @Test
        public void FindListTest() {
            List<String> list = Arrays.asList(null, "Australia", "India", "Africa", "Indonesia", "Malaysia");
            findList(list);
        }

        public static String findUncommonCharacters(String s1, String s2) {
            Map<Character, Integer> map = new TreeMap<>();
            for (char c : s1.toCharArray()) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }
            for (char c : s2.toCharArray()) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }

            StringBuilder sb = new StringBuilder();
            for (char c : map.keySet()) {
                if (map.get(c) == 1 ) {
                    sb.append(c);
                }
            }
            return sb.toString();

        }

        @Test
        public void findUncommonCharactersTest() {
            System.out.println(findUncommonCharacters("characters", "alphabets")); // bclpr
        }

        public static String[] findUncommonWord(String s1, String s2) {
            Map<String, Integer> map = new HashMap<>();
            for (String word1 : s1.split(" ")) {
                map.put(word1, map.getOrDefault(word1, 0) + 1);
            }
            for (String word2 : s2.split(" ")) {
                map.put(word2, map.getOrDefault(word2, 0) + 1);
            }
            List<String> ans = new LinkedList<>();
            for (String s : map.keySet()) {
                if (map.get(s) == 1) {
                    ans.add(s);
                }
            }
            return ans.toArray(new String[0]);
        }
    @Test
        public void findUncommonWordTest() {
        System.out.println(Arrays.toString(findUncommonWord("I'm arun", "I'm gupta")));
     }

        public static boolean isRotation(String mainString, String testString) {
            if (mainString.length() != testString.length()) return false;
            String concatenatedString = mainString + mainString;
            return concatenatedString.contains(testString);
        }
        @org.junit.jupiter.api.Test
        public void isRoTest(){
            String mainString = "abcd";
            String string1 = "bcda";
            String string2 = "cdab";
            String string3 = "cbda";
            System.out.println(isRotation(mainString, string1)); // Output: true
            System.out.println(isRotation(mainString, string2)); // Output: true
            System.out.println(isRotation(mainString, string3));
        }
        public void primeNumber() {
            IntStream.range(2, 100).filter(i -> IntStream.rangeClosed(2, (int) Math.sqrt(i))
                    .noneMatch(j -> i % j == 0)).forEach(System.out::println);
        }
        @Test
        public void primeNumberTest(){
            primeNumber();
        }

        public void countEachNum(int[] arr){

            Arrays.stream(arr).boxed().collect(Collectors.groupingBy(s -> s)).forEach((k, v) -> System.out.println(v.size()+","+k));

        }
        @Test
        public void countEachNumTest(){
            int[] arr = {1,2,3,2,1,1};
            countEachNum(arr);
        }

        List<Customer> customers = Arrays.asList(new Customer("B", 23), new Customer("A", 21), new Customer("F", 24),
                new Customer("D", 25),new Customer("C", null),null,new Customer(null, 22));
        // TODO: write the code to sort customers by age
        @Test
        public void sortedByAgeTest() {
            List<Customer> sortedCustomers = customers.stream().filter(c -> c != null && c.getAge() != null).sorted(Comparator.comparing(Customer::getAge))
                    .collect(Collectors.toList());
            System.out.println(sortedCustomers);
        }

     //    How to sum a list of integers with java streams?

        @Test
        public void sumOfListInteger(){
            List<Integer> list = Arrays.asList(2, 4, 5, 6);
         int sum = list.stream().collect(Collectors.summingInt(Integer::intValue));
          //  int sum = list.values().stream().reduce(0, Integer::sum);
            System.out.println(sum);
        }

        @Test
        public void SecondHighestSalary(){
            Map<String,Integer> map2 = new HashMap<>();
            map2.put("anil",1000);
            map2.put("ankit",1200);
            map2.put("bhavna",1200);
            map2.put("james",1200);
            map2.put("micael",1000);
            map2.put("tom",1300);
            map2.put("daniel",1300);

            Map.Entry<Integer,List<String>> finalResult2 =
                    map2.entrySet().stream().collect(Collectors.groupingBy(entry ->entry.getValue(),
                            Collectors.mapping(entry -> entry.getKey(),Collectors.toList())
                    )).entrySet().stream().sorted(Comparator.comparing(it -> it.getKey())).toList().get(1);
            System.out.println(finalResult2);
        }


        @Test
        public void lowestHighestSalary(){
          /*  List<Employee> empsWithLowestSalary = employee.stream()
                    .collect(Collectors.groupingBy(Employee::getSalary, TreeMap::new, Collectors.toList()))
                    .firstEntry()
                    .getValue();*/
        }


        @Test
        public void sameSalary(){
            List<Employee1> employees = new ArrayList<>();

            employees.add(new Employee1(1, "John" , 1000));
            employees.add(new Employee1(1, "Peter" , 2000));
            employees.add(new Employee1(1, "Ben" , 3000));
            employees.add(new Employee1(1, "Steve" , 2000));
            employees.add(new Employee1(1, "Parker" , 1000));

           // employees.entrySet().stream().filter(e -> e.getValue().size()>1)
           //         .forEach((e) -> System.out.println( "Salary :: " + e.getKey() + " is same for " + e.getValue()));

            /*
        Map<String, Double> map = list.stream().collect(Collectors.groupingBy
                (Employee::getDepartment,Collectors.summingDouble(Employee::getSalary)));
        System.out.println(map);
        //Highest Salary
        Map<String, Double> mapSa = list.stream().collect(Collectors.groupingBy
                (e->e.getDepartment(),TreeMap::new,Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparingDouble(e->e.getSalary())),o->o.get().getSalary())));
*/
        }

        //How to convert a Java 8 Stream to an Array?
        @Test
        public void convertJava8StreamToArray(){
            Stream<String> stringStream = Stream.of("a", "b", "c");
          //  String[] stringArray = stringStream.toArray(String[]::new);
            String[] stringArray = stringStream.toArray(size -> new String[size]);
            Arrays.stream(stringArray).forEach(System.out::println);

            
        }

        //Java 8 List<V> into Map<K, V>
        @Test
        public void listToMap(){
            List<Employee1> employees = new ArrayList<>();

            employees.add(new Employee1(1, "John" , 1000));
            employees.add(new Employee1(1, "Peter" , 2000));
            employees.add(new Employee1(1, "Ben" , 3000));
            employees.add(new Employee1(1, "Steve" , 2000));
            employees.add(new Employee1(1, "Parker" , 1000));
            Map<String, Employee1> result =
                    employees.stream().collect(Collectors.toMap(Employee1::getName,
                            Function.identity()));
            System.out.println(result);

            //or
            Map<String, List<Employee1>> res =
                    employees.stream().collect(Collectors.groupingBy(Employee1::getName));
            System.out.println(res);
        }

        @Test
        public void sum(){
            List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);
            Integer sum = integers.stream().reduce(0, (a, b) -> a + b);
            System.out.println(sum);

            Integer sum1 = integers.stream().reduce(0, Integer::sum);
            System.out.println(sum1);

            Integer sum2 = integers.stream().mapToInt(Integer::intValue).sum();
            System.out.println(sum2);

            Integer sum3 = integers.stream().collect(Collectors.summingInt(Integer::intValue));
            System.out.println(sum3);

            String string = "Item1 10 Item2 25 Item3 30 Item4 45";
            Integer sum4 = Arrays.stream(string.split(" "))
                    .filter((s) -> s.matches("\\d+"))
                    .mapToInt(Integer::valueOf)
                    .sum();
            System.out.println(sum4);

          



            @Getter @Setter @AllArgsConstructor
          class Item {
                private int id; private Integer price;
            }

            List<Item> items = Arrays.asList(
                    new Item(1, 10),
                    new Item(2, 15),
                    new Item(3, 25),
                    new Item(4, 40));

            Integer sum5 =items.stream().map(Item::getPrice).reduce(0, Integer::sum);
            System.out.println(sum5);
            Integer sum6 = items.stream().map(item -> item.getPrice()).reduce(0, (a, b) -> a + b);
            System.out.println(sum6);
            Integer sum7 = items.stream().map(Item::getPrice).mapToInt(Integer::intValue).sum();
            System.out.println(sum7);
            Integer sum8 = items.stream().mapToInt(Item::getPrice).sum();
            System.out.println(sum8);


            int n = 10;
            int sum_n = IntStream.rangeClosed(1, n).sum();
            System.out.println("The sum of integers from 1 through " + n + " is " + sum_n);

          //  IntStream natural = IntStream.generate(new AtomicInteger()::getAndIncrement);
        }

        @Test
        public void sumOfPower(){
            int n = 10;
            var sum = IntStream.range(0,n).map(i->(int)Math.pow(2,i)).sum();
            System.out.println("The sum of powers of 2 from 0 to " + n + " is " + sum);

        }

        //Java 8 Stream skip() vs limit()
        @Test
        public void Stream_skipVslimit(){
         //   Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).filter(i -> i % 2 == 0).skip(2).forEach(i -> System.out.print(i + " "));
           Stream.of(1,2,3,4,5,6,7,8,9,10).filter(i->i%2 == 0).skip(2).forEach(System.out::println);
            Stream.of(1,2,3,4,5,6,7,8,9,10).filter(i->i%2 ==0).limit(2).forEach(i-> System.out.print(i+" "));



        }
        // Collecting Stream Elements into a List in Java
        @Test
        public void StreamElementsList(){

            Stream<String> isoCountries = Stream.of(Locale.getISOCountries());
            System.out.println(Arrays.toString(isoCountries.toArray()));

            List<String> result = Stream.of(Locale.getISOCountries()).collect(Collectors.toList());
            System.out.println(result);

            List<String> result1 = Stream.of(Locale.getISOCountries()).collect(Collectors.toUnmodifiableList());
            System.out.println(result1);

            List<String> result2 = Stream.of(Locale.getISOCountries()).toList();
            System.out.println(result2);

        }

      //  When to Use a Parallel Stream in Java
      @Test
      public void ParallelStream(){

          List<Integer> listofNum = Arrays.asList(1,2,3,4);
          //Sequential Streams
        listofNum.stream().forEach(num-> System.out.println(num+" "+Thread.currentThread().getName()));


        //Parallel Streams
          listofNum.parallelStream().forEach(i-> System.out.println(i+" "+Thread.currentThread().getName()));

          //Splitting Source
       //  int sum =   listofNum.parallelStream().r(5,Integer::sum);
          int sum = listofNum.parallelStream().reduce(0, Integer::sum) + 5;
          System.out.println(sum);

          int reduce = IntStream.rangeClosed(1, 100).reduce(0, Integer::sum);
          int reduce1 = IntStream.rangeClosed(1, 100).parallel().reduce(0, Integer::sum);
          System.out.println(reduce1);
      }

       //10 Examples of Stream API in Java 8 - count + filter + map + distinct + collect() Examples
        @Test
        public void streamapi(){

            // Count the empty strings
           List<String> stringList = Arrays.asList("abc","","bcd","","defg","jk");
           var count = stringList.stream().filter(i->i.isEmpty()).count();
            System.out.printf("%s = %d",stringList,count);

            // // Count String with length more than 3
           var  count1 = stringList.stream().filter(i->i.length()>3).count();
            System.out.printf("%s = %d",stringList,count1);

            // Count number of String which startswith "a"
            long a = stringList.stream().filter(i -> i.startsWith("a")).count();
            System.out.println("\n"+ "a ="+ a);


            Integer[] arr  = {1,2,3,4,5,6,7,8,9,10,60,65};   // 6,60,65
            var printnum = Arrays.stream(arr)
                    .filter(num -> String.valueOf(num).startsWith("6"))
                    .collect(Collectors.toList());
            System.out.println(printnum);


            Integer[] arr1  = {1,25,3,4,15,6,7,8,5,10,60,65};   // 
            var printnu = Arrays.stream(arr1)
                    .filter(num -> !String.valueOf(num).startsWith("5"))
                    .collect(Collectors.toList());
            System.out.println(printnu);

            // // Remove all empty Strings from List
            List<String> filterd = stringList.stream().filter(i->!i.isEmpty()).toList();
            System.out.println(filterd);

            // // Create a List with String more than 2 characters
           var ss =  stringList.stream().filter(i->i.length() > 2).toList();
            System.out.println(ss);

            // Create List of square of all distinct numbers
            List<Integer> num = Arrays.asList(9,10,3,4,7,3,4);
            var sq = num.stream().map(i->i*i).distinct().toList();
            System.out.println(sq);

        }

     
     

    




    }




@Getter @Setter @ToString @AllArgsConstructor
class Customer { private String name;private Integer age;
}

    @Getter @Setter @ToString @AllArgsConstructor
    class Employee1{ int id; String name; int salary;
    };
