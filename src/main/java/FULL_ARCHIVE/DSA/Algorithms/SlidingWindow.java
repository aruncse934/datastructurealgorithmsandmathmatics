package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SlidingWindow {

    //Longest Turbulent Subarray
    public int maxTurbulenceSize(int[] arr){
        int n = arr.length;
        int ans=1,anchor = 0;
        for(int i =1; i < n; i++){
            int c= Integer.compare(arr[i-1], arr[i]);
            if(c == 0){
                anchor = i;
            } else if(i == n-1 || c * Integer.compare(arr[i],arr[i+1] )!= -1){
                ans = Math.max(ans,i-anchor+1);
                anchor = i;
            }
        }
        return ans;
    }
    @Test
    public void  maxTurbulenceSizeTest(){
        int[] arr = {9,4,2,10,7,8,8,1,9};
        int res = maxTurbulenceSize(arr);
        assertEquals(5,res);
    }

    //340. Longest Substring With K Unique Characters | Variable Size Sliding Window
      public static int findLongestSubStrWithKUniqueChars(String s, int k){
        if(s==null || s.length() == 0 || k <= 0) return -1 ;
          int left = 0, maxLeft = 0, maxLen = 0;
        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i < s.length(); i++){
            char currChar = s.charAt(i);
            map.put(currChar,map.getOrDefault(currChar,0)+1);
            while (map.size() > k){
                char leftChar = s.charAt(left);
                map.put(leftChar,map.get(leftChar)-1);
                if(map.get(leftChar) == 0){
                    map.remove(leftChar);
                }
                left++;
            }
            if(i - left + 1 > maxLen){
                maxLen = i - left + 1;
                maxLeft = left;
            }
        }
        return s.substring(maxLeft,maxLeft+maxLen).length();
      }

      @Test
    public void findLongestSubStrWithKUniqueCharsTest(){
        String s = "aabacbebebe";
        int k = 3;
          System.out.println(findLongestSubStrWithKUniqueChars(s,k));
      }

      //Problems Solvable using this template
    //3. Longest Substring Without Repeating Characters
      public int lengthOfLongestSubstring(String s) {
          int result = 0;
          int left = 0;
          int[] arr = new int[128];
          for (int i = 1; i <= s.length(); i++) {
              char temp = s.charAt(i - 1);
              if (arr[temp] != 0) {
                  left = Math.max(left, arr[temp]);
              }
              result = Math.max(result, i - left);
              arr[temp] = i;
          }
          return result;
      }

    //159. Longest Substring with At Most Two Distinct Characters (Medium)
    //340. Longest Substring with At Most K Distinct Characters //   while (distinctCount > k) {
      public int lengthOfLongestSubstringTwoDistinct(String s) {
          int maxLength = 0;
          int left = 0;
          int[] charIndex = new int[128]; // Assuming ASCII characters
          int distinctCount = 0;

          for (int right = 0; right < s.length(); right++) {
              char c = s.charAt(right);
              if (charIndex[c]++ == 0) {
                  distinctCount++;
              }

              while (distinctCount > 2) {
                  char leftChar = s.charAt(left++);
                  if (--charIndex[leftChar] == 0) {
                      distinctCount--;
                  }
              }

              maxLength = Math.max(maxLength, right - left + 1);
          }

          return maxLength;
      }
    //340. Longest Substring with At Most K Distinct Characters
    //424. Longest Repeating Character Replacement
    //487. Max Consecutive Ones II
    //713. Subarray Product Less Than K
    //1004. Max Consecutive Ones III
    //1208. Get Equal Substrings Within Budget (Medium)
    //1493. Longest Subarray of 1's After Deleting One Element
    //1695. Maximum Erasure Value
    //1838. Frequency of the Most Frequent Element
    //2009. Minimum Number of Operations to Make Array Continuous
    //2024. Maximize the Confusion of an Exam
    //The following problems are also solvable using the shrinkable template with the "At Most to Equal" trick
    //
    //930. Binary Subarrays With Sum (Medium)
    //992. Subarrays with K Different Integers
    //1248. Count Number of Nice Subarrays (Medium)
    //2062. Count Vowel Substrings of a String (Easy)
    
    /*  Fix Sized Window
    30. Substring with Concatenation of All Words
187. Repeated DNA Sequences
438. Find All Anagrams in a String
567. Permutation in String
643. Maximum Average Subarray I
1100. Find K-Length Substrings With No Repeated Characters (P)
1151. Minimum Swaps to Group All 1's Together (P)
1176. Diet Plan Performance (P)
1343. Number of Sub-arrays of Size K and Average Greater than or Equal to Threshold
1423. Maximum Points You Can Obtain from Cards
1456. Maximum Number of Vowels in a Substring of Given Length
1652. Defuse the Bomb
1876. Substrings of Size Three with Distinct Characters
2090. K Radius Subarray Averages
2269. Find the K-Beauty of a Number
2379. Minimum Recolors to Get K Consecutive Black Blocks
Variable Sized Window
3. Longest Substring Without Repeating Characters
76. Minimum Window Substring
159. Longest Substring with At Most Two Distinct Characters
209. Minimum Size Subarray Sum
340. Longest Substring with At Most K Distinct Characters (P)
424. Longest Repeating Character Replacement
487. Max Consecutive Ones II (P)
904. Fruit Into Baskets
992. Subarrays with K Different Integers
1004. Max Consecutive Ones III
1297. Maximum Number of Occurrences of a Substring
1493. Longest Subarray of 1's After Deleting One Element
1695. Maximum Erasure Value
1852. Distinct Numbers in Each Subarray (P)
2024. Maximize the Confusion of an Exam
2062. Count Vowel Substrings of a String*/
}

