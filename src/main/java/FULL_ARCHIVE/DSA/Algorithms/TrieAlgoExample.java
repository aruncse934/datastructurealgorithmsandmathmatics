package FULL_ARCHIVE.DSA.Algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrieAlgoExample {
    int[] exists = new int[26];
    Trie root = new Trie();

    public List<String> wordBreak(String str, List<String> wordDict) {
        List<String> result = new ArrayList();
        for (String w : wordDict) {
            root.insert(w);
        }
        for (char ch : str.toCharArray()) {
            if (exists[ch - 'a'] != 1)
                return new ArrayList();
        }
        search(result, root, new StringBuilder(), str.toCharArray(), 0);
        return result;
    }

    public void search(List<String> result, Trie curr, StringBuilder temp, char[] given, int index) {
        if (index == given.length) return;
        if (curr.children[given[index] - 'a'] == null) return;
        Trie next = curr.children[given[index] - 'a'];
        temp.append(given[index]);
        if (next.isWord) {
            if (index + 1 == given.length) {
                result.add(temp.toString());
                temp.deleteCharAt(temp.length() - 1);
                return;
            } else {
                temp.append(" ");
                search(result, root, temp, given, index + 1);
                temp.deleteCharAt(temp.length() - 1);
            }
        }
        search(result, next, temp, given, index + 1);
        temp.deleteCharAt(temp.length() - 1);
    }


    class Trie {
        boolean isWord;
        Trie[] children;

        Trie() {
            isWord = false;
            children = new Trie[26];
        }

        public void insert(String str) {
            Trie curr = this;
            for (int i = 0; i < str.length(); i++) {
                int ch = str.charAt(i) - 'a';
                exists[ch]=1;
                if (curr.children[ch] == null) {
                    curr.children[ch] = new Trie();
                }
                curr = curr.children[ch];
            }
            curr.isWord = true;
        }
    }
    @org.junit.jupiter.api.Test
    public void mergeTest(){
        String s = "catsanddog";
        List<String> wordDict = Arrays.asList("cat", "cats", "and", "sand", "dog");
        System.out.println( wordBreak(s,wordDict));

    }
}
