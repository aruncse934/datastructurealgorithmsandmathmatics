package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Greedy {

    // Majority Element
    

    //Jump Game
    public boolean canJumpGame(int[] nums){
        int curMax = nums[0];
        for(int i = 1; i < nums.length; i++){
            if(curMax < i) return false;
            curMax = Math.max(curMax, i+nums[i]);
        }
        return true;
    }

    @Test
    public void canJumpGameTest(){
        int[] arr = {2,3,1,1,4};
        boolean res = canJumpGame(arr);
        assertTrue(res);
    }

    //Jump Game II
    public int jumGameII(int[] nums){
        int res =0, r = 0,l = 0, fur = 0;
        while(r < nums.length-1){
            fur =0;
            for(int i = l; i <=r;i++)
                fur = Math.max(fur,i+nums[i]);
            l = r+1;
            r = fur;
            res++;
        }
        return res;
    }
      //  Minimum number of jumps
    public int jumpGame2(int[] nums){
        int jumps = 0, curEnd = 0, curFarthest = 0;
        for(int i = 0; i < nums.length-1;i++){
            curFarthest = Math.max(curFarthest, i+nums[i]);
            if(i == curEnd) {
                jumps++;
                curEnd = curFarthest;
            }
        }
        return jumps;
    }
    @Test
    public void jumGameIITest(){
        int[] nums = {1, 4, 3, 2, 6, 7};
        int res = jumpGame2(nums);
        assertEquals(2,res);
    }
    //45. Jump Game II
    public int jump(int[] nums) {
        int jump = 0, maxLen=0, end = 0;
        for(int i = 0; i<nums.length-1; i++){
            maxLen =  Math.max(maxLen , nums[i]+i);
            if(i == end){
                jump++;
                end = maxLen;
            }
        }
        return jump;
    }
    @Test
    public void jumpTest(){
        int[] nums = {2,3,1,1,4};
        System.out.println(jump(nums));
        int[] nums1 ={2,3,0,1,4};
        System.out.println(jump(nums1));

    }

    public static long findMaxXWithEqualBitsAndMaxV(long n) {
        long maxXor = n;
        long bits = 0;
        long temp = n;

        while (temp > 0) {
            temp >>= 1;
            bits++;
        }

        long mask = (1L << bits) - 1;

        for (long x = n ; x <= (n | mask); x++) {
            long v = n;
            for (long i = n ; i <= x; i++) {
                v ^= i;
            }
            if (v > maxXor) {
                maxXor = v;
            }
        }


        return maxXor;
    }
    @Test
    public void  findMaxXWithEqualBitsAndMaxVTest(){
        long n = 12;
        long res = findMaxXWithEqualBitsAndMaxV(n);
        assertEquals(14,res);
    }



    // Maximum Subarray
    public int maxSubArray(int[] nums){
        if(nums.length == 1) return nums[0];
        int sum =0;
        int max = Integer.MIN_VALUE;
        for(int n :nums){
            sum +=n;
            max = Math.max(max,sum);
            if(sum<0){
                sum =0;
            }
        }
        return max;
    }
   @Test
    public void maxSubarrayTest(){
       int[] nums =  {-2,1,-3,4,-1,2,1,-5,4};
       int res = maxSubArray(nums);
       assertEquals(6,res);
   }

    //Minimum Sum of Four Digit Number After Splitting Digits
    public int miniSum(int num){
        int[] dig = new int[4];
        int curr = 0;
        while(num > 0){
            dig[curr++] = num %10;
            num /= 10;
        }
        Arrays.sort(dig);
        return (dig[0]+dig[1])*10+dig[2]+dig[3];
    }
    @Test
    public void  miniSumTest(){
        int num = 2932;
        int res = miniSum(num);
        assertEquals(52,res);
    }
    //Gas Station
      public int canCompleteCircuit(int[] gas, int[] cost){
        int sGas = 0, sCost = 0, res = 0, total = 0;
        for(int i = 0; i < gas.length; i++){
            sGas += gas[i];
            sCost += cost[i];
        }
        if(sGas < sCost) return -1;
        for(int i = 0; i<gas.length; i++){
            total += gas[i] - cost[i];
            if(total < 0){
                total = 0;
                 res = i+1;
            }
        }
        return res;
      }
      @Test
    public void   canCompleteCircuitTest(){
        int[] gas = {1,2,3,4,5};
        int[] cost = {3,4,5,1,2};
        int res = canCompleteCircuit(gas, cost);
        assertEquals(3,res);
      }

    /*   public int canCompleteCircuit(int[] gas, int[] cost) {
        int n = gas.length;
        int total_surplus = 0;
        int surplus = 0;
        int start = 0;

        for(int i = 0; i < n; i++){
            total_surplus += gas[i] - cost[i];
            surplus += gas[i] - cost[i];
            if(surplus < 0){
                surplus = 0;
                start = i + 1;
            }
        }
        return (total_surplus < 0) ? -1 : start;
    }*/


    //Strong Password Checker
    public int strongPasswordChecker(String password) {
            Set<Character> lowercase = new HashSet<>();
            Set<Character> uppercase = new HashSet<>();
            for (char ch = 'a'; ch <= 'z'; ch++) {
                lowercase.add(ch);
                uppercase.add(Character.toUpperCase(ch));
            }
            Set<Character> digits = new HashSet<>();
            for (char ch = '0'; ch <= '9'; ch++) {
                digits.add(ch);
            }

            int numDeletions = Math.max(0, password.length() - 20);

            boolean hasLowercase = false;
            boolean hasUppercase = false;
            boolean hasDigits = false;
            for (char ch : password.toCharArray()) {
                if (lowercase.contains(ch)) {
                    hasLowercase = true;
                } else if (uppercase.contains(ch)) {
                    hasUppercase = true;
                } else if (digits.contains(ch)) {
                    hasDigits = true;
                }
            }

            int numMissingTypes = (hasLowercase ? 0 : 1) + (hasUppercase ? 0 : 1) + (hasDigits ? 0 : 1);

            List<Integer> substringLengths = countSubstringLengths(password);
            breakSubstringsWithDeletions(substringLengths, numDeletions);
            int numSubstringBreaks = 0;
            for (int length : substringLengths) {
                numSubstringBreaks += length / 3;
            }

            int numInsertions = Math.max(0, 6 - password.length());

            return numDeletions + Math.max(numMissingTypes, Math.max(numSubstringBreaks, numInsertions));
        }

        private List<Integer> countSubstringLengths(String s) {
            List<Integer> result = new ArrayList<>();
            char lastSeenCharacter = s.charAt(0);
            int count = 1;
            for (int i = 1; i < s.length(); i++) {
                if (s.charAt(i) == lastSeenCharacter) {
                    count++;
                } else {
                    result.add(count);
                    count = 1;
                }
                lastSeenCharacter = s.charAt(i);
            }
            result.add(count);
            return result;
        }

        private void breakSubstringsWithDeletions(List<Integer> substringLengths, int numDeletions) {
            while (numDeletions > 0) {
                int bestIdxToDelete = -1;
                for (int i = 0; i < substringLengths.size(); i++) {
                    if (substringLengths.get(i) >= 3 && (substringLengths.get(i) % 3 == 0 || substringLengths.get(i) % 3 == 1) &&
                            (bestIdxToDelete == -1 || substringLengths.get(i) > substringLengths.get(bestIdxToDelete))) {
                        bestIdxToDelete = i;
                    }
                }
                if (bestIdxToDelete == -1) {
                    break;
                }
                substringLengths.set(bestIdxToDelete, substringLengths.get(bestIdxToDelete) - 1);
                numDeletions--;
            }
        }

        @Test
    public void strongPasswordCheckerTest(){
        String pass = "a";
        assertEquals(5,strongPasswordChecker(pass));

            String pass1 = "aA1";
            assertEquals(3,strongPasswordChecker(pass1));

            String pass2 = "FFFFFFFFFFFFFFF11111111111111111111AAA";
            assertEquals(28,strongPasswordChecker(pass2));
        }
        
    public boolean isMatch(String s, String p){
        int sIndx = 0, pIndx = 0, match = 0, starIndx = -1;
        while(sIndx < s.length()){
            if(pIndx < p.length() && (p.charAt(pIndx) == '?' || s.charAt(sIndx) == p.charAt(pIndx))){
                sIndx++;
                pIndx++;
            } else if(pIndx < p.length() && p.charAt(pIndx) == '*'){
                starIndx = pIndx;
                match = sIndx;
                pIndx++;
            } else if(starIndx != -1){
                pIndx = starIndx + 1;
                match++;
                sIndx = match;
            }  else {
                return false;
            }
        }
        while(pIndx < p.length() && p.charAt(pIndx) == '*'){
            pIndx++;
        }
        return pIndx == p.length();
    }


    @Test
    public void isMatch(){
        String s  ="aa";
        String p = "a";
        System.out.println(isMatch(s,p));

        String s1  ="aa";
        String p1 = "*";
        System.out.println(isMatch(s1,p1));

        String s2  ="cb";
        String p2 = "?a";
        System.out.println(isMatch(s2,p2));
    }




}
