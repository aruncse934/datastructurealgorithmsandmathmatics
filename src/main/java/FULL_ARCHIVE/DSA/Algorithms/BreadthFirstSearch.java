package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Graph1 {
    private int vertices; // Number of vertices
    private LinkedList<Integer>[] adjacencyList; // Adjacency list

    // Constructor
    public Graph1(int vertices) {
        this.vertices = vertices;
        this.adjacencyList = new LinkedList[vertices];

        for (int i = 0; i < vertices; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    // Add an edge to the graph
    public void addEdge(int source, int destination) {
        adjacencyList[source].add(destination);
    }

    // Breadth-First Search traversal starting from a given source vertex
    public void bfs(int source) {
        boolean[] visited = new boolean[vertices]; // Mark all vertices as not visited

        Queue<Integer> queue = new LinkedList<>();
        visited[source] = true;
        queue.offer(source);

        while (!queue.isEmpty()) {
            int currentVertex = queue.poll();
            System.out.print(currentVertex + " ");

            // Visit all adjacent vertices
            for (int neighbor : adjacencyList[currentVertex]) {
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    queue.offer(neighbor);
                }
            }
        }
    }
}

// Main class

public class BreadthFirstSearch {
     @Test
    public void bd(){
        Graph1 graph = new Graph1(6);
        // Add edges
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);

        // Perform BFS starting from vertex 0
        System.out.println("Breadth-First Search starting from vertex 0:");
        graph.bfs(0);
    }

   // Binary Tree Level Order Traversal
   public List<List<Integer>> levelOrder(TreeNode root) {
       List<List<Integer>> res = new ArrayList<>();
       if (root == null)return res;
       Queue<TreeNode> queue = new LinkedList<>();
       queue.add(root);
       while(!queue.isEmpty()){
           List<Integer> list = new ArrayList<>();
           int levelSize = queue.size();
           for(int i = 0; i < levelSize; i++){
               TreeNode node = queue.poll();
               list.add(node.val);
               if(node.left != null){ queue.add(node.left);}
               if(node.right != null){ queue.add(node.right);}
           }
           res.add(list);
       }
       return res;
   }

    @Test
    public void levelOrderTraversalTest(){
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        System.out.println("levelOrder Traversal (Iterative):");
        System.out.println(levelOrder(root));
    }
}

