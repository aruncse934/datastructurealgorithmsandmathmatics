package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BitManipulation {

    //Sum of Two Integers
    public int getSum(int a, int b) {
        while(b!=0){
            int carry = a&b;
            a ^= b;
            b = carry << 1;
        }
        return a;
    }
    @Test public  void getSumTest(){
        int a = 1;
        int b =2;
        System.out.println(getSum(a,b));
    }
    public int hammingWeight(int n) {
        int count = 0;
        while(n != 0){
            count += (n&1);
            n =  n>>>1;
        }
        return count;
    }
    @Test
    public void hammingWeighTest(){
        int n = 00000000000000000000000000001011;
        System.out.println(hammingWeight(n));
    }
  
    public int[] countBits(int n) {
        int[] ans = new int[n+1];
        for(int i = 1; i<=n; i++){
            ans[i]  = ans[i >> 1] + (i&1);
        }
        return ans;
    }
    @Test
    public void countBitsTest(){
        int n = 2;
        System.out.println(Arrays.toString(countBits(n)));
    }

    public int missingNumber(int[] nums) {
        int res = nums.length;
        for(int i =0;  i< nums.length; i++){
            res ^= i^nums[i];
        }
        return res;
    }
    @Test
    public void missingNumTest(){
        int[] nums= {3,0,1};
        System.out.println(missingNumber(nums));
    }

    public int reverseBits(int n) {
        int rev = 0;
        for(int i = 0; i < 32;i++){
            rev = (rev << 1) | (n&1);
            n >>= 1;
        }
        return rev;
    }
    @Test
    public void reverseBits(){
        int n = 10;
        System.out.println(reverseBits(n));
        
    }

    //Find Two Missing Numbers
    static List<Integer> find_missing_numbers(List<Integer> arr) {
        int N = arr.size() + 2;
        long sum = ((long) N * (N + 1)) / 2;
        for (int i : arr) {
            sum -= i;
        }
        long average = sum / 2;
        long sum_average = (average * (average + 1)) / 2;
        long x = 0;
        for (int i : arr) {
            if (i <= average) {
                x += i;
            }
        }
        int num1 = (int) (sum_average - x);
        int num2 = (int) (sum - num1);
        List<Integer> result = new ArrayList<>();
        result.add(num1);
        result.add(num2);
        return result;
    }

    @Test
    public void find_missing_numbersBits(){
        List<Integer> arr = Arrays.asList(6, 1, 3, 2);
        System.out.println(find_missing_numbers(arr));

    }


    //Min Number of Powers of 2 to Get an Integer
    //Input: 15, Output: 2 Explanation: 2^4 - 2^0 = 16 - 1 = 15
    public static int minPowersOfTwo(int n) {
        int comple = (1 << Integer.toBinaryString(n).length()) - n;
        int pos = 0, neg = 0;
        while (n != 0) {
            int bit = n % 2;
            n /= 2;
            int compleBit = comple % 2;
            comple /= 2;
            pos += bit;
            neg += compleBit;
            pos = Math.min(pos, 1 + neg);
            neg = Math.min(neg, 1 + pos);
        }
        return pos;
    }

}
