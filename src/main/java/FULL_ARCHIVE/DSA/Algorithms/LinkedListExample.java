package FULL_ARCHIVE.DSA.Algorithms;

import AlgoApproach.AaHackingCodingInterview.LinkedList.ListNode;
import org.junit.jupiter.api.Test;

public class LinkedListExample {

    //𝐒𝐢𝐧𝐠𝐥𝐲 𝐋𝐢𝐧𝐤𝐞𝐝 𝐋𝐢𝐬𝐭 𝐑𝐞𝐯𝐞𝐫𝐬𝐚𝐥
    public ListNode reverseList(ListNode head){
        ListNode prev = null;
        while (head!=null){
            ListNode temp = head.next;
            head.next = prev;
            prev = head;
            head = temp;
        }
        return prev;
    }

    @Test
    public void reverseListTest(){
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        // Reverse the linked list
        ListNode reversedList = reverseList(head);
        // Print the reversed linked list
        while (reversedList != null) {
            System.out.print(reversedList.val + " ");
            reversedList = reversedList.next;
        }
      
    }

    public boolean hasCycle(ListNode head) {
        if(head == null)return false;
        ListNode slow=head;
        ListNode fast=head;
        while(fast != null && fast.next != null ){
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast) return true;
        }
        return false;
    }


    /*
      //Add Two Numbers II
      Input: l1 = [7,2,4,3], l2 = [5,6,4]
      Output: [7,8,0,7]
     */
    public ListNode revList(ListNode head){
        ListNode prev = null;
        while(head != null){
            ListNode temp = head.next;
            head.next = prev;
            prev = head;
            head = temp;
        }
        return prev;
    }
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode r1 = revList(l1);
        ListNode r2 = revList(l2);

        int totalSum = 0, carry = 0;
        ListNode ans = new ListNode();
        while(r1 != null || r2 != null){
            if(r1 != null){
                totalSum += r1.val;
                r1=r1.next;
            }
            if(r2 != null){
                totalSum += r2.val;
                r2 = r2.next;
            }
            ans.val = totalSum % 10;
            carry = totalSum /10;
            ListNode head = new ListNode(carry);
            head.next = ans;
            ans = head;
            totalSum = carry;
        }
        return carry == 0 ? ans.next : ans;
    }

    @Test
    public void addTwoNumberTest(){
        ListNode l1 = new ListNode(7);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(4);
        l1.next.next.next = new ListNode(3);
                                                          //: [7, 2, 4, 3] and [5, 6, 4]
        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);

        // Call the addTwoNumbers method
        ListNode result =addTwoNumbers(l1, l2); //[7,8,0,7]
        // Print the result
        while (result != null) {
            System.out.print(result.val + " ");
            result = result.next;
        }
    }

    public ListNode reverseBetwwen(ListNode head, int left, int right){
        if(head == null) return head;
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy;
        for(int i = 0; i < left-1;i++){
            prev = prev.next;
        }
        ListNode curr = prev.next;
        for(int i = 0; i< right-left; i++){
            ListNode temp = curr.next;
            curr.next = temp.next;
            temp.next = prev.next;
            prev.next = temp;
        }
        return  dummy.next;
    }
    @Test
    public void reverseBetwwenTest(){
         //Input: head = [1,2,3,4,5], left = 2, right = 4
        //Output: [1,4,3,2,5]
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(3);
        l1.next.next.next = new ListNode(4);
        l1.next.next.next.next = new ListNode(5);
        int left = 2, right = 4;

        ListNode reversedList = reverseBetwwen(l1,left,right);
        // Print the reversed linked list
        while (reversedList != null) {
            System.out.print(reversedList.val + " ");
            reversedList = reversedList.next;
        }

        /*public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode ans = new ListNode(0);
        ListNode curr = ans;
        while(l1 != null || l2 != null || carry != 0){
            int x = (l1 != null) ? l1.val : 0;
            int y = (l2 != null) ? l2.val : 0;
            int sum = carry + x + y;
            carry = sum/10;
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            if(l1 != null){
                l1=l1.next;
            }
            if(l2 != null){
                l2 = l2.next;
            }
        }
        return ans.next;
    }*/

    }
}
