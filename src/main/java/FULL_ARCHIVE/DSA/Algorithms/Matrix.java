package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Matrix {

    //four pointer
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> res = new ArrayList<>();
        if(matrix.length == 0) return res;
          int rowBegin = 0, rowEnd = matrix.length-1;
          int colBegin = 0, colEnd = matrix[0].length-1;
         while (rowBegin <= rowEnd && colBegin <= colEnd){
             for(int i = colBegin; i<= colEnd; i++){   // travs right
                 res.add(matrix[rowBegin][i]);
             }
             rowBegin++;
             for(int i = rowBegin; i <= rowEnd;i++){     // travs down
                 res.add(matrix[i][colEnd]);
             }
             colEnd--;

             if(rowBegin <= rowEnd){
                 for(int i =colEnd; i>=colBegin; i-- ){    //travs left
                    res.add(matrix[rowEnd][i]);
                 }
             }
             rowEnd--;
             if(colBegin <= colEnd){
                 for(int i = rowEnd; i >=rowBegin; i--){   //travs up
                     res.add(matrix[i][colBegin]);
                 }
             }
             colBegin++;
         }
         return res;
    }

    @Test
    public void   spiralOrderTest(){
        int[][] matrix =  {{1,2,3},{4,5,6},{7,8,9}};
         List<Integer> res = spiralOrder(matrix);
         assertEquals(Arrays.asList(1,2,3,6,9,8,7,4,5),res);

    }
     //Set Matrix Zeroes
    public void setZeroes(int[][] matrix) {
        int row = matrix.length,col = matrix[0].length;
        boolean flag = false;
        for(int i = 0; i < row; i++){
            if(matrix[i][0] == 0){
                flag = true;
            }
            for(int j = 1; j < col; j++){
                if(matrix[i][j] == 0){
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        for(int i = 1; i < row; i++){
            for(int j = 1; j< col; j++){
                if(matrix[i][0] == 0 || matrix[0][j] == 0){
                    matrix[i][j] = 0;
                }
            }
        }
        if(matrix[0][0] == 0){
            for(int j = 0; j < col; j++){
                matrix[0][j] = 0;
            }
        }
        if(flag){
            for(int i = 0; i < row; i++){
                matrix[i][0] = 0;
            }
        }
    }
   // Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
 //   Output: [[1,0,1],[0,0,0],[1,0,1]]
    @Test
    public void setZerosTest(){
        int[][] matrix ={{1,1,1},{1,0,1},{1,1,1}};
        setZeroes(matrix);
         for (int[] row : matrix) {
            for (int element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }

    }
       //Rotate Image
    public void rotate(int[][] matrix) {
        int s = 0, n = matrix.length-1;
        while(s<n){
            int[] temp = matrix[s];
            matrix[s] = matrix[n];
            matrix[n] = temp;
            s++;
            n--;
        }
        for(int i = 0; i<matrix.length;i++){
            for(int j =i+1; j <matrix[i].length;j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }
  //  Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
 //   Output: [[7,4,1],[8,5,2],[9,6,3]]
    @Test
    public void rotateTest(){
        int[][] matrix ={{1,2,3},{4,5,6},{7,8,9}};
        rotate(matrix);
        for (int[] row : matrix) {
            for (int value : row) {
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }

    //Word Search
    public boolean exist(char[][] board, String word) {
        int m = board.length, n = board[0].length;
        if(m*n < word.length()) return false;
        char[] w = word.toCharArray();
        int[] b = new int[128];
        for(int i = 0; i< m; i++){
            for(int j =0; j<n;j++){
                ++b[board[i][j]];
            }
        }
        for(char ch : w){
            if( --b[ch] < 0) return false;
        }
        if(b[w[0]] > b[w[w.length-1]])
            reverse(w);
        for(int i =0; i < m; i++){
            for(int j = 0; j <n;j++){
                if(w[0] == board[i][j] && found(board,i,j,w, new boolean[m][n],0))
                    return true;
            }
        }
        return false;
    }
    private void reverse(char[] word){
        int n = word.length;
        for(int i = 0; i<n/2;i++){
            char temp = word[i];
            word[i] = word[n-i-1];
            word[n-i-1] = temp;
        }
    }
    private static final int[] dirs = {0,-1,0,1,0};
    private boolean found(char[][] board, int row, int col, char[] word, boolean[][] visited, int index){
        if(index == word.length) return true;
        if(row < 0 || col < 0 || row == board.length || col == board[0].length || board[row][col] != word[index] || visited[row][col]) return false;
        visited[row][col] = true;
        for(int i = 0; i < 4; i++){
            if(found(board, row+dirs[i], col+dirs[i+1],word,visited,index+1)) return true;
        }
        visited[row][col] = false;
        return false;
    }
    /*Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"*/

    @Test
    public void WordSearchTest(){
         char[][] board = {{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','F'}};
         String word = "ABCCED";
         System.out.println(exist(board,word));
    }
}
