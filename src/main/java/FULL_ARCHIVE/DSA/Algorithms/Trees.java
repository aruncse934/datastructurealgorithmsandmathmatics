package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class Trees {

}
class BinaryTrees{
    public int maxDepth(TreeNode root) {
        if(root == null) return 0;
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        return Math.max(left,right) +1;
    }
    @Test
    public void maxDepthTest(){
        
    }
}


class BinaryOrN_aryTrees {

}
 class BinarySearchTree {
      //Closest Binary Search Tree Value II
      // Input: root = [4,2,5,1,3], target = 3.714286, and k = 2    Output: [4,3]
      public List<Integer> closestKValues(TreeNode root, double target, int k) {
         List<Integer> res = new ArrayList<>();
         inOrder(root,target, k, res);
         return res;
      }
     private void inOrder(TreeNode root, double target, int k, List<Integer> res) {
          if(root == null) return;
          inOrder(root.left,target,k,res);
          if(res.size() < k){
              res.add(root.val);
          } else {
              double diff1 = Math.abs(root.val-target);
              double diff2 = Math.abs(res.get(res.size()-1)-target);
              if(diff1<diff2){
                  res.remove(res.size()-1);
                  res.add(root.val);
              }
          }
          inOrder(root.right, target,k,res);
     }
     @Test
     public void  closestKValuesTest(){
         TreeNode root = new TreeNode(4);
         root.left = new TreeNode(2);
         root.right = new TreeNode(5);
         root.left.left = new TreeNode(1);
         root.left.right = new TreeNode(3);
         System.out.println(closestKValues(root, 3.714286, 2));

         TreeNode root1 = new TreeNode(1);
         System.out.println(closestKValues(root,0.000000 , 1));
     }
 }
 class HeapsOrPriorityQueues {
    
 }
