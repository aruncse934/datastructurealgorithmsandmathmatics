package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

public class Recursion {

    //print 1 to n using java  without foreach loop
    public static void print(int n) {
        if(n !=0) {
            print(n-1);
            System.out.println(n);
        }
    }
    @org.junit.jupiter.api.Test
    public void printTest() {
        print(100);
    }

    // factorial
   public static int fact(int n) {
        if (n <= 1)
            return 1;
        else
            return n*fact(n-1);
    }
    @org.junit.jupiter.api.Test
    public void factTest() {
        System.out.println(  fact(5));
    }

    // Function for fibonacci
    /*  public int fib(int n) {
        if(n <= 1)
        return n;
       return fib(n-1)+fib(n-2); */
    public static int fib(int n) {
        if (n == 0)
            return 0;

        // Stop condition
        if (n == 1 || n == 2)
            return 1;
        else
            return (fib(n - 1) + fib(n - 2));
    }

    @Test
    public void fibTest(){
        System.out.println(fib(10));
    }
    //1. Direct Recursion: These can be further categorized into four types:
    //Tail Recursion:
    // Recursion function
    public static void fun(int n){
        if (n > 0) {
            fun(n - 1);
            System.out.print(n + " ");
        }
    }
    @Test
    public void funTest(){
        fun(10);
    }
}
