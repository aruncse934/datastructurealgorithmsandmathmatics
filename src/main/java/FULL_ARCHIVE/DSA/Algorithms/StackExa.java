package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.Stack;

public class StackExa {

    private int[] stack;
    private int  top;
    private int capacity;

    public StackExa(int[] stack, int top, int size) {
        this.stack = stack;
        this.top = top;
        this.capacity = size;
    }
     public void push(int[] stack,int x, int n ){
         if(top == n-1)
             System.out.println("Stack is full.Overflow condition!");
         else
             top = top+1;
             stack[top] = x;
     }

     public void pop(int[] stack, int n){
          if(isEmpty())
              System.out.println("Stack is empty. Underflow condition!");
          else
              top = top-1;
     }

     public int topElement(){

        return stack[top];
     }
     public boolean isEmpty(){
       if(top == -1) return true;
        return false;
     }

     public int size(){
        return top+1;
     }



   /* public static void main(String[] args) {
        Stack stack = new Stack();
         stack.push(10);
         stack.push(20);
         stack.push(50);
         stack.push(70);

     //   System.out.println("Current size of stack is",+stack.size());
         stack.push(90);
         stack.push(40);
         stack.push(75);


         

    }*/

    public static boolean isValid(String input) {
        Stack<Character> stack = new Stack<>();
        for(char c : input.toCharArray()){
            if(c == '(')stack.push(')');
            else if(c=='{') stack.push('}');
            else if(c=='[') stack.push(']');
            else if(Character.isDigit(c) ||c == '+' || c == '-' || c == '*' || c == '/' || c == '%'){
            }
            else{ return false;}
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        String s = "((1+2)*3*)";
        System.out.println(isValid(s));
    }

    public boolean isValidExpression(String input) {
        Stack<Character> stack = new Stack<>();
        for(char c : input.toCharArray()){
            if(c == '('|| c=='{'|| c == '[')stack.push(c);
            else if(c==')' && !stack.isEmpty() && stack.peek() == '(') stack.pop();
            else if(c=='}' && !stack.isEmpty() && stack.peek() == '{') stack.pop();
            else if(c==']' && !stack.isEmpty() && stack.peek() == '[') stack.pop();
            else if(Character.isDigit(c) ||c == '+' || c == '-' || c == '*' || c == '/' || c == '%'){
            }
            else{ return false;}
        }
        return stack.isEmpty();
    }
    
    @Test
    public void isValidExpressionTest(){
        String s = "((2 + 3) * [5 - 1])";
        System.out.println(isValidExpression(s));   //"{(1+2)*3}+4"
    }
    

}
