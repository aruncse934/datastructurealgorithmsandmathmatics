package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ArraysExa {
    // 1D array
    public static int printArray(int[] n){
         for (int i = 0; i < n.length-1; i++){
             System.out.println(n[i]);
         }
         return 0;
    }
    @Test
    public void printArrayTest(){
        int[] n  = {4, 12, 7, 15, 9};
        System.out.println(printArray(n));
    }

    //2D array
    public int print2D_Array(int[][] arr){
        for(int i = 0; i < arr.length;i++){
            for(int j = 0; j <arr[0].length; j++){
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }
        return 0;
    }
    @Test
    public void print2D_ArrayTest(){
        int[][] arr = {{5, 12, 17, 9, 3}, {13, 4, 8, 14, 1}, {9, 6, 3, 7, 21}};
        System.out.println(print2D_Array(arr));
    }

    public int[] twoSum(int[] nums, int target){
        int i = 0, j = 1, x = 1;
        while(true){
            if(nums[i] + nums[j] == target){
                break;
            } else if(j == nums.length -1){
                x++;
                i = 0;
                j = x;
            } else {
                i++;
                j++;
            }
        }
        return  new int[]{i,j};
    }
    @Test
    public void twoSumTest(){
        int[] nums = {2,7,11,15};
        int target = 9;
        System.out.println(Arrays.toString(twoSum(nums, target)));
    }

    public int maxProfit(int[] prices) {
        int buy = Integer.MAX_VALUE;
        int sell = 0;
        for(int i : prices){
            buy = Math.min(buy,i);
            sell = Math.max(sell, i -buy);
        }
        return sell;
    }

    @Test
    public void maxProfitTest(){
       int[] prices = {7,1,5,3,6,4};
        System.out.println(maxProfit(prices));
    }
    //Best Time to Buy and Sell Stock II
    public int maxProfits(int[] prices) {
        int maxProfit = 0;
        for(int i = 1;i<prices.length; i++){
            if(prices[i-1] < prices[i]) {
                maxProfit += prices[i] - prices[i-1];
            }
        }
        return maxProfit;
    }
    @Test
    public void maxProfitsTest(){
        int[] prices = {7,1,5,3,6,4};
        System.out.println(maxProfits(prices));
    }

    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int n : nums){
            if(!set.add(n)) return true;
        }
        return false;
    }
    @Test
    public void containsDuplicateTest(){
      int[] nums = {1,2,3,1};
        System.out.println(containsDuplicate(nums));
    }

    public int[] productExceptSelf(int[] nums){
        int[] res = new int[nums.length];
        int prefix = 1;
        for(int i =0 ; i < nums.length; i++){
            res[i] = prefix;
            prefix *= nums[i];
        }
        int  postfix = 1;
        for(int i = nums.length - 1; i >= 0; i--){
            res[i] *= postfix;
            postfix *= nums[i];
        }
        return res;
    }
    @Test
    public void productExceptSelfTest(){
        int[] nums = {1,2,3,4};
        System.out.println(Arrays.toString(productExceptSelf(nums)));
    }

    /*public int maxSubArray(int[] a) {

        int maxSum = Integer.MIN_VALUE;
        int curSum = 0;
        int n = a.length;
        for(int i=0; i<n ; i++){
            curSum += a[i];


            maxSum = (curSum>maxSum) ? curSum: maxSum;

            if(curSum <0) { curSum = 0; }


        }
        return maxSum;

    }*/
    public int maxSubArray(int[] arr){
        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        for(int i : arr){
            sum += i;
            maxSum = Math.max(sum, maxSum);
            if(sum < 0) sum = 0;
        }
        return maxSum;
    }

    @Test
    public void maxSubArray(){
        int[] arr = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(maxSubArray(arr));
                
    }

    public int maxProduct(int[] nums) {
        int maxProduct = Integer.MIN_VALUE;
        int product  = 1;
        for (int j : nums) {
            product *= j;
            maxProduct = Math.max(maxProduct, product);
            if (product == 0) product = 1;
        }
        product = 1;
        for(int i = nums.length-1;i>=0;i--) {
            product *= nums[i];
            maxProduct = Math.max(maxProduct,product);
            if (product == 0) product =1;
        }
        return maxProduct;
    }
    @Test
    public void maxProduct(){
        int[] nums = {2,3,-2,4};
        System.out.println(maxProduct(nums));
    }

    public int findMin(int[] nums) {
        int min = Integer.MAX_VALUE;
        for(int i : nums){
            if(min > i)
                min = i;
        }
        return min;
    }
    @Test
    public void findMinTest(){
        int[] nums = {3,4,5,1,2};
        System.out.println(findMin(nums));
    }

    public int search(int[] nums, int target) {
        int l = 0,r = nums.length-1;
        while(l <= r){
            int mid = (l+r)/2;
            if(nums[mid] == target) return mid;
            if(nums[l] <= nums[mid]){
                if(nums[l] <= target && nums[mid] > target) {
                    r = mid-1;
                }
                else{
                    l = mid+1;
                }
            }else{
                if(nums[r] >= target && nums[mid] < target) {
                    l = mid+1;
                }
                else{
                    r = mid-1;
                }
            }
        }
        return -1;
    }
    @Test
    public void SearchTest(){
        int[] nums = {4,5,6,7,0,1,2};
        int target = 0;
        System.out.println(search(nums,target));
    }

    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        Set<List<Integer>> result = new HashSet<>();
        for (int i = 0; i < nums.length - 2; i++) {
            for (int j = i + 1; j < nums.length - 1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        List<Integer> triplet = new ArrayList<>(Arrays.asList(nums[i], nums[j], nums[k]));
                        result.add(triplet);
                    }
                }
            }
        }
        return new ArrayList<>(result);
    }

    public List<List<Integer>> threeSumOptimal(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum == 0) {
                    result.add(Arrays.asList(nums[i], nums[left], nums[right]));
                    left++;
                    right--;
                    while (left < right && nums[left] == nums[left - 1]) {
                        left++;
                    }
                    while (left < right && nums[right] == nums[right + 1]) {
                        right--;
                    }
                } else if (sum < 0) {
                    left++;
                } else {
                    right--;
                }
            }
        }
        return result;
    }
    @Test
    public void ThreesumTest(){
        int[] nums = {-1,0,1,2,-1,-4};
        System.out.println(threeSumOptimal(nums));
    }

    public int maxArea(int[] height) {
        int i = 0, j = height.length-1, ans =0;
        while(i<j){
            int area = (j-i)*Math.min(height[i], height[j]);
            if(area> ans) ans = area;
            if(height[i] > height[j])
                j--;
            else
                i++;
        }
        return ans;
    }
    @Test
    public void maxAreaTest(){
        int[] height = {1,8,6,2,5,4,8,3,7};
        System.out.println(maxArea(height));
    }
      //Merge Sorted Array
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m-1, j = n-1, k = m+n-1;
        while(j >= 0){
            if(i>= 0 && nums1[i] > nums2[j])
                nums1[k--] = nums1[i--];
            else
                nums1[k--] = nums2[j--];
        }
    }
    public void merge1(int[] nums1, int m, int[] nums2, int n) {
        int index = 0;
        for (int i = m; i < n + m; i++) {
            nums1[i] = nums2[index++];
        }
        Arrays.sort(nums1);
    }
    @Test
    public void mergeTest(){
        int[] nums1 = {1,2,3,0,0,0};
        int m = 3;
        int[] nums2 = {2,5,6};
        int n =3;
        merge(nums1,m,nums2,n);
        for (int num : nums1) {
            System.out.print(num + " ");
        }
    }

    //Java Program to Reverse an Array Without Using Another Array
    public static void reverseArray(int[] arr){
        int i =0, j = arr.length-1;
        while (i<j){
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }
    @Test
    public void reverseArrayTest(){
        int[] arr = {1,2,3,4,5};
        reverseArray(arr);
        for(int i : arr) System.out.print(i+" ");
    }
    //Java Program to Find Duplicate Elements in an Array
    public static  void findDuplicateEle(int[] arr){
        Set<Integer> set = new HashSet<>();
        for(int i : arr){
            if(!set.add(i)){
                System.out.println(i+" ");
            }
        }
    }
    @Test
    public void findDuplicateTest(){
        int[] arr = {1, 3, 5, 2, 4, 5, 3};
        findDuplicateEle(arr);
    }

    //Java Program to Find Largest Number in an Array
    public int findLargestNumber(int[] arr){
        int max = arr[0];
        for(int i : arr){
            if(i > max)
                max = i;
        }
        return max;
    }
    @Test
    public void findLargestNumberTest(){
        int[] arr = {25, 47, 33, 56, 19, 40};
        System.out.println(findLargestNumber(arr));
    }

    public int subarraySum(int[] nums, int k) {
        int count = 0, sum = 0;
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0,1);
        for(int i =0; i < nums.length;i++){
            sum += nums[i];
            if(map.containsKey(sum-k))
                count += map.get(sum-k);
            map.put(sum,map.getOrDefault(sum,0)+1);
        }
        return count;
    }
    @Test
    public void subarraySumTest(){
        int[] arr = {5, 1, 2, -3, 7, -4};
        int k = 0;
        System.out.println(subarraySum(arr,k));
    }
    
    public static List<Integer> sum_zero(List<Integer> arr) {
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int  sum = 0;
        for (int i = 0; i < arr.size(); i++) {
            if(arr.get(i) == 0){
                return new ArrayList<Integer>(Arrays.asList(i, i));
            }
            sum += arr.get(i);
            if (map.containsKey(sum))
                return new ArrayList<Integer>(Arrays.asList(map.get(sum) + 1, i));
             else
                map.put(sum, i);
        }
        return new ArrayList<Integer>(Arrays.asList(-1));
    }

    @Test
    public void sum_zeroTest(){
       List<Integer> arr = Arrays.asList(5, 1, 2, -3, 7, -4);
        List<Integer> arr1 = Arrays.asList(1, 2, 3, 5, -9);
        
        System.out.println(sum_zero(arr));
        System.out.println(sum_zero(arr1));
    }
   
     public int[] maxSlidingWindow(int[] nums, int k){
         int n = nums.length;
         int[] arr = new int[100010];
         int start = 0, end = 0;
         int[] res = new int[n-k+1];
         for(int i = 0; i < n; i++){
             while (start < end && arr[end-1] < nums[i]){
                 end--;
             }
             arr[end++] = nums[i];
             if(i < k -1) continue;
             res[i-(k-1)] = arr[start];
             if(nums[i-(k-1)] == arr[start]) start++;
         }
         return res;
     }

    @Test
    public void maxSlidingWindowTest(){
        int[] arr = {1,3,-1,-3,5,3,6,7};
        int k =3;
        System.out.println(Arrays.toString(maxSlidingWindow(arr, k)));
    }
    //274. H-Index
    public int hIndex(int[] citations) {
        int n = citations.length;
        int[] buckets = new int[n+1];
        for(int c : citations){
            buckets[(c >= n) ? n : c]++;
        }
        int count = 0;
        for(int i =n; i >= 0; i--){
            count += buckets[i];
            if(count >= i) return i;
        }
        return 0;
    }
    @Test
    public void hIndexTest(){
        int[] arr = {3,0,6,1,5};
        System.out.println(hIndex(arr));
    }

    //134. Gas Station
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int total_gas = 0, total_cost = 0, curr_gas= 0, start = 0;
        for(int i = 0; i < gas.length; i++){
            total_gas += gas[i];
            total_cost += cost[i];
            curr_gas += gas[i] - cost[i];
            if(curr_gas < 0){
                start = i+1;
                curr_gas = 0;
            }
        }
        return (total_gas < total_cost) ? -1 : start;
    }
    //[1,2,3,4,5], cost = [3,4,5,1,2]
    @Test
    public void canCompleteCircuitTest(){
        int[] gas = {1,2,3,4,5};
        int[] cost = {3,4,5,1,2};
        System.out.println(canCompleteCircuit(gas,cost));
        int[] gas1 = {2,3,4};
        int[] cost1 = {3,4,3};
        System.out.println(canCompleteCircuit(gas1,cost1));
    }

    //Minimum Swaps 2 hackerrank
    static int minimumSwaps(int[] arr) {
        int count = 0;
        for( int i = 0; i < arr.length;i++){
            while(arr[i] < i+1){
                int temp = arr[i];
                arr[i] = arr[temp-1];
                arr[temp-1] = temp;
                count++;
            }
        }
        return count;
    }
    @Test
    public void minimumSwapsTest(){
        int[] arr = {4, 3, 1, 2};
        System.out.println(minimumSwaps(arr));

    }

    public static int numIdenticalPairs(int[] nums) {
       /* Map<Integer, Integer> map = new HashMap<>();
        int count =0;
        for(int num : nums) {
            if (map.containsKey(num)) {
                count += map.get(num);
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        return count;*/
        return (int) Arrays.stream(nums).boxed().collect(
                Collectors.groupingBy(Function.identity(),
                        Collectors.counting()))
                .values().stream().mapToLong(x -> x).map(x -> x * (x - 1) / 2).sum();

    }

    @Test
    public void numIdenticalPairsTest() {
        int[] arr = {1,2,3,1,1,3};
        System.out.println(numIdenticalPairs(arr));
    }

    public int[] smallerNumbersThanCurrent(int[] nums) {
        int[] res = new int[nums.length];
        int[] copy =  nums.clone();  //Arrays.copyOf(nums,nums.length);
        Arrays.sort(copy);
        Map<Integer,Integer> map = new HashMap<>();
       /* for(int num=0; num<copy.length; num++){
            if(!map.containsKey(copy[num]) ){
                map.put(copy[num],num);
            }
        }*/
        //or
        for (int i = 0; i < nums.length; i++) {
            map.putIfAbsent(copy[i], i);
        }

        for(int i =0; i<nums.length;i++){
            res[i] = map.get(nums[i]);
        }
        return res;
    }

    @Test
    public void smallerNumbersThanCurrentTest() {
        int[] arr = {8,1,2,2,3};     //4,0,1,1,3]
        System.out.println(Arrays.toString(smallerNumbersThanCurrent(arr)));
    }


    //2824. Count Pairs Whose Sum is Less than Target
    public int countPairs(List<Integer> nums, int target) {
        int count =0;
        Integer[] arr = nums.toArray(new Integer[nums.size()]);
        for(int i = 0; i<nums.size()-1;i++){
            for(int j = i+1;j < nums.size(); j++){
                if(nums.get(i)+nums.get(j) < target){
                    count++;
                }
            }
        }
        return count;
    }

    /*Input: nums = [-1,1,2,3,1], target = 2
Output: 3
*/

    //Leaders in an array
    public static List<Integer> leadersArray(int[] arr,int n){
       List<Integer> list = new ArrayList<>();
       int rightSide = arr[n-1];
       list.add(rightSide);
       for(int i = n-2; i >= 0; i--){
           if(arr[i] >= rightSide){
               rightSide = arr[i];
               list.add(rightSide);
           }
       }
     //  Collections.reverse(list);
       List<Integer> res = new ArrayList<>();
       for(int i = list.size()-1;i>=0;i--){
           res.add(list.get(i));
       }

       return res;
    }
    @Test
    public void leadersTest() {
        int[] arr = {16, 17, 4, 3, 5, 2};
        int n = 6;
        System.out.println(leadersArray(arr, n));
    }

}


