package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Sorting {
    
    // 𝐈𝐧𝐬𝐞𝐫𝐭𝐢𝐨𝐧 𝐒𝐨𝐫𝐭 Iterative technique tc o(n^2)
    public int[] insertionSort(int[] arr){
        for (int i = 0; i < arr.length;i++){
            int key = arr[i];
            int j = i-1;
            while (j >= 0 && arr[j] > key){
                  arr[j+1] = arr[j];
                  j--;
            }
            arr[j+1] = key;
        }
        return arr;
    }
    
    @Test public void insertionSortTest(){
        int[] arr = {6, 9, 2, 10, 8, 6, 4, 1, 5, 9, 4, 6, 9, 6, 1};
        System.out.println(Arrays.toString(insertionSort(arr)));

    }

    // 𝐒𝐞𝐥𝐞𝐜𝐭𝐢𝐨𝐧 𝐒𝐨𝐫𝐭
    public int[] selectionSort(int[] arr){
        for(int i = 0; i < arr.length-1;i++){
            int minIndex = i;
           for(int j = i+1; j < arr.length; j++){
               if(arr[j] < arr[minIndex]){
                   minIndex = j;
               }
           }
           if(minIndex != i){
               int temp = arr[i];
               arr[i] = arr[minIndex];
               arr[minIndex] = temp;
           }
        }
        return arr;
    }
    @Test public void selectionSortTest(){
        int[] arr = {6, 9, 2, 10, 8, 6, 4, 1, 5, 9, 4, 6, 9, 6, 1};
        System.out.println(Arrays.toString(selectionSort(arr)));
    }
    // 𝐂𝐨𝐮𝐧𝐭𝐢𝐧𝐠 𝐒𝐨𝐫𝐭

    public void countingSort(int[] arr){
        int max = arr[0];
        int min = arr[0];
        for(int i = 1; i < arr.length;i++){
             if(arr[i] > max){
                 max = arr[i];
             }
             if(arr[i] < min){
                 min = arr[i];
             }
        }
        int[] count  = new int[max - min + 1];
        for(int i = 0; i < arr.length; i++){
            count[arr[i] - min]++;
        }
        for(int i = 1; i < count.length; i++){
          count[i] += count[i-1];
        }
        int[] sorted = new int[arr.length];
        for(int i = arr.length-1; i >= 0;i--){
           sorted[count[arr[i] - min] -1] = arr[i];
           count[arr[i] - min]--;
        }
        System.arraycopy(sorted,0,arr,0,arr.length);


    }

  @Test public void countingSort(){
      int[] arr = {6, 9, 2, 10, 8, 6, 4, 1, 5, 9, 4, 6, 9, 6, 1};
      System.out.println(Arrays.toString(arr));
  }
    // 𝐇𝐞𝐚𝐩 𝐒𝐨𝐫𝐭
    // 𝐌𝐞𝐫𝐠𝐞 𝐒𝐨𝐫𝐭
    // 𝐐𝐮𝐢𝐜𝐤 𝐒𝐨𝐫𝐭
    // 𝐓𝐨𝐩𝐨𝐥𝐨𝐠𝐢𝐜𝐚𝐥 𝐒𝐨𝐫𝐭

    //Sort Colors(Dutch national flag Algo) Input: nums = [2,0,2,1,1,0]  Output: [0,0,1,1,2,2]
    public int[] sortColors(int[] nums){
        for(int i = 0 ; i < nums.length;i++){
            for(int j = 0; j < nums.length; j++) {
                if (nums[i] < nums[j])  {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        return nums;
    }
    // one pass in place solution
    public int[] sortColor(int[] arr){
        int n0 = -1, n1 = -1, n2 = -1;
        for(int i = 0; i< arr.length; i++){
            if(arr[i] == 0){
              arr[++n2] =2; arr[++n1] = 1; arr[++n0] = 0;
            } else if(arr[i] == 1){
                arr[++n2] = 2; arr[++n1] = 1;
            } else if(arr[i] == 2){
                arr[++n2] = 2;
            }
        }
        return arr;
    }

    @Test
    public void sortColorsTest(){
        int[] nums ={2,0,2,1,1,0};
        System.out.println(Arrays.toString(sortColor(nums)));
    }


    //Merge Intervals Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
    //Output: [[1,6],[8,10],[15,18]]
    //Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].
    //Sorting
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals,(a,b)-> Integer.compare(a[0],b[0]));
        LinkedList<int[]> merged = new LinkedList<>();
        for(int[] interval : intervals){
            if(merged.isEmpty() || merged.getLast()[1] < interval[0])
                merged.add(interval);
            else
                merged.getLast()[1] = Math.max(merged.getLast()[1],interval[1]);
        }
        return merged.toArray(new int [merged.size()][]);
    }
    @Test
    public void mergeTest(){
      int[][] intervals = {{1,3},{2,6},{8,10},{15,18}};
        System.out.println(Arrays.deepToString(merge(intervals)));
    }


    public static List<Integer> countingSort(List<Integer> arr) {
        int[] couArr = new int[100];

        for(Integer num : arr){
            couArr[num]++;
        }

        List<Integer> sortedList = new ArrayList<>();
        for(Integer i : couArr){
            sortedList.add(i);
        }
        return sortedList;
    }
}



class MergeSort{

}

class QuickSort{
    
}

class BubbleSort{

}


