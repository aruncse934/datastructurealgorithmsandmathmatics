package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.*;

public class StringExample {

    //Convert a number to string, without using inbuilt functions
    public static String numberToString(int num) {
        if (num == 0) {
            return "0";
        }
     /*   // Determine the sign of the number (positive or negative)
        boolean isNegative = num < 0;
        if (isNegative) {
            num = -num; // Make num positive for the conversion
        }
*/
        StringBuilder result = new StringBuilder();

        while (num > 0) {
            int digit = num % 10; // Extract the last digit
            char digitChar = (char) ('0' + digit); // Convert digit to character
            result.insert(0, digitChar); // Prepend the digit to the result
            num /= 10; // Remove the last digit
        }

       /* if (isNegative) {
            result.insert(0, '-'); // Add the negative sign if necessary
        }*/
        return result.toString();
    }

    //
    public static String sumTwoNumber(String num1, String num2){  //addTwoNumbers
        //  return String.valueOf(Integer.parseInt(num1)+Integer.parseInt(num2));
       // return String.valueOf(new BigInteger(num1).add(new BigInteger(num2)));
            int i = num1.length() - 1, j = num2.length() - 1, carry = 0;
            StringBuilder sb = new StringBuilder();

            while(i >= 0 || j >= 0 || carry != 0) {
                int sum = carry;
                sum += (i >= 0) ? num1.charAt(i--) - '0' : 0;// if(i >= 0) sum += num1.charAt(i--) - '0';
                sum += (j >= 0) ? num2.charAt(j--) - '0' : 0; //if(j >= 0) sum += num2.charAt(j--) - '0';
                carry = sum/ 10;
                sb.append(sum % 10);
            }
            return sb.reverse().toString();
        }
    

    @Test
    public void NumberTest(){
        int num = 123;
        System.out.println(numberToString(num));
        System.out.println(sumTwoNumber("6913259244","71103343"));
    }

    /*Input: s = "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".*/
    public int countPalindromicSubstrings(String s) {
        int count  = 0;
        for(int i = 0; i < s.length(); i++){
            count += countPalindrome(s,i,i);
            count += countPalindrome(s,i,i+1);

        }
        return count;
    }
    private int countPalindrome(String s, int i, int j){
        int count =0;
        while( i>= 0 && j < s.length() && s.charAt(i--) == s.charAt(j++)){
            count++;
        }
        return count;
    }

    @Test
    public void countPalindromicSubstringsTest(){
        System.out.println(countPalindromicSubstrings("abc"));
    }
    public String longestPalindrome(String s) {
        StringBuilder sb = new StringBuilder("#");
        for(char c : s.toCharArray()){
            sb.append(c).append("#");
        }
        int n = sb.length();
        int[] palindromR = new int[n];
        int cen = 0, rad = 0;
        for(int i = 0; i < n; i++){
            int mirror = 2*cen-i;
            if(i<rad){
                palindromR[i] = Math.min(rad -i, palindromR[mirror]);
            }
            while(i+1+palindromR[i] < n && i-1-palindromR[i] >= 0 && sb.charAt(i+1+palindromR[i]) == sb.charAt(i-1- palindromR[i])){
                palindromR[i]++;
            }
            if(i+palindromR[i] > rad){
                cen = i;
                rad = i+palindromR[i];
            }
        }
        int maxLen = 0,cenInd = 0;
        for(int i = 0; i < n; i++){
            if(palindromR[i] > maxLen){
                maxLen = palindromR[i];
                cenInd = i;
            }
        }
        int startIndex = (cenInd - maxLen) / 2;
        String longestPalindrome = s.substring(startIndex, startIndex + maxLen);
        return longestPalindrome;
    }
    @Test
    public void Teat(){
        System.out.println(longestPalindrome("babad"));
    }

    public boolean isPalindrome(String s) {
        char[] ch =s.toCharArray();
        int i =0;
        int j = ch.length-1;
        while (i<j){
            if(!Character.isLetterOrDigit(ch[i]))
                i++;
            else if(!Character.isLetterOrDigit(ch[j]))
                j--;
            else if(Character.toLowerCase(ch[i++]) != Character.toLowerCase(ch[j--]))
                return false;
        }
        return true;
    }
    @Test
    public void isPalindromeTest(){
        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
    }

    public static List<List<String>> groupAnagrams(String[] strs){
        Map<String, List<String>> map = new HashMap<>();
        for(String s : strs){
            char[] c = s.toCharArray();
            Arrays.sort(c);
            String strKey = new String(c);

            if(!map.containsKey(strKey)) {
                map.put(strKey,new ArrayList<>());
            }
            map.get(strKey).add(s);
        }
        return new ArrayList<>(map.values());
    }
    @Test
    public void groupAnagramsTest(){
        String[] strings = {"eat", "tea", "tan", "ate", "nat", "bat"};
        System.out.println(groupAnagrams(strings));
    }
    public String encode(List<String> strs) {
        StringBuilder encodedString = new StringBuilder();
        for (String str : strs) {
            encodedString.append(str.length()).append("#").append(str);
        }
        return encodedString.toString();
    }

    public List<String> decode(String str) {
        List<String> list = new ArrayList<>();
        int i = 0;
        while (i < str.length()) {
            int j = i;
            while (str.charAt(j) != '#') j++;

            int length = Integer.valueOf(str.substring(i, j));
            i = j + 1 + length;
            list.add(str.substring(j + 1, i));
        }
        return list;
    }
    @Test
    public void encodeest(){

       // System.out.println(encode();
    }
    @Test
    public void DecodeTest(){

        System.out.println(decode("arun"));
    }

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for(char c : s.toCharArray()){
            if(c=='(') stack.push(')');
            else if(c=='{') stack.push('}');
            else if(c=='[') stack.push(']');
            else if(stack.isEmpty() || stack.pop()!=c)
                return false;
        }
        return stack.isEmpty();
    }
    @Test
    public void isValidTest(){

        System.out.println(isValid("()[]{}"));
    }

    public boolean isAnagram(String s, String t) {
        if(s.length() != t.length()) return false;
        int[] ar = new int[26];
        for(int i = 0; i<s.length(); i++){
            ar[s.charAt(i) - 'a']++;
            ar[t.charAt(i) - 'a']--;
        }
        for(int j : ar)
            if(j != 0)
                return false;
        return true;
    }

    @Test
    public void isAnagramTest(){

        System.out.println(isAnagram("bat","cab"));
    }


    //  Input: a = "11", b = "1"
    //Output: "100"
    public String addBinary(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        int i = a.length()-1;
        int j = b.length()-1;
        while(i >= 0 || j >= 0 || carry == 1){
            if(i >= 0) carry += a.charAt(i--) -'0';
            if( j>= 0) carry += b.charAt(j--) -'0';
            sb.append(carry%2);
            carry /= 2;
        }
        return sb.reverse().toString();
    }
    @Test
    public void addBinaryTest(){
        System.out.println(addBinary("11","1"));
    }
    //
    public int lengthOfLongestSubString(String s){
        int res = 0;
        int left = 0;
        int[] arr = new int[128];
        for(int i =1; i<= s.length(); i++){
            char temp = s.charAt(i-1);
            if(arr[temp] != 0) {
                left = Math.max(left,arr[temp]);
            }
            res = Math.max(res,i-left);
            arr[temp] = i;
        }
        return res;
    }
    @Test
    public void lengthOfLongestSubStringTest(){
        System.out.println(lengthOfLongestSubString("abcabcbb")); //3
        System.out.println(lengthOfLongestSubString("bbbbb"));
        System.out.println(lengthOfLongestSubString("pwwkew"));
    }

    
    /*Input: s = "ABAB", k = 2
Output: 4*/
    // Longest Repeating Character Replacement
    public int characterReplacement(String s, int k){
        int[] count  = new int[26];
        int j = 0, maxCount = 0, maxLen = 0;
        char[] ch = s.toCharArray();
        for(int i = 0; i<s.length();i++){
            maxCount = Math.max(maxCount,++count[ch[i]-'A']);
            while(i - j +1-maxCount > k){
                count[ch[j] - 'A']--;
                j++;
            }
            maxLen = Math.max(maxLen,i-j +1);
        }
        return maxLen;
    }
    @Test
    public void characterReplacementTest(){
        System.out.println(characterReplacement("ABAB",2));
        System.out.println(characterReplacement("AABABBA",1));
    }

    public String minWindow(String s, String t) {
        int n1 = s.length();
        char[] ch = s.toCharArray();
        int[] freq = new int[128];
        for(char c : t.toCharArray())
            freq[c]++;
            int left = 0, start =0, minLen = Integer.MAX_VALUE, n2 = t.length();
            char cha,rem;
            for(int right = 0; right < n1;right++) {
                cha = ch[right];
                freq[cha]--;
                if (freq[cha] >= 0) n2--;
                while (n2 == 0) {
                    if (minLen > right - left + 1) {
                        minLen = right - left + 1;
                        start = left;
                    }
                    rem = ch[left++];
                    freq[rem]++;
                    if (freq[rem] > 0) n2++;
                }
            }
        return minLen == Integer.MAX_VALUE ? "" : s.substring(start,start+ minLen);

    }
    @Test
    public void minWindowTest(){
        System.out.println(minWindow("ADOBECODEBANC","ABC"));
        System.out.println(minWindow("a","a")); //a
        System.out.println(minWindow("a","aa")); //""
    }

    public String longestCommonPrefix(String[] strs) {
        String s = strs[0];
        for(String str : strs){
            while(!str.startsWith(s)){
                s = s.substring(0,s.length()-1);
            }
            if(s.length()==0) return "";
        }
        return s;
    }
    @Test
    public void longestCommanPrefixTest(){
        String[] s= {"flower","flow","flight"};
        System.out.println(longestCommonPrefix(s));
    }

    //Java Program to Find the First Non-repeated Character in a String

    public static char findFirstNonRepeatedChar(String s){
        Map<Character,Integer> map = new HashMap<>();
        for(char c : s.toCharArray()){
            map.put(c,map.getOrDefault(c,0)+1);
        }

        for (char i :s.toCharArray() ){
            if(map.get(i) == 1) return i;
        }
        return '0';
    }

    @Test
    public void findFirstNonRepeatedChar(){
        String s = "Java Program";
        System.out.println(findFirstNonRepeatedChar(s));
    }
    public void countlargestOccurChar(String s){
        Map<Character, Integer> map = new HashMap<>();
        for(char c : s.toCharArray()){
            map.put(c,map.getOrDefault(c,0) + 1);
        }
        int count = 0;
        char c = ' ';
        for (Map.Entry<Character, Integer> entry : map.entrySet()){
            if(entry.getValue() > count){
                count = entry.getValue();
                c = entry.getKey();
                
            }
        }
        System.out.println(c+"="+count);
    }
    @Test
    public void countlargestOccurCharTest(){
        String s = "Hello World";
        countlargestOccurChar(s);
    }
    public void DuplicateCharacteCharacte(String s){
        Set<Character> set = new HashSet<>();
        for (char c : s.toCharArray()){
            if(!set.add(c)){
                System.out.println(c);
            }
        }
    }
    @Test
    public void DuplicateCharacteTest(){
        String s = "Hello Java";
        DuplicateCharacteCharacte(s);
    }

    public int lengthOfLastWord(String s) {
        int count = 0;
        for(int i = s.length() -1; i >= 0; i--){
            if(s.charAt(i) != ' ') count++;
            else if(count > 0) return count;
        }
        return count;
    }

    @Test
    public void lengthOfLastWordTest(){
        String s = "Hello Java";
        System.out.println(lengthOfLastWord(s));
    }
    //Find the Index of the First Occurrence in a String
    public int strStr(String haystack, String needle) {
        return haystack.indexOf(needle);
    }
    @Test
    public void strStrTest(){
        String haystack = "sadbutsad"; String needle = "sad";
        System.out.println(strStr(haystack,needle));
    }
    //6. Zigzag Conversion
    public String convert(String s, int numRows) {
        if(numRows == 1) return s;
        int n = s.length(), step = numRows*2 - 2;
        int subStep = step, index = 0, row = 0;
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            res[i] = s.charAt(index);
            if(subStep > 0 && subStep < step && subStep + index < n){
                res[++i] =  s.charAt(index + subStep);
            }
            index += step;
            if(index >= n){
                index = ++row;
                subStep -= 2;
            }

        }
        return new String(res);
    }
    /*Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"*/

    @Test
    public void convertTest(){
        String s = "PAYPALISHIRING";
        int numRows = 3;
        System.out.println(convert(s,numRows));
    }
    //Java Program to Check Palindrome String
    

    // Substring of String
    //Number of substrings of one string present in other

    //Find the count of palindromic sub-string of a string in its sorted form
    //
    //Check if a string contains a palindromic sub-string of even length
    //
    //Check if the given string is K-periodic
    //
    //Longest sub string of 0's in a binary string which is repeated K times
    //
    //Find the longest substring with k unique characters in a given string
    //
    //Shortest Palindromic Substring
    //
    //Count of K-size substrings having palindromic permutations
    //
    //Maximize partitions such that no two substrings have any common character
    //
    //Count of substrings having all distinct characters
    //
    //Smallest String consisting of a String S exactly K times as a Substring
    //
    //Check if a String contains Anagrams of length K which does not contain the character X
    //
    //Check if a Palindromic String can be formed by concatenating Substrings of two given Strings
    //
    //Minimum size substring to be removed to make a given string palindromic
    //
    //Count ways to split a Binary String into three substrings having equal count of zeros
    //
    //Count of substrings of a string containing another given string as a substring
    //
    //Longest substring where all the characters appear at least K times | Set 3


    
    //Subsequence of String
    //Print all subsequences of a string
    //
    //Check if two same sub-sequences exist in a string or not
    //
    //Minimum number of palindromic subsequences to be removed to empty a binary string
    //
    //Find largest word in dictionary by deleting some characters of given string
    //
    //Longest Common Anagram Subsequence
    //
    //Find the lexicographically largest palindromic Subsequence of a String
    //
    //Maximum number of removals of given subsequence from a string
    //
    //Longest subsequence with at least one character appearing in every string
    //
    //Distinct strings such that they contains given strings as sub-sequences
    //
    //Smallest Palindromic Subsequence of Even Length in Range [L, R]
    //
    //Minimum number of subsequences required to convert one string to another
    //
    //Longest Palindromic Subsequence of two distinct characters
    //
    //Longest subsequence with different adjacent characters
    //



    //String Searching
    //Check if a string is substring of another
    //
    //Print all occurrences of a string as a substring in another string



    //Pattern Searching
    //Naive algorithm for Pattern Searching
    //
    //Rabin-Karp Algorithm for Pattern Searching
    //
    //KMP Algorithm for Pattern Searching
    //
    //Z algorithm (Linear time pattern searching Algorithm)
    //
    //Suffix Array | Set 1 (Introduction)


    //Problems on Pattern Searching
    //Find all the patterns of "1(0+)1" in a given string (General Approach)
    //
    //Minimum number of times A has to be repeated such that B is a substring of it
    //
    //Anagram Substring Search (Or Search for all permutations)
    //
    //Longest prefix which is also suffix


    //Easy Problems on String
    //Reverse a String
    //
    //Reverse words in a given string
    //
    //Program to count occurrence of a given character in a string
    //
    //Convert characters of a string to opposite case
    //
    //Program to count vowels in a string (Iterative and Recursive)
    //
    //Check if a number is Palindrome
    //
    //Remove duplicates from a given string
    //
    //Count words in a given string
    //
    //Check if a string is substring of another
    //
    //Check whether two Strings are anagram of each other
    //
    //Remove minimum number of characters so that two strings become anagram
    //
    //Minimum Number of Manipulations required to make two Strings Anagram Without Deletion of Character
    //
    //Capitalize the first and last character of each word in a string
    //
    //Check if given strings are rotations of each other or not



    //Medium Problems on String
    //Print all palindrome permutations of a string
    //
    //Sum of two large numbers
    //
    //Multiply Large Numbers represented as Strings
    //
    //Length of the longest valid substring
    //
    //Length of the longest substring without repeating characters
    //
    //Find the longest substring with k unique characters in a given string
    //
    //Smallest window that contains all characters of string itself
    //
    //Minimum rotations required to get the same string

    
    //Hard Problems on String
    //Palindrome Substring Queries
    //
    //Minimum number of bracket reversals needed to make an expression balanced
    //
    //Number of distinct words of size N with at most K contiguous vowels
    //
    //Longest substring whose characters can be rearranged to form a Palindrome
    //
    //Find all distinct palindromic sub-strings of a given string

}



   




