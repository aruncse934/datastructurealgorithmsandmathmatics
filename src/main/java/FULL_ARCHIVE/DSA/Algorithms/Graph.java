package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.*;

public class Graph {

    //1791. Find Center of Star Graph Input: edges = [[1,2],[2,3],[4,2]]
    //Output: 2
    //Explanation: As shown in the figure above, node 2 is connected to every other node, so 2 is the center.
      public int findCenter(int[][] edges){
          int a=edges[0][0], b = edges[0][1], c = edges[1][0], d = edges[1][1];
          if(a==c || a ==d)
              return a;
          return b;
      }
      @Test
    public void findCenterTest(){
          int[][] edges = {{1,2},{2,3},{4,2}};
          System.out.println(findCenter(edges));
      }
      
      //1971. Find if Path Exists in Graph    Input: n = 3, edges = [[0,1],[1,2],[2,0]], source = 0, destination = 2
      //Output: true
      public boolean validPath(int n, int[][] edges, int source, int destination){
          //DFS
          boolean[] visited = new boolean[n];
          visited[source] = true;
          boolean proceed = true;
          while (!visited[destination] && proceed){
              proceed = false;
              for(int[] edge : edges){
                  if(visited[edge[0]] ^ visited[edge[1]]){
                      visited[edge[0]] = visited[edge[1]] = true ;
                      proceed = true;
                      if(visited[destination]) return true;
                  }
              }
          }
          return visited[destination];
      }
    public boolean validPaths( int[][] edges, int source, int destination) {
        Set<Integer> visited = new HashSet<>();
        visited.add(source);
        boolean proceed = true;
        while (!visited.contains(destination) && proceed) {
            proceed = false;
            for (int[] edge : edges) {
                if (visited.contains(edge[0]) ^ visited.contains(edge[1])) {
                    visited.add(edge[0]);
                    visited.add(edge[1]);
                    proceed = true;
                    if (visited.contains(destination)) {
                        return true;
                    }
                }
            }
        }

        return visited.contains(destination);
    }
     @Test
    public void validPathTest(){ 
          int n =3;
          int[][] edges = {{0,1},{1,2},{2,0}};
          int source = 0;
          int destination = 2;

         System.out.println(validPath(n,edges,source,destination));
     }


     //997. Find the Town Judge   Input: n = 2, trust = [[1,2]]
     //Output: 2
    public int findJudge(int n, int[][] trust){
          int[] degree = new int[n+1];
          for(int[] trustPair : trust){
              degree[trustPair[1]]++;
          }
          int judge = -1;
          for(int i= 1; i<= n; i++){
              if(degree[i] == n-1){
                  judge =i;
                  break;
              }
          }
        for(int[] trustPair : trust){
            if(trustPair[0] == judge) return -1;
        }

          return judge;
    }
    @Test
    public void findJudgeTest(){
          int n = 2;
          int[][] trust = {{1,2}};
        System.out.println(findJudge(n, trust));
    }

}


class BreadthFirstSearchs {
    
}

class DepthFirstSearchs {

}

class MinimumSpanningTree {

}

class ShortestPathAlgorithms{

}

class FloodFillAlgorithm{

}

class ArticulationPointsAndBridges{
    
}

class BiconnectedComponents{
    
}

class StronglyConnectedComponents{
    
}

class TopologicalSort{
    
}

class HamiltonianPath{
    
}

class Maximumflow{
    
}

class MinimumCostMaximumFlow{
    
}

class MinCut{

}