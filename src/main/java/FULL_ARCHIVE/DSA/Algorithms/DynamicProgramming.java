package FULL_ARCHIVE.DSA.Algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DynamicProgramming {

    //Climbing Stairs dp   Tabulation
    public int climbStairs(int n) {
             if(n ==1)return 1;
             int[] dp =new int[n+1];
             dp[1] = 1;
             dp[2] = 2;
             for(int i = 3; i <= n; i++){
                 dp[i] = dp[i-1]+dp[i-2];
             }
        return  dp[n];
    }

    //Approach 2: Space Optimization
    public int climbStairs1(int n) {
        if(n == 1) return 1;
        int pre =1, curr =1;
        for(int i = 2; i <= n; i++){
            int temp = curr;
            curr = pre + curr;
            pre = temp;
        }
        return curr;
    }


    @Test
    public void climbStairsTest(){
        int n = 3;
        int res = climbStairs(n);
        assertEquals(3, res);
    }

    //  Kadane's Algorithm 
    public int maxSubArray1(int[] nums){
        int maxx = Integer.MIN_VALUE, sum = 0;
        for(int i =0; i< nums.length; i++){
            sum += nums[i];
            maxx = Math.max(sum, maxx);
            if(sum < 0) sum = 0;
        }
        return maxx;
    }
    //DP
    public int maxSubArray(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int maxm = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i] + dp[i - 1], nums[i]);// dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
            maxm = Math.max(maxm, dp[i]);
        }
        return maxm;
    }

    @Test
    public void maxSubArrayTest(){
        int[] nums =  {-2,1,-3,4,-1,2,1,-5,4};
        int res = maxSubArray1(nums);
        assertEquals(6,res);
    }


    public int coinChange(int[] coins, int amount){
        int max = amount+1;
        int[] dp = new int[amount+1];
        Arrays.fill(dp,max);
        dp[0] = 0;
        for(int i=1;i<= amount; i++){
            for(int j=0; j < coins.length; j++){
                if(coins[j] <= i ){
                    dp[i] = Math.min(dp[i], dp[i-coins[j]]+1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }

    public int coinChange1(int[] coins, int amount){
        int[] dp = new int[amount+1];
        Arrays.fill(dp,amount+1);
        dp[0] = 0;
        for(int coin : coins){
            for(int i = coin; i <= amount; i++){
                dp[i] = Math.min(dp[i], dp[i-coin]+1);
            }
        }
        return dp[amount] <= amount ? dp[amount] : -1;
    }
    
    @Test
    public void coinChangeTest(){
        int[] coins = {1,2,5};
        int amount = 11;
        int res = coinChange1(coins,amount);
        assertEquals(3,res);
    }


    //  Partition Equal Subset Sum dp
    public boolean canPartition(int[] nums){
        int sum = 0;
        for(int num : nums){
            sum += num;
        }
        if((sum & 1) == 1) {
            return false;
        }
        sum /= 2;
        boolean[] dp = new boolean[sum+1];
        Arrays.fill(dp, false);
        dp[0] = true;

        for(int num : nums){
            for(int i = sum;i>0; i--){
                if(i >= num){
                    dp[i] = dp[i] || dp[i-num];
                }
            }
        }
        return dp[sum];
    }

    @Test
    public void  canPartitionTest(){
        int[] nums = {1,5,11,5};
        boolean res = canPartition(nums);
        assertEquals(true,res);
    }

    //Unique Paths
     public int uniquePaths(int m, int n){
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        for(int i = 1; i< m; i++){
            for(int j = 1; j < n; j++) {
                dp[j] += dp[j-1];
            }
        }
        return dp[n-1];
     }
     @Test
    public void uniquePathsTest(){
        int m =3, n = 7;
        int res = uniquePaths(m,n);
        assertEquals(28, res);
     }

     //House Robber
      public int rob(int[] nums)  {
        if(nums.length == 0) return 0;
        int[] dp = new int[nums.length+1];
        dp[0]=0;
        dp[1] = nums[0];
        for(int i = 1; i < nums.length; i++){
            dp[i+1] = Math.max(dp[i], dp[i-1] + nums[i]);
        }
        return dp[nums.length];
      }
      //100%
      public int rob1(int[] nums) {
        if(nums.length == 0) return 0;
        int prev1 = 0;
        int prev2 = 0;
        for(int num : nums){
            int temp = prev1;
            prev1 = Math.max(prev1, prev2+num);
            prev2 = temp;
        }
        return prev1;
      }

    // DP with O(1) space
    public int robDP2(int[] nums) {
        if (nums == null || nums.length == 0) return 0;

        int dp0 = 0, dp1 = 0, curr;

        for (int i = 0; i < nums.length; i++) {
            curr = Math.max(dp0 + nums[i], dp1);
            dp0 = dp1;
            dp1 = curr;
        }
        return Math.max(dp0, dp1);
    }

      @Test
    public void robTest(){
        int[] nums = {1,1};
        int res = rob(nums);
        assertEquals(1,res);
      }

    // House Robber II
    
       //   Maximum Product Subarray
      public int maxProduct(int[] arr){
        int n = arr.length, res = arr[0], l = 0, r = 0;
        for(int i = 0; i < n; i++){
            l = (l == 0 ? 1 : l)*arr[i];
            r = (r == 0 ? 1 : r) * arr[n-1-i];
            res = Math.max(res, Math.max(l,r));
        }
        return res;
      }

      public int maxProduct1(int[] arr){
        int maxProduct = Integer.MIN_VALUE;
        int product  = 1;
          for (int j : arr) {
              product *= j;
              maxProduct = Math.max(maxProduct, product);
              if (product == 0) product = 1;
          }
            product = 1;
            for(int i = arr.length-1;i>=0;i--) {
            product *= arr[i];
            maxProduct = Math.max(maxProduct,product);
            if (product == 0) product =1;
        }
        return maxProduct;
      }

      @Test
    public void maxProductTest(){
          int[] arr ={2,3,-2,4};
          int res = maxProduct1(arr);
          assertEquals(6,res);
      }

     //  Longest Increasing Subsequence dp
      public int lenOfLIS(int[] nums){
        int[] dp = new int[nums.length];
        Arrays.fill(dp,1);
        for(int i =0; i <nums.length;i++){
            for(int j = 0; j < i; ++j){
                if(nums[i] > nums[j] && dp[i] < dp[j] +1)
                    dp[i] = dp[j]+1;
            }
        }
        int maxLen = 0;
        for(int len : dp){
            maxLen =Math.max(maxLen,len);
        }
        return maxLen;
      }

      public int lenOfLIS1(int[] nums){
        List<Integer> list = new ArrayList<>();
        for(int n : nums){
            if(list.isEmpty() || list.get(list.size()-1) < n) {
                list.add(n);
            }
            else {
                int i = 0;
                int j = list.size() - 1;
                while (i < j){
                    int m = i+(j-i)/2;
                    if(list.get(m) < n)
                        i = m+1;
                    else
                        j = m;
                }
                list.set(j,n);
            }
        }
        return list.size();
      }

    public int lenOfLIS2(int[] nums) {
        int[] dp = new int[nums.length];
        int size =0;
        for(int n : nums){
            int i = 0, j = size;
            while (i < j){
                int m = i+(j-i)/2;
                if(dp[m] < n)
                    i = m+1;
                else
                    j = m;
            }
            dp[i] = n;
            if(i==size) size++;
        }
        return size;
    }
      @Test
    public void lenOfLISTest(){
        int[] nums = {10,9,2,5,3,7,101,18};
        int res= lenOfLIS2(nums);
        assertEquals(4,res);
      }

      //Minimum number of increment-other operations to make all array elements equal.
    public int miniOp(int[] arr){
        int count = 0;
        int x = arr[0];
        for (int j : arr) {
            if (j < x) x = j;
            count += j;
        }
        return count - arr.length * x;
    }
    @Test
    public void miniOpTest(){
        int[] arr ={1,2,3};
        int res = miniOp(arr);
        assertEquals(3,res);
    }

    //Longest Common Subsequence
    public int longestCommonSubsequence(String text1, String text2) {
        int m = text1.length();
        int n = text2.length();
        char[] ch1 = text1.toCharArray();
        char[] ch2 = text2.toCharArray();
        int[][] dp = new int[m+1][n+1];
        for(int i = 1; i <= m; i++){
            for(int j = 1; j<= n;j++){
                if(ch1[i-1] == ch2[j-1]){
                    dp[i][j] = dp[i-1][j-1]+1;
                }
                else{
                    dp[i][j] = Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }
        return dp[m][n];
    }

    @Test
    public void  longestCommonSubsequenceTest(){
        String text1 ="abcde";
        String text2 ="ace";
        System.out.println(longestCommonSubsequence(text1,text2));
    }

    public boolean wordBreak(String s, List<String> wordDict){
        int n = s.length();
        boolean[] b = new boolean[n+1];
        b[0] = true;
        for(int i = 0; i < n; i++){
            if(b[i] == true){
                for(String word : wordDict){
                    if(s.substring(i).indexOf(word) == 0) b[i+word.length()] = true;
                }
            }
        }
        return b[n];
    }

    @Test
    public void wordBreakTest(){
        String s = "leetcode";
        List<String> wordDict = Arrays.asList("leet","code");
        System.out.println(wordBreak(s,wordDict));
    }
   
    public int combinationSum4(int[] nums, int target){
        int[] dp = new int[target+1];
        dp[0] = 1;
        for(int i = 1; i <= target; i++){
            for(int num : nums){
                if( i >= num) dp[i] += dp[i-num];
            }
        }
        return dp[target];
    }
    @Test
    public void combinationSum4Test(){
        int[] nums = {1,2,3};
        int target = 4;
        System.out.println(combinationSum4(nums,target));
    }

    //64. Minimum Path Sum
    public int minimumTotal(List<List<Integer>> triangle){
        int[] dp = new int[triangle.size()+1];
        for(int i = triangle.size()-1; i >= 0; i--){
            List<Integer> arr = triangle.get(i);
            for(int j = 0; j < arr.size(); j++){
                dp[j] = Math.min(dp[j], dp[j+1]) + arr.get(j);
            }
        }
        return  dp[0];
    }
    @Test
    public void minimumTotalTest(){
        /* Input: grid = [[1,3,1],[1,5,1],[4,2,1]] Output: 7 */
        List<List<Integer>> triangle= Arrays.asList(Arrays.asList(1,3,1),Arrays.asList(1,5,1),Arrays.asList(4,2,1));

        System.out.println(minimumTotal(triangle));
    }
   //72. Edit Distance
    public int minDistance(String world1, String world2){
        char[] c1 = world1.toCharArray();
        char[] c2 = world2.toCharArray();
        int[][] dp = new int[c1.length+1][c2.length+1];
        for(int i = 0; i <= c1.length; i++){
            dp[i][0] = i;
        }
        for(int j = 0; j<=c2.length;j++){
            dp[0][j] = j;
        }
        for(int i = 0; i <c1.length;i++){
            for(int j = 0; j<c2.length; j++){
                if(c1[i] == c2[j]){
                    dp[i+1][j+1] = dp[i][j];
                } else {
                    dp[i+1][j+1] = Math.min(Math.min(dp[i][j+1],dp[i+1][j]),dp[i][j])+1;
                }
            }
        }
        return dp[c1.length][c2.length];
    }
    @Test
    public void minDistanceTest(){
        String world1 = "horse";
        String world2 = "ros";
        System.out.println(minDistance(world1,world2));
    }
    //97. Interleaving String
    public boolean isInterleave(String s1, String s2, String s3){
        int m = s1.length();
        int n = s2.length();
        int l = s3.length();
        if(m+n != l) return false;
        boolean[] dp = new boolean[n+1];
        dp[0] = true;
        for(int j = 1; j <= n; j++){
            dp[j] = dp[j-1] && s2.charAt(j-1) == s3.charAt(j-1);
        }
        for(int i = 1; i <= m; i++){
            dp[0] = dp[0] && s1.charAt(i-1) == s3.charAt(i-1);
            for(int j = 1; j<= n; j++){
                dp[j] = (dp[j] && s1.charAt(i-1) == s3.charAt(i+j-1) || (dp[j-1] && s2.charAt(j-1) == s3.charAt(i+j-1)));
            }
        }
        return dp[n];
    }
    @Test
    public void isInterleaveTest(){
        String s1 = "aabcc";
        String s2 = "dbbca";
        String s3 = "aadbbcbcac";
        System.out.println(isInterleave(s1,s2,s3));
    }
}



//Word Break Problem
//Combination Sum
//House Robber II
//Decode Ways
//Unique Paths


