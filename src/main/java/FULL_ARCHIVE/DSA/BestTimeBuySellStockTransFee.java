package FULL_ARCHIVE.DSA;

public class BestTimeBuySellStockTransFee {

    public int maxProfit(int[] prices, int fee){
        int profit = 0, hold = -prices[0];
        for(int i = 1; i < prices.length; i++){
            profit = Math.max(profit, hold + prices[i] - fee);
            hold =  Math.max(hold, profit - prices[i]);
        }
        return profit;
    }

    public static void main(String[] args) {
        int[] prices = {1,3,7,5,10,3};
        int fee = 3;

        System.out.println(new BestTimeBuySellStockTransFee().maxProfit(prices,fee));
    }
}
