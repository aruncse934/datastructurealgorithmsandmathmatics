package FULL_ARCHIVE.DSA;

import java.util.List;

public class Load {

    public static void main(String[] args) {
        int n = 6;
        List<Integer> burstTime = List.of(14, 3, 2, 2, 2, 6);

        long minMaxLoadTime = getMinMaxLoadTime(n, burstTime);
        System.out.println("Minimum max load time: " + minMaxLoadTime);
    }

    static long getMinMaxLoadTime(int n, List<Integer> burstTime) {
        long left = 0;
        long right = 0;

        // Calculate the total burst time
        for (int i = 0; i < n; i++) {
            right += burstTime.get(i);
        }

        // Perform binary search to find the minimum max load
        while (left < right) {
            long mid = (left + right) / 2;

            // Check if the current mid value is feasible
            if (isFeasible(n, burstTime, mid)) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        return left;
    }

    // Function to check if a given max load is feasible
    static boolean isFeasible(int n, List<Integer> burstTime, long maxLoad) {
        long currentLoad = 0;
        int machines = 1;

        for (int i = 0; i < n; i++) {
            // If the current burst time exceeds the max load, the allocation is not feasible
            if (burstTime.get(i) > maxLoad) {
                return false;
            }

            // Check if we can add the current burst time to the current machine
            if (currentLoad + burstTime.get(i) <= maxLoad) {
                currentLoad += burstTime.get(i);
            } else {
                machines++;
                currentLoad = burstTime.get(i);
            }
        }

        // Check if the number of machines required is less than or equal to the total number of resources
        return machines <= n;
    }
}
