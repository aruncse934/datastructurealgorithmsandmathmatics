package FULL_ARCHIVE.DSA.StringWithHashingAndDP;

public class LongestIdealSubsequence {   //Longest Special Subsequences
   /* public int longestIdealStrings(String s, int k) {    // error
        int len = 1;
        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i < s.length(); i++){
            char ch = s.charAt(i);
            int res = 1;
            char cfrm = (char) Math.max('a', ch - k);
            char ctox = (char) Math.max('z', ch + k);
           for(char c = cfrm; c <= ctox; c++){
               res = Math.max(res, 1+map.getOrDefault(c, 0));
          }
           map.put(ch,res);
           len = Math.max(len, res);
        }
        return len;
    }
*/
    public static void main(String[] args) {
         String s  = "pvjcci";
         int k = 4;
        System.out.println(new LongestIdealSubsequence().longestIdealString(s,k));
    }

    public int longestIdealString(String s, int k) {
        int DP[] = new int[26], ans = 1;
        for (int ch = 0, n = s.length(); ch < n; ch++) {
            int i = s.charAt(ch) - 'a';
            DP[i] = DP[i] + 1;

            for (int j = Math.max(0, i - k); j <= Math.min(25, i + k); j++)
                if (j != i)
                    DP[i] = Math.max(DP[i], DP[j] + 1);

            ans = Math.max(ans, DP[i]);
        }

        return ans;
    }
    
}
/*

    public int longestIdealString(String s, int k) {
        int res = 0, n = s.length(), dp[] = new int[150];
        for (int ci = 0; ci < n; ++ci) {
            int i = s.charAt(ci);
            for (int j = i - k; j <= i + k; ++j)
                dp[i] = Math.max(dp[i], dp[j]);
            res = Math.max(res, ++dp[i]);
        }
        return res;
    }
    }*/

