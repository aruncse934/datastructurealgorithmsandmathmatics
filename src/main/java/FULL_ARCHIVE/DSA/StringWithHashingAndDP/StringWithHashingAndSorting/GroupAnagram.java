package FULL_ARCHIVE.DSA.StringWithHashingAndDP.StringWithHashingAndSorting;

import java.util.*;

public class GroupAnagram {
    public static void main(String[] args) {
      String[] strs = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};
        System.out.println(new GroupAnagram().groupAnagrams(strs));
    }
    public List<List<String>> groupAnagrams(String[] s){
        List<List<String>> res =new ArrayList<>();
        if(s.length == 0) return res;
        Map<String,List<String>> map = new HashMap<>();
        for(String str : s){
            int[] hash = new int[26];
            for(char c : str.toCharArray()){
                hash[c - 'a']++;
            }
            String key = Arrays.toString(hash);
            map.computeIfAbsent(key, k -> new ArrayList<>());
            map.get(key).add(str);

        }
        res.addAll(map.values());
        return res;
    }
}
