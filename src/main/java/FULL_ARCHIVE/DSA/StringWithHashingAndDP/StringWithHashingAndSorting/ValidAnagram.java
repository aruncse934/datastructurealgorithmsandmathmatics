package FULL_ARCHIVE.DSA.StringWithHashingAndDP.StringWithHashingAndSorting;

public class ValidAnagram {

    public boolean isAnagram(String s, String t) {
       if(s.length() != t.length()) return false;
       int[] ar = new int[26];
       for(int i =0; i < s.length(); i++){
            ar[s.charAt(i) - 'a']++;
            ar[t.charAt(i) - 'a']--;
        }
       for(int n : ar)
           if(n !=0) return false;
       return true;
    }

    public static void main(String[] args) {
        String s = "anagram";
        String t = "nagaram";
        System.out.println(new ValidAnagram().isAnagram(s, t));
        
    }
}
