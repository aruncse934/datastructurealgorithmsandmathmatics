package FULL_ARCHIVE.DSA.StringWithHashingAndDP;

import java.util.stream.Collectors;

public class MinimumNumOperation {


    /*public static int calMiniOperation(String s){
        int operation = 0, count = 0;
        char prev = s.charAt(0);
          for(int i =0; i < s.length(); i++){
              char  curr = s.charAt(i);
              if(curr == prev){
                  count++;
              }  else {
                  operation++;
                  prev = curr;
                  count =1;
              }
          }
        operation++;
          return operation;
    }*/
    public static int calMinOperations( String s) {
        return (int) s.chars().distinct().count();
    }
    public static int calMinOperations1(int n, String s) {
        return  s.chars()
                .mapToObj(ch -> (char) ch)
                .collect(Collectors.toSet())
                .size();
    }




    public static void main(String[] args) {
        String s = "abaca";
        int n = 5;
     //   System.out.println("x"+calMiniOperation(s));
        System.out.println("x"+calMinOperations(s));
        System.out.println("y"+calMinOperations1(n,s));

    }
}
