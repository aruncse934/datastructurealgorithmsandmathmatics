package FULL_ARCHIVE.DSA.StringWithHashingAndDP;

import java.util.Stack;

public class RemoveAllAdjacentDuplicatesStringII {

    //stack 
    public static String removeDuplicates(String s, int k) {
        Stack<Character> stack = new Stack<>();
        int  i =0;
        while (i < s.length()){
            char ch = s.charAt(i++);
            stack.push(ch);
            int count = 0;
            while (stack.size() > 0 && (stack.peek() == ch)) {
                count++;
                stack.pop();
            }
            if(count == k)
                continue;
            else {
                while (count > 0){
                    stack.push(ch);
                    count--;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char ch : stack)
            sb = sb.append(ch);
        return sb.toString();
    }
    //Two pointer
    public static String removeDuplicates1(String s, int k) {
    int i = 0, n = s.length(), count[] = new int[n];
    char[] ch = s.toCharArray();
    for(int j = 0; j < n; ++j, ++i) {
        ch[i] = ch[j];
        count[i] = i > 0 && ch[i -1] == ch [j] ? count[i-1] + 1 : 1;
        if(count[i] == k) i -= k;
    }
    return new String(ch, 0, i);
    }

    //String Builder

    public static String removeDuplicates2(String s, int k) {
    int[] count = new int[s.length()];
    StringBuilder sb = new StringBuilder();
    for(char c : s.toCharArray()){
        sb.append(c);
        int last  = sb.length() -1;
        count[last] = 1 + (last > 0 && sb.charAt(last) == sb.charAt(last -1) ? count[last - 1] : 0);
        if(count[last] >= k)
            sb.delete(sb.length()-k,sb.length());
    }
    return sb.toString();
    }

    
    public static void main(String[] args) {
      String s = "pbbcggttciiippooaais";
      int k = 2;

        System.out.println(removeDuplicates(s,k));
        System.out.println(removeDuplicates1(s,k));
        System.out.println(removeDuplicates2(s,k));
    }
}
