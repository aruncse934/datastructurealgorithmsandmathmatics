package FULL_ARCHIVE.DSA.DP_Pattern;


import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

class Node
{
    // (x, y) represents coordinates of a cell in the matrix
    int x, y;

    // `level` stores the distance of a current node from the source node
    // (i.e., BFS level)
    int level;

    Node(int x, int y, int level)
    {
        this.x = x;
        this.y = y;
        this.level = level;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ')';
    }
}

public class FindAllPath
{
    // Below arrays detail all four possible movements from a cell
    private static int[] row = { -1, 0, 0, 1 };
    private static int[] col = { 0, -1, 1, 0 };

    // The function returns false if (x, y) is not a valid position
    private static boolean isValid(int x, int y, int N) {
        return (x >= 0 && x < N) && (y >= 0 && y < N);
    }

    // Find the shortest route in a matrix from source cell (x, y) to
    // destination cell (N-1, N-1)
    public static int findPath(int matrix[][], int x, int y)
    {
        // base case
        if (matrix == null || matrix.length == 0) {
            return -1;
        }

        // `N × N` matrix
        int N = matrix.length;

        // create a queue and enqueue the first node
        Queue<Node> q = new ArrayDeque<>();
        Node src = new Node(x, y, 0);
        q.add(src);

        // set to check if the matrix cell is visited before or not
        Set<String> visited = new HashSet<>();

        String key = src.x + "," + src.y;
        visited.add(key);

        // loop till queue is empty
        while (!q.isEmpty())
        {
            // dequeue front node and process it
            Node curr = q.poll();
            int i = curr.x;
            int j = curr.y;
            int level = curr.level;

            // return if the destination is found
            if (i == N - 1 && j == N - 1) {
                return level;
            }

            // value of the current cell
            int n = matrix[i][j];

            // check all four possible movements from the current cell
            // and recur for each valid movement
            for (int k = 0; k < row.length; k++)
            {
                // get next position coordinates using the value of the current cell
                x = i + row[k] * n;
                y = j + col[k] * n;

                // check if it is possible to go to the next position
                // from the current position
                if (isValid(x, y, N))
                {
                    // construct the next cell node
                    Node next = new Node(x, y, level + 1);

                    key = next.x + "," + next.y;

                    // if it isn't visited yet
                    if (!visited.contains(key))
                    {
                        // enqueue it and mark it as visited
                        q.add(next);
                        visited.add(key);
                    }
                }
            }
        }

        // return a negative number if the path is not possible
        return -1;
    }

    public static void main(String[] args)
    {
       /* int[][] matrix =
                {
                        { 4, 4, 6, 5, 5, 1, 1, 1, 7, 4 },
                        { 3, 6, 2, 4, 6, 5, 7, 2, 6, 6 },
                        { 1, 3, 6, 1, 1, 1, 7, 1, 4, 5 },
                        { 7, 5, 6, 3, 1, 3, 3, 1, 1, 7 },
                        { 3, 4, 6, 4, 7, 2, 6, 5, 4, 4 },
                        { 3, 2, 5, 1, 2, 5, 1, 2, 3, 4 },
                        { 4, 2, 2, 2, 5, 2, 3, 7, 7, 3 },
                        { 7, 2, 4, 3, 5, 2, 2, 3, 6, 3 },
                        { 5, 1, 4, 2, 6, 4, 6, 7, 3, 7 },
                        { 1, 4, 1, 7, 5, 3, 6, 5, 3, 4 }
                };
*/
        int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        // Find a route in the matrix from source cell (0, 0) to
        // destination cell (N-1, N-1)
        int dist = findPath(matrix, 0, 0);

        if (dist != -1) {
            System.out.println("The shortest path length is " + dist);
        }
        else {
            System.out.println("Destination is not found");
        }
    }
}



       /* public static void main(String[] args) {
            int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
            int k = 3;

            List<List<Integer>> paths = findAllPaths(matrix, k);

            for (List<Integer> path : paths) {
                for (int element : path) {
                    System.out.print(element + " ");
                }
                System.out.println();
            }
        }

        private static List<List<Integer>> findAllPaths(int[][] matrix, int k) {
            List<List<Integer>> paths = new ArrayList<>();

            int n = matrix.length;
            int m = matrix[0].length;

            // Initialize the current path
            List<Integer> currentPath = new ArrayList<>();

            // Recursively find all possible paths from the top left corner to the bottom right corner
            findPaths(matrix, 0, 0, k, currentPath, paths);

            return paths;
        }

        private static void findPaths(int[][] matrix, int i, int j, int k, List<Integer> currentPath, List<List<Integer>> paths) {
            // If we have reached the bottom right corner, we have found a valid path
            if (i == matrix.length - 1 && j == matrix[0].length - 1) {
                paths.add(new ArrayList<>(currentPath));
                return;
            }

            // Try moving right
            if (j + 1 < matrix[0].length && matrix[i][j + 1] % k == 0) {
                currentPath.add(matrix[i][j + 1]);
                findPaths(matrix, i, j + 1, k, currentPath, paths);
                currentPath.remove(currentPath.size() - 1);
            }

            // Try moving down
            if (i + 1 < matrix.length && matrix[i + 1][j] % k == 0) {
                currentPath.add(matrix[i + 1][j]);
                findPaths(matrix, i + 1, j, k, currentPath, paths);
                currentPath.remove(currentPath.size() - 1);
            }
        }
}
*/