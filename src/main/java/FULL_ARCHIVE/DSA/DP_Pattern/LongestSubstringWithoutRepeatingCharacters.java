package FULL_ARCHIVE.DSA.DP_Pattern;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {

    //HashSet
    public int lengthOfLongestSubstring(String s){
        int i = 0, j = 0, max = 0;
        Set<Character> set = new HashSet<>();
        while(j < s.length()){
            if(!set.contains(s.charAt(j))){
                set.add(s.charAt(j++));
                max = Math.max(max,set.size());
            }
            else {
                set.remove(s.charAt(i++));
            }
        }

        return max;
    }
    public static void main(String[] args) {
        String s = "abcabcbb";

        System.out.println(new LongestSubstringWithoutRepeatingCharacters().lenOfL(s));
    }

    //Sliding Window Optimized 2
    public int lenOfLongestSubstring(String s){
        Map<Character,Integer> map = new HashMap<>();
        int start = 0, len = 0;

        for(int i = 0; i<s.length(); i++){
            char c = s.charAt(i);
            if(map.containsKey(c)){
                if(map.get(c) >= start)
                    start = map.get(c) + 1;
            }
            len = Math.max(len, i - start + 1);
            map.put(c, i);
        }
        return len;
    }

    //Sliding Window Optimized  1
    public int lenofLS(String s){
        Map<Character, Integer> map = new HashMap<>();
        int ans = 0;
        for (int j = 0, i =0; j < s.length(); j++) {
            char c = s.charAt(j);
            if(map.containsKey(c)){
                i = Math.max(map.get(c), i);
            }
            ans = Math.max(ans,j - i + 1);
            map.put(c, j + 1);
        }
        return ans;
    }

    public int lenOflonSub(String s){
        Map<Character, Integer> map = new HashMap<>();
        int left = 0, right = 0, res = 0;
        while (right < s.length()){
            char c = s.charAt(right);
            map.put(c,map.getOrDefault(c, 0) + 1);
            while(map.get(c) > 1){
                char l = s.charAt(left);
                map.put(l, map.get(l) - 1);
                left++;
            }
            res = Math.max(res, right - left + 1);
            right++;
        }
        return res;
    }

   //AS
    public int lenOfL(String s){
        Integer[] chars = new Integer[128];

        int left = 0, right = 0, res = 0;
        while(right < s.length()){
          char c = s.charAt(right);
          Integer index = chars[c];
          if(index != null && index >= left && index < right){
              left = index + 1;
          }
          res = Math.max(res, right - left + 1);
          chars[c] = right;
          right++;
      }
        return res;
    }

}
