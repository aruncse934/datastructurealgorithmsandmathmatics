package FULL_ARCHIVE.DSA.DP_Pattern;

public class LongestSpecialSubsequence {

    public static void main(String[] args) {
        String s = "afcbedf";
        int k = 2;
        System.out.println(findLongestSpecialSubsequence(s,k));
    }

    public static int findLongestSpecialSubsequence(String s, int k){
        int[] dp = new int[s.length()];
        dp[0] = 0;
        for(int i = 1; i < s.length(); i++){
            dp[i] = 1;
            for(int j =  i-1; j >= 0; j--){
                if(Math.abs(s.charAt(i) - s.charAt(j)) <= k){
                    dp[i] = Math.max(dp[i],dp[j] +1);
                    break;
                }
            }
        }
        int maxLen = 0;
        for(int i = 0; i<s.length(); i++){
             maxLen = Math.max(maxLen, dp[i]);
        }
        return maxLen;
    }
}
