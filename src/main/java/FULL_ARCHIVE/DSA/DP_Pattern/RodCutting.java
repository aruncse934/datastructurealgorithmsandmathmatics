package FULL_ARCHIVE.DSA.DP_Pattern;

public class RodCutting {
    public static int maximumProfit(int[] prices, int n) {
        if (n == 0) {
            return 0;
        }
        int maxProfit = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            maxProfit = Math.max(maxProfit, prices[i - 1] + maximumProfit(prices, n - i));
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int n =4;
        int arr[] = {100,10000, 58, 40};

        System.out.println(maximumProfit(arr, n));
    }
}
