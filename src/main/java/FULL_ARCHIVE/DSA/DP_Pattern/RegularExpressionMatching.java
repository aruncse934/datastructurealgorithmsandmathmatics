package FULL_ARCHIVE.DSA.DP_Pattern;

public class RegularExpressionMatching {

    public static boolean  isMatch(String s, String p){
        return s.matches(p);
    }

    //Dp
    public static boolean isMatch1(String s, String p){
        boolean[][] dp = new boolean[s.length()+ 1][p.length()+1];

        dp[s.length()][p.length()] = true;
        for(int i = s.length(); i >= 0; i--){
            for(int j =p.length() -1; j >= 0; j--){
                boolean fMatch = (i < s.length() && (p.charAt(j) == s.charAt(i) || p.charAt(j) == '.'));
                if( j+ 1 < p.length() && p.charAt(j+1) == '*'){
                    dp[i][j] = dp[i][j+2] || fMatch && dp[i+1][j];
                }else {
                    dp[i][j] = fMatch && dp[i+1][j+1];
                }
            }
        }
        return dp[0][0];
    }
    public static void main(String[] args) {

        System.out.println(isMatch1("aa", "a*"));


    }
}
