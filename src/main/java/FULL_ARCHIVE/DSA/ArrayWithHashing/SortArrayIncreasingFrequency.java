package FULL_ARCHIVE.DSA.ArrayWithHashing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class SortArrayIncreasingFrequency {

    public static int[] sortByFreq(int[] arr){
        Map<Integer, Integer> map = new HashMap<>();
        for(int n : arr){
            map.put(n, map.getOrDefault(n,0)+1);
        }
        PriorityQueue<Integer> pq = new PriorityQueue<>((a,b) -> map.get(a) - map.get(b));
        for(int num : arr){
            pq.offer(num);
        }
        int[] res = new int[arr.length];
        int  i = 0;
        while (!pq.isEmpty()){
            res[i++] = pq.poll();
        }
        return res;
    }

    //java 8
    public static int[] sortfreq(int[] nums){
        Map<Integer, Integer> map = new HashMap<>();
        Arrays.stream(nums).forEach(n->map.put(n,map.getOrDefault(n, 0)+1));
    return Arrays.stream(nums).boxed().sorted((a,b) -> map.get(a) != map.get(b) ? map.get(a) - map.get(b) : b-a).mapToInt(n->n).toArray();

    }

    public static void main(String[] args) {
           int[] arr = {1,1,2,2,2,3};
        System.out.println(Arrays.toString(sortByFreq(arr)));

        System.out.println(Arrays.toString(sortByFreq(arr)));
    }
}
                                                           /*public int[] frequencySort(int[] nums) {
	Map<Integer, Integer> map = new HashMap<>();
	// count frequency of each number
	Arrays.stream(nums).forEach(n -> map.put(n, map.getOrDefault(n, 0) + 1));
	// custom sort
	return Arrays.stream(nums).boxed()
			.sorted((a,b) -> map.get(a) != map.get(b) ? map.get(a) - map.get(b) : b - a)
			.mapToInt(n -> n)
			.toArray();
}*/