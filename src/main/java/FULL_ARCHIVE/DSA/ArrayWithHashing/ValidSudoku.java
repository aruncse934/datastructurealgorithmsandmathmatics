package FULL_ARCHIVE.DSA.ArrayWithHashing;

import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {
    public static void main(String[] args) {

        String[][] board = {
                {"5","3",".",".","7",".",".",".","."},
                {"6",".",".","1","9","5",".",".","."},
                {".","9","8",".",".",".",".","6","."},
                {"8",".",".",".","6",".",".",".","3"},
                {"4",".",".","8",".","3",".",".","1"},
                {"7",".",".",".","2",".",".",".","6"},
                {".","6",".",".",".",".","2","8","."},
                {".",".",".","4","1","9",".",".","5"},
                {".",".",".",".","8",".",".","7","9"}
        };

 //       System.out.println(isValidSudoku(board));

    }

 /*  public static boolean isValidSudoku(String[][] board){
        Set<Character> row = null;
        Set<Character> col = null;
        for(int  i = 0 ; i < board.length; i++){
            row = new HashSet<>();
            for(int j = 0; j < board[0].length;j++){
                if(board[i][j] == null ){    // ','
                    continue;
                }
                if(row.contains(board[i][j])){
                    return false;
                }
                row.add((char) board[i][j]);
            }
        }

        for(int i = 0; i < board[0].length; i++){
            col = new HashSet<>();
            for(int j = 0; j<board.length; j++){
                if(board[j][i] == '.'){
                    continue;
                }
                if(col.contains(board[j][i])){
                    return false;
                }
                col.add((char) board[j][i]);
            }
        }

        for(int i = 0; i < board.length; i=i+3){
            for(int j = 0; j < board[0].length; j = j+3){
                if(!checkBlock(i, j, board)) {
                    return false;
                }
            }

        }

         return true;
    }
*/
    private boolean checkBlock(int idx, int jdx, int[][] board) {
        Set<Character> blockset = new HashSet<>();
        int rows = idx + 3;
        int cols = jdx + 3;

        for(int i = idx; i < rows; i++){
            for(int j = jdx; j < cols; j++){
                if(board[i][j] == '.'){
                    continue;
                }
                if(blockset.contains(board[i][j])){
                    return false;

                }
                blockset.add((char) board[i][j]);
            }
        }

        return true;
    }

}

/*class

    public boolean checkBlock(int idxI, int idxJ, char[][] board) {
        Set<Character> blockSet = new HashSet<>();
        int rows = idxI + 3;
        int cols = idxJ + 3;
        for (int i = idxI; i < rows; i++) {
            for (int j = idxJ; j < cols; j++) {
                if (board[i][j] == '.') {
                    continue;
                }

                if (blockSet.contains(board[i][j])) {
                    return false;
                }

                blockSet.add(board[i][j]);
            }
        }

        return true;
    }
}

*/

/***
 *
 * static char[] solve(int[][] Q, String[] arr){
 *
 *         char[] res = new char[Q.length];
 *
 *         for(int i = 0; i < Q.length; i++){
 *             int l = Q[i][0];
 *             int r = Q[i][1];
 *             int k = Q[i][2];
 *
 *         StringBuilder sb = new StringBuilder();
 *         for(int j = l-1; j < r; j++){
 *             sb.append(arr[j]);
 *         }
 *         char[] c = sb.toString().toCharArray();
 *         Arrays.sort(c);
 *          res[i] = c[k -1];
 *         }
 *         return res;
 *     }
String s ="java";

    Map<Character, Integer> map = new LinkedHashMap();
        for(int i =0; i < s.length(); i++){
        char c = s.charAt(i);
        if(!map.containsKey(c)){
            map.put(c, 1);
        }else{
            map.put(c,map.get(c) + 1);
        }
    }

        for(int i = 0; i < s.length(); i++){
        char c = s.charAt(i);
        if(map.containsKey(c)){
            System.out.println(c+"="+map.get(c));
            map.remove(c);
        }

    }

}

    static int CountDIGIT(String str) {
        int count =0;
        for(int i = 0; i < str.length(); i++){
            if(Character.isDigit(str.charAt(i))){
                count++;
            }
        }
        return count;

    }

    static int CountLETTER(String str) {
        int count =0;
        for(int i = 0; i < str.length(); i++){
            if(Character.isLetter(str.charAt(i))){
                count++;
            }
        }
        return count;
    }


    static char[] solve(int[][] Q, String[] arr){
        int n = arr.length;

        char[] res = new char[Q.length];
        int index = 0;

        for(int i = 0; i < Q.length; i++){
            int l = query[i][0];
            int r = query[i][1];
            int k = query[i][2];

            StringBuilder sb = new StringBuilder();
            for(int j = l-1; j < r; j++){
                sb.append(arr[j]);
            }
            char[] c = sb.toString().toCharArray();
            Arrays.sort(c);
            res[i] = c[k -1];
        }
        return res;
    }*/