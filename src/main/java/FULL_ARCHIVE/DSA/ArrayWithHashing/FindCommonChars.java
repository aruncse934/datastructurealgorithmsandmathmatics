package FULL_ARCHIVE.DSA.ArrayWithHashing;

import java.util.ArrayList;
import java.util.*;
import java.util.stream.IntStream;

public class FindCommonChars {

    public static List<String> commonChars(String[] words) {
        int[] count  = new int[26];
        Arrays.fill(count, Integer.MAX_VALUE);
        for(String s : words){
            int[] cnt = new int[26];
            s.chars().forEach(c-> ++cnt[c-'a']);
            IntStream.range(0,26).forEach(i -> count[i] = Math.min(cnt[i], count[i]));
        }
        List<String> res = new ArrayList<>();
        IntStream.range('a','z' + 1).forEach(c->res.addAll(Collections.nCopies(count[c -'a'], ""+ (char) c)));

        return res;
    }

    public static void main(String[] args) {
        String[] s = new String[]{"bella","label","roller"};
        System.out.println(commonChars(s));

    }
}
