package FULL_ARCHIVE.DSA.ArrayWithHashing;

import java.util.HashMap;
import java.util.Map;

public class MaxNumberKSumPairs {

    public static int maxSumPair(int[] arr, int k){
        int count =0;
        Map<Integer, Integer> map = new HashMap<>();
        for(int n : arr){
            int res = k - n;
            if(map.containsKey(res) && map.get(res) > 0){
                count++;
                map.put(res, map.get(res) - 1);
            }else {
                map.put(n, map.getOrDefault(n, 0) + 1);
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int[] arr = {1,2,3,4};
        int k   = 5;
        System.out.println(maxSumPair(arr, k));
    }
}
