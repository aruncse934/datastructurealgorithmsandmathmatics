package FULL_ARCHIVE.DSA.ArrayWithHashing;

import java.util.*;

public class SortCharactersByFrequency {

    public static String freqSort(String s){
        Map<Character,Integer> map = new HashMap<>();
        for(char c : s.toCharArray()){
             map.put(c, map.getOrDefault(c, 0) + 1);
        }
        List<Map.Entry<Character, Integer>> list = new ArrayList<>(map.entrySet());
        list.sort((a, b) -> b.getValue() - a.getValue());
        
        StringBuilder sb  = new StringBuilder();
        for(Map.Entry<Character, Integer> entry : list){
            char  c = entry.getKey();
            int freq = entry.getValue();
            for(int i = 0; i < freq ; i++){
                sb.append(c);
            }
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        String s = "tree";
        System.out.println(freqSort(s));


    }
}
