package FULL_ARCHIVE.DSA.Arrays;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class TwoNumberSum {

    public static void threeSum(int[] arr, int sum){
        for(int i = 0; i < arr.length; i++){
            for(int j = i+1; j < arr.length; j++){
                for(int k = j+1;k< arr.length; k++){
                    if(arr[i]+arr[j]+arr[k] == sum)
                        System.out.println(arr[i]+","+arr[j]+","+arr[k]);
                }
            }
        }

    }


    @Test
    public void testThreeSum() {
        int[] arr = {12, 3, 4, 1, 6, 9};
        int sum = 9;

        threeSum(arr, sum);
    }
    public static int[] findTwoNumberSum(int[] array, int targetSum) {
        Map<Integer, Integer> numbers = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int complement = targetSum - array[i];
            if (numbers.containsKey(complement)) {
                return new int[]{complement, array[i]};
            }
            numbers.put(array[i], i);
        }
        return new int[0];
    }
    public static void main(String[] args) {
        int[] array = {3, 5, -4, 8, 11, 1, -1, 6};
        int targetSum = 10;

     //   System.out.println(Arrays.toString(findTwoNumberSum(array, targetSum)));
    }
}

/*This code uses a Hash Map to store the input numbers and their indices. For each number in the array, we calculate its complement (i.e., the number we need to add to reach the target sum) and check if it exists in the Hash Map. If the complement is already in the Hash Map, we have found a pair of numbers that sum up to the target number, and we return them as an array. If the complement is not in the Hash Map, we add the current number to the Hash Map and continue with the next number in the array.*/