package FULL_ARCHIVE.DSA.Arrays;
import java.util.*;
public class ArrayOfProducts {

    public static int[] arrayOfProducts(int[] array) {
        int[] products = new int[array.length]; // Create a new array to hold the products
        int leftProduct = 1;
        int rightProduct = 1;

        for (int i = 0; i < array.length; i++) {
            products[i] = 1; // Initialize each product to 1

            products[i] *= leftProduct; // Multiply the current product by the product of all elements to the left of i
            leftProduct *= array[i];

            products[array.length - 1 - i] *= rightProduct; // Multiply the current product by the product of all elements to the right of i
            rightProduct *= array[array.length - 1 - i];
        }

        return products; // Return the array of products
    }

    public static int[] arrayOfProductss(int[] nums) {
        int n = nums.length;
        int[] output = new int[n];

        // Compute the product of all elements to the left of each element
        int leftProduct = 1;
        for (int i = 0; i < n; i++) {
            output[i] = leftProduct;
            leftProduct *= nums[i];
        }

        // Compute the product of all elements to the right of each element and multiply it with the corresponding element in the output array
        int rightProduct = 1;
        for (int i = n - 1; i >= 0; i--) {
            output[i] *= rightProduct;
            rightProduct *= nums[i];
        }

        return output;
    }
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4};
        int[] products = arrayOfProductss(array);
        System.out.println(Arrays.toString(products)); // [24, 12, 8, 6]
    }
}
