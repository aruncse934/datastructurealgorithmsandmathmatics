package FULL_ARCHIVE.DSA.Arrays;

public class MinimumNumbersFunctionCallsMakeTargetArray {

    //Bits count
     public static int minOperation(int[] nums){
           int res = 0, maxlen = 1;
           for(int n : nums){
               int bits = 0;
               while (n > 0){
                   res += n&1;
                   bits++;
                   n >>= 1;
               }
               maxlen = Math.max(maxlen,bits);
           }
         return res + maxlen -1;
     }
     // Build Funtion use
     public static int minOperation1(int[] nums) {
       int res = 0, max = 0;
       for(int n : nums){
           if(n == 0) continue;
           res += Integer.bitCount(n);
           max = Math.max(max,Integer.numberOfTrailingZeros(Integer.highestOneBit(n)));
       }
     return res + max;
     }

     //
     public static int minOperation2(int[] nums) {
     int count =0, currDiv = 0;
     int maxDiv = Integer.MIN_VALUE;
     for (int i = 0; i < nums.length; i++){
         currDiv = 0;
         while (nums[i] > 0){
             if(nums[i] % 2 == 0){
                 nums[i] /= 2;
                 currDiv++;
                 if(currDiv > maxDiv){
                     count++;
                     maxDiv = currDiv;
                 }
             } else {
                 nums[i]--;
                 count++;
             }
         }
     }
     return count;
     }
    public static void main(String[] args) {
        int[] nums = {4,2,5};
        System.out.println(minOperation(nums));
        System.out.println(minOperation1(nums));
        System.out.println(minOperation2(nums));
    }
}
