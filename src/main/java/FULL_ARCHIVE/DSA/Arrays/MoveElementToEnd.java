package FULL_ARCHIVE.DSA.Arrays;

import java.util.*;

public class MoveElementToEnd {

    public static List<Integer> moveElementToEnd(List<Integer> array, int toMove) {
        int left = 0; // Initialize the left pointer
        int right = array.size() - 1; // Initialize the right pointer

        while (left < right) { // Loop until the pointers cross each other
            while (left < right && array.get(right) == toMove) { // Move the right pointer until a non-toMove element is found
                right--;
            }
            if (array.get(left) == toMove) { // If the left element is toMove
                Collections.swap(array, left, right); // Swap the left and right elements
            }
            left++; // Move the left pointer to the next element
        }

        return array; // Return the modified array
    }

    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(2, 1, 2, 2, 2, 3, 4, 2));
        int toMove = 2;
        List<Integer> modifiedArray = moveElementToEnd(array, toMove);
        System.out.println(modifiedArray);
    }
}

/*In this example, the moveElementToEnd method takes an array of integers and a target integer as input, and modifies the array by moving all instances of the target integer to the end of the array while maintaining the relative order of the other integers. The method returns the modified array as a list of integers.*/