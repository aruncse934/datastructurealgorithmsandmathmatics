package FULL_ARCHIVE.DSA.Arrays;

import java.util.Arrays;

public class SortedSquareArray {
    public static int[] sortedSquareArray(int[] nums) {
        int[] result = new int[nums.length];
        int left = 0, right = nums.length - 1;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (Math.abs(nums[left]) > Math.abs(nums[right])) {
                result[i] = nums[left] * nums[left];
                left++;
            } else {
                result[i] = nums[right] * nums[right];
                right--;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums ={-4, -2, 0, 1, 3};// {-4, -2, 0, 3, 5};
        System.out.println(Arrays.toString(sortedSquareArray(nums)));
    }
}

/*This code uses two pointers, one starting from the beginning of the array and the other starting from the end of the array. It compares the absolute values of the numbers at the left and right pointers and puts the square of the greater value in the resulting array, starting from the end.*/