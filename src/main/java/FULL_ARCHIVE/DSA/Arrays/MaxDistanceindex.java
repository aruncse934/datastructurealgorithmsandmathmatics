package FULL_ARCHIVE.DSA.Arrays;

import java.util.HashMap;
import java.util.Map;

public class MaxDistanceindex {

    public static void main(String[] args) {
        int[] arr = {3, 5, 4, 2};
        System.out.println(maxIndexDiff(arr));
    }
    static int maxIndexDiff(int arr[]) {
        int n = arr.length;
        int maxDiff = -1;
        int i, j;

        for (i = 0; i < n; ++i) {
            for (j = i+1; j < n; j++) {
                if (arr[j] > arr[i] && maxDiff < (j - i))
                    maxDiff = j - i;
            }
        }

        return maxDiff;
    }

    public static int maxIndexDiff1(int[] A) {
        int n = A.length;
        int maxDiff = -1;

        // Create a map to store the minimum index for each element
        Map<Integer, Integer> minIndex = new HashMap<>();

        // Iterate over the array
        for (int i = 0; i < n; i++) {
            // If the current element is not in the map, add it with the current index
            if (!minIndex.containsKey(A[i])) {
                minIndex.put(A[i], i);
            } else {
                // Calculate the difference between the current index and the minimum index for the element
                int diff = i - minIndex.get(A[i]);
                // Update the maximum difference if necessary
                if (diff > maxDiff) {
                    maxDiff = diff;
                }
            }
        }

        return maxDiff;
    }
}
