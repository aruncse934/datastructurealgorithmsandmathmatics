package FULL_ARCHIVE.DSA.Arrays;

import java.util.Arrays;
public class SmallestDifference {

    public static int[] smallestDifference(int[] array1, int[] array2) {
        Arrays.sort(array1); // Sort the first array in ascending order
        Arrays.sort(array2); // Sort the second array in ascending order
        int i = 0; // Initialize the index for the first array
        int j = 0; // Initialize the index for the second array
        int smallestDiff = Integer.MAX_VALUE; // Initialize the smallest difference to the maximum value
        int[] smallestPair = new int[2]; // Initialize the smallest pair

        while (i < array1.length && j < array2.length) { // Loop until either array is exhausted
            int currentDiff = Math.abs(array1[i] - array2[j]); // Calculate the current difference
            if (currentDiff < smallestDiff) { // If the current difference is smaller than the smallest difference
                smallestDiff = currentDiff; // Update the smallest difference
                smallestPair[0] = array1[i]; // Update the smallest pair
                smallestPair[1] = array2[j];
            }
            if (array1[i] < array2[j]) { // If the current element in the first array is smaller
                i++; // Increment the index for the first array
            } else { // If the current element in the second array is smaller or equal
                j++; // Increment the index for the second array
            }
        }
        return smallestPair; // Return the smallest pair
    }

    public static void main(String[] args) {
        int[] array1 = {1, 3, 15, 11, 2};
        int[] array2 = {23, 127, 235, 19, 8};
        int[] smallestPair = smallestDifference(array1, array2);
        System.out.println(Arrays.toString(smallestPair));
    }
}

/*In this example, the smallestDifference method takes two arrays of integers as input, sorts them in ascending order, and uses the two-pointer technique to find the pair of numbers (one from each array) whose absolute difference is the smallest. The method returns an array of integers containing the smallest pair.*/