package FULL_ARCHIVE.DSA.Arrays;

public class LongestPeak {

    public static int longestPeak(int[] array) {
        int longestPeakLength = 0; // Initialize the length of the longest peak
        int i = 1;

        while (i < array.length - 1) {
            boolean isPeak = array[i - 1] < array[i] && array[i] > array[i + 1]; // Check if i is a peak

            if (!isPeak) {
                i++;
                continue;
            }

            int leftIndex = i - 2; // Traverse left to find the start of the peak
            while (leftIndex >= 0 && array[leftIndex] < array[leftIndex + 1]) {
                leftIndex--;
            }

            int rightIndex = i + 2; // Traverse right to find the end of the peak
            while (rightIndex < array.length && array[rightIndex] < array[rightIndex - 1]) {
                rightIndex++;
            }

            int currentPeakLength = rightIndex - leftIndex - 1; // Calculate the length of the current peak
            longestPeakLength = Math.max(longestPeakLength, currentPeakLength); // Update the length of the longest peak

            i = rightIndex; // Move to the next index after the end of the current peak
        }

        return longestPeakLength; // Return the length of the longest peak
    }

    public static int longestPeaks(int[] arr) {
        int n = arr.length;
        int maxLength = 0;
        int i = 1;

        while (i < n - 1) {
            // Check if the current element is a peak
            if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) {
                int left = i - 2;
                while (left >= 0 && arr[left] < arr[left + 1]) {
                    left--;
                }
                int right = i + 2;
                while (right < n && arr[right] < arr[right - 1]) {
                    right++;
                }

                // Calculate the length of the peak
                int peakLength = right - left - 1;
                if (peakLength > maxLength) {
                    maxLength = peakLength;
                }

                // Move the index to the end of the peak
                i = right;
            } else {
                // Move the index to the next element
                i++;
            }
        }

        return maxLength;
    }


    public static void main(String[] args) {
        int[] array = {1, 3, 2, 4, 7, 6, 5, 4, 2, 1};
        int longestPeakLength = longestPeaks(array);
        System.out.println(longestPeakLength); // 6
    }
}
/*In this example, the longestPeak method takes an array of integers as input, and finds the length of the longest peak in the array. The method returns an integer representing the length of the longest peak.*/
