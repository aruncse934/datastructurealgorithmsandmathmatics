package FULL_ARCHIVE.DSA.Arrays;

import java.util.*;

public class FirstDuplicateValue {
    public static int findFirstDuplicate(int[] arr) {
        Set<Integer> seen = new HashSet<Integer>();
        for (int i = 0; i < arr.length; i++) {
            if (seen.contains(arr[i])) {
                return arr[i];
            } else {
                seen.add(arr[i]);
            }
        }
        return -1; // no duplicate found
    }

    public static void main(String[] args) {
        int[] arr = { 2, 3, 4, 5, 2, 6, 7 };
        int firstDuplicate = findFirstDuplicate(arr);
        System.out.println("The first duplicate value is " + firstDuplicate);

    }
}
