package FULL_ARCHIVE.DSA.Arrays;

import java.util.*;
import java.util.HashMap;
import java.util.Map;

public class TournamentWinner {

    public static String tournamentWinner(ArrayList<ArrayList<String>> competitions, ArrayList<Integer> results) {
        Map<String, Integer> scores = new HashMap<String, Integer>();
        String currentBestTeam = "";
        scores.put(currentBestTeam, 0);

        for (int i = 0; i < competitions.size(); i++) {
            String homeTeam = competitions.get(i).get(0);
            String awayTeam = competitions.get(i).get(1);

            String winningTeam = results.get(i) == 0 ? awayTeam : homeTeam;

            if (!scores.containsKey(winningTeam)) {
                scores.put(winningTeam, 0);
            }
            scores.put(winningTeam, scores.get(winningTeam) + 3);

            if (scores.get(winningTeam) > scores.get(currentBestTeam)) {
                currentBestTeam = winningTeam;
            }
        }

        return currentBestTeam;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<String>> competitions = new ArrayList<ArrayList<String>>();
        ArrayList<String> competition1 = new ArrayList<String>(Arrays.asList("HTML", "C#"));
        ArrayList<String> competition2 = new ArrayList<String>(Arrays.asList("C#", "Python"));
        ArrayList<String> competition3 = new ArrayList<String>(Arrays.asList("Python", "HTML"));
        competitions.add(competition1);
        competitions.add(competition2);
        competitions.add(competition3);

        ArrayList<Integer> results = new ArrayList<Integer>(Arrays.asList(0, 0, 1));

        System.out.println(tournamentWinner(competitions,results));
    }
}

/*This code uses a HashMap to keep track of the scores of each team. It starts by setting the score of the current best team to 0 and adding it to the HashMap. Then, it iterates over the competitions and results, updating the scores of the winning teams and keeping track of the current best team.*/