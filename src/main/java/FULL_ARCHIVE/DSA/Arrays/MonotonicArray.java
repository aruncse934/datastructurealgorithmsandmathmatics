package FULL_ARCHIVE.DSA.Arrays;

public class MonotonicArray {

    public static boolean isMonotonic(int[] array) {
        boolean isNonDecreasing = true; // Initialize a flag for non-decreasing order
        boolean isNonIncreasing = true; // Initialize a flag for non-increasing order

        for (int i = 1; i < array.length; i++) { // Loop through the array
            if (array[i] < array[i - 1]) { // If the current element is less than the previous element
                isNonDecreasing = false; // The array is not non-decreasing
            }
            if (array[i] > array[i - 1]) { // If the current element is greater than the previous element
                isNonIncreasing = false; // The array is not non-increasing
            }
        }

        return isNonDecreasing || isNonIncreasing; // The array is monotonic if either flag is true
    }

    public static void main(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5};
        int[] array2 = {5, 4, 3, 2, 1};
        int[] array3 = {1, 2, 2, 3, 4};
        System.out.println(isMonotonic(array1)); // true
        System.out.println(isMonotonic(array2)); // true
        System.out.println(isMonotonic(array3)); // true
    }
}

/*In this example, the isMonotonic method takes an array of integers as input, and determines whether the array is monotonic, i.e., whether it is entirely non-increasing or entirely non-decreasing. The method returns a boolean value indicating whether the array is monotonic or not.*/
