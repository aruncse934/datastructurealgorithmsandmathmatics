package FULL_ARCHIVE.DSA.Arrays;

import java.util.Arrays;

public class ProductArrayExceptSelf {
    public static void main(String[] args) {
      int[] nums = {1,2,3,4};
        System.out.println(Arrays.toString(productExceptSelf(nums)));
    }

    public static int[] productExceptSelf(int[] nums){
        int[] arr = new int[nums.length];
        int right = 1, left = 1;
        for(int i = 0; i< nums.length; i++){
            arr[i] = left;
            left *= nums[i];
        }
        for(int i = nums.length -1; i >= 0; i--){
            arr[i] *= right;
            right *= nums[i];
        }
        return arr;
    }
}

/* public int[] productExceptSelf(int[] nums) {
    int n = nums.length;
    int[] res = new int[n];
    res[0] = 1;
    for (int i = 1; i < n; i++) {
        res[i] = res[i - 1] * nums[i - 1];
    }
    int right = 1;
    for (int i = n - 1; i >= 0; i--) {
        res[i] *= right;
        right *= nums[i];
    }
    return res;
}

    }*/