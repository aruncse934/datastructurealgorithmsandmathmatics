package FULL_ARCHIVE.DSA.Arrays;

import java.util.*;

public class SpiralTraverse {

    public static List<Integer> spiralTraverse(int[][] array) {
        List<Integer> result = new ArrayList<>(); // Initialize a list to store the spiral order

        int startRow = 0; // Initialize the starting row
        int endRow = array.length - 1; // Initialize the ending row
        int startCol = 0; // Initialize the starting column
        int endCol = array[0].length - 1; // Initialize the ending column

        while (startRow <= endRow && startCol <= endCol) { // Loop until all elements have been traversed
            for (int col = startCol; col <= endCol; col++) { // Traverse the top row from left to right
                result.add(array[startRow][col]);
            }
            for (int row = startRow + 1; row <= endRow; row++) { // Traverse the right column from top to bottom
                result.add(array[row][endCol]);
            }
            for (int col = endCol - 1; col >= startCol && startRow != endRow; col--) { // Traverse the bottom row from right to left
                result.add(array[endRow][col]);
            }
            for (int row = endRow - 1; row > startRow && startCol != endCol; row--) { // Traverse the left column from bottom to top
                result.add(array[row][startCol]);
            }
            startRow++; // Move to the next starting row
            endRow--; // Move to the next ending row
            startCol++; // Move to the next starting column
            endCol--; // Move to the next ending column
        }

        return result; // Return the spiral order list
    }

    public static void main(String[] args) {
        int[][] array = {{1, 2, 3, 4}, {10, 11, 12, 5}, {9, 8, 7, 6}};
        List<Integer> spiralOrder = spiralTraverse(array);
        System.out.println(spiralOrder); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    }
}
/*In this example, the spiralTraverse method takes a 2D array of integers as input, and traverses the array in a spiral order. The method returns a list of integers containing the elements in the spiral order.*/