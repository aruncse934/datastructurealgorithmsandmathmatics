package FULL_ARCHIVE.DSA.Arrays;
import java.util.*;
class Interval {
    int start;
    int end;

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
public class MergeOverlappingIntervals {
    public List<Interval> merges(List<Interval> intervals) {
        // Sort the intervals based on their start times
        intervals.sort((i1, i2) -> Integer.compare(i1.start, i2.start));

        List<Interval> mergedIntervals = new ArrayList<>();

        // Initialize the current interval with the first interval in the sorted list
        Interval current = intervals.get(0);

        for (int i = 1; i < intervals.size(); i++) {
            Interval interval = intervals.get(i);

            // If the current interval and the current one overlap, update the end time of the current interval
            if (interval.start <= current.end) {
                current.end = Math.max(current.end, interval.end);
            } else {
                // If they do not overlap, add the current interval to the merged list and update "current"
                mergedIntervals.add(current);
                current = interval;
            }
        }

        // Add the final current interval to the merged list
        mergedIntervals.add(current);

        return mergedIntervals;
    }
    public static void main(String[] args) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(8, 10));
        intervals.add(new Interval(15, 18));

        List<Interval> mergedIntervals = new MergeOverlappingIntervals().merges(intervals);

        System.out.println(mergedIntervals);
    }
}
