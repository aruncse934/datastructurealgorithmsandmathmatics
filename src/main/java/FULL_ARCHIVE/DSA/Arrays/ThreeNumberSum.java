package FULL_ARCHIVE.DSA.Arrays;

import java.util.*;

public class ThreeNumberSum {

    public static List<int[]> threeNumberSum(int[] array, int targetSum) {
        Arrays.sort(array); // Sort the array in ascending order
        List<int[]> triplets = new ArrayList<>(); // Create a list to store the triplets

        for (int i = 0; i < array.length - 2; i++) {
            int left = i + 1; // Initialize the left pointer
            int right = array.length - 1; // Initialize the right pointer

            while (left < right) { // Loop until the left pointer meets the right pointer
                int currentSum = array[i] + array[left] + array[right];
                if (currentSum == targetSum) { // If the current sum equals the target sum
                    int[] triplet = {array[i], array[left], array[right]}; // Create a triplet
                    triplets.add(triplet); // Add the triplet to the list
                    left++; // Increment the left pointer to find the next triplet
                    right--; // Decrement the right pointer to find the next triplet
                } else if (currentSum < targetSum) { // If the current sum is less than the target sum
                    left++; // Increment the left pointer to increase the sum
                } else { // If the current sum is greater than the target sum
                    right--; // Decrement the right pointer to decrease the sum
                }
            }
        }
        return triplets; // Return the list of triplets
    }
    public static void main(String[] args) {
        int[] array = {12, 3, 1, 2, -6, 5, -8, 6};
        int targetSum = 0;
        List<int[]> triplets = threeNumberSum(array, targetSum);
        for (int[] triplet : triplets) {
            System.out.println(Arrays.toString(triplet));
        }
    }
}

/*In this example, the threeNumberSum method takes an array of integers and a target sum as input, sorts the array in ascending order, and uses the two-pointer technique to find all the unique triplets in the array that add up to the target sum. The method returns a list of integer arrays, each of which contains three elements representing a triplet.*/