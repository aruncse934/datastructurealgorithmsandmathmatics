package FULL_ARCHIVE.DSA.Arrays;
import java.util.*;
public class NonConstructibleChange {
    public static int nonConstructibleChange(int[] coins) {
        Arrays.sort(coins);
        int currentChangeCreated = 0;
        for (int coin : coins) {
            if (coin > currentChangeCreated + 1) {
                return currentChangeCreated + 1;
            }
            currentChangeCreated += coin;
        }
        return currentChangeCreated + 1;
    }
    public static void main(String[] args) {
        int[] coins = {1, 2, 5};
        System.out.println(nonConstructibleChange(coins));
    }
}

/*This code takes an array of integers representing the available coins and sorts them in ascending order. It then iterates over the coins, keeping track of the smallest amount of change that cannot be created using the coins seen so far. This is done by adding each coin to the sum of coins seen so far and comparing it to the next coin. If the sum plus 1 is less than the next coin, the function returns the current sum plus 1, since this is the smallest amount of change that cannot be created.

 */