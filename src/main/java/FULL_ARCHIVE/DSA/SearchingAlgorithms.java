package FULL_ARCHIVE.DSA;

import com.google.common.math.BigIntegerMath;

import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.*;
public class SearchingAlgorithms {

      //Linear Search Algorithm TC o(n)
    public static int linearSerach(int[] arr, int k){
        for(int i = 0; i < arr.length; i++){
            if(arr[i] == k) return i;
        }
        return -1;
    }

    // Recursive Linear Search:   TC o(n)
    public static int recursiveLinearSearch(int[] arr,int n, int k){
        if(n == 0) return -1;
        else if (arr[n-1]==k) {
            return n-1;
        }
        return recursiveLinearSearch(arr,n-1,k);
    }


    //Sentinel Lenear Search 1   TC o(n)
    public static void sentinelLenearSearch(int[] arr,int k){
        int n = arr.length;
        int last = arr[n-1];
        arr[n-1]=k;
        int i = 0;
        while (arr[i] != k) i++;
        arr[n-1] =last;
        if((i< n-1) ||(arr[n-1] == k))
            System.out.println(k+" is present at index "+i);
        else
            System.out.println("Element Not found");
    }

    //    Sentinel Lenear Search 2    TC o(n)

    public static int sentinelLinearSearch2(int[] arr, int k){
        int last = arr[arr.length-1];
        arr[arr.length-1]=k;
        int i = 0;
        while (arr[i] != k){ i++;   }
        arr[arr.length-1]=last;
        if(i <arr.length-1 || last == k){ return i;}
        else { return -1;}
    }

    //Iterative  Binary Search Algorithm  TC O(log N)
    public static int iterativeBinarySearch(int[] arr, int x){
         int l = 0, r = arr.length-1;
         while (l <= r){
             int m = l+(r-l)/2;
             if(arr[m]==x) return m;
             if(arr[m]<x) l = m +1;
             else r = m -1;
         }
         return -1;
    }
    //Approach 1: Find the Exact Value   Binary Search Algorithm  TC O(log N)

     public int search(int[] nums, int target){
        int l = 0, r = nums.length -1;
        while(l <= r){
            int mid = l + (r -l)/2;
            if(nums[mid] == target){ return mid;}
            else if(nums[mid] < target) { return l = mid + 1;}
            else { r = mid - 1;}
        }
        return -1;
     }
 //Approach 2: Find Upper bound     Binary Search Algorithm  TC O(log N)
     public int search1(int[] nums, int target){
        int left = 0, right = nums.length;
        while (left < right){
            int mid = left + (right -left)/2;
            if(nums[mid] <= target){left = mid + 1;}
            else { right = mid;}
            }
        if(left > 0 && nums[left-1]==target){return left-1;}
        else {
            return -1;
        }
     }

  //  Approach 3: Find Lower bound    Binary Search Algorithm  TC O(log N)
      public int search2(int[] nums, int target) {
         int left = 0, right = nums.length;
         while (left < right){
             int mid = left + (right -left) /2;
             if(nums[mid] >= target){
                  right =mid;
             }else {
                 left = mid +1;
             }
         }
         if(left < nums.length && nums[left] == target){
             return left;
         } else {
             return -1;
         }
     }

   
   //Recursive  Binary Search Algorithm:  O(log N)
     public static int recursiveBinarySearch(int[] arr, int l, int r, int x){
        if(r >= l) {
            int mid = l + (r - l) / 2;
            if(arr[mid] == x) return mid;
            if(arr[mid] > x) return recursiveBinarySearch(arr,l,mid-1,x);
           else return recursiveBinarySearch(arr,mid+1, r,x);
        }
        return -1;
     }

     //Meta Binary Search    O(log N)
    public static int metaBinarySearch(Vector<Integer> A, int k){
        int n = A.size();
        int length = BigIntegerMath.log2(BigInteger.valueOf(n-1), RoundingMode.UNNECESSARY)+1;
        int pos = 0;
        for(int i = length -1; i >= 0; i--){
            if(A.get(pos) == k){return pos;}

            int new_pos = pos | (1 << i);
            if((new_pos < n) && (A.get(new_pos) <= k)){ pos = new_pos;}
        }

        return ((A.get(pos) == k) ? pos : -1);

    }

    //Recursive Implementation of Ternary Search   O(log3n)
      public static int ternarySearch(int l, int r, int k, int[] arr){
        if(r >= l){
            int mid1 = l + (r-l) /3;
            int mid2 = r - (r - l) /3;

            if(arr[mid1] == k){ return mid1; }
            if(arr[mid2] == k){ return mid2; }
            if(k < arr[mid1]){ return ternarySearch(l, mid1 -1, k, arr); }
            else if(k >  arr[mid2]) { return ternarySearch(mid2 + 1, r, k,arr); }
            else { return ternarySearch(mid1+1,mid2 -1, k, arr); }
        }
        return -1;
      }


      //Jump Search Algo Time Complexity : O(√n)

    public static int jumpSearch(int[] arr, int k){
        int n = arr.length;
        int step = (int) Math.floor(Math.sqrt(n));
        int prev = 0;
        for(int minStep = Math.min(step,n)-1; arr[minStep] < k; minStep = Math.min(step, n) -1){
            prev = step;
            step += Math.floor(Math.sqrt(n));
            if(prev >= n) return -1;
        }
        while (arr[prev] < k){
            prev++;
            if(prev == Math.min(step, n)) return -1;
        }
        if(arr[prev] == k) return prev;
        return -1;
    }

    //Interpolation Search algorithm Time Complexity: O(log2(log2 n))

    public static int interpolationSearch(int[] arr, int lo, int hi, int x){
         int pos;
         if(lo <= hi && x >= arr[lo] && x <= arr[hi]){
             pos = lo + (((hi - lo) / (arr[hi] - arr[lo])) * (x - arr[lo]));
             if(arr[pos] == x) return pos;
             if(arr[pos] < x) return interpolationSearch(arr, pos + 1, hi, x);
             if(arr[pos] > x) return interpolationSearch(arr, lo, pos-1,x);
         }
         return -1;
    }

    //Exponential Search Algo Time Complexity : O(Log n)
    public static int exponentialSearch(int[] arr, int n, int x){
        if(arr[0] == x) return 0;
        int i = 1;
        while (i < n && arr[i] <= x) i = i*2;
        return Arrays.binarySearch(arr, i/2, Math.min(i, n-1), x);
    }

    //Fibonacci Search Algo Time Complexity : O(Log n)

    public static int fibonacciSearch(int[] arr, int x){
        int n = arr.length;
        if (n == 0){ return -1;}

        return -1;
    }

    /*public static int fibonacciSearch(int[] arr, int x) {
    int n = arr.length;
    if (n == 0) {
      return -1;
    }

    // Initialize Fibonacci numbers
    int fib1 = 0, fib2 = 1, fib3 = fib1 + fib2;

    // Find the smallest Fibonacci number greater than or equal to n
    while (fib3 < n) {
      fib1 = fib2;
      fib2 = fib3;
      fib3 = fib1 + fib2;
    }

    // Initialize variables for the current and previous split points
    int offset = -1;
    while (fib3 > 1) {
      int i = Math.min(offset + fib2, n-1);

      // If x is greater than the value at index i, move the split point to the right
      if (arr[i] < x) {
        fib3 = fib2;
        fib2 = fib1;
        fib1 = fib3 - fib2;
        offset = i;
      }

      // If x is less than the value at index i, move the split point to the left
      else if (arr[i] > x) {
        fib3 = fib1;
        fib2 = fib2 - fib1;
        fib1 = fib3 - fib2;
      }

      // If x is equal to the value at index i, return the index
      else {
        return i;
      }
    }

    // If x is not found in the array, return -1
    if (fib2 == 1 && arr[offset+1] == x) {
      return offset + 1;
    } else {
      return -1;
    }
  }*/
   
    
  
    

    public static void main(String[] args) {
        int arr[] = { 20,30,9, 0, 2, 3, 4, 10, 40 };
        int k = 10;


        int result = linearSerach(arr,k);
        if(result == -1)
            System.out.print("Element is not present in array");
        else
            System.out.print("Element is present at index  :: "+result);

        System.out.println("\n===========");
        int arr1[] = { 5, 15, 6, 9, 4 };
        int key = 4;

        // Function call to find key
        int index = recursiveLinearSearch(arr1, arr1.length, key);
        if (index != -1)
            System.out.println("The element " + key + " is found at " + index + " index of the given array.");
        else
            System.out.println("The element " + key + " is not found.");

        System.out.println();

        int arr2[] = { 10, 20, 180, 30, 60, 50, 110, 100, 70 };
        int k1 = 180;
        sentinelLenearSearch(arr2,k1);

        System.out.println();

        int[] arr3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int k3 = 5;
        int index3 = sentinelLinearSearch2(arr3, k3);
        if (index3 == -1) {
            System.out.println(k3 + " is not found in the array: " + Arrays.toString(arr3));
        } else {
            System.out.println(k3 + " is found at index " + index3 + " in the array: " + Arrays.toString(arr3));
        }

        System.out.println();

        int arr4[] = { 2, 3, 4, 10, 40 };

        int x4 = 10;
        int result4 = iterativeBinarySearch(arr4, x4);
        if (result4 == -1) System.out.println("Element is not present in array");
        else System.out.println("Element is present at " + "index " + result4);

        System.out.println();

        int[] arr5 = { 2, 3, 4, 10, 40 };
        int x5 = 10;
        int result5 = recursiveBinarySearch(arr, 0, arr5.length - 1, x5);
        if (result5 == -1)
            System.out.println("Element is not present in array");
        else
            System.out.println("Element is present at index " + result5);

        System.out.println();

        Vector<Integer> A = new Vector<Integer>();
        int[] arr6 = {-2, 10, 100, 250, 32315};
        for (int i = 0; i < arr6.length; i++) {
            A.add(arr6[i]);
        }
        System.out.println(metaBinarySearch(A, 10));


        System.out.println();
        
        int l =0, r= 9,k0 = 5;
        int ar[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int p = ternarySearch(l, r, k0, ar);
        System.out.println("Index of " + k0 + " is " + p);

        System.out.println("Jump Search :: ");

        int ar6[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610};
        int x2 = 55;
        int inde = jumpSearch(ar6, x2);
        System.out.println("\nNumber " + x2 + " is at index " + inde);

        System.out.println("Interpolation Search :: ");

        int ar1[] = { 10, 12, 13, 16, 18, 19, 20, 21, 22, 23, 24, 33, 35, 42, 47 };
        int n = ar1.length;
        int x = 18;
        int ind = interpolationSearch(ar1, 0, n - 1, x);
        System.out.println((ind == -1) ? "Element not found." : "Element found at index " + ind);

        System.out.println("Exponential Search :: ");

        int ar2[] = {2, 3, 4, 10, 40};
        int x1= 10;
        int resu = exponentialSearch(ar2, ar2.length, x1);
        System.out.println((resu < 0) ? "Element is not present in array" : "Element is present at index " + resu);
        

    }
}
