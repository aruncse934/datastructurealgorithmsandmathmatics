package FULL_ARCHIVE.DSA;

public class BestTimeBuySellStockCooldown {
        public int maxProfit(int[] prices) {
            int sold =0, hold = -prices[0], rest =0;
            for(int i = 1; i< prices.length; i++){
                int preSold = sold;
                sold = hold + prices[i];
                hold = Math.max(hold, rest-prices[i]);
                rest = Math.max(rest, preSold);
            }
            return Math.max(sold, rest);
    }

    public static void main(String[] args) {
        int[] prices = {1,2,3,0,2};
        System.out.println(new BestTimeBuySellStockCooldown().maxProfit(prices));
    }
}
