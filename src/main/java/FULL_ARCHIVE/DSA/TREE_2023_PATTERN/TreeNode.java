package FULL_ARCHIVE.DSA.TREE_2023_PATTERN;

public class TreeNode {

    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }
}
