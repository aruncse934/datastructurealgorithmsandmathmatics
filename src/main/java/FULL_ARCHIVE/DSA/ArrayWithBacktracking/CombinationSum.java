package FULL_ARCHIVE.DSA.ArrayWithBacktracking;

import java.util.ArrayList;
import java.util.List;

public class CombinationSum {


    public List<List<Integer>> combinationSum(int[] candidates, int target) {

        List<List<Integer>> ans = new ArrayList<>();
        List<Integer>   cur = new ArrayList<>();
          backtrack(candidates, target, ans, cur, 0);
        return ans;
    }

    private void backtrack(int[] candidates, int target, List<List<Integer>> ans, List<Integer> cur, int i) {
        if(target ==0) ans.add(new ArrayList<>(cur));
        else if(target < 0 || i >= candidates.length) return;
        else {
            cur.add(candidates[i]);
            backtrack(candidates, target - candidates[i], ans, cur, i);
            cur.remove(cur.get(cur.size() - 1));
            backtrack(candidates, target, ans, cur, i + 1);
        }

    }

    public static void main(String[] args) {
        int arr[] = {2,3,6,7};
        int target = 7; //[[2,2,3],[7]]
        System.out.println(new CombinationSum().combinationSum(arr,target));
    }
}
