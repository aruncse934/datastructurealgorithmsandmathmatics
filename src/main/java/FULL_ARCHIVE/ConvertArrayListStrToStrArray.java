package FULL_ARCHIVE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ConvertArrayListStrToStrArray {

    public static void main(String[] args) {
        List<String>  list = new ArrayList<>();
        list.add("android");
        list.add("apple");


        String[] strArray = list.toArray(new String[0]);
        System.out.println(Arrays.toString(strArray));

        //java 8
        String[] array = list.stream().toArray(String[]::new);
        System.out.println(Arrays.toString(array));

        //Java 11+
        String[] strings = list.toArray(String[]::new);
        System.out.println(Arrays.toString(strings));

       //forEach(
        String[] string = new String[list.size()];
        string  = list.toArray(string);
        for (String s : string){
            System.out.println(s);
        }

        //
        String[] arr = list.toArray(new String[list.size()]);
        System.out.println(Arrays.toString(arr));

        List<String> obj = Arrays.asList("ant", "1", "cat", "ant",
                "we", "java", "cat");
        obj.stream().filter(i -> Collections.
                        frequency(obj, i) > 1).collect(Collectors.toSet())
                .forEach(System.out::println);

    }
}
