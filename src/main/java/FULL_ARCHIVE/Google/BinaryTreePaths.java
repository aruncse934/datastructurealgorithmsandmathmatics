package FULL_ARCHIVE.Google;

import AlgoApproach.Tree.BinarySearchTree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {
    
    public static List<String> binaryTreePaths(TreeNode root){
        List<String> res = new ArrayList<>();
        if(root != null) helper(root, "", res);
        return res;
        
    }

    private static void helper(TreeNode root, String s, List<String> res) {
    }

    public static void main(String[] args) {

    }
}
/*public List<String> binaryTreePaths(TreeNode root) {
		List<String> res = new ArrayList<>();
		helper(res, root, "");
		return res;
	}

	public void helper(List<String> res, TreeNode node, String str) {
		if(node == null) {
			return;
		}
		str += node.val;
		if(node.left == null && node.right == null) {
			res.add(str);
			return;
		}
		if(node.left != null || node.right != null) {
			str += "->";
		}
		helper(res, node.left, str);
		helper(res, node.right, str);
	}*/

/*public List<String> binaryTreePaths(TreeNode root) {
    List<String> answer = new ArrayList<String>();
    if (root != null) searchBT(root, "", answer);
    return answer;
}
private void searchBT(TreeNode root, String path, List<String> answer) {
    if (root.left == null && root.right == null) answer.add(path + root.val);
    if (root.left != null) searchBT(root.left, path + root.val + "->", answer);
    if (root.right != null) searchBT(root.right, path + root.val + "->", answer);
}*/