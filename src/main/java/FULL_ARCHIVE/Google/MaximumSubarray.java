package FULL_ARCHIVE.Google;

public class MaximumSubarray {

    public int maxSubarray(int[] nums){
        int n = nums.length;
        int maxd = Integer.MIN_VALUE, sum = 0;
        for(int i = 0; i < n; i++){
            sum += nums[i];
            maxd = Math.max(sum,maxd);

            if (sum < 0) sum = 0;
        }
        return maxd;
    }

    public static void main(String[] args) {
        int nums[] = {-2,1,-3,4,-1,2,1,-5,4};
        int ans = new MaximumSubarray().maxSubarray(nums);
        System.out.println(ans);
    }
}
