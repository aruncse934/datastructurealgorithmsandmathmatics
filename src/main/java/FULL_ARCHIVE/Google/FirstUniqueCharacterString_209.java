package FULL_ARCHIVE.Google;

import java.util.HashMap;
import java.util.Map;

public class FirstUniqueCharacterString_209 {

    //
    public static char firstUniqueChar(String str){
        int[] a = new int[26];
        char[] chars = str.toCharArray();
        for(char c : chars){
            a[c - 'a']++;
        }
        char ans = '\0';
        for(int i = 0; i < chars.length;i++){
            if(a[i] == 1){
                ans = (char) ('a'+i);
                break;
            }
        }
        return ans;
    }

    //
    public static int firstUniqChar(String s) {
        int[] arr = new int[26];
        char[] ch = s.toCharArray();
        for(char c : ch){
            arr[c - 'a']++;
        }
        for(int i = 0; i < ch.length;++i){
            if(arr[ch[i] - 'a'] == 1)
            return i;
        }
        return -1;
    }

    public int firstUniqChars(String s) {
        Map<Character, Integer> count = new HashMap<Character, Integer>();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            count.put(c, count.getOrDefault(c, 0) + 1);
        }
        for (int i = 0; i < n; i++) {
            if (count.get(s.charAt(i)) == 1)
                return i;
        }
        return -1;
    }

    public static void main(String[] args) {
        String s ="loveleetcode";
        System.out.println(firstUniqueChar(s));

        System.out.println(firstUniqChar(s));

    }
}
