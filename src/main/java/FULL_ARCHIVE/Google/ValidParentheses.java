package FULL_ARCHIVE.Google;

import java.util.Stack;

public class ValidParentheses {

    public static boolean isValidParentheses(String s){
        Stack<Character> stack = new Stack<>();
        for(char c : s.toCharArray()){
            if (c == '(' || c == '{' || c =='['){
                stack.push(c);
            }
            if( c == ')'){
                if(stack.isEmpty() || stack.pop() != '(')
                    return false;
            }
            if( c == '}'){
                if(stack.isEmpty() || stack.pop() != '{')
                    return false;
            }

            if( c == ']'){
                if(stack.isEmpty() || stack.pop() != '[')
                    return false;
            }
        }
        return stack.isEmpty();
    }

    public static boolean isValidParenth(String s){
        Stack<Character> stack = new Stack<>();
        for(char c : s.toCharArray()){
            if( c == '(')
                stack.push(')');
            else if (c == '{')
                stack.push('}');
            else if(c == '[')
                stack.push(']');
            else if(stack.isEmpty() || stack.pop() != c)
                return false;
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        String s = "(){}[]";

        System.out.println(isValidParenth(s));
    }
}
