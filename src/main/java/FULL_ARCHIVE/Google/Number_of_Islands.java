package FULL_ARCHIVE.Google;

public class Number_of_Islands {

    public static int numberOfIslands(boolean[][] grid){
        if (grid==null)return 0;
        int row = grid.length;
        if (row<=0)return 0;
        int col = grid[0].length;
        int count = 0;
        for (int i = 0;i<row;i++){
            for (int j =0;j<col;j++){
                if(grid[i][j] ==true){
                    count++;
                    dfs(grid, i, j);
                }
            }
        }
        return count;
    }
    private static void dfs(boolean[][] grid, int i, int j){
        int row = grid.length;
        int col = grid[0].length;
        if(i < 0 || j < 0 || i > row -1 || j > col -1 || !grid[i][j]){
            return;
        }
        grid[i][j] = false;
        dfs(grid, i-1, j);
        dfs(grid, i+1, j);
        dfs(grid, i, j-1);
        dfs(grid, i, j+1);
    }

    public static void main(String[] args) {
      //  int[][] grid = {{1,1,0,0,0},{0,1,0,0,1},{0,0,0,1,1},{0,0,0,0,0},{0,0,0,0,1}};
        boolean[][] grid = {  {true, true, false, false, false}, {false, true, false, false, true}, {false, false, false, true, true}, {false, false, false, false, false}, {false, false, false, false, true}};
        System.out.println(numberOfIslands(grid));

    }
}
/*
public int numIslands(char[][] grid) {
    int count = 0;
    n = grid.length;
    if (n == 0) return 0;
    m = grid[0].length;
    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++)
            if (grid[i][j] == '1') {
                DFSMarking(grid, i, j);
                ++count;
            }
    }
    return count;
}

private void DFSMarking(char[][] grid, int i, int j) {
    if (i < 0 || j < 0 || i >= n || j >= m || grid[i][j] != '1') return;
    grid[i][j] = '0';
    DFSMarking(grid, i + 1, j);
    DFSMarking(grid, i - 1, j);
    DFSMarking(grid, i, j + 1);
    DFSMarking(grid, i, j - 1);
}}*/