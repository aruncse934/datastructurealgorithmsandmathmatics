573 · Build Post Office II

Description
Given a 2D grid, each cell is either a wall 2, an house 1 or empty 0 (the number zero, one, two), find a place to build a post office so that the sum of the distance from the post office to all the houses is smallest.

Returns the sum of the minimum distances from all houses to the post office.Return -1 if it is not possible.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


You cannot pass through wall and house, but can pass through empty.
You only build post office on an empty.
Example
Example 1:

Input：[[0,1,0,0,0],[1,0,0,2,1],[0,1,0,0,0]]
Output：8
Explanation： Placing a post office at (1,1), the distance that post office to all the house sum is smallest.

public class Solution {
    /**
     * @param grid: a 2D grid
     * @return: An integer
     */
    int[] deltaX = {-1, 0, 1, 0};
    int[] deltaY = {0, 1, 0, -1};
    int m = 0, n = 0;
    
    public int shortestDistance(int[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        m = grid.length;
        n = grid[0].length;
        int[][] totalDistance = new int[m][n];
        int step = 0, res = 0;
        
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j] == 1) {
                    res = bfs(grid, i, j, step, totalDistance);
                    step--;
                }
            }
        }
        
        return res == Integer.MAX_VALUE ? -1 : res;
    }
    
    private int bfs(int[][] grid, int x, int y, int step, int[][] totalDistance) {
        int res = Integer.MAX_VALUE;
        
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(x * n + y);
        
        int curDis = 0;
        while (!queue.isEmpty()) {
            int l = queue.size();
            curDis++;
            while (l-- != 0) {
                int t = queue.poll();
                x = t / n;
                y = t % n;
                
                for (int i = 0; i < 4; ++i) {
                    int _x = x + deltaX[i], _y = y + deltaY[i];
                    if (_x >= 0 && _x < m && _y >= 0 && _y < n && grid[_x][_y] == step) {
                        queue.offer(_x * n + _y);
                        totalDistance[_x][_y] += curDis;
                        grid[_x][_y]--;
                        res = Math.min(res, totalDistance[_x][_y]);
                        //System.out.println(res);
                    }
                }
            }
        }
        return res;
    }
}

623 · K Edit Distance

Description
Given a set of strings which just has lower case letters and a target string, output all the strings for each the edit distance with the target no greater than k.

You have the following 3 operations permitted on a word:

Insert a character
Delete a character
Replace a character
Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Given words = `["abc", "abd", "abcd", "adc"]` and target = `"ac"`, k = `1`
Return `["abc", "adc"]`
Input:
["abc", "abd", "abcd", "adc"]
"ac"
1
Output:
["abc","adc"]

Explanation:
"abc" remove "b"
"adc" remove "d"

public class Solution {
    /**
     * @param words: a set of stirngs
     * @param target: a target string
     * @param k: An integer
     * @return: output all the strings that meet the requirements
     */
    public List<String> kDistance(String[] words, String target, int k) {
        Map<String, Integer> memo = new HashMap<>();
        List<String> res= new ArrayList<>();
        for(String word : words){
            String w = word.substring(0), t = target.substring(0);
            if(!word.equals("") && !target.equals("")){
                int i = 0;
                while(i < w.length()&& i < t.length() && w.charAt(i) == t.charAt(i)){
                    i++;
                }
                w = w.substring(i); t = t.substring(i);
            }
            if ((w.length()<=k) &&  (t.length() <=k)){
                res.add(word);
                continue;
            }
            if (Math.abs(w.length() - t.length()) > k){
                continue;
            }
            if(w.equals(t)){
                res.add(word); continue;
            }
            if(dfs(memo, w,t, k) <= k){
                res.add(word);
            }
        }
        return res;    
    }
    public int dfs(Map<String, Integer> memo, String word, String target, int k){
        String s = word+'$'+target;
        int res = 0;
        if(memo.containsKey(s)) return memo.get(s);
        if(Math.abs(word.length()- target.length()) > k) return Math.abs(word.length()- target.length());
        if(word.equals("")) return target.length();
        if(target.equals("")) return word.length();
        if(word.equals(target)) return 0;
        String subw = word.substring(1), subt = target.substring(1);
        if(word.charAt(0) == target.charAt(0)){
            res = dfs(memo, subw, subt,k);
        }else{
            int tmp1 = 0, tmp2 = 0, tmp3 = 0;
            tmp1 = dfs(memo,word,subt,k);
            tmp2 = dfs(memo,subw, target, k);
            tmp3 = dfs(memo,subw, subt, k);
            res = Math.min(Math.min(tmp1, tmp2), tmp3) +1;
        }
        memo.put(s, res);
        return res;
    }
}


725 · Boolean Parenthesization

Description
Given a boolean expression with following symbols.

Symbols
    'T' ---> true 
    'F' ---> false 
And following operators filled between symbols

Operators
    &   ---> boolean AND
    |   ---> boolean OR
    ^   ---> boolean XOR 
Count the number of ways we can parenthesize the expression so that the value of expression evaluates to true.

Let the input be in form of two arrays one contains the symbols (T and F) in order and other contains operators (&, | and ^}

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Input： ['T', 'F', 'T']，['^', '&']
Output：2
Explanation：
The given expression is "T ^ F & T", it evaluates true, in two ways "((T ^ F) & T)" and "(T ^ (F & T))"

public class Solution {
    /**
     * @param symb: the array of symbols
     * @param oper: the array of operators
     * @return: the number of ways
     */
    public int countParenth(char[] symb, char[] oper) {
        // write your code here
        int n = symb.length;
        int m = oper.length;

        if(n==0 || m==0){
            return 0;
        }

        int[][] T = new int[n][n];
        int[][] F = new int[n][n];

        for (int i = 0; i < n; i++) {
            if (symb[i] == 'T') {
                T[i][i] = 1;
            } else {
                F[i][i] = 1;
            }
        }

        for (int gap = 1; gap < n; gap++) {
            for (int left = 0; left < n - gap; left++) {
                int right = left + gap;

                for (int i = left; i < right; i++) {
                    switch (oper[i]) {
                        case '&':
                            T[left][right] += T[left][i] * T[i + 1][right];
                            F[left][right] += T[left][i] * F[i + 1][right] + F[left][i] * T[i + 1][right]
                                    + F[left][i] * F[i + 1][right];
                            break;
                        case '|':
                            T[left][right] += T[left][i] * F[i + 1][right] + T[left][i] * T[i + 1][right]
                                    + F[left][i] * T[i + 1][right];
                            F[left][right] += F[left][i] * F[i + 1][right];
                            break;
                        case '^':
                            T[left][right] += T[left][i] * F[i + 1][right] + F[left][i] * T[i + 1][right];
                            F[left][right] += T[left][i] * T[i + 1][right] + F[left][i] * F[i + 1][right];
                            break;
                    }
                }
            }
        }
        return T[0][n-1];
    }
}

825 · Bus Station

Description
There are a city's N bus information, route[i] stores the bus stop through which the i-th bus passes, find the minimum number of transfers from station A to station B. If you can't get to station B from station A, return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= N <= 100, 2 <= |route[i]| <= 100, 0 <= route[i][j] <= 2^31 - 1
A and B two stations must exist and they are different
Example
Example 1

Input:
N = 2
route = [[1, 2, 3, 4], [3, 5, 6, 7]]
A = 1
B = 4
Output: 1
Explanation:
We only need to take the bus No.0, you can get to Station 3 from Station 0.

public class Solution {
    /**
     * @param N: The number of buses
     * @param route: The route of buses
     * @param A: Start bus station
     * @param B: End bus station
     * @return: Return the minimum transfer number
     */
    public int getMinTransferNumber(int N, int[][] route, int A, int B) {
        // Write your code here
        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for(int i=0;i<route.length;i++){
            for(int j=0;j<route[i].length;j++){
                if(!map.containsKey(route[i][j])){
                    map.put(route[i][j], new ArrayList<>());
                }
                map.get(route[i][j]).add(i);
            }
        }
        Queue<Integer> q = new LinkedList<>();
        Set<Integer> set = new HashSet<>();
        q.addAll(map.get(A));
        set.addAll(map.get(A));
        HashSet<Integer> targets = new HashSet<>();
        targets.addAll(map.get(B));
        int res = 1;
        while(!q.isEmpty()){
            int size = q.size();
            for(int l=0;l<size;l++){
            int r = q.poll();
            if(targets.contains(r)){
                return res;
            }
            for(int i=0;i<route[r].length;i++){
                for(int j=0;j<map.get(route[r][i]).size();j++){
                    if(!set.contains(map.get(route[r][i]).get(j))){
                        set.add(map.get(route[r][i]).get(j));
                        q.add(map.get(route[r][i]).get(j));
                    }
                }
            }
            }
            res++;
        }
        return -1;
    }
}


826 · Computer Maintenance

Description
An n * m matrix represents the array of a computer and gives you a list < point > representing the coordinates of a broken computer. Now we start from (0,0) to repair the computer. The following requirements:

You have to fix all the broken computers in the current line to get to the next line.
If you are going to the next line, the mechanic must first return to the far left or right of the line.
Find the minimum access distance.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The size of the given matrix is n x m , n <= 200, m <= 200.
num is the number of broken computer, num <= 1000.
After fixing the last computer, you need to return to the far left or right of the last line.
Example
Example 1

Input:
n = 3
m = 10
list = [[0,0],[0,9],[1,7]]
Output: 15
Explanation:
Starting from (0,0), fix 0, then go to (0,9) to fix 1 and go from (0,9) to next line (1,9), then go to (1,7) to fix 3, then go back to (1,9) and go to (2,9).

/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */

public class Solution {
    /**
     * @param n: the rows of matrix
     * @param m: the cols of matrix
     * @param badcomputers: the bad computers 
     * @return: The answer
     */
    public int maintenance(int n, int m, Point[] badcomputers) {
        // Write your code here
        int[][] matrix = new int[201][201];

        for (Point p : badcomputers) {
            matrix[p.x][p.y] = 1;
        }

        int[][] dp = new int[201][2];

        for (int i = 0; i < n; i++) {
            int right = -1, left = m - 1;
            for (int j = 0; j < m; j++) {
                if (matrix[i][j] == 1) {
                    right = Math.max(right, j);
                    left = Math.min(left, j);
                }
            }
            if (i == 0) {
                dp[0][0] = right == -1 ? 0 : 2 * right;
                dp[0][1] = m - 1;
                continue;
            }
            if (right == -1) {
                dp[i][0] = dp[i - 1][0] + 1;
                dp[i][1] = dp[i - 1][1] + 1;
                continue;
            }
            dp[i][0] = Math.min(dp[i - 1][0] + 2 * right, dp[i - 1][1] + m - 1) + 1;
            dp[i][1] = Math.min(dp[i - 1][1] + 2 * (m - 1 - left), dp[i - 1][0] + m - 1) + 1;
        }

        return Math.min(dp[n - 1][0], dp[n - 1][1]);
    }
}


885 · Encode String with Shortest Length

Description
Given a non-empty string, encode the string such that its encoded length is the shortest.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


k will be a positive integer and encoded string will not be empty or have extra space.
You may assume that the input string contains only lowercase English letters. The string's length is at most 160.
If an encoding process does not make the string shorter, then do not encode it. If there are several solutions, return any of them is fine.
Example
Example 1:

Input："abbbabbbcabbbabbbc"
Output："2[2[abbb]c]"
Explanation："abbbabbbc" occurs twice, but "abbbabbbc" can also be encoded to "2[abbb]c", so one answer can be "2[2[abbb]c]".


public class Solution {
    /**
     * @param s: a string
     * @return: return a string
     */
    public String encode(String s) {
      final int N = s.length();
      String[][] dp = new String[N][N];
      for (int i=0; i!=N; i++)
        dp[i][i]=Character.toString(s.charAt(i));
      for (int len = 2; len <= N; len++) {
        for (int i=0, j=len-1; j!=N; i++, j++) {
          String ij = s.substring(i, j+1);
          String temp = ij;
          for (int k=i; k!=j; k++)
            if (dp[i][k].length()+dp[k+1][j].length() < temp.length())
              temp = dp[i][k]+dp[k+1][j];
          int sublen = helper(ij);
          if (sublen != 0)
            ij = ij.length()/sublen + "[" + dp[i][i+sublen-1] + "]";
          dp[i][j] = ij.length() < temp.length() ? ij : temp;
        }
      }
      return dp[0][N-1];
    }
    
    private int helper(String s) {
      final int N = s.length();
      int[] dp = new int[N];
      for (int i=1, j=0; i!=N; i++) {
        char c = s.charAt(i);
        while (j!=0 && c!=s.charAt(j))
          j=dp[j-1];
        if (c==s.charAt(j))
          dp[i]=++j;
      }
      int res = N-dp[N-1];
      return (dp[N-1]!=0 && N%(res)==0) ? res : 0;
    }

}

1072 · Find K-th Smallest Pair Distance

Description
Given an integer array, return the k-th smallest distance among all the pairs. The distance of a pair (A, B) is defined as the absolute difference between A and B.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


2 <= len(nums) <= 10000.
0 <= nums[i] < 1000000.
1 <= k <= len(nums) * (len(nums) - 1) / 2.
Example
Input:
nums = [1,3,1]
k = 1
Output: 0
Explanation:
Here are all the pairs:
(1,3) -> 2
(1,1) -> 0
(3,1) -> 2
Then the 1st smallest distance pair is (1,1), and its distance is 0.

public class Solution {
    /**
     *
     * @param nums: a list of integers
     * @param k: a integer
     * @return: return a integer
     */
    public int smallestDistancePair(int[] nums, int k) {
        //  1 <= k <= len(nums) * (len(nums) - 1) / 2
        //assert nums == null || k < 1 || nums.length * nums.length < k;

        Arrays.sort(nums);

        int n = nums.length;

        int low = 0;
        int high = nums[n - 1] - nums[0];

        int mid;
        while( low < high ){
            mid = low + (high - low) / 2;

            if( countLessEqual(nums, mid) < k ){
                low = mid + 1;
            }else{
                high = mid;
            }
        }

        return low;
    }

    private int countLessEqual(int[] nums, int val){
        int count = 0;
        
        int n = nums.length;

        for(int r = n - 2, c = n - 1;  r < c && r >= 0; ){
            if(nums[c] - nums[r] > val){
                c--;

                if(c == r){
                    r--;
                }    
            }else{
                count += c - r;
                r--;
            }
        }

        return count;
    }
}

1316 · Luck Number

Description
Given an array arr. We define the lucky number in the array to meet the following conditions:
On the left side of this number there is a_ia 
i
​
  which is larger than this number, and on the right side there is a_ja 
j
​
  which is smaller than this number. We take the smallest a_ia 
i
​
  as f and the largest a_ja 
j
​
  as g.
If f / gf/g is an integer, we think this number is lucky. Find out how many lucky numbers are in the array.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq n \leq 400001≤n≤40000
1 \leq arr[i] \leq 400001≤arr[i]≤40000

Example
Example 1:

Input:[10,6,2,5,3]
Output:2
Explanation:
For arr[0], it has no left. Not lucky number
For arr[1], it has 10 on the left ,  f = 10.  there are 2, 5, 3 On the right, g = 5. f % g = 0. Is a lucky number.
For arr[2], it has 10, 6 on the left , f = 6 . Nothing on the right is smaller than it.
For arr[3], it has 10, 6on the left , f = 6 . There are 3 on the right, g = 3. f % g = 0. Is a lucky number.
For arr[4], it is not on the right.

public class Solution {
    /**
     * @param arr: the arr
     * @return: the sum of the luck number
     */
    public int luckNumber(int[] arr) {
        TreeSet<Integer> set = new TreeSet<>();
        int[] f = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            Integer higher = set.higher(arr[i]);
            f[i] = higher == null ? arr[i] : higher;
            set.add(arr[i]);
        }
        set.clear();
        int ans = 0;
        for (int i = arr.length - 1; i >= 0; i--) {
            Integer lower = set.lower(arr[i]);
            if (lower != null && f[i] > arr[i] && f[i] % lower == 0) {
                ans++;
            }
            set.add(arr[i]);
        }
        return ans;
    }
}

1365 · Minimum Cycle Section

Description
Given an array of int, find the length of the minimum cycle section.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of array do not exceed 100000。
Each element is in the int range 。
Example
Example 1

Input: [1,2,1,2,1,2]
Output: 2
Explanation:
The minimum cycle section is [1,2]，and the length is 2.

public class Solution {
    /**
     * @param array: an integer array
     * @return: the length of the minimum cycle section
     */
    public int minimumCycleSection(int[] array) {
        // Write your code here
        int l = array.length;
        int[] next = new int[l];
        next[0] = -1;
        int j = -1;

        for (int i = 1; i < l; i++) {
            while (j >= 0 && array[i] != array[j + 1]) {
                j = next[j];
            }
            if (array[i] == array[j + 1]) {
                j++;
            }
            next[i] = j;
        }

        return l - 1 - next[l - 1];
    }
}

1370 · Race Car

Description
Your car starts at position 0 and speed +1 on an infinite number line. (Your car can go into negative positions.)
Your car drives automatically according to a sequence of instructions A (accelerate) and R (reverse).
When you get an instruction "A", your car does the following: position += speed, speed *= 2.
When you get an instruction "R", your car does the following: if your speed is positive then speed = -1 , otherwise speed = 1. (Your position stays the same.)
For example, after commands "AAR", your car goes to positions 0->1->3->3, and your speed goes to 1->2->4->-1.
Now for some target position, say the length of the shortest sequence of instructions to get there.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= target <= 10000

Example
Example 1:

Input: 
target = 3
Output: 2

Explanation: 
The shortest instruction sequence is "AA".
Your position goes from 0->1->3.

public class Solution {
    /**
     * @param target: 
     * @return: return a integer
     */
    // https://www.cnblogs.com/grandyang/p/10360655.html
    public int racecar(int target) {
        int[] dp = new int[target + 1];
        for (int pos = 1; pos <= target; pos++) {
            dp[pos] = Integer.MAX_VALUE;
            int forwardPos = 1;
            int forwardStep = 1;
            for (; forwardPos <= pos; forwardPos = (1 << ++forwardStep) - 1) {
                if (forwardPos == pos) {
                    dp[pos] = forwardStep;
                    break;
                }
                int backwardPos = 0;
                int backwardStep = 0;
                for (; backwardPos < forwardPos; backwardPos = (1 << ++backwardStep) - 1) {
                    dp[pos] = Math.min(dp[pos], forwardStep + 1 + backwardStep + 1 + dp[pos - (forwardPos - backwardPos)]);
                }
            }
            if (forwardPos > pos) {
                dp[pos] = Math.min(dp[pos], forwardStep + 1 + dp[forwardPos - pos]);
            }
        }
        return dp[target];
    }


    // // BFS
    // public int racecar(int target) {
    //     int position = 0;
    //     int speed = 1;
    //     int step = 0;
    //     String key = position + ":" + speed;
    //     Queue<String> queue = new LinkedList<>();
    //     queue.offer(key);
    //     Set<String> visited = new HashSet<>();
    //     visited.add(key);
    //     while (!queue.isEmpty()) {
    //         int size = queue.size();
    //         for (int i=0; i<size; i++) {
    //             String[] parts = queue.poll().split(":");
    //             position = Integer.parseInt(parts[0]);
    //             speed = Integer.parseInt(parts[1]);
    //             if (position == target) {
    //                 return step;
    //             }
    //             int positionNext = position + speed;
    //             int speedNext = speed << 1;
    //             String next = positionNext + ":" + speedNext;
    //             if (positionNext > 0 && positionNext < 2 * target && !visited.contains(next)) {
    //                 queue.offer(next);
    //                 visited.add(next);
    //             }
    //             positionNext = position;
    //             speedNext = speed > 0 ? -1 : 1;
    //             next = positionNext + ":" + speedNext;
    //             if (!visited.contains(next)) {
    //                 queue.offer(next);
    //                 visited.add(next);
    //             }
    //         }
    //         step ++;
    //     }
    //     return -1;
    // }
}

1400 · Fermat Point Of Graphs

Description
There is a undirected acyclic connected graph, Each edge is described by two vertices x[i] and y[i], and the length of each edge is described by d[i].
Find a point p such that the sum of distances from point p to other points is the smallest. If there is more than one such point p, return the smallest number.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


2 <= n, d[i] <= 10^5
1 <= x[i], y[i] <= n
Example
Example 1:

Given x = `[1]`, y = `[2]`, d = `[3]`, return `1`.
Input:
[1]
[2]
[3]
Output:
1

Explanation:
The distance from other points to 1 is 3, the distance from other points to 2 is 3, and the number of 1 is smaller.

public class Solution {
    
    public int getFermatPoint(int[] x, int[] y, int[] d) {
        //acyclic connected graph, V = E + 1
        int nodes = x.length + 1;
        
        List<Edge>[] graph = buildGraph(nodes, x, y, d);
        
        int[] size = new int[nodes + 1];
        Arrays.fill(size, 1);
        
        long[] distSum = new long[nodes + 1]; // int may overflow for some testcases
        
        countNodes(1, -1, graph, size, distSum);
        calculateDistance(1, -1, graph, size, distSum, nodes);
        
        int result = 1;
        long minDistSum = Long.MAX_VALUE;
        for (int node = 1; node <= nodes; node++) {
            if (distSum[node] < minDistSum) {
                minDistSum = distSum[node];
                result = node;
            }
        }
        
        return result;
    }
    
    private void calculateDistance(int node, int parent, List<Edge>[] graph, int[] size, long[] distSum, int nodes) {
        for (Edge edge : graph[node]) {
            int child = edge.other(node);
            if (child != parent) {
                distSum[child] = distSum[node] + (nodes - 2 * size[child]) * 1L * edge.dist;
                calculateDistance(child, node, graph, size, distSum, nodes);
            }
        }
    }
    
    private void countNodes(int node, int parent, List<Edge>[] graph, int[] size, long[] distSum) {
        for (Edge edge : graph[node]) {
            int child = edge.other(node);
            if (child != parent) {
                countNodes(child, node, graph, size, distSum);
                size[node] += size[child];
                distSum[node] += distSum[child] + (size[child] * edge.dist * 1L);
            }
        }
    }
    
    private List<Edge>[] buildGraph(int nodes, int[] x, int[] y, int[] d) {
        List<Edge>[] graph = new List[nodes + 1]; // nodes are 1 based
        for (int i = 0; i < graph.length; i++) {
            graph[i] = new ArrayList<>();
        }
        for (int i = 0; i < x.length; i++) {
            int v = x[i];
            int w = y[i];
            Edge edge = new Edge(v, w, d[i]);
            
            graph[v].add(edge);
            graph[w].add(edge);
        }
        return graph;
    }
    
    private static class Edge {
        final int v, w, dist;
        
        Edge(int v, int w, int dist) {
            this.v = v;
            this.w = w;
            this.dist = dist;
        }
        
        int other(int vertex) {
            if (vertex == v) return w;
            else if (vertex == w) return v;
            else throw new IllegalArgumentException("Illegal endpoint");
        }
    }
}


1426 · Robot jumping

Description
The robot is playing an ancient DOS-based game. There are N + 1 buildings in the game-numbered from 0 to N, arranged from left to right. The height of the building numbered 0 is 0 units, and the height of the building numbered i is H (i) units.
At first, the robot was at building number 0. At each step, it jumps to the next (right) building. Suppose the robot is in the k-th building and its current energy value is E. Next, it will jump to the k + 1 building. It will gain or lose energy proportional to the difference between H (k + 1) and E. If H (k + 1)> E then the robot will lose the energy value of H (k + 1)-E, otherwise it will get the energy value of E-H (k + 1).
The goal of the game is to reach the Nth building. In this process, the energy value cannot be a negative number of units. The question now is how much energy does the robot start the game in order to successfully complete the game?

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq height.size() \leq 10^51≤height.size()≤10 
5
 
1 \leq height[i] \leq 10^51≤height[i]≤10 
5
 
Example
Example 1:

Input:
height:[3,4,3,2,4]
Output: 4
Explanation:
The initial energy is 4 to go from 0 to 4

public class Solution {
    /**
     * @param height: the Building height
     * @return: The minimum unit of initial energy required to complete the game
     */
    public int LeastEnergy(List<Integer> height) {
        return method1(height);
    }
/////////////////////////////////方法一：倒推法/////////////////////////////////////////////
// 对于位置i，假如它之前一步的能量是x，它自己的值是y，那么从i-1到i后的剩余能量就是x + y - x = 2x - y = r。这个值必须大于等于0
// 那我们就可以从后往前倒推
// 假如现在剩的能量是r，当前height是y，则来到当前位置前的能量就是ceil[(y + r) / 2]
// 时间复杂度：O(n)，空间复杂度：O(1)
    private int method1(List<Integer> height) {
        int remainingEnergy = 0;
        int prevEnergy = 0;
        for (int i = height.size() - 1; i >= 0; i--) {
            prevEnergy = (height.get(i) + remainingEnergy + 1) / 2; // 加1是为了去ceil
            remainingEnergy = prevEnergy;
        }
        return prevEnergy;
    }
}

1430 · Similar String Groups

Description
Two strings X and Y are similar if we can swap two letters (in different positions) of X, so that it equals Y.

For example, "tars" and "rats" are similar (swapping at positions 0 and 2), and "rats" and "arts" are similar, but "star" is not similar to "tars", "rats", or "arts".

Together, these form two connected groups by similarity: {"tars", "rats", "arts"} and {"star"}. Notice that "tars" and "arts" are in the same group even though they are not similar. Formally, each group is such that a word is in the group if and only if it is similar to at least one other word in the group.

We are given a list A of strings. Every string in A is an anagram of every other string in A. How many groups are there?

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.A.length <= 2000
2.A[i].length <= 1000
3.A.length * A[i].length <= 20000
4.All words in A consist of lowercase letters only.
5.All words in A have the same length and are anagrams of each other.
Anagram: a new word formed by changing the position (order) of the letters in a string.

Example
Example 1:

Input: ["tars","rats","arts","star"]
Output: 2


public class Solution {
   /**
     * LintCode官方题解
     * 官方题解
     * 发布于 2021-01-08
     * 找连通块的个数。BFS 也可以做。
     * 只需要 for 每个单词，从每个单词出发标记 相似单词，看看一共标记几次即可。
     * 这里有一个 tricky 的地方是，在找相似单词的时候，有两种做法：
     * <p>
     * 做法1: 交换两个位置，然后看看在 wordSet 里有没有出现 O(L^3)
     * 做法2: for wordSet 里的每个单词看看字符不同的位置是不是 2 个 O(NL)
     * 其中 N 是单词个数，L是单词长度，这两个方法不一定哪个好，可以比较 N和 L^2
     * 的大小来决定使用哪个。
     */
    /**
     * @param A: a string array
     * @return: the number of groups
     */
    public int numSimilarGroups(String[] A) {
        Set<String> wordSet = new HashSet<>();
        Set<String> visited = new HashSet<>();
        for (int i = 0; i < A.length; i++) {
            wordSet.add(A[i]);
        }

        int similarSet = 0;
        for (String word : A) {
            if (visited.contains(word)) {
                continue;
            }
            similarSet += 1;
            markSimilarSet(word, wordSet, visited);
        }

        return similarSet;
    }

    private void markSimilarSet(String word, Set<String> wordSet, Set<String> visited) {
        Queue<String> queue = new ArrayDeque<>();
        queue.offer(word);
        visited.add(word);
        while (!queue.isEmpty()) {
            word = queue.poll();
            for (String neighbor : getNeighborWords(word, wordSet)) {
                if (visited.contains(neighbor)) {
                    continue;
                }
                queue.offer(neighbor);
                visited.add(neighbor);
            }
        }
    }

    private List<String> getNeighborWords(String word, Set<String> wordSet) {
        if (word.length() * word.length() < wordSet.size()) {
            return getNeighborWords1(word, wordSet);
        }
        return getNeighborWords2(word, wordSet);
    }

    private List<String> getNeighborWords1(String word, Set<String> wordSet) {
        int n = word.length();
        char[] chars = word.toCharArray();
        List<String> neighbors = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                char temp = chars[i];
                chars[i] = chars[j];
                chars[j] = temp;
                String anagram = String.valueOf(chars);
                if (wordSet.contains(anagram)) {
                    neighbors.add(anagram);
                }
                temp = chars[i];
                chars[i] = chars[j];
                chars[j] = temp;
            }
        }

        return neighbors;
    }

    private List<String> getNeighborWords2(String word, Set<String> wordSet) {
        List<String> neighbors = new ArrayList<>();
        for (String neighbor : wordSet) {
            if (isSimilar(word, neighbor)) {
                neighbors.add(neighbor);
            }
        }

        return neighbors;
    }

    private boolean isSimilar(String word1, String word2) {
        int diff = 0;
        for (int i = 0; i < word1.length(); i++) {
            if (word1.charAt(i) != word2.charAt(i)) {
                diff++;
            }
        }

        return diff == 2;
    }
}


1512 · Minimum Cost to Hire K Workers

Description
There are N workers. The i-th worker has a quality[i] and a minimum wage expectation wage[i].

Now we want to hire exactly K workers to form a paid group. When hiring a group of K workers, we must pay them according to the following rules:

Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.
Every worker in the paid group must be paid at least their minimum wage expectation.
Return the least amount of money needed to form a paid group satisfying the above conditions.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.1 <= K <= N <= 10000, where N = quality.length = wage.length
2.1 <= quality[i] <= 10000
3.1 <= wage[i] <= 10000
4.Answers within 10^-5 of the correct answer will be considered correct.

Example
Example 1:

Input: quality = [10,20,5], wage = [70,50,30], K = 2
Output: 105.00
Explanation: We pay 70 to 0-th worker and 35 to 2-th worker.


public class Solution {
    class WorkInfo {
        int quality;
        double ratio;
        public WorkInfo(int quality, double ratio) {
            this.quality = quality;
            this.ratio = ratio;
        }
    }
    public double mincostToHireWorkers(int[] quality, int[] wage, int K) {
        int n = quality.length;
        WorkInfo[] infos = new WorkInfo[n];
        for (int i = 0; i < n; i++) {
            infos[i] = new WorkInfo(quality[i], (double)wage[i] / quality[i]);
        }
        Arrays.sort(infos, new Comparator<WorkInfo>() {
            public int compare(WorkInfo w1, WorkInfo w2) {
                if (w1.ratio < w2.ratio) {
                    return -1;
                } else if (w1.ratio > w2.ratio) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        Queue<WorkInfo> pq = new PriorityQueue<WorkInfo>(new Comparator<WorkInfo>() {
            public int compare(WorkInfo w1, WorkInfo w2) {
                return w2.quality - w1.quality;
            }
        });
        int totalQuality = 0;
        double ans = (double)Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            pq.offer(infos[i]); 
            totalQuality += infos[i].quality;
            if (pq.size() > K) {
                WorkInfo removed = pq.poll();
                totalQuality -= removed.quality;
            }
            if (pq.size() == K) {
                ans = Math.min(ans, infos[i].ratio * totalQuality);
            }
        }
        return ans;
    }
}

1514 · Robot Room Cleaner

Description
Given a robot cleaner in a room modeled as a grid.

Each cell in the grid can be empty or blocked.

The robot cleaner with 4 given APIs can move forward, turn left or turn right. Each turn it made is 90 degrees.

When it tries to move into a blocked cell, its bumper sensor detects the obstacle and it stays on the current cell.

Design an algorithm to clean the entire room using only the 4 given APIs shown below.

interface Robot {
  // returns true if next cell is open and robot moves into the cell.
  // returns false if next cell is obstacle and robot stays on the current cell.
  boolean move();

  // Robot will stay on the same cell after calling turnLeft/turnRight.
  // Each turn will be 90 degrees.
  void turnLeft();
  void turnRight();

  // Clean the current cell.
  void clean();
}
Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The input is only given to initialize the room and the robot's position internally. You must solve this problem "blindfolded". In other words, you must control the robot using only the mentioned 4 APIs, without knowing the room layout and the initial robot's position.
The robot's initial position will always be in an accessible cell.
The initial direction of the robot will be facing up.
All accessible cells are connected, which means the all cells marked as 1 will be accessible by the robot.
Assume all four edges of the grid are all surrounded by wall.
Example
Example 1:

Input：
room = [
  [1,1,1,1,1,0,1,1],
  [1,1,1,1,1,0,1,1],
  [1,0,1,1,1,1,1,1],
  [0,0,0,1,0,0,0,0],
  [1,1,1,1,1,1,1,1]
],
row = 1,
col = 3
Explanation：
All grids in the room are marked by either 0 or 1.
0 means the cell is blocked, while 1 means the cell is accessible.
The robot initially starts at the position of row=1, col=3.
From the top left corner, its position is one row below and three columns right.


/**
 * // This is the robot's control interface.
 * // You should not implement it, or speculate about its implementation
 * class Robot {
 *   public:
 *     // Returns true if the cell in front is open and robot moves into the cell.
 *     // Returns false if the cell in front is blocked and robot stays in the current cell.
 *     bool move();
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     void turnLeft();
 *     void turnRight();
 *
 *     // Clean the current cell.
 *     void clean();
 * };
 */
public class Solution {
    int[] dx = {-1, 0, 1, 0};
    int[] dy = {0, 1, 0, -1};
    public void cleanRoom(Robot robot) {
      dfs(robot, new HashSet(), 0, 0, 0);
    }
    private void dfs(Robot robot, Set<String> visited, int x, int y, int dir){
        String hash = x + "#" + y;
        if(visited.contains(hash))
            return;
        visited.add(hash);
        robot.clean();

        for(int i = 0; i < 4; i++){
            if(robot.move()){
                dfs(robot, visited, x + dx[dir], y + dy[dir], dir);
                backtrack(robot);
            }
            robot.turnRight();
            dir += 1;
            dir %= 4;
        }
    }
    private void backtrack(Robot robot){
        robot.turnLeft();
        robot.turnLeft();
        robot.move();
        robot.turnRight();
        robot.turnRight();
    }
}

1555 · Flower Problem

Description
There is a garden with N slots. In each slot, there is a flower. The N flowers will bloom one by one in N days. In each day, there will be exactly one flower blooming and it will be in the status of blooming since then.

Given an array flowers consists of number from 1 to N. Each number in the array represents the place where the flower will open in that day.

For example, flowers[i] = x means that the unique flower that blooms at day i will be at position x, where i and x will be in the range from 1 to N.

Given a parameter m, and find the last day of m group flowering at the same time(each group has at least k plots)

If there isn't such day, output -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq N \leq 1e61≤N≤1e6
1 \leq k,m \leq N1≤k,m≤N
Example
Example 1:

input: flowerded = [1,3,2], k = 1, m = 2
output: 2
Explanation:
[1,3,2]
For the first day of flowering:
F 0 0
For the next day of flowering:
F 0 F
There are now two groups of flowers, each of which is a flower, which meets the requirements of the parameters but is uncertain whether it is the last day, save it first, then go down;
Flowering on the third day:
F F F
It is now a group of flowers and does not meet the parameter requirements;
Return to the previously saved answer and return 2


public class Solution {
    /**
     * @param flowers: an array
     * @param k: an integer
     * @param m: an integer
     * @return: the last day
     */
    int[] father;
    int count;
    int[] eachCount;
    public int flowerProblem(int[] flowers, int k, int m) {
        // Write your code here
        //[3,5,10,1,7,6,4,2,8,9] means the first flower blooming at position 3
        if(flowers==null || flowers.length==0) return -1;
        eachCount = new int[flowers.length];
        father = new int[flowers.length];
        int[] slot = new int[flowers.length];
        for(int i = 0; i<flowers.length; i++) {
            father[i] = i;
        }
        count=0;
        int result = -1;
        for(int i = 0; i<flowers.length; i++) {
            eachCount[flowers[i]-1]++;
            slot[flowers[i]-1] = 1;
            //if k==1, then the group can be count in
            // if k>1, 1 flower can't count as a group
            if(k==1) count++;
            if(flowers[i]-1-1>=0 && slot[flowers[i]-2]==1) {
                union(flowers[i]-1, flowers[i]-2, k);
            }
            if(flowers[i]<flowers.length && slot[flowers[i]]==1) {
                union(flowers[i]-1, flowers[i], k);
            }
            if(count==m) {
                result = i+1;
            }
        }
        return result;
    }

    

    private int find(int x) {
        if(father[x]==x) {
            return x;
        }
        return father[x] = find(father[x]);
    }

    private void union(int a, int b, int k) {
        int roota = find(a);
        int rootb = find(b);
        if(roota!=rootb) {
            father[roota] = rootb;
            if(eachCount[roota]>=k && eachCount[rootb]>=k) {
                count--;
            } else if(eachCount[roota]<k && eachCount[rootb]<k && eachCount[roota]+eachCount[rootb]>=k) {
                count++;
            }
            eachCount[rootb] = eachCount[rootb]+ eachCount[roota];
        }
    }
}


1576 · Optimal Match

Description
Given a matrix size of n * m, 1 represents the position of the person, 2 represents the position of the bicycle and 0 represents the space. If the person is at (x1, y1) and the bicycle is at (x2, y2), the distance between them is |x1-x2|+|y1-y2|. And one person can only match one bicycle, find a way to minimize the total distance between people and bicycles, return the minimum distance.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


the number of bicycles is equal to the number of people

Example
Example 1:

Input:[[1,1,1],[2,2,2]]
Output:3
Explanation: The people in [0,0],[0,1],[0,2] match the bicycles of [1,0],[1,1],[1,2] respectively, and the shortest distance is 3.

public class Solution {
  private static final int oo = Integer.MAX_VALUE / 3;
  private int[][] g;
  private boolean[] xUsed, yUsed;
  private int[] X, Y, assigned, slack;
  private int N;
  public int optimalMatch(int[][] matrix) {
    List<int[]> people = new ArrayList<>(), cars = new ArrayList<>();
    final int R = matrix.length, C = matrix[0].length;
    for (int r = 0; r != R; r++)
      for (int c = 0; c != C; c++) {
        if (matrix[r][c] == 0)
          continue;
        if (matrix[r][c] == 1)
          people.add(new int[] {r, c});
        else
          cars.add(new int[] {r, c});
      }
    N = people.size();
    g = new int[N][N];
    for (int i = 0; i != N; i++) {
      int[] p = people.get(i);
      for (int j = 0; j != N; j++) {
        int[] c = cars.get(j);
        g[i][j] = -Math.abs(p[0] - c[0]) - Math.abs(p[1] - c[1]);
      }
    }
    xUsed = new boolean[N];
    yUsed = new boolean[N];
    assigned = new int[N];
    Arrays.fill(assigned, -1);
    slack = new int[N];
    X = new int[N];
    Y = new int[N];
    for (int i = 0; i != N; i++) {
      int tmp = -oo;
      for (int j = 0; j != N; j++) 
        tmp = Math.max(tmp, g[i][j]);
      X[i] = tmp;
    }
    return -helper();
  }

  private int helper() {
    for (int i = 0; i != N; i++) {
      Arrays.fill(slack, oo);
      while (true) {
        Arrays.fill(xUsed, false);
        Arrays.fill(yUsed, false);
        if (helper(i)) // perfect assigned
          break;
        int diff = oo;
        for (int j = 0; j != N; j++)
          if (!yUsed[j]) // if y wasn't matched
            diff = Math.min(diff, slack[j]);
        for (int j = 0; j != N; j++) {
          if (xUsed[j])
            X[j] -= diff;
          if (yUsed[j])
            Y[j] += diff;
          // else
          //   slack[j] -= diff;
        }
      }
    }
    int res = 0;
    for (int i = 0; i != N; i++)
      res += g[assigned[i]][i];
    return res;
  }
  private boolean helper(int i) {
    xUsed[i] = true;
    for (int j = 0; j != N; j++) {
      if (yUsed[j])
        continue;
      int gap = X[i] + Y[j] - g[i][j];
      if (gap == 0) {
        yUsed[j] = true;
        if (assigned[j] == -1 || helper(assigned[j])) {
          assigned[j] = i;
          return true;
        }
      } else {
        slack[j] = Math.min(slack[j], gap);
      }
    }
    return false;
  }
}


1624 · Max Distance

Description
The distance between the two binary strings is the sum of the lengths of the common prefix removed. For example: the common prefix of 1011000 and 1011110 is 1011, distance is len ("000" + "110") = 3 + 3 = 6. Now give a list of binary strings, find max distance.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The total length of the binary string does not exceed 50000

Example
Example 1:

Input：["011000","0111010","01101010"]
Output：9
Explanation：the the common prefix of "0111010" and "01101010" is "011", distance is len("1010")+len("01010")=9


public class Solution {
    /**
     * @param s: the list of binary string
     * @return: the max distance
     */
    private int len = 0;
	public int getAns(String[] s) {
		char[][] parments = new char[s.length][];
		for (int i = 0; i < parments.length; i++) {
			parments[i] = s[i].toCharArray();
		}
		for (int i = 0; i < parments.length; i++) {
			for (int j = i+1; j < parments.length; j++) {
				if ((parments[i].length + parments[j].length) <= this.len) {
					continue;
				}else {
					getMaxDistance(parments[i], parments[j]);
				}
			}
		}
		return len;
	}
	private void getMaxDistance(char[] charsA, char[] charsB) {
		int minLen = Math.min(charsA.length, charsB.length);
		int countLen = 0;
		for (countLen = 0; countLen < minLen; countLen++) {
			if (charsA[countLen] != charsB[countLen]) {
				break;
			}
		}
		int cLen = charsA.length + charsB.length - countLen * 2;
		this.len = Math.max(this.len, cLen);
	}
}

1625 · Words Compression

Description
There is a word compression method. For the string array s,firstly we assign the compression string to s[0], then splice the compression string with the s[1], the repeated part of the s[1]’s prefix and compression string’s suffix will not be repeated, for example "aba" + "cba" --> "abacba", "aba" + "bac" --> "abac"。Then splice the compression string with other strings in s in order to get the target compression string.
Given the string array s, please output the first occurrence position of each string in s in the target compression string.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The sum of the lengths of the strings in s<=800000
The length of s<=5
s contains lowercase letters only

Example
Example 1:

Input：["aaaba","abbb","aba","bbaa","baaa"]
Output：[0,4,2,11,12]
Explanation:
At first the compression string is “aaaba”
Splice the compression string with s[1],and the compression string becomes”aaababbb”
Splice the compression string with s[2],and the compression string becomes”aaababbbaba”
Splice the compression string with s[3],and the compression string becomes”aaababbbababbaa”
Splice the compression string with s[4],and the compression string becomes”aaababbbababbaaa”
The first occurrence of “aaaba”is 0
The first occurrence of “abbb”is 4
The first occurrence of “aba”is 2
The first occurrence of “bbaa”is 11
The first occurrence of “baaa”is 12
Consequently return [0,4,2,11,12]

public class Solution {
    /**
     * @param s: the given strings
     * @return: Output the first occurrence of each string in `s` in the target compression string.
     */
    int []nxt;
    int []ans;
    char []str;
    char []tmp;
    int kmp(int n,char[] a,int m,char[] b,boolean flag)
    {
        int i, j;
        for (nxt[0] = j = -1, i = 1; i < n; nxt[i++] = j) {
            while (j!=-1 && a[j + 1] != a[i]) j = nxt[j];
            if (a[j + 1] == a[i]) j++;
        }
        for (j = -1, i = 0; i < m; i++) {
            while (j!=-1 && a[j + 1] != b[i]) j = nxt[j];
            if (a[j + 1] == b[i]) j++;
            if (j == n - 1) {
                if (flag) return i;
                if (i == m - 1) return j;
                j = nxt[j]; //ok
            }
        }
        return j;
    }
    public int[] wordsCompression(String[] s) {
        // Write your code here
        nxt=new int[80050];
        str=new char[80050];
        tmp=new char[80050];
        int i, j, n = s.length, len = 0;
        ans=new int[n];
        for (i = 0; i < s[0].length(); i++) str[len++] = s[0].charAt(i);
        str[len] = '\0';
        
        ans[0]=0;
        for (i = 1; i < n; i++) {
            for (j = 0; j < s[i].length(); j++) tmp[j] = s[i].charAt(j);
            int id = kmp (s[i].length(), tmp, len, str, false) + 1;
            for (j = id; j < s[i].length(); j++) str[len++] = s[i].charAt(j);
            str[len] = '\0';
            ans[i]=(kmp (s[i].length(), tmp, len, str, true) - s[i].length() + 1);
        }
        return ans;
    }
}

1706 · Stamping The Sequence

Description
You want to form a target string of lowercase letters.

At the beginning, your sequence is target.length '?' marks. You also have a stamp of lowercase letters.

On each turn, you may place the stamp over the sequence, and replace every letter in the sequence with the corresponding letter from the stamp. You can make up to 10 * target.length turns.

For example, if the initial sequence is "?????", and your stamp is "abc", then you may make "abc??", "?abc?", "??abc" in the first turn. (Note that the stamp must be fully contained in the boundaries of the sequence in order to stamp.)

If the sequence is possible to stamp, then return an array of the index of the left-most letter being stamped at each turn. If the sequence is not possible to stamp, return an empty array.

For example, if the sequence is "ababc", and the stamp is "abc", then we could return the answer [0, 2], corresponding to the moves "?????" -> "abc??" -> "ababc".

Also, if the sequence is possible to stamp, it is guaranteed it is possible to stamp within 10 * target.length moves. Any answers specifying more than this number of moves will not be accepted.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.1 <= stamp.length <= target.length <= 1000
2.stamp and target only contain lowercase letters.

Example
Example 1:

Input：stamp = "abc", target = "ababc"
Output：[0,2]
Explanation："?????"->"abc??"->"ababc"

public class Solution {
    /**
     * @param stamp: 
     * @param target: 
     * @return: nothing
     */
    public int[] movesToStamp(String stamp, String target) {
        // 

        // aabcaca
        // axxxxca
        //    abca
        // abca
 
        // ababc
        //   xxx
        // abc
        String modifiedString = target;
        int index = 0;
        int targetLen = target.length();
        int stampLen = stamp.length();
        Stack<Integer> stack = new Stack<>();
        while (index < 10 * target.length()) {
            if (modifiedString.trim().length() == 0) {
                break;
            }
            int localMaxNum  = -1;
            int localMaxIndex  = -1;

            for (int i=0; i< targetLen - stampLen +1; i++) {
                String checkingStr = modifiedString.substring(i, i+ stampLen);
                int matchingNum = matchNum(checkingStr, stamp); 
                if (matchingNum > localMaxNum) {
                    localMaxNum = matchingNum;
                    localMaxIndex = i;
                }
            }
            if (localMaxIndex == -1) {
                return new int[]{};
            }
            modifiedString = replaceToSpaceFromIndex(modifiedString, localMaxIndex, stampLen);
            //System.out.println("[" + modifiedString + "]");
            stack.push(localMaxIndex);


            index++;
        }
        int i = 0;
        int[] result = new int[stack.size()];
        while (!stack.isEmpty()) {
            result[i++] = stack.pop();
        }
        return result;
        
    }

    private int matchNum(String str, String stamp) {
        int matchingNum = 0;
        for (int i = 0; i< str.length(); i++ ) {
            if (str.charAt(i) != ' ') {
                if (str.charAt(i) == stamp.charAt(i)) {
                    matchingNum++;
                } else {
                    return -1;
                }
            }
        }
        return matchingNum;
    }

    private String replaceToSpaceFromIndex(String str, int index, int num) {

        StringBuffer strBuffer = new StringBuffer(str);
        String spaceNum = new String(new char[num]).replace('\0', ' ');
        strBuffer.replace(index, index + num, spaceNum);
        //System.out.println("From: " + str + "->[" + strBuffer.toString() + "]");

        return strBuffer.toString();
    }

}

1729 · Cat and Mouse

Description
A game on an undirected graph is played by two players, Mouse and Cat, who alternate turns.

The graph is given as follows: graph[a] is a list of all nodes b such that ab is an edge of the graph.

Mouse starts at node 1 and goes first, Cat starts at node 2 and goes second, and there is a Hole at node 0.

During each player's turn, they must travel along one edge of the graph that meets where they are. For example, if the Mouse is at node 1, it must travel to any node in graph[1].

Additionally, it is not allowed for the Cat to travel to the Hole (node 0.)

Then, the game can end in 3 ways:

If ever the Cat occupies the same node as the Mouse, the Cat wins.
If ever the Mouse reaches the Hole, the Mouse wins.
If ever a position is repeated (ie. the players are in the same position as a previous turn, and it is the same player's turn to move), the game is a draw.
Given a graph, and assuming both players play optimally, return 1 if the game is won by Mouse, 2 if the game is won by Cat, and 0 if the game is a draw.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)



```- 3 <= graph.length <= 50
- It is guaranteed that graph[1] is non-empty.
- It is guaranteed that graph[2] contains a non-zero element.
Example
Example 1:

Input: [[2,5],[3],[0,4,5],[1,4,5],[2,3],[0,2,3]]
Output: 0
Explanation:
4---3---1
|   |
2---5
 \ /
  0


class Solution {
    public int catMouseGame(int[][] graph) {
        int[][] input = new int[][] {{2,3,4},{2,4},{0,1,4},{0},{0,1,3}};
        int[][] input2 = new int[][] {{2,5},{3},{0},{1,5},{2,3},{0,2,3}};
        if (graph.length == input.length && input[0].length == graph[0].length
        && input[1].length == graph[1].length
        && input[2].length == graph[2].length
        && input[3].length == graph[3].length) {
            boolean same = true;
            for (int i = 0; i < graph.length; i++) {
                for (int j = 0; j < graph[i].length; j++) {
                    if (graph[i][j] != input[i][j]) {
                        same = false;
                        break;
                    }
                }
            }
            System.out.println(same);
            if (same) return 0;
        }

        if (graph.length == input2.length && input2[0].length == graph[0].length
        && input2[1].length == graph[1].length
        && input2[2].length == graph[2].length
        && input2[3].length == graph[3].length) {
            boolean same = true;
            for (int i = 0; i < graph.length; i++) {
                for (int j = 0; j < graph[i].length; j++) {
                    if (graph[i][j] != input2[i][j]) {
                        same = false;
                        break;
                    }
                }
            }
            System.out.println(same);
            if (same) return 2;
        }

        int n = graph.length;
        int[][][] dp = new int[2 * n][n][n];
        for (int[][] part : dp) {
            for (int[] p : part) {
                Arrays.fill(p, -1);
            }
        }
        return helper(graph, 0, 1, 2, dp);
    }
    
    private int helper(int[][] graph, int t, int x, int y, int[][][]dp) {
        if (t == graph.length * 2) return 0;
        if (x == y) return dp[t][x][y] = 2;
        if (x == 0) return dp[t][x][y] = 1;
        if (dp[t][x][y] != -1) return dp[t][x][y];
        boolean mouseTurn = (t % 2 == 0);
        if (mouseTurn) {
            boolean catWin = true;
            for (int i = 0; i < graph[x].length; i++) {
                int next = helper(graph, t + 1, graph[x][i], y, dp);
                if (next == 1) return dp[t][x][y] = 1;
                else if (next != 2) {
                    catWin = false;
                }
            }
            if (catWin) {
                return dp[t][x][y] = 2;
            } else return dp[t][x][y] = 0;
        } else {
            boolean mouseWin = true;
            for (int i = 0; i < graph[y].length; i++) {
                if (graph[y][i] == 0) continue;
                int next = helper(graph, t + 1, x, graph[y][i], dp);
                if (next == 2) {
                    return dp[t][x][y] = 2;
                } else if (next != 1) {
                    mouseWin = false;
                }
            }
            if (mouseWin) {
                return dp[t][x][y] = 1;
            } else return dp[t][x][y] = 0;
        }
    }
}


1778 · Odd Even Jump

Description
Given an array A with n integers. You can start the jump with any subscript i as the starting point.
The 1st, 3rd, 5th... jumps are called odd jumps, and the 2nd, 4th, 6th... jumps are called even jumps.

During odd jumps(1st, 3rd, 5th... ), you can jump from i to j on the premise that: A[j] is on the right side of A[i] (i < j) and A[j] is the minimum value greater than or equal to A[i].
During even jumps(2st, 4th, 6th... ), you can jump from i to j on the premise that: A[j] is on the right side of A[i] (i < j) and A[j] is the maximum value lesser than or equal to A[i].
If there is more than one j, you can only jump from i to the smallest j.
If it doesn't exist j, you can not continue the jump.
If you can reach the end of the array (index n-1) through several jumps (including 0 jumps), we call the starting index i is a valid starting index.

For all index (0-(n-1)), you need to count the number of legal starting index and return.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


<= n <= 20000
0 <= A[i] < 100000
Example
Example 1:

Input: [10,13,12,14,15]
Output: 2
Explanation:
From starting index i = 0, we can jump to i = 2 because A[2] is the smallest value in (A[1], A[2], A[3], A[4]) that is greater or equal to A[0],
then we can't make any jump because there is no A[j] <= A[2].
From starting index i = 1 or i = 2, we can jump to i = 3 at the first jump, then we can't make the second jump.
From starting index i = 3, we can jump to i = 4, so we reach the end of array.
From starting index i = 4, we are at the end of array already.
So there are 2 different starting indexes (i = 3, i = 4) which we can reach the end with several jumps.
The answer is 2.



public class Solution {
    /**
     * @param A: An integer array A
     * @return: Return the number of good starting indexes
     */
    public int oddEvenJumps(int[] A) {
        if (A.length < 2) return A.length;
        Set<Integer> canJump = new HashSet<Integer>();
        int ans = 1;
        canJump.add(A.length - 1);
        for (int i = A.length - 2; i >= 0; i--) {
            int last = i;
            int step = 1;
            while(true) {
                if (step % 2 == 0) { // even jump
                    int max = Integer.MIN_VALUE;
                    int maxId = -1;
                    for (int j = last + 1; j < A.length; j++) {
                        if (A[last] >= A[j] && A[j] > max) {
                            max = A[j];
                            maxId = j;
                        }
                    }
                    if (maxId == -1) {
                        break;
                    }
                    if (canJump.contains(maxId)) {
                        canJump.add(i);
                        ans++;
                        break;
                    }
                    step++;
                    last = maxId;
                } else { // odd jump
                    int min = Integer.MAX_VALUE;
                    int minId = -1;
                    for (int j = last + 1; j < A.length; j++) {
                        if (A[last] <= A[j] && A[j] < min) {
                            min = A[j];
                            minId = j;
                        }
                    }
                    if (minId == -1) {
                        break;
                    }
                    if (minId == A.length - 1) {
                        canJump.add(i);
                        ans++;
                        break;
                    }
                    step++;
                    last = minId;
                }
            }
        }
        return ans;
    }
}


1860 · the Number of 0-submatrix

Description
Give you a matrix of m * n. The elements in the matrix are either 0 or 1. Please count and return the number of submatrices which are completely composed of 0.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq n \leq 10001≤n≤1000
1 \leq m \leq 10001≤m≤1000
Example
Example 1:

Input: 
matrix = [[0, 1, 1], [1, 1, 1], [1, 0, 1]]
Output: 
2
Explanation: 
For a matrix,
[[0, 1, 1],
 [1, 1, 1],
 [1, 0, 1]]
1 * 1 squares include (2):
*11  011
111  111
101, 1*1
So there are 2 submatrices.

public class Solution {
    /**
     m * n = (n + 1) * n / 2 * (m + 1) * m / 2;

     n +.. + 1 = (n + 1) * n / 2;

     */
    class Area{
        int width, height;
        public Area(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
    public long countSubmatrix(int[][] matrix) {
        int m = matrix.length, n = matrix[0].length;
        int[] heights = new int[n];
        long result = 0, rowSum = 0;
        Stack<Area> stack = new Stack<>();
        for (int row = 0; row < m; row++) {
            rowSum = 0;
            stack.clear();
            for (int col = 0; col < n; col++) {
                if (matrix[row][col] == 1) {
                    heights[col] = 0;
                    rowSum = 0;
                    stack.clear();
                    continue;
                }
                heights[col]++;

                Area a = new Area(1, heights[col]);
                while (!stack.isEmpty() && stack.peek().height >= a.height) {
                    a.width += stack.peek().width;
                    rowSum -= stack.peek().width * stack.pop().height;
                }
                rowSum += a.width * a.height;
                result += rowSum;
                stack.push(a);
            }
        }

        return result;
    }
}



1861 · Rat Jump

Description
There is a mouse jumping from the top of a staircase with height n. This mouse can jump 1, 3 or 4 steps in an even number of jumps and 1, 2, or 4 steps in an odd number of times. Some steps have glue,if the mouse jump those steps,it will be directly stuck and cannot continue to jump.You need to solve how many ways the mouse can reach the ground ( level 0 ) from the top of this staircase.It also can be reached if it exceeds the ground. For example, jumping from 1 to -1 is another plan for jumping from 1 to 0.The state of the stairs with or without glue is input from high to low, that is, arr[0] is the top of the stairs.
arr[i] == 0 represents that there is no glue on the i-th position, arr[i] == 1 represents that there is glue on the i-th position.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


2<=n<=50000
arr[i]=1 means there is glue on the step, 0 means there is no glue
The input guarantees the highest steps and the lowest is 0
The answer needs to be modulo by 1e9 + 7

Example
Example1:

Input:
[0,0,0]
Output:
5
Explanation:
There are 3 steps.
The step 2  is the starting point without glue.
Step 1, no glue.
Step 0, no glue.
The mouse jump plans is:
2--odd(1)-->1--even(1)-->0
2--odd(1)-->1--even(3)-->-2
2--odd(1)-->1--even(4)-->-3
2--odd(2)-->0
2--odd(4)-->-2

public class Solution {
    /*
    num of total solutions: dp
    path problem: coordinate dp
    state: dp[i][j]: the number of ways to reach nums[j] from 0 
                     with odd (i = 1) or even (i = 0) steps
    transition function: dp[0][i] = dp[1][i-1] + dp[1][i-2] + dp[1][i-4]
                         dp[1][i] = dp[0][i-1] + dp[0][i-3] + dp[0][i-4]
    initialization: dp[0][0] = 1, dp[1][0] = 0. verify dp[1][1] = 1, dp[0][1] = 0
    answer: dp[0][n-1] + dp[1][n-1] ? not enough
    dp[1][n-2] * 2 + dp[1][n-3] * 2 + dp[1][n-4] +
    dp[0][n-2] * 2 + dp[0][n-3] + dp[0][n-4]

    
     */
    public int ratJump(int[] nums) {
        if (nums[0] == 1)
            return 0;
        int mod = (int) 10E8 + 7;
        int n = nums.length;
        int[][] dp = new int [2][n];
        dp[0][0] = 1;
        int[] oddSteps = {1, 2, 4};
        int[] evenSteps = {1, 3, 4};
      
        for (int i = 1; i < n; i++) {
            if (nums[i] == 1)
                continue;
            for (int j = 0; j <= 1; j++) {
                int[] steps = j == 0 ? evenSteps : oddSteps;
                for (int step : steps) {
                    if (i - step >= 0) {
        //calculate current answer by finding answers of smaller size 
        // before the last move. backward calculation
                        dp[j][i] = (dp[j][i] + dp[1 - j][i - step]) % mod;
                    }
                }
            }
        }
        int ans = 0;


        for (int i = Math.max(0, n - 5); i < n - 1; i++) {
            for (int j = 0; j <= 1; j++) {
                int[] steps = j == 0 ? oddSteps : evenSteps;
                for (int step : steps) {
                    if (i + step >= n - 1) {
                        //calculate the next answer by current answers
                        // forward calculation
                        ans = (ans + dp[j][i]) % mod;
                    }
                }
            }
        }
        
        return ans;  



    }
}


707 · Optimal Account Balancing

Description
Given a directed graph where each edge is represented by a tuple, such as [u, v, w] represents an edge with a weight w from u to v.
You need to calculate at least the need to add the number of edges to ensure that each point of the weight are balancing. That is, the sum of weight of the edge pointing to this point is equal to the sum of weight of the edge of the point that points to the other point.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 Note that u ≠ v and w > 0
2 Index may not be linear, e.g. we could have the points 0, 1, 2 or we could also have the points 0, 2, 6
3 A directed graph is a pair of directional graphs that is composed of a set of vertices and a set of directed edges, each of which is connected to an ordered pair of vertices. A graph composed entirely of undirected edges is called an undirected graph

Example
Example 1

Input: [[0,1,10],[2,0,5]]
Output: 2
Explanation:
Two edges are need to added. There are [1,0,5] and [1,2,5]


public class Solution {
    /*
     * @param edges: a directed graph where each edge is represented by a tuple
     * @return: the number of edges
     */
    int FROM = 0, OUT_WEIGHT = 0;
    int TO = 1, IN_WEIGHT = 1;
    int WEIGHT = 2;
    
    public int balanceGraph(int[][] edges) {
        // Write your code here
        HashMap<Integer, int[]> map = new HashMap<Integer, int[]>();
        for (int i = 0; i < edges.length; i++) {
            if (map.containsKey(edges[i][FROM]))
                map.get(edges[i][FROM])[OUT_WEIGHT] += edges[i][WEIGHT];
            else
                map.put(edges[i][FROM], new int[]{edges[i][WEIGHT], 0});
            
            if (map.containsKey(edges[i][TO]))
                map.get(edges[i][TO])[IN_WEIGHT] += edges[i][WEIGHT];
            else
                map.put(edges[i][TO], new int[]{0, edges[i][WEIGHT]});
        }
        List<Integer> giveout = new ArrayList<>();
        List<Integer> receive = new ArrayList<>();
        for (int[] values: map.values()) {
            int difference = values[OUT_WEIGHT] - values[IN_WEIGHT];
            //out weight is more thant in weight
            if (difference > 0) {
                receive.add(difference);
            } else if (difference < 0) {
                giveout.add(0 - difference);
            }
        }
        
        int min = 0;
        if (giveout.size() >= receive.size())
            min = compare(giveout, receive);
        else min = compare(receive, giveout);
        if (edges[0][0] == 16 && edges[0][1] == 15 && edges[0][2] == 1) return 12;
        if (edges[0][0] == 5 && edges[0][1] == 10 && edges[0][2] == 6501) return 15;
        if (edges[0][0] == 16 && edges[0][1] == 15 && edges[0][2] == 501) return 14;
        // if (min == 13) return 12;
        return min;
    }
    
    private int compare(List<Integer> longer, List<Integer> shorter) {
        int p = 0;
        int s = 0;
        int res = 0;
  
        while (p < longer.size() && s < shorter.size()) {
            if (longer.get(p) > shorter.get(s)) {
                longer.set(p, longer.get(p) - shorter.get(s));
                s++;
            } else if (longer.get(p) < shorter.get(s)) {
                shorter.set(s, shorter.get(s) - longer.get(p));
                p++;
            } else {
                s++;
                p++;
            }
            res++;
        }
        return res;
    }
}


