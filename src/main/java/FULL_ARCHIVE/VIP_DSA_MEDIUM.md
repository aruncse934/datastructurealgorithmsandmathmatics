199 · Judge Connection
Description
Given a matrix of integers arr and an integer k, determine if all instances of value k in arr are connected. Two cells in a matrix are considered "connected" if they are horizontally or vertically adjacent and have the same value.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


|arr|\leq500∣arr∣≤500
|arr[i]|\leq500∣arr[i]∣≤500
0 \leq arr[i][j] \leq 100000≤arr[i][j]≤10000

Example
Example 1:

Input:arr=[
[2,2,2,0],
[0,0,0,2],
[0,1,0,2],
[1,1,1,2]]
k=2
Output:false
Explanation: Not all `2` are connected to each other

public class Solution {
    /**
     * @param arr: the arr
     * @param k: the k
     * @return: if all instances of value k in arr are connected
     */
    void dfs(int[][] arr,int x,int y,int k){
        arr[x][y] = -1;
        int dx[] = {0, 0, 1, -1};
        int dy[] = {1, -1, 0, 0};
        for (int i = 0; i < 4; i++) {
            int x1 = x + dx[i];
            int y1 = y + dy[i];
            if (x1 >= 0 && y1 >= 0 && x1 < arr.length && y1 < arr[0].length && arr[x1][y1] == k) {
                dfs(arr, x1, y1, k);
            }
        }
    }
    public boolean judgeConnection(int[][] arr, int k) {
        // Write your code here.
        int i, j, sum = 0;
        for (i = 0; i < arr.length; i++)
            for (j = 0; j < arr[0].length; j++) {
                if (arr[i][j] == k) {
                    dfs(arr, i, j, k);
                    sum++;
                    if (sum >= 2) return false;
                }
            }
        return true;
    }
}


541 · Zigzag Iterator II
Description
Follow up Zigzag Iterator: What if you are given k 1d vectors? How well can your code be extended to such cases? The "Zigzag" order is not clearly defined and is ambiguous for k > 2 cases. If "Zigzag" does not look right to you, replace "Zigzag" with "Cyclic".

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example1

Input: k = 3
vecs = [
    [1,2,3],
    [4,5,6,7],
    [8,9],
]
Output: [1,4,8,2,5,9,3,6,7]

public class ZigzagIterator2 {
    private Queue<Iterator<Integer>> iterators;
    private Iterator<Integer> tempIterator;

    /*
    * @param vecs: a list of 1d vectors
    */
    public ZigzagIterator2(List<List<Integer>> vecs) {
        iterators = new LinkedList<>();

        for (int i = 0; i < vecs.size(); i++) {
            if (vecs.get(i).size() != 0){
                iterators.offer(vecs.get(i).iterator());
            }
        }
    }

    /*
     * @return: An integer
     */
    public int next() {
        tempIterator = iterators.poll();
        int ele = tempIterator.next();
        iterators.offer(tempIterator);
        
        return ele;
    }

    /*
     * @return: True if has next
     */
    public boolean hasNext() {
        if (iterators.size() == 0) {
            return false;
        }
        if (!iterators.peek().hasNext()) {
            iterators.poll();
            return hasNext();
        }
        return true;
    }
}

//
public class ZigzagIterator2 {
    private List<Integer> locs;
    private List<List<Integer>> lists;
    private int count = 0;
    private int idx = 0;
    /*
    * @param vecs: a list of 1d vectors
    */public ZigzagIterator2(List<List<Integer>> vecs) {
        locs = new ArrayList<>();
        lists = vecs;
        for (int i = 0; i < lists.size(); i++) {
            locs.add(0);
            count += lists.get(i).size();
        }
    }

    /*
     * @return: An integer
     */
    public int next() {
        while (locs.get(idx) == lists.get(idx).size()) {
            locs.remove(idx);
            lists.remove(idx);
            idx = idx % locs.size();
        }
        
        int res = lists.get(idx).get(locs.get(idx));
        locs.set(idx, locs.get(idx) + 1);
        idx = (idx + 1) % locs.size();
        count--;
        return res;
    }

    /*
     * @return: True if has next
     */
    public boolean hasNext() {
        return count > 0;
    }
}

/**
 * Your ZigzagIterator2 object will be instantiated and called as such:
 * ZigzagIterator2 solution = new ZigzagIterator2(vecs);
 * while (solution.hasNext()) result.add(solution.next());
 * Output result
 */
 
 564 · Combination Sum IV
 Description
 Given an integer array nums with all positive numbers and no duplicates, find the number of possible combinations that add up to a positive integer target.
 
 Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)
 
 
 A number in the array can be used multiple times in the combination.
 Different orders are counted as different combinations.
 1 \leq nums.length \leq 1001≤nums.length≤100
 1 \leq nums[i] \leq 10001≤nums[i]≤1000
 1 \leq target \leq 10001≤target≤1000
 
 Example
 Example1
 
 Input: nums = [1, 2, 4], and target = 4
 Output: 6
 Explanation:
 The possible combination ways are:
 [1, 1, 1, 1]
 [1, 1, 2]
 [1, 2, 1]
 [2, 1, 1]
 [2, 2]
 [4]
 
 class Solution {
     public int combinationSum4(int[] nums, int target) {
         int[] sum = new int[target + 1];
         sum[0] = 1;
         for (int i = 1; i <= target; i++) {
             for (int num: nums) {
                 if (i >= num) {
                     sum[i] += sum[i - num];
                 }
             }
         }
         return sum[target];
     }
 }
 
 //public class Solution {
       /**
        * @param nums: an integer array and all positive numbers, no duplicates
        * @param target: An integer
        * @return: An integer
        */
       public int backPackVI(int[] nums, int target) {
           int[] dp = new int[target + 1];
           dp[0] = 1;
           for (int i = 1; i <= target; i++) {
               for (int num : nums) {
                   if (num <= i) {
                       dp[i] += dp[i - num];
                   }
               }
           }
           return dp[target];
       }
   }
   

568 · Three Chances
Description
You're given an integer array, every element are in the interval [-10, 10][−10,10].
You have three chances to change an element to any integer.
Please calculate the possible length of the subarray that its elements are the same.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of array is nn, 1 \le n \le 10^51≤n≤10 
5
 .
In the first example, the array could be changed to [0,0,0,0,0,3][0,0,0,0,0,3] or [0,3,3,3,3,3][0,3,3,3,3,3], the result are both 55.

Example
Input：
[0,3,1,0,6,3]
Output：
5
Input：
[1,2]
Output：
2

public class Solution {
    /**
     * @param nums: an integer array.
     * @return: return the possible longest length of the subarray that elements are the same.
     */
    public int threeChances(int[] nums) {
        int maxLength = 1;
        int updateCount;
        if(nums.length == 1)
        return 1;
        for(int i = 0; i < nums.length;) {
            // System.out.println("SELECTED NUM : " + i + " : " + nums[i]);

            updateCount = 0;
            int selectedNum = nums[i];
            int r = i + 1;
            while(r < nums.length && (nums[r] == selectedNum || updateCount < 3)) {
                if(nums[r] != selectedNum) {
                    updateCount++;
                }
                r++;
            }
            // System.out.println(i + " : " + r);
            maxLength = Math.max(maxLength, r - i);

            while(i < nums.length && (nums[i] == selectedNum)) i++;
        }
        return maxLength;
    }
}

643 · Longest Absolute File Path
Description
Suppose we abstract our file system by a string in the following manner:

The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:

dir
    subdir1
    subdir2
        file.ext
The directory dir contains an empty sub-directory subdir1 and a sub-directory subdir2 containing a file file.ext.

The string

"dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
represents:

dir
    subdir1
        file1.ext
        subsubdir1
    subdir2
        subsubdir2
            file2.ext
The directory dir contains two sub-directories subdir1 and subdir2. subdir1 contains a file file1.ext and an empty second-level sub-directory subsubdir1. subdir2 contains a second-level sub-directory subsubdir2 containing a file file2.ext.

We are interested in finding the longest (number of characters) absolute path to a file within our file system. For example, in the second example above, the longest absolute path is "dir/subdir2/subsubdir2/file2.ext", and its length is 32 (not including the double quotes).

Given a string representing the file system in the above format, return the length of the longest absolute path to file in the abstracted file system. If there is no file in the system, return 0.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The \n in the input is not a line break, but the characters \ and n. \t is the same.
The name of a file contains at least a . and an extension.
The name of a directory or sub-directory will not contain a ..
Time complexity required: O(n) where n is the size of the input string.
Notice that a/aa/aaa/file1.txt is not the longest file path, if there is another path aaaaaaaaaaaaaaaaaaaaa/sth.png.
Example
Example1

Input: "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"
Output: 20
Explanation:
See description for details.

public class Solution {
    /**
     * @param input: an abstract file system
     * @return: return the length of the longest absolute path to file
     */
    public int lengthLongestPath(String input) {
        // write your code here

        List<Integer> paths = new ArrayList<>();
        int idx = 0;
        int tCnt = 0;
        int begin = 0;
        boolean isFile = false;

        int max = 0;

        while (idx != input.length()) {
            if (input.charAt(idx) == '.') {
                isFile = true;
            }           
            if (input.charAt(idx) == '\t') {
                tCnt++;
                begin++;
            }
            if (input.charAt(idx) =='\n') {
                int len = idx - begin;
                if (isFile) {
                    int pLen = 0;
                    for (int i = 0; i < tCnt; i++) {
                        pLen += paths.get(i);
                    }
                    max = Math.max(max, pLen + len);

                } else {
                    if (paths.size() > tCnt) {
                        paths.set(tCnt, len + 1); // 加一个分隔符 '/'
                    } else {
                        paths.add(len + 1);                    
                    }                    
                }
                isFile = false;
                tCnt = 0;
                begin = idx + 1;
            }
            idx++;
        }

        if (isFile) {
            int len = idx - begin; 
            int pLen = 0;
            for (int i = 0; i < tCnt; i++) {
                pLen += paths.get(i);
            }
            max = Math.max(max, pLen + len);
        }

        return max;
        
    }
}

//
666 · Guess Number Higher or Lower II

Description
We are playing the Guess Game. The game is as follows:
I pick a number from 1 to n. You have to guess which number I picked.
Every time you guess wrong, I'll tell you whether the number I picked is higher or lower.
However, when you guess a particular number x, and you guess wrong, you pay $x. You win the game when you guess the number I picked.
Given a particular n ≥ 1, find out how much money you need to have to guarantee a win.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example1

Input: 10
Output: 16
Explanation:
Given n = 10, I pick 2.
First round:  You guess 7, I tell you that it's lower. You pay $7.
Second round: You guess 3, I tell you that it's lower. You pay $3.
Third round:  You guess 1, I tell you that it's higher. You pay $1.
Game over. 1 is the number I picked.
You end up paying $7 + $3 + $1 = $11.

Given n = 10, I pick 4.
First round:  You guess 7, I tell you that it's lower. You pay $7.
Second round: You guess 3, I tell you that it's higher. You pay $3.
Third round:  You guess 5, I tell you that it's lower. You pay $5.
Game over. 4 is the number I picked.
You end up paying $7 + $3 + $5 = $15.

Given n = 10, I pick 8.
First round:  You guess 7, I tell you that it's higher. You pay $7.
Second round: You guess 9, I tell you that it's lower. You pay $9.
Game over. 8 is the number I picked.
You end up paying $7 + $7 + $9 = $16.

So given n = 10, the answer is 16.

public class Solution {
    /**
     * @param n: An integer
     * @return: how much money you need to have to guarantee a win
     */
    public int getMoneyAmount(int n) {
        // write your code here

        int[][] dp=new int[n+1][n+1];


        for(int len=2; len<=n; len++){
            for(int start=1; start<=n-len+1; start++){
                int temp=Integer.MAX_VALUE;
                for(int k=start; k<start+len-1; k++){
                    int result=k+Math.max(dp[start][k-1], dp[k+1][start+len-1]);
                    temp=Math.min(temp, result);
                }
                dp[start][start+len-1]=temp;
            }
        }

        return dp[1][n];
    }
}

718 · Repeat String
Description
Write a function, give a string A consisting of N characters and a string B consisting of M characters, returns the number of times A must be stated such that B is a substring of the repeated string. If B can never be a substring of the repeated A, then your function should return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Assume that 0 <= N <= 1000, 1 <= M <= 1000

Example
Example1

Input:  A = "abcd", B = "cdabcdab"
Output: 3
Explanation: 
After stating string A three times we obtain the string `abcdabcdabcd`. String B is a substring of this string.

public class Solution {
    /**
     * @param A: string A to be repeated
     * @param B: string B
     * @return: the minimum number of times A has to be repeated
     */
    public int repeatedString(String A, String B) {
        // write your code here
        if(A.length() == 0 || B.length() == 0){
            return -1;
        }
        int m = A.length();
        int n = B.length();
        StringBuilder sb = new StringBuilder("");
        //  System.out.println(n + " " + m);
        int result = n / m;
        if(n % m != 0){
            result++;
        }
        // System.out.println(result);
        for(int i = 0; i < result; i++){
            sb.append(A);
        }
        if(sb.toString().indexOf(B) != -1){
            return result;
        }
        sb.append(A);
        result ++;
        // System.out.println(result);
        if(sb.toString().indexOf(B) != -1){
            return result;
        }
        return -1;
    }
}

729 · Last Digit By Factorial Divide
Description
We are given two numbers A and B such that B >= A. We need to compute the last digit of this resulting F such that F = B! / A! where 1 <= A, B <= 10^18 (A and B are very large)

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Given A = 2, B = 4, return 2
A! = 2 and B! = 24, F = 24 / 2 = 12 --> last digit = 2

Given A = 107, B = 109, return 2

public class Solution {
    /**
     * @param A: the given number
     * @param B: another number
     * @return: the last digit of B! / A! 
     */
    public int computeLastDigit(long A, long B) {
        // write your code here
        if(A == B) return 1;
        int count = 0;
        long current = A + 1;
        long ans = current % 10;
        while(count < 10 && current < B) {
          current++;
          ans = ((ans % 10) * (current % 10)) % 10;
          count++;
        }
        return (int)ans;
    }
}

824 · Single Number IV

Description
Give an array, all the numbers appear twice except one number which appears once and all the numbers which appear twice are next to each other. Find the number which appears once.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= nums.length < 10^4
In order to limit the time complexity of the program, your program will run 10^5 times.
Example
Example 1:

Input: [3,3,2,2,4,5,5]
Output: 4
Explanation: 4 appears only once.

public class Solution {
    /**
     * @param nums: The number array
     * @return: Return the single number
     */
    public int getSingleNumber(int[] nums) {
        for(int i = 0; i < nums.length; i += 2) {
            if(i == nums.length-1) return nums[i];
            if(nums[i] != nums[i+1]) return nums[i];
        }
        return -1;
    }
    
    //
     public int getSingleNumber(int[] nums) {
            int i;//标识下标
            if(nums.length==1)return nums[0];
            for(i=1;i<nums.length-1;i++){
               if(nums[i]!=nums[i+1]&&nums[i-1]!=nums[i])break;
            }
            return nums[i];
        }
}

855 · Sentence Similarity II

Description
Given two sentences words1, words2 (each represented as an array of strings), and a list of similar word pairs pairs, determine if two sentences are similar.
For example, words1 = ["great", "acting", "skills"] and words2 = ["fine", "drama", "talent"] are similar, if the similar word pairs are pairs = [["great", "good"], ["fine", "good"], ["acting","drama"], ["skills","talent"]].
Note that the similarity relation is transitive. For example, if "great" and "good" are similar, and "fine" and "good" are similar, then "great" and "fine" are similar.
Similarity is also symmetric. For example, "great" and "fine" being similar is the same as "fine" and "great" being similar.
Also, a word is always similar with itself. For example, the sentences words1 = ["great"], words2 = ["great"], pairs = [] are similar, even though there are no specified similar word pairs.
Finally, sentences can only be similar if they have the same number of words. So a sentence like words1 = ["great"] can never be similar to words2 = ["doubleplus","good"].

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.The length of words1 and words2 will not exceed 1000.
2.The length of pairs will not exceed 2000.
3.The length of each pairs[i] will be 2.
4.The length of each words[i] and pairs[i][j] will be in the range [1, 20].

Example
Example 1:

Input:
["7", "5", "4", "11", "13", "15", "19", "12", "0", "10"]
["16", "1", "7", "3", "15", "10", "13", "2", "19", "8"]
[["6", "18"], ["8", "17"], ["1", "13"], ["0", "8"], ["9", "14"], ["11", "17"], ["11", "19"], ["13", "16"], ["0", "18"], ["3", "11"], ["1", "9"], ["2", "11"], ["2", "4"], ["0", "19"], ["8", "12"], ["8", "19"], ["16", "19"], ["1", "11"], ["2", "18"], ["0", "16"], ["7", "11"], ["6", "8"], ["9", "17"], ["8", "16"], ["3", "13"], ["7", "9"], ["7", "10"], ["3", "6"], ["15", "19"], ["1", "5"], ["2", "14"], ["1", "18"], ["8", "15"], ["14", "19"], ["3", "17"], ["6", "10"], ["5", "17"], ["10", "15"], ["1", "10"], ["4", "6"]]
Output:
true

public class Solution {
    /**
     * @param words1: 
     * @param words2: 
     * @param pairs: 
     * @return: Whether sentences are similary or not?
     */

    HashMap<String, String> parent;
    HashMap<String, Integer> rank;

    public boolean areSentencesSimilarTwo(List<String> words1, List<String> words2, List<List<String>> pairs) {
        if (words1.size() != words2.size()) {
            return false;
        }

        // Initialising Parent & Rank
        parent = new HashMap<>();
        rank = new HashMap<>();

        for (int i = 0; i < pairs.size(); i++) {
            String s1 = pairs.get(i).get(0);
            String s2 = pairs.get(i).get(1);

            if (parent.containsKey(s1) == false) {
                parent.put(s1, s1);
                rank.put(s1, 0);
            }

            if (parent.containsKey(s2) == false) {
                parent.put(s2, s2);
                rank.put(s2, 0);
            }

        }

        // Performing DSU
        for (int i = 0; i < pairs.size(); i++) {
            String s1 = pairs.get(i).get(0);
            String s2 = pairs.get(i).get(1);

            String l1 = find(s1);
            String l2 = find(s2);

            if (l1.equals(l2) != true) {
                // Merging
                if (rank.get(l1) < rank.get(l2)) {
                    parent.put(l1, l2);
                } else if (rank.get(l1) > rank.get(l2)) {
                    parent.put(l2, l1);
                } else {
                    parent.put(l1, l2);
                    rank.put(l2, rank.get(l2) + 1);
                }
            }

        }

        // Checking Whether Two Sentences Are Similar Or Not
        for (int i = 0; i < words1.size(); i++) {
            String word1 = words1.get(i);
            String word2 = words2.get(i);

            if (word1.equals(word2) == true) {
                continue;
            }

            if (parent.containsKey(word1) != true || parent.containsKey(word2) != true) {
                return false;
            }

            String p1 = find(word1);
            String p2 = find(word2);

            if (p1.equals(p2) == false) {
                return false;
            }

        }

        return true;
    }

    public String find(String x) {
        if (parent.get(x).equals(x) == true) {
            return x;
        } else {
            String answer = find(parent.get(x));
            parent.put(x, answer); // Path Compression
            return answer;
        }
    }

}

906 · Sort Transformed Array

Description
Given a sorted array of integers nums and integer values a, b and c. Apply a quadratic function of the form f(x)=ax^2+bx+cf(x)=ax 
2
 +bx+c to each element xx in the array.

The returned array must be in sorted order.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Expected time complexity: O(n)

Example
Example1

Input: nums = [-4, -2, 2, 4], a = 1, b = 3, c = 5
Output: [3, 9, 15, 33]

public class Solution {
    /**
     * @param nums: a sorted array
     * @param a: 
     * @param b: 
     * @param c: 
     * @return: a sorted array
     */
    public int[] sortTransformedArray(int[] nums, int a, int b, int c) {
        // Write your code here
        int[] res = new int[nums.length];
        int start = 0, end = nums.length - 1, idx = 0;
        if (a >= 0) {
            idx = end;
        }
        while (start <= end) {
            int startRes = getRes(nums[start], a, b, c);
            int endRes = getRes(nums[end], a, b, c);
            if (a >= 0) {
                if (startRes >= endRes) {
                    res[idx--] = startRes;
                    start++;
                } else {
                    res[idx--] = endRes;
                    end--;
                }
            } else {
                if (startRes <= endRes) {
                    res[idx++] = startRes;
                    start++;
                } else {
                    res[idx++] = endRes;
                    end--;
                }
            }
        }
        return res;
    }

    private int getRes(int x, int a, int b, int c) {
        return a * x * x + b * x + c;
    }
}

922 · Group Shifted Strings

Description
Given a string, we can "shift" each of its letter to its successive letter, for example: "abc" -> "bcd". We can keep "shifting" which forms the sequence:

"abc" -> "bcd" -> ... -> "xyz"
Given a list of strings which contains only lowercase alphabets, group all strings that belong to the same shifting sequence.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


You don't need to care about the order of the result.

Example
Example 1:

input:["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"]
output: [["a","z"],["abc","bcd","xyz"],["acef"],["az","ba"]]

public class Solution {
    /**
     * @param strings: a string array
     * @return: return a list of string array
     */
    public List<List<String>> groupStrings(String[] strings) {
        // write your code here

        Map<String, List<String>> map = new HashMap<>();
        for(String s : strings){
            String low = getLowerString(s);
            System.out.println(s + " to low " + low);
            if(map.containsKey(low)){
                map.get(low).add(s);
            }else{
                map.put(low, new ArrayList<>());
                map.get(low).add(s);
            }
        }

        List<List<String>> ans = new ArrayList<>();
        ans.addAll(map.values());
        return ans;
    }

    public String getLowerString(String str){
        if(str.length() == 0){
            return str;
        }

        if(str.length() == 1){
            return "a";
        }

        char min = str.charAt(0);
        int diff = min - 'a';    
        System.out.println(diff);

        StringBuilder sb = new StringBuilder();
        for(int i=0;i<str.length();i++){
            sb.append((char) ((26 + str.charAt(i)-diff) % 26 ));
        }

        return sb.toString();
    }
}

1257 · Evaluate Division

Description
Equations are given in the format A / B = k, where A and B are variables represented as strings, and k is a real number (floating point number). Given some queries, return the answers. If the answer does not exist, return -1.0.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is no contradiction.
Example
Given a / b = 2.0, b / c = 3.0.
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .
return [6.0, 0.5, -1.0, 1.0, -1.0 ].

The input is: vector<pair<string, string>> equations, 
vector<double>& values, 
vector<pair<string, string>> queries , 
where equations.size() == values.size(), and the values are positive. 
This represents the equations. Return vector<double>.

According to the example above:

equations = [ ["a", "b"], ["b", "c"] ],
values = [2.0, 3.0],
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ].

public class Solution {
    /**
     * @param equations: 
     * @param values: 
     * @param queries: 
     * @return: return a double type array
     */
    public double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        // write your code here
        if(queries == null || queries.size() == 0) return new double[0];
        // Store a point's root in the union find
        Map<String, String> edgeMap = new HashMap<>();
        // Store a point's relationship with its root
        Map<String, Double> ratioMap = new HashMap<>();

        int n = values.length;
        if(n == 0) return new double[0];
        for(int i = 0; i < n; i ++){
            String left = equations.get(i).get(0), right = equations.get(i).get(1);
            if(!edgeMap.containsKey(left)){
                edgeMap.put(left, left);
                ratioMap.put(left, 1.0);
            }

            if(!edgeMap.containsKey(right)){
                edgeMap.put(right, right);
                ratioMap.put(right, 1.0);
            }

            String root1 = find(left, edgeMap, ratioMap);
            String root2 = find(right, edgeMap, ratioMap);

            edgeMap.put(root1, root2);
            double ratio = ratioMap.get(right) * values[i] / ratioMap.get(left);
            ratioMap.put(root1, ratio);
        }
        double[] result = new double[queries.size()];
        int i = 0;
        for(List<String> query : queries){
            String left = query.get(0), right = query.get(1);
            if(!edgeMap.containsKey(left) || !edgeMap.containsKey(right) || !find(left, edgeMap, ratioMap).equals(find(right, edgeMap, ratioMap))){
                result[i] = -1.0;
                i++;
                continue;
            }
            result[i] = ratioMap.get(left) / ratioMap.get(right);
            i++;
        }
        return result;
    }


    private String find(String start, Map<String, String> edgeMap, Map<String, Double> ratioMap){
        if(edgeMap.get(start).equals(start)) return start;

        String prev = edgeMap.get(start);
        String root = find(edgeMap.get(start), edgeMap, ratioMap);

        edgeMap.put(start, root);

        ratioMap.put(start, ratioMap.get(prev) * ratioMap.get(start));

        return root;

    }
}

1313 · Bipartite Graph

Description
You are given a complete graph and you should divide the nodes into 2 groups so that the maximum weight of an edge which connects two points of a same group is minimized.
The number of points in each group should not be less than 2

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


number of point4 \leq n \leq 144≤n≤14
edge value 1 \leq val \leq 500001≤val≤50000

Example
input：[[0,270,60,20],[270,0,35,90],[60,35,0,100],[20,90,100,0]]
output：35
Explanation: Point 1 and 2 are in the same group, and point 0 and 3 are in the same group. In this way, the weight of edge of point 1 and 2 is larger than the that of point 0 and 3, so the answer is 35.

public class Solution {
    private int[] flag;
    private int cnt1 = 0, cnt2 = 0;
    /**
     * @param graph: graph edge value
     * @return: return the minium length of graph
     */
    public int getMiniumValue(int[][] graph) {
        flag = new int[graph.length];

        int low = 1, high = 50010;
        while (low + 1 < high) {
          int mid = low + (high - low) / 2;

          if (check(graph, mid)) {
            high = mid;
          } else {
            low = mid;
          }
        }

        if (check(graph, low)) {
          return low;
        }

        return high;
    }

    private boolean check(int[][] graph, int threshold) {
        int i, j, n = graph.length;
        cnt1 = cnt2 = 0;

        Arrays.fill(flag, -1);

        for (i = 0; i < n; i ++) {
          if (flag[i] < 0) {
            flag[i] = cnt1 < cnt2 ? 0 : 1;
            if (!dfs(i, threshold, graph)) {
              return false;
            }
          }
        }

        return cnt1 > 1 && cnt2 > 1;
    }

    private boolean dfs(int current, int threshold, int[][] graph) {
      int n = graph.length;

      if (flag[current] == 0) {
        cnt1 ++;
      } else {
        cnt2 ++;
      }

      for (int i = 0; i < n; i ++) {
        if (graph[current][i] > threshold) {
          if (flag[current] == flag[i]) {
            return false;
          }

          if (flag[i] < 0) {
            flag[i] = 1 - flag[current];
            if (!dfs(i, threshold, graph)) {
              return false;
            }
          }
        }
      }

      return true;
    }
}

1366 · Directed Graph Loop

Description
Please judge whether there is a cycle in the directed graph with n vertices and m edges. The parameter is two int arrays. There is a directed edge from start[i] to end[i].

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


2 <= n <= 10^5
1 <= m <= 4*10^5
1 <= start[i], end[i] <= n
Example
Example1

Input: start = [1],end = [2]
Output: "False"
Explanation:
There is only one edge 1->2, and do not form a cycle.

public class Solution {
    /**
     * @param start: The start points set
     * @param end: The end points set
     * @return: Return if the graph is cyclic
     */
    public boolean isCyclicGraph(int[] start, int[] end) {
        // Write your code here
        int n = 0;
        for(int i=0;i<start.length;i++){
            n = Math.max(start[i], n);
            n = Math.max(end[i], n);
        }

        List<Integer>[] adj = new LinkedList[n+1];
        for(int i=0;i<adj.length;i++)
            adj[i] = new LinkedList<>();
        for(int i=0;i<start.length;i++)
            adj[start[i]].add(end[i]);
        boolean[] recStack = new boolean[adj.length];
        boolean[] visited = new boolean[adj.length];
        for(int i=1;i<adj.length;i++){
            if(detected(adj, visited, recStack, i))
                return true;
        }
        return false;
    }
    boolean detected(List<Integer>[] adj, boolean[] visited, boolean[] recStack, int i){
        if(recStack[i])
            return true;
        if(visited[i])
            return false;
        recStack[i] = true;
        for(int next: adj[i]){
            if(detected(adj, visited, recStack, next))
                return true;
        }
        recStack[i] = false;
        visited[i] = true;
        return false;
    }
}

1367 · Police Distance

Description
Given a matrix size of n x m, element 1 represents policeman, -1 represents wall and 0 represents empty.
Now please output a matrix size of n x m, output the minimum distance between each empty space and the nearest policeman

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Given a matrix size of n x m， n <= 200，m <= 200.
We guarantee that each empty space can be reached by one policeman at least.
Example
Example1

Input: 
mat =
[
    [0, -1, 0],
    [0, 1, 1],
    [0, 0, 0]
]
Output: [[2,-1,1],[1,0,0],[2,1,1]]
Explanation:
The distance between the policeman and himself is 0, the shortest distance between the two policemen to other empty space is as shown above

public class Solution {
    /**
     * @param matrix : the martix
     * @return: the distance of grid to the police
     */
    class Point {
        int x, y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    int[] dirX = {-1, 0, 1, 0};
    int[] dirY = {0, 1, 0, -1};
    public int[][] policeDistance(int[][] matrix) {
        int[][] result = new int[matrix.length][matrix[0].length];
        if (matrix == null || matrix.length == 0 && matrix[0].length == 0) {
            return null;
        }
        Queue<Point> queue = new LinkedList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == 1) {
                    queue.offer(new Point(i, j));
                    result[i][j] = 0; //police
                }
                if (matrix[i][j] == -1) {
                    result[i][j] = -1;
                }
                if (matrix[i][j] == 0) {
                    result[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            steps++;
            for (int i = 0; i < size; i++) {
                Point p = queue.poll();
                for (int j = 0; j < dirX.length; j++) {
                    Point neighbor = new Point(p.x + dirX[j], p.y + dirY[j]);
                    if (!inBound(matrix, neighbor)) {
                        continue;
                    }
                    if (steps < result[neighbor.x][neighbor.y]) {
                        result[neighbor.x][neighbor.y] = steps;
                        queue.offer(neighbor);
                    }
                }
            }
        }
        return result;
    }
    private boolean inBound(int[][] matrix, Point p) {
        int n = matrix.length;
        int m = matrix[0].length;
        return p.x >= 0 && p.x < n && p.y >= 0 && p.y < m;
    }
}

1373 · Movies on Flight

Description
You are on a flight and wanna watch two movies during this flight.
You are given an array movieDurations which includes all the movie durations.
You are also given the duration of the flight which is k minutes.
Now, you need to pick two movies and the total duration of the two movies is less than or equal to (k - 30min).

Find the pair of movies with the longest total duration and return their indexes. If multiple found, he pair with the largest duration of a single movie.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example:
Input: movieDurations = [90, 85, 75, 60, 120, 150, 125], d = 250
Output: [0, 6]
Explanation: movieDurations[0] + movieDurations[6] = 90 + 125 = 215 is the maximum number within 220 (250min - 30min)

public class Solution {
    /**
     * @param arr: An integer array represents durations of movies
     * @param k: An integer represents the duration of the flight
     * @return: the pair of movies index with the longest total duration
     */
    public int[] FlightDetails(int[] nums, int k) {
        // write your code here
	   if( nums == null || nums.length < 2){
		   return new int[2];
	   }
	   Map<Integer,Integer> map = new HashMap<>();
	   for(int i = 0;i < nums.length;i++){
		   map.put(nums[i],i);
	   }
	   Arrays.sort(nums);
	   int left = 0,right = nums.length - 1,sum = 0;
	   int[] res = new int[2];
	   while(left < right){
		   int newSum = nums[left] + nums[right];
		   if(newSum > k - 30){
			   right--;
		   }else{
			 if(newSum > sum) {
				 sum = newSum;
				 res[0] = map.get(nums[left]) <  map.get(nums[right]) ? map.get(nums[left]) : map.get(nums[right]);
				 res[1] = map.get(nums[left]) >  map.get(nums[right]) ? map.get(nums[left]) : map.get(nums[right]);
			 }
			 left++; 
		   }
	   }
	   return res;
    }
}


1399 · Take Coins

Description
There aren coins in a row, each time you want to take a coin from the left or the right side. Take a total of K times and write an algorithm to maximize the sum of coins you can get.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= k <= n <= 100000。
The value of the coin is not greater than 10000。
Example
Example 1:

Input: list = [5,4,3,2,1], k = 2
Output :9
Explanation:Take two coins from the left.

public class Solution {
    /**
     * @param list: The coins
     * @param k: The k
     * @return: The answer
     */
    public int takeCoins(int[] list, int k) {
        // Write your code here
        int n = list.length;
        int i = 0, j;
        int sum = 0, total = 0, min = Integer.MAX_VALUE;
        for(j = 0; j < n; j++){
            sum += list[j];
            total += list[j];
            if(j - i + 1 == n - k){
                min = Math.min(min, sum);
                sum -= list[i];
                i ++;
            }
        }
        return n == k ? total : total - min;
    }
}

1402 · Recommend Friends

Description
Give n personal friends list, Tell you who the user is. Find the person that user is most likely to know. (He and the user have the most common friends and he is not a friend of user)

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


n <= 500.
The relationship between friends is mutual. (if B appears on a's buddy list, a will appear on B's friends list).
Each person's friend relationship does not exceed m, m <= 3000.
If there are two people who share the same number of friends as user, the smaller number is considered the most likely person to know.
If user and all strangers have no common friends, return -1.
Example
Example 1:

Input:list = [[1,2,3],[0,4],[0,4],[0,4],[1,2,3]], user = 0
Output:4
Explanation:0 and 4 are not friends, and they have 3 common friends. So 4 is the 0 most likely to know.


public class Solution {
    /**
     * @param friends: people's friends
     * @param user: the user's id
     * @return: the person who most likely to know
     */
    public int recommendFriends(int[][] F, int U) {
        // Write your code here 
        if (F == null || F.length == 0) {
            return -1;
        }

        Set<Integer> set = new HashSet<>();
        for (int i : F[U]) {
            set.add(i);
        }

        int max = 0;
        int res = F.length;

        for (int i = 0; i < F.length; i++) {
            if (i == U || set.contains(i)) {
                continue;
            }

            int count = common(set, F[i]);
            if (count == 0) {
                continue;
            }

            if (count > max) {
                max = count;
                res = i;
            } else if (count == max) {
                if (res > i) {
                    res = i;
                    max = count;
                }
            }
        }

        return res == F.length ? -1 : res;
    }

    int common(Set<Integer> set, int[] a) {
        int res = 0;

        for (int i : a) {
            if (set.contains(i)) {
                res++;
            }
        }
        return res;
    }
}

1446 · 01 Matrix Walking Problem

Description
Given an 01 matrix gird of size n*m, 1 is a wall, 0 is a road, now you can turn a 1 in the grid into 0, Is there a way to go from the upper left corner to the lower right corner? If there is a way to go, how many steps to take at least?

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq n \leq 1* 10^31≤n≤1∗10 
3
 
1 \leq m \leq 1* 10^31≤m≤1∗10 
3
 
Example
Example 1:

Input: a = [[0,1,0,0,0],[0,0,0,1,0],[1,1,0,1,0],[1,1,1,1,0]] 
Output:7 
Explanation:Change `1` at (0,1) to `0`, the shortest path is as follows:
(0,0)->(0,1)->(0,2)->(0,3)->(0,4)->(1,4)->(2,4)->(3,4) There are many other options of length `7`, not listed here.
Example 2:

Input:a = [[0,1,1],[1,1,0],[1,1,0]] 
Output:-1 
Explanation:Regardless of which `1` is changed to `0`, there is no viable path.


public class Solution {
    /**
     * @param grid: The gird
     * @return: Return the steps you need at least
     */
    class Point {
        int x, y, replace;
        public Point(int x, int y, int replace) {
            this.x = x;
            this.y = y;
            this.replace = replace;
        }
    }
    int[] dirX = {-1, 0, 1, 0};
    int[] dirY = {0, 1, 0, -1};
    public int getBestRoad(int[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return -1;
        }
        Queue<Point> queue = new LinkedList<>();
        queue.offer(new Point(0, 0, 0));
        int[][] visited = new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                visited[i][j] = 2;
            }
        }
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            steps++;
            for (int i = 0; i < size; i++) {
                Point p = queue.poll();
                for (int j = 0; j < dirX.length; j++) {
                    if (!inBound(grid, p.x + dirX[j], p.y + dirY[j])) {
                        continue;
                    }
                    Point neighbor = new Point(p.x + dirX[j], p.y + dirY[j], p.replace + grid[p.x + dirX[j]][p.y + dirY[j]]);
                    if (neighbor.replace > 1) {
                        continue;
                    }
                    if (neighbor.x == grid.length - 1 && neighbor.y == grid[0].length - 1) {
                        return steps;
                    }
                    if (neighbor.replace < visited[neighbor.x][neighbor.y]) {
                        queue.offer(neighbor);
                        visited[neighbor.x][neighbor.y] = neighbor.replace;
                    }
                }
            }
        }
        return -1;
    }
    private boolean inBound(int[][] grid, int x, int y) {
        int n = grid.length;
        int m = grid[0].length;
        return x >= 0 && x < n && y >= 0 && y < m;
    }
}

1447 · Calculation The Sum Of Path

Description
Enter a matrix of length L, width W and three points that must pass through. How many ways can you walk from the upper left corner to the lower right corner？(Each step can only go right or down). The input is guaranteed that there is at least one legal path. You only need to return the solution number mod 1000000007.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 \leq l, w \leq 20001≤l,w≤2000
Example
Example 1:

Given `l=4`, `w=4`. The three mandatory points are `[1,1],[2,2],[3,3]`. Return `8`.
Input:
4
4
[[1,1],[2,2],[3,3]]
Output:
8

Explanation:
[1,1]->[1,2]->[2,2]->[2,3]->[3,3]->[3,4]->[4,4]
[1,1]->[1,2]->[2,2]->[2,3]->[3,3]->[4,3]->[4,4]
[1,1]->[1,2]->[2,2]->[3,2]->[3,3]->[3,4]->[4,4]
[1,1]->[1,2]->[2,2]->[3,2]->[3,3]->[4,3]->[4,4]
[1,1]->[2,1]->[2,2]->[2,3]->[3,3]->[3,4]->[4,4]
[1,1]->[2,1]->[2,2]->[2,3]->[3,3]->[4,3]->[4,4]
[1,1]->[2,1]->[2,2]->[3,2]->[3,3]->[3,4]->[4,4]
[1,1]->[2,1]->[2,2]->[3,2]->[3,3]->[4,3]->[4,4]
The sum is 8.

/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */

class PointComparator implements Comparator<Point> {
    public int compare(Point a, Point b) {
        if (a.x == b.x) {
            return a.y - b.y;
        }

        return a.x - b.x;
    }
}

public class Solution {

    private static final int MOD = 1000000007;

    /**
     * @param l: The length of the matrix
     * @param w: The width of the matrix
     * @param points: three points 
     * @return: The sum of the paths sum
     */
    public long calculationTheSumOfPath(int l, int w, Point[] points) {
        // Write your code here

        // int MOD = 1000000007;

        Arrays.sort(points, new PointComparator());

        long result = 1L;
        int n = points.length;
        List<Point> newPoints = new ArrayList<>();

        if (points[0].x != 1 && points[0].y != 1) {
            newPoints.add(new Point(1,1));
        }

        for (int i = 0; i < n; i++) {
            newPoints.add(points[i]);
        }

        if (points[n-1].x != l && points[n-1].y != w) {
            newPoints.add(new Point(l, w));
        }
        n = newPoints.size();
        
        // Point start = points[0];
        for (int i = 1; i < n; i++) {
            // Point end = points[i];

            result = (result * getNumsOfSolutions(newPoints.get(i-1), newPoints.get(i))) % MOD;
            // start = end;
        }

        return result;
    }

    private int getNumsOfSolutions( Point start, Point end) {
        int n = end.x - start.x + 1;
        int m = end.y - start.y + 1;

        int[][] dp = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                    continue;
                }

                dp[i][j] = (dp[i-1][j] + dp[i][j-1]) % MOD;
            }
        }

        return dp[n-1][m-1];

        // dp[i][j] = dp[i-1][j] + dp[i][j-1]; if (i > 0 and j > 0);
        // corner case: dp[0][j] = 1; dp[i][0] = 1;
    }
}

1448 · Card Game

Description
A card game that gives you the number of cards n，and two non-negative integers: totalProfit, totalCost. Then it will give you the profit value of every card a[i] and the cost value of every card b[i].It is possible to select any number of cards from these cards, form a scheme. Now we want to know how many schemes are satisfied that all selected cards' profit values are greater than totalProfit and the costs are less than totalCost.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Since this number may be large, you only need to return the solution number mod 1e9 + 7.
0 \leq n \leq 1000≤n≤100
0 \leq totalProfit\leq 1000≤totalProfit≤100
0 \leq totalCost \leq 1000≤totalCost≤100
0 \leq a[i] \leq 1000≤a[i]≤100
0 \leq b[i] \leq 1000≤b[i]≤100
Example
Example 1:

Input：n = 2，totalProfit = 3，totalCost = 5，a = [2,3]，b = [2,2] 
Output：1
Explanation:
At this time, there is only one legal scheme, which is to select both cards. At this time, a[1]+a[2] = 5 > totalProfit and b[1] + b[2] < totalCost.

public class Solution {
  /**
   * @param n: The number of cards
   * @param totalProfit: The totalProfit
   * @param totalCost: The totalCost
   * @param a: The profit of cards
   * @param b: The cost of cards
   * @return: Return the number of legal plan
   */
  public int numOfPlan(int n, int totalProfit, int totalCost, int[] a, int[] b) {
      int mod = 1000000007;
      totalProfit++;
      totalCost--;
      int[][][] dp = new int[n + 1][totalProfit + 1][totalCost + 1]; // dp[i][j][k] : # of plans, pick from the first n cards, profit >= j, cost <= k

    //   for (int i = 0; i < n + 1; i++) {
        for (int k = 0; k < totalCost + 1; k++) {
          dp[0][0][k] = 1;
        }
    //   }
      
      for (int i = 1; i < n + 1; i++) {
        for (int j = 0; j < totalProfit + 1; j++) {
          for (int k = 0; k < totalCost + 1; k++) {
            // profit is a[i - 1], cost is b[i - 1];
            if (b[i - 1] <= k) {
                if (a[i - 1] <= j) {
                    dp[i][j][k] = (dp[i - 1][j][k] + dp[i - 1][j - a[i - 1]][k - b[i - 1]]) % mod;
                } else {
                    dp[i][j][k] = (dp[i - 1][j][k] + dp[i - 1][0][k - b[i - 1]]) % mod;
                }
                
            } else {
                dp[i][j][k] = dp[i - 1][j][k];
            }
          }
        }
      }

      return (int) dp[n][totalProfit][totalCost];
  }
}

1513 · Exam Room

Description
In an exam room, there are N seats in a single row, numbered 0, 1, 2, ..., N-1.

When a student enters the room, they must sit in the seat that maximizes the distance to the closest person. If there are multiple such seats, they sit in the seat with the lowest number. (Also, if no one is in the room, then the student sits at seat number 0.)

Return a class ExamRoom(int N) that exposes two functions: ExamRoom.seat() returning an int representing what seat the student sat in, and ExamRoom.leave(int p) representing that the student in seat number p now leaves the room. It is guaranteed that any calls to ExamRoom.leave(p) have a student sitting in seat p.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= N <= 10^9
ExamRoom.seat() and ExamRoom.leave() will be called at most 10^4 times across all test cases.
Calls to ExamRoom.leave(p) are guaranteed to have a student currently sitting in seat number p.
Example
Example 1:

Input: ["ExamRoom","seat","seat","seat","seat","leave","seat"], [[10],[],[],[],[],[4],[]]
Output: [null,0,9,4,2,null,5]
Explanation:
ExamRoom(10) -> null
seat() -> 0, no one is in the room, then the student sits at seat number 0.
seat() -> 9, the student sits at the last seat number 9.
seat() -> 4, the student sits at the last seat number 4.
seat() -> 2, the student sits at the last seat number 2.
leave(4) -> null
seat() -> 5, the student sits at the last seat number 5.


public class ExamRoom {
    private List<Integer> seats;
    private int N;
    

    public ExamRoom(int N) {
        this.N = N;
        seats = new ArrayList<>();
    }
    
    public int seat() {
        int size = seats.size();
        if (size == 0) {
            seats.add(0);
            return 0;
        }
        int seatId = 0, index = N - 1, maxDist = N - 1 - seats.get(size - 1);
        for (int i = size - 1; i > 0; i--) {
            int dist = (seats.get(i) - seats.get(i - 1)) / 2;
            if (dist >= maxDist) {
                index = i - 1;
                maxDist = dist;
            }
        }
        if (seats.get(0) >= maxDist) {
            seatId = 0;
            seats.add(0, seatId);
        } else if (index == N - 1) {
            seatId = N - 1;
            seats.add(seatId);
        } else {
            seatId = (seats.get(index) + seats.get(index + 1)) / 2;
            seats.add(index + 1, seatId);
        }
        return seatId;
    }
    
    public void leave(int p) {
        for (int i = 0; i < seats.size(); i++) {
            if (seats.get(i) == p) {
                seats.remove(i);
            }
        }
    }
}

//

class ExamRoom {
    int N;
    TreeSet<Integer> students;

    public ExamRoom(int N) {
        this.N = N;
        students = new TreeSet();
    }

    public int seat() {
        //Let's determine student, the position of the next
        //student to sit down.
        int student = 0;
        if (students.size() > 0) {
            //Tenatively, dist is the distance to the closest student,
            //which is achieved by sitting in the position 'student'.
            //We start by considering the left-most seat.
            int dist = students.first();
            Integer prev = null;
            for (Integer s: students) {
                if (prev != null) {
                    //For each pair of adjacent students in positions (prev, s),
                    //d is the distance to the closest student;
                    //achieved at position prev + d.
                    int d = (s - prev) / 2;
                    if (d > dist) {
                        dist = d;
                        student = prev + d;
                    }
                }
                prev = s;
            }

            //Considering the right-most seat.
            if (N - 1 - students.last() > dist)
                student = N - 1;
        }

        //Add the student to our sorted TreeSet of positions.
        students.add(student);
        return student;
    }

    public void leave(int p) {
        students.remove(p);
    }
}


1539 · Flipped the Pixel

Description
An image is arranged in pixels of the two-dimensional array byte[][], where each element of the array represents a pixel bit (0 or 1). Now you need to flip these pixels, first flip the pixels of each row symmetrically, then flip the pixels on each bit (0->1,1->0)。

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.both 1 and 0 are integers
2.The Byte is not empty

Example
Example 1

Input: byte[][]= [[1,0,1,1,0],[0,1,1,0,1],[1,1,0,1,0],[0,0,1,0,0]]
Output: byte[][]= [[1,0,0,1,0],[0,1,0,0,1],[1,0,1,0,0],[1,1,0,1,1]]
Explanation: First, we flip the pixels of each row symmetrically, which changes to [[0,1,1,0,1],[1,0,1,1,0],[0,1,0,1,1],[0,0,1,0,0]]. Then we flip the pixels on each bit and get the answer [[1,0,0,1,0],[0,1,0,0,1],[1,0,1,0,0],[1,1,0,1,1]].


public class Solution {
    /**
     * @param Byte: 
     * @return: return the answer after flipped
     */
    public int[][] flippedByte(int[][] Byte) {
        // Write your code here
        if (Byte == null || Byte.length == 0 || Byte[0].length == 0) {
            return Byte;
        }
        int n = Byte.length, m = Byte[0].length;
        int[][] result = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                result[i][j] = -1 * Byte[i][j] + 1;
            }
        }
        for (int[] row : result) {
            getSymmetric(row);
        }
        return result;

    }
    private void getSymmetric(int[] row) {
        int n = row.length;
        int left = 0, right = n - 1;
        while (left < right) {
            int tmp = row[left];
            row[left] = row[right];
            row[right] = tmp;
            left++;
            right--;
        }
    }
}

1541 · Put Box

Description
There are a group of boxes and a group of positions. Given the two arrays the height of boxes and the height of positions. You can put the box in the position if the height of the box is not higher than the position. And only one box can be placed per position. You need to put the box in position in order and find out the maximum number of boxes you can put in.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of the array box and position multiplied by each other will not exceed 1000000.

Example
Example 1:

input: box = [4,2,3,1], position = [1,2,3,4]
output: 3
Explanation:
In addition to the box 1, the other three can be put down
So the answer is 3
Example 2:

input: box = [2,6], position = [3,8]
output: 2
Explanation:
Because the height of the box is lower than the height of the position, it can be put down
So the answer is 2


public class Solution {
  /**

      4   3   2   1
  1   1
  3
  2
  4
    */
  public int putBox(int[] box, int[] position) {
    final int N = position.length;
    int[] dp = new int[N + 1], prev = dp.clone();
    for (int i = box.length - 1; i != -1; i--) {
      final int boxheight = box[i];
      int[] tmp = prev;
      prev = dp;
      dp = tmp;
      for (int j = 0, j2 = N - 1; j != N; j++, j2--) {
        if (boxheight <= position[j2])
          dp[j + 1] = prev[j] + 1;
        else
          dp[j + 1] = Math.max(dp[j], prev[j + 1]);
      }
    }
    return dp[N];
  }
  private void reverse(int[] nums) {
    for (int i = 0, j = nums.length - 1; i < j; i++, j--) {
      int tmp = nums[i];
      nums[i] = nums[j];
      nums[j] = tmp;
    }
  }
}

1542 · NextTime Norepeat

Description
Give a String, representing the time, such as "12:34"(This is a legal input data), and find its next time does not repeat the number. If it is the largest"23:59", the reply is the smallest"01:23". If the input is illegal, return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.The given String represents a time of 24 hours
2.The string contains only numeric characters and ":"
3. The length of the string is 5

Example
Example 1

Input: "23:59"
Output: "01:23"


public class Solution {
    /**
     * @param time: 
     * @return: return a string represents time
     */
    public String nextTime(String time) {
        // write your code here
        if (time == null || time.length() != 5) {
          return "-1";
        }

        String[] curr = time.split(":");
        int hour = Integer.valueOf(curr[0]);
        int minute = Integer.valueOf(curr[1]);
        if (hour >= 24 || minute >= 60) {
          return "-1";
        }

        int i = hour;
        while (i < hour + 24) {
          for (int j = minute + 1; j < minute + 1 + 60; j ++) {
            if (j >= 60) {
              i ++;
            }
            j = j % 60;

            i = i % 24;

            if (isValid(i, j)) {
              return getString(i, j);
            }
          }
        }

        return "-1";
    }

    private String getString(int hour, int minute) {
      StringBuilder sb = new StringBuilder();

      if (hour < 10) {
        sb.append("0");
      } 

      sb.append(hour + ":");

      if (minute < 10) {
        sb.append("0");
      }

      sb.append(minute + "");

      return sb.toString();
    }

    private boolean isValid(int x, int y) {
      Set<Character> visited = new HashSet<>();

      String hour = String.valueOf(x), minute = String.valueOf(y);

      if (hour.length() == 1) {
        visited.add('0');
        visited.add(hour.charAt(0));
      } else {
        visited.add(hour.charAt(0));
        visited.add(hour.charAt(1));
      }

      if (minute.length() == 1) {
        visited.add('0');
        visited.add(minute.charAt(0));
      } else {
        visited.add(minute.charAt(0));
        visited.add(minute.charAt(1));
      }

      return visited.size() == 4;
    }
}

1545 · Last Closest Time

Description
Given a string representing the time"12:34"(This is a legal input data), return the most recent time within the first 24 hours.If the input is illegal, return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.The given String represents a time of 24 hours
2.The string contains only numeric characters and ":"
3. The length of string is 5

Example
Example 1

Input: "00:00" 
Output "23:59"

public class Solution {
    /**
     * @param time: 
     * @return: return a string represent time
     */
    public String lastTime(String time) {
        // Write your code here
        if(!time.contains(":")){
            return "-1";
        }
        String[] str = time.split(":");
        int minutes = Integer.valueOf(str[1]);
        int times = Integer.valueOf(str[0]);
        if(minutes<0 || minutes>=60 || times<0 || times>=24 || str[1].length()!=2 || 
        str[0].length()!=2){
            return "-1";
        }
        if(minutes==0){
            minutes = 59;
            if(times==0){
                times = 23;
            }else{
                times--;
            }
        }else{
            minutes--;
        }
        return (times<10?"0":"")+times+":"+minutes;
    }
}

1554 · LastTime Norepeat

Description
Give a String, representing the time, such as "12:34"(This is a legal input data). Find the most recent time in the last 24 hours and don't include duplicate numbers. If it is the samllest "00:00", the reply is the largest "23:59".If the input is illegal, return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.The given String represents a time of 24 hours
2.The string contains only numeric characters and ":"
3. The length of string is 5

Example
Example 1

Input: "00:00"
Output: "23:59"

public class Solution {
    /**
     * @param time: 
     * @return: return a string represents time
     */
    public String lastTime(String time) {
        // Write your code here      
        if (time == null || time.length() == 0 || time.length() != 5) {
          return "-1";
        }
        if (time.charAt(2) != ':') {
          return "-1";
        }

        int hour = (time.charAt(0) - '0') * 10 + (time.charAt(1) - '0');
        int minute = (time.charAt(3) - '0') * 10 + (time.charAt(4) - '0');        
        
        if (hour > 23 || minute > 59) {
          return "-1";
        }

        int minutes = 60 * hour + minute;
        minutes--;
        if (minutes < 0) {
          minutes += 24 * 60;
        }
        while (!isValid(minutes)){
          minutes--;
          if (minutes < 0) {
            minutes += 24 * 60;
          }
        }

        hour = (minutes / 60 ) % 24;
        minute = minutes % 60;

        return String.format("%02d:%02d",hour,minute);
    }
    
    private boolean isValid(int minutes) {
      int hour = (minutes / 60 ) % 24;
      int minute = minutes % 60;

      int num1 = hour / 10;
      int num2 = hour % 10;
      int num3 = minute / 10;
      int num4 = minute % 10;
      return (num1 != num2 && num1 != num3  && num1 != num4 
      && num2 != num3 && num2 != num4 && num3 != num4);
    }
}

1571 · Top K GPA

Description
Given a List, each element in the list represents a student's StudentId and GPA. Return the StudentId and GPA of the top K GPA,in the original order.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1.if k > the number of students, Return all student information.
2.Both StudentId and GPA are String.
3.The GPA between two students is different

Example
Example 1

Input:
List = [["001","4.53"],["002","4.87"],["003","4.99"]]
k = 2
Output:[["002","4.87"],["003","4.99"]]

public class Solution {
    /**
     * @param list: the information of studnet
     * @param k:
     * @return: return a list
     */
    public List<List<String>> topKgpa(List<List<String>> list, int k) {
        // Write your code here
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i).get(1);
            nodes.add(new Node(i, Double.parseDouble(s)));
        }
        nodes.sort(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return o2.score > o1.score ? 1 : -1;
            }
        });
        List<Node> subList = nodes;
        if (k < nodes.size()) {
            subList = nodes.subList(0, k);
        }
        subList.sort(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return o1.index - o2.index;
            }
        });
        List<List<String>> res = new ArrayList<>();
        for (Node node : subList) {
            res.add(list.get(node.index));
        }
        return res;
    }

    class Node {
        int index;
        double score;

        public Node(int index, double score) {
            this.index = index;
            this.score = score;
        }
    }
}

1577 · Sum of leaf nodes

Description
Given a binary tree, find the sum of all leaf nodes.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Input:
{1,2,3,4,5}
Output : 12

Explanation：
      1
     / \
   2   3
  / \     
4   5    
Leaf nodes are nodes without children. 4+5+3 = 12


/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * @param root: 
     * @return: the sum of leafnode
     */
    private int sum = 0;
    public int sumLeafNode(TreeNode root) {
        helper(root);
        return sum;
    }

    public void helper(TreeNode root) {
        if(root == null) return;
        if(root.left == null && root.right == null) {
            sum += root.val;
            return;
        }
        helper(root.left);
        helper(root.right);
    }
}

1579 · Alphabetic string calculation

Description
In the world of the moon, ordinary people will solve a problem. This problem is a string of English letters, each time you can delete an English letter.
Try to delete at least as many times as possible to make each letter appear differently

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Only lowercase letters exist.The length of the English string will not exceed 100,000

Example
Example 1:

Input: "aaabbb"
Output: 1
Explanation: Because there are three 'a' numbers and three 'b' numbers, deleting one 'a' or one 'b' can satisfy the meaning of the question.

public class Solution {
    /**
     * @param aString: letter string
     * @return: the Minimum times
     */
    public int Kstart(String str) {
        // write your code here
         if (str == null || str.length() == 0) return 0;

        int[] arr = new int[256];

        for (int i = 0; i < str.length(); i++) {

            arr[str.charAt(i)]++;
        }

        Arrays.sort(arr);
        int len = 0;
        for (int i = arr.length - 2; i >= 0; i--) {

            if (arr[i] == 0) break;
            while (arr[i] >=  arr[i + 1]) {
                arr[i] -= 1;
                len += 1;
                if (arr[i] == 0) break;

            }
        }
        return len;
    }
}

1580 · Transition String

Description
Give a startString, an endString, ask if you can transfer from start to end through a series of independent transformations. The rule is to have only 26 lowercase letters, and only one type of letter can be changed per operation. For example, if you change a to b, then all a in the start string must be b. For each type of character you can choose to convert or not convert, the conversion must be conducted between one character in startString and one character in endString. Return true or false.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Given : startString = "abc" ; endString = "cde"
Return : True

Explanation : 
a->b, b->d, c->e
"abc"->"abe"->"ade"->"cde"
but if you convert to "cbc" first, you will never convert to "cde" again.


public class Solution {
    /**
     * @param startString: a string
     * @param endString: a string
     * @return: if startString can be converted to endString
     */
    public boolean canTransfer(String startString, String endString) {
        // write your code here
        if (startString == null && endString == null) {
          return true;
        }

        if (startString.length() == 0 && endString.length() == 0) {
          return true;
        }

        if (startString.length() != endString.length()) {
          return false;
        }

        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < startString.length(); i ++) {
          if (!map.containsKey(startString.charAt(i))) {
            map.put(startString.charAt(i), endString.charAt(i));
          } else {
            if (map.get(startString.charAt(i)) != endString.charAt(i)) {
              return false;
            }
          }
        }

        // List<Character> heads = new ArrayList<>();
        // for (Character key : map.keySet()) {
        //   char value = map.get(key);

        //   if (map.containsKey(value)) {
        //     heads.add(value);
        //   }
        // }

        for (Character key : map.keySet()) {
          char from = key, to = map.get(key);
          while (map.containsKey(to)) {
            if (map.get(to) == from) {
              break;
            }

            if (map.get(to) == key) {
              return false;
            }
            from = to;
            to = map.get(from);
          }
        }

        return true;

        // Set<Integer> visited = new HashSet<>();
        // return dfs(heads, visited, map, startString.toCharArray(), endString.toCharArray());
    }

    public boolean dfs(List<Character> heads, Set<Integer> visited, Map<Character, Character> map, char[] startS, char[] endS) {
      if (visited.size() == heads.size()) {
        return true;
      }

      for (int i = 0; i < heads.size(); i ++) {
        if (visited.contains(i)) {
          continue;
        }

        char from = heads.get(i), to = map.get(from);
        char[] nextS = transform(startS, from, to);
        visited.add(i);

        if (visited.size() == heads.size()) {
          return true;
        }
        
        if (getCount(startS, to) == getCount(endS, to)) {
            dfs(heads, visited, map, nextS, endS);
        }

        visited.remove(i);
      }

      return false;
    } 

    public char[] transform(char[] startS, char from, char to) {
      char[] res = new char[startS.length];

      for (int i = 0; i < startS.length; i ++) {
        res[i] = startS[i] == from ? to : startS[i];
      }

      return res;
    }

    private int getCount(char[] chars, char target) {
      int count = 0;
      for (char ch : chars) {
        if (ch == target) {
          count ++;
        }
      }

      return count;
    }
}


1619 · Candy II

Description
There are N children standing in a line. Each child is assigned a rating value.

You are giving candies to these children subjected to the following requirements:

Each child must have at least one candy.
Children with a higher rating get more candies than their neighbors.
Children with the same rating and are located next to each other get the same candies.
What is the minimum candies you must give?

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

Input: 4 7 8 1 6 6 2
Output: 12

Explanation: 1 + 2 + 3 + 1 + 2 + 2 + 1 = 12


public class Solution {
    /**
     * @param ratings: rating value of each child
     * @return: Return the minimum candies you must give.
     */
    public int candyII(int[] ratings) {
        // Write your code here
        int len = ratings.length;
        if(len == 0)
        {
            return 0;
        }
        int[] left = new int[len];
        for(int i = 0; i < len; i++)
        {
            if(i == 0 || ratings[i] < ratings[i-1]) 
            {
                left[i] = 1;
            }
            else if(ratings[i] == ratings[i-1]) 
            {
                left[i] = left[i-1];
            }
            else 
            {
                left[i] = left[i-1]+1;
            }
        }
        
        int res = 0;
        for(int i = len-2; i >= 0; i--)
        {
            if(ratings[i] > ratings[i+1]) 
            {
                left[i] = Math.max(left[i], left[i+1]+1);
            }
            else if(ratings[i] == ratings[i+1]) 
            {
                left[i] = Math.max(left[i], left[i+1]);
            }
            res += left[i];
        }
        res += left[len-1];
        return res;
    }
}

1626 · Salary Adjustment

Description
Given a list of salaries, find the smallest cap which makes the sum of adjusted salary be equal to or larger than the given target. cap is defined as: if the current salary is smaller than cap, then cap is used as the new salary, otherwise keep the original salary

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of the list does not exceed 100000100000
The salaries do not exceed 1000010000

Example
Example 1：

Give `a=[1,2,3,4],target=13`,
return `3`.
Input:
1 2 3 4
13
Output:3

Explanation:
If cap=3, the list will change into [3,3,3,4].


public class Solution {
    /**
     * @param a: the list of salary
     * @param target: the target of the sum
     * @return: the cap it should be
     */
    public int getCap(int[] a, int target) {
        // Write your code here.
        Arrays.sort(a);
        int sum = 0;
        for (int x : a) {
            sum += x;
        }
        int gap = target - sum;
        int avg = gap / a.length + 1;
        int low = a[0], high = a[a.length - 1] + avg;
        return binarySearch(a, low, high, gap);
    }

    private int binarySearch(int[] a, int start, int end, int target) {
        int left = start, right = end;
        while (left + 1 < right) {
            int mid = (left + right)/2;
            int i = 0;
            int sum = 0;
            for (i = 0; i < a.length; i++) {
                if (a[i] < mid) {
                    sum += mid - a[i];
                } else {
                    break;
                }
            }
            if (sum < target) {
                left = mid;
            } else {
                right = mid;
            }
        }
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] < left) {
                sum += left - a[i];
            } else {
                break;
            }
        }
        if (sum >= target) {
            return left;
        }
        return right;
    }
}

1627 · Word Segmentation

Description
Given a long string S, only include normal English words, words are separated by a single space, and give you a positive integer. Please divide the string into several lines and the number of lines is minimum. Requirement 1: You can only wrap between words. The same word cannot be separated; Requirement 2: Each line cannot be more than k characters after the division

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


String length does not exceed 100000
Data guarantee legal

Example
Example 1:

Input:
aaaa bbb cccc ddd ee ff ggggg
8
Output:
aaaa bbb
cccc ddd
ee ff
ggggg


public class Solution {
    /**
     * @param s: the string
     * @param k: the k
     * @return: the answer
     */
    public String[] wordSegmentation(String s, int k) {
        // Write your code here
        int start = 0;
        List<String> res = new ArrayList<>();
        while (true) {
            int end = start + k;
            if (end >= s.length()) {
                res.add(s.substring(start));
                break;
            } else {
                int index = s.lastIndexOf(" ", end);
                res.add(s.substring(start, index));
                start = index + 1;
            }
        }
        String[] arr = new String[res.size()];
        for (int i = 0; i < res.size(); i++) {
            arr[i] = res.get(i);
        }
        return arr;
    }

    public static void main(String[] args) {
        new Solution().wordSegmentation("x zclg qnrh sic zzsl jfwuor rnbh zyqxug okbngeu glvvtni", 7);
    }
}

1628 · Driving problem

Description
There is a road with a length L and a width W. There are some circular obstacles in the road. The radius is 1, there is a circular car with a radius of 2. Ask if the car can pass this road. You can think of the road as a rectangle on two-dimensional coordinates. The four points are (0,0), (0,W), (L,0), (L,W). Now you need to start from x=0 To x=L, contact with obstacles is not allowed, and all parts of the car are between y=0 and y=W, contact is not allowed.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The coordinates of the obstacle can be floating point numbers
The car can't drive out of the road
The number of obstacles does not exceed 1,000.
Obstacles can intersect or overlap

Example
Example 1:

Input:
8 8
Output:
yes

Explanation:
Given `L=8`,`W=8`, the obstacle coordinates are `[[1,1],[6,6]]`. Return `yes`.
The center of the car can go from (0,5) to (2,5) to (5,2) to (8,2), so return yes.

public class Solution {
    /**
     * @param L: the length
     * @param W: the width
     * @param p:  the obstacle coordinates
     * @return: yes or no
     */
    public String drivingProblem(int L, int W, double[][] p) {
        // Write your code here.

        if (W < 5) {
            return "no";
        }

        int obstacleCount = p.length;

        // + 2 = 左右边界
        UnionFind unionFind = new UnionFind(obstacleCount + 2); 

        for (int i = 0; i < obstacleCount; i++) {
            // 障碍到左边界距离不够车穿过
            if (p[i][1] - 0.0 <= 5.0) {
                unionFind.merge(i, obstacleCount);
            }

            // W is index also, not length
            if (W - p[i][1] <= 5.0) {
                unionFind.merge(i, obstacleCount + 1);
            }
        }

        for (int i = 0; i < obstacleCount; i++) {
            for (int j = i + 1; j < obstacleCount; j++) {
                double distance = Math.pow(p[i][0] - p[j][0], 2) + Math.pow(p[i][1] - p[j][1], 2);

                if (distance < 36.0) {
                    unionFind.merge(i, j);
                }
            }
        }


        return unionFind.isConnected(obstacleCount, obstacleCount + 1) ? "no" : "yes";
    }

    class UnionFind {
        int[] fathers;

        public UnionFind(int count) {
            fathers = new int[count];
            for (int i = 0; i < count; i++) {
                // add()
                fathers[i] = i;
            }
        }

        public void merge(int a, int b) {
            int root1 = find(a);
            int root2 = find(b);

            if (root1 != root2) {
                fathers[root1] = root2;
            }
        }

        public int find(int num) {
            int root = num;

            // not != num, 否则num1 -> num2, num2 -> itself，会死循环 
            while (fathers[root] != root) {
                root = fathers[root];
            }

            while (root != num) {
                int originalFather = fathers[num];
                fathers[num] = root;
                num = originalFather;
            }

            return root;    
        }

        public boolean isConnected(int a, int b) {
            int root1 = find(a);
            int root2 = find(b);
            return root1 == root2;
        }
    }
}

1629 · Find the nearest store

Description
There are some stores and houses on a street. Please find the nearest store to each house.

You are given two array represent the location of the stores and houses.

Return an array with the location of the nearest store to each house. If there are two stores that are the same distance from the house return the left one.

Tips: 
1. There are multiple stores and houses in the same location. And the locations in array are disordered.
2. The array of location must not be empty.
Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example 1:

stores: [4,7,8,1,6,6,2]
houses: [1, 8, 5]
return: [1, 8, 4]

Input:
4 7 8 1 6 6 2
1 8 5
Output:
1 8 4

public class Solution {
    /**
     * @param stores: The location of each store.
     * @param houses: The location of each house.
     * @return: The location of the nearest store to each house.
     */
    public List<Integer> findNearestStore(List<Integer> stores, List<Integer> houses) {
        Collections.sort(stores);
	   List<Integer> res = new ArrayList<>();
	   if(stores == null || stores.size() == 0){
		   return res;
	   }
	   for(int house : houses){
		   int closeStore = findStore(stores,house);
		   res.add(closeStore);
	   }
	   return res;
    }

    private int findStore(List<Integer> stores,int house){
	    int left = 0, right = stores.size() - 1;
	    while(left + 1 < right){
		    int mid = left + (right - left)/2;
		    if(stores.get(mid) == house){
			    return house; 
		    }else if(stores.get(mid) > house){
			    right = mid;
		    }else{
			    left = mid;
		    }
	    }
	    return Math.abs(stores.get(left) - house) <= Math.abs(stores.get(right) - house) ? stores.get(left) : stores.get(right);
    }
}

1630 · Interesting String

Description
There is now a string s consisting of only numbers and lowercase letters. If the string s is interesting, then s must be split into several substrings, each substring satisfies the beginning of the number, and the number represents the number of characters after it. For example, s = "4g12y6hunter", we can divide it into "4g12y" and "6hunter", so the string is interesting.
If s is an interesting string, output "yes", otherwise output "no"

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


the length of string no more than 10000
Example
Example 1:

s = "124gray6hunter",return "yes"
Input:
124gray6hunter
Output:
yes

Explanation:
we can divide it into "12", "4gray", "6hunter".

public class Solution {
    /**
     * @param s: the string s
     * @return: check if the string is interesting
     */
    public String check(String s) {
        // Write your code here
        if (s.length() == 0) {
            return "yes";
        }

        int n = s.length(), currIndex = 0, currNum = 0;
        while (currIndex < n && Character.isDigit(s.charAt(currIndex))) {
            currNum = 10 * currNum + s.charAt(currIndex++) - '0';
            if (currNum > n - currIndex) {
                return "no";
            }
            if (currNum == n - currIndex) {
                return "yes";
            } else {
                if (check(s.substring(currIndex+currNum, n)) == "yes") {
                    return "yes";
                }
            }
        }
        return "no";
        
    }
}

1631 · Interesting Subarray

Description
If there are no more than two values in a subarray, the subarray is interesting. Given an array a, find the longest interesting subarray. Output the maximum length.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


the length of a is no more than 1e5
0 <= a[i] <= 1e3
Example
Example 1:

a = [1,2,1,3]
return 3
Input:
1 2 1 3
Output:
3

public class Solution {
    /**
     * @param a: the array a
     * @return: return the maximum length
     */
    public int maxLen(int[] A) {
        // Write your code here
        int cur = 0,max = 0,a = 0,b = 0,count_b = 0;
        for(int num: A){
            if(num == a||num == b)
            {
                cur++;
            }
            else
            {
                cur = count_b+1;
            }
            if(num == b)
            {
                count_b ++;
            }
            else
            {
                count_b = 1;
                a = b;
                b = num;
            }
            max = Math.max(max,cur);
        }
        return max;
    }
}

1634 · Secret Word

Description
Given a secrect word, and an encoding rule as follows: Transform each letter in the secret, different letters can not be changed to the same letter. Such as banana -> xyzyzy, but banana can not become xyyyyy, because there is no way to decode back.
Now input a very long string, and it is required to determine whether a substring exists in the string can be transformed by the above encoding rule. If exists, output string "yes", otherwise output "no".

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of the secret word does not exceed 1010.

The length of the string does not exceed 1000010000.

The string only consists of lowercase.

Example
Examlpe 1:

Give `s="abcabcd"`, `word="xyzxyz"`, return `yes`
Input:
abcabcd
xyzxyz
Output:
yes

Explaination:
"x" can transfer to "a", "y" can transfer to "b" and "z" can transfer to "c".

public class Solution {
    /**
     * @param s: the long string
     * @param word: the secrect word
     * @return: whether a substring exists in the string can be transformed by the above encoding rule
     */
    public String getAns(String s, String word) {
        // Write a code here
        if(findSecretWord(s,word)){
            return "yes";
        }
        return "no";
    }
    
    public boolean findSecretWord(String encoded, String secret) {
        int count = getUniqueCharsCount(secret);
        int[] window = new int[128];
        int distinct = 0; // number of distinct chars in the window of length = secret.length
        int len = secret.length();

        for (int i = 0; i < len; i++) {
            char c = encoded.charAt(i);
            window[c]++;
            if (window[c] == 1) distinct++;
        }

        if (distinct == count && isIsomorphic(encoded.substring(0, len), secret)) {
            return true;
        }

        for (int i = len; i < encoded.length(); i++) {
            // add new char
            char c = encoded.charAt(i);
            window[c]++;
            if (window[c] == 1) distinct++;

            // remove from window
            c = encoded.charAt(i - len);
            window[c]--;
            if (window[c] == 0) distinct--;

            if (distinct == count && isIsomorphic(encoded.substring(i - len + 1, i + 1), secret)) {
                return true;
            }
        }

        return false;
    }

     private int getUniqueCharsCount(String str) {
        boolean[] charset = new boolean[128];
        int unique = 0;
        for (char c : str.toCharArray()) {
            if (!charset[c]) unique++;
            charset[c] = true;
        }
        return unique;
    }

    private boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()) return false;

        int[] map1 = new int[128];
        int[] map2 = new int[128];

        for (int i = 0; i < s.length(); i++) {
            char c1 = s.charAt(i);
            char c2 = t.charAt(i);

            if (map1[c1] != map2[c2]) return false;
            map1[c1] = i + 1;
            map2[c2] = i + 1;
        }

        return true;
    }
}


1641 · Max Remove Order

Description
Give a m * n board with a value of 0 or 1. At each step we can turn a 1 into 0 if there is another 1 in the same row or column. Return the max number of 1 we can turn into 0.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


n and m do not exceed 5050

Example
Example 1:

Input：[[1,1,1,1],[1,1,0,1],[1,0,1,0]]
Output：8
Explanation:
In the board
1, 1, 1, 1
1, 1, 0, 1
1, 0, 1, 0
We can remove 1 from right to left, from bottom to top, until there is only one 1 at (0, 0). Totally 8 removed.


public class Solution {
    /**
     * @param mp: the board
     * @return: the max number of points we can remove
     */
int [] fa= new int [5005];
    public int find(int x) {
        if(fa[x] == x) 
          return x;
        return fa[x] = find(fa[x]);
    } 
    public void unity(int x, int y) {
        x = find(x);
        y = find(y);
        fa[x] = y;
    }
    public int getAns(int[][] mp) {
        // Write your code here.
        int cnt = 0;
        int [] line = new int[3000];	//保存某行为1的一个点的编号
        int [] column = new int [3000];		//保存某列为1的一个点的编号
        int m = mp.length;					//获取行数
        int n = mp[0].length;				//获取列数
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++) {
                if(mp[i][j] == 1) {				
                    fa[i*n+j+1] = i*n+j+1;
                    line[i] = i*n+j+1;			//该点为1，给行存入编号
                    column[j] = i*n+j+1;		//该点为1，给列存入编号
                    cnt ++;
                }
            }
        for(int i = 0; i < m; i++)
            for(int j = 0;j < n; j++) {
               if(mp[i][j] == 1) {
                   unity(i * n + j + 1, line[i]);
                   unity(i * n + j + 1, column[j]);
               }        
        }
        for(int i = 0; i < m; i++)
            for(int j = 0; j<n; j++) {
                if(mp[i][j] == 1 && fa[i * n + j + 1] == i * n + j + 1)
                         cnt --;				//统计祖先数量
            }
        return cnt;
    }
}

1642 · Query String

Description
Give a binary string str and an integer n to check if the substring of the string contains all binary representations of non-negative integers less than or equal to the given integer.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


String length does not exceed 100,000
n does not exceed 100,000
Binary starts at 0 and does not require leading zeros

Example
Example 1:

Input："0110"，n=3
Output："yes"
Explanation：
The substring of str contains "0", "1", "10", "11".


public class Solution {
    /**
     * @param str: the string
     * @param n: the n
     * @return: yes or no
     */
    public String queryString(String str, int n) {
        // Write your code here.
        boolean[] f = new boolean[n + 1];
        int m = str.length();
        for (int i = 0; i < m; i++) {
            char ch = str.charAt(i);
            if (ch == '0') {
                f[0] = true;
            } else {
                int val = 0;
                for (int j = i; j < m; j++) {
                    val = (val << 1) | (str.charAt(j) - '0');
                    if (val > n) {
                        break;
                    }
                    f[val] = true;
                }
            }
        }
        for (boolean flag : f) {
            if (!flag) {
                return "no";
            }
        }
        return "yes";
    }
}

1643 · Pick Fruits

Description
Xiaohong went to the orchard to pick fruit. There are 2 baskets that can hold countless fruits, but each baskert can only hold one type of fruit. Start from the tree at any position and pick it to the right. Stop picking when one of the following two conditions occurs, 1. encountered the third type of fruit, no basket can be put, 2. meet the end. Returns the maximum number of fruits that can be picked.The fruit array is represented by arr.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


The length of the array does not exceed 100,000

Example
Example 1:

Input：[1,2,1,3,4,3,5,1,2]
Output：3
Explanation：
Select [1, 2, 1] or [3, 4, 3]. The length is 3.

public class Solution {
    /**
     * @param arr: the arr
     * @return: the length of the longset subarray
     */
    public int pickFruits(int[] arr) {
        // Write your code here.
        int sum = 0; //record fruit type count
        int r = 0;
        int result = 0;
        int[] count = new int[256];
        for(int l =0; l< arr.length; l++){       
            while(r< arr.length && sum <= 2){
                count[arr[r]]++;
                if(count[arr[r]] == 1){
                    sum++;
                }
                r++;
            }
            if(r == arr.length && sum <= 2){
                result = Math.max(result,r-l);
            }
            result = Math.max(result,r -l -1);
            count[arr[l]]--;
            if(count[arr[l]] == 0){
                sum--;
            }
        }
        return result;
    }
}


1644 · Plane Maximum Rectangle

Description
Given a n point on the two-dimensional coordinate system, output the maximum area of the rectangle that consisting of four points. If it cannot form a rectangle, output 0

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


n <= 1000
0 <= x,y <= 1000
each side of the rectangle is required to be perpendicular to the X or Y axis
Example
Example 1:

Input：[[4,4],[3,2],[3,1],[1,3],[2,1],[1,4],[4,1],[4,4],[1,2]]
Output：0
Explanation：
No four points can form a rectangle

public class Solution {
    /**
     * @param a: the points
     * @return: return the maximum rectangle area
     */
    public int getMaximum(int[][] a) {
        // write your code here
        if (a == null || a.length == 0) {
            return 0;
        }
        
        boolean[][] map = new boolean[1001][1001];
        
        for (int i = 0; i < a.length; i++) {
            map[a[i][0]][a[i][1]] = true;
        }
        
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (map[a[j][0]][a[i][1]] && map[a[i][0]][a[j][1]]) {
                    int area = (a[i][0] - a[j][0]) * (a[i][1] - a[j][1]);
                    res = Math.max(res, area);
                }
            }
        }
        return res;
    }
}

1645 · Least Subsequences

Description
Given an array with n integers. Splitting it into subsequences of strictly descending order.
Output the minimum number of subsequences you can get by splitting.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example1

Input: [5,2,4,3,1,6]
Output: 3
Explanation:
You can split this array into : [5,2,1], [4,3], [6]. And there are 3 subsequences you get.﻿ 
Or you can split it into [5, 4, 3],[2,1], [6] also 3 subsequences.
But [5, 4, 3, 2, 1], [6] is not legal because [5, 4, 3, 2, 1] is not a subsuquence of the original array.

public class Solution {
    /**
     * @param arrayIn: The original array.
     * @return: Count the minimum number of subarrays.
     */
    public int LeastSubsequences(List<Integer> arrayIn) {
        // Write your code here.
        // patient sorting
        if (arrayIn.size() == 0) {
            return 0;
        }

        List<Integer> buckets = new ArrayList<>();
        buckets.add(arrayIn.get(0));
        for(int i = 1; i < arrayIn.size(); i++){
            int cur = arrayIn.get(i);
            int position = binary_search(cur, buckets);
            // buckets 里的元素全部小于等于 target
            if (position == buckets.size()){
                buckets.add(cur);
            } else {
                // 从大于的那一个开始
                buckets.set(position, cur);
            }
        }

        return buckets.size();
    }

    private int binary_search(int target, List<Integer> buckets){
        int start = 0;
        int end = buckets.size() - 1;

        if (buckets.get(start) > target){
            return start;
        }

        while(start + 1 < end){
            int mid = start + (end - start) / 2;
            if(buckets.get(mid) > target){
                end = mid;
            }
            else{
                start = mid;
            }
        }
        
        if (buckets.get(end) <= target){
            return end + 1;
        }

        return start + 1;
    }
}

1646 · CheckWords

Description
Given a word s, and a string set str. This word removes one letter at a time until there is only one letter in the word. Is there a sort of deletion in which all words are in str. For example, the word is 'abc', the string set is {'a', 'ab', 'abc'}, if the order of deletion is 'c', 'b', and 'abc', 'ab', 'a' are all in the collection, so it is eligible.
Output whether this combination meets the condition.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1<=|str[i]|,|s|<=30
1<=the number of strings in str<=100

Example
Example 1:

Input：s="abc"，str=["abc","ac","c"]
Output：true
Explanation:
First "abc" is in `str`
Delete 'b', "ac" is  in `str`
Delete 'a', "c" is in `str`

public class Solution {
    /**
     * @param s: 
     * @param str: 
     * @return: Output whether this combination meets the condition
     */

    Map<String, Boolean> map = new HashMap();

    public boolean checkWord(String s, String[] str) {
        // Write your code here
        Set<String> set = new HashSet();
        for (String word : str) {
            set.add(word);
        }
        return check(s, set);
    }

    private boolean check(String s, Set<String> set) {
        if (s.length() == 0)    return true;
        if (map.containsKey(s))    return map.get(s);
        if (!set.contains(s))    return false;
        // if (check(s.substring(1), set)) {
        //         map.put(s, true);
        //         return true;
        //     }
        for (int i = 0; i < s.length(); i++) {
            if (check(s.substring(0, i) + s.substring(i + 1), set)) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}

1737 · Fruit Into Baskets

Description
In a row of trees, the i-th tree produces fruit with type tree[i].

You start at any tree of your choice, then repeatedly perform the following steps:

1.Add one piece of fruit from this tree to your baskets. If you cannot, stop.
2.Move to the next tree to the right of the current tree. If there is no tree on the right, stop.

Note that you do not have any choice after the initial choice of starting tree: you must perform step 1, then step 2, then back to step 1, then step 2, and so on until you stop.

You have two baskets, and each basket can carry any quantity of fruit, but you want each basket to only carry one type of fruit each.

What is the total amount of fruit you can collect with this procedure?

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= tree.length <= 40000
0 <= tree[i] < tree.length
Example
Example 1:

Input: [1,2,1]
Output: 3
Explanation: We can collect [1,2,1].

public class Solution {
    /**
     * @param tree: The type of fruit
     * @return: The total amount of fruit you can collect.
     */
    public int totalFruit(int[] tree) {
        int[] basket = new int[2];
        int f1 = 0, f2 = -1, res = 1;
        int b1 = 1, b2 = 0;
        for(int i = 1; i < tree.length; i++) {
            if(tree[f1] == tree[i]) {
                b1++;
            } else if(f2 == -1) {
                f2 = i;
                b2 = 1;
            } else if(tree[f2] == tree[i]) {
                b2++;
            } else {
                if(f1 < f2) {
                    f1 = i;
                    b1 = 1;
                } else {
                    f2 = i;
                    b2 = 1;
                }
            }
            res = Math.max(res, b1+b2);
        }
        return res;
    }
}

1808 · Minimum Domino Rotations For Equal Row

Description
In a row of dominoes, A[i] and B[i] represent the top and bottom halves of the i-th domino. (A domino is a tile with two numbers from 1 to 6 - one on each half of the tile.)

We may rotate the i-th domino, so that A[i] and B[i] swap values.

Return the minimum number of rotations so that all the values in A are the same, or all the values in B are the same.

If it cannot be done, return -1.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


1 <= A[i], B[i] <= 6
2 <= A.length == B.length <= 20000
Example
图片

Example1:
Input: A = [2,1,2,4,2,2], B = [5,2,6,2,3,2]
Output: 2
Explanation: 
The first figure represents the dominoes as given by A and B: before we do any rotations.
If we rotate the second and fourth dominoes, we can make every value in the top row equal to 2, as indicated by the second figure.

public class Solution {
    /**
     * @param A: a integer array
     * @param B: a integer array
     * @return: Return the minimum number of rotations
     */
    public int minDominoRotations(int[] A, int[] B) {
        // write your code here
        int res = Integer.MAX_VALUE;
        int n = A.length;
        int[] countA = new int[7];
        int[] countB = new int[7];
        int[] same = new int[7];
        for (int i = 0; i < n; i++) {
            if (A[i] == B[i]) {
                same[A[i]]++;
            }
            countA[A[i]]++;
            countB[B[i]]++;
        }

        for (int i = 1; i < 7; i++) {
            if (countA[i] + countB[i] - same[i] == n) {
                if (countA[i] > countB[i]) {
                    res = Math.min(res, n - countA[i]);
                } else {
                    res = Math.min(res, n - countB[i]);
                }
                //res = Math.min(res, n - Math.max(countA[i], countB[i]));
            }
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

1811 · Find Maximum Gold

Description
M * N grid, each grid has a number representing the number of golds in that grid. If it is empty, the number is 0.. Find a path to get the maximum sum of golds under conditions: 1. 1. A road can't walk a repeating grid 2. Never cross a zero.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


Example
Example:
Input:
grid = [[1,2,0,0],
       [0,0,2,2]]
Output: 4


public class Solution {
    int[] dx = new int[] {1, -1, 0, 0};
    int[] dy = new int[] {0, 0, 1, -1};
    int maxResult = 0;
    /**
     * @param grids: a integer two-dimensional array
     * @return: return the maximum sum of golds 
     */
    public int FindMaximumGold(int[][] grids) {
        if (grids == null || grids.length == 0 || grids[0].length == 0) return 0;
        boolean[][] visited = new boolean[grids.length][grids[0].length];
        for (int i = 0; i < grids.length; i++) {
            for (int j = 0; j < grids[0].length; j++) {
                if (grids[i][j] != 0) {
                    visited[i][j] = true;
                    dfs(i, j, grids, visited, grids[i][j]);
                    visited[i][j] = false;
                }
            }
        }
        return maxResult;
    }

    private void dfs(int i, int j, int[][] grids, boolean[][] visited, int currentCount) {
        // go to four direction
        for (int n = 0; n < 4; n++) {
            int newI = i + dx[n];
            int newJ = j + dy[n];
            if (isValid(newI, newJ, grids, visited)) {
                maxResult = Math.max(maxResult, grids[newI][newJ] + currentCount);
                visited[newI][newJ] = true;
                dfs(newI, newJ, grids, visited, grids[newI][newJ] + currentCount);
                visited[newI][newJ] = false;
            }
        }
    }

    private boolean isValid(int i, int j, int[][] grids, boolean[][] visited) {
        if (i < 0 || i >= grids.length) return false;
        if (j < 0 || j >= grids[0].length) return false;
        return grids[i][j] != 0 && !visited[i][j];
    }
}

1868 · Find the Number of “Balance” in the String

Description
You are given a string S consisting of N lowercase English letters. A split of string S is a partition into two non-empty strings S1 and S2 such that S1+S2=S(where the "+" operator means string concatenation).If there is a partition where the number of distinct letters in S1 equals the number of distinct letters in S2, it is called a "Balance". Please find out how many "Balance" in S.

Wechat reply 【Google】 get the latest requent Interview questions. (wechat id : jiuzhang0607)


the length N of the string S is an integer within the range [2, 200000]
string S consists only of lowercase letters(a-z)
Example
Example 1:

Input: 
S = "abaca"
Output:  
2
Explanation: 
S has the following possible splits:("a","baca"),("ab","aca"),("aba","ca"),("abac","a") with the numbers of distinct letters respectively:(1,3),(2,2),(2,2),(3,1),so the only valid splits are ("ab","aca") and ("aba","ca"), and therefore the result is 2.


public class Solution {
    /**
     * @param S: the string
     * @return: return the number of “Balance” in the string
     */
    public int find(String S) {
        int n = S.length();
        int[] leftDistinctCounts = new int[n];
        boolean[] hasSeenChar = new boolean[26];
        int totalDistinctCount = 0;
        for (int i = 0; i < n; i++) {
            int charIndex = S.charAt(i) - 'a';
            if (!hasSeenChar[charIndex]) {
                totalDistinctCount++;
                hasSeenChar[charIndex] = true;
            }
            leftDistinctCounts[i] = totalDistinctCount;
        }
        int[] rightDistinctCounts = new int[n];
        hasSeenChar = new boolean[26];
        totalDistinctCount = 0;
        for (int i = n - 1; i >= 0; i--) {
            int charIndex = S.charAt(i) - 'a';
            if (!hasSeenChar[charIndex]) {
                totalDistinctCount++;
                hasSeenChar[charIndex] = true;
            }
            rightDistinctCounts[i] = totalDistinctCount;
        }
        int balanceCount = 0;
        for (int i = 0; i < n - 1; i++) {
            if (leftDistinctCounts[i] == rightDistinctCounts[i + 1]) {
                balanceCount++;
            }
        }
        return balanceCount;
    }
}


