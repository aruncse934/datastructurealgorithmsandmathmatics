package FULL_ARCHIVE.OOD;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

@AllArgsConstructor
class PairPosition {
       private int start;
       private int end;

}


class Entities {


    static HashMap<Integer,Integer> snakes;
    static HashMap<Integer,Integer> ladders;
    static HashMap<Integer,String> players;
    static Entities instance = null;

    public Entities(){
        snakes = new HashMap<>();
        ladders = new HashMap<>();
        players = new HashMap<>();
    }

    public static Entities getInstance() {
        if(instance == null) {
            instance = new Entities();
        }
        return instance;
    }


    public void setSnake(int startPosition, int endPosition) {
        snakes.put(startPosition,endPosition);
    }

    public HashMap<Integer, Integer> getSnakes() {
        return snakes;
    }

    public void setLadder(int startPosition, int endPosition) {
        ladders.put(startPosition,endPosition);
    }

    public HashMap<Integer, Integer> getLadders() {
        return ladders;
    }

    public void setPlayer(int index,String playerName) {
        players.put(index,playerName);
    }

    public HashMap<Integer, String> getPlayers() {
        return players;
    }


}

class Dice {
    private int numberOfDice;
    private static int MIN = 1;
    private Random random;
    public Dice(int numberOfDice) {
        random = new Random();
        this.numberOfDice = numberOfDice;
    }

    public int getNumberOfDice() {
        return random.nextInt((this.numberOfDice-MIN)+1)+1;
    }
}
class  PlaySnakeAndLadder{
   private Map<String, PairPosition> playerHistory;
   private Map<String, Integer>  playerLatestPosition;
   private Entities  entities;
   private Dice dice;

   public PlaySnakeAndLadder(int N){
       playerHistory = new HashMap<>();
       playerLatestPosition = new HashMap<>();
       entities = Entities.getInstance();
       dice = new Dice(N);
   }
   public String PlayGame() {
       initilizePlayersStartValue();
       int i = -1;
       do {
           i++;
           if (i >= entities.getPlayers().size()) {
               i = 0;
           }
           StringBuilder str = new StringBuilder();
           String playerName = entities.getPlayers().get(i);
           str.append(playerName);
           int diceNumber = dice.getNumberOfDice();
           int endPosition = playerLatestPosition.get(playerName) + diceNumber;
           String sl = "";
           if (checkFordiceNumGreaterThan100(endPosition)) {
               str.append(" rolled a ").append(diceNumber);
               str.append(" and moved from ").append(playerLatestPosition.get(playerName));
               if (entities.getSnakes().get(endPosition) != null) {
                   sl = " after snake dinner";
                   playerLatestPosition.put(playerName, entities.getSnakes().get(endPosition));
               } else {
                   if (entities.getLadders().get(endPosition) != null) {
                       sl = " after Ladder ride ";
                       playerLatestPosition.put(playerName, entities.getLadders().get(endPosition));
                   } else {
                       playerLatestPosition.put(playerName, endPosition);
                   }
               }
               str.append(" to ").append(playerLatestPosition.get(playerName));
               str.append(sl);

           }
           System.out.println(str);

       }
       while (!isPlayerWon(entities.getPlayers().get(i)));
       return entities.getPlayers().get(i);

   }

    private void initilizePlayersStartValue() {
       for(int i =0; i<entities.getPlayers().size(); i++){
             playerLatestPosition.put(entities.getPlayers().get(i),0);
       }
    }

    private boolean checkFordiceNumGreaterThan100(int endPosition) {
       return endPosition <= 100;
    }

    private boolean isPlayerWon(String player) {
        return playerLatestPosition.get(player) == 100;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Entities entities1 = Entities.getInstance();

        int noOfSnakes = sc.nextInt();
        while (noOfSnakes>0){
            int startPosition = sc.nextInt();
            int endPosition = sc.nextInt();
            entities1.setSnake(startPosition,endPosition);
            noOfSnakes--;
    }
        int noOfLadders = sc.nextInt();
        while (noOfLadders > 0) {
            int startPosition = sc.nextInt();
            int endPosition = sc.nextInt();
            entities1.setLadder(startPosition,endPosition);
            noOfLadders--;
        }
        int noOfPlayers = sc.nextInt();
        int i = 0;
        while (noOfPlayers>0) {
            String player = sc.next();
            entities1.setPlayer(i++, player);
            noOfPlayers--;
        }

        PlaySnakeAndLadder playSnakeAndLadder = new PlaySnakeAndLadder(6);
        System.out.println(playSnakeAndLadder.PlayGame()+" wins the game");

}
}

