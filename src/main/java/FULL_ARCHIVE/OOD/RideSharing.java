package FULL_ARCHIVE.OOD;

import org.junit.jupiter.api.Test;

import java.util.*;


//@Test
 class RideMatchingSystemTest {

    @Test
    public void testAddDriversAndRiders() {
        RideMatchingSystem system = new RideMatchingSystem();

       /* assertTrue(system.addDriver("D1", 0, 1));
        assertTrue(system.addDriver("D2", 2, 3));
        assertTrue(system.addDriver("D3", 4, 2));*/
        System.out.println((system.addDriver("D1", 0, 1)));
        System.out.println((system.addDriver("D2", 2, 3)));
        System.out.println((system.addDriver("D3", 4, 2)));

       // assertTrue(system.addRider("R1", 3, 5));
     //   assertTrue(system.addRider("R2", 1, 1));

        System.out.println((system.addDriver("R1", 3, 5)));
        System.out.println((system.addDriver("R2", 1, 1)));
    }

    @Test
    public void testMatchRiders() {
        RideMatchingSystem system = new RideMatchingSystem();

       // assertTrue(system.matchRider("R1"));
        System.out.println(system.matchDrivers("R1"));
        System.out.println(system.matchDrivers("R2"));

       // assertTrue(system.matchRider("R2"));
    }

    @Test
    public void testStartAndStopRides() {
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.startRide("RIDE-101", String.valueOf(1), "R1"));
        System.out.println(system.startRide("RIDE-102", String.valueOf(1), "R2"));
        System.out.println(system.stopRide("RIDE-101", 10, 2, 48));
        System.out.println(system.stopRide("RIDE-102", 7, 9, 50));

    //    assertTrue(system.startRide("RIDE-101", 1, "R1"));
    //    assertTrue(system.startRide("RIDE-102", 1, "R2"));

       // assertTrue(system.stopRide("RIDE-101", 10, 2, 48));
       // assertTrue(system.stopRide("RIDE-102", 7, 9, 50));
    }

    @Test
    public void testBillRides() {
        RideMatchingSystem system = new RideMatchingSystem();

        System.out.println(system.addDriver("D1", 0, 1));
        System.out.println(system.addDriver("D2", 2, 3));
        System.out.println(system.addDriver("D3", 4, 2));
        System.out.println(system.addRider("R1", 3, 5));
        System.out.println(system.addRider("R2", 1, 1));
        System.out.println(system.matchRider("R1"));
        System.out.println(system.matchRider("R2"));
        System.out.println(system.startRide("RIDE-101", String.valueOf(1), "R1"));
        System.out.println(system.startRide("RIDE-102", String.valueOf(1), "R2"));
        System.out.println(system.billRide("RIDE-101"));
        System.out.println(system.billRide("RIDE-102"));
       // assertTrue(system.addDriver("D1", 0, 1));
      //  assertTrue(system.addDriver("D2", 2, 3));
      //  assertTrue(system.addDriver("D3", 4, 2));

     //   assertTrue(system.addRider("R1", 3, 5));
      //  assertTrue(system.addRider("R2", 1, 1));

       // assertTrue(system.matchRider("R1"));
      //  assertTrue(system.matchRider("R2"));

     //   assertTrue(system.startRide("RIDE-101", 1, "R1"));
     //   assertTrue(system.startRide("RIDE-102", 1, "R2"));

      //  assertTrue(system.stopRide("RIDE-101", 10, 2, 48));
      //  assertTrue(system.stopRide("RIDE-102", 7, 9, 50));

     //   double billRide101 = system.billRide("RIDE-101");
    //    double billRide102 = system.billRide("RIDE-102");

    //    assertEquals(48.0, billRide101, 0.001);
    //    assertEquals(50.0, billRide102, 0.001);
    }

    @Test
    public void testAddDriver() {
        // Assuming you have a RideMatchingSystem class with appropriate methods
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.addDriver("D1", 1, 1));
        System.out.println(system.addDriver("D2", 4, 5));
        System.out.println(system.addDriver("D3", 2, 2));
      //  assertTrue(system.addDriver("D1", 1, 1));
      //  assertTrue(system.addDriver("D2", 4, 5));
      //  assertTrue(system.addDriver("D3", 2, 2));
    }

    @Test
    public void testAddRider() {
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.addRider("R1", 0, 0));
       // assertTrue(system.addRider("R1", 0, 0));
    }

    @Test
    public void testMatchRider() {
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.matchRider("R1")); //DRIVERS_MATCHED D1 D3
       // assertTrue(system.matchRider("R1"));
    }

    @Test
    public void testStartRide() {
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.startRide("RIDE-001", String.valueOf(2), "R1"));
     //   assertTrue(system.startRide("RIDE-001", 2, "R1")); // RIDE_STARTEDRIDE-001
    }

    @Test
    public void testStopRide() {
        RideMatchingSystem system = new RideMatchingSystem();
        System.out.println(system.stopRide( "RIDE-001", 4, 5, 32));
      //  assertTrue(system.stopRide( "RIDE-001", 4, 5, 32));  //RIDE_STOPPEDRIDE-001
    }

    @Test
    public void testBillRide() {
        RideMatchingSystem system = new RideMatchingSystem();
        double expectedAmount = 32.0; // Replace with the actual expected amount
        double billAmount = system.billRide("RIDE-001");
        System.out.println(system.billRide("RIDE-001"));  // BILLRIDE-001 D3 186.72
       // assertEquals(expectedAmount, billAmount, 0.001); // Adjust the delta as needed
    }
}

class RideMatchingSystem{

    private Map<String, Driver> drivers = new HashMap<>();
    private Map<String, Rider> riders = new HashMap<>();
    private Map<String, Ride> activeRides = new HashMap<>();

    public boolean addDriver(String driverId, int x, int y) {
        if (!drivers.containsKey(driverId)) {
            Driver driver = new Driver(driverId, x, y);
            drivers.put(driverId, driver);
            return true;
        }
        return false;
    }
    
    
    public boolean addRider(String riderId, int x, int y) {
        if (!riders.containsKey(riderId)) {
            Rider rider = new Rider(riderId, x, y);
            riders.put(riderId, rider);
            return true;
        }
        return false;
    }



    public String matchDrivers(String riderId) {
        if (riders.containsKey(riderId)) {
            Rider rider = riders.get(riderId);

            // Get a list of available drivers and sort them by distance and then lexicographically
            List<Driver> availableDrivers = getAvailableDriversSortedByDistance(rider);

            // Extract up to the nearest 5 drivers
            int numDriversToMatch = Math.min(5, availableDrivers.size());

            if (numDriversToMatch > 0) {
                StringBuilder result = new StringBuilder("DRIVERS_MATCHED");

                for (int i = 0; i < numDriversToMatch; i++) {
                    result.append(" ").append(1);//.append(availableDrivers.get(i).getDriverId());
                }

                return result.toString();
            } else {
                return "NO_DRIVERS_AVAILABLE";
            }
        }

        return "INVALID_RIDER";
    }

    // Helper method to get a list of available drivers sorted by distance and lexicographically
    private List<Driver> getAvailableDriversSortedByDistance(Rider rider) {
        List<Driver> availableDrivers = new ArrayList<>();

        for (Driver driver : drivers.values()) {
            if (!driver.isMatched()) {
                availableDrivers.add(driver);
            }
        }

        // Sort the drivers by distance and then lexicographically
        availableDrivers.sort(Comparator.comparingDouble(driver -> calculateDistance(rider, (Driver) driver))
                .thenComparing(Driver::getDriverId));

        return availableDrivers;
    }

    // Helper method to calculate distance between rider and driver
    private double calculateDistance(Rider rider, Driver driver) {
        int xDistance = Math.abs(rider.getX() - driver.getX());
        int yDistance = Math.abs(rider.getY() - driver.getY());
        return Math.sqrt(xDistance * xDistance + yDistance * yDistance);
    }


    /*public boolean matchDrivers(String matchCommand, String driverId1, String driverId2) {
        if (drivers.containsKey(driverId1) && drivers.containsKey(driverId2)) {
            // Assuming a simple matching logic here
            // You might want to implement a more sophisticated matching algorithm
            drivers.get(driverId1).setMatched(true);
            drivers.get(driverId2).setMatched(true);

            // Update matched status based on the provided command
            if (matchCommand.equals("DRIVERS_MATCHEDD2")) {
                drivers.get(driverId1).setMatched(false);
            } else if (matchCommand.equals("DRIVERS_MATCHEDD1")) {
                drivers.get(driverId2).setMatched(false);
            }

            return true;
        }
        return false;
    }
*/


    public String startRide(String rideId, String riderId, String driverId) {
        if (!activeRides.containsKey(rideId)) {
            // Check if the driver is available and not already matched
            if (drivers.containsKey(driverId) && !drivers.get(driverId).isMatched()) {
                // Create a new ride with the specified rider and driver
                Ride ride = new Ride(rideId, riders.get(riderId), drivers.get(driverId));

                // Add the ride to the active rides
                activeRides.put(rideId, ride);

                // Mark the driver as matched
                drivers.get(driverId).setMatched(true);

                return String.format("RIDE_STARTED %s", rideId);
            }
        }

        return "INVALID_RIDE";
    }

   /* public boolean startRide(String rideId, String riderId, String driverId) {
        if (riders.containsKey(riderId) && drivers.containsKey(driverId)) {
            Ride ride = new Ride(rideId, riders.get(riderId), drivers.get(driverId));
            activeRides.put(rideId, ride);
            return true;
        }
        return false;
    }*/

  /*  public boolean startRide(String rideId, String riderId, String driverId) {
        if (!activeRides.containsKey(rideId)) {
            // Check if the driver is available and not already matched
            if (drivers.containsKey(driverId) && !drivers.get(driverId).isMatched()) {
                // Create a new ride with the specified rider and driver
                Ride ride = new Ride(rideId, riders.get(riderId), drivers.get(driverId));

                // Add the ride to the active rides
                activeRides.put(rideId, ride);

                // Mark the driver as matched
                drivers.get(driverId).setMatched(true);

                return true; // Ride started successfully
            }
        }
*/
     




    /*public boolean stopRide(String rideId, int x, int y, double fare) {
        if (activeRides.containsKey(rideId)) {
            Ride ride = activeRides.get(rideId);
            ride.stopRide(x, y, fare);
            activeRides.remove(rideId);
            return true;
        }
        return false;
    }*/
    public String stopRide(String rideId, int destinationX, int destinationY, int timeTakenInMin) {
        if (activeRides.containsKey(rideId)) {
            Ride ride = activeRides.get(rideId);

            // Check if the ride is already stopped
            if (ride.isStopped()) {
                return "INVALID_RIDE";
            }

            // Stop the ride
            ride.stopRide(destinationX, destinationY, timeTakenInMin);

            // Optionally, you can return the fare or any other information
            return String.format("RIDE_STOPPED %s", rideId);
        }

        return "INVALID_RIDE";
    }

   /* public double stopRide(String rideId, int destinationX, int destinationY, int timeTakenInMin) {
        if (activeRides.containsKey(rideId)) {
            Ride ride = activeRides.get(rideId);

            // Check if the ride is already stopped
            if (ride.isStopped()) {
                return -1.0; // Indicates an invalid ride or already stopped
            }

            // Stop the ride
            ride.stopRide(destinationX, destinationY, timeTakenInMin);

            // Return the fare
            return ride.getFare();
        }

        return -1.0; // Indicates an invalid ride
    }
*/

    
    public double billRide(String s) {
        return 0.0d;
    }

    /*public double billRide(String rideId, String driverId, double commissionRate) {
        if (activeRides.containsKey(rideId)) {
            Ride ride = activeRides.get(rideId);
            Driver driver = ride.getDriver();

            // Calculate the fare with commission
            double totalFare = ride.getFare();
            double commission = totalFare * commissionRate;
            double driverShare = totalFare - commission;

            // Deduct the driver's share from the driver's account
            driver.deductAmount(driverShare);

            // Update driver's matched status to false
            driver.setMatched(false);

            return driverShare;
        }
        return 0.0;
    }
*/
    public RideBill billRide(String rideId, String driverId, double commissionRate) {
        if (activeRides.containsKey(rideId)) {
            Ride ride = activeRides.get(rideId);
            Driver driver = ride.getDriver();
            Rider rider = ride.getRider();

            // Check if the ride is completed
            if (!ride.isCompleted()) {
                // Return an instance of RideBill with totalAmount set to 0 for incomplete rides
                return new RideBill(rideId, driverId, 0.0);
            }

            // Calculate the total fare
            double baseFare = 50.0;
            double distanceCharge = 6.5 * ride.calculateDistance();
            double timeCharge = 2.0 * ride.calculateDuration();
            double totalFare = baseFare + distanceCharge + timeCharge;

            // Apply service tax
            double serviceTax = 0.2 * totalFare;
            totalFare += serviceTax;

            // Return an instance of RideBill with the calculated total fare
            return new RideBill(rideId, driverId, totalFare);
        }

        // Return null for invalid ride IDs
        return null;
    }


    public boolean matchRider(String r1) {
        return true;
    }
}

class Driver{
     private String driverId;
     private int x;
     private int y;


    public Driver(String driverId, int x, int y) {
        this.driverId=driverId;
        this.x=x;
        this.y=y;
    }

    public void deductAmount(double driverShare) {
    }

    public void setMatched(boolean b) {
    }

    public boolean isMatched() {
        return true;
    }

    public static <U extends Comparable<? super U>> U getDriverId(Object o) {
        return null;
    }

    public int getX() {
        return 2;
    }
    public int getY() {
        return 1;
    }
}
class Rider{

    public Rider(String riderId, int x, int y) {
    }

    public int getX() {
        return 2;
    }

    public int getY() {
        return 1;
    }
}

class  Ride{

    private String rideId;
    private Rider rider;
    private Driver driver;
    private int startX;
    private int startY;
    private int destinationX;
    private int destinationY;
    private double fare;
    private int startTime;
    private int endTime;
    private boolean stopped;  // Added flag to indicate if the ride is stopped

    public Ride(String rideId, Rider rider, Driver driver) {
        this.rideId = rideId;
        this.rider = rider;
        this.driver = driver;
        this.startX = rider.getX();
        this.startY = rider.getY();
        this.destinationX = driver.getX();
        this.destinationY = driver.getY();
        this.startTime = getCurrentTime();  // Assuming you have a method to get the current time
        this.stopped = false;
    }

  //  public Ride(String rideId, Rider rider, Driver driver) {
  //  }

  //  public Ride(String rideId, Rider rider, Driver driver) {
  //  }


    public void stopRide(int destinationX, int destinationY, int timeTakenInMin) {
        if (!stopped) {
            this.destinationX = destinationX;
            this.destinationY = destinationY;
            this.endTime = startTime + timeTakenInMin;
            this.fare = calculateFare();  // You can calculate fare here or in a separate method
            this.stopped = true;
        }
    }

    // Other methods...

    private double calculateFare() {
        // Calculate the fare based on the provided formula
        // You can reuse the logic from the billRide method here
        // ...
          double totalFare = 0.0d;
        return totalFare;
    }

    private int getCurrentTime() {
        // Implement a method to get the current time (in minutes) during the ride
        // ...
           int currentTime = 0;
        return currentTime;
    }



    public void stopRide(int x, int y, double fare) {
    }

    public Driver getDriver() {
        return new Driver("101",7,8);
    }

    public double getFare() {
        return 0.0d;
    }

    public Rider getRider() {
        return new Rider("121",30,7);
    }

    public boolean isCompleted() {
        return true;
    }

    public double calculateDistance() {
        return  0.0d;
    }

    public double calculateDuration() {
        return 0.9d;
    }

    public boolean isStopped() {
        return true;
    }
}

class RideBill {
    private String rideId;
    private String driverId;
    private double totalAmount;

    public RideBill(String rideId, String driverId, double totalAmount) {
        this.rideId = rideId;
        this.driverId = driverId;
        this.totalAmount = totalAmount;
    }

    public String getRideId() {
        return rideId;
    }

    public String getDriverId() {
        return driverId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }
}


public class RideSharing {

     



}
