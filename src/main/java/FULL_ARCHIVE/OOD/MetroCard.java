package FULL_ARCHIVE.OOD;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MetroCard {
    private String cardNumber;
    private double balance;

    public MetroCard(String cardNumber, double balance) {
        this.cardNumber = cardNumber;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void recharge(double amount) {
        double serviceFee = amount * 0.02;
        balance += amount - serviceFee;
    }

    public void deduct(double amount) {
        balance -= amount;
    }
}

class Passenger {
    private String age;

    public Passenger(String age) {
        this.age = age;
    }

    public String getAge() {
        return age;
    }
}

class MetroStation {
    private String name;
    private double totalCollection;
    private double totalDiscount;
    private Map<String, Integer> passengerCount;

    public MetroStation(String name) {
        this.name = name;
        totalCollection = 0;
        totalDiscount = 0;
        passengerCount = new HashMap<>();
    }

    public void calculateFareAndTravel(Passenger passenger, String journeyType, MetroCard metroCard) {
        double fare;
        if (journeyType.equals("single")) {
            fare = calculateFare(passenger, "single");
        } else {
            fare = calculateFare(passenger, "return");
        }

        if (metroCard.getBalance() < fare) {
            System.out.println("Insufficient balance. Please recharge your MetroCard.");
            return;
        }

        metroCard.deduct(fare);
        totalCollection += fare;
        if (journeyType.equals("return")) {
            totalDiscount += fare * 0.5;
        }

        passengerCount.put(passenger.getAge(), passengerCount.getOrDefault(passenger.getAge(), 0) + 1);
    }

    private double calculateFare(Passenger passenger, String journeyType) {
        if (journeyType.equals("single")) {
            if (passenger.getAge().equals("ADULT")) {
                return 100;
            } else if (passenger.getAge().equals("SENIOR")) {
                return 50;
            } else if (passenger.getAge().equals("KID")) {
                return 30;
            }
        } else {
            return calculateFare(passenger, "single") * 0.5;
        }
        return 0;
    }

    public String getName() {
        return name;
    }

    public double getTotalCollection() {
        return totalCollection;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public Map<String, Integer> getPassengerCount() {
        return passengerCount;
    }
}

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MetroStation centralStation = new MetroStation("Central");
        MetroStation airportStation = new MetroStation("Airport");

        while (scanner.hasNextLine()) {
            String[] input = scanner.nextLine().split(" ");
            String command = input[0];

            switch (command) {
                case "BALANCE":
                    String cardNumber = input[1];
                    double balance = Double.parseDouble(input[2]);
                    MetroCard metroCard = new MetroCard(cardNumber, balance);
                    break;
                case "CHECK_IN":
                    String cardNumberCheckIn = input[1];
                    String passengerType = input[2];
                    String fromStation = input[3];
                    Passenger passenger = new Passenger(passengerType);
                    MetroCard metroCardCheckIn = getMetroCard(cardNumberCheckIn);
                    if (fromStation.equals("Central")) {
                        centralStation.calculateFareAndTravel(passenger, "single", metroCardCheckIn);
                    } else if (fromStation.equals("Airport")) {
                        airportStation.calculateFareAndTravel(passenger, "single", metroCardCheckIn);
                    }
                    break;
                case "PRINT_SUMMARY":
                    printSummary(airportStation, centralStation);
                    break;
                default:
                    break;
            }
        }
    }

    private static MetroCard getMetroCard(String cardNumber) {
        // Return MetroCard object based on cardNumber from some repository or storage
        return null;
    }

    private static void printSummary(MetroStation airportStation, MetroStation centralStation) {
        System.out.println("TOTAL_COLLECTION AIRPORT " + airportStation.getTotalCollection() + " " +
                airportStation.getTotalDiscount());
        printPassengerSummary(airportStation);

        System.out.println("TOTAL_COLLECTION CENTRAL " + centralStation.getTotalCollection() + " " +
                centralStation.getTotalDiscount());
        printPassengerSummary(centralStation);
    }

    private static void printPassengerSummary(MetroStation station) {
        station.getPassengerCount().entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()
                        .thenComparing(Map.Entry.comparingByKey()))
                .forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue()));
    }




    @Test
    public void testPrintSummary() {
        String input = "BALANCE MC1 400\n" +
                "BALANCE MC2 100\n" +
                "BALANCE MC3 50\n" +
                "BALANCE MC4 50\n" +
                "CHECK_IN MC1 SENIOR_CITIZEN AIRPORT\n" +
                "CHECK_IN MC2 KID AIRPORT\n" +
                "CHECK_IN MC3 ADULT CENTRAL\n" +
                "CHECK_IN MC1 SENIOR_CITIZEN CENTRAL\n" +
                "CHECK_IN MC3 ADULT AIRPORT\n" +
                "CHECK_IN MC3 ADULT CENTRAL\n" +
                "PRINT_SUMMARY";

        String expectedOutput = "TOTAL_COLLECTION CENTRAL 457 50\n" +
                "PASSENGER_TYPE_SUMMARY\n" +
                "ADULT 2\n" +
                "SENIOR_CITIZEN 1\n" +
                "TOTAL_COLLECTION AIRPORT 252 100\n" +
                "PASSENGER_TYPE_SUMMARY\n" +
                "ADULT 1\n" +
                "KID 1\n" +
                "SENIOR_CITIZEN 1\n";


        assertEquals(expectedOutput, input);
    }
}
