package FULL_ARCHIVE.OOD;

 /*

 1. You are tasked with implementing an online bookstore system.
    The system should have the following features:
    Book Class:
    • Create a Book class with attributes like title, author, ISBN, price, etc.
    • Implement methods for retrieving and updating book information. Shopping Cart:
    • Design a Shopping Cart class to add/remove books.
    • Calculate the total cost of items in the cart. Discounts:
    • Implement a discount system based on different criteria (e.g., total purchase amount, specific books).
    • Apply discounts to the total cost accordingly. User Authentication:
    • Include a simple user authentication system.
    • Users should be able to log in, view their order history, and update their profile.
    Concurrency:
    • Address potential concurrency issues in the shopping cart when multiple users try to purchase items simultaneously.

Implement this system using Java 8 features such as Lambdas, Streams, and Optional.
 Consider incorporating design patterns where applicable.



2. Employee object has two attribute named age and Salary. Find out average salary of employees who are older than 30 years. Please print their names in uppercase sorted by their salaries in descending order using streams and functional programming.

  */

public class BookShoppingCart {
}
