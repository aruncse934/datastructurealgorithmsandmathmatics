Key Responsibilities:

Demonstrate a customer centric mindset as part of day-day work.
Ensure that the implementation adheres to defined specs and processes.
Own end-to-end quality of deliverables during all phases of the software development lifecycle.
Understand technical designs by working within PayPal’s cross-functional technology team.
Work with managers, leads and peers to come up with implementation options
Ability to function effectively in a fast-paced environment and manage continuously changing business needs


Technical Skills

Strong programming skills and expertise in Java/J2EE, Spring and Hibernate
Experience with REST API’s, Web Services, Unit Testing and build tools.
Good understanding and hands on experience with databases such as MySQL/ Oracle etc.
Rich object-oriented design and analysis skills


Personal Traits

Be a quick learner undaunted by complex systems.
Take risks and thrive in an environment of constant change.
Contributed to building the complete life cycle of transaction processing preferably in payments domain
Experience in refactoring existing code base and in understanding existing implementations to decide to refactor or rewrite.
Exposure and participation in tech events, hackathons, contributions to open source, authoring of blogs and active participation in online tech forums is a plus.


Organizational Context:
Member of a product engineering or delivery and integration team reporting to a Senior Engineer, Engineering Director or Director Product Delivery & Integration.
How will you make an impact in this role?
Software Development

Beginner level ability in software development, such coding assignments
Function as member of an agile team by contributing to software builds through consistent development practices (tools, common components, and documentation)
Participate in code reviews and automated testing
Debug basic software components and identify code defects for remediation
Enable the deployment, support, and monitoring of software across test, integration, and production environments
Automate deployments in test or production environments
Automatically scale applications based on demand projections
Leadership

Demonstrate increased self-reliance to achieve team goals
Influence team members with creative changes and improvements by challenging status quo and demonstrating risk taking
Range of Impact/Influence:

Accountable for team completing work you’re leading and work you are doing as agreed upon
Accountable to team for delivery of quality work
You won’t just shape the world of software. You’ll shape the world of life, work and play. Our Software Engineers not only understand how technology works, but how that technology intersects with the people who count on it every day. Today, innovative ideas, insight and new perspectives are at the core of how we create a more powerful, personal and fulfilling experience for all our customers. So if you’re interested in a career creating breakthrough software and making an impact on an audience of millions, look no further. You won’t just keep up, you’ll break new ground.  There are hundreds of opportunities to make your mark on technology and life at American Express. Here’s just some of what you’ll be doing: - Taking your place as a core member of an agile team driving the latest development practices - Writing code and unit tests, working with API specs and automation - Identifying opportunities for adopting new technologies -

Up to 4 years of software development experience in a professional environment and/or comparable experience such as:

Must have Java experience
Web development work experience preferred
Demonstrated experience in Agile development, application design, software development, and testing
Experience with Cassandra, Spark, Java
Experience with building RESTful APIs
Expertise in objected oriented analysis and design across a variety of platforms
Thorough understanding of JSON, Web Service technologies, and data structure fundamentals
Experience with adaptive and responsive development techniques
Aptitude for learning and applying programming concepts
Ability to effectively communicate with internal and external business partners
Experience with a broad range of software languages and payments technologies is a plus.
Familiar with Agile or other rapid application development methods
Experience with design and coding across one or more platforms and languages as appropriate
Hands-on expertise with application design, software development and automated testing
Experience with distributed (multi-tiered) systems, algorithms, and relational databases
Confirmed experience with object-oriented design and coding with variety of languages
Bachelor’s Degree in computer science, computer science engineering, or related experience required; advanced degree preferred
Knowledge/Skills:

Design, code, and implement highly scalable and reliable web-based applications.
Coordinate with other team’s architect, engineers and vendors as necessary.
Deliver on all phases of development work from initial kick-off, technical setup, application development, and support.
Identify exciting opportunities for adopting new technologies to solve existing needs and predicting future challenges
Perform ongoing refactoring of code, utilizing visualization and other techniques to fast-track concepts, and delivering continuous improvement
Work with product managers to prioritize features for ongoing sprints and managing a list of technical requirements based on industry trends, new technologies, known defects, and issues
Manage your own time, and work well both independently and as part of a team
Quickly generate and update proof of concepts for testing and team feedback
Embrace emerging standards while promoting best practices
Able to understand and use complex data structures and associated components
Designs, codes, tests, maintains, and documents applications
Takes part in reviews of own work and reviews of colleagues' work
ditions based on the requirements and specifications provided
Has deep understanding of the core tools used in the planning, analyzing, crafting, building, testing, configuring, and maintaining of assigned application(s)
Intermediate knowledge of infrastructure technologies and components
American Express is an equal opportunity employer and makes employment decisions without regard to race, color, religion, sex, sexual orientation, gender identity, national origin, veteran status, disability status, age, or any other status protected by law.
Offer of employment with American Express is conditioned upon the successful completion of a background verification check, subject to applicable laws and regulations.

