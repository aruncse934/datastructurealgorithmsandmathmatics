package SystemDesign.Tiny_Url;

import java.util.HashMap;
import java.util.Random;

public class TinyUrl {

    private String prefix = "http://tiny.url/";
    private HashMap<String, String> shortToLongMap = new HashMap<>();
    private HashMap<String, String> longToShortMap = new HashMap<>();
    private String SPACE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public String longToShort(String url){
        if (longToShortMap.containsKey(url)) {
            return longToShortMap.get(url);
        }
        String shortURL = generateRandomShortURL();
        longToShortMap.put(url,shortURL);
        shortToLongMap.put(shortURL,url);
        return shortURL;
    }

    private String generateRandomShortURL(){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        while (true){
            int count = 6;
            while (count > 0){
                sb.append(SPACE.charAt(random.nextInt(62)));
                count--;
            }
            String shortURL = prefix + sb.toString();
            if(shortToLongMap.containsKey(shortURL)){
                sb = new StringBuilder();
            }else {
                return shortURL;
            }

        }
    }
    public String shortToLong(String url){
        if(shortToLongMap.containsKey(url)){
            return shortToLongMap.get(url);

        }
        return null;
    }

    public static void main(String[] args) {
        TinyUrl u = new TinyUrl();
        System.out.println(u.shortToLong(u.longToShort("http://www.lintcode.com/faq/?id=10")));
    }

}
/*Input: shortToLong(longToShort("http://www.lintcode.com/faq/?id=10"))
Output: "http://www.lintcode.com/faq/?id=10"
Explanation:
  When longToShort() called, you can return any short url.
  For example, "http://tiny.url/abcdef".
  And "http://tiny.url/ABCDEF" is ok as well.
Example 2:

Input:
  shortToLong(longToShort("http://www.lintcode.com/faq/?id=10"))
  shortToLong(longToShort("http://www.lintcode.com/faq/?id=10"))
Output:
  "http://www.lintcode.com/faq/?id=10"
  "http://www.lintcode.com/faq/?id=10"*/