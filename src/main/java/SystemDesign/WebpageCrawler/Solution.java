package SystemDesign.WebpageCrawler;


import java.util.List;






import java.util.Collections;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.*;
import java.lang.Thread;
import java.net.*;
import java.io.*;
 class HtmlHelper {
    public static List<String> parseUrls(String url) {
        return null;
    }
    // Get all urls from a webpage of given url.
}
class CrawlerThread extends Thread {
    // Must use thread safe or concurrent collections
    private static LinkedBlockingQueue<String> queue = new LinkedBlockingQueue();
    private static ConcurrentMap<String, Boolean> finishedUrls = new ConcurrentHashMap();
    private static List<String> results = Collections.synchronizedList(new ArrayList<String>());

    // Assume the first one must be wikipedia.org
    static void setFirstUrl(String url) {
        try {
            queue.put(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static List<String> getResults() {
        return results;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            String url = null;
            try {
                url = queue.take();
            } catch (Exception e) {
                e.printStackTrace();
            }
            URL netUrl = null;
            try {
                netUrl = new URL(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String domain = netUrl.getHost();

            if (finishedUrls.containsKey(url)
                    || !domain.endsWith("wikipedia.org")) {
                //skip already crawled or unqualified url
                continue;
            }
            finishedUrls.putIfAbsent(url, true);
            results.add(url);
            List<String> linked = HtmlHelper.parseUrls(url);
            for (String u: linked) {
                try {
                    queue.put(u);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

public class Solution {
    /**
     * @param url a url of root page
     * @return all urls
     */
    public List<String> crawler(String url) {
        int numThread = 3;
        CrawlerThread.setFirstUrl(url);

        Thread[] threadPool = new Thread[numThread];
        for (int i = 0; i < numThread; i++) {
            threadPool[i] = new CrawlerThread();
        }

        for (int i = 0; i < numThread; i++) {
            threadPool[i].start();
        }

        try {
            // use sleep to wait for all thread to finish
            Thread.sleep(2000);
        } catch (InterruptedException e) {
//            e.printStackTrace(); .
        }

        for (int i = 0; i < numThread; i++) {
            threadPool[i].interrupt();
        }

        return CrawlerThread.getResults();
    }
}
