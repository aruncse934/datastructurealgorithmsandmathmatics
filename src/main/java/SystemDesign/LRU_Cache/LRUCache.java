package SystemDesign.LRU_Cache;

import java.util.HashMap;
import java.util.Map;

class DoubleLLNode{
    int key;
    int value;
    DoubleLLNode next;
    DoubleLLNode prev;

    DoubleLLNode(int key, int value) {
       this.key = key;
       this.value = value;
    }
}

/*class Node {


    @Override
    public String toString() {
        return String.valueOf(value);
    }
}*/
public class LRUCache {
    /*  Node head = new Node(0, 0), tail = new Node(0, 0);
  Map<Integer, Node> map = new HashMap();
  int capacity;*/
    private int capacity;
    private Map<Integer, DoubleLLNode> cache;
    private DoubleLLNode head;
    private DoubleLLNode tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        cache = new HashMap<>(capacity);
    }

    public int get(int key) {
            if (cache.containsKey(key)) {
                DoubleLLNode node = cache.get(key);
                moveToFront(node);
                return node.value;
            }
            return -1;
        }
        /* if(map.containsKey(key)) {
      Node node = map.get(key);
      remove(node);
      insert(node);
      return node.value;
    } else {
      return -1;
    }*/
    
    public void put(int key, int value) {
        if (cache.containsKey(key)) {
            DoubleLLNode node = cache.get(key);
            node.value = value;
            moveToFront(node);
            return;
        }

        DoubleLLNode node = new DoubleLLNode(key, value);

        if (cache.size() == capacity) {
            cache.remove(tail.key);
            removeNode(tail);
        }

        cache.put(key, node);
        addFirst(node);

        /*public void put(int key, int value) {
    if(map.containsKey(key)) {
      remove(map.get(key));
    }
    if(map.size() == capacity) {
      remove(tail.prev);
    }
    insert(new Node(key, value));
  }

  private void remove(Node node) {
    map.remove(node.key);
    node.prev.next = node.next;
    node.next.prev = node.prev;
  }

  private void insert(Node node){
    map.put(node.key, node);
    Node headNext = head.next;
    head.next = node;
    node.prev = head;
    headNext.prev = node;
    node.next = headNext;
  }*/
    }

    private void addFirst(DoubleLLNode node) {
    }

    private void removeNode(DoubleLLNode tail) {
    }


    private void moveToFront(DoubleLLNode node) {
    }

}

    

