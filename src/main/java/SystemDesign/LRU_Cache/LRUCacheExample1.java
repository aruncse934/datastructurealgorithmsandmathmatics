package SystemDesign.LRU_Cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Node{
    int key;
    int value;
    Node next;
    Node prev;

    public Node(int key, int value){
        this.key = key;
        this.value = value;
    }
    public String toString(){
        return String.valueOf(value);
    }
}
class LruCache1 {
    private int capacity;
    private Map<Integer, Node> cache;
    private Node head;
    private Node tail;

    public LruCache1(int capacity){
        this.capacity = capacity;
        cache = new HashMap<>(capacity);
    }

    public int get(int key){
        if(cache.containsKey(key)){
            Node n = cache.get(key);
            moveToFront(n);
            return n.value;
        }
        return -1;
    }
    public void put(int key, int value){
        if(cache.containsKey(key)){
            Node n = cache.get(key);
            n.value = value;
            moveToFront(n);
            return;
        }
        Node n1 = new Node(key,value);
        if(cache.size() == capacity){
            cache.remove(tail.key);
           removeNode(tail);
        }
        cache.put(key,n1);
        addFirst(n1);
    }
    private void moveToFront(Node node){
        removeNode(node);
        addFirst(node);
    }
    private void removeNode(Node node){
       Node prevNode = node.prev;
       Node nextNode = node.next;

       if(prevNode != null){
           prevNode.next = nextNode;
       }else {
           head = nextNode;
       }
       if(nextNode != null){
           nextNode.prev = prevNode;
       }else {
           tail = prevNode;
       }
    }
    private void addFirst(Node node){
        node.next = head;
        node.prev = null;
        if(head != null){
            head.prev = node;
        }
        head = node;
        if(tail == null){
            tail =node;
        }
    }
}

public class LRUCacheExample1{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int capacity = sc.nextInt();
        LruCache1 cache1 = new LruCache1(capacity);

        while(true){
            String[] cl = sc.nextLine().trim().split("\\s");
            String command = cl[0];
            if(command.isEmpty()){
                continue;
            }
            switch (command){
                case "get":{
                    int num = Integer.parseInt(cl[1]);
                    System.out.println(cache1.get(num));
                    break;
                }
                case "put":{
                    int key =Integer.parseInt(cl[1]);
                    int value = Integer.parseInt(cl[2]);
                    cache1.put(key,value);
                    break;
                }
                case "exit":{
                    return;
                }
                default:
                    System.out.println("Invalid command");
            }
        }
    }
}
/*2
put 5 7
put 8 20
get 5
7
put 3 6
get 8
-1
put 4 12
get 5
-1
get 3
6
get 4
12
exit*/