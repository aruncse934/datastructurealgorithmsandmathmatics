package SystemDesign.DesignTwitter;

import java.util.*;

/*Input
["Twitter", "postTweet", "getNewsFeed", "follow", "postTweet", "getNewsFeed", "unfollow", "getNewsFeed"]
[[], [1, 5], [1], [1, 2], [2, 6], [1], [1, 2], [1]]
Output
[null, null, [5], null, null, [6, 5], null, [5]]

Explanation
Twitter twitter = new Twitter();
twitter.postTweet(1, 5); // User 1 posts a new tweet (id = 5).
twitter.getNewsFeed(1);  // User 1's news feed should return a list with 1 tweet id -> [5]. return [5]
twitter.follow(1, 2);    // User 1 follows user 2.
twitter.postTweet(2, 6); // User 2 posts a new tweet (id = 6).
twitter.getNewsFeed(1);  // User 1's news feed should return a list with 2 tweet ids -> [6, 5]. Tweet id 6 should precede tweet id 5 because it is posted after tweet id 5.
twitter.unfollow(1, 2);  // User 1 unfollows user 2.
twitter.getNewsFeed(1);  // User 1's news feed should return a list with 1 tweet id -> [5], since user 1 is no longer following user 2.*/
 class Twitter {

	private static int timeStamp =0;
	private Map<Integer, User> userMap;

	private class Tweet{
		public int id;
		public int time;
		public Tweet next;

		public Tweet(int id){
			this.id = id;
			time = timeStamp++;
			next = null;
		}
	}

	class User{
		public int id;
		public Set<Integer> followed;
		public Tweet tweet_head;

		public User(int id) {
			this.id = id;
			followed = new HashSet<>();
			follow(id);
		    tweet_head = null;
		}
		public void follow(int id){
			followed.add(id);
		}
		public void unfollow(int id){
			followed.remove(id);
		}

		public void post(int id){
			Tweet t = new Tweet(id);
			t.next = tweet_head;
			tweet_head = t;
		}
	}


	public Twitter(){
		userMap =  new HashMap<Integer,User>();
	}

	public void postTweet(int userId, int tweetId){
		if(!userMap.containsKey(userId)){
			User u = new User(userId);
			userMap.put(userId,u);
		}
		userMap.get(userId).post(tweetId);
	}

	public List<Integer> getNewsFeed(int userId){
		List<Integer> res = new LinkedList<>();
		if(!userMap.containsKey(userId))
			return res;
		Set<Integer> users = userMap.get(userId).followed;
		PriorityQueue<Tweet> q = new PriorityQueue<>(users.size(),(a,b) -> (b.time-a.time));
		for(int user : users){
			Tweet t = userMap.get(user).tweet_head;
			if(t !=null){
				q.add(t);
			}
		}
		int n =0;
		while (!q.isEmpty() && n<10){
			Tweet t = q.poll();
			res.add(t.id);
			n++;
			if(t.next != null)
				q.add(t.next);
		}
		return res;
	}

	public void follow(int followerId, int followeeId){
		if(!userMap.containsKey(followerId)){
			User u = new User(followerId);
			userMap.put(followerId,u);
		}
		if (!userMap.containsKey(followeeId)) {
			User u = new User(followeeId);
			userMap.put(followeeId,u);
		}
		userMap.get(followerId).follow(followeeId);
	}
	public void unfollow(int followerId,int followeeId){
		if(!userMap.containsKey(followerId) || followerId == followeeId)
			return;
		userMap.get(followerId).unfollow(followeeId);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		Twitter t = new Twitter();

	}
}



/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter obj = new Twitter();
 * obj.postTweet(userId,tweetId);
 * List<Integer> param_2 = obj.getNewsFeed(userId);
 * obj.follow(followerId,followeeId);
 * obj.unfollow(followerId,followeeId);
 */