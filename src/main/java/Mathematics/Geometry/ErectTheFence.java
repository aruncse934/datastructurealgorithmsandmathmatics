package Mathematics.Geometry;

public class ErectTheFence {


    /*    public List<Point> outerTrees(Point[] points) {
            if (points.length <= 1)
                return Arrays.asList(points);
            sortByPolar(points, bottomLeft(points));
            Stack<Point> stack = new Stack<>();
            stack.push(points[0]);
            stack.push(points[1]);
            for (int i = 2; i < points.length; i++) {
                Point top = stack.pop();
                while (ccw(stack.peek(), top, points[i]) < 0)
                    top = stack.pop();
                stack.push(top);
                stack.push(points[i]);
            }
            return new ArrayList<>(stack);
        }

        private static Point bottomLeft(Point[] points) {
            Point bottomLeft = points[0];
            for (Point p : points)
                if (p.y < bottomLeft.y || p.y == bottomLeft.y && p.x < bottomLeft.x)
                    bottomLeft = p;
            return bottomLeft;
        }

        *//**
         * @return positive if counter-clockwise, negative if clockwise, 0 if collinear
         *//*
        private static int ccw(Point a, Point b, Point c) {
            return a.x * b.y - a.y * b.x + b.x * c.y - b.y * c.x + c.x * a.y - c.y * a.x;
        }

        *//**
         * @return distance square of |p - q|
         *//*
        private static int dist(Point p, Point q) {
            return (p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y);
        }

        private static void sortByPolar(Point[] points, Point r) {
            Arrays.sort(points, (p, q) -> {
                int compPolar = ccw(p, r, q);
                int compDist = dist(p, r) - dist(q, r);
                return compPolar == 0 ? compDist : compPolar;
            });
            // find collinear points in the end positions
            Point p = points[0], q = points[points.length - 1];
            int i = points.length - 2;
            while (i >= 0 && ccw(p, q, points[i]) == 0)
                i--;
            // reverse sort order of collinear points in the end positions
            for (int l = i + 1, h = points.length - 1; l < h; l++, h--) {
                Point tmp = points[l];
                points[l] = points[h];
                points[h] = tmp;
            }
        }
*/

}

/*There are some trees, where each tree is represented by (x,y) coordinate in a two-dimensional garden. Your job is to fence the entire garden using the minimum length of rope as it is expensive. The garden is well fenced only if all the trees are enclosed. Your task is to help find the coordinates of trees which are exactly located on the fence perimeter.



Example 1:

Input: [[1,1],[2,2],[2,0],[2,4],[3,3],[4,2]]
Output: [[1,1],[2,0],[4,2],[3,3],[2,4]]
Explanation:

Example 2:

Input: [[1,2],[2,2],[4,2]]
Output: [[1,2],[2,2],[4,2]]
Explanation:

Even you only have trees in a line, you need to use rope to enclose them.


Note:

All trees should be enclosed together. You cannot cut the rope to enclose trees that will separate them in more than one group.
All input integers will range from 0 to 100.
The garden has at least one tree.
All coordinates are distinct.
Input points have NO order. No order required for output.
input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.
Accepted*/