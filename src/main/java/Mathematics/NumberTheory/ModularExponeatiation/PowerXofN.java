package Mathematics.NumberTheory.ModularExponeatiation;

public class PowerXofN {
    public static void main(String[] args) {
        int x = 10;
        int n =9;

        System.out.println(recursivePower( x,n ));
        System.out.println(iterativePower( x,n ));
    }

    // Basic Method Recursive Method O(n)
    public static int recursivePower(int x,int n){
        if(n == 0)
            return 1;
        else
            return x*recursivePower( x,n-1 );
    }

    //Basic Method Iterative Method O(n)
    public static int iterativePower(int x, int n){
        int result =1;
        while (n > 0){
            result = result *x;
            n--;
        }
        return result;
    }
}
