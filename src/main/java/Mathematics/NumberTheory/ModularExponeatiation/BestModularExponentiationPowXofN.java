package Mathematics.NumberTheory.ModularExponeatiation;

public class BestModularExponentiationPowXofN {
    public static void main(String[] args) {
        int x = 2312 ;
        int n = 3434 ;
        int M =6789;

        System.out.println(modularExponentiation( x,n,M ));
        System.out.println(modularExponentiations( x,n,M ));

    }

    //recur TC O(logn) MC O(logn)
    public static int modularExponentiation(int x,int n,int M){
        if(n == 0)
            return 1;
        else if(n%2 == 0)
            return modularExponentiation( (x*x)%M,n/2,M );
        else
            return (x*modularExponentiation((x*x)%M, (n-1)/2,M ))%M;
    }

    //Iter TC O(log n) MC O(1)
    public static int modularExponentiations(int x, int n, int M){
        int result = 1;
        while (n > 0){
            if(n % 2 == 1)
            result = (result * x) % M;
            x = (x*x)%M;
            n = n/2; //n = n>>1;
        }
        return result;
    }
}
