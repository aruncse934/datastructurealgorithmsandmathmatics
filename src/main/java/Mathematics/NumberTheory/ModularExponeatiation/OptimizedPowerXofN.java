package Mathematics.NumberTheory.ModularExponeatiation;

public class OptimizedPowerXofN {

    public static void main(String[] args) {
        int x =10;
        int n = 18;
        System.out.println(binaryExponentiation( x,n ));
        System.out.println(binaryExponentiations( x,n ));

    }

    // Recursive O(log n )
    public static int binaryExponentiation(int x, int n){
        if(n==0)
            return 1;
        else if(n%2 == 0)        //n is even
            return binaryExponentiation(x*x,n/2);
        else                             //n is odd
            return x*binaryExponentiation(x*x,(n-1)/2);
    }

    //Iteraive O(log n
    public static int binaryExponentiations(int x,int n){
        int result =1;
        while (n > 0){
            if(n % 2 == 1)
                result = result *x;
                x = x*x;
                n = n/2;
        }
        return  result;
    }
}
