package Mathematics.NumberTheory.GreatestCommonDivisor;

public class GreatestCommonDiv {
    public static void main(String[] args) {
        int A =22;
        int B=20;
        System.out.println(gcd( A,B));

    }

    //Naive

    //Euclid's Algorithms
    public static  int gcd(int A,int B){
        if(B == 0)
            return A;
        else
            return gcd(B,A%B);
    }
}
