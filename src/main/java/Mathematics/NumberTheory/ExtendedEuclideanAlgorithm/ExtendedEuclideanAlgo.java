package Mathematics.NumberTheory.ExtendedEuclideanAlgorithm;

public class ExtendedEuclideanAlgo {
    public static void main(String[] args) {
        int x =1;
        int y =1;
        int A = 35;
        int B=15;
        System.out.println(extendedEuclideanAlgo( A,B,x,y ));
    }
    public static int  extendedEuclideanAlgo(int A, int B,int x,int y){
        if(A == 0){
            x=0;
            y=1;
            return B;
        }
            int x1 =1;
            int y1 =1;
           int gcd = extendedEuclideanAlgo(B%A,A,x1,y1);
           x = y1 -(B/A) *x1;
           y = x1;
        return gcd;
    }
}
