package Mathematics.NumberTheory.SieveOfEratosthenes;

public class SieveOfEratosthenesAlgo {
    public static void main(String[] args) {
        int n;
        for (n = 1; n <= 10; n++)
            System.out.println("phi(" + n + ") = " + sieveOfEratosthenes(n));
    }
    public static  int sieveOfEratosthenes(int n){
        float res  = n;
        for (int p = 2; p * p <= n; ++p) {
            if (n % p == 0) {
                while (n % p == 0)
                    n /= p;
                res *= (1.0 - (1.0 / (float)p));
            }
        }
        if (n > 1)
            res *= (1.0 - (1.0 / (float)n));
        return (int) res;

    }
}
