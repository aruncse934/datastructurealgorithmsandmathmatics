package Mathematics.NumberTheory.ModularMultiplicativeInverse;

public class ModularMultiplicationInverse {

    public static void main(String[] args) {
        int A =3;
        int M = 11;
        System.out.println(modInverse( A,M ));
        System.out.println(modInv( A,M));

    }
    //Naive Algo O(m)
    public static int modInverse(int A ,int M){
        A = A%M;
        for(int i =0;i<M;i++)
            if((A*i)%M==1)
                return i;
        return 1;
    }

    //Fermats’s little theorem
    public static int modInv(int A, int M){
        return power(A,M-2,M);

    }
    public static int power(int x, int n, int M){
        int results = 1;
        while (n > 0){
            if(n % 2 == 1)
                results = (results * x) % M;
            x = (x*x)%M;
            n = n/2; //n = n>>1;
        }
        return results;
    }
}
