package Mathematics.MATRIX_2023;

import java.util.HashSet;
import java.util.Set;

public class SetMatrixZeroes {

    public static void setZeroes(int[][] matrix){
        int R = matrix.length;
        int C = matrix[0].length;
        Set<Integer> rows   = new HashSet<>();
        Set<Integer> cols   = new HashSet<>();

        for(int i = 0; i < R; i++){
            for(int j = 0; j < C; j++){
                if(matrix[i][j] == 0){
                    rows.add(i);
                    cols.add(j);
                }
            }
        }
        for(int i = 0; i < R; i++){
            for(int j = 0; j < C; j++){
                if(rows.contains(i) || cols.contains(j)){
                    matrix[i][j] = 0;
                }
            }
        }
    }

    public static void main(String[] args) {

        int[][] matrix = new int[][] {{1,1,1},{1,0,1},{1,1,1}};

        setZeroes(matrix);
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length ; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }




    }
}
/*Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
Output: [[1,0,1],[0,0,0],[1,0,1]]*/