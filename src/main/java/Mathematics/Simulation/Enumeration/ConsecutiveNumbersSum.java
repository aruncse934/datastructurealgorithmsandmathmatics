package Mathematics.Simulation.Enumeration;

public class ConsecutiveNumbersSum {

    public int consecutiveNumbersSum(int n) {
        int ans =0;
       for(int i = 1; i*(i-1)/2 < n; ++i) {
           if ((n - i * (i - 1) / 2) % i == 0)
               ++ans;
       }
        return ans;
    }
    public static void main(String[] args) {

        int n = 5;
        System.out.println(new ConsecutiveNumbersSum().consecutiveNumbersSum(n));
    }
}
