package Mathematics.Simulation;

import java.util.Arrays;

public class Sum3ConsecutiveIntegers {

    public long[] sumOfThree(long num) {
        if(num % 3 != 0)
            return new long[0];
        num /=3;
        return new long[]{num-1,num, num+1};
    }

    public static void main(String[] args) {
        int n = 33;

        System.out.println(Arrays.toString(new Sum3ConsecutiveIntegers().sumOfThree(n)));

    }
}
/*Input: num = 33
Output: [10,11,12]
Explanation: 33 can be expressed as 10 + 11 + 12 = 33.
10, 11, 12 are 3 consecutive integers, so we return [10, 11, 12].*/