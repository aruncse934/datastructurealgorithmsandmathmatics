package Arrays.Sort;

import java.util.Arrays;
import java.util.Scanner;

public class InsertionSort {
    public static void main(String[] args) {
      /*  Scanner scan = new Scanner(System.in);
        int[] arr = new int[scan.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scan.nextInt();
        }
       *//* int[] indexArr = insertionSort(arr);

        for (int i : indexArr) {
            System.out.print(i + 1 + " ");
        }
        scan.close();
*//*
        sort(arr);
        printArray(arr);*/

        int ar[] = {7,4,5,2};
        System.out.println("Recursive Insertion sort::");
        insertionSortRecursive(ar,ar.length);
        System.out.println(Arrays.toString(ar));
    }

    static void insertionSortRecursive(int arr[], int n){
        if (n <= 1)
            return;
        insertionSortRecursive( arr, n-1 );
        int last = arr[n-1];
        int j = n-2;
        while (j >= 0 && arr[j] > last)
        {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = last;
    }
    public static void sort(int arr[]) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
    static void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

   /* public static int[] insertionSort(int[] arr) {
        int[] index = new int[arr.length];
        int[] aux = new int[arr.length];

        for (int i = 0; i < index.length; i++) {
            index[i] = i;
            aux[i] = i;
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (arr[j + 1] < arr[j]) {
                    swap(arr, j, j + 1);
                    swap(index, j, j+1);
                    swap(aux, index[j], index[j+1]);
                }
            }

        }
        return aux;
    }

    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }*/


}
