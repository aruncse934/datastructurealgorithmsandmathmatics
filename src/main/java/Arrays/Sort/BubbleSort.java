package Arrays.Sort;

import java.util.Arrays;
public class BubbleSort {
    public static void main(String[] args) {
        BubbleSort ob = new BubbleSort();
        int arr[] = {64, 34, 25, 12, 22, 11, 90};
        ob.bubbleSort(arr);
        System.out.println("Sorted array");
        ob.printArray(arr);
        System.out.println("Recursive method");
        bubbleSort(arr, arr.length);
        System.out.println(Arrays.toString(arr));
    }
    //Recursive
    static void bubbleSort(int arr[], int n){
        if (n == 1)
            return;
        for (int i=0; i<n-1; i++)
            if (arr[i] > arr[i+1]){
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            }
        bubbleSort(arr, n-1);
    }
    //Iteration methods
    void bubbleSort(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }
    /* Prints the array */
    void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
}
