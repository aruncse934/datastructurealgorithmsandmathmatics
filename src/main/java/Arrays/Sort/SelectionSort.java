package Arrays.Sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SelectionSort {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name[] = br.readLine().split(" ");
        int n = Integer.parseInt(name[0]);
        int x = Integer.parseInt(name[1]);
        int arr[] = new int[n];
        name = br.readLine().split(" ");
        for(int i = 0;i<n;i++)
            arr[i] = Integer.parseInt(name[i]);
       // sort(arr);
        //printArray(arr);
        selection_sort(arr,n,x);
    }

    public static void selection_sort(int a[],int n,int x)
    {
        int minimum;
        for(int i=0;i<x;i++)
        {
            minimum=i;
            for(int j=i+1;j<n;j++)
            {
                if(a[j]<a[minimum])
                    minimum=j;
            }
            int temp=a[minimum];
            a[minimum]=a[i];
            a[i]=temp;
        }
        for(int m=0;m<n;m++)
        {
            System.out.print(a[m]+" ");
        }
    }

     /*   public static void sort(int arr[]){
                int n = arr.length;
                for (int i = 0; i < n-1; i++){
                    int min_idx = i;
                    for (int j = i+1; j < n; j++)
                        if (arr[j] < arr[min_idx])
                            min_idx = j;
                    int temp = arr[min_idx];
                    arr[min_idx] = arr[i];
                    arr[i] = temp;
                }
            }
           public static void printArray(int arr[])
            {
                int n = arr.length;
                for (int i=0; i<n; ++i)
                    System.out.print(arr[i]+" ");
                System.out.println();
            }*/
    }
