package Arrays.Sort;

public class SegregateEvenNoddNum {
    public static void main(String[] args) {

        int arr[] = {1,9,5,3,2,6,7,11};
        int n = arr.length;
        arrayEvenAndOdd(arr,n);

    }

    public static void arrayEvenAndOdd(int arr[],int n){
        int i = -1;
        int j = 0;

        while(j != n){
            if(arr[j] % 2 == 0){
                i++;

                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
            j++;
        }
        for(int k = 0; k<n;k++){
            System.out.print(arr[k]+" ");
        }
    }
}
