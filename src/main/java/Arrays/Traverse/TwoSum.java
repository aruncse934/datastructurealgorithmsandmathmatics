package Arrays.Traverse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*Input: nums = [3,2,4], target = 6
Output: [1,2]*/
public class TwoSum {

    //brute force approach
    public static int[] twoSum(int arr[], int target){
        for(int i =0; i<arr.length;i++){
            for(int j = arr.length-1;j>i;j--){
                if(arr[i]+arr[j]== target) {
                    return new int[]{i, j};
                }

            }
        }

        return null;
    }
    //Hashtable approach
    public static int[] twoSumHashMap(int[] nums, int target){
        Map<Integer, Integer> map = new HashMap<>();
        for(int i =0; i < nums.length; i++){
            int complement = target - nums[i];
            if(map.containsKey(complement)){
                return new int[]{map.get(complement),i};
            }
            map.put(nums[i],i);
        }
        return null;
    }
    public static void main(String[] args) {
        int arr[] = {3,2,4};
        int target = 6;
        System.out.println(Arrays.toString(twoSum(arr, target)));
        System.out.println(Arrays.toString(twoSumHashMap(arr, target)));
    }
}
