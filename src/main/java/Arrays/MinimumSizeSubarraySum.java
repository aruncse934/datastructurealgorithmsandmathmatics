package Arrays;

public class MinimumSizeSubarraySum {
    public static int minSubArray(int A[], int s){
        if(A == null || A.length == 0)
            return 0;
        int i =0;
        int j = 0;
        int sum = 0;
        int min = Integer.MAX_VALUE;
        while ( j<A.length){
            sum += A[j++];
            while (sum >= s){
                min = Math.min(min,j-i);
                sum -= A[i++];
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
