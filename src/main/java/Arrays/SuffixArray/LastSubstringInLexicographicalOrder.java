package Arrays.SuffixArray;

public class LastSubstringInLexicographicalOrder {
    public String lastSubstring(String s) {
        int l = s.length();
        int i = 0, j = 1, k = 0;
        while(j + k < l) {
            if (s.charAt(i+k) == s.charAt(j+k)){
                k++;
                continue;
            }
            if (s.charAt(i+k) > s.charAt(j+k)){
                j++;
            } else {
                i = j;
                j = i + 1;
            }
            k = 0;
        }

        return s.substring(i);
    }
}


/*Given a string s, return the last substring of s in lexicographical order.



Example 1:

Input: "abab"
Output: "bab"
Explanation: The substrings are ["a", "ab", "aba", "abab", "b", "ba", "bab"]. The lexicographically maximum substring is "bab".
Example 2:

Input: "leetcode"
Output: "tcode"

Note:

1 <= s.length <= 4 * 10^5
s contains only lowercase English letters.

*/