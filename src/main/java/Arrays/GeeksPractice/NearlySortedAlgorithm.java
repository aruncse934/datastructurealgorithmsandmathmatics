package Arrays.GeeksPractice;

import java.util.PriorityQueue;
import java.util.Scanner;

public class NearlySortedAlgorithm {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0)
        {
            int n=sc.nextInt();
            int k=sc.nextInt();
            // int arr[]=new int[n];
            PriorityQueue<Integer> q=new PriorityQueue<Integer>();
            for(int i=0;i<k;i++)
                q.add(sc.nextInt());
            for(int i=k;i<n;++i)
            {
                System.out.print(q.poll()+" ");
                q.add(sc.nextInt());
            }
            while(!q.isEmpty())
                System.out.print(q.poll()+" ");
            System.out.println();
        }
    }

}

/*
*
* Given an array of n elements, where each element is at most k away from its target position. The task is to print array in sorted form.

Input:
First line consists of T test cases. First line of every test case consists of two integers N and K, denoting number of elements in array and at most k positions away from its target position respectively. Second line of every test case consists of elements of array.

Output:
Single line output to print the sorted array.

Constraints:
1<=T<=100
1<=N<=100
1<=K<=N

Example:
Input:
2
3 3
2 1 3
6 3
2 6 3 12 56 8
Output:
1 2 3
2 3 6 8 12 56
*
* */