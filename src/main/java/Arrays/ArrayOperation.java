package Arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class ArrayOperation {

    //Array Traversal
    public static void traversalArray(int[] arr){

        System.out.println("for Loop");
        for(int i =0;i<arr.length;i++){
            System.out.println("index "+i + "= "+arr[i]);
        }

        System.out.println("for Each Loop");
        for(int i : arr){
            System.out.println("number = "+i);
        }

        System.out.println("Java stream for each Loop");
        Arrays.stream(arr).forEach(System.out::print);
    }

    @Test
    public void traversalArrayTest(){
        int[] arr = {1,2,3,4,5};
        traversalArray(arr);
    }


    //Insertion in Array:
    public static void insertionArray(int[] arr, int n, int x, int pos){
        // Shift elements to the right to make space for the new element
     for(int i = n-1; i>= pos; i--){
         arr[i+1] = arr[i];
     }
        // Insert the new element
        arr[pos] = x;
    }
    @Test
    public void insertionArrayTest(){
        int[] arr = new int[6]; // 5 original elements, plus 1 extra space
        arr[0] = 1;
        arr[1] = 2;
        arr[2] = 3;
        arr[3] = 5;
        arr[4] = 6;

        int n = 5; // Number of elements in the array
        int x = 4; // New element to insert
        int pos = 3; // Position to insert the new element

        insertionArray(arr,n,x,pos);
        for(int i = 0; i<n+1;i++){
            System.out.println(arr[i]+" ");
        }
        System.out.println();
    }
}
