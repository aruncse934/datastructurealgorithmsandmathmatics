package Arrays;

public class SumOfProductAllPair {
    public static void main(String[] args) {
  int arr[] = {1,3,4};
  int n = arr.length;
        System.out.println(sumOfProductAllPair( arr,n));
    }

    public static int sumOfProductAllPair(int arr[], int n){
        int sumOfProduct = 0;

        for(int i = 0; i< n; i++)
            sumOfProduct = sumOfProduct+arr[i];
        int sumProductSqu = sumOfProduct*sumOfProduct;
        int individualSquSum = 0;
        for(int i = 0; i < n; i++)
            individualSquSum = individualSquSum + arr[i]* arr[i];
        return (sumProductSqu -individualSquSum)/2;
    }
}
