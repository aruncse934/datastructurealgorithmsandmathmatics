package Arrays.Search;

import java.util.Arrays;
import java.util.HashSet;

public class TripletsSumEqualNum {
    //brute force method
    public static void  findSumEqualNum(int nums[], int sum){
        System.out.println("Input array : "+ Arrays.toString(nums));
        System.out.println("Given Number : "+sum);
        System.out.println("Array Triplets Whose sum is "+sum+" are :");

        for(int i = 0; i <nums.length-2;i++){
            for(int j = i+1; j<nums.length-1;j++){
                for(int k = j+1;k < nums.length;k++){
                    if(nums[i]+nums[j]+nums[k] == sum){
                        System.out.println("["+nums[i]+ ","+nums[j]+","+nums[k]+"]");
                    }
                }

            }
        }
    }
    //Using HashSet  method
    public static void findArrayTripletsSum(int nums[], int sum){
        System.out.println("Input Array : "+Arrays.toString(nums));
        System.out.println("Given Sum Number : "+sum);
        System.out.println("Array Triplets whose sum is "+sum+" are :");

        for(int i = 0 ;i < nums.length-1 ; i++){
            HashSet<Integer> set = new HashSet<>();
            for(int j = i+1; j < nums.length;j++){
                if(set.contains(sum-nums[i]-nums[j])){
                    System.out.println("["+nums[i]+","+nums[j]+","+(sum - nums[i]-nums[j]) + "]");
                }
                else {
                    set.add(nums[j]);
                }
            }
        }
    }
    //Using sorting method
    public static void findArrayTripletsSums(int nums[], int sum){
        System.out.println("Input Array : "+Arrays.toString(nums));
        System.out.println("Given Sum Number : "+sum);
        System.out.println("Array Triplets whose sum is "+sum+" are :");
        /* List<Integer[]> result = new ArrayList<>();
    Arrays.sort(nums);
    for (int i = 0; i < nums.length; i++) {
      int left = i + 1;
      int right = nums.length - 1;
      while (left < right) {
        if (nums[i] + nums[left] + nums[right] == target) {
          result.add(new Integer[] { nums[i], nums[left], nums[right] });
          left++;
          right--;
        } else if (nums[i] + nums[left] + nums[right] < target) {
          left++;
        } else {
          right--;
        }
      }
    }
    return result;
  }*/

        Arrays.sort(nums);
        for(int i = 0; i< nums.length; i++){
            int left = i+1;
            int right = nums.length-1;
            while (left < right){
                if(nums[i] + nums[left] + nums[right] == sum){
                    System.out.println("["+nums[i] + ","+nums[left] + ","+nums[right]+ "]");
                    left++;
                    right--;
                }
                else if(nums[i] + nums[left] + nums[right] < sum){
                    left++;
                }
                else {
                    right--;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("========================Using Sorting Method===========================");
        findArrayTripletsSums(new int[]{7,5,9,3,0,8,6}, 12);
        System.out.println("===========================");
        findArrayTripletsSums(new int[] {-3, 7, -1, -5, 2, -9, 1 }, 0);//-3, 7, -1, -5, 2, -9, 1  /-1, 0, 1, 2, -1, -4
        System.out.println("===========================");
        findArrayTripletsSums(new int[] {17, 51, 39, 29, 33, 21, 65}, 89);

        System.out.println("========================Using Brute Force Method===========================");

        findSumEqualNum(new int[]{7,5,9,3,0,8,6}, 12);
        System.out.println("===========================");
        findSumEqualNum(new int[] {-3, 7, -1, -5, 2, -9, 1}, 0);
        System.out.println("===========================");
        findSumEqualNum(new int[] {17, 51, 39, 29, 33, 21, 65}, 89);

        System.out.println("========================Using Hashset Method===========================");

        findArrayTripletsSum(new int[]{7,5,9,3,0,8,6}, 12);
        System.out.println("===========================");
        findArrayTripletsSum(new int[] {-3, 7, -1, -5, 2, -9, 1}, 0);
        System.out.println("===========================");
        findArrayTripletsSum(new int[] {17, 51, 39, 29, 33, 21, 65}, 89);

    }
}
/*private static List<Integer[]> findThreeSum_Sorting(int[] nums, int target) {
    List<Integer[]> result = new ArrayList<>();
    Arrays.sort(nums);
    for (int i = 0; i < nums.length; i++) {
      int left = i + 1;
      int right = nums.length - 1;
      while (left < right) {
        if (nums[i] + nums[left] + nums[right] == target) {
          result.add(new Integer[] { nums[i], nums[left], nums[right] });
          left++;
          right--;
        } else if (nums[i] + nums[left] + nums[right] < target) {
          left++;
        } else {
          right--;
        }
      }
    }
    return result;
  }
}*/