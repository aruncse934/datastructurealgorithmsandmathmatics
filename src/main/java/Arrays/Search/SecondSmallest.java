package Arrays.Search;

public class SecondSmallest {

        public static void main(String[] args){
            //int arr[] = {4,5,6,1,2,3};
            int arr[] = {3, 6, 100, 9, 10, 12, 7, -1, 10};
            printSecondSmallest(arr);

        }

        public static void printSecondSmallest(int arr[]){
            int first;
            int second;
            int n = arr.length;


            if(n<2){
                System.out.println("Invalid input");
                return;
            }
            first = second = Integer.MAX_VALUE;

            for(int i =0; i<n;i++){
                if(arr[i] < first){
                    second = first;
                    first = arr[i];
                }

                else if(arr[i] < second && arr[i] != first)

                    second = arr[i];
            }

            if(second == Integer.MAX_VALUE)
                System.out.println("There is no second smallest elements");
            else
                System.out.println("The smallest elements is " + first +" and second smallest elemts is " + second);
        }
}
