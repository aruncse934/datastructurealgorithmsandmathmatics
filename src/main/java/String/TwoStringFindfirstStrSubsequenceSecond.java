package String;

public class TwoStringFindfirstStrSubsequenceSecond {
    public static void main(String[] args) {
        String s = "abc";
        String t = "ahbgdc";
        System.out.println( isSubsequence( s, t ) );
        System.out.println( isSubsequences( s, t ) );

    }

    //Two pointer
    public static boolean isSubsequence(String s, String t) {
        if (t.length() < s.length())
            return false;
        int prev = 0;
        for (int i = 0; i < s.length(); i++) {
            char tempChar = s.charAt( i );
            prev = t.indexOf( tempChar, prev );
            if (prev == -1)
                return false;
            prev++;
        }
        return true;
    }

   //Two pointer
    public static boolean isSubsequences(String s, String t) {
        if (s.length() == 0)
            return true;
        int indexS = 0, indexT = 0;
        while (indexT < t.length()) {
            if (t.charAt( indexT ) == s.charAt( indexS )) {
                indexS++;
                if (indexS == s.length())
                    return true;
            }
            indexT++;
        }
        return false;
    }
}