package String;

public class Test {

    public static void main(String[] args)
    {
        String X = "kjldsdfdrqiotuodkjhwdkljlkereoprieopieidflkdjkljs";
        String Y = "sakdjshdsklajsowijwodjlksjklsdnjopwiiweopdsddfkjdkljweioejw";
        System.out.println(printShortestSuperSeq(X, Y));
        //sakdjldsdfhdrqisklajsotuwijwodjlksjhwdklsdnjlkereoprwiiweopdieisddflkdjdkljsweioejw
    }

    public static String SCS(String X, String Y, int m, int n, int[][] T){
        if (m == 0) {
            return Y.substring(0, n);
        }
        if (n == 0) {
            return X.substring(0, m);
        }
        if (X.charAt(m - 1) == Y.charAt(n - 1)) {
            return SCS(X, Y, m - 1, n - 1, T) + X.charAt(m - 1);
        }
        else {

            if (T[m - 1][n] < T[m][n - 1]) {
                return SCS(X, Y, m - 1, n, T) + X.charAt(m - 1);
            }
            else {
                return SCS(X, Y, m, n - 1, T) + Y.charAt(n - 1);
            }
        }
    }
    public static void SCSLength(String X, String Y, int m, int n, int[][] T) {
        for (int i = 0; i <= m; i++) {
            T[i][0] = i;
        }
        for (int j = 0; j <= n; j++) {
            T[0][j] = j;
        }
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (X.charAt(i - 1) == Y.charAt(j - 1)) {
                    T[i][j] = T[i - 1][j - 1] + 1;
                }
                else {
                    T[i][j] = Integer.min(T[i - 1][j] + 1, T[i][j - 1] + 1);
                }
            }
        }
    }

    static String printShortestSuperSeq(String X, String Y)
    {
        int m = X.length();
        int n = Y.length();

        int dp[][] = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++){
                if (i == 0){
                    dp[i][j] = j;
                }
                else if (j == 0){
                    dp[i][j] = i;
                }
                else if (X.charAt(i - 1) == Y.charAt(j - 1)){
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                }
                else{
                    dp[i][j] = 1 + Math.min(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        int index = dp[m][n];
        String str = "";

        int i = m, j = n;
        while (i > 0 && j > 0){
            if (X.charAt(i - 1) == Y.charAt(j - 1)){
                str += (X.charAt(i - 1));
                i--;
                j--;
                index--;
            }
            else if (dp[i - 1][j] > dp[i][j - 1]){
                str += (Y.charAt(j - 1));
                j--;
                index--;
            }
            else{
                str += (X.charAt(i - 1));
                i--;
                index--;
            }
        }
        while (i > 0){
            str += (X.charAt(i - 1));
            i--;
            index--;
        }
        while (j > 0)
        {
            str += (Y.charAt(j - 1));
            j--;
            index--;
        }
        str = reverse(str);
        return str;
    }

    static String reverse(String input)
    {
        char[] temparray = input.toCharArray();
        int left, right = 0;
        right = temparray.length - 1;

        for (left = 0; left < right; left++, right--) {
            char temp = temparray[left];
            temparray[left] = temparray[right];
            temparray[right] = temp;
        }
        return String.valueOf(temparray);
    }
}
