package String;

import java.util.HashMap;
import java.util.Map;

public class FirstUniqueChar {
    public static void main(String[] args) {
        String s = "loveleetcode";
        System.out.println(firstUniqueChar( s ));
    }
    public static int firstUniqueChar(String s){
        Map<Character,Integer> hm = new HashMap<>();
        int n=s.length();
        for(int i = 0; i<n;i++){
            char c = s.charAt(i);
            hm.put(c,hm.getOrDefault(c,0 )+1);
        }
        for(int i =0;i<n;i++){
            if(hm.get( s.charAt( i ) )==1);
            return i;
        }
        return -1;
    }
}
