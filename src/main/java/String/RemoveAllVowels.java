package String;
//How To Remove All Vowels From String In Java?
public class RemoveAllVowels {
    public static void main(String[] args) {
        String s ="Java Programming Language";
        String s1 = s.replaceAll("[AEIOUaeiou]","" );
        System.out.println(s1);
    }
}
