package String;

public class ShortestCommanSuperSequence {
    public static void main(String[] args) {
        String X = "AGGTAB";
        String Y = "GXTXAYB";
        System.out.println(superSeq(X, Y, X.length(),Y.length()));

    }

    static int superSeq(String X, String Y,
                        int m, int n)
    {
        int[][] dp = new int[m + 1][n + 1];

        // Fill table in bottom up manner
        for (int i = 0; i <= m; i++)
        {
            for (int j = 0; j <= n; j++)
            {
                // Below steps follow above recurrence
                if (i == 0)
                    dp[i][j] = j;
                else if (j == 0)
                    dp[i][j] = i;
                else if (X.charAt(i - 1) == Y.charAt(j - 1))
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                else
                    dp[i][j] = 1 + Math.min(dp[i - 1][j],
                            dp[i][j - 1]);
            }
        }

        return dp[m][n];
    }

   /* public static int create_answer(int n,int i,int j){
        if(s1)
    }

    public static int f(int i,int j){
        if(i ==)

    }
    *//*int f(i, j) {
        if(i == S1.length() and j == S2.length()) return 1;
        if(S1*==S2[j]) return f(i+1, j+1);
        return f(i+1, j) + f(i, j+1);
    }
    create_answer(n, i, j) {
        if(S1* == S2[j]) {
            print S1*;
            create_answer(n,i+1,j+1);
        }
        else if(S1*<S2[j]) {
            if(n <= f(i+1, j)) {
                print S1*;
                create_answer(n, i+1, j);
            }
            else {print S2[j]; create_answer(n-f(i+1,j), i, j+1);}
        }
        else {
            if(n <= f(i,j+1)) {print S2[j]; create_answer(n, i, j+1);}
            else {print S1*; create_answer(n-f(i,j+1), i+1, j);}
        }
    }
    */
}
