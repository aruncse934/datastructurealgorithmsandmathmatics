package String;

import java.util.ArrayList;
import java.util.List;

public class PalindromicSubsttring {

    static int count =0;
    public static int countPalindromeSubstrings(String s) {
        if (s == null || s.length() == 0) return 0;
        for (int i = 0; i < s.length(); i++) { // i is the mid point
            extendPalindrome(s, i, i); // odd length;
            extendPalindrome(s, i, i + 1); // even length
        }
        return count;
    }
    public static void extendPalindrome(String s, int left, int right) {
        while (left >=0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            count++; left--; right++;
        }
    }

/*
    public static List<String> allPalindromeSubstring(String s){
        List<String> list = new ArrayList<String>();
        for (float pivot = 0; pivot < s.length(); pivot += .5) {
            float palindromeRadius = pivot - (int)pivot;
            while ((pivot + palindromeRadius) < s.length()
                    && (pivot - palindromeRadius) >= 0
                    && s.charAt((int)(pivot - palindromeRadius))
                    == s.charAt((int)(pivot + palindromeRadius))) {

                list.add(s.substring((int)(pivot - palindromeRadius),
                        (int)(pivot + palindromeRadius + 1)));
                palindromeRadius++;
            }
        }
        return list;
    }*/
    public static void main(String[] args) {
        String s ="aaa";
        System.out.println(countPalindromeSubstrings(s));

        /*String s1 = "nitin";
        System.out.println(allPalindromeSubstring(s1));*/
    }
}
/*Given a string, your task is to count how many palindromic substrings in this string.

The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.

Example 1:

Input: "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".


Example 2:

Input: "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".


Note:

The input string length won't exceed 1000.

*/