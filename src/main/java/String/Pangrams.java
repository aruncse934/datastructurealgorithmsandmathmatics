package String;

import java.util.HashSet;
import java.util.Set;

public class Pangrams {

    public static void main(String[] args) {

        String s = "We promptly judged antique ivory buckles for the next prize";
        String s1 ="The quick brown fox jumps over the lazy dog";
        String s3 ="abcdefghijklmnopqrstuvwxyz";//ABCDEFGHIJKLMNOPQRSTUVWXYZ
        System.out.println(pangrams( s3 ));
    }

    public static String pangrams(String s){
        Set<Character> hs = new HashSet<>( );
        String replaced =s.replaceAll( " ","" ).toLowerCase();
        for(int i =0;i<replaced.length();i++){
            hs.add( replaced.charAt( i ) );
        }
         if(hs.size() == 26)
             return "Pangrams";
         return "Not Pangrams";
    }

}
