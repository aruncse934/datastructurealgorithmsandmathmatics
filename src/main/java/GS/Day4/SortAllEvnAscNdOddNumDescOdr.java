package GS.Day4;

import java.util.Arrays;

public class SortAllEvnAscNdOddNumDescOdr {
    public static void main(String[] args) {
        int arr[] ={2,3,6,1,7,8,5,9,11};
        int n = arr.length;
        sortAllEvnAscNdOddNumDescOdr( arr,n );
       // printArray( arr,n );

        System.out.println(Arrays.toString( arr ));
    }
    public static void sortAllEvnAscNdOddNumDescOdr(int arr[], int n){
        for(int i=0;i<n;i++)
            if((arr[i] &1) != 0)
                arr[i] *= -1;

        Arrays.sort( arr);

        for(int i = 0; i< n ;i++)
            if((arr[i] &1) !=  0)
                arr[i] *= -1;
    }
    /*public static void printArray(int arr[] , int n){
        for (int i = 0;i<n;i++)
            System.out.print(arr[i]+" ");
    }*/
}
