package GS.Day4;

import java.util.Arrays;
import java.util.Collections;

public class SortFirstKvluesAscOrNkVluesDescOr {

    public static void main(String[] args) {
    Integer arr[] = { 5, 4, 6, 2, 1, 3, 8, 9, -1 };
    int k = 4;
        sortFirstKvluesAscOrNkVluesDescOr(arr,k );
        System.out.println(Arrays.toString( arr ));
    }
    public static void sortFirstKvluesAscOrNkVluesDescOr(Integer arr[], int k){
        int n = arr.length;
        Arrays.sort(arr,0,k);
        Arrays.sort(arr,k,n, Collections.reverseOrder());
    }

}
