package GS.Day4;

import java.util.Arrays;

public class RearrangeMaxMinArray {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7};
        int n = arr.length;
        rearrangeMaxMinArray( arr,n );
        System.out.println( Arrays.toString( arr ) );

    }

    public static void rearrangeMaxMinArray(int arr[], int n){
        int max_ele = arr[n-1];
        int min_ele = arr[0];

        for(int i = 0 ; i< n;i++){
            if(i%2 == 0) {
                arr[i] = max_ele;
                max_ele -= 1;
            }
            else{
                arr[i] = min_ele;
                min_ele += 1;
            }

        }
    }
}
