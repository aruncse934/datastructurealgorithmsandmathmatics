package GS.Day6;

public class SmallestSubArraywithSumNegativeNum {

    public static void main(String[] args) {
     int arr[] =  {- 8, 1, 4, 2, -6};
     int n = arr.length;
     int x = 6;
     int res = smallestSubArrayWithSumNegativeNum(arr, n,x);
     if(res == n+1)
         System.out.println("not possible");
     else
         System.out.println(res);
    }
    public static int smallestSubArrayWithSumNegativeNum(int arr[], int n, int x){
        int min_len = n+1;
        int curr_sum = 0;
        int start = 0;
        int end = 0;
        while (end < n){
            while (curr_sum <= x && end < n){
                if(curr_sum <= 0 && x>0){
                    start = end;
                    curr_sum =0;
                }
                curr_sum += arr[end++];
                while (curr_sum > x && start < n){
                    if(end-start < min_len)
                        min_len =end-start;
                    curr_sum -= arr[start++];
                }
            }
        }
        return min_len;
    }
}
