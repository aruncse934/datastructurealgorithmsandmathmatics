package GS.Day6;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class FindMinmumDistanceBetween2Num {

    public static void main(String[] args) {
        int arr[] = {2, 5, 3, 5, 4, 4, 2, 3};//{3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3};

       // Input: arr[] = {2, 5, 3, 5, 4, 4, 2, 3}, x = 3, y = 2
        int x = 3;
        int y = 2;

        System.out.println("Minimum distance between " + x + " and " + y
                + " is " + findminimumdistance(arr, x, y));
        System.out.println("Minimum distance between " + x + " and " + y
                + " is " + minDist(arr,arr.length, x, y));
    }

    static int findminimumdistance(int[] arr,int a,int b)
    {
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for(int i=0;i<arr.length;i++)
        {
            if(map.get(arr[i])==null)
                map.put(arr[i], i);
        }

        return Math.abs(map.get(a)-map.get(b));
    }

    public  static int minDist(int arr[], int n, int x, int y){
        int i = 0;
        int min_dist = Integer.MAX_VALUE;
        int prev=0;
        for (i = 0; i < n; i++) {
            if (arr[i] == x || arr[i] == y) {
                prev = i;
                break;
            }
        }
        for (; i < n; i++) {
            if (arr[i] == x || arr[i] == y){
                if (arr[prev] != arr[i] && (i - prev) < min_dist){
                    min_dist = i - prev;
                    prev = i;
                }
                else
                    prev = i;
            }
        }
        return min_dist;
    }
   /* public int shortestDistance(Vector<Integer> nums, int n1, int n2) {
        int dist = Integer.MAX_VALUE;
        int startIndex = -1;
        for (int i = 0; i < nums.size(); ++i)
        {
            if (nums[i] == n1 || nums[i] == n2)
            {
                if (startIndex != -1 && nums[i] != nums[startIndex])
                {
                    dist = min(dist, i - startIndex);
                }

                startIndex = i;
            }
        }

        return dist;
    }*/
}
