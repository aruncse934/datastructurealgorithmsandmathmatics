package GS.Day2;

public class FindMinDisBetTwoNum {

    public static void main(String[] args) {

        int arr[] = {3,4,5};
        int n = arr.length;
        int x =3;
        int y = 5;

        System.out.println("min Distance is "+ minDist(arr,n,x,y));

    }

    public  static int minDist(int arr[], int n, int x, int y){
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i< n;i++)
            for(int j = i+1; j<n;j++)
                if((x == arr[i] && y == arr[j] || y == arr[i] && x == arr[j]) && minDist > Math.abs( i-j ))
                    minDist = Math.abs( i-j );
                return minDist;
    }
}
