package GS.Day3;

//Find the only different element in an array
public class FindOnlyDiffElement {

    public static void main(String[] args) {
        int arr[] = {20,20,10,20,20,20};
        int n = arr.length;
        System.out.println(findOnlyDiffElement( arr, n ));

    }

    public static int findOnlyDiffElement(int arr[], int n) {
        if (n == 1)
            return -1;
        if (n == 2)
            return 0;
        if (arr[0] == arr[1] && arr[0] != arr[2])
            return 2;
        if (arr[0] == arr[2] && arr[0] != arr[1])
            return 1;
        if (arr[1] == arr[2] && arr[0] != arr[1])
            return 0;

        for (int i = 3; i < n; i++)
            if (arr[i] != arr[i - 1])
                return i;
        return -1;
    }

}
