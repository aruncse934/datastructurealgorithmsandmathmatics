package GS.Day3;

public class CountPairLessK {
    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5, 6, 7, 8};
        int n = arr.length;
        int x = 7;
        System.out.println(oountPair( arr,n,x ));
    }

    public static int oountPair(int arr[], int n, int k){
        int l =0, r = n-1;
        int result = 0;
        while (l<r){
            if(arr[l] * arr[r] < k){ //+
                result += (r-l);
                l++;
            }
            else
                r--;
        }
        return result;
    }
}
