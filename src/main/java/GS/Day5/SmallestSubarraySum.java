package GS.Day5;
//Minimum Size Subarray Sum
//Smallest subarray with sum greater than a given value
public class SmallestSubarraySum {
    public static void main(String[] args) {
        int arr[] = {2, 3, 1, 2, 4, 3};
        int x = 7;
        int n = arr.length;

        System.out.println( smallestSubarraySum( arr, x ) );

    }

    /*public static boolean smallestSubarraySum(int arr[],int n, int x){
        int i =0;
        int j = n-1;

        while (i < j) {
            int sum = arr[i] + arr[j];

            if (sum == x) {
                return true;
            } else if (sum < x) {
                i++;
            } else {
                j--;
            }

        }
        return false;
    }*/

    //Two Pointer Approach O(n)
    public static int smallestSubarraySum(int arr[], int x) {
        int n = arr.length;
        int ans = Integer.MAX_VALUE;
        int left = 0;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr[i];
            while (sum >= x) {
                ans = Math.min( ans, i + 1 - left );
                sum -= arr[left++];
            }
        }
        return (ans != Integer.MAX_VALUE) ? ans : 0;
    }
}