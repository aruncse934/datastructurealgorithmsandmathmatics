package GS.Day5;

import java.util.Arrays;
public class MedianTwoSortedArray {

        public static int Solution(int[] arr) {
            int n = arr.length;
            if (n % 2 == 0) {
                int z = n / 2;
                int e = arr[z];
                int q = arr[z - 1];

                int ans = (e + q) / 2;
                return ans;
            }
            else {
                int z = Math.round(n / 2);
                return arr[z];
            }
        }

        // Driver Code
        public static void main(String[] args)
        {

            // TODO Auto-generated method stub
            int[] arr1 = { -5, 3, 6, 12, 15 };
            int[] arr2 = { -12, -10, -6, -3, 4, 10 };

            int i = arr1.length;
            int j = arr2.length;

            int[] arr3 = new int[i + j];

            // Merge two array into one array
            System.arraycopy(arr1, 0, arr3, 0, i);
            System.arraycopy(arr2, 0, arr3, i, j);

            // Sort the merged array
            Arrays.sort(arr3);

            // calling the method
            System.out.print("Median = " + Solution(arr3));
        }
    }

    /*public double findMedianSortedArrays(int[] A, int[] B) {
	    int m = A.length, n = B.length;
	    int l = (m + n + 1) / 2;
	    int r = (m + n + 2) / 2;
	    return (getkth(A, 0, B, 0, l) + getkth(A, 0, B, 0, r)) / 2.0;
	}

public double getkth(int[] A, int aStart, int[] B, int bStart, int k) {
	if (aStart > A.length - 1) return B[bStart + k - 1];
	if (bStart > B.length - 1) return A[aStart + k - 1];
	if (k == 1) return Math.min(A[aStart], B[bStart]);

	int aMid = Integer.MAX_VALUE, bMid = Integer.MAX_VALUE;
	if (aStart + k/2 - 1 < A.length) aMid = A[aStart + k/2 - 1];
	if (bStart + k/2 - 1 < B.length) bMid = B[bStart + k/2 - 1];

	if (aMid < bMid)
	    return getkth(A, aStart + k/2, B, bStart,       k - k/2);// Check: aRight + bLeft
	else
	    return getkth(A, aStart,       B, bStart + k/2, k - k/2);// Check: bRight + aLeft
}*/



    /*public static int getMedian(int ar1[],int ar2[],int n){
       *//* if (n <= 0)
            return -1;
        if (n == 1)
            return (ar1[0] + ar2[0]) / 2;
        if (n == 2)
            return (Math.max(ar1[0], ar2[0]) + Math.min(ar1[1], ar2[1])) / 2;
        int m1 = median(ar1, n);
        int m2 = median(ar2, n);
        if (m1 == m2)
            return m1;
        if (m1 < m2)
        {
            if (n % 2 == 0)
                return getMedian(ar1 + n / 2 - 1, ar2, n - n / 2 + 1);
            return getMedian(ar1 + n / 2, ar2, n - n / 2);
        }

        if (n % 2 == 0)
            return getMedian(ar2 + n / 2 - 1, ar1, n - n / 2 + 1);
        return getMedian(ar2 + n / 2, ar1, n - n / 2);*//*
    }

    public static int median(int arr[], int n){
        if (n % 2 == 0)
            return (arr[n / 2] + arr[n / 2 - 1]) / 2;
        else
            return arr[n / 2];
    }*/

