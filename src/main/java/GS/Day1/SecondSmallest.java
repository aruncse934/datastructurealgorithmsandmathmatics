package GS.Day1;
import java.util.Arrays;

public class SecondSmallest {

        public static void main(String[] args){
            //int arr[] = {4,5,6,1,2,3};
            int arr[] = {3, 6, 100, 9, 10, 12, 7, -1, 10};
            printSecondSmallest(arr);
            getSmallestAndSecondSmallestElement(arr);
            getSmallestSecondSmallestJava8(arr);

        }
//java8
    private static void getSmallestSecondSmallestJava8(int[] inputArray){
        System.out.println("Input Array : "+Arrays.toString(inputArray));
        System.out.println("Smallest And Second Smallest Elements Are :");
        //Java 8 Code
        Arrays.stream(inputArray).sorted().limit(2).forEach(System.out::println);
    }
        //dp
    private static void getSmallestAndSecondSmallestElement(int[] inputArray) {
        int smallest = inputArray[0];
        int secondSmallest = inputArray[0];
        for (int i = 0; i < inputArray.length; i++){
            if (inputArray[i] < smallest){
                secondSmallest = smallest;
                smallest = inputArray[i];
            }
            else if (inputArray[i] > smallest && inputArray[i] < secondSmallest){
                secondSmallest = inputArray[i];
            }
        }

        System.out.println("Input Array : "+Arrays.toString(inputArray));

        System.out.println("Smallest Element : "+smallest);

        System.out.println("Second Smallest Element : "+secondSmallest);
    }
// efficient
        public static void printSecondSmallest(int arr[]){
            int first;
            int second;
            int n = arr.length;
            if(n<2){
                System.out.println("Invalid input");
                return;
            }
            first = second = Integer.MAX_VALUE;

            for(int i =0; i<n;i++){
                if(arr[i] < first){
                    second = first;
                    first = arr[i];
                }

                else if(arr[i] < second && arr[i] != first)

                    second = arr[i];
            }

            if(second == Integer.MAX_VALUE)
                System.out.println("There is no second smallest elements");
            else
                System.out.println("The smallest elements is " + first +" and second smallest elemts is " + second);
        }
}
