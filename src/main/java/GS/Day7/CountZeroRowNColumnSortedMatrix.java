package GS.Day7;

public class CountZeroRowNColumnSortedMatrix {
        public static int N = 5;
        static int countZeroes(int mat[][]){
            int row = N - 1, col = 0;
            int count = 0;
            while (col < N) {
                while (mat[row][col] > 0)
                    if (--row < 0)
                        return count;
                count += (row + 1);
                col++;
            }
            return count;
        }
        public static void main (String[] args) {
            int mat[][] = { { 0, 0, 0, 0, 1 },
                    { 0, 0, 0, 1, 1 },
                    { 0, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 },
                    { 1, 1, 1, 1, 1 } };
            System.out.println(countZeroes(mat));
        }
    }