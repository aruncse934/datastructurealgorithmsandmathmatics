package GS.Day7;

import java.util.HashMap;
import java.util.*;

public class KdiffPairs {
    public static void main(String[] args) {
        int arr[] = {8, 12, 16, 4, 0, 20 };
        int k = 4;
        System.out.println("Count of pairs with given diff is "
                + findPairs(arr, k));
        System.out.println("Count of pairs with given diff is "
                + countPairsWithDiffK(arr,arr.length, k));
    }
    //HashMap Approach
    public static int findPairs(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k < 0)   return 0;

        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        for (int i : nums) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (k == 0) {
                //count how many elements in the array that appear more than twice.
                if (entry.getValue() >= 2) {
                    count++;
                }
            } else {
                if (map.containsKey(entry.getKey() + k)) {
                    count++;
                }
            }
        }

        return count;
    }

    //Two Pointer
   public static int countPairsWithDiffK(int arr[], int n,
                                   int k)
    {
        int count = 0;
        Arrays.sort(arr); // Sort array elements

        int l = 0;
        int r = 0;
        while(r < n)
        {
            if(arr[r] - arr[l] == k)
            {
                count++;
                l++;
                r++;
            }
            else if(arr[r] - arr[l] > k)
                l++;
            else // arr[r] - arr[l] < sum
                r++;
        }
        return count;
    }
}
