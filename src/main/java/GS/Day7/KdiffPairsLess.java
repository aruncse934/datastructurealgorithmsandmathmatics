package GS.Day7;

import java.util.*;
public class KdiffPairsLess {
    static int countPairs(int a[], int n, int k){
        Arrays.sort(a);
        int res = 0;
        for (int i = 0; i < n; i++){
            int j = i + 1;
            while (j < n && a[j] - a[i] < k) {
                res++;
                j++;
            }
        }
        return res;
    }
    public static void main (String[] args){
        int a[] = {1, 10, 4, 2};
        int k = 3;
        int n = a.length;
        System.out.println(countPairs(a, n, k));
    }
}
