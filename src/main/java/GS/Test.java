package GS;

import java.util.HashMap;
import java.util.Map;

public class Test {// public class MaxLenDist {
    public static void main(String[] args) {
        int a[] = {6, 5, 1, 2, 3, 2, 1, 4, 5};
        int m = 5;

         maxLenDist( a,m );
    }

    public static void maxLenDist(int a[], int m){
            int startInd=0;
            int endInd=0;
            int len;
            int maxLen=0;
            int maxStart =0 , maxEnd =0;
            Map<Integer, Integer> hm ;
            for (int i = 0; i < a.length; i++) {
                int j;
                hm= new HashMap<>();
                for (j = i; j < a.length; j++) {
                    if(hm.size()<m || (hm.size()==3 && hm.containsKey(a[j])))
                    {
                        if(hm.containsKey(a[j]))
                        {
                            hm.put(a[j],hm.get(a[j])+1);
                        }
                        else
                        {
                            hm.put(a[j],1);
                        }
                        endInd=j;
                    }
                    if(hm.size()==3 && ! (hm.containsKey(a[j])))
                    {
                        break;
                    }
                    startInd=i;
                }
                //endInd=j;
                len = endInd-startInd +1;
                if(maxLen<len)
                {
                    maxLen=len;
                    maxStart=startInd;
                    maxEnd=endInd;
                }

            }
            System.out.println("Max length   "+ maxLen);
            for (int i = maxStart; i <= maxEnd; i++) {
                System.out.print(" " + a[i]);
            }
        }
    }
