package SlidingWindows;

import java.util.HashSet;
import java.util.Set;

public class Test {

    //Maximum Sum of Distinct Subarrays With Length K
     //SlidingWindow with HashSet
    public static long maximumSubarraySum(int[] nums, int k) {
        Set<Integer> set = new HashSet<>();
        long maxm = 0, sum = 0;
        int count = 0;
        for (int num : nums) {
            while (set.contains(num) || set.size() == k) {
                set.remove(nums[count]);
                sum -= nums[count++];
            }
            sum += num;
            set.add(num);

            if (set.size() == k) {
                maxm = Math.max(maxm, sum);
            }
        }
        return maxm;
    }

    public static void main(String[] args) {

        //Maximum Sum of Distinct Subarrays With Length K
        int[] nums = {1,5,4,2,9,9,9};
        int k =3;
        System.out.println(maximumSubarraySum(nums,k));
    }
}
