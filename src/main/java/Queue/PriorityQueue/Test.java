package Queue.PriorityQueue;

import java.util.Arrays;
import java.util.PriorityQueue;

public class Test {
    public static int[] fun(int[] arr) {
        int[] res = new int[3];
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        for (int i = 0; i < arr.length; i++) {
            heap.add(arr[i]);
        }
        for (int i = 0; i < 3; i++) {
            res[i] = heap.poll();
        }
        return res;
    }

    public static void main(String[] args) {
        int arr[] = {30, 20, 10, 100, 33, 12};
        System.out.println(Arrays.toString(fun(arr)));
    }
}
