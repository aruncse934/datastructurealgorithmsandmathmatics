package sorting;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortAlgorithmsTest {


    @Test
    public void TestCase1(){
        int actual[] = { 5, 1, 6, 2, 3, 4 };
        int[] expected = { 1, 2, 3, 4, 5, 6 };
        MergeSortAlgorithms.mergeSort(actual,actual.length);
        Assert.assertArrayEquals(expected,actual);
    }
}