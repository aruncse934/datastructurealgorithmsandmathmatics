# Software Architect and Design ::
----------------------------------

Software Design:: - low level 
-------------------

Software Architect:: - High level
--------------------

Q1: What Is Load Balancing?  Related To: Load Balancing
Add to PDF Entry 
Q2: What Is CAP Theorem?  Related To: CAP Theorem
Add to PDF Entry 
Q3: Define Microservice Architecture  Related To: Microservices
Add to PDF Junior 
Q4: Why use WebSocket over Http?  Related To: WebSockets
Add to PDF Junior 
Q5: What do you mean by lower latency interaction?  Related To: WebSockets
Add to PDF Junior 
Q6: What Is Scalability?  Related To: Scalability
Add to PDF Junior 
Q7: Why Do You Need Clustering?  
Add to PDF Junior 
Q8: What Is A Cluster?  
Add to PDF Junior 
Q9: What is Domain Driven Design?  Related To: DDD
Add to PDF Junior 
Q10: What defines a software architect?  
Add to PDF Junior 
Q11: What is meant by the KISS principle?  
Add to PDF Junior 
Q12: Why is it a good idea for “lower” application layers not to be aware of “higher” ones?  Related To: Layering & Middleware
Add to PDF Junior 
Q13: What is Test Driven Development?  Related To: Agile & Scrum
Add to PDF Junior 
Q14: What does the expression “Fail Early” mean, and when would you want to do so?  
Add to PDF Junior 
Q15: What does “program to interfaces, not implementations” mean?  Related To: Design Patterns
 Add to PDF Mid 
Q16: What is Elasticity (in contrast to Scalability)?  Related To: Scalability
 Add to PDF Mid 
Q17: What is Back-Pressure?  Related To: Availability & Reliability
 Add to PDF Mid 
Q18: WebSockets vs Rest API for real time data? Which to choose?  Related To: API Design, WebSockets, REST & RESTful
 Add to PDF Mid 
Q19: What is the difference between Monolithic, SOA and Microservices Architecture?  Related To: Microservices, SOA
 Add to PDF Mid 
Q20: What Is Session Replication?  
 Add to PDF Mid 
Q21: What Is Middle Tier Clustering?  Related To: Layering & Middleware
 Add to PDF Mid 
Q22: How Do You Update A Live Heavy Traffic Site With Minimum Or Zero Down Time?  Related To: Availability & Reliability
 Add to PDF Mid 
Q23: What Is ACID Property Of A System?  Related To: Databases
 Add to PDF Mid 
Q24: What Is Sticky Session Load Balancing? What Do You Mean By "Session Affinity"?  Related To: Load Balancing
 Add to PDF Mid 
Q25: What Do You Mean By High Availability (HA)?  Related To: Availability & Reliability
 Add to PDF Mid 
Q26: What does it mean "System Shall Be Resilient"?  Related To: Availability & Reliability
 Add to PDF Mid 
Q27: What is a Model in DDD?  Related To: DDD
 Add to PDF Mid 
Q28: What is Domain in DDD?  Related To: DDD
 Add to PDF Mid 
Q29: Explain the Single Responsibility Principle (SRP)?   
 Add to PDF Mid 
Q30: What is difference between fault tolerance and fault resilience?  
 Add to PDF Mid 
Q31: What is the difference between Concurrency and Parallelism?  Related To: Concurrency
 Add to PDF Mid 
Q32: What is the difference between DTOs and ViewModels in DDD?  Related To: DDD
 Add to PDF Mid 
Q33: What Is Load Balancing Fail Over?  Related To: Load Balancing
 Add to PDF Mid 
Q34: What are the DRY and DIE principles?  
 Add to PDF Mid 
Q35: What does SOLID stand for? What are its principles?  
 Add to PDF Mid 
Q36: Is it better to return NULL or empty values from functions/methods where the return value is not present?  
 Add to PDF Mid 
Q37: "People who like this also like... ". How would you implement this feature in an e-commerce shop?  
 Add to PDF Mid 
Q38: How can you keep one copy of your utility code and let multiple consumer components use and deploy it?  
 Add to PDF Mid 
Q39: Name some Performance Testing best practices  Related To: Software Testing
 Add to PDF Mid 
Q40: Name some Performance Testing metrics to measure  Related To: Software Testing
 Add to PDF Mid 
Q41: Two customers add a product to the basket in the same time whose the stock was only one (1). What will you do?   Related To: Concurrency
 Add to PDF Senior 
Q42: Explain Failure in Contrast to Error  Related To: Availability & Reliability
 Add to PDF Senior 
Q43: Provide Definition Of Location Transparency  
 Add to PDF Senior 
Q44: What Is Sharding?  Related To: Databases
 Add to PDF Senior 
Q45: Why layering your application is important? Provide some bad layering example.  Related To: Layering & Middleware
 Add to PDF Senior 
Q46: What's the difference between principles YAGNI and KISS?  
 Add to PDF Senior 
Q47: What is GOD class and why should we avoid it?  
 Add to PDF Senior 
Q48: Why should you structure your solution by components?  Related To: Layering & Middleware
 Add to PDF Senior 
Q49: What Is BASE Property Of A System?  Related To: Databases, NoSQL
 Add to PDF Senior 
Q50: Explain threads to your grandparents  
 Add to PDF Senior 
Q51: How to handle exceptions in a layered application?  Related To: Layering & Middleware
 Add to PDF Senior 
Q52: Why should I isolate my domain entities from my presentation layer?  Related To: Layering & Middleware
 Add to PDF Senior 
Q53: What Is IP Address Affinity Technique For Load Balancing?  Related To: Load Balancing
 Add to PDF Senior 
Q54: What is actor model in context of a programming language?  
 Add to PDF Senior 
Q55: Defend the monolithic architecture.  
 Add to PDF Senior 
Q56: What is Unit test, Integration Test, Smoke test, Regression Test and what are the differences between them?   Related To: Software Testing
 Add to PDF Senior 
Q57: How do you off load work from the Database?  Related To: Databases
 Add to PDF Senior 
Q58: Explain what is Cache Stampede  Related To: Caching
 Add to PDF Senior 
Q59: Compare "Fail Fast" vs "Robust" approaches of building software  Related To: Availability & Reliability
 Add to PDF Senior 
Q60: What does Amdahl's Law mean?  Related To: Reactive Systems
 Add to PDF Expert 
Q61: What is the most accepted transaction strategy for microservices?  Related To: Microservices
 Add to PDF Expert 
Q62: What is the difference between Cohesion and Coupling?  Related To: Microservices
 Add to PDF Expert 
Q63: Why is writing software difficult? What makes maintaining software hard?  
 Add to PDF Expert 
Q64: Are you familiar with The Twelve-Factor App principles?  
 Add to PDF Expert 
Q65: What are heuristic exceptions?  
 Add to PDF Expert 
Q66: What Does Eventually Consistent Mean?  Related To: Databases
 Add to PDF Expert 
Q67: What Is Shared Nothing Architecture? How Does It Scale?  
 Add to PDF Expert 
Q68: How do I test a private function or a class that has private methods, fields or inner classes?  Related To: Software Testing, OOP
 Add to PDF Expert 
Q69: What are best practices for caching paginated results whose ordering/properties can change?  Related To: Caching
 Add to PDF Expert 
Q70: Cache miss-storm: Dealing with concurrency when caching invalidates for high-traffic sites  Related To: Caching
 Add to PDF Expert 
Code Challenges
Q1: Could you provide an example of the Single Responsibility Principle?  