#Data Structure & Algorithms ::
--------------------------------------------------------------------------------------------------------
Arrays::-
-----
Array is a collection of homogeneous data elements. It is a very simple data structure. The elements of an array are stored in successive memory location. Array is refered by a name and index number. Array is nice because of their simplicity and well suited for situations where the number is known. Array operation :

Traverse
Insert
Delete
Sort
Search


There are two types of array. One dimensional array and multi dimensional array.One dimensional array This type of array of array represent and strore in linear form. Array index start with zero.

Declaration : datatype arrayname[size];
                        int arr[10];

Input array : for(int i=0; i<10; i++)  cin>>arr[i];

We can use store integer type of data to the array arr using above segment.

----------

Traverse : Traversing can easy in linera array. Algorithm:


C++ implement :
void traverse(int arr[])
{
       for(int i=0; i<10; i++)   cout<<arr[i];
}

----------

Insertion : Inserting an element at the end of a linear array can be easily done provided the memory space space allocated for the array is large enough to accommodate the additional element. Inserting an element in the middle . . Algorithm : Insertion(arr[], n, k, item) here arr is a linear array with n elements and k is index we item insert. This algorithm inserts an item to kth in index in arr.

Step 1:Start
Step 2: Repeat for i=n-1 down to k(index)
   Shift the element dawn by one position] arr[i+1]=arr[i];
  [End of the loop]
Step 3: set arr[k] = item
Step 4: n++; Step 5 : Exit.


C++ implement :
void insert(int arr[], int n, int k, int item)
{
   for(int i=n-1; i>=k; i--)
      {
         arr[i]=arr[i+1];
      }
      arr[k] = item;
      n++;
}

----------

Deletion : Deletion is very easy on linear array.

Algorithm : Deletion(arr, n, k) Here arr is a linear array with n number of items. K is position of elememt which can be deleted.
Step 1:Start
Step 2: Repeat for i=k upto n
       [Move the element upword]  arr[i]=arr[i+1];
      [End of the loop]
Step 3: n--;
Step 4 : Exit.


C++ implementation :
 void deletion(int arr[], int n, int k)
 {
      for(int i=k; i<n; i++)
       {
            arr[i] = arr[i+1];
       }
      n--;
  }

----------

Searching : Searching means find out a particular element in linear array. Linear seach and binary search are common algorithm for linear array. We discuss linear search and binary search.

Linear search Algorithm : Linear search is a simple search algorithm that checks every record until it finds the target value
Algorithm: LinearSeach(arr, n, item)
Step 1:Start.
Step 2: Initialize loc=0;
Step 3: Repeat for i=0 upto n-1 if(arr[i]==item) loc++; [End of the loop]
Step 4: if loc is not zero then print found otherwise print not found.
Step 5 : Exit.

C++ implementation :
  void linear search(int arr[], int n, item)
   {
      for(int i=0; i<n-1; i++)
       {
         if(arr[i]==item) loc++;
       }

      if(loc) cout<<"Found"<<endl;
      else cout<<"Not found"<<endl
   }

   -------------------------------------------------------------------
			Two Sum II - Input array is sorted
			----------------------------------
			Input: numbers = [2,7,11,15], target = 9
			Output: [1,2]
			Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.

Implementation::::--

			public int[] twoSum(int[] nums, int target) {
				int left=0;
				int right = nums.length-1;
				while(nums[left] + nums[right] != target){
				if(nums[left] + nums[right] >target)
					right--;
				else
					left++;
				}
				return new int[]{left+1,right+1};
			}



----------

Binary search : Binary search is available for sorted array. It compares the target value to the middle element of the array; if they are unequal,
the half in which the target cannot lie is eliminated and the search continues on the remaining half until it is successful.

Algorithm : BinarySeach(arr, n, item)
Step 1:Start
Step 2: Initialize low = 0 and high = n-1;
Step 3: While loop low<=high
    mid = (low + high)/2;
    if (a[mid] == item) return mid;
    else if (a[mid] < item) low = mid + 1;
     else high = mid - 1;
Step 4: If item is not found in array return -1. Step 5: End.


C++ implementation :
 int binarySearch(int[] a, int n, int item)
 {
    int low = 0;
    int high = n - 1;
    while(low<=high){
    {
      int mid = (low + high)/2;
      if (a[mid] == item) return mid;
      else if (a[mid] < item) low = mid + 1;
      else high = mid - 1;l
    }
    return -1;
 }


--------------------
Sorting : There are various sorting algorithm in linear array. We discuss bubble sort and quick sort in this post.
Bubble Sort: Bubble sort is a example of sorting algorithm . In this method we at first compare the data element in the first position with the second position and arrange them in desired order.Then we compare the data element with with third data element and arrange them in desired order. The same process continuous until the data element at second last and last position.

Algorithm : BubbleSort(arr,n)
Step 1:Start
Step 2: Repeats i=0 to n
Step 3: Repeats j=0 to n
         if(arr[j]>arr[j+1]) then interchange arr[j] and arr[j+1]
         [End of inner loop]
        [End of outer loop]
Step 4: Exit.

C++ implement :
 void BubbleSort(int arr, int n)
 {
    for(int i=0; i<n-1; i++)
    {
        for(int j=0; j<n-1; j++)
        {
           if(arr[j]>arr[j+1])     swap(arr[j],arr[j+1]);
        }
     }
 }

Quick Sort:
 Quick sort is a divide and conquer paradism. In this paradism one element is to be chosen as partitining element .
We divide the whole list array into two parts with respect to the partitiong elemnt . The data which are similar than or equal to the partitining element remain in
 the first part and data data which are greater than the partitioning element
 remain in the second part. If we find any data which is greater than the partitioning value that will be transfered to the second part., If we find any data whichis smaller than the partitioning element that will be transferred to first part.
Transferring the data have been done by exchanging the position of the the data
found in first and second part. By repeating this process ,
we can sort the whole list of data.

Algorithm: QUICKSORT(arr, l, h)
if  l<h then pi ← PARTITION(A, l, h)
QUICKSORT(A, l, pi–1)
QUICKSORT(A, pi+1, h)

C++ implementation :

 int partition(int arr[], int start, int end)
 {
    int pivotValue = arr[start];
    int pivotPosition = start;
     for (int i=start+1; i<=end; i++)
     {
        if (pivotValue > arr[i])
       {
          swap(arr[pivotPosition+1], arr[i]);
          swap(arr[pivotPosition] , arr[pivotPosition+1]);
          pivotPosition++;
       }
     }
    return pivotPosition;
  }


 void quickSort(int arr[], int low, int high)
 {
   if (low < high)
   {
      int pi = partition(arr, low, high);
      quickSort(arr, low, pi - 1);
      quickSort(arr, pi + 1, high);
   }
 }






C++ example for simple sorting program with stl function:

 #include <bits/stdc++.h>
 using namespace std;

 int main()
 {
   int  n, arr[100];
   cin >> n;
   for(int i=0; i<n; i++)
   {
     cin>>arr[i];
   }

   sort(arr, arr + n);

   for (int i=0; i<n; i++)
   {
      cout<<arr[i]<<" ";
   }
   cout<<endl;

   return 0;
 }

 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1.How To Find Duplicates In Array In Java?
----------------------------------------
public static void findDuplicate(String[] str){
Set<String> set = new HashSet<String>();
for(String s : str){
if(set.add(s) ==false){
    Sopln(s);
  }
 }
}

--------------------------------------------------------------------------------------------------------------------------
Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.

Example 1:

Input: [3,2,1,5,6,4] and k = 2
Output: 5
Example 2:

Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4
Note:
You may assume k is always valid, 1 ≤ k ≤ array's length.

---------------------------------------------
You can take a couple of approaches to actually solve it:

O(N lg N) running time + O(1) memory
The simplest approach is to sort the entire input array and then access the element by it's index (which is O(1)) operation:

public int findKthLargest(int[] nums, int k) {
        final int N = nums.length;
        Arrays.sort(nums);
        return nums[N - k];
}
O(N lg K) running time + O(K) memory
Other possibility is to use a min oriented priority queue that will store the K-th largest values. The algorithm iterates over the whole input and maintains the size of priority queue.

public int findKthLargest(int[] nums, int k) {

    final PriorityQueue<Integer> pq = new PriorityQueue<>();
    for(int val : nums) {
        pq.offer(val);

        if(pq.size() > k) {
            pq.poll();
        }
    }
    return pq.peek();
}
O(N) best case / O(N^2) worst case running time + O(1) memory
The smart approach for this problem is to use the selection algorithm (based on the partion method - the same one as used in quicksort).

public int findKthLargest(int[] nums, int k) {

        k = nums.length - k;
        int lo = 0;
        int hi = nums.length - 1;
        while (lo < hi) {
            final int j = partition(nums, lo, hi);
            if(j < k) {
                lo = j + 1;
            } else if (j > k) {
                hi = j - 1;
            } else {
                break;
            }
        }
        return nums[k];
    }

    private int partition(int[] a, int lo, int hi) {

        int i = lo;
        int j = hi + 1;
        while(true) {
            while(i < hi && less(a[++i], a[lo]));
            while(j > lo && less(a[lo], a[--j]));
            if(i >= j) {
                break;
            }
            exch(a, i, j);
        }
        exch(a, lo, j);
        return j;
    }

    private void exch(int[] a, int i, int j) {
        final int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    private boolean less(int v, int w) {
        return v < w;
    }
O(N) guaranteed running time + O(1) space

So how can we improve the above solution and make it O(N) guaranteed? The answer is quite simple, we can randomize the input, so that even when the worst case input would be provided the algorithm wouldn't be affected. So all what it is needed to be done is to shuffle the input.

public int findKthLargest(int[] nums, int k) {

        shuffle(nums);
        k = nums.length - k;
        int lo = 0;
        int hi = nums.length - 1;
        while (lo < hi) {
            final int j = partition(nums, lo, hi);
            if(j < k) {
                lo = j + 1;
            } else if (j > k) {
                hi = j - 1;
            } else {
                break;
            }
        }
        return nums[k];
    }

private void shuffle(int a[]) {

        final Random random = new Random();
        for(int ind = 1; ind < a.length; ind++) {
            final int r = random.nextInt(ind + 1);
            exch(a, ind, r);
        }
    }

    -----------------------------------------------------------------
------------------------------------------------------------------------------------------------------

=======================================================================================================================================================================================
Prifix Array::::-
-----------------

------------------------------------------------------------------------------------------------------------------------
Sufix Array:::-
----------------


------------------------------------------------------------------------------------------------------------------------
Pascal's Triangle::
--------------------
Input: 5
Output:
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]

  public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList();
        List<Integer> row = new ArrayList();
        for(int i = 0; i < numRows; i++) {
            for(int j = row.size() - 1; j >= 1 ; j--) {
                row.set(j, row.get(j) + row.get(j - 1));
            }
            row.add(1);
            res.add(new ArrayList(row));
        }
        return res;
    }
=======================================================================================================================================================================================
Sorting:::-
-------------
1. Merge Sort::- Merge sort is a “divide and conquer” algorithm where in we first divide the problem into subproblems.
Time Complexity::-O(nLogn). Space Complexity:: O(n).
---------------------------------------------------
class MergeSort
{
	void merge(int arr[], int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;
		int L[] = new int [n1];
		int R[] = new int [n2];

		for (int i=0; i<n1; ++i)
			L[i] = arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = arr[m + 1+ j];

		int i = 0, j = 0;
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i] <= R[j])
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
			}
			k++;
		}
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
	}
	void sort(int arr[], int l, int r)
	{
		if (l < r)
		{
			int m = (l+r)/2;
			sort(arr, l, m);
			sort(arr , m+1, r);
			merge(arr, l, m, r);
		}
	}
	static void printArray(int arr[])
	{
		int n = arr.length;
		for (int i=0; i<n; ++i)
			System.out.print(arr[i] + " ");
		System.out.println();
	}
	public static void main(String args[])
	{
		int arr[] = {12, 11, 13, 5, 6, 7};

		System.out.println("Given Array");
		printArray(arr);

		MergeSort ob = new MergeSort();
		ob.sort(arr, 0, arr.length-1);

		System.out.println("\nSorted array");
		printArray(arr);
	}
}



---------------------------------------------------
2.Quick Sort :::- is Divide and Conquer Algorithms
--------------------------------------------------



3. Heap Sort::::-
------------------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------
3. Topological Sorting::
-------------------------------------------------
-------------
Strings:::-
------------
First Unique Character in a String
-----------------------------------
s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Approach 1: Linear time solution:
---------------------------------
    public int firstUniqChar(String s) {
        HashMap<Character, Integer> count = new HashMap<Character, Integer>();
        int n = s.length();
        // build hash map : character and how often it appears
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            count.put(c, count.getOrDefault(c, 0) + 1);
        }

        // find the index
        for (int i = 0; i < n; i++) {
            if (count.get(s.charAt(i)) == 1)
                return i;
        }
        return -1;
    }
----------------------------------------------------------------
Manacher's Algorithm::-
---------------------------------------------------------------------
Intuition::
Manacher's algorithm is a textbook algorithm that finds in linear time, the maximum size palindrome for any possible palindrome center. If we had such an algorithm,
finding the answer is straightforward.

What follows is a discussion of why this algorithm works.

Algorithm
---------
Our loop invariants will be that center, right is our knowledge of the palindrome with the largest right-most boundary with center < i, centered at center with right-boundary right.
Also, i > center, and we've already computed all Z[j]'s for j < i.

When i < right, we reflect i about center to be at some coordinate j = 2 * center - i. Then, limited to the interval with radius right - i and center i, the situation for Z[i] is
the same as for Z[j].

For example, if at some time center = 7, right = 13, i = 10, then for a string like A = '@#A#B#A#A#B#A#'`, the `center` is at the `'#'` between the two middle `'A'`'s,
the right boundary is at the last `'#'`, `i` is at the last `'B'`, and `j` is at the first `'B'`. Notice that limited to the interval `[center - (right - center), right]`
(the interval with center `center` and right-boundary `right`), the situation for `i` and `j` is a reflection of something we have already computed. Since we already
know `Z[j] = 3`, we can quickly find `Z[i] = min(right - i, Z[j]) = 3`. Now, why is this algorithm linear? The while loop only checks the condition more than once
when `Z[i] = right - i`. In that case, for each time `Z[i] += 1`, it increments `right`, and `right` can only be incremented up to `2*N+2` times. Finally, we sum up `(v+1) / 2` for
each `v in Z`. Say the longest palindrome with some given center C has radius R. Then, the substring with center C and radius R-1, R-2, R-3, ..., 0 are also palindromes.
Example: `abcdedcba` is a palindrome with center `e`, radius 4: but `e`, `ded`, `cdedc`, `bcdedcb`, and `abcdedcba` are all palindromes. We are dividing by 2 because we were using
half-lengths instead of lengths. For example we actually had the palindrome `a#b#c#d#e#d#c#b#a`, so our length is twice as big.

public int countSubstrings(String S) {
        char[] A = new char[2 * S.length() + 3];
        A[0] = '@';
        A[1] = '#';
        A[A.length - 1] = '$';
        int t = 2;
        for (char c: S.toCharArray()) {
            A[t++] = c;
            A[t++] = '#';
        }

        int[] Z = new int[A.length];
        int center = 0, right = 0;
        for (int i = 1; i < Z.length - 1; ++i) {
            if (i < right)
                Z[i] = Math.min(right - i, Z[2 * center - i]);
            while (A[i + Z[i] + 1] == A[i - Z[i] - 1])
                Z[i]++;
            if (i + Z[i] > right) {
                center = i;
                right = i + Z[i];
            }
        }
        int ans = 0;
        for (int v: Z) ans += (v + 1) / 2;
        return ans;
    }

or

Idea is start from each index and try to extend palindrome for both odd and even length.

public class Solution {
    int count = 0;

    public int countSubstrings(String s) {
        if (s == null || s.length() == 0) return 0;

        for (int i = 0; i < s.length(); i++) { // i is the mid point
            extendPalindrome(s, i, i); // odd length;
            extendPalindrome(s, i, i + 1); // even length
        }

        return count;
    }

    private void extendPalindrome(String s, int left, int right) {
        while (left >=0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            count++; left--; right++;
        }
    }
}


**Complexity Analysis** * Time Complexity: O(N)O(N) where whereNN is the length of `S`. As discussed above, the complexity is linear.
* Space Complexity: isthelengthof‘S‘.Asdiscussedabove,thecomplexityislinear.∗SpaceComplexity:O(N)O(N)$, the size ofAandZ`.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------
Searching:::
------------

------------
Dictionaries and HashMaps:::
----------------------------

----------------------------
Greedy Algorithms:::
----------------------

================================================================================================================================================================================================
-------------------------
Two Pointer Algorithm::
-------------------------
================================================================================================================================================================================================
Valid Palindrome::
-------------------
 //Two Pointer
    public static boolean validPalindrome(String s){
        char[] ch =s.toCharArray();
        int i =0;
        int j = ch.length-1;
        //for(int i = 0,j= ch.length-1;i<j; ){
        while (i<j){
            if(!Character.isLetterOrDigit(ch[i]))
                i++;
            else if(!Character.isLetterOrDigit(ch[j]))
                j--;
            else if(Character.toLowerCase(ch[i++]) != Character.toLowerCase(ch[j--]))
                return false;
        }
        return true;
    }


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
=====================================================================================================================================================================================
Dynamic Programming:::-
=====================================================================================================================================================================================
A method for solving complex problems by breaking them up into sub-problems first. This technique can be used when a given problem can be split into overlapping sub-problems and when there is an optimal sub-structure to the problem. Often used in optimization problems (max,min) - some examples of which are given later.

(Informally) generally used for problems of the type :  f(x)= Some function of ( f(x-a), f(x-b) ... etc. )
Simple Example :
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Easy::

Kadane's Algorithm — (Dynamic Programming) — For new Solvers! TC O(n)
---------------------------------------------------------------------

Initialize: max_so_far = 0
max_ending_here = 0
Loop for each element of the array
(a) max_ending_here = max_ending_here + a[i]
(b) if(max_ending_here < 0) max_ending_here = 0
(c) if(max_so_far < max_ending_here) max_so_far = max_ending_here
return max_so_far;

Explanation:
-------------Simple idea of the Kadane's algorithm is to look for all positive contiguous segments of the array (max_ending_here is used for this).
And keep track of maximum sum contiguous segment among all positive segments (max_so_far is used for this). Each time we get a positive sum compare it with max_so_far and update max_so_far if it is greater than max_so_far

Lets take the example:
{-2, -3, 4, -1, -2, 1, 5, -3}

for(int i = 0; i < N; i++){
max_ending_here += Number[i];
        if(max_ending_here > max_so_far)max_so_far = max_ending_here;
        if(max_ending_here < 0)max_ending_here = 0;
    }

------------------------------------------------------
Maximum Subarray::-Largest Sum Contiguous SubarrayS
------------------------------------------------------
Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6
---------------
KadaneAlgo::

static int maxSubArraySum(int a[]){
        int n = a.length;
        int max_so_far = Integer.MIN_VALUE, max_ending_here = 0;
        for (int i = 0; i < n; i++){
            max_ending_here = max_ending_here + a[i];
            if (max_so_far < max_ending_here)
                max_so_far = max_ending_here;
            if (max_ending_here < 0)
                max_ending_here = 0;
        }
        return max_so_far;
    }
}

or
DP sol::-
public static int maxSubArray(int[] A){
    int maxCurr = A[0];
    int maxEnd = A[0];
	for(int i = 1;i<A.length;i++){
	maxEnd = Math.max(maxEnd+A[i],A[i]);
	maxCurr = Math.max(maxCurr,maxEnd);
	}
	return maxCurr;
}
--------------------------------------------------------------------------
Minimum Size Subarray Sum::
------------------------------------------------------------------------
Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under the problem constraint.
------------------------------------------------------------------------------------
public int minSubArrayLen(int s, int[] a) {
  if (a == null || a.length == 0)
    return 0;

  int i = 0, j = 0, sum = 0, min = Integer.MAX_VALUE;

  while (j < a.length) {
    sum += a[j++];

    while (sum >= s) {
      min = Math.min(min, j - i);
      sum -= a[i++];
    }
  }

  return min == Integer.MAX_VALUE ? 0 : min;
}
------------------------------------------------------------------------------------------------------------------------------------
Smallest subarray with sum greater than a given value:::
--------------------------------------------------------------
arr[] = {1, 4, 45, 6, 0, 19}
   x  =  51
Output: 3
Minimum length subarray is {4, 45, 6}
--------------------------------------------------------------------------------------------------------------------------------
   static int smallestSubWithSum(int arr[], int n, int x){
        int curr_sum = 0, min_len = n + 1;
        int start = 0, end = 0;
        while (end < n){
            while (curr_sum <= x && end < n)
                curr_sum += arr[end++];
            while (curr_sum > x && start < n)
            {
                if (end - start < min_len)
                    min_len = end - start;
                curr_sum -= arr[start++];
            }
        }
        return min_len;
    }
----------------------------------------------------------------------------------------------------------------------------------
Smallest subarray with sum greater than a given value(Negative Element)
----------------------------------------------------------------------------------------------------------------------------------
static int smallestSubWithSum(int arr[],int n, int x) {
    int curr_sum = 0, min_len = n + 1;

    int start = 0, end = 0;
    while (end < n){
        while (curr_sum <= x && end < n){
            if (curr_sum <= 0 && x > 0){
                start = end;
                curr_sum = 0;
            }

            curr_sum += arr[end++];
        }
        while (curr_sum > x && start < n){
            if (end - start < min_len)
                min_len = end - start;
            curr_sum -= arr[start++];
        }
    }
    return min_len; I have attached a copy of my updated resume that details my projects and experience in software development.
}
-------------------------------------------------------------------------------------------------------------------------------------
 Decode Ways::::
 ------------------------------------
 Input: "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).
----------------------------------------------------------------------------------
 public int numDecodings(String s) {
        int n = s.length();
        if (n == 0) return 0;

        int[] memo = new int[n+1];
        memo[n]  = 1;
        memo[n-1] = s.charAt(n-1) != '0' ? 1 : 0;

        for (int i = n - 2; i >= 0; i--)
            if (s.charAt(i) == '0') continue;
            else memo[i] = (Integer.parseInt(s.substring(i,i+2))<=26) ? memo[i+1]+memo[i+2] : memo[i+1];

        return memo[0];
    }
-----------------------------------------------------------------------------------------------------------
Decode Ways II :::
-----------------------------------------
Input: "*"
Output: 9
Explanation: The encoded message can be decoded to the string: "A", "B", "C", "D", "E", "F", "G", "H", "I".
Example 2:
Input: "1*"
Output: 9 + 9 = 18
-----------------------------------------------------------------------------------------------------------------
class Solution {
    int M = 1000000007;
    public int numDecodings(String s) {
        long first = 1, second = s.charAt(0) == '*' ? 9 : s.charAt(0) == '0' ? 0 : 1;
        for (int i = 1; i < s.length(); i++) {
            long temp = second;
            if (s.charAt(i) == '*') {
                second = 9 * second;
                if (s.charAt(i - 1) == '1')
                    second = (second + 9 * first) % M;
                else if (s.charAt(i - 1) == '2')
                    second = (second + 6 * first) % M;
                else if (s.charAt(i - 1) == '*')
                    second = (second + 15 * first) % M;
            } else {
                second = s.charAt(i) != '0' ? second : 0;
                if (s.charAt(i - 1) == '1')
                    second = (second + first) % M;
                else if (s.charAt(i - 1) == '2' && s.charAt(i) <= '6')
                    second = (second + first) % M;
                else if (s.charAt(i - 1) == '*')
                    second = (second + (s.charAt(i) <= '6' ? 2 : 1) * first) % M;
            }
            first = temp;
        }
        return (int) second;
    }

}
------------------------------------------------------------------------------------------------------------------------------
Maximum Product Subarray:::
--------------------------------------------------
Input: [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
--
Input: [-2,0,-1]
Output: 0
Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
---------------------------------------------------------------------------------
public int maxProduct(int[] A) {
    if (A.length == 0) {
        return 0;
    }

    int maxherepre = A[0];
    int minherepre = A[0];
    int maxsofar = A[0];
    int maxhere, minhere;

    for (int i = 1; i < A.length; i++) {
        maxhere = Math.max(Math.max(maxherepre * A[i], minherepre * A[i]), A[i]);
        minhere = Math.min(Math.min(maxherepre * A[i], minherepre * A[i]), A[i]);
        maxsofar = Math.max(maxhere, maxsofar);
        maxherepre = maxhere;
        minherepre = minhere;
    }
    return maxsofar;
}
------------------------------------------------------------------------------------------------------------------------------------
Longest Palindromic Substring :::
----------------------------------
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.

Input: "cbbd"
Output: "bb"
---------------------------------------
class Solution {
    private int lo, maxLen;
public String longestPalindrome(String s) {
	int len = s.length();
	if (len < 2)
		return s;

    for (int i = 0; i < len-1; i++) {
     	extendPalindrome(s, i, i);
     	extendPalindrome(s, i, i+1);
    }
    return s.substring(lo, lo + maxLen);
}
private void extendPalindrome(String s, int j, int k) {
	while (j >= 0 && k < s.length() && s.charAt(j) == s.charAt(k)) {
		j--;
		k++;
	}
	if (maxLen < k - j - 1) {
		lo = j + 1;
		maxLen = k - j - 1;
	}
}
}
----------------------------------------------------------------------------------------------
Shortest Palindrome:::
-----------------------
Given a string s, you are allowed to convert it to a palindrome by adding characters in front of it. Find and return the shortest palindrome you can find by performing this transformation.

Input: "aacecaaa"
Output: "aaacecaaa"

Input: "abcd"
Output: "dcbabcd"
-----------------------------------------------------
 public String shortestPalindrome(String s) {
     int j = 0;
    for (int i = s.length() - 1; i >= 0; i--) {
        if (s.charAt(i) == s.charAt(j)) { j += 1; }
    }
    if (j == s.length()) { return s; }
    String suffix = s.substring(j);
    return new StringBuffer(suffix).reverse().toString() + shortestPalindrome(s.substring(0, j)) + suffix;
    }
------------------------------------------------------------------------------------------------------------


--------------------
Tries:::
------------------

------------------
Graphs::::
-------------------------

-------------------------
===========================================================================================================================================================================================
Trees:::
===========================================================================================================================================================================================
Binary Trees/ N-ary Trees::: T.C O(n)
-------------------------------------
A binary tree is a structure comprising nodes, where each node has the following 3 components:

->Data element: Stores any kind of data in the node
->Left pointer: Points to the tree on the left side of node
->Right pointer: Points to the tree on the right side of the node

As the name suggests, the data element stores any kind of data in the node.
The left and right pointers point to binary trees on the left and right side of the node respectively.

->If a tree is empty, it is represented by a null pointer.

Commonly-used terminologies
----------------------------
->Root: Top node in a tree
->Child: Nodes that are next to each other and connected downwards
->Parent: Converse notion of child
->Siblings: Nodes with the same parent
->Descendant: Node reachable by repeated proceeding from parent to child
->Ancestor: Node reachable by repeated proceeding from child to parent.
->Leaf: Node with no children
->Internal node: Node with at least one child
->External node: Node with no children

Application of trees::
-----------------------
->a Manipulate hierarchical data
->Make information easy to search (see tree traversal)
->Manipulate sorted lists of data
->Use as a workflow for compositing digital images for visual effects
->Use in router algorithms


/* Class containing left and right child of current
node and key value*/
class Node
{
	int key;
	Node left, right;

	public Node(int item)
	{
		key = item;
		left = right = null;
	}
}

===========================================================================================================================================================================================
Binary Search Tree::- BST is a node based binary tree data structure.
----------------------
For a binary tree to be a binary search tree, the data of all the nodes in the left sub-tree of the root node should be  the data of the root.
The data of all the nodes in the right subtree of the root node should be  the data of the root.

Basic operations of a Binary Search Tree::-
-------------------------------------------
-> Searching of elements 		O(log n)	O(n)
-> Insertion of elements 		O(log n)	O(n)
-> deletion of elements		    O(log n)	O(n)
-> Traversal of elements   		O(log n)	O(n)
       -----------------
       (a)Pre-order Traversal − Traverses a tree in a pre-order manner.
       (b)In-order Traversal − Traverses a tree in an in-order manner.
       (c)Post-order Traversal − Traverses a tree in a post-order manner.
Example


In Fig. 1, consider the root node with data = 10.
->Data in the left subtree is: [5,1,6]
->All data elements are  < 10
->Data in the right subtree is: [19,17].
->All data elements are > 10.

ex :: [ left_subtree (keys)  ≤  node (key)  ≤  right_subtree (keys) ]

 [5,1,6] <= 10 <= [19,17].

Also, considering the root node with data = 5 , its children also satisfy the specified ordering. Similarly, the root node with data =19 also satisfies this ordering.
When recursive, all subtrees satisfy the left and right subtree ordering.

The tree is known as a Binary Search Tree or BST.

Traversing the tree::
---------------------
There are mainly three types of tree traversals.

Pre-order traversal:
--------------------
In this traversal technique the traversal order is root-left-right i.e.
->Process data of root node
->First, traverse left subtree completely
->Then, traverse right subtree

    void perorder(struct node*root)
    {
        if(root)
        {
            printf("%d ",root->data);    //Printf root->data
            preorder(root->left);    //Go to left subtree
            preorder(root->right);     //Go to right subtree
        }
    }

Post-order traversal:
---------------------
In this traversal technique the traversal order is left-right-root.

->Process data of left subtree
->First, traverse right subtree
->Then, traverse root node

    void postorder(struct node*root){
        if(root){
            postorder(root->left);    //Go to left sub tree
            postorder(root->right);     //Go to right sub tree
            printf("%d ",root->data);    //Printf root->data
        }
    }

In-order traversal
-------------------
In in-order traversal, do the following:

->First process left subtree (before processing root node)
->Then, process current root node
->Process right subtree

    void inorder(struct node*root){
        if(root){
            inorder(root->left);    //Go to left subtree
            printf("%d ",root->data);    //Printf root->data
            inorder(root->right);     //Go to right subtree
        }
    }

Consider the in-order traversal of a sample BST

->The 'inorder( )' procedure is called with root equal to node with data = 10
->Since the node has a left subtree, 'inorder( )' is called with root equal to node with data= 5
->Again, the node has a left subtree, so 'inorder( )' is called with data = 1
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BSTGreaterSumTree::
------------------
Given the root of a binary search tree with distinct values, modify it so that every node has a new value equal to the sum of the values of the original tree
that are greater than or equal to node.val.

As a reminder, a binary search tree is a tree that satisfies these constraints:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.


Example 1:



Input: [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]


Note:

The number of nodes in the tree is between 1 and 100.
Each node will have value between 0 and 100.
The given tree is a binary search tree.

Example::
---------
 class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(Integer x) { val = x; }
  }

public class BSTGreaterSumTree {

    int pre = 0;
    public TreeNode bstToGst(TreeNode root) {
        if (root.right != null)
            bstToGst(root.right);
        pre = root.val = pre + root.val;

        if (root.left != null)
            bstToGst(root.left);
        return root;
    }


------------------------

-----------------------
Linked List:::::
-----------------------

-----------------------
Recursion  Backtracking::
------------------------

--------------
Stack and Queue:::
--------------------
-> Stacks are dynamic data structures that follow the Last In First Out (LIFO) principle. The last item to be inserted into a stack is 
the first one to be deleted from it.

-> Stack is an ordered list in which, insertion and deletion can be performed only at one end that is called top.
-> Stack is a recursive data structure having pointer to its top element.
-> Stacks are sometimes called as Last-In-First-Out (LIFO) lists i.e. the element which is inserted first in the stack, will be deleted 
   last from the stack.

For example, you have a stack of trays on a table. The tray at the top of the stack is the first item to be moved if you require a tray from 
that stack.

Applications of Stack::
----------------------------------
->Recursion
->Expression evaluations and conversions
->Parsing
->Browsers
->Editors
->Tree Traversals


Inserting and deleting elements::
---------------------------------
-> Stacks have restrictions on the insertion and deletion of elements. Elements can be inserted or deleted only from one end of the stack
   i.e. from the top. The element at the top is called the top element. The operations of inserting and deleting elements are called 
   push() and pop() respectively.
   
-> When the top element of a stack is deleted, if the stack remains non-empty, then the element just below the previous top element becomes 
   the new top element of the stack.
   
-> For example, in the stack of trays, if you take the tray on the top and do not replace it, then the second tray automatically becomes 
   the top element (tray) of that stack.
   
  Features of stacks
--------------------- 
 ->Dynamic data structures
 ->Do not have a fixed size
 ->Do not consume a fixed amount of memory
 ->Size of stack changes with each push()  and pop()  operation. Each push() and pop() operation increases and decreases the size of the 
   stack by 1, respectively.
   
   Operations::
   -------------
   
  1. push( x ): Insert element x at the top of a stack
   -----------------------------------------------------------------
   void push (int stack[ ] , int x , int n) {
    if ( top == n-1 ) {         //If the top position is the last of position in a stack, this means that the stack is full
       cout << “Stack is full.Overflow condition!” ;
       }
       else{
           top = top +1 ;            //Incrementing top position 
           stack[ top ] = x ;       //Inserting element on incremented position  
       }
   }
  2. pop( ): Removes an element from the top of a stack
  -----------------------------------------------------------------
   
      void pop (int stack[ ] ,int n ) 
       {
   
           if( isEmpty ( ) )
           {
               cout << “Stack is empty. Underflow condition! ” << endl ;
           }
           else    
           {
                top = top - 1 ; //Decrementing top’s position will detach last element from stack            
           }
       }
  3. topElement ( ): Access the top element of a stack
  --------------------------------------------------------
   
     int topElement ( )
       {
           return stack[ top ];
       }
  4. isEmpty ( ) : Check whether a stack is empty
  ----------------------------------------------------------------
   
       bool isEmpty ( )
       {
           if ( top == -1 )  //Stack is empty
           return true ; 
           else
           return false;
       }
  5. size ( ): Determines the current size of a stack
  ------------------------------------------------------------------
   
      int size ( )
       {
           return top + 1;
       }
-------------------
Heaps:::
-----------------

----------------
Divide and Conquer:::
---------------------
1. Divide: Break the given problem into subproblems of same type.
2. Conquer: Recursively solve these subproblems
3. Combine: Appropriately combine the answers

1) Binary Search is a searching algorithm. In each step, the algorithm compares the input element x with the value of the middle element in array.
   If the values match, return the index of middle. Otherwise, if x is less than the middle element, then the algorithm recurs for left side of middle element,
   else recurs for right side of middle element.

     //recursion1 TC O(logn)
    public static int binarySearch1(int arr[],int left,int right,int x){
       if(left > right)
           return -1;
       int mid =(left+right)/2;

       if(x == arr[mid]) {
           return mid;
       }
       else if(x < arr[mid]) {
           return binarySearch1( arr, left, mid - 1, x );
       }
       else {
           return binarySearch1( arr, mid + 1, right, x );
       }
    }

    //Recursion2 TC O(logn)
    public static int binarySearch2(int arr[], int left,int right, int x){
        if( right >= left){
            int  mid = left + (right-left)/2;
            if(arr[mid] == x)
                return mid;
            if(arr[mid] > x)
                return binarySearch2( arr,left,mid-1,x );
            return binarySearch2( arr,mid+1,right,x);
        }
        return -1;
    }

    //Iterations TC O(logn)
    public static int binarySearchs(int arr[],int x){
        int left = 0;
        int right = arr.length-1;
        while (left <= right){
            int mid = (left + right)/2;
            if(x == arr[mid]){
                return mid;
            }
            else if(x < arr[mid]){
                right = mid-1;
            }
            else
                left = mid+1;
        }
        return -1;
    }

---------------------------------------------------------------------------------------------------------------------------------------------------
Given a sorted array consisting of only integers where every element appears exactly twice except for one element which appears exactly once. Find this single element that appears only once.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Example 1:

Input: [1,1,2,3,3,4,4,8,8]
Output: 2
Example 2:

Input: [3,3,7,7,10,11,11]
Output: 10
public class FindElementOnceSortedArray {
    public static int singleNonDuplicate(int[] nums) {
        // corner case
        if(nums == null || nums.length == 0)
            return -1;

        int lo = 0;
        int hi = nums.length - 1;
        while(lo < hi){
            int mid = lo + (hi - lo)/2;
            // trick here int temp = mid % 2 == 0 ? mid + 1: mid - 1;
            int temp = mid ^ 1; // if even, mid + 1; if odd, mid - 1
            if(nums[mid] == nums[temp]){
                // if mid is even, then nums[mid] = nums[mid + 1], single number is >= mid + 2
                // if mid is odd, then nums[mid] = nums[mid - 1], single number is >= mid + 1
                // so we choose mid + 1
                lo = mid + 1;
            }else{
                // maybe nums[hi] is the single numer or
                // maybe the single number is to the left of nums[hi]
                // <= hi
                hi = mid;
            }
        }

        return nums[lo];
    }

  //or
        public static void search(int[] arr, int low, int high)
        {
            if(low > high)
                return;
            if(low == high)
            {
                System.out.println("The required element is "+arr[low]);
                return;
            }
            int mid = (low + high)/2;

            // If mid is even and element next to mid is
            // same as mid, then output element lies on
            // right side, else on left side
            if(mid % 2 == 0)
            {
                if(arr[mid] == arr[mid+1])
                    search(arr, mid+2, high);
                else
                    search(arr, low, mid);
            }
            // If mid is odd
            else if(mid % 2 == 1)
            {
                if(arr[mid] == arr[mid-1])
                    search(arr, mid+1, high);
                else
                    search(arr, low, mid-1);
            }
        }

    public static void main(String[] args) {
    int nums[] = {3,3,7,7,10,11,11};
        System.out.println(singleNonDuplicate( nums ));
        search(nums, 0, nums.length-1);
  }
}
----------------------------------------------------------------------------------------------------
Java program to search an element in sorted and rotated array using single pass of Binary Search
----------------------------------------------------------------------------------------------------
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Your algorithm's runtime complexity must be in the order of O(log n).

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1

public class SearchElementSortedRotatedArray {
        static int search(int arr[], int l, int h, int key){
            if (l > h)
                return -1;

            int mid = (l+h)/2;
            if (arr[mid] == key)
                return mid;
            if (arr[l] <= arr[mid]){
                if (key >= arr[l] && key <= arr[mid])
                    return search(arr, l, mid-1, key);
                return search(arr, mid+1, h, key);
            }
            if (key >= arr[mid] && key <= arr[h])
                return search(arr, mid+1, h, key);
            return search(arr, l, mid-1, key);
        }
        public static void main(String args[])
        { 		int arr[] = {4,5,6,7,0,1,2};

            int n = arr.length;
            int key = 2;
            int i = search(arr, 0, n-1, key);
            if (i != -1)
                System.out.println("Index: " + i);
            else
                System.out.println("Key not found");
        }
}
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------






-------------------------------------------------------------------------------------------------------------------------------------

2) Quicksort is a sorting algorithm. The algorithm picks a pivot element, rearranges the array elements in such a way that all elements smaller than
  the picked pivot element move to left side of pivot, and all greater elements move to right side. Finally, the algorithm recursively sorts
  the subarrays on left and right of pivot element.

3) Merge Sort is also a sorting algorithm. The algorithm divides the array in two halves, recursively sorts them and finally merges the two sorted halves.

4) Closest Pair of Points The problem is to find the closest pair of points in a set of points in x-y plane. The problem can be solved in O(n^2) time by calculating distances of every pair of points and comparing the distances to find the minimum. The Divide and Conquer algorithm solves the problem in O(nLogn) time.

5) Strassen’s Algorithm is an efficient algorithm to multiply two matrices. A simple method to multiply two matrices need 3 nested loops and is O(n^3). Strassen’s algorithm multiplies two matrices in O(n^2.8974) time.

6) Cooley–Tukey Fast Fourier Transform (FFT) algorithm is the most common algorithm for FFT. It is a divide and conquer algorithm which works in O(nlogn) time.

7) Karatsuba algorithm for fast multiplication  it does multiplication of two n-digit numbers in at most 3 n^{\log_23}\approx 3 n^{1.585}single-digit multiplications in general (and exactly n^{\log_23} when n is a power of 2). It is therefore faster than the classical algorithm, which requires n2 single-digit products. If n = 210 = 1024, in particular, the exact counts are 310 = 59,049 and (210)2 = 1,048,576, respectively.





----------------------
Bit Manipulation:::
-------------------
Power of Two::
--------------

Method 1: Iterative
--------------------
check if n can be divided by 2. If yes, divide n by 2 and check it repeatedly.

				if (n == 0) return false;
				while (n%2 == 0) n/=2;
				return n == 1;

Time complexity = O(log n)
-------------------------
Method 2: Recursive
-------------------

		return n > 0 && (n == 1 || (n%2 == 0 && isPowerOfTwo(n/2)));

Time complexity = O(log n)
--------------------------
Method 3: Bit operation
-----------------------
If n is the power of two:

n = 2 ^ 0 = 1 = 0b0000...00000001, and (n - 1) = 0 = 0b0000...0000.
n = 2 ^ 1 = 2 = 0b0000...00000010, and (n - 1) = 1 = 0b0000...0001.
n = 2 ^ 2 = 4 = 0b0000...00000100, and (n - 1) = 3 = 0b0000...0011.
n = 2 ^ 3 = 8 = 0b0000...00001000, and (n - 1) = 7 = 0b0000...0111.
we have n & (n-1) == 0b0000...0000 == 0

Otherwise, n & (n-1) != 0.

For example, n =14 = 0b0000...1110, and (n - 1) = 13 = 0b0000...1101.


		return n > 0 && ((n & (n-1)) == 0);

Time complexity = O(1)
----------------------------
Method 4: Math derivation
----------------------------
Because the range of an integer = -2147483648 (-2^31) ~ 2147483647 (2^31-1), the max possible power of two = 2^30 = 1073741824.

(1) If n is the power of two, let n = 2^k, where k is an integer.

We have 2^30 = (2^k) * 2^(30-k), which means (2^30 % 2^k) == 0.

(2) If n is not the power of two, let n = j*(2^k), where k is an integer and j is an odd number.

We have (2^30 % j*(2^k)) == (2^(30-k) % j) != 0.


return n > 0 && (1073741824 % n == 0);


Time complexity = O(1)

Update:
Thanks for everyone's comment. Following are some other solutions metioned in comments.

------------------------
Method 5: Bit count
------------------------

Very intuitive. If n is the power of 2, the bit count of n is 1.
Note that 0b1000...000 is -2147483648, which is not the power of two, but the bit count is 1.

		return n > 0 && Integer.bitCount(n) == 1;

Time complexity = O(1)

The time complexity of bitCount() can be done by a fixed number of operations.
-----------------------------------------------------------------
More info in https://stackoverflow.com/questions/109023.

------------------------
Method 6: Look-up table
------------------------
There are only 31 numbers in total for an 32-bit integer.

		return new HashSet<>(Arrays.asList(1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824)).contains(n);

time complexity = O(1)
--------------------------------------------------------------------------------------------------------

#Count the number of ones in the binary representation of the given number.
---------------------------------------------------------------------------
Approach #1 (Loop and Flip) [Accepted]
----------------------------------------

The solution is straight-forward. We check each of the 3232 bits of the number. If the bit is 11, we add one to the number of 11-bits.

We can check the i^{th}i
th
  bit of a number using a bit mask. We start with a mask m=1m=1, because the binary representation of 11 is,

0000 0000 0000 0000 0000 0000 0000 0001 00000000000000000000000000000001 Clearly, a logical AND between any number and the mask 11 gives us the least significant bit of this number. To check the next bit, we shift the mask to the left by one.

0000 0000 0000 0000 0000 0000 0000 0010 00000000000000000000000000000010

And so on.


public int hammingWeight(int n) {
    int bits = 0;
    int mask = 1;
    for (int i = 0; i < 32; i++) {
        if ((n & mask) != 0) {
            bits++;
        }
        mask <<= 1;
    }
    return bits;
}

Complexity Analysis

The run time depends on the number of bits in nn. Because nn in this piece of code is a 32-bit integer, the time complexity is O(1)O(1).

The space complexity is O(1)O(1), since no additional space is allocated.
--------------------------------------

Approach #2 (Bit Manipulation Trick) [Accepted]
Algorithm

We can make the previous algorithm simpler and a little faster. Instead of checking every bit of the number, we repeatedly flip the least-significant 11-bit of the number to 00, and add 11 to the sum. As soon as the number becomes 00, we know that it does not have any more 11-bits, and we return the sum.

The key idea here is to realize that for any number nn, doing a bit-wise AND of nn and n - 1n−1 flips the least-significant 11-bit in nn to 00. Why? Consider the binary representations of nn and n - 1n−1.


public int hammingWeight(int n) {
    int sum = 0;
    while (n != 0) {
        sum++;
        n &= (n - 1);
    }
    return sum;
}
Complexity Analysis

The run time depends on the number of 11-bits in nn. In the worst case, all bits in nn are 11-bits. In case of a 32-bit integer, the run time is O(1)O(1).

The space complexity is O(1)O(1), since no additional space is allocated.
------------------------------------------------------------------------------

#Check if the ith bit is set in the binary form of the given number.

--------------------------------------------------------------------------------------------------------
#Mathmatics::::
---------------------------------------------------------------------------------------------------------
Fundamentals:::
----------------------

----------------------
Basic math operations (addition, subtraction, multiplication, division, exponentiation)
---------------------------------------------------------------------------------------
Fast Modulo Multiplication (Exponential Squaring)
-------------------------------------------------
-> Fast Modulo Multiplication (also known as Exponential Squaring or Repeated Squaring).
   This is a very useful technique to have under your arsenal as a competitive programmer, especially because such technique often appears on Maths related problems and, sometimes,
   it might be the difference between having AC veredict or TLE veredict, so, for someone who still doesn't know about it, I hope this tutorial will help :)

Main reason why the usage of repeated squaring is useful
--------------------
This technique might seem a bit too complicated at a first glance for a newbie, after all, say, we want to compute the number 310. We can simply write the following code and do a simple for loop:

#include <iostream>
using namespace std;

int main()
{
    int base = 3;
    int exp = 10;
    int ans = 1;
    for(int i = 1; i <= exp; i++)
    {
        ans *= base;
    }
    cout << ans;
    return 0;
}

The above code will correctly compute the desired number, 59049. However, after we analyze it carefully, we will obtain an insight which will be the key to develop a faster method.

Apart from being correct, the main issue with the above method is the number of multiplications that are being executed.

We see that this method executes exactly exp-1 multiplications to compute the number n^exp, which is not desirable when the value of exp is very large.

Usually, on contest problems, the idea of computing large powers of a number appears coupled with the existence of a modulus value,
i.e., as the values being computed can get very large, very fast, the value we are looking to compute is usually something of the form:

n^exp % M ,

where M is usually a large prime number (typically, 10^9 + 7).

Note that we could still use the modulus in our naive way of computing a large power: we simply use modulus on all the intermediate steps and take modulus at the end, to ensure every calculation is kept within the limit of "safe" data-types.

The fast-exponentiation method: an implementation in C++
---------------------------------------------------
It is possible to find several formulas and several ways of performing fast exponentiation by searching over the internet, but, there's nothing like implementing them on our own to get a better feel about what we are doing :)

I will describe here the most naive method of performing repeated squaring. As found on Wikipedia, the main formula is:

	  { x(x^2)^n-1/2, if n is odd
x^n = { (x^2)^n/2 , if n is even.
      {
A brief analysis of this formula (an intuitive analysis if you prefer), based both on its recursive formulation and on its implementation allows us to see that the formula uses only O(log2n) squarings and O(log2n) multiplications!

This is a major improvement over the most naive method described in the beginning of this text, where we used much more multiplication operations.

Below, I provide a code which computes base^exp % 1000000007, based on wikipedia formula:

long long int fast_exp(int base, int exp)
{
    if(exp==1)
    return base;
    else
    {
        if(exp%2 == 0)
        {
            long long int base1 = pow(fast_exp(base, exp/2),2);
            if(base1 >= 1000000007)
            return base1%1000000007;
            else
            return base1;
        }
        else
        {
            long long int ans = (base*  pow(fast_exp(base,(exp-1)/2),2));
            if(ans >= 1000000007)
            return ans%1000000007;
            else
            return ans;
        }
    }
}
The 2^k-ary method for repeated squaring
Besides the recursive method detailed above, we can use yet another insight which allows us to compute the value of the desired power.

The main idea is that we can expand the exponent in base 2 (or more generally, in base 2^k, with k >= 1) and use this expansion to achieve the same result as above, but, this time, using less memory.


ll fast_exp(int base, int exp) {
    ll res=1;
    while(exp>0) {
       if(exp%2==1) res=(res*base)%MOD;
       base=(base*base)%MOD;
       exp/=2;
    }
    return res%MOD;
}
These are the two most common ways of performing repeated squaring on a live contest and I hope you have enjoyed this text and have found it useful :)

-------------------------------------------------------------------------------------------
------------------------
Number Theory :::
-----------------------
Number Theory: Number theory is a branch of pure mathematics devoted primarily to the study of natural numbers and integers.

1.Modular Arithmetic::
--------------------
-> Modular arithmetic is a system of arithmetic for integers, where numbers wrap around upon reaching a certain value i.e. the modulus.
-> When one number is divided by another, the modulo operation finds the remainder. It is denoted by the % symbol.
Example
Assume that you have two numbers 5 and 2. 5%2 is 1 because when 5 is divided by 2, the remainder is 1.

Properties::
------------
1. (a+b)%c=((a%c)+(b%c))%c
2. (a*b)%c=((a%c)*(b%c))%c
3. (a-b)%c=((a%c)-(b%c)+ c)%c
4. (a/b)%c=((a%c)*(b^-1%c))%c

Note: In the last property, b^-1 is the multiplicative modulo inverse of b and c.

2. Modular exponentiation:::
------------------------------
Exponentiation is a mathematical operation that is expressed as x^n and computed as x^n = x*x*...*x (n times).

Basic Method:: O(n)
--------------
int recursivePower(int x,int n)
{
    if(n==0)
        return 1;
    return x*recursivePower(x,n-1);
}

 or-----------

int iterativePower(int x,int n)
{
    int result=1;
    while(n>0)
    {
        result=result*x;
        n--;
    }
    return result;
}


BinaryExponentiation::
-----------------------
int binaryExponentiation(int x,int n)
{
    if(n==0)
        return 1;
    else if(n%2 == 0)        //n is even
        return binaryExponentiation(x*x,n/2);
    else                             //n is odd
        return x*binaryExponentiation(x*x,(n-1)/2);
}

or

int binaryExponentiation(int x,int n)
{
    int result=1;
    while(n>0)
    {
        if(n % 2 ==1)
            result=result * x;
        x=x*x;
        n=n/2;
    }
    return result;
}

ModularExponentiation::
------------------------
int modularExponentiation(int x,int n,int M)
{
    if(n==0)
        return 1;
    else if(n%2 == 0)        //n is even
        return modularExponentiation((x*x)%M,n/2,M);
    else                             //n is odd
        return (x*modularExponentiation((x*x)%M,(n-1)/2,M))%M;

}

or

int modularExponentiation(int x,int n,int M)
{
    int result=1;
    while(n>0)
    {
        if(power % 2 ==1)
            result=(result * x)%M;
        x=(x*x)%M;
        n=n/2;
    }
    return result;
}



3. Greatest Common Divisor (GCD):::
-----------------------------------
The GCD of two or more numbers is the largest positive number that divides all the numbers that are considered. For example, the GCD of 6 and 10 is 2 because it is the largest positive number that can divide both 6 and 10.

//Naive   time complexity of this function is O(min(A, B)).
int GCD(int A, int B) {
    int m = min(A, B), gcd;
    for(int i = m; i > 0; --i)
        if(A % i == 0 && B % i == 0) {
            gcd = i;
            return gcd;
        }
}

or

//Euclid’s Algorithm  time complexity is O(log(max(A, B))).
int GCD(int A, int B) {
    if(B==0)
        return A;
    else
        return GCD(B, A % B);
}

4. Extended Euclidean algorithm:::
------------------------------------
--> This algorithm is an extended form of Euclid’s algorithm. GCD(A,B) has a special property so that it can always be represented in the form of an equation i.e. .Ax + By = GCD(A,B).
-> The coefficients (x and y) of this equation will be used to find the modular multiplicative inverse. The coefficients can be zero, positive or negative in value.
-> This algorithm takes two inputs as A and B and returns  GCD(A,B)and coefficients of the above equation as output.


  void extendedEuclid(int A, int B,int d,int x,int y) {
    if(B == 0) {
        d = A;
        x = 1;
        y = 0;
    }
    else {
        extendedEuclid(B, A%B);
        int temp = x;
        x = y;
        y = temp - (A/B)*y;
    }
}// TC O(log(max(A,B)))

5. Modular multiplicative inverse :::
---------------------------------------
When is this algorithm used?
-> This algorithm is used when A and B are co-prime. In such cases, x becomes the multiplicative modulo inverse of A under modulo B, and y becomes
the multiplicative modulo inverse of B under modulo A. This has been explained in detail in the Modular multiplicative inverse section.
-> What is a multiplicative inverse? If A.B=1, you are required to find B such that it satisfies the equation. The solution is simple. The value of B is 1/A or A^-1 . Here, B is the multiplicative inverse of A.

What is modular multiplicative inverse? If you have two numbers A and M, you are required to find B such it that satisfies the following equation:

(A.B)%M =1
Here B is the modular multiplicative inverse of A under modulo M.

Formally, if you have two integers A and M, B is said to be modular multiplicative inverse of A under modulo M if it satisfies the following equation:

A.B = 1(modM). where B is in the range [1,M-1]

This equation is a formal representation of the equation discussed earlier.

Why is B in the range [1,M-1]?

(A*B)%M = (A%M*B%M)%M
Since we have B%M, the inverse must be in the range [0,M-1]. However, since 0 is invalid, the inverse must be in the range [1,M-1].

Existence of modular multiplicative inverse

An inverse exists only when A and M are coprime i.e. GCD(A,M)=1.

Approach 1 (naive approach) O(M)
----------------------------
int modInverse(int A,int M)
{
    A=A%M;
    for(int B=1;B<M;B++)
        if((A*B)%M)==1)
            return B;
}

Approach 2  Time complexity O(log(max(A,M)))
-----------
A and M are coprime i.e.Ax +My=1 . In the extended Euclidean algorithm, x is the modular multiplicative inverse of A under modulo M. Therefore, the answer is x. You can use the extended Euclidean algorithm to find the multiplicative inverse.
----------------
int d,x,y;
int modInverse(int A, int M)
{
    extendedEuclid(A,M);
    return (x%M+M)%M;    //x may be negative
}

Approach 3 (used only when M is prime) Time complexity O(log M)
--------------------------------------

int modInverse(int A,int M)
{
    return modularExponentiation(A,M-2,M);
}
-------------------------------------------
What are prime numbers and composite numbers?
----------------------------------------------
-> Prime numbers are those numbers that are greater than 1 and have only two factors 1 and itself.
-> Composite numbers are also numbers that are greater than 1 but they have at least one more divisor other than 1 and itself.

For example, 5 is a prime number because 5 is divisible only by 1 and 5 only. However, 6 is a composite number as 6 is divisible by 1, 2, 3, and 6.

Naive approach
---------------
void checkprime(int N){
        int count = 0;
        for( int i = 1;i <= N;++i )
            if( N % i == 0 )
                count++;
        if(count == 2)
            cout << N << “ is a prime number.” << endl;
        else
            cout << N << “ is not a prime number.” << endl;
    }



or

void checkprime(int N) {
        int count = 0;
        for( int i = 1;i * i <=N;++i ) {
             if( N % i == 0) {
                 if( i * i == N )
                         count++;
                 else       // i < sqrt(N) and (N / i) > sqrt(N)
                         count += 2;
              }
        }
        if(count == 2)
            cout << N << “ is a prime number.” << endl;
        else
            cout << N << “ is not a prime number.” << endl;
    }


Sieve of Eratosthenes:::
-------------------------
-> You can use the Sieve of Eratosthenes to find all the prime numbers that are less than or equal to a given number N or to find out whether a number is a prime number.
->The basic idea behind the Sieve of Eratosthenes is that at each iteration one prime number is picked up and all its multiples are eliminated. After the elimination process is complete, all the unmarked numbers that remain are prime.

Pseudo code
-------------
1.Mark all the numbers as prime numbers except 1
2.Traverse over each prime numbers smaller than sqrt(N)
3.For each prime number, mark its multiples as composite numbers
4.Numbers, which are not the multiples of any number, will remain marked as prime number and others will change to composite numbers.

void sieve(int N) {
        bool isPrime[N+1];
        for(int i = 0; i <= N;++i) {
            isPrime[i] = true;
        }
        isPrime[0] = false;
        isPrime[1] = false;
        for(int i = 2; i * i <= N; ++i) {
             if(isPrime[i] == true) {                    //Mark all the multiples of i as composite numbers
                 for(int j = i * i; j <= N ;j += i)
                     isPrime[j] = false;
            }
        }
    }



------------
What is Euler's Totient Function?
---------------------------------
-> Number theory is one of the most important topics in the field of Math and can be used to solve a variety of problems. Many times one might have come across problems that relate to the prime factorization of a number, to the divisors of a number, to the multiples of a number and so on.
-> Euler's Totient function is a function that is related to getting the number of numbers that are coprime to a certain number X that are less than or equal to it. In short , for a certain number X we need to find the count of all numbers y where gcd(X,Y) =1 and 1<= Y <= X.

-> A naive method to do so would be to Brute-Force the answer by checking the gcd of  X and every number less than or equal to X and then incrementing the count whenever a GCD of 1 is obtained. However, this can be done in a much faster way using Euler's Totient Function.

set<> primes;
static void mark(int num,int max,int[] arr)
{
    int i=2,elem;
    while((elem=(num*i))<=max)
    {
        arr[elem-1]=1;
        i++;
    }
}
GeneratePrimes()
{
    int arr[max_prime];
    for(int i=1;i<arr.length;i++)
    {
        if(arr[i]==0)
        {
            list.add(i+1);
            mark(i+1,arr.length-1,arr);
        }
    }
}
main()
{
    GeneratePrimes();
    int N=nextInt();
    int ans=N;
    for(int k:set)
    {
        if(N%k==0)
        {
            ans*=(1-1/k);
        }
    }
    print(ans);
}

or
static int phi(int n){
        float result = n;
        for (int p = 2; p * p <= n; ++p) {
            if (n % p == 0) {
                while (n % p == 0)
                    n /= p;
                result *= (1.0 - (1.0 / (float)p));
            }
        }
        if (n > 1)
            result *= (1.0 - (1.0 / (float)n));

        return (int)result;
    }
---------------------------------------






--------------------------
Permutaion & Combinatorics
---------------------------


-------------------------------
Algebra:::
-------------------------------

------------------------------
Geometry:::
---------------------------------------------------------

--------------------------------------------------------------------------------------------------------
Probability::::
-------------------------------------------

-------------------------------------------
Linear Algebra Foundations:::
--------------------------------

---------------------------------
Statistics:::
-----------------

-----------------
---------------------------------------------------------
DISCRETE MATHEMATICS::::
---------------------------------------------------------
The Foundations: Logic and Proofs::-
-----------------------------------
1.1 Propositional Logic::
-------------------------
Let p be a proposition.The negation of p,denoted by ¬p(also denoted by compliments p),is the statement
“It is not the case that p.” The proposition ¬p is read “not p.”The truth value of the negation of p,¬p, is the opposite of the truth value of p.

TABLE 1 The TruthTable for the Negation of a Proposition.
 p  ¬p
-------
 T   F
-------
 F   T


-------------------------------------------------------------------------------------------------------