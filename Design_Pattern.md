#Design Patterns in Java
------------------------
In that name, we've compiled a list of all the Design Patterns you'll encounter or use as a software developer, implemented in Java.

-----------------------------------
Creational Patterns::-
--------------
->Creational patterns are ones that create objects for you, rather than having you instantiate objects directly. This gives your program more flexibility in
  deciding which objects need to be created for a given case.
-----------------------------------

Abstract :  factory groups object factories that have a common theme.
Builder:  constructs complex objects by separating construction and representation.
Factory method : creates objects without specifying the exact class to create.
Prototype : creates objects by cloning an existing object.
Singleton : restricts object creation for a class to only one instance.

Abstract Factory pattern: a class requests the objects it requires from a factory object instead of creating the objects directly
Factory method/Template pattern: centralize creation of an object of a specific type choosing one of several implementations
Builder pattern: separate the construction of a complex object from its representation so that the same construction process can create different representations
Dependency Injection pattern: a class accepts the objects it requires from an injector instead of creating the objects directly
Lazy initialization pattern: tactic of delaying the creation of an object, the calculation of a value, or some other expensive process until the first time it is needed
Object pool pattern: avoid expensive acquisition and release of resources by recycling objects that are no longer in use
Prototype pattern: used when the type of objects to create is determined by a prototypical instance, which is cloned to produce new objects
Singleton pattern: restrict instantiation of a class to one object

Abstract Factory : -Creates an instance of several families of classes
Builder : - Separates object construction from its representation
Factory Method : - Creates an instance of several derived classes
Object Pool : - Avoid expensive acquisition and release of resources by recycling objects that are no longer in use
Prototype : - A fully initialized instance to be copied or cloned
Singleton : - A class of which only a single instance can exist

-------------------------------------
Structural Patterns::
------------------------------------
These concern class and object composition. They use inheritance to compose interfaces and define ways to compose objects to obtain new functionality.

Adapter : allows classes with incompatible interfaces to work together by wrapping its own interface around that of an already existing class.
Bridge : decouples an abstraction from its implementation so that the two can vary independently.
Composite : composes zero-or-more similar objects so that they can be manipulated as one object.
Decorator : dynamically adds/overrides behaviour in an existing method of an object.
Facade : provides a simplified interface to a large body of code.
Flyweight : reduces the cost of creating and manipulating a large number of similar objects.
Proxy : provides a placeholder for another object to control access, reduce cost, and reduce complexity.



Adapter : - Match interfaces of different classes
Bridge : - Separates an object’s interface from its implementation
Composite : - A tree structure of simple and composite objects
Decorator : - Add responsibilities to objects dynamically
Facade : - A single class that represents an entire subsystem
Flyweight : - A fine-grained instance used for efficient sharing
Private Class Data - Restricts accessor/mutator access
Proxy : - An object representing another object
Filter

--------------------------------------
Behavioral Patterns
--------------------------------------

Most of these design patterns are specifically concerned with communication between objects.

Chain of responsibility :  delegates commands to a chain of processing objects.
Command : creates objects which encapsulate actions and parameters.
Interpreter : implements a specialized language.
Iterator : accesses the elements of an object sequentially without exposing its underlying representation.
Mediator : allows loose coupling between classes by being the only class that has detailed knowledge of their methods.
Memento : provides the ability to restore an object to its previous state (undo).
Observer : is a publish/subscribe pattern which allows a number of observer objects to see an event.
State : allows an object to alter its behavior when its internal state changes.
Strategy : allows one of a family of algorithms to be selected on-the-fly at runtime.
Template method  : defines the skeleton of an algorithm as an abstract class, allowing its subclasses to provide concrete behavior.
Visitor : separates an algorithm from an object structure by moving the hierarchy of methods into one object.


Chain of responsibility : - A way of passing a request between a chain of objects
Command : - Encapsulate a command request as an object
Interpreter : - A way to include language elements in a program
Iterator : - Sequentially access the elements of a collection
Mediator : - Defines simplified communication between classes
Memento :- Capture and restore an object's internal state
Null Object :- Designed to act as a default value of an object
Observer :- A way of notifying change to a number of classes
State :- Alter an object's behavior when its state changes
Strategy :- Encapsulates an algorithm inside a class
Template method :- Defer the exact steps of an algorithm to a subclass
Visitor : -Defines a new operation to a class without change


---------------------------------------
J2EE Patterns
----------------------------------------
MVC Pattern
Business Delegate Pattern
Composite Entity Pattern
Data Access Object Pattern
Front Controller Pattern
Intercepting Filter Pattern
Service Locator Pattern
Transfer Object Pattern





-----------------------------------------------------------------------------------------------------------------------------------------------------------------

Software design patterns
Creational
Abstract factory Builder Dependency injection Factory method Lazy initialization Multiton Object pool Prototype RAII Singleton
Structural
Adapter Bridge Composite Decorator Delegation Facade Flyweight Front controller Marker interface Module Proxy Twin
Behavioral
Blackboard Chain of responsibility Command Interpreter Iterator Mediator Memento Null object Observer Servant Specification State Strategy Template method Visitor
Functional
Closure Currying Function composition Functor Monad Generator
Concurrency
Active object Actor Balking Barrier Binding properties Coroutine Compute kernel Double-checked locking Event-based asynchronous Fiber Futex Futures and promises Guarded suspension
Immutable object Join Lock Messaging Monitor Nuclear Proactor Reactor Read write lock Scheduler Thread pool Thread-local storage
Architectural
ADR Active record Broker Client–server CBD DAO DTO DDD ECS EDA Front controller Identity map Interceptor Implicit invocation Inversion of control Model 2 MOM Microservices MVA MVC MVP MVVM Monolithic Multitier Naked objects ORB P2P Publish–subscribe PAC REST SOA Service locator SN SBA Specification
Cloud
Distributed
Ambassador Anti-Corruption Layer Bulkhead Cache-Aside Circuit Breaker CQRS Compensating Transaction Competing Consumers Compute Resource Consolidation Event Sourcing External Configuration Store Federated Identity Gatekeeper Index Table Leader Election MapReduce Materialized View Pipes Filters Priority Queue Publisher-Subscriber Queue-Based Load Leveling Retry Scheduler Agent Supervisor Sharding Sidecar Strangler Throttling Valet Key



================================================================================================================================================================
// class CPU
abstract class CPU {}

// class EmberCPU
class EmberCPU extends CPU {}

// class EnginolaCPU
class EnginolaCPU extends CPU {}

// class MMU
abstract class MMU {}

// class EmberMMU
class EmberMMU extends MMU {}

// class EnginolaMMU
class EnginolaMMU extends MMU {}

// class EmberFactory
class EmberToolkit extends AbstractFactory {
    @Override
    public CPU createCPU() {
        return new EmberCPU();
    }

    @Override
    public MMU createMMU() {
        return new EmberMMU();
    }
}

// class EnginolaFactory
class EnginolaToolkit extends AbstractFactory {
    @Override
    public CPU createCPU() {
        return new EnginolaCPU();
    }

    @Override
    public MMU createMMU() {
        return new EnginolaMMU();
    }
}

enum Architecture {
    ENGINOLA, EMBER
}

abstract class AbstractFactory {
    private static final EmberToolkit EMBER_TOOLKIT = new EmberToolkit();
    private static final EnginolaToolkit ENGINOLA_TOOLKIT = new EnginolaToolkit();

    // Returns a concrete factory object that is an instance of the
    // concrete factory class appropriate for the given architecture.
    static AbstractFactory getFactory(Architecture architecture) {
        AbstractFactory factory = null;
        switch (architecture) {
            case ENGINOLA:
                factory = ENGINOLA_TOOLKIT;
                break;
            case EMBER:
                factory = EMBER_TOOLKIT;
                break;
        }
        return factory;
    }

    public abstract CPU createCPU();

    public abstract MMU createMMU();
}

public class Client {
    public static void main(String[] args) {
        AbstractFactory factory = AbstractFactory.getFactory(Architecture.EMBER);
        CPU cpu = factory.createCPU();
    }
}

The 7 Most Important Software Design Patterns:::
------------------------------------------------

Why Design Patterns?
Design Patterns have become an object of some controversy in the programming world in recent times, largely due to their perceived ‘over-use’ leading to code that can be harder to understand and manage.
It’s important to understand that Design Patterns were never meant to be hacked together shortcuts to be applied in a haphazard, ‘one-size-fits-all’ manner to your code. There is ultimately no substitute for genuine problem solving ability in software engineering.
The fact remains, however, that Design Patterns can be incredibly useful if used in the right situations and for the right reasons. When used strategically, they can make a programmer significantly more efficient by allowing them to avoid reinventing the proverbial wheel, instead using methods refined by others already. They also provide a useful common language to conceptualize repeated problems and solutions when discussing with others or managing code in larger teams.
That being said, an important caveat is to ensure that the how and the why behind each pattern is also understood by the developer.
Without further ado (in general order of importance, from most to least):
The Most Important Design Patterns

1.Singleton
The singleton pattern is used to limit creation of a class to only one object. This is beneficial when one (and only one) object is needed to coordinate actions across the system. There are several examples of where only a single instance of a class should exist, including caches, thread pools, and registries.
It’s trivial to initiate an object of a class — but how do we ensure that only one object ever gets created? The answer is to make the constructor ‘private’ to the class we intend to define as a singleton. That way, only the members of the class can access the private constructor and no one else.
Important consideration: It’s possible to subclass a singleton by making the constructor protected instead of private. This might be suitable under some circumstances. One approach taken in these scenarios is to create a register of singletons of the subclasses and the getInstance method can take in a parameter or use an environment variable to return the desired singleton. The registry then maintains a mapping of string names to singleton objects, which can be accessed as needed.
2. Factory Method
A normal factory produces goods; a software factory produces objects. And not just that — it does so without specifying the exact class of the object to be created. To accomplish this, objects are created by calling a factory method instead of calling a constructor.

Usually, object creation in Java takes place like so:
SomeClass someClassObject = new SomeClass();
The problem with the above approach is that the code using the SomeClass’s object, suddenly now becomes dependent on the concrete implementation of SomeClass. There’s nothing wrong with using new to create objects but it comes with the baggage of tightly coupling our code to the concrete implementation class, which can occasionally be problematic.
3. Strategy
The strategy pattern allows grouping related algorithms under an abstraction, which allows switching out one algorithm or policy for another without modifying the client. Instead of directly implementing a single algorithm, the code receives runtime instructions specifying which of the group of algorithms to run.
4. Observer
This pattern is a one-to-many dependency between objects so that when one object changes state, all its dependents are notified. This is typically done by calling one of their methods.
For the sake of simplicity, think about what happens when you follow someone on Twitter. You are essentially asking Twitter to send you (the observer) tweet updates of the person (the subject) you followed. The pattern consists of two actors, the observer who is interested in the updates and the subject who generates the updates.

A subject can have many observers and is a one to many relationship. However, an observer is free to subscribe to updates from other subjects too. You can subscribe to news feed from a Facebook page, which would be the subject and whenever the page has a new post, the subscriber would see the new post.
Key consideration: In case of many subjects and few observers, if each subject stores its observers separately, it’ll increase the storage costs as some subjects will be storing the same observer multiple times.
5. Builder
As the name implies, a builder pattern is used to build objects. Sometimes, the objects we create can be complex, made up of several sub-objects or require an elaborate construction process. The exercise of creating complex types can be simplified by using the builder pattern. A composite or an aggregate object is what a builder generally builds.
Key consideration: The builder pattern might seem similar to the ‘abstract factory’ pattern but one difference is that the builder pattern creates an object step by step whereas the abstract factory pattern returns the object in one go.
6. Adapter
This allows incompatible classes to work together by converting the interface of one class into another. Think of it as a sort of translator: when two heads of states who don’t speak a common language meet, usually an interpreter sits between the two and translates the conversation, thus enabling communication.

If you have two applications, with one spitting out output as XML with the other requiring JSON input, then you’ll need an adapter between the two to make them work seamlessly.
7. State
The state pattern encapsulates the various states a machine can be in, and allows an object to alter its behavior when its internal state changes. The machine or the context, as it is called in pattern-speak, can have actions taken on it that propel it into different states. Without the use of the pattern, the code becomes inflexible and littered with if-else conditionals.
Want to keep learning?
With Software Design Patterns: Best Practices for Developers you’ll have the chance to do more than just read the theory. You’ll be able to dive deep into real problems and understand practical solutions with real-life code examples.
The course is based on the popular book by the Gang of Four, but presented in an interactive, easy-to-digest format. You will master the 23 famous design patterns from the book interactively, learn the proper applications of the 3 key design pattern types (creational, structural, and behavioral), and learn to incorporate these design patterns into your own projects.