How to find second highest or maximum salary of Employee in SQL - Interview question

SELECT max(salary) FROM Employee WHERE salary < (SELECT max(salary) FROM Employee);

Second maximum salary using LIMIT keyword of MYSQL database
LIMIT keyword of MySQL database is little bit similar with TOP keyword of SQL Server database and allows to take only certain rows from result set. If you look at below SQL example, its very much similar to SQL Server TOP keyword example.

mysql> SELECT salary  FROM (SELECT salary FROM Employee ORDER BY salary DESC LIMIT 2) AS emp ORDER BY salary LIMIT 1;
