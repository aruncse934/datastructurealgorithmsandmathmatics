
#Comman QA:::
=======================================================================================================================
    --------------
    JAR vs WAR vs EAR :::
    ---------------------
    In J2EE application, modules are packaged as EAR, JAR and WAR based on their functionality
    JAR: EJB modules which contain enterprise java beans (class files) and EJB deployment descriptor are packed as JAR files with .jar extenstion
    WAR: Web modules which contain Servlet class files, JSP Files, supporting files, GIF and HTML files are packaged as JAR file with .war (web archive) extension
    EAR: All above files (.jar and .war) are packaged as JAR file with .ear (enterprise archive) extension and deployed into Application Server.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------
    Difference between BufferedReader and Scanner are following::::
    ---------------------------------------------------------------
    BufferedReader is synchronized but Scanner is not synchronized.
    BufferedReader is thread safe but Scanner is not thread safe.
    BufferedReader has larger buffer memory but Scanner has smaller buffer memory.
    BufferedReader is faster but Scanner is slower in execution.
    Code to read a line from console:
    BufferedReader:
    ----------------
     InputStreamReader isr=new InputStreamReader(System.in);
     BufferedReader br= new BufferedReader(isr);
     String st= br.readLine();
    
    Scanner:
    --------
    Scanner sc= new Scanner(System.in);
    String st= sc.nextLine();
    -------------------------------------------------------------------


=======================================================================================================================

#Java8:::
=====================================================================================

Java 8 Features::
---------------------
->Lambda Expressions − a new language feature allowing treating actions as objects.
                     - It is a feature derived from the functional programming. It is a function that does not belong to any class.
->Stream API − a special iterator class that allows processing collections of objects in a functional manner.
->Default methods in the interface − give us the ability to add full implementations in interfaces besides abstract methods
->Functional Interface – an interface with maximum one abstract method, implementation can be provided using a Lambda Expression
                       -Each functional interface has a single abstract method, called the functional method, implementation can be provided using the lambda expressions.
->Optional − special wrapper class used for expressing optionality
           -Instead of using null values Optional class is used for representing Optional values.
->Method references − enable defining Lambda Expressions by referring to methods directly using their names
->Date API − an improved, immutable JodaTime-inspired Date API
->Nashorn, JavaScript Engine − Java-based engine for executing and evaluating JavaScript code

1.What are main advantages of using Java 8?
------------------------------------------
->More compact code
->Less boiler plate code
->More readable and reusable code
->More testable code
->Parallel operations

Functional Interfaces and Lambda Expressions
-----------------------------------------------
If you notice the above interface code, you will notice @FunctionalInterface annotation. Functional interfaces are a new concept introduced in Java 8. An interface with exactly one abstract method becomes a Functional Interface. We don’t need to use @FunctionalInterface annotation to mark an interface as a Functional Interface.

@FunctionalInterface annotation is a facility to avoid the accidental addition of abstract methods in the functional interfaces. You can think of it like @Override annotation and it’s best practice to use it. java.lang.Runnable with a single abstract method run() is a great example of a functional interface.

One of the major benefits of the functional interface is the possibility to use lambda expressions to instantiate them. We can instantiate an interface with an anonymous class but the code looks bulky.

Runnable r = new Runnable(){
@Override
public void run() {
System.out.println("My Runnable");
}};
Since functional interfaces have only one method, lambda expressions can easily provide the method implementation. We just need to provide method arguments and business logic. For example, we can write above implementation using lambda expression as:

Runnable r1 = () -> {
System.out.println("My Runnable");
};
If you have single statement in method implementation, we don’t need curly braces also. For example above Interface1 anonymous class can be instantiated using lambda as follows:

Interface1 i1 = (s) -> System.out.println(s);

i1.method1("abc");
So lambda expressions are a means to create anonymous classes of functional interfaces easily. There are no runtime benefits of using lambda expressions, so I will use it cautiously because I don’t mind writing a few extra lines of code.

A new package java.util.function has been added with bunch of functional interfaces to provide target types for lambda expressions and method references. Lambda expressions are a huge topic, I will write a separate article on that in the future.

Collection API improvements
----------------------------
We have already seen forEach() method and Stream API for collections. Some new methods added in Collection API are:

Iterator default method forEachRemaining(Consumer action) to perform the given action for each remaining element until all elements have been processed or the action throws an exception.
Collection default method removeIf(Predicate filter) to remove all of the elements of this collection that satisfy the given predicate.
Collection spliterator() method returning Spliterator instance that can be used to traverse elements sequentially or parallel.
Map replaceAll(), compute(), merge() methods.
Performance Improvement for HashMap class with Key Collisions

Concurrency API improvements
-----------------------------
Some important concurrent API enhancements are:

ConcurrentHashMap compute(), forEach(), forEachEntry(), forEachKey(), forEachValue(), merge(), reduce() and search() methods.
CompletableFuture that may be explicitly completed (setting its value and status).
Executors newWorkStealingPool() method to create a work-stealing thread pool using all available processors as its target parallelism level.

Java IO improvements
---------------------
Some IO improvements known to me are:

Files.list(Path dir) that returns a lazily populated Stream, the elements of which are the entries in the directory.
Files.lines(Path path) that reads all lines from a file as a Stream.
Files.find() that returns a Stream that is lazily populated with Path by searching for files in a file tree rooted at a given starting file.
BufferedReader.lines() that return a Stream, the elements of which are lines read from this BufferedReader.

Java Time API
---------------
It has always been hard to work with Date, Time, and Time Zones in java. There was no standard approach or API in java for date and time in Java. One of the nice addition in Java 8 is the java.time package that will streamline the process of working with time in java.

Just by looking at Java Time API packages, I can sense that they will be very easy to use. It has some sub-packages java.time.format that provides classes to print and parse dates and times and java.time.zone provides support for time zones and their rules.

The new Time API prefers enums over integer constants for months and days of the week. One of the useful classes is DateTimeFormatter for converting DateTime objects to strings.

Miscellaneous Java 8 Core API improvements
---------------------------------------------
Some miscellaneous API improvements that might come handy are:

ThreadLocal static method withInitial(Supplier supplier) to create instances easily.
The Comparator interface has been extended with a lot of default and static methods for natural ordering, reverse order, etc.
min(), max() and sum() methods in Integer, Long and Double wrapper classes.
logicalAnd(), logicalOr() and logicalXor() methods in Boolean class.
ZipFile.stream() method to get an ordered Stream over the ZIP file entries. Entries appear in the Stream in the order they appear in the central directory of the ZIP file.
Several utility methods in Math class.
jjs command is added to invoke Nashorn Engine.
jdeps command is added to analyze class files
JDBC-ODBC Bridge has been removed.
PermGen memory space has been removed

------------------------------------------------------------------------------------------------
 1.Difference Between Collections Vs Streams In Java :
    -----------------------------------------------------
          Collections	                                                     Streams
    -> Collections are mainly used to store and group the data.	Streams are mainly used to perform operations on data.
    -> You can add or remove elements from collections.        	You can’t add or remove elements from streams.
    -> Collections have to be iterated externally.	            Streams are internally iterated.
    -> Collections can be traversed multiple times.	            Streams are traversable only once.
    -> Collections are eagerly constructed.	                    Streams are lazily constructed.
    -> Ex : List, Set, Map…	                                    Ex : filtering, mapping, matching…
    
 Explain Differences between Collection API and Stream API?
 -------------------------------------------------------------
    	Collection API	                                                         Stream API
    1.	It’s available since Java 1.2	                                      -> It is introduced in Java SE 8
    2.	It is used to store Data (A set of Objects).	                      -> It is used to compute data (Computation on a set of Objects).
    3.	We can use both Spliterator and Iterator to iterate elements. We can use forEach to performs an action for each element of this stream.	-> We can’t use Spliterator or Iterator to iterate elements.
    4.	It is used to store unlimited number of elements.                     -> Stream API is used to process on the elements of a Collection.
    5.	Typically, it uses External Iteration concept to iterate Elements such as Iterator.	 -> Stream API uses internal iteration to iterate Elements, using forEach methods.
    6.	Collection Object is constructed Eagerly.	                          -> Stream Object is constructed Lazily.
    7.	We add elements to Collection object only after it is computed completely.  ->	We can add elements to Stream Object without any prior computation. That means Stream objects are computed on-demand.
    8.	We can iterate and consume elements from a Collection Object at any number of times.	-> We can iterate and consume elements from a Stream Object only once.

2.What does the map() function do? why you use it?
-------------------------------------------------------------------------------------
->The map() function perform map functional operation in Java. This means it can transform one type of object to others by applying a function.

->For example, if you have a List of String and you want to convert that to a List of Integer, you can use map() to do so.

->Just supply a function to convert String to Integer e.g., parseInt() to map() and it will apply that to all elements of List and give you a List 
of Integer. In other words, the map can convert one object to another.

3.What does the filter() method do? when you use it? 
----------------------------------------------------------------------
->The filter method is used to filter elements that satisfy a certain condition that is specified using a Predicate function.

->A predicate function is nothing but a function that takes an Object and returns a boolean. For example, if you have a List of Integer and 
  you want a list of even integers.

->In this case, you can use the filter to achieve that. You supply a function to check if a number is even or odd, just like this function and 
  filter will apply this to stream elements and filter the elements which satisfy the condition and which doesn't.

4.What does the flatmap() function do? why you need it?
----------------------------------------------------------
->The flatmap function is an extension of the map function. Apart from transforming one object into another, it can also flatten it.
->For example, if you have a list of the list but you want to combine all elements of lists into just one list. In this case, you can use 
  flatMap() for flattening. At the same time, you can also transform an object like you do use map() function.

5.What is difference between flatMap() and map() functions? 
-------------------------------------------------------------------
->Even though both map() and flatMap() can be used to transform one object to another by applying a method on each element.

->The main difference is that flatMap() can also flatten the Stream. For example, if you have a list of the list, then you can convert 
  it to a big list by using flatMap() function.

6.What is the difference between intermediate and terminal operations on Stream? 
------------------------------------------------------------------------------------------
->The intermediate Stream operation returns another Stream, which means you can further call other methods of Stream class to compose a pipeline.

->For example after calling map() or flatMap() you can still call filter() method on Stream.

-> the terminal operation produces a result other than Streams like a value or a Collection.

-> Once a terminal method like forEach() or collect() is called, you cannot call any other method of Stream or reuse the Stream.

7.What does the peek() method does? When should you use it?
----------------------------------------------------------------
->The peek() method of Stream class allows you to see through a Stream pipeline. You can peek through each step and print meaningful messages 
  on the console. It's generally used for debugging issues related to lambda expression and Stream processing.

8.What do you mean by saying Stream is lazy?
----------------------------------------------
When we say Stream is lazy, we mean that most of the methods defined on Java .util.stream.Stream class is lazy i.e. they will not work by just 
including them on Stream pipeline.

They only work when you call a terminal method on the Stream and finish as soon as they find the data they are looking for rather than scanning 
through the whole set of data.

9.What is a functional interface in Java 8?
-------------------------------------------------
->a functional interface is an interface that represents a function. Technically, an interface with just one abstract method is called a functional interface.

->You can also use @FunctionalInterface to annotated a functional interface. In that case, the compiler will verify if the interface actually contains just one abstract method or not. It's like the @Override annotation, which prevents you from accidental errors.

->Another useful thing to know is that If a method accepts a functional interface, then you can pass a lambda expression to it.

->Some examples of the functional interface are Runnable, Callable, Comparator, and Comparable from old API and Supplier, Consumer, and Predicate, etc. from new function API. 

10.What is the difference between a normal and functional interface in Java? 
-----------------------------------------------------------------------------------
->The normal interface in Java can contain any number of abstract methods, while the functional interface can only contain just one abstract method.

->You might be thinking why they are called functional interfaces? Once you know the answer, it might be a little easier for you to remember the concept.

->Well, they are called functional interfaces because they wrap a function as an interface. The function is represented by the single abstract method on the interface.

11.What is difference between findFirst() and findAny() method?
------------------------------------------------------------------
->The findFirst() method will return the first element meeting the criterion i.e. Predicate, while the findAny() method will return any element 
 meeting the criterion, very useful while working with a parallel stream.
 
 12.What is a Predicate interface?
 -------------------------------------------
 ->A Predicate is a functional interface that represents a function, which takes an Object and returns a boolean. 
 It is used in several Stream methods like filter(), which uses Predicate to filter unwanted elements.
 
 here is how a Predicate function looks like:
 
 public boolean test(T object){
    return boolean; 
 }
 
 ->You can see it just has one test() method which takes an object and returns a boolean. The method is used to test a condition if it passes; 
   it returns true otherwise false.
   
 13.What are Supplier and Consumer Functional interface? 
 ----------------------------------------------------------
 ->The Supplier is a functional interface that returns an object. It's similar to the factory method or new(), which returns an object.
 
-> The Supplier has get() functional method, which doesn't take any argument and return an object of type T. This means you can use it anytime you 
   need an object.
 
-> Since it is a functional interface, you can also use it as the assignment target for a lambda expression or method reference.
 
-> A Consumer is also a functional interface in JDK 8, which represents an operation that accepts a single input argument and returns no result.
 
-> Unlike other functional interfaces, Consumer is expected to operate via side-effects. The functional method of Consumer is accept(T t), 
   and because it's a functional interface, you can use it as the assignment target for a lambda expression or method interface in Java 8.
 
 14.Can you convert an array to Stream? How?
 -----------------------------------------------
 Yes, you can convert an array to Stream in Java. The Stream class provides a factory method to create a Stream from an array, like Stream .of(T ...) which accepts a variable argument, that means you can also pass an array to it as shown in the following example:
 
 String[] languages = {"Java", "Python", "JavaScript"};
 Stream numbers = Stream.of(languages);
 numbers.forEach(System.out::println);
 
 Output:
 Java
 Python
 JavaScript


15.What is the parallel Stream? How can you get a parallel stream from a List? 
------------------------------------------------------------------------------
A parallel stream can parallel execute stream processing tasks. For example, if you have a parallel stream of 1 million orders and you are 
 looking for orders worth more than 1 million, then you can use a filter to do that.

Unlike sequential Stream, the parallel Stream can launch multiple threads to search for those orders on the different part of Stream and then 
combine the result.

16.When do we go for Java 8 Stream API? Why do we need to use Java 8 Stream API in our projects?
-------------------------------------------------------------------------------------------------------
->When our Java project wants to perform the following operations, it’s better to use Java 8 Stream API to get lot of benefits:

When we want perform Database like Operations. For instance, we want perform groupby operation, orderby operation etc.
->When want to Perform operations Lazily.
->When we want to write Functional Style programming.
->When we want to perform Parallel Operations.
->When want to use Internal Iteration
->When we want to perform Pipelining operations.
->When we want to achieve better performance.

16.What is Spliterator in Java SE 8?Differences between Iterator and Spliterator in Java SE 8?
----------------------------------------------------------------------------------------------------------------------------------------
->Spliterator stands for Splitable Iterator. It is newly introduced by Oracle Corporation as part Java SE 8.
Like Iterator and ListIterator, It is also one of the Iterator interface.

    	Spliterator	                                 Iterator
1.	It is introduced in Java SE 8.	               ->It is available since Java 1.2.
2.	Splitable Iterator	                           ->Non-Splitable Iterator
3.	It is used in Stream API.	                   ->It is used for Collection API.
4.	It uses Internal Iteration concept to iterate Streams.	 ->It uses External Iteration concept to iterate Collections.
5.	We can use Spliterator to iterate Streams in Parallel and Sequential order. ->We can use Iterator to iterate Collections only in Sequential order.
6.	We can get Spliterator by calling spliterator() method on Stream Object. ->We can get Iterator by calling iterator() method on Collection Object.
7.	Important Method: tryAdvance()	                                         ->Important Methods: next(), hasNext()

16.What is Lambda Expression?
-----------------------------
->Lambda Expression is an anonymous function which accepts a set of input parameters and returns results.
->Lambda Expression is a block of code without any name, with or without parameters and with or without results. 
  This block of code is executed on demand.
->Lambda expressions are the method without name i.e Anonymous method. In other words, Lambda expression is a function that can be passed 
  around and referenced as an object.
->Lambda expressions introduce functional style processing in Java and facilitate the writing of compact and easy-to-read code.
->Because of this, lambda expressions are a natural replacement for anonymous classes as method arguments. One of their main uses is to define
  inline implementations of functional interfaces.

17.What are the three parts of a Lambda Expression? What is the type of Lambda Expression?
-------------------------------------------------------------------------------------------------
   A Lambda Expression contains 3 parts:
   
   1.Parameter List::- A Lambda Expression can contain zero or one or more parameters. It is optional.
   -------------- 
   
   Lambda Arrow Operator::-   “->” is known as Lambda Arrow operator. It separates the parameters list and body.
   -----------------------
   
   Lambda Expression Body::-
   --------------------------
   The type of “Journal Dev” is java.lang.String. The type of “true” is Boolean. In the same way, what is the type of a Lambda Expression?
   The Type of a Lambda Expression is a Functional Interface.
   
   Example:- What is the type of the following Lambda Expression?
   
      () -> System.out.println("Hello World");
   This Lambda Expression does not have parameters and does return any results. So it’s type is “java.lang.Runnable” Functional Interface.


18.What is a Functional Interface? What is SAM Interface?
--------------------------------------------------------------
->A Functional Interface is an interface, which contains one and only one abstract method. Functional Interface is also known as SAM Interface 
  because it contains only one abstract method.

->SAM Interface stands for Single Abstract Method Interface. Java SE 8 API has defined many Functional Interfaces.

19.Is is possible to define our own Functional Interface? What is @FunctionalInterface? What are the rules to define a Functional Interface?
---------------------------------------------------------------------------------------------------------------------------------------------
->Yes, it is possible to define our own Functional Interfaces. We use Java SE 8’s @FunctionalInterface annotation to mark an interface as Functional Interface.

We need to follow these rules to define a Functional Interface:
->Define an interface with one and only one abstract method.
->We cannot define more than one abstract method.
->Use @FunctionalInterface annotation in interface definition.
->We can define any number of other methods like Default methods, Static methods.
->If we override java.lang.Object class’s method as an abstract method, which does not count as an abstract method.

20.Is @FunctionalInterface annotation mandatory to define a Functional Interface? What is the use of @FunctionalInterface annotation? Why do we need Functional Interfaces in Java?
----------------------------------------------------------------------------------------------------------------------------------------------------------
->It is not mandatory to define a Functional Interface with @FunctionalInterface annotation. If we don’t want, We can omit this annotation. However, if we use it in Functional Interface definition, Java Compiler forces to use one and only one abstract method inside that interface.

->Why do we need Functional Interfaces? The type of a Java SE 8’s Lambda Expression is a Functional Interface. Whereever we use Lambda Expressions that means we are using Functional Interfaces.

21.What is Optional in Java 8? What is the use of Optional?Advantages of Java 8 Optional?
-----------------------------------------------------------------------------------------------
   Optional:
   ->Optional is a final Class introduced as part of Java SE 8. It is defined in java.util package.
   ->It is used to represent optional values that are either exist or not exist. It can contain either one value or zero value. 
     If it contains a value, we can get it. Otherwise, we get nothing.
   ->It is a bounded collection that is it contains at most one element only. It is an alternative to the “null” value.
   
   Main Advantage of Optional is:
   ->It is used to avoid null checks.
   ->It is used to avoid “NullPointerException”.

Q22.What is Type Inference? Is Type Inference available in older versions like Java 7 and Before 7 or it is available only in Java SE 8?
----------------------------------------------------------------------------------------------------------------------------------------
->Type Inference means determining the Type by compiler at compile-time.
->It is not a new feature in Java SE 8. It is available in Java 7 and before Java 7 too.

Before Java 7:-
--------------
Let us explore Java arrays. Define a String of Array with values as shown below:

String str[] = { "Java 7", "Java 8", "Java 9" };
Here we have assigned some String values at the right side, but not defined its type. Java Compiler automatically infers its type and creates a String of Array.

Java 7:
------
Oracle Corporation has introduced “Diamond Operator” new feature in Java SE 7 to avoid unnecessary Type definition in Generics.

Map<String,List<Customer>> customerInfoByCity = new HashMap<>();
Here we have not defined Type information at the right side, simply defined Java SE 7’s Diamond Operator.

Java SE 8:
----------
Oracle Corporation has enhanced this Type Inference concept a lot in Java SE 8. We use this concept to define Lambda Expressions, Functions, Method References etc.

ToIntBiFunction<Integer,Integer> add = (a,b) -> a + b;
Here Java Compiler observes the type definition available at the left-side and determines the type of Lambda Expression parameters a and b as Integers.
---------------------------------------------------------------------------------------------------------------------------------------------
23.What is Internal Iteration in Java SE 8?
-----------------------------------------
->Before Java 8, We don’t Internal Iteration concept. Java 8 has introduced a new feature known as “Internal Iteration”. Before Java 8, 
  Java Language has only External Iteration to iterate elements of an Aggregated Object like Collections, Arrays etc.

->Internal Iteration means “Iterating an Aggregated Object elements one by one internally by Java API”. Instead of Java Application do 
  iteration externally, We ask Java API to do this job internally.

24.Differences between External Iteration and Internal Iteration?
------------------------------------------------------------------
	External Iteration	                                      Internal Iteration
1.	Available before Java 8 too.	                         ->It is introduced in Java SE 8
2.	Iterating an Aggregated Object elements externally.	     ->Iterating an Aggregated Object elements internally (background).
3.	Iterate elements by using for-each loop and Iterators like Enumeration, Iterator, ListIterator.	->Iterate elements by using Java API like “forEach” method.
4.	Iterating elements in Sequential and In-Order only.	     ->Not required to iterate elements in Sequential order.
5.	It follows OOP approach that is Imperative Style.	     ->It follows Functional Programming approach that is Declarative Style.
6.	It does NOT separate responsibilities properly that is, it defines both “What is to be done” and “How it is to be done”.  ->It defines only “What is to be done”. No need to worry about “How it is to be done”. Java API takes care about “How to do”.
7.	Less Readable Code.	                                     ->More Readable code.

25.What are the major drawbacks of External Iteration?
-------------------------------------------------------
External Iteration has the following drawbacks:

->We need to write code in Imperative Style.
->There is no clear separation of Responsibilities. Tightly-Coupling between “What is to be done” and “How it is to be done” code.
->Less Readable Code.
->More Verbose and Boilerplate code.
->We have to iterate elements in Sequential order only.
->It does not support Concurrency and Parallelism properly.

26.What are the major advantages of Internal Iteration over External Iteration?
-----------------------------------------------------------------------------------------------
   Compare to External Iteration, Internal Iteration has the following advantages:
   
-> As it follows Functional Programming style, we can write Declarative Code.
->    More Readable and concise code.
->    Avoids writing Verbose and Boilerplate code
 ->   No need to iterate elements in Sequential order.
 ->   It supports Concurrency and Parallelism properly.
 ->   We can write Parallel code to improve application performance.
 ->   Clear separation of Responsibilities. Loosely-Coupling between “What is to be done” and “How it is to be done” code.
  ->  We need to write code only about “What is to be done” and Java API takes care about “How it is to be done” code.

27.What is the major drawback of Internal Iteration over External Iteration?
-------------------------------------------------------------------------------------------------------------
   Compare to External Iteration, Internal Iteration has one major drawback:
   
 ->  In Internal Iteration, as Java API takes care about Iterating elements internally, we do NOT have control over Iteration.

28.What is the major advantage of External Iteration over Internal Iteration?
--------------------------------------------------------------------------------------------
Compare to Internal Iteration, External Iteration has one major advantage:

->In External Iteration, as Java API does NOT take care about Iterating elements, we have much control over Iteration.

29.When do we need to use Internal Iteration? When do we need to use External Iteration?
-----------------------------------------------------------------------------------------------------------------------------------
   We need to understand the situations to use either Internal Iteration or External Iteration.
   
 >  When we need more control over Iteration, we can use External Iteration.
 >  When we do NOT need more control over Iteration, we can use Internal Iteration.
 >  When we need to develop Highly Concurrency and Parallel applications and we , we should use Internal Iteration.

30.Differences between Intermediate Operations and Terminal Operations of Java 8’s Stream API?
----------------------------------------------------------------------------------------------------------------------------------------
     Stream Intermediate Operations	                                                Stream Terminal Operations
1.	Stream Intermediate operations are not evaluated until we chain it with Stream Terminal Operation.-> Stream Terminal Operations are evaluated on it’s own. No need other operations help.
2.	The output of Intermediate Operations is another Stream.     -> 	The output of Intermediate Operations is Not a Stream. Something else other than a Stream.
3.	Intermediate Operations are evaluated Lazily.	             ->     Terminal Operations are evaluated Eagerly.
4.	We can chain any number of Stream Intermediate Operations.	 ->     We can NOT chain Stream Terminal Operations.
5.	We can use any number of Stream Intermediate Operations per Statement. -> 	We can use only one Stream Terminal Operation per Statement.

31.Is it possible to provide method implementations in Java Interfaces? If possible, how do we provide them?
---------------------------------------------------------------------------------------------------------------------
   In Java 7 or earlier, It is not possible to provide method implementations in Interfaces. Java 8 on-wards, it is possible.
   
   In Java SE 8, We can provide method implementations in Interfaces by using the following two new concepts:
   
  ->   Default Methods
  ->   Static Methods

32.What is a Default Method? Why do we need Default methods in Java 8 Interfaces?
----------------------------------------------------------------------------------------------
   A Default Method is a method which is implemented in an interface with “default” keyword. It’s new featured introduced in Java SE 8.
   
   We need Default Methods because of the following reasons:
   
 ->  It allow us to provide method’s implementation in Interfaces.
 ->  To add new Functionality to Interface without breaking the Classes which implement that Interface.
 ->  To provide elegant Backwards Compatibility Feature.
 ->  To ease of extend the existing Functionality.
 ->  To ease of Maintain the existing Functionality.
 
33.What is a Static Method? Why do we need Static methods in Java 8 Interfaces?
------------------------------------------------------------------------------------------------------
   A Static Method is an Utility method or Helper method, which is associated to a class (or interface). It is not associated to any object.
   
   We need Static Methods because of the following reasons:
   
  -> We can keep Helper or Utility methods specific to an interface in the same interface rather than in a separate Utility class.
  -> We do not need separate Utility Classes like Collections, Arrays etc to keep Utility methods.
  -> Clear separation of Responsibilities. That is we do not need one Utility class to keep all Utility methods of Collection API like Collections etc.
  -> Easy to extend the API.
  -> Easy to Maintain the API. 
 
34.Differences between Functional Programming and Object-Oriented Programming?
-----------------------------------------------------------------------------------
  Functional Programming	                                    OOP
>    Does not exist State                        ->     	Exists State
>    Uses Immutable data	                       ->         Uses Mutable data
>    It follows Declarative Programming Model	   ->         It follows Imperative Programming Model
>    Stateless Programming Model	                ->        Stateful Programming Model
>    Main Fcous on: “What you are doing”	        ->        Main focus on “How you are doing”
>    Good for Parallel (Concurrency) Programming   ->   	Poor for Parallel (Concurrency) Programming
>    Good for BigData processing and analysis      ->   	NOT Good for BigData processing and analysis
>    Supports pure Encapsulation	                  ->      It breaks Encapsulation concept
>    Functions with No-Side Effects	              ->      Methods with Side Effects
>    Functions are first-class citizens	           ->     Objects are first-class citizens
>    Primary Manipulation Unit is “Function”	        ->    Primary Manipulation Unit is Objects(Instances of Classes)
>    Flow Controls: Function calls, Function Calls with Recursion	  ->    Flow Controls: Loops, Conditional Statements
>    It uses “Recursion” concept to iterate Collection Data.	      ->   It uses “Loop” concept to iterate Collection Data. For example:-For-each loop in Java
>    Order of execution is less importance.	                     ->    Order of execution is must and very important.
>    Supports both “Abstraction over Data” and “Abstraction over Behavior”. ->	Supports only “Abstraction over Data”.
>    We use FP when we have few Things with more operations.	               -> We use OOP when we have few Operations with more Things. For example: Things are classes and Operations are Methods in Java. 
  
 
35.Explain issues of Old Java Date API? What are the advantages of Java 8’s Date and Time API over Old Date API and Joda Time API?
-------------------------------------------------------------------------------------------------------------------------------------------
 Java’s OLD Java Date API means Date API available before Java SE 8 that is Date, Calendar, SimpleDateFormat etc.
 
 Java’s Old Date API has the following Issues or Drawbacks compare to Java 8’s Date and Time API and Joda Time API.
 
  >Most of the API is deprecated.
  >Less Readability.
  >java.util.Date is Mutable and not Thread-Safe.
  >java.text.SimpleDateFormat is not Thread-Safe.
  >Less Performance.
 
 Java SE 8’s Date and Time API has the following Advantages compare to Java’s OLD Date API.
 
 >Very simple to use.
 >Human Readable Syntax that is More Readability.
 >All API is Thread-Safe.
 >Better Performance. 

36.Why do we need new Date and Time API in Java SE 8?Explain how Java SE 8 Data and Time API solves issues of Old Java Date API?
-------------------------------------------------------------------------------------------------------------------------------------------------
->We need Java 8’s Date and Time API to develop Highly Performance, Thread-Safe and Highly Scalable Java Applications.

->Java 8’s Date and Time API solves all Java’s Old Date API issues by following Immutability and Thread-Safety principles.

37.Differences between Java’s OLD Java Date API and Java 8’s Date and Time API:
--------------------------------------------------------------------------------

	Java’s OLD Java Date API	                      Java 8’s Date and Time API
1.	Available before Java 8 too.	                ->It is introduced in Java SE 8
2.	Not Thread Safe.	                            ->Thread Safe.
3.	Mutable API.	                                ->Immutable API.
4.	Less Performance.	                            ->Better Performance.
5.	Less Readability.	                            ->More Readability.
6.	It’s not recommended to use as its deprecated.	->It’s always recommended to use.
7.	Not Extendable.	                                ->Easy to Extend.
8.	It defines months values from 0 to 11, that is January = 0.	 ->It defines months values from 1 to 12, that is January = 1.
9.	It’s an old API.	                             ->It’s a new API.
  
  
 38.What is Multiple Inheritance? How Java 8 supports Multiple Inheritance?
 ----------------------------------------------------------------------------------------
 -> Multiple Inheritance means a class can inherit or extend characteristics and features from more than one parent class.
  
 -> In Java 7 or Earlier, Multiple Inheritance is not possible because Java follows “A class should extend one and only one class or abstract class” Rule. However, it’s possible to provide Multiple Implementation Inheritance using Interface because Java follows “A class can extend any number of Interfaces” Rule.
  
 -> However, Java 8 supports “Implementing Methods in Interfaces” by introducing new features: Default methods in Interface. Because of this feature, Java 8 supports Multiple Inheritance with some limitations.
  
39.What is Diamond Problem in interfaces due to default methods? How Java 8 Solves this problem?
----------------------------------------------------------------------------------------------------------------
  > Java 8 default methods can introduce a diamond problem when a class implements multiple interfaces. It occurs when a Class extends more than one interfaces with the same method implementations (Default method).
   
   diamond problem interface default methods
   
   Sample Java SE 8 Code to show the Diamond Problem with interface default methods.
   
 >  interface A {
   	default void display() {
   		System.out.println("A");
   	}
   }
   
 > interface B extends A {
   	default void display() {
   		System.out.println("B");
   	}
   }
   
  > interface C extends A {
   	default void display() {
   		System.out.println("C");
   	}
   }
   
   >class D implements B, C {
   }
   In the above code snippet, class D gives compile-time error as “Duplicate default methods named display with the parameters () and () are inherited from the types C and B”. It’s because Java Compiler will get confused about which display() to use in class D. Class D inherits display() method from both interfaces B and C. To solve this problem, Java 8 has given the following solution.
   
   >class D implements B, C {
   	@Override
   	public void display() {
   		B.super.display();
   	}
   }

   This B.super.display(); will solve this Diamond Problem. If you want to use C interface default method, then use C.super.display();.
   
   
    1. Streams - Serial vs Parallel Streams, List to map, Flatmap, Grouping,Supplier,Consumer,Predicate,Functions, Creating Functional interfaces and using them in Code.
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    -----------------------------------------------
    2. Singleton pattern, class level locking.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------
      Object level and Class level locks in Java
    ------------------------------------------``````````
    ------------------
    #Synchronization :
     ----------------Synchronization is a modifier which is used for method and block only. With the help of synchronized modifier we can restrict a
     shared resource to be accessed only by one thread. When two or more threads need access to shared resources, there is some loss of data i.e.
     data inconsistency. The process by which we can achieve data consistency between multiple threads it is called Synchronization.
    
    ->Synchronization in java is a strategy or a method to avoid thread interference and hence protecting the data from inconsistency.
     synchronization is also one of the way to make code thread safe. Through synchronization, we can make the threads to execute particular method or block in sync not simultaneously.
    
    -> Synchronization in java is implemented using synchronized keyword. synchronized keyword can be used with methods or blocks but not with the variables.
    
    -> When a method or block is declared as synchronized, only one thread can enter into that method or block. When one thread is executing synchronized method or block,
     the other threads which wants to execute that method or block wait or suspend their execution until first thread is done with that method or block. Thus avoiding the
     thread interference and achieving thread safeness. This can be explained well with the help of an example.
    Ex::
    ----
    class Shared
    {
        int i;
    
        synchronized void SharedMethod()
        {
            Thread t = Thread.currentThread();
    
            for(i = 0; i <= 1000; i++)
            {
                System.out.println(t.getName()+" : "+i);
            }
        }
    }
    
    public class ThreadsInJava
    {
        public static void main(String[] args)
        {
            final Shared s1 = new Shared();
    
            Thread t1 = new Thread("Thread - 1")
            {
                @Override
                public void run()
                {
                    s1.SharedMethod();
                }
            };
    
            Thread t2 = new Thread("Thread - 2")
            {
                @Override
                public void run()
                {
                    s1.SharedMethod();
                }
            };
    
            t1.start();
    
            t2.start();
        }
    }
    
    The Logic Behind The Synchronization In Java :
    ----------------------------------------------
    -> The synchronization in java is built around an entity called object lock or monitor. Here is the brief description about lock or monitor.
    -> Whenever an object is created to any class, an object lock is created and is stored inside the object.
    -> One object will have only one object lock associated with it.
    -> Any thread wants to enter into synchronized methods or blocks of any object, they must acquire object lock associated with that object and release the lock after they are
       done with the execution.
    -> The other threads which wants to enter into synchronized methods of that object have to wait until the currently executing thread releases the object lock.
    -> To enter into static synchronized methods or blocks, threads have to acquire class lock associated with that class as static members are stored inside the class memory.
    
    Synchronized Blocks :
    ---------------------
    Some times, you need only some part of the method to be synchronized not the whole method. This can be achieved with synchronized blocks.
    Synchronized blocks must be defined inside a definition blocks like methods, constructors, static initializer or instance initializer.
    
    synchronized block takes one argument and it is called mutex. if synchronized block is defined inside non-static definition blocks like non-static methods, instance initializer
    or constructors, then this mutex must be an instance of that class. If synchronized block is defined inside static definition blocks like static methods or static initializer,
    then this mutex must be like ClassName.class.
    
    Here is an example of static and non-static synchronized blocks.
    Ex::
    ---
    class Shared
    {
        static void staticMethod()
        {
            synchronized (Shared.class)
            {
                //static synchronized block
            }
        }
    
        void NonStaticMethod()
        {
            synchronized (this)
            {
                //Non-static synchronized block
            }
        }
    
        void anotherNonStaticMethod()
        {
            synchronized (new Shared())
            {
                //Non-static synchronized block
            }
        }
    }
    
    10 Points-To-Remember About Synchronization In Java :
    ------------------------------------------------------
    
    1) You can use synchronized keyword only with methods but not with variables, constructors, static initializer and instance initializers.
    
    class Shared
    {
        synchronized int i;    //compile time error, can't use synchronized keyword with variables
    
        synchronized public Shared()
        {
            //compile time error, constructors can not be synchronized
        }
    
        synchronized static
        {
            //Compile time error, Static initializer can not be synchronized
        }
    
        synchronized
        {
            //Compile time error, Instance initializer can not be synchronized
        }
    }
    2) Constructors, Static initializer and instance initializer can’t be declared with synchronized keyword, but they can contain synchronized blocks.
    
    class Shared
    {
        public Shared()
        {
            synchronized (this)
            {
                //synchronized block inside a constructor
            }
        }
    
        static
        {
            synchronized (Shared.class)
            {
                //synchronized block inside a static initializer
            }
        }
    
        {
            synchronized (this)
            {
                //synchronized block inside a instance initializer
            }
        }
    }
    3) Both static and non-static methods can use synchronized keyword. For static methods, thread need class level lock and for non-static methods, thread need object level lock.
    
    class Shared
    {
        synchronized static void staticMethod()
        {
            //static synchronized method
        }
    
        synchronized void NonStaticMethod()
        {
            //Non-static Synchronized method
        }
    }
    4) It is possible that both static synchronized and non-static synchronized methods can run simultaneously. Because, static methods need class level lock and non-static methods need object level lock.
    
    5) A method can contain any number of synchronized blocks. This is like synchronizing multiple parts of a method.
    
    class Shared
    {
        static void staticMethod()
        {
            synchronized (Shared.class)
            {
                //static synchronized block - 1
            }
    
            synchronized (Shared.class)
            {
                //static synchronized block - 2
            }
        }
    
        void NonStaticMethod()
        {
            synchronized (this)
            {
                //Non-static Synchronized block - 1
            }
    
            synchronized (this)
            {
                //Non-static Synchronized block - 2
            }
        }
    }
    6) Synchronization blocks can be nested.
    
    synchronized (this)
    {
        synchronized (this)
        {
            //Nested synchronized blocks
        }
    }
    7) Lock acquired by the thread before executing a synchronized method or block must be released after the completion of execution, no matter whether execution is completed normally or abnormally (due to exceptions).
    
    8) Synchronization in java is Re-entrant in nature. A thread can not acquire a lock that is owned by another thread. But, a thread can acquire a lock that it already owns. That means if a synchronized method gives a call to another synchronized method which needs same lock, then currently executing thread can directly enter into that method or block without acquiring the lock.
    
    9) synchronized method or block is very slow. They decrease the performance of an application. So, special care need to be taken while using synchronization. Use synchronization only when you needed it the most.
    
    10) Use synchronized blocks instead of synchronized methods. Because, synchronizing some part of a method improves the performance than synchronizing the whole method.
    
    
    Why do you need Synchronization?
    --------------------------------
    Let us assume if you have two threads that are reading and writing to the same ‘resource’. Suppose there is a variable named as geek, and you want that at one time only one thread should
    access the variable(atomic way). But Without the synchronized keyword, your thread 1 may not see the changes thread 2 made to geek, or worse, it may only be half changed that cause the
    data inconsistency problem. This would not be what you logically expect. The tool needed to prevent these errors is synchronization.
    
    In synchronization, there are two types of locks on threads:
    ------------------------------------------------------------
    
    Object level lock :
    -------------------Every object in java has a unique lock. Whenever we are using synchronized keyword, then only lock concept will come in the
    picture. If a thread wants to execute synchronized method on the given object. First, it has to get lock of that object. Once thread got the lock
    then it is allowed to execute any synchronized method on that object. Once method execution completes automatically thread releases the lock.
    Acquiring and release lock internally is taken care by JVM and programmer is not responsible for these activities. Lets have a look on the below
    program to understand the object level lock:
    ex:
    // Java program to illustrate Object lock concept
    class Geek implements Runnable {
        public void run()
        {
            Lock();
        }
        public void Lock()
        {
            System.out.println(Thread.currentThread().getName());
            synchronized(this)
            {
                System.out.println("in block "
                    + Thread.currentThread().getName());
                System.out.println("in block " +
                    Thread.currentThread().getName() + " end");
            }
        }
    
        public static void main(String[] args)
        {
            Geek g = new Geek();
            Thread t1 = new Thread(g);
            Thread t2 = new Thread(g);
            Geek g1 = new Geek();
            Thread t3 = new Thread(g1);
            t1.setName("t1");
            t2.setName("t2");
            t3.setName("t3");
            t1.start();
            t2.start();
            t3.start();
        }
    }
    
    Class level lock :
    -----------------Every class in java has a unique lock which is nothing but class level lock. If a thread wants to execute a static synchronized
    method, then thread requires class level lock. Once a thread got the class level lock, then it is allowed to execute any static synchronized
    method of that class. Once method execution completes automatically thread releases the lock. Lets look on the below program for better
    understanding:
    Ex:
    // Java program to illustrate class level lock
    class Geek implements Runnable {
        public void run()
        {
            Lock();
        }
    
        public void Lock()
        {
            System.out.println(Thread.currentThread().getName());
            synchronized(Geek.class)
            {
                System.out.println("in block "
                    + Thread.currentThread().getName());
                System.out.println("in block "
                    + Thread.currentThread().getName() + " end");
            }
        }
    
        public static void main(String[] args)
        {
            Geek g1 = new Geek();
            Thread t1 = new Thread(g1);
            Thread t2 = new Thread(g1);
            Geek g2 = new Geek();
            Thread t3 = new Thread(g2);
            t1.setName("t1");
            t2.setName("t2");
            t3.setName("t3");
            t1.start();
            t2.start();
            t3.start();
        }
    }
    
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------
    3. Java 8 Metaspace, how to avoid Out of memory
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    4. How to create Immutable class.
    ---------------------------------
    
    ->Immutable classes are those class, whose object can not be modified once created, it means any modification on immutable object will result in another immutable object.
    best example to understand immutable and mutable objects are, String and StringBuffer. Since String is immutable class, any change on existing string object will result in
    another string e.g. replacing a character into String, creating substring from String, all result in a new objects. While in case of mutable object like StringBuffer,
    any modification is done on object itself and no new objects are created.
    
    Immutable class means that once an object is created, we cannot change its content. In Java, all the wrapper classes (like String, Boolean, Byte, Short) and String class is immutable.
    We can create our own immutable class as well.
    
    Following are the requirements:
    • Class must be declared as final (So that child classes can’t be created)
    • Data members in the class must be declared as final (So that we can’t change the value of it after object creation)
    • A parameterized constructor
    • Getter method for all the variables in it
    • No setters(To not have option to change the value of the instance variable)
    
    or
    -> Declare the class as final so it can’t be extended.
    -> Make all fields private so that direct access is not allowed.
    -> Don’t provide setter methods for variables
    -> Make all mutable fields final so that it’s value can be assigned only once.
    -> Initialize all the fields via a constructor performing deep copy.
    -> Perform cloning of objects in the getter methods to return a copy rather than returning the actual object reference.
    -> To understand points 4 and 5, let’s run the sample Final class that works well and values don’t get altered after instantiation.
    
    benefit::
    1) Immutable objects are by default thread safe, can be shared without synchronization in concurrent environment.
    2) Immutable object simplifies development, because its easier to share between multiple threads without external synchronization.
    
    3) Immutable object boost performance of Java application by reducing synchronization in code.
    
    4) Another important benefit of Immutable objects is reusability, you can cache Immutable object and reuse them, much like String literals and Integers
    
    
    Ex::
        public final class Student {
    
       private final String name;
        private final int id;
    
        public Student(String name, int id) {
            this.name = name;
            this.id = id;
        }
    
        public String getName() {
            return name;
        }
    
        public int getId() {
            return id;
        }
    }
    
    public class StudentImpl {
        public static void main(String[] args) {
            Student student = new Student("Arun",101);
            System.out.println(student.getName()+" "+student.getId());
        }
    }
    
    
    
    Note
    Immutable object is simple, thread-safe (no need synchronization), less prone to error and more secure. If possible, make all objects immutable.
    
    1. Mutable object – You can change the states and fields after the object is created. For examples: StringBuilder, java.util.Date and etc.
    
    2. Immutable object – You cannot change anything after the object is created. For examples: String, boxed primitive objects like Integer, Long and etc.
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    5. Future vs Callable Future.
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    6. Countdown latch (how it works),Reentrant lock
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    7. Thread life cycle (diff between sleep wait)
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    8. How do threads share data, what is ThreadLocal.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    9. Applicability of Comparable and Comparator.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------
    10. Comparison of time and space complexity of sorting algorithms.
    
    
    https://stackoverflow.com/questions/47073421/how-rest-can-use-soap-as-protocol
    https://javarevisited.blogspot.com/2012/07/why-enum-singleton-are-better-in-java.html
    https://crunchify.com/how-to-run-multiple-threads-concurrently-in-java-executorservice-approach/
    
     1. Cloning - deep vs shallow
     2. Serialization - what happens when we change the version.
     3. Weak References
    4. Linked Lists vs ArrayLists vs Map.(What ds to use when)
    5.   Blocking Queue
    6. Generics (Wildcards with example)
    7. Spring (Error handling, Global error Handling,Sync vs Async communication,Spring Batch triggering,)
    8. Hibernate advantages , disadvantages , queries, Criteria API, Transactions
    9. Oracle - pl/sql, procedures functions , viewes , grouping , Partitions.
    10. COncurrency vs Parallelism.
    11. Design patterns used by Spring
    12. Spring Bean Creation process and Bean life cycle.
    13. How container creates and configures beans in Spring.
    
    
    
    
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    1. forEach() method in Iterable interface:::
    ---------------------------------------------
    
    2.default and static methods in Interfaces:::
    ---------------------------------------------
    
    3. Functional Interfaces and Lambda Expressions:::
    --------------------------------------------------
    
    4. Java Stream API for Bulk Data Operations on Collections::
    ------------------------------------------------------------
    
    5. Java Time API:::
    -------------------
    
    6. Collection API improvements::
    ---------------------------------
    
    1.Difference Between Collections Vs Streams In Java :
    -----------------------------------------------------
          Collections	                                                     Streams
    -> Collections are mainly used to store and group the data.	Streams are mainly used to perform operations on data.
    -> You can add or remove elements from collections.        	You can’t add or remove elements from streams.
    -> Collections have to be iterated externally.	            Streams are internally iterated.
    -> Collections can be traversed multiple times.	            Streams are traversable only once.
    -> Collections are eagerly constructed.	                    Streams are lazily constructed.
    -> Ex : List, Set, Map…	                                    Ex : filtering, mapping, matching…
    
    
    7. Concurrency API improvements::
    ---------------------------------
    
    8. Java IO improvements::
    --------------------------------
    
    9. Miscellaneous Core API improvements::
    -----------------------------------------
    
    
    
    Funtional Interface::
    ---------------------
    Lambda E
    -----------------------------------------------------------------------
    Java Sorting Example(Comparable and Comparator):::
    --------------------------------------------------
    1. Sort an ArrayList::
    ---------------------
    Collections API’s utility class Collections provide a handy way to sort an ArrayList in a natural ordering provided all elements in the list must implement the Comparable interface.
    
    public class SimpleSorting {
    
        public static void main(String[] args) {
    
            List<String> locationList = new ArrayList<String>();
            locationList.add("California");
            locationList.add("Texas");
            locationList.add("Seattle");
            locationList.add("New Delhi");
    
            Collections.sort(locationList);
    
            for (String location : locationList) {
                System.out.println("Location is: " + location);
            }
        }
    }
    
    Output
    
    Location is: California
    Location is: New Delhi
    Location is: Seattle
    Location is: Texas
    
    Above example worked perfectly with Collections.Sort() as List defined in example was Lits<String> and java.lang.String implements the Comparable interface.
    With List contains custom objects, Collections.sort() will fail if the custom object does not implement the Comparable interface.
    --------------------------------------------------------------------------------------------------------------------------------------------------------
    2. Sort Object using Comparable::
    -------------------------------------
    public class Person implements Comparable<Person> {
    
        private String name;
        private int age;
    
        public Person(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public int getAge() {
            return age;
        }
    
        public void setAge(int age) {
            this.age = age;
        }
    
        public int compareTo(Person person) {
            return this.age > person.age ? 1 : this.age < person.age ? -1 : 0;
        }
    }
    
    To sort it, we will use Collections.sort() method
    
    public class ComparableDemo {
    
        public static void main(String[] args) {
    
            List<Person> persons = new ArrayList<Person>();
    
            persons.add(new Person("Umesh Awasthi", 35));
            persons.add(new Person("Robert Hickle", 55));
            persons.add(new Person("John Smith", 40));
            persons.add(new Person("David", 23));
    
            Collections.sort(persons);
    
            for (Person person : persons) {
                System.out.println("Person is: " + person.getName());
            }
    
        }
    }
    
    Output is
    
    Person is: David
    Person is: Umesh Awasthi
    Person is: John Smith
    Person is: Robert Hickle
    
    Pay close attention to Person class declaration public class Person implements Comparable<Person>  which shows that Person class is a
    comparable if you remove Comparable<Person> from your Person class and try Collection.sort(persons) , compile will give the following error
    
    Error:(21, 20) java: no suitable method found for sort(java.util.List<com.umeshawasthi.tutorials.corejava.sorting.Person>)
        method java.util.Collections.<T>sort(java.util.List<T>) is not applicable
          (inferred type does not conform to upper bound(s)
            inferred: com.umeshawasthi.tutorials.corejava.sorting.Person
            upper bound(s): java.lang.Comparable<? super com.umeshawasthi.tutorials.corejava.sorting.Person>)
        method java.util.Collections.<T>sort(java.util.List<T>,java.util.Comparator<? super T>) is not applicable
          (cannot infer type-variable(s) T
            (actual and formal argument lists differ in length))
    
    As a developer, it’s out responsibility to pass the comparable object to Collections.sort() method.
    
    It’s up to you how you want to use the compareTo method and what will be your comparison logic, I have used a simple logic to compare the age of the person but you can use any custom logic to in compareTo()  method.
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    3. Sort Object using Comparator :::
    -----------------------------------
    java.lang.Comparable interface provides only one way to compare this object with another one, what are the options in case we need to compare 2 objects to sort them correctly. How about sorting Person with a name or age?  What if we want to override the natural ordering? We need to create a Comparator for these requirements.
    
    First and super short template is to provide a custom comparator while calling Collections.sort()  method
    
    public class ComparatorDemo {
    
        public static void main(String[] args) {
            List<Person> persons = new ArrayList<Person>();
    
            persons.add(new Person("Umesh Awasthi", 35));
            persons.add(new Person("Robert Hickle", 55));
            persons.add(new Person("John Smith", 40));
            persons.add(new Person("David", 23));
            persons.add(new Person("David", 63));
    
            Collections.sort(persons, new Comparator<Person>() {
                public int compare(Person person, Person person1) {
                    int name = person.getName().compareTo(person1.getName());
                    if(name == 0){
                        return name;
                    }
                    return person.getAge() > person1.getAge() ? 1 : person.getAge() < person1.getAge() ? -1 : 0;
                }
            });
    
    
            for (Person person : persons) {
                System.out.println("Person is: " + person.getName());
            }
        }
    }
    
    Output
    
    Person is: David
    Person is: Umesh Awasthi
    Person is: John Smith
    Person is: Robert Hickle
    Person is: David
    
    Check the output, the interesting part is David, which is first and last result of the output.Another efficient way is to define the Comparators in the Contact itself to reuse them instead of recreating every time
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public class ComparatorPerson {
    
        private String name;
        private int age;
    
        public ComparatorPerson(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public int getAge() {
            return age;
        }
    
        public void setAge(int age) {
            this.age = age;
        }
    
        public static Comparator<ComparatorPerson> COMPARE_BY_NAME = new Comparator<ComparatorPerson>() {
            public int compare(ComparatorPerson one, ComparatorPerson other) {
                return one.name.compareTo(other.name);
            }
        };
    
        public static Comparator<ComparatorPerson> COMPARE_BY_AGE = new Comparator<ComparatorPerson>() {
            public int compare(ComparatorPerson one, ComparatorPerson other) {
                return one.age > other.age ? 1 : one.age < other.age ? -1 : 0;
            }
        };
    }
    
    Comparator with multi options
    ------------------------------
    public class MultiComparatorDemo {
        public static void main(String[] args) {
            List<ComparatorPerson> persons = new ArrayList<ComparatorPerson>();
    
            persons.add(new ComparatorPerson("Umesh Awasthi", 35));
            persons.add(new ComparatorPerson("Robert Hickle", 55));
            persons.add(new ComparatorPerson("John Smith", 40));
            persons.add(new ComparatorPerson("David", 23));
            persons.add(new ComparatorPerson("David", 63));
    
            Collections.sort(persons,ComparatorPerson.COMPARE_BY_AGE );
    
            for (ComparatorPerson person : persons) {
                System.out.println("Person Name Sorted by Age is: " + person.getName());
            }
    
            System.out.println("######################################################");
            Collections.sort(persons,ComparatorPerson.COMPARE_BY_NAME );
    
            for (ComparatorPerson person : persons) {
                System.out.println("Person Name Sorted by Name  is: " + person.getName());
            }
        }
    }
    
    Output
    
    Person Name Sorted by Age is: David
    Person Name Sorted by Age is: Umesh Awasthi
    Person Name Sorted by Age is: John Smith
    Person Name Sorted by Age is: Robert Hickle
    Person Name Sorted by Age is: David
    ######################################################
    Person Name Sorted by Name  is: David
    Person Name Sorted by Name  is: David
    Person Name Sorted by Name  is: John Smith
    Person Name Sorted by Name  is: Robert Hickle
    Person Name Sorted by Name  is: Umesh Awasthi
    -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    4. Sort Object using Java8:::
    ------------------------------
    Java 8 added a new way of making Comparators that reduces the amount of code you have to write Comparator.comparing. Java 8 streaming API provides neater approach. It’s always good to understand how compareTo()  and compare()  method work under the hood
    
    public class Java8CompareExample {
    
        public static void main(String[] args) {
    
            List<Person> persons = new ArrayList<Person>();
    
            persons.add(new Person("Umesh Awasthi", 35));
            persons.add(new Person("Robert Hickle", 55));
            persons.add(new Person("John Smith", 40));
            persons.add(new Person("David", 23));
            persons.add(new Person("David", 63));
    
            persons.sort(Comparator.comparing(Person::getName).thenComparingInt(Person::getAge));
    
            for (Person person : persons) {
                System.out.println("Person Name is: " + person.getName());
            }
    
            persons.sort(Comparator.comparingInt(Person::getAge).thenComparing(Person::getName));
    
            for (Person person : persons) {
                System.out.println("Person Name is: " + person.getName());
            }
        }
    }
----------------------------------------------------------------------------------------------------------------------------------------


=======================================================================================================================

#Core Java:::
=======================================================================================================================
    Basics::
    --------
    0.Explain JDK, JRE and JVM?
    ---------------------------
    JDK::-> It stands for Java Development Kit.
    -> It is the tool necessary to compile,document and package Java programs.
    ->Along with JRE, it includes an interpreter/loader, a compiler (javac), an archiver (jar), a
    documentation generator (javadoc) and other tools needed in Java development. In short, it contains JRE + development tools.
    
    JRE:::->It stands for Java Runtime Environment.
    -> JRE refers to a runtime environment in which java bytecode can be executed.
    -> It implements the JVM (Java Virtual Machine) and provides all the class libraries and other support files that JVM uses at runtime. So JRE is a software package that contains what is required to run a Java program.
    Basically, it’s an implementation of the JVM which physically exists.
    
    JVM::: ->It stands for Java Virtual Machine.
    -> It is an abstract machine. It is a specification that provides run-time environment in which java bytecode can be executed.
    ->JVM follows three notations: Specification(document that describes the implementation of the Java virtual machine),Implementation​ (program that meets the requirements of JVM specification) and ​ Runtime Instance ​ (instance of JVM is created whenever you write a java command on the command prompt and run class).
    ----------------------------------------------------------------------------------------
    1.What is the difference between public, protected, package-private and private in Java?
    ----------------------------------------------------------------------------------------
    The official tutorial may be of some use to you.
    _____________________________________________________________
                │ Class │ Package │ Subclass │ Subclass │ World
                │       │         │(same pkg)│(diff pkg)│
    ────────────┼───────┼─────────┼──────────┼──────────┼────────
    public      │   +   │    +    │    +     │     +    │   +
    ────────────┼───────┼─────────┼──────────┼──────────┼────────
    protected   │   +   │    +    │    +     │     +    │
    ────────────┼───────┼─────────┼──────────┼──────────┼────────
    no modifier │   +   │    +    │    +     │          │
    ────────────┼───────┼─────────┼──────────┼──────────┼────────
    private     │   +   │         │          │          │
    ____________________________________________________________
     + : accessible         blank : not accessible
    --------------------------------------------------------------
    Difference Between Class Variables And Instance Variables In Java :
    --------------------------------------------------------------------
    Class Variables	                                      Instance Variables
    
    -> Class variables are declared with keyword static.  ->	Instance variables are declared without static keyword.
    -> Class variables are common to all instances of a class. These variables are shared between the objects of a class.	-> Instance variables are not shared between the objects of a class. Each instance will have their own copy of instance variables.
    -> As class variables are common to all objects of a class, changes made to these variables through one object will reflect in another. ->	As each object will have its own copy of instance variables, changes made to these variables through one object will not reflect in another object.
    -> Class variables can be accessed using either class name or object reference.	-> Instance variables can be accessed only through object reference.
    -------------------------------------------------------------------------------
    #2.Explain public static void main(String args[]).
    ---------------------------------------------------------
    public::: Public is an access modifier, which is used to specify who can access this method. Public means that this Method will be accessible by any Class.
    static::​: It is a keyword in java which identifies it is class based i.e it can be accessed without creating the instance of a Class.
    void​ ::: It is the return type of the method. Void defines the method which will not return any value.
    main​ ::: It is the name of the method​ ​ which is searched by JVM as a starting point for an application with a particular signature only. It is the method where the main execution occurs.
    String args[]​ ::: It is the parameter passed to the main method.
    -----------------------------------------------------------------------------------------------------
    #3. Why Java is platform independent?
    ---------------------------------------
    Platform independent practically means “write once run anywhere”. Java is called so because of its byte codes which can run on any system irrespective of its underlying operating system.
    --------------------------------------------------------------------------------------------------
    #4. Why java is not 100% Object-oriented?
    -------------------------------------------
    Java is not 100% Object-oriented because it makes use of eight primitive datatypes such as boolean, byte, char, int,float, double, long, short which are ​ not objects.
    ---------------------------------------------------------------------------------------------------
    #5. Why java is not 100% Object-oriented?
    --------------------------------------------
    Java is not 100% Object-oriented because it makes use of eight primitive datatypes such as boolean, byte, char, int,float, double, long, short which are ​ not objects.
    ---------------------------------------------------------------------------------------------------
    #6. What are wrapper classes?
    Wrapper classes converts the java primitives into the reference types (objects). Every primitive data type has a class dedicated to it.
    These are known as wrapper classes because they “wrap” the primitive data type into an object of that class. Refer to the below image which displays different primitive type,
    wrapper class and constructor argument.
    --------------------------------------------------------------------------------------------------
    
    #7.Difference between constructor and method in Java
   --------------------------------------------------------------------------------------
    Java Constructor	                                        Java Method
    ----------------                                            -----------------------
    -> A constructor is used to initialize the state of an object.	-> A method is used to expose the behavior of an object.
    -> A constructor must not have a return type.	-> A method must have a return type.
    -> The constructor is invoked implicitly.	 -> The method is invoked explicitly.
    -> The Java compiler provides a default constructor if you don't have any constructor in a class.-> 	The method is not provided by the compiler in any case.
    -> The constructor name must be same as the class name.	 -> The method name may or may not be same as the class name.

===================================================================================================================

#OOPS::Object-Oriented Programming is a methodology or paradigm to design a program using classes and objects. It simplifies software development and maintenance by providing some concepts:
============================================================================================================    
1) Difference between method overloading and overriding        
2) Difference between abstraction and polymorphism  
3) What is multi-catch block in java and its usage?            
   4) What is static block and its usage?      
      5) Explain the most complex or recent project he worked on in 50-100 words max

          #Encapsulation in Java
          --------------------------------
          -> Encapsulation is data hiding.
          -> Encapsulation in Java is a process of wrapping code and data together into a single unit.
          -> We can create a fully encapsulated class in Java by making all the data members of the class private. Now we can use setter and getter 
             methods to set and get the data in it.
          -> The Java Bean class is the example of a fully encapsulated class.
    
          Advantage of Encapsulation in Java
          --------------------------------------------------
          -> By providing only a setter or getter method, you can make the class read-only or write-only. In other words, you can skip the getter or setter methods.
          -> It provides you the control over the data. Suppose you want to set the value of id which should be greater than 100 only, you can write the logic inside the setter method.
            You can write the logic not to store the negative numbers in the setter methods.
          -> It is a way to achieve data hiding in Java because other class will not be able to access the data through the private data members.
          -> The encapsulate class is easy to test. So, it is better for unit testing.
          --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
          Abstraction in Java
          ---------------------------
          -> Abstraction is detail hiding(implementation hiding).
          -> Abstraction is a process of hiding the implementation details and showing only functionality to the user.
          -> Another way, it shows only essential things to the user and hides the internal details, for example, sending SMS where you type the text         and send the message. You don't know the internal processing about the message delivery.
          -> Abstraction lets you focus on what the object does instead of how it does it.
    
          Ways to achieve Abstraction
          ----------------------------------------
          There are two ways to achieve abstraction in java
    
          -> Abstract class (0 to 100%)
          -> Interface (100%)
          ---------------------------
             Abstract class in Java
             A class which is declared as abstract is known as an abstract class. It can have abstract and non-abstract methods. It needs to be extended and its method implemented. It cannot be instantiated.
    
               Points to Remember
               An abstract class must be declared with an abstract keyword.
               It can have abstract and non-abstract methods.
               It cannot be instantiated.
               It can have constructors and static methods also.
               It can have final methods which will force the subclass not to change the body of the method.
    
   
    
  Difference between abstract class and interface
    -------------------------------------------------------------------
    -> Abstract class and interface both are used to achieve abstraction where we can declare the abstract methods. Abstract class and interface both can't be instantiated.
    
    But there are many differences between abstract class and interface that are given below.
    
    Abstract class                    Interface
    -------------------- --------------------------
    1) Abstract class can have abstract and non-abstract methods.-> Interface can have only abstract methods. Since Java 8, it can have default and static methods also.
    2) Abstract class doesn't support multiple inheritance.                ->	Interface supports multiple inheritance.
    3) Abstract class can have final, non-final, static and non-static variables. ->	Interface has only static and final variables.
    4) Abstract class can provide the implementation of interface. ->	Interface can't provide the implementation of abstract class.
    5) The abstract keyword is used to declare abstract class.	 -> The interface keyword is used to declare interface.
    6) An abstract class can extend another Java class and implement multiple Java interfaces.	 -> An interface can extend another Java interface only.
    7) An abstract class can be extended using keyword "extends". ->	An interface class can be implemented using keyword "implements".
    8) A Java abstract class can have class members like private, protected, etc.	->  Members of a Java interface are public by default.
    9)Example:
    public abstract class Shape{
    public abstract void draw();
    }
    
    Example:
    public interface Drawable{
    void draw();
    }
    
   
    ---------------------------------------------------------------------------------------------------
   
    # Polyporphism
    ---------------------------------------------------------------------------------------------------
    -> Polymorphism means one name many forms. In Java, polymorphism can be achieved by method overloading and method overriding.
    
    -> There are two types of polymorphism in java.
    
       1. Compile time polymorphism:-(Method Overloading)
       -----------------------------
        -> Compile time Polymorphism is nothing but method overloading in java. You can define various methods with same name but different method arguments.
        -> If a class has multiple methods having same name but different in parameters, it is known as Method Overloading.
        
       2. Run time polymorphism:-
       --------------------------
       -> Runtime Polymorphism is nothing but method overriding in java.If subclass is having same method as base class then it is known as method 
       overriding Or in another word, If subclass provides specific implementation to any method which is present in its one of parents classes then 
       it is known as method overriding.
       ->If subclass (child class) has the same method as declared in the parent class, it is known as method overriding in Java.
         
       -> In other words, If a subclass provides the specific implementation of the method that has been declared by one of its parent class,
        it is known as method overriding.
       
           #Usage of Java Method Overriding
           --------------------------------
            -> Method overriding is used to provide the specific implementation of a method which is already provided by its superclass.
            -> Method overriding is used for runtime polymorphism
           
           #Rules for Java Method Overriding
           -----------------------------------
             ->  The method must have the same name as in the parent class
             ->  The method must have the same parameter as in the parent class.
             ->  There must be an IS-A relationship (inheritance).
             
    --------------------------------------------------------------------------
     #Differences between method overloading and method overriding in java. A list of differences between method overloading and method overriding are given below:
     
     No.	Method Overloading	                                                             Method Overriding
     1)	Method overloading is used to increase the readability of the program.    ->	Method overriding is used to provide the specific implementation of the method that is already provided by its super class.
     2)	Method overloading is performed within class.	    -> Method overriding occurs in two classes that have IS-A (inheritance) relationship.
     3)	In case of method overloading, parameter must be different.	 -> In case of method overriding, parameter must be same.
     4)	Method overloading is the example of compile time polymorphism.	-> Method overriding is the example of run time polymorphism.
     5)	In java, method overloading can't be performed by changing return type of the method only. Return type can be same or different in method overloading. But you must have to change the parameter.	   ->  Return type must be same or covariant in method overriding.
             
    
    #Inheritance( IS-A Relationship)
    --------------------------------------------------------------------------------------------------
     ->Inheritance in Java is a mechanism in which one object acquires all the properties and behaviors of a parent object.
     ->The idea behind inheritance in Java is that you can create new classes that are built upon existing classes. When you inherit from an existing class, you can reuse methods and fields of the parent class. Moreover, you can add new methods and fields in your current class also.
     ->Inheritance represents the IS-A relationship which is also known as a parent-child relationship.
     
       # Why use inheritance in java
       ---------------------------
        -> For Method Overriding (so runtime polymorphism can be achieved).
        -> For Code Reusability.
        
       # Terms used in Inheritance
       ----------------------------
            -> Class: A class is a group of objects which have common properties. It is a template or blueprint from which objects are created.
            -> Sub Class/Child Class: Subclass is a class which inherits the other class. It is also called a derived class, extended class, or child class.
            -> Super Class/Parent Class: Superclass is the class from where a subclass inherits the features. It is also called a base class or a parent class.
            -> Reusability: As the name specifies, reusability is a mechanism which facilitates you to reuse the fields and methods of the existing class when you create a new class. You can use the same fields and methods already defined in the previous class.  
    --------------------------------------------------------------------------------------------------- 
    
    #Class::
    --------------------------
     -> Collection of objects is called class. It is a logical entity.
        
     -> A class can also be defined as a blueprint from which you can create an individual object. Class doesn't consume any space. 
    --------------------------
        
    #Object::
    -------------------------
     -> Any entity that has state and behavior is known as an object.It can be physical or logical.
     
     -> An object has three characteristics:
     
         1. State: represents the data (value) of an object.
         2. Behavior: represents the behavior (functionality) of an object such as deposit, withdraw, etc.
         3. Identity: An object identity is typically implemented via a unique ID. The value of the ID is not visible to the external user. However, it is used internally by the JVM to identify each object uniquely.
         
         -> For Example, Pen is an object. Its name is Reynolds; color is white, known as its state. It is used to write, so writing is its behavior.
     
     -> An Object can be defined as an instance of a class. A class is a template or blueprint from which objects are created. So, an object is the instance(result) of a class.
     
     #Types of inheritance in java
     -----------------------------
     -> there can be three types of inheritance in java: single, multilevel and hierarchical.
     
     -> multiple and hybrid inheritance is supported through interface only. 
     
    ---------------------------
    
    --------------------------------------------------------------------------------------------------------------------
    # Apart from these concepts, there are some other terms which are used in Object-Oriented design:
    -------------------------------------------------------------------------------------------------------------------
    
    #Coupling
    -----------------------------------------------------------------------------------------------
    -> Coupling refers to the knowledge or information or dependency of another class. It arises when classes are aware of each other. If a class has the details information of another class, there is strong coupling. In Java, we use private, protected, and public modifiers to display the visibility level of a class, method, and field. You can use interfaces for the weaker coupling because there is no concrete implementation.
    -----------------------------------------------------------------------------------------------
    #Cohesion
    -----------------------------------------------------------------------------------------------
     -> Cohesion refers to the level of a component which performs a single well-defined task. A single well-defined task is done by a highly cohesive method. The weakly cohesive method will split the task into separate parts. The java.io package is a highly cohesive package because it has I/O related classes and interface. However, the java.util package is a weakly cohesive package because it has unrelated classes and interfaces.
     
    -----------------------------------------------------------------------------------------------
    #Association
    -----------------------------------------------------------------------------------------------
     -> Association represents the relationship between the objects. Here, one object can be associated with one object or many objects. There can be four types of association between the objects:
        
        1. One to One
        2. One to Many
        3. Many to One, and
        4. Many to Many
        
      -> Let's understand the relationship with real-time examples. For example, One country can have one prime minister (one to one), and a prime minister can have many ministers (one to many). Also, many MP's can have one prime minister (many to one), and many ministers can have many departments (many to many).
        
      -> Association can be undirectional or bidirectional.
     
    ----------------------------------------------------------------------------------------------- 
    #Aggregation(HAS-A Relationship)
    -----------------------------------------------------------------------------------------------
    -> Aggregation is a way to achieve Association. Aggregation represents the relationship where one object contains other objects as a part of its state. It represents the weak relationship between objects. It is also termed as a has-a relationship in Java. Like, inheritance represents the is-a relationship. It is another way to reuse objects.
    
    -> If a class have an entity reference, it is known as Aggregation. Aggregation represents HAS-A relationship.
    
      #Why use Aggregation?
      ---------------------
      -> For Code Reusability.
      
      # When use Aggregation?
      -----------------------
      -> Code reuse is also best achieved by aggregation when there is no is-a relationship.
      -> Inheritance should be used only if the relationship is-a is maintained throughout the lifetime of the objects involved; 
        otherwise, aggregation is the best choice.
    
    -----------------------------------------------------------------------------------------------
    #Composition
    -----------------------------------------------------------------------------------------------
    ->The composition is also a way to achieve Association. The composition represents the relationship where one object contains other objects 
    as a part of its state. There is a strong relationship between the containing object and the dependent object.
     It is the state where containing objects do not have an independent existence. 
     If you delete the parent object, all the child objects will be deleted automatically.
    
                
                
    -----------------------------------------------------------------------------------------------
    


===================================================================================================================

#Collections::
====================================================================================================================
    Array Vs ArrayList In Java :
    ----------------------------
                    Array	                                                                                     ArrayList
    -> Arrays are static in nature. Arrays are fixed length data structures. You can’t change their size once they are created.	-> ArrayList is dynamic in nature. Its size is automatically increased if you add elements beyond its capacity.
    -> Arrays can hold both primitives as well as objects.	-> ArrayList can hold only objects.
    -> Arrays can be iterated only through for loop or for-each loop.  -> ArrayList provides iterators to iterate through their elements.
    -> The size of an array is checked using length attribute.	-> The size of an ArrayList can be checked using size() method.
    -> Array gives constant time performance for both add and get operations.	-> ArrayList also gives constant time performance for both add and get operations provided adding an element doesn’t trigger resize.
    -> Arrays don’t support generics.	-> ArrayList supports generics.
    -> Arrays are not type safe.	-> ArrayList are type safe.
    -> Arrays can be multi-dimensional.	-> ArrayList can’t be multi-dimensional.
    -> Elements are added using assignment operator. ->	Elements are added using add() method.
    -------------------------------------------------------------------------------------------------------
    
    1.Convert HashMap To ArrayList In Java.
    ---------------------------------------
    Map<Integer,String>map = new HashMap<>();
    map.put(1,"mark");
    map.put(2,"arun");
    List<String> list = new ArrayList<>(map.values());
    for(String s : list){
    Sopln(s);
    }
    ----------------------------------------------------
    2.Differences between HashMap and Hashtable?
    --------------------------------------------
    HashMap and Hashtable both are used to store data in key and value form. Both are using hashing technique to store unique keys.
    But there are many differences between HashMap and Hashtable classes that are given below.
    
    HashMap::::
    -------------
    HashMap is non synchronized. It is not-thread safe and can't be shared between many threads without proper synchronization code.
    HashMap allows one null key and multiple null values.
    HashMap is a new class introduced in JDK 1.2.
    HashMap is fast.
    We can make the HashMap as synchronized by calling this code
    Map m = Collections.synchronizedMap(HashMap);
    HashMap is traversed by Iterator.
    Iterator in HashMap is fail-fast.
    HashMap inherits AbstractMap class.
    
    Hashtable:::
    ------------
    Hashtable is synchronized. It is thread-safe and can be shared with many threads.
    Hashtable doesn't allow any null key or value.
    Hashtable is a legacy class.
    Hashtable is slow.
    Hashtable is internally synchronized and can't be unsynchronized.
    Hashtable is traversed by Enumerator and Iterator.
    Enumerator in Hashtable is not fail-fast.
    Hashtable inherits Dictionary class.
    --------------------------------------------------------------------------------------
    3.What's the difference between ConcurrentHashMap and Collections.synchronizedMap(Map)?
    --------------------------------------------------------------------------------------
    ->ConcurrentHashMap does not allow null keys or values. So they are NOT equal alternatives of a synchronized map.
    For your needs, use ConcurrentHashMap. It allows concurrent modification of the Map from several threads without the need to block them. Collections.
    synchronizedMap(map) creates a blocking Map which will degrade performance, albeit ensure consistency (if used properly).
    Use the second option if you need to ensure data consistency, and each thread needs to have an up-to-date view of the map.
    Use the first if performance is critical, and each thread only inserts data to the map, with reads happening less frequently.
    ╔═══════════════╦═══════════════════╦═══════════════════╦═════════════════════╗
    ║   Property    ║     HashMap       ║    Hashtable      ║  ConcurrentHashMap  ║
    ╠═══════════════╬═══════════════════╬═══════════════════╩═════════════════════╣
    ║      Null     ║     allowed       ║              not allowed                ║
    ║  values/keys  ║                   ║                                         ║
    ╠═══════════════╬═══════════════════╬═════════════════════════════════════════╣
    ║Is thread-safe ║       no          ║                  yes                    ║
    ╠═══════════════╬═══════════════════╬═══════════════════╦═════════════════════╣
    ║     Lock      ║       not         ║ locks the whole   ║ locks the portion   ║
    ║  mechanism    ║    applicable     ║       map         ║                     ║
    ╠═══════════════╬═══════════════════╩═══════════════════╬═════════════════════╣
    ║   Iterator    ║               fail-fast               ║ weakly consistent   ║
    ╚═══════════════╩═══════════════════════════════════════╩═════════════════════╝
    Regarding locking mechanism: Hashtable locks the object, while ConcurrentHashMap locks only the bucket.
    ---------------------------------------------------------------------------------------------------------
    ArrayList:- Java ArrayList class uses a dynamic array for storing the elements. It inherits AbstractList class and implements List interface.
    -------------
    Java ArrayList class can contain duplicate elements.
    Java ArrayList class maintains insertion order.
    Java ArrayList class is non synchronized.
    Java ArrayList allows random access because array works at the index basis.
    In Java ArrayList class, manipulation is slow because a lot of shifting needs to occur if any element is removed from the array list.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Difference between ArrayList and Vector
    ---------------------------------------------------------
    ArrayList and Vector both implements List interface and maintains insertion order.
    ArrayList
    -------------
    1) ArrayList is not synchronized.
    2) ArrayList increments 50% of current array size if the number of elements exceeds from its capacity.
    3) ArrayList is not a legacy class. It is introduced in JDK 1.2.
    4) ArrayList is fast because it is non-synchronized.
    5) ArrayList uses the Iterator interface to traverse the elements.
    ---------------------------------------------------------------------------------------------------------------
    Vector
    -----------------------------------------------------------------------------------------------------
    Vector is synchronized.
    Vector increments 100% means doubles the array size if the total number of elements exceeds than its capacity.
    Vector is a legacy class.
    Vector is slow because it is synchronized, i.e., in a multithreading environment, it holds the other threads in runnable or non-runnable state until current thread releases
    the lock of the object.
    A Vector can use the Iterator interface or Enumeration interface to traverse the elements.
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    LinkedList::- Java LinkedList class uses a doubly linked list to store the elements. It provides a linked-list data structure.
    It inherits the AbstractList class and implements List and Deque interfaces.
    
    Java LinkedList class can contain duplicate elements.
    Java LinkedList class maintains insertion order.
    Java LinkedList class is non synchronized.
    In Java LinkedList class, manipulation is fast because no shifting needs to occur.
    Java LinkedList class can be used as a list, stack or queue.
    ----------------------------------------------------------------------------------------------------------------------
    Difference between ArrayList and LinkedList
    ---------------------------------------------------------------
    ArrayList and LinkedList both implements List interface and maintains insertion order. Both are non synchronized classes.
    
    ArrayList	LinkedList
    1) ArrayList internally uses a dynamic array to store the elements.
    ->   LinkedList internally uses a doubly linked list to store the elements.
    2) Manipulation with ArrayList is slow because it internally uses an array. If any element is removed from the array, all the bits are shifted in memory.
        -> Manipulation with LinkedList is faster than ArrayList because it uses a doubly linked list, so no bit shifting is required in memory.
    3) An ArrayList class can act as a list only because it implements List only.
     - -> LinkedList class can act as a list and queue both because it implements List and Deque interfaces.
    4) ArrayList is better for storing and accessing data.
    -->	LinkedList is better for manipulating data.
    ----------------------------------------------------------------------------
    Java LinkedHashMap::-Java LinkedHashMap class is Hashtable and Linked list implementation of the Map interface, with predictable iteration order.
    It inherits HashMap class and implements the Map interface.
    
    Java LinkedHashMap contains values based on the key.
    Java LinkedHashMap contains unique elements.
    Java LinkedHashMap may have one null key and multiple null values.
    Java LinkedHashMap is non synchronized.
    Java LinkedHashMap maintains insertion order.
    The initial default capacity of Java HashMap class is 16 with a load factor of 0.75.
    --------------------------------------------------------------------------------------------------------------------
    HashMap: - Java HashMap class implements the map interface by using a hash table. It inherits AbstractMap class and implements Map interface.
    
    Java HashMap class contains values based on the key.
    Java HashMap class contains only unique keys.
    Java HashMap class may have one null key and multiple null values.
    Java HashMap class is non synchronized.
    Java HashMap class maintains no order.
    The initial default capacity of Java HashMap class is 16 with a load factor of 0.75.
    ----------------------------------------------------------------------------------------
    Difference between HashMap and Hashtable
    ------------------------------------------
    HashMap and Hashtable both are used to store data in key and value form. Both are using hashing technique to store unique keys.
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Hashtable:-Java Hashtable class implements a hashtable, which maps keys to values. It inherits Dictionary class and implements the Map interface.
    
    A Hashtable is an array of a list. Each list is known as a bucket. The position of the bucket is identified by calling the hashcode() method.
    A Hashtable contains values based on the key.
    Java Hashtable class contains unique elements.
    Java Hashtable class doesn't allow null key or value.
    Java Hashtable class is synchronized.
    The initial default capacity of Hashtable class is 11 whereas loadFactor is 0.75.
    --------------------------------------------------------------------------------------------------------------
    HashMap	Hashtable
    1) HashMap is non synchronized. It is not-thread safe and can't be shared between many threads without proper synchronization code.
     --->Hashtable is synchronized. It is thread-safe and can be shared with many threads.
    2) HashMap allows one null key and multiple null values.
    --->	Hashtable doesn't allow any null key or value.
    3) HashMap is a new class introduced in JDK 1.2.
    ---> Hashtable is a legacy class.
    4) HashMap is fast.
     --->Hashtable is slow.
    5) We can make the HashMap as synchronized by calling this code  Map m = Collections.synchronizedMap(hashMap);
      ---> Hashtable is internally synchronized and can't be unsynchronized.
    6) HashMap is traversed by Iterator.
    --->Hashtable is traversed by Enumerator and Iterator.
    7) Iterator in HashMap is fail-fast.
     ---->Enumerator in Hashtable is not fail-fast.
    8) HashMap inherits AbstractMap class.
    ---> Hashtable inherits Dictionary class.
    -----------------------------------------------------------------------------------------------------------------------------------
    Java TreeMap class hierarchy
    -----------------------------
    Java TreeMap class is a red-black tree based implementation. It provides an efficient means of storing key-value pairs in sorted order.
    
    Java TreeMap contains values based on the key. It implements the NavigableMap interface and extends AbstractMap class.
    Java TreeMap contains only unique elements.
    Java TreeMap cannot have a null key but can have multiple null values.
    Java TreeMap is non synchronized.
    Java TreeMap maintains ascending order.
    ---------------------------------------------------------------------------------
    ConcurrentHashMap ::--ConcurrentHashMap ConcurrentHashMap class is introduced in JDK 1.5, which implements ConcurrentMap as well as Serializable interface also.
    ConcureentHashMap is enhancement of HashMap as we know that while dealing with Threads in our application HashMap is not a good choice because performance wise HashMap is not upto
    the mark.
    
    * The underlined data structure for ConcurrentHashMap is Hashtable.
    * ConcurrentHashMap class is thread-safe i.e. multiple thread can operate on a single object without any complications.
    * At a time any number of threads are applicable for read operation without locking the ConcurrentHashMap object which is not there in HashMap.
    * In ConcurrentHashMap, the Object is divided into number of segments according to the concurrency level.
    * Default concurrency-level of ConcurrentHashMap is 16.
    * In ConcurrentHashMap, at a time any number of threads can perform retrieval operation but for updation in object, thread must lock the particular segment in which thread want to
         * operate.This type of locking mechanism is known as Segment locking or bucket locking.Hence at a time 16 updation operations can be performed by threads.
    * null insertion is not possible in ConcurrentHashMap as key or value.
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    WeakHashMap :: - WeakHashMap is the Hash table based implementation of the Map interface, with weak keys.
    An entry in a WeakHashMap will automatically be removed when its key is no longer in ordinary use. More precisely, the presence of a mapping for a given key will not prevent
    the key from being discarded by the garbage collector, that is, made finalizable, finalized, and then reclaimed. When a key has been discarded its entry is effectively removed
    from the map, so this class behaves somewhat differently from other Map implementations. Few important features of a weak hashmap are:
    
    1. Both null values and null keys are supported in WeakHashMap.
    2. It is not synchronised.
    3. This class is intended primarily for use with key objects whose equals methods test for object identity using the == operator.
    ------------------------------------------------------------------------
    Difference between Comparable and Comparator
    ---------------------------------------------------
    Comparable and Comparator both are interfaces and can be used to sort collection elements.
    
    Comparable	Comparator
    1) Comparable provides a single sorting sequence. In other words, we can sort the collection on the basis of a single element such as id, name, and price.
    --->The Comparator provides multiple sorting sequences. In other words, we can sort the collection on the basis of multiple elements such as id, name, and price etc.
    2) Comparable affects the original class, i.e., the actual class is modified.
    --->	Comparator doesn't affect the original class, i.e., the actual class is not modified.
    3) Comparable provides compareTo() method to sort elements.
    --->	Comparator provides compare() method to sort elements.
    4) Comparable is present in java.lang package.
    -->	A Comparator is present in the java.util package.
    5) We can sort the list elements of Comparable type by Collections.sort(List) method.
    ----> We can sort the list elements of Comparator type by Collections.sort(List, Comparator) method.
    ------------------------------------------------------------------------------------------------------------------------------------------
    HashSet::- Java HashSet class is used to create a collection that uses a hash table for storage. It inherits the AbstractSet class and implements Set interface.
    
    HashSet stores the elements by using a mechanism called hashing.
    HashSet contains unique elements only.
    HashSet allows null value.
    HashSet class is non synchronized.
    HashSet doesn't maintain the insertion order. Here, elements are inserted on the basis of their hashcode.
    HashSet is the best approach for search operations.
    The initial default capacity of HashSet is 16, and the load factor is 0.75.
    ----------------------------------------------------------------------------------------------------
    TreeSet :-- Java TreeSet class implements the Set interface that uses a tree for storage. It inherits AbstractSet class and implements the NavigableSet interface.
                The objects of the TreeSet class are stored in ascending order.
    
    Java TreeSet class contains unique elements only like HashSet.
    Java TreeSet class access and retrieval times are quiet fast.
    Java TreeSet class doesn't allow null element.
    Java TreeSet class is non synchronized.
    Java TreeSet class maintains ascending order.
    -----------------------------------------------------------------------------------------------------------------
    Difference Between Iterator And ListIterator In Java::
    ----------------------------------------------------------
    Iterator::	                                                                        
    	->The Iterator traverses the elements in the forward direction only.	
    	->The Iterator can be used in List, Set, and Queue.	
    	->The Iterator can only perform remove operation while traversing the collection.(it supports only READ and DELETE operations.)	
    	
    ListIterator::
       -> ListIterator traverses the elements in backward and forward directions both.
       -> ListIterator can be used in List only.
       -> ListIterator can perform ?add,? ?remove,? and ?set? operation while traversing the collection.(It supports all CRUD operations.)
    ---------------------------------------------------------------------------------------------------------------------------------------
    What is the difference between Iterator and Enumeration?:
    -------------------------------------------------------------------   
     Iterator::	
     	The Iterator can traverse legacy and non-legacy elements.	
     	The Iterator is fail-fast.	
     	The Iterator is slower than Enumeration.	
     	The Iterator can perform remove operation while traversing the collection.	
          
     Enumeration::
         Enumeration can traverse only legacy elements.
         Enumeration is not fail-fast.
         Enumeration is faster than Iterator.
         The Enumeration can perform only traverse operation on the collection.
    ----------------------------------------------------------------------------------------------------------------------------------------
     
    
    ------------------------------------------------------------------------------------------------------------
    Difference Between Collections Vs Streams In Java :::
    -------------------------------------------------------
    Collections	                                          Streams
    
    -> Collections are mainly used to store and group the data. ->	Streams are mainly used to perform operations on data.
    -> You can add or remove elements from collections.	        ->  You can’t add or remove elements from streams.
    -> Collections have to be iterated externally.	            ->  Streams are internally iterated.
    -> Collections can be traversed multiple times.             ->	Streams are traversable only once.
    -> Collections are eagerly constructed.                     -> 	Streams are lazily constructed.
    ->Ex : List, Set, Map…	                                    ->  Ex : filtering, mapping, matching…
============================================================================================================================

#Java Streams
=======================================================================================================================

=====================================================================================================================

#String Handling::
============================================================================================================================
    String class ::
    ---------------
    String is a sequence of characters. In java, objects of String are immutable which means a constant and cannot be changed once created.
    
    There are two ways to create string in Java:
    --------------------------------------------
    1).String literal
    ------------------
    String s = “GeeksforGeeks”;
    
    2). Using new keyword
    ----------------------
    String s = new String (“GeeksforGeeks”); // String str = new String();
    
    String Constructors::
    ---------------------
    
    1). String(byte[] byte_arr) – Construct a new String by decoding the byte array. It uses the platform’s default character set for decoding.
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    String s_byte =new String(b_arr); //Geeks
    
    2). String(byte[] byte_arr, Charset char_set) – Construct a new String by decoding the byte array. It uses the char_set for decoding.
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    Charset cs = Charset.defaultCharset();
    String s_byte_char = new String(b_arr, cs); //Geeks
    
    3). String(byte[] byte_arr, String char_set_name) – Construct a new String by decoding the byte array. It uses the char_set_name for decoding.
        It looks similar to the above constructs and they appear before similar functions but it takes the String(which contains char_set_name) as parameter while the above constructor
      takes CharSet.
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    String s = new String(b_arr, "US-ASCII"); //Geeks
    
    4). String(byte[] byte_arr, int start_index, int length) – Construct a new string from the bytes array depending on the start_index(Starting location) and
        length(number of characters from starting location).
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    String s = new String(b_arr, 1, 3); // eek
    
    5). String(byte[] byte_arr, int start_index, int length, Charset char_set) – Construct a new string from the bytes array depending on the start_index(Starting location) and
    length(number of characters from starting location).Uses char_set for decoding.
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    Charset cs = Charset.defaultCharset();
    String s = new String(b_arr, 1, 3, cs); // eek
    
    6).String(byte[] byte_arr, int start_index, int length, String char_set_name) – Construct a new string from the bytes array depending on the start_index(Starting location) and length(number of characters from starting location).Uses char_set_name for decoding.
    
    Example:
    byte[] b_arr = {71, 101, 101, 107, 115};
    String s = new String(b_arr, 1, 4, "US-ASCII"); // eeks
    
    7).String(char[] char_arr) – Allocates a new String from the given Character array
    
    Example:
    char char_arr[] = {'G', 'e', 'e', 'k', 's'};
    String s = new String(char_arr); //Geeks
    
    8).String(char[] char_array, int start_index, int count) – Allocates a String from a given character array but choose count characters from the start_index.
    
    Example:
    char char_arr[] = {'G', 'e', 'e', 'k', 's'};
    String s = new String(char_arr , 1, 3); //eek
    
    9).String(int[] uni_code_points, int offset, int count) – Allocates a String from a uni_code_array but choose count characters from the start_index.
    
    Example:
    int[] uni_code = {71, 101, 101, 107, 115};
    String s = new String(uni_code, 1, 3); //eek
    
    10).String(StringBuffer s_buffer) – Allocates a new string from the string in s_buffer
    
    Example:
    StringBuffer s_buffer = new StringBuffer("Geeks");
    String s = new String(s_buffer); //Geeks
    
    11).String(StringBuilder s_builder) – Allocates a new string from the string in s_builder
    
    Example:
    StringBuilder s_builder = new StringBuilder("Geeks");
    String s = new String(s_builder); //Geeks
    
    String Methods:::
    ----------------
    
    1) int length(): Returns the number of characters in the String.
          "GeeksforGeeks".length();  // returns 13
    
    2) Char charAt(int i): Returns the character at ith index.
          "GeeksforGeeks".charAt(3); // returns  ‘k’
    
    3) String substring (int i): Return the substring from the ith  index character to end.
          "GeeksforGeeks".substring(3); // returns “ksforGeeks”
    
    4) String substring (int i, int j): Returns the substring from i to j-1 index.
          "GeeksforGeeks".substring(2, 5); // returns “eks”
    
    5) String concat( String str): Concatenates specified string to the end of this string.
          String s1 = ”Geeks”;
          String s2 = ”forGeeks”;
          String output = s1.concat(s2); // returns “GeeksforGeeks”
    
    6) int indexOf (String s): Returns the index within the string of the first occurrence of the specified string.
          String s = ”Learn Share Learn”;
          int output = s.indexOf(“Share”); // returns 6
    
    7) int indexOf (String s, int i): Returns the index within the string of the first occurrence of the specified string, starting at the specified index.
          String s = ”Learn Share Learn”;
          int output = s.indexOf(‘a’,3);// returns 8
    
    8) Int lastIndexOf( String s): Returns the index within the string of the last occurrence of the specified string.
           String s = ”Learn Share Learn”;
           int output = s.lastIndexOf(‘a’); // returns 14
    
    9) boolean equals( Object otherObj): Compares this string to the specified object.
           Boolean out = “Geeks”.equals(“Geeks”); // returns true
           Boolean out = “Geeks”.equals(“geeks”); // returns false
    
    10) boolean  equalsIgnoreCase (String anotherString): Compares string to another string, ignoring case considerations.
           Boolean out= “Geeks”.equalsIgnoreCase(“Geeks”); // returns true
           Boolean out = “Geeks”.equalsIgnoreCase(“geeks”); // returns true
    
    11) int compareTo( String anotherString): Compares two string lexicographically.
           int out = s1.compareTo(s2);  // where s1 ans s2 are
                                 // strings to be compared
    
                                 This returns difference s1-s2. If :
                                 out < 0  // s1 comes before s2
                                 out = 0  // s1 and s2 are equal.
                                 out > 0   // s1 comes after s2.
    
    12)int compareToIgnoreCase( String anotherString): Compares two string lexicographically, ignoring case considerations.
             int out = s1.compareToIgnoreCase(s2);
             // where s1 ans s2 are
             // strings to be compared
    
             This returns difference s1-s2. If :
             out < 0  // s1 comes before s2
             out = 0   // s1 and s2 are equal.
             out > 0   // s1 comes after s2.
            Note- In this case, it will not consider case of a letter (it will ignore whether it is uppercase or lowercase).
    
    13) String toLowerCase(): Converts all the characters in the String to lower case.
            String word1 = “HeLLo”;
            String word3 = word1.toLowerCase(); // returns “hello"
    
    14) String toUpperCase(): Converts all the characters in the String to upper case.
            String word1 = “HeLLo”;
            String word2 = word1.toUpperCase(); // returns “HELLO”
    
    15) String trim(): Returns the copy of the String, by removing whitespaces at both ends. It does not affect whitespaces in the middle.
              String word1 = “ Learn Share Learn “;
              String word2 = word1.trim(); // returns “Learn Share Learn”
    
    16) String replace (char oldChar, char newChar): Returns new string by replacing all occurrences of oldChar with newChar.
          String s1 = “feeksforfeeks“;
          String s2 = “feeksforfeeks”.replace(‘f’ ,’g’); // returns “geeksgorgeeks”
    
    17) public int codePointAt(int index) – It takes as parameter a index which must be from 0 to length() – 1. ad returns a character unicode point of a index.
    
    18) public int codePointBefore(int index) – It takes as parameter a index which must be from 0 to length() – 1. and returns a unicode point of a character just before the index .
    
    19) public int codePointCount(int start_index, int end_index) – It takes as parameter start_index and end_index and returns the count of Unicode code points between the range.
    
    20) public CharSequence subSequence(int start_index, int end_index) – This method returns CharSequence which is a subsequence of the String on which this method is invoked.
    Note: It behaves similarly to subString(int start_index, int end_index), but subString() returns String while subSequence returns CharSequence.
    
    21) public boolean contains(CharSequence char_seq) – It returns true if the given CharSquence is present in the String on which its invoked.
    
    22) public boolean contentEquals(CharSequence char_seq) – It returns true only if the given CharSequence exactly matches the String on which its invoked
    
    23) public boolean endsWith(String suf) – It takes in parameter a String suffix and return true if the String has same suffix.
    
    24) public boolean startsWith(String pre) – It takes in parameter a String prefix and returns true if the String has a same prefix
    
    25) public void getChars(int start, int end, char[] destination, int destination_start) : It takes in four parameters, start and end refers to the range which is to copied to the character array, destination is the character array to be copied to, and destination_start is the starting location of the destination array.
    
    26) public char[] toCharArray() – It coverts the entire String to the character array.
    Note :- getChars provide more flexibility when, a range of characters is to be copied to an exiting array or a new array while toCharArray converts the entire string to a new character array.
    
    
    1.Difference between StringBuilder and StringBuffer?
    ----------------------------------------------------
    ->StringBuffer is synchronized, StringBuilder is not.
    ->String is an immutable.
    StringBuffer is a mutable and synchronized.
    StringBuilder is also mutable but its not synchronized.


================================================================================================

#Exception Handling:::
====================================================================================================================
    0.Exception Handling in Java
    -----------------------------
    Exception Handling in Java is one of the powerful mechanism to handle the runtime errors so that normal flow of the application can be maintained.
    ->The java.lang.Throwable class is the root class of Java Exception hierarchy which is inherited by two subclasses: Exception and Error.
    ->There are mainly two types of exceptions: checked and unchecked. Here, an error is considered as the unchecked exception.
    ------------------------------------------------------------------------------------------------------
    1.Difference between Checked and Unchecked Exceptions
    --------------------------------------------------------------------------------------------------
    Checked Exception:::
    --------------------
    -> The classes which directly inherit Throwable class except RuntimeException and Error are known as checked exceptions e.g. IOException, SQLException etc.
       Checked exceptions are checked at compile-time.
    
    Unchecked Exception:::
    ----------------------
    -> The classes which inherit RuntimeException are known as unchecked exceptions e.g. ArithmeticException, NullPointerException, ArrayIndexOutOfBoundsException etc.
     Unchecked exceptions are not checked at compile-time, but they are checked at runtime.
    
    Error:::
    --------
    -> Error is irrecoverable e.g. OutOfMemoryError, VirtualMachineError, AssertionError etc.
    ------------------------------------------------------------------------------------------------------
    2.There are 5 keywords which are used in handling exceptions in Java.
    ---------------------------------------------------------------------
    try:::
    ------   ->The "try" keyword is used to specify a block where we should place exception code. The try block must be followed by either catch or finally.
              It means, we can't use try block alone.
    
    catch:::
     ------- -> The "catch" block is used to handle the exception. It must be preceded by try block which means we can't use catch block alone. It can be followed by finally block later.
    
    finally:::
    ---------  -> The "finally" block is used to execute the important code of the program. It is executed whether an exception is handled or not.
    
    throw:::
    -------- 	-> The "throw" keyword is used to throw an exception.
    throws:::
    ---------   ->The "throws" keyword is used to declare exceptions. It doesn't throw an exception. It specifies that there may occur an exception in the method.
                 It is always used with method signature.
    ---------------------------------------------------------------------------------------------------
    
    ------------------------------------------------------------------------------------------------------
    1. Difference between throw and throws in Java.
    ------------------------------------------------
             throw	                             throws
            --------                          ---------------------
    1)Java throw keyword is used to explicitly throw an exception.-> Java throws keyword is used to declare  an exception.
    2)Checked exception cannot be propagated using throw only.-> Checked exception can be propagated with throws.
    3)Throw is followed by an instance. -> Throws is followed by class.
    4)Throw is used within the method. -> Throws is used with the method signature.
    5)You cannot throw multiple exceptions.-> You can declare multiple exceptions e.g.
                                              public void method()throws IOException,SQLException.
    -------------------------------------------------------------------------------------------------
    2.Difference between final, finally and finalize.
    ---------------------------------------------------
    final:::
    -------
    -> Final is used to apply restrictions on class, method and variable. Final class can't be inherited, final method can't be overridden and final variable value can't be changed.
    ->Final is a keyword.
    
    finally:::
    ----------
    ->Finally is used to place important code, it will be executed whether exception is handled or not.
    ->Finally is a block.
    
    finalize:::
    ----------
    ->Finalize is used to perform clean up processing just before object is garbage collected.
    ->Finalize is a method.
    -----------------------------------------------------------------------------------------------------
    3.What is Custom Exception.
    ----------------------------
    -> If you are creating your own Exception that is known as custom exception or user-defined exception.
       Java custom exceptions are used to customize the exception according to user need.
    -> By the help of custom exception, you can have your own exception and message.
    ->Ex. class InvalidAgeException extends Exception{ }
    ---------------------------------------------------------------------------------------------------
     differences between StackOverflowError Vs OutOfMemoryError in java.
     ----------------------------------------------------------------------
       StackOverflowError	                        OutOfMemoryError
    
    -> It is related to Stack memory.	             -> It is related to heap memory.
    -> It occurs when Stack is full.	             -> It occurs when heap is full.
    -> It is thrown when you call a method and there is no space left in the stack.   -> It is thrown when you create a new object and there is no space left in the heap.
    -> It occurs when you are calling a method recursively without proper terminating condition.	 -> It occurs when you are creating lots of objects in the heap memory.
    -> How to avoid? Make sure that methods are finishing their execution and leaving the stack memory. -> How to avoid?Try to remove references to objects which you don’t need anymore.

======================================================================================================================


#Multithreading:: Thread::
================================================================================================
    1.What is Multithreading?
    -------------------------
    -> Multithreading is a process of executing multiple threads simultaneously. Multithreading is used to obtain the 
       multitasking. It consumes less memory and gives the fast and efficient performance. 
       Its main advantages are:
            -> Threads share the same address space.
            -> The thread is lightweight.
            -> The cost of communication between the processes is low.
  
    -> A thread is a lightweight sub-process, the smallest unit of processing. Multiprocessing and multithreading,
       both are used to achieve multitasking.
    -> However, we use multithreading than multiprocessing because threads use a shared memory area. They don't allocate 
       separate memory area so saves memory, and context-switching between the threads takes less time than process.
    -> Java Multithreading is mostly used in games, animation, etc.
    
    2.Advantages of Java Multithreading::-
    -----------------------------------
    -> It doesn't block the user because threads are independent and you can perform multiple operations at the same time.
    -> You can perform many operations together, so it saves time.
    -> Threads are independent, so it doesn't affect other threads if an exception occurs in a single thread.
    
    3. What is Multitasking?
    ----------------------
     -> Multitasking is a process of executing multiple tasks simultaneously. We use multitasking to utilize the CPU.
        Multitasking can be achieved in two ways:
    
        -> 1.  Process-based Multitasking (Multiprocessing)
        -> 2.  Thread-based Multitasking (Multithreading)
        
       1.Process-based Multitasking (Multiprocessing)::
       -----------------------------------------------
        -> Each process has an address in memory. In other words, each process allocates a separate memory area.
        -> A process is heavyweight.
        -> Cost of communication between the process is high.
        -> Switching from one process to another requires some time for saving and loading registers, memory maps, updating lists, etc.
        -> In process based multitasking two or more processes and programs can be run concurrently.
        -> In process based multitasking a process or a program is the smallest unit.
        -> Program is a bigger unit.
        -> Process based multitasking requires more overhead.
        -> Process requires its own address space.
        -> Process to Process communication is expensive.
        -> Here, it is unable to gain access over idle time of CPU.
        -> It is comparatively heavy weight.
        -> It has slower data rate multi-tasking.
     
       2. Thread-based Multitasking (Multithreading)::
       -----------------------------------------------
        -> Threads share the same address space.
        -> A thread is lightweight.
        -> Cost of communication between the thread is low.
        -> In thread based multitasking two or more threads can be run concurrently.
        -> In thread based multitasking a thread is the smallest unit.
        -> Thread is a smaller unit.
        -> Thread based multitasking requires less overhead.
        -> Threads share same address space.
        -> Thread to Thread communication is not expensive.
        -> It allows taking gain access over idle time taken by CPU.
        -> It is comparatively light weight.
        -> It has faster data rate multi-tasking.
        
        Preemptive multitasking::-  
                                In preemptive multitasking, the operating system decides how to allocate CPU time slices to 
         each program.  At the end of a time slice, the currently active program is forced to yield control to the operating 
         system, whether it wants to or not.  Examples of operating systems that support premptive multitasking are Unix, Windows 95/98,
         Windows NT and the planned release of Mac OS X.
         
        Cooperative multitasking::-  
                                  In cooperative multitasking, each program controls how much CPU time it needs.  This means that a
           program must cooperate in yielding control to other programs, or else it will hog the CPU.  Examples of operating systems that
           support cooperative multitasking are Windows 3.1 and MacOS 8.5.
        
    4. Program vs Process vs Thread:::
    --------------------------------
      Program::
      ---------
      A program is a set of instructions and associated data that resides on the disk and is loaded by the operating system 
      to perform some task. An executable file or a python script file are examples of programs. In order to run a program,
      the operating system's kernel is first asked to create a new process, which is an environment in which a program executes.
      
      Process::
      ---------
      A process is a program in execution. A process is an execution environment that consists of instructions, user-data, and 
      system-data segments, as well as lots of other resources such as CPU, memory, address-space, disk and network I/O acquired at
      runtime. A program can have several copies of it running at the same time but a process necessarily belongs to only one program.
      
      Thread::
      --------
      Thread is the smallest unit of execution in a process. A thread simply executes instructions serially. A process can have 
      multiple threads running as part of it. Usually, there would be some state associated with the process that is shared among 
      all the threads and in turn each thread would have some state private to itself. The globally shared state amongst the 
      threads of a process is visible and accessible to all the threads, and special attention needs to be paid when any thread
      tries to read or write to this global shared state. There are several constructs offered by various programming languages 
      to guard and discipline the access to this global state, which we will go into further detail in upcoming lessons.
      
     5. What is Thread in java ::
     ----------------------------
      -> A thread is a lightweight subprocess, the smallest unit of processing. It is a separate path of execution.
      -> Threads are independent. If there occurs exception in one thread, it doesn't affect other threads. 
         It uses a shared memory area.
      ->A thread is a lightweight subprocess. It is a separate path of execution because each thread runs in a different stack 
      frame. A process may contain multiple threads. Threads share the process resources, but still, they execute independently.
      
    6. Differentiate between process and thread?
    ---------------------------------------------- 
      -> A Program in the execution is called the process whereas; A thread is a subset of the process
      -> Processes are independent whereas threads are the subset of process.
      -> Process have different address space in memory, while threads contain a shared address space.
      -> Context switching is faster between the threads as compared to processes.
      -> Inter-process communication is slower and expensive than inter-thread communication.
      -> Any change in Parent process doesn't affect the child process whereas changes in parent thread can affect the child thread.

    What do you understand by inter-thread communication?
    -------------------------------------------------------
    -> The process of communication between synchronized threads is termed as inter-thread communication.
    -> Inter-thread communication is used to avoid thread polling in Java.
    -> The thread is paused running in its critical section, and another thread is allowed to enter (or lock) in the same 
       critical section to be executed.
    -> It can be obtained by wait(), notify(), and notifyAll() methods.
    
    What is the purpose of wait() method in Java?
    -------------------------------------------------
    -> The wait() method is provided by the Object class in Java. This method is used for inter-thread communication in Java. 
       The java.lang.Object.wait() is used to pause the current thread, and wait until another thread does not call the notify()
       or notifyAll() method. Its syntax is given below.
       
    Why must wait() method be called from the synchronized block?
    -------------------------------------------------------------
    -> We must call the wait method otherwise it will throw java.lang.IllegalMonitorStateException exception.
       Moreover, we need wait() method for inter-thread communication with notify() and notifyAll(). Therefore It must be present
       in the synchronized block for the proper and correct communication.
    
    ->  public final void wait()
    
    What are the advantages of multithreading?
    ------------------------------------------
    -> Multithreading allows an application/program to be always reactive for input, even already running with some background tasks
    -> Multithreading allows the faster execution of tasks, as threads execute independently.
    -> Multithreading provides better utilization of cache memory as threads share the common memory resources.
    -> Multithreading reduces the number of the required server as one server can execute multiple threads at a time.
    
    What are the states in the lifecycle of a Thread?
    -------------------------------------------------
    A thread can have one of the following states during its lifetime:
    
     1. New: 
             -> In this state, a Thread class object is created using a new operator, but the thread is not alive.
                Thread doesn't start until we call the start() method.
     2. Runnable: 
              ->  In this state, the thread is ready to run after calling the start() method. However, the thread is not yet 
                   selected by the thread scheduler.
     3. Running: 
              -> In this state, the thread scheduler picks the thread from the ready state, and the thread is running.
     4. Waiting/Blocked: 
              -> In this state, a thread is not running but still alive, or it is waiting for the other thread to finish.
     5. Dead/Terminated: 
              -> A thread is in terminated or dead state when the run() method exits.

    1.What does 'synchronized' mean?
    --------------------------------
    -> The synchronized keyword is all about different threads reading and writing to the same variables, objects and resources.
    This is not a trivial topic in Java, but here is a quote from Sun:synchronized methods enable a simple strategy for preventing
    thread interference and memory consistency errors: if an object is visible to more than one thread, all reads or writes to that object's variables are done through synchronized methods.
    ->synchronized means that in a multi threaded environment, an object having  synchronized method(s)/block(s) does not let two threads to access the synchronized method(s)/block(s) of code at the same time.
     This means that one thread can't read while another thread updates it.
    -----------------------------------------------------------------------------------------------------
    2.Differences between Start And Run Methods In Java Threads?
    -------------------------------------------------------------
     start()                            	                       run()
    New thread is created.	                                 No new thread is created.
    Newly created thread executes task kept in run() method. Calling thread itself executes task kept in run() method.
    It is a member of java.lang.Thread class.	             It is a member of java.lang.Runnable interface.
    You can’t call start() method more than once.	         You can call run() method multiple times.
    Use of multi-threaded programming concept.	             No use of multi-threaded programming concept.
    -----------------------------------------
    3. Difference Between BLOCKED Vs WAITING States In Java?
    ----------------------------------------------------------
    WAITING
    --------
    The thread will be in this state when it calls wait() or join() method. The thread will remain in WAITING state until any other thread calls notify() or notifyAll().
    The WAITING thread is waiting for notification from other threads.
    The WAITING thread can be interrupted.
    
    BLOCKED
    ---------
    The thread will be in this state when it is notified by other thread but has not got the object lock yet.
    The BLOCKED thread is waiting for other thread to release the lock.
    The BLOCKED thread can’t be interrupted.


====================================================================================================================================

# Java Concurrency
====================================================================================================================================
    1. Difference between Concurrency and Parallelism::
    
    ----------------------------------------------------
    Concurrency::
    -------------
        -> Multiple tasks makes progress at the same time.
        -> A condition that exists when at least two threads are making progress. A more generalized form of parallelism that
           can include time-slicing as a form of virtual parallelism.
        -> Concurrency means that an application is making progress on more than one task at the same time (concurrently). 
          Well, if the computer only has one CPU the application may not make progress on more than one task at exactly the same 
          time, but more than one task is being processed at a time inside the application. It does not completely finish one task
          before it begins the next. Instead, the CPU switches between the different tasks until the tasks are complete.
        -> concurrency is related to how an application handles multiple tasks it works on. An application may process one task
           at at time (sequentially) or work on multiple tasks at the same time (concurrently).
          
          ![alt text](http://tutorials.jenkov.com/images/java-concurrency/concurrency-vs-parallelism-1.png)
          


    Parallelism::
    ------------- 
      -> Each task is broken into subtasks which can be processed in parallel.
      -> A condition that arises when at least two threads are executing simultaneously.
      -> Parallelism means that an application splits its tasks up into smaller subtasks which can be processed in parallel,
         for instance on multiple CPUs at the exact same time.
      -> Parallelism on the other hand, is related to how an application handles each individual task. An application may process
         the task serially from start to end, or split the task up into subtasks which can be completed in parallel.
         
    
   	CONCURRENCY	                                                  PARALLELISM
    1.	Concurrency is the task of running and managing the multiple computations at the same time. 
       -> 	While parallelism is the task of running multiple computations simultaneously.
    2.	Concurrency is achieved through the interleaving operation of processes on the central processing unit(CPU) or in other words by the context switching.
       ->	While it is achieved by through multiple central processing units(CPUs).
    3.	Concurrency can be done by using a single processing unit. 
       -> 	While this can’t be done by using a single processing unit. it needs multiple processing units.
    4.	Concurrency increases the amount of work finished at a time. 
       -> 	While it improves the throughput and computational speed of the system.
    5.	Concurrency deals lot of things simultaneously. 
       -> 	While it do lot of things simultaneously.
    6.	Concurrency is the non-deterministic control flow approach.	
       ->   While it is deterministic control flow approach.
    7.	In concurrency debugging is very hard. 
       -> 	While in this debugging is also hard but simple than concurrency.
       
       
       1. The Basics
       Introduction
       Preview
       Program vs Process vs Thread
       Preview
       Concurrency vs Parallelism
       Cooperative Multitasking vs Preemptive Multitasking
       Synchronous vs Asynchronous
       I/O Bound vs CPU Bound
       Throughput vs Latency
       Critical Sections & Race Conditions
       Deadlocks, Liveness & Reentrant Locks
       Mutex vs Semaphore
       Mutex vs Monitor
       Java's Monitor & Hoare vs Mesa Monitors
       Semaphore vs Monitor
       Amdahl's Law
       Moore's Law
       
       2. Multithreading in Java
       Thready Safety & Synchronized
       Wait & Notify
       Interrupting Threads
       Volatile
       Reentrant Locks & Condition Variables
       Missed Signals
       Semaphore in Java
       Spurious Wakeups
       Miscellaneous Topics
       
       3. Java Memory Model
       
       Java Memory Model
       Reordering Effects
       The happens-before Relationship
       4. Interview Practice Problems
       Blocking Queue | Bounded Buffer | Consumer Producer
       ... continued
       ... continued
       Rate Limiting Using Token Bucket Filter
       ... continued
       Thread Safe Deferred Callback
       Implementing Semaphore
       ReadWrite Lock
       Unisex Bathroom Problem
       Implementing a Barrier
       Uber Ride Problem
       Dining Philosophers
       Barber Shop
       Superman Problem
       ... continued
       Multithreaded Merge Sort
       Asynchronous to Synchronous Problem
       Epilogue
       Preview
       
       5. Bonus Questions
       Ordered Printing
       Printing Foo Bar n times
       Printing Number Series (Zero, Even, Odd)
       Build a Molecule
       Fizz Buzz Problem
       
       6. Beyond the Interview
       Next Steps
       
       7. Java Thread Basics
       Setting-up Threads
       Basic Thread Handling
       Executor Framework
       Executor Implementations
       Thread Pools
       Types of Thread Pools
       An Example: Timer vs ScheduledThreadPool
       Callable Interface
       Future Interface
       CompletionService Interface
       ThreadLocal
       CountDownLatch
       CyclicBarrier
       Concurrent Collections
    
    =========================================================================================================================

#Java InputOutput(IO)
=====================================================================================================

#Java Arrays::
=======================================================================================================================

#Java Regular Expressions::
=======================================================================================================================

#Java Reflection
===============================================================

#Java11::
=======================================================================================================================

#Java 11 features are:
--------------------
1.Running Java File with single command
2.New utility methods in String class
3.Local-Variable Syntax for Lambda Parameters
4.Nested Based Access Control
5.JEP 321: HTTP Client
6.Reading/Writing Strings to and from the Files
7.JEP 328: Flight Recorder

1.Running Java File with single command
---------------------------------------
One major change is that you don’t need to compile the java source file with javac tool first. You can directly run the file with java command and it implicitly compiles. This feature comes under JEP 330. Following is a sneak peek at the new methods of Java String class introduced in Java 11:

--------------------------

2.Java 11 New String Methods
---------------------------
There are 6 new string methods are introduced in Java 11. They are – isBlank(), lines(), repeat(), strip(), stripLeading() and stripTrailing().

isBlank() :
You can use this method to check whether given string is blank or not. A string is said to be blank if it is empty or contains only white spaces.

lines() :
This method returns a stream of lines extracted from the given string. A line can be defined as a sequence of zero or more characters followed by a line terminator.

repeat() :

This method returns the calling string repeated n times.

strip() :

You can use this method to remove all leading and trailing white spaces of the given string.

stripLeading() :

This method removes only leading white spaces of a string.

stripTrailing() :

This method removes only trailing white spaces of a string.

---------------------

isBlank() - This instance method returns a boolean value. Empty Strings and Strings with only white spaces are treated as blank.

import java.util.*;

public class Main {
public static void main(String[] args) throws Exception {
// Your code here!

        System.out.println(" ".isBlank()); //true
        
        String s = "Anupam";
        System.out.println(s.isBlank()); //false
        String s1 = "";
        System.out.println(s1.isBlank()); //true
    }
}

lines() This method returns a stream of strings, which is a collection of all substrings split by lines.

import java.util.stream.Collectors;

public class Main {
public static void main(String[] args) throws Exception {

        String str = "JD\nJD\nJD"; 
        System.out.println(str);
        System.out.println(str.lines().collect(Collectors.toList()));
    }
}
The output of the above code is:java 11 string lines methodstrip(), stripLeading(), stripTrailing() strip() - Removes the white space from both, beginning and the end of string.

But we already have trim(). Then what’s the need of strip()? strip() is “Unicode-aware” evolution of trim(). When trim() was introduced, Unicode wasn’t evolved. Now, the new strip() removes all kinds of whitespaces leading and trailing(check the method Character.isWhitespace(c) to know if a unicode is whitespace or not)

An example using the above three methods is given below:

public class Main {
public static void main(String[] args) throws Exception {
// Your code here!

        String str = " JD "; 
        System.out.print("Start");
        System.out.print(str.strip());
        System.out.println("End");
        
        System.out.print("Start");
        System.out.print(str.stripLeading());
        System.out.println("End");
        
        System.out.print("Start");
        System.out.print(str.stripTrailing());
        System.out.println("End");
    }
}
The output in the console from the above code is:java 11 string strip methodrepeat(int) The repeat method simply repeats the string that many numbers of times as mentioned in the method in the form of an int.

public class Main {
public static void main(String[] args) throws Exception {
// Your code here!

        String str = "=".repeat(2);
        System.out.println(str); //prints ==
    }
}

3.Local-Variable Syntax for Lambda Parameters
----------------------------------------------
 Local-Variable Syntax for Lambda Parameters is the only language feature release in Java 11. In Java 10, Local Variable Type Inference was introduced. Thus we could infer the type of the variable from the RHS - var list = new ArrayList<String>(); JEP 323 allows var to be used to declare the formal parameters of an implicitly typed lambda expression. We can now define :

(var s1, var s2) -> s1 + s2
This was possible in Java 8 too but got removed in Java 10. Now it’s back in Java 11 to keep things uniform. But why is this needed when we can just skip the type in the lambda? If you need to apply an annotation just as @Nullable, you cannot do that without defining the type. Limitation of this feature - You must specify the type var on all parameters or none. Things like the following are not possible:

(var s1, s2) -> s1 + s2 //no skipping allowed
(var s1, String y) -> s1 + y //no mixing allowed

var s1 -> s1 //not allowed. Need parentheses if you use var in lambda.

4.Nested Based Access Control
-----------------------------
Before Java 11 this was possible:

public class Main {

    public void myPublic() {
    }
 
    private void myPrivate() {
    }
 
    class Nested {
 
        public void nestedPublic() {
            myPrivate();
        }
    }
}
private method of the main class is accessible from the above-nested class in the above manner. But if we use Java Reflection, it will give an IllegalStateException.

Method method = ob.getClass().getDeclaredMethod("myPrivate");
method.invoke(ob);
Java 11 nested access control addresses this concern in reflection. java.lang.Class introduces three methods in the reflection API: getNestHost(), getNestMembers(), and isNestmateOf().


5.Dynamic Class-File Constants
-------------------------------
The Java class-file format now extends support a new constant pool form, CONSTANT_Dynamic. The goal of this JEP is to reduce the cost and disruption of developing new forms of materializable class-file constraints, by creating a single new constant-pool form that can be parameterized with user-provided behavior. This enhances performance
                                                                                                                
6.Epsilon: A No-Op Garbage Collector
---------------------------------------------
Unlike the JVM GC which is responsible for allocating memory and releasing it, Epsilon only allocates memory. It allocates memory for the following things:

Performance testing.
Memory pressure testing.
VM interface testing.
Extremely short lived jobs.
Last-drop latency improvements.
Last-drop throughput improvements.
Now Elipson is good only for test environments. It will lead to OutOfMemoryError in production and crash the applications. The benefit of Elipson is no memory clearance overhead. Hence it’ll give an accurate test result of performance and we can no longer GC for stopping it. Note: This is an experimental feature.

7.Remove the Java EE and CORBA Modules
---------------------------------------
The modules were already deprecated in Java 9. They are now completely removed. Following packages are removed: java.xml.ws, java.xml.bind, java.activation, java.xml.ws.annotation, java.corba, java.transaction, java.se.ee, jdk.xml.ws, jdk.xml.bind
                                   
8.Flight Recorder
------------------
Flight Recorder which earlier used to be a commercial add-on in Oracle JDK is now open-sourced since Oracle JDK is itself not free anymore. JFR is a profiling tool used to gather diagnostics and profiling data from a running Java application. Its performance overhead is negligible and that’s usually below 1%. Hence it can be used in production applications.
                   
9.HTTP Client
--------------
Java 11 standardizes the Http CLient API. The new API supports both HTTP/1.1 and HTTP/2. It is designed to improve the overall performance of sending requests by a client and receiving responses from the server. It also natively supports WebSockets.

10.Reading/Writing Strings to and from the Files
-------------------------------------------------
Java 11 strives to make reading and writing of String convenient. It has introduced the following methods for reading and writing to/from the files:

readString()
writeString()
Following code showcases an example of this

Path path = Files.writeString(Files.createTempFile("test", ".txt"), "This was posted on JD");
System.out.println(path);
String s = Files.readString(path);
System.out.println(s); //This was posted on JD

11.ChaCha20 and Poly1305 Cryptographic Algorithms
-------------------------------------------------
Java 11 provides ChaCha20 and ChaCha20-Poly1305 cipher implementations. These algorithms will be implemented in the SunJCE provider.

12.Improve Aarch64 Intrinsics
------------------------------
Improve the existing string and array intrinsics, and implement new intrinsics for the java.lang.Math sin, cos, and log functions, on AArch64 processors.

13.A Scalable Low-Latency Garbage Collector (Experimental)
----------------------------------------------------------
Java 11 has introduced a low latency GC. This is an experimental feature. It’s good to see that Oracle is giving importance to GC’s.
                          
14.Deprecate the Nashorn JavaScript Engine
-------------------------------------------
Nashorn JavaScript script engine and APIs are deprecated thereby indicating that they will be removed in the subsequent releases.


#Java10::
=======================================================================================================================

#Java9::
=======================================================================================================================

#Java7::
=======================================================================================================================

#Java12::
=======================================================================================================================

#Java13::
=======================================================================================================================

#Java14::
=======================================================================================================================

What Is a Functional Interface? What Are the Rules of Defining a Functional Interface?

Explain the major project you worked and any special contribution made in it

Tell us briefly about the solution design of the project?

Which was your most challenging project?

How ConcurrentHashMap works internally in Java

Define the advantages and disadvantages of the heap compared to a stack?

Write a program to print first non-repeating character in the given string using java8

What is the difference between Authentication and Authorization. How you will implement in Spring Boot application.

What are the challenges you faced while migrating the application from Monolithic to Microservices

Why Functional interface is introduced in java8 if abstract class is already available for alternative solution

Write a Java Program to open all links of gmail.com.

When you will going to use comparable and comparator? explain with real time scenario, have you used in your project

What is the difference between CrudRepository and JPARepository in Spring Boot.

How to handle exception in Spring Boot.

Circuit breaker pattern in Microservices

Do you know how the memory is affected by signed and unsigned numbers?

Given a LinkedList(LL) Node, How would you determine, Whether the given Node is, Single LL, Double LL or Circular LL?

Explain whether a linked list is considered as a linear or non-linear data structure?

Explain how encryption algorithm works?

How @SpringBootApplication annotation works internally, explain the architecture flow

How you will manage response time between inter communication microservices

How you will integrate your spring boot application with Cl/CD pipeline of Jenkins

What is the difference between Authentication and Authorization. How you will implement in Spring Boot application.

Which sorting algorithm is considered the fastest? Why?

How you will manage million of request at a time in microservices

Why and when should I use Stack or Queue data structures instead of Arrays/Lists?

How you would aggregate logs in distributed micro service architecture?

how to override the default configuration in springboot

What is MetaSpace in Java8? How does it differ from PermGen Space

What Is the Difference Between Intermediate and Terminal Operations?

What is double checked locking in Singleton?

How APIs are authenticated in Spring framework?

What’s the difference between @Component, @Controller, @Repository & @Service annotations in Spring?

How to handle exceptions in Spring MVC Framework?

How you would aggregate logs in distributed micro service architecture?



Why do you need the Eureka Server? (Microservices architecture)

Tree Data Structure: Depth First v/s Breadth First process 


Write a program to print first non-repeating character in the given string using java8













Find the string that appears most in a list of strings.



How to find duplicate numbers in an array if it contains multiple duplicates?



Write a program to increase the salary by 10% and return employees name whose salary is less than 20000 using stream api



















Write a program to print first non-repeating character in the given string using java8



Explain the difference between intermediate and terminal operations with examples. Ask code from the description.





Write a program to print first non-repeating character in the given string using java8







How do you insert a new item in a binary search tree? 

 

 