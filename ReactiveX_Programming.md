Base classes::- RxJava3.0
----------------
RxJava 3 features several base classes you can discover operators on:
-----------------------------------------------------------------------
Flowable: 0..N flows, supporting Reactive-Streams and backpressure

Observable: 0..N flows, no backpressure,

Single: a flow of exactly 1 item or an error,

Completable: a flow without items but only a completion or error signal,

Maybe: a flow with no items, exactly one item or an error.

API Components:
---------------
The API consists of the following components that are required to be provided by Reactive Stream implementations:

->Publisher

->Subscriber

->Subscription

->Processor


Observable
Operators
Single
Subject
Scheduler