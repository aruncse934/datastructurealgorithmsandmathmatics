#Domain Driven Design Architecture ::
-------------------------------------
Hi, 
 I'm Arun Kumar Gupta. and I'm  Java  Backend Developer.
Regarding My Work Profile I am having total of 3.7 years Experience in Software Development.
I have been Working at Cognizant as Programmer Analyst.
My Skills::-
---------------
Algorithms & Data Structure | Discrete Mathematics | Mathematics | Design Patterns | Object Oriented Design | System Design | Microservices | 

TECHNICAL SKILLS
Languages:​ Java / J2EE
Frameworks:​ Spring Boot, Spring, Hibernate.
Database:​ MySQL, Oracle, MongoDB
Operating Systems:​ Linux, Windows
Distributed Technologies:​ Restful, Soap.
Methodologies:​ Waterfall, Agile
Web Servers: ​ Apache Tomcat, WebLogic
Tools:​ Maven/Gradle, Git, Putty, Postman, Junit, Docker, Log4J, GitLab, ELK.
Cloud Platform:​ AWS.
IDEs​ : IntelliJ IDEA, Eclipse, STS.

I have Good Knowledge and Experience in NLP/ML | AI | Python |

My Responsibilities in Project::----------------------------------------------------------------------------------------------
● Worked with the Product owner and team lead to analyse business requirements, elaborate   user stories, identify and define 
the technical features required for implementation.
● Working on Java & API code development,Junit and API Testing for various Features.
● Involved in code enhancement, Bugs fixing.

My hobbies are cooking, Problem Solving, playing chess, etc. 
My strength is problem-solving. 
That's all about MySelf 