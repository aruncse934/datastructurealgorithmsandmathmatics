
Facebook Engineering: https://engineering.fb.com/
Uber Engineering: https://eng.uber.com/
Netflix Tech Blog: https://netflixtechblog.com/

    System Design Interviews - A step by step guide
System Design Master Template
Designing a URL Shortening service like TinyURL
Designing Pastebin
Designing Instagram
Designing Dropbox
Designing Facebook Messenger
Designing Twitter
Designing Youtube or Netflix
Designing Typeahead Suggestion
Designing an API Rate Limiter
Designing Twitter Search
Designing a Web Crawler
Designing Facebook’s Newsfeed
Designing Yelp or Nearby Friends
Designing Uber backend
Designing Ticketmaster
Additional Resources
System Design Basics
Key Characteristics of Distributed Systems
Load Balancing
Caching
Data Partitioning
Indexes
Proxies
Redundancy and Replication
SQL vs. NoSQL
CAP Theorem
PACELC Theorem (New)
Consistent Hashing (New)
Long-Polling vs WebSockets vs Server-Sent Events
Bloom Filters (New)
Quorum (New)
Leader and Follower (New)
Heartbeat (New)
Checksum (New)

10 strategies for achieving low latency in a distributed system.

1. System Design Problems ::
--------------------------
System Design Interviews: A step by step guide
Designing a URL Shortening service like TinyURL
Designing Pastebin
Designing Instagram
Designing Dropbox
Designing Facebook Messenger
Designing Twitter
Designing Youtube or Netflix
Designing Typeahead Suggestion
Designing an API Rate Limiter
Designing Twitter Search
Designing a Web Crawler
Designing Facebook’s Newsfeed
Designing Yelp or Nearby Friends
Designing Uber backend
Design Ticketmaster (*New*)
Additional Resources

2. Glossary of System Design Basics
-----------------------------------
System Design Basics
Key Characteristics of Distributed Systems
Load Balancing
Caching
Data Partitioning
Indexes
Proxies
Redundancy and Replication
SQL vs. NoSQL
CAP Theorem
Consistent Hashing
Long-Polling vs WebSockets vs Server-Sent Events


--------------------------------------------------------------------------------------------------
#. 6 Systems Design Interview Questions
---------------------------------------
0.Design 
1. Design A code- Deployment System
2. Design A Stockbroker
3. Design Facebook News Feed
4. Google Drive
5. Design The Reddit API

#1. What do you mean by System Design?
The process of designing a system's properties, such as its modules, architecture, components and interfaces, and data, based on specified requirements is known as system design. It is the process of identifying, developing, and designing systems to meet the specific aims and expectations of a corporation or organisation. Systems design is less about coding and more about system analysis, architectural patterns, APIs, design patterns, and glueing it all together. Designing your system appropriately for the requirements of your application can minimise unnecessary costs and maintenance efforts, as well as providing a better experience for your end-users, because your application will be able to handle the architectural load.

#2.  What do you mean by a system's latency, throughput, and availability?
Performance is an important consideration in system design since it contributes to the speed and reliability of our services. The three most important performance indicators are as follows:
    
Latency: The time it takes for data to get from one location on a network to another is known as latency. Assume New York Server A sends a data packet to Dublin Server B. At 04:38:00.000 GMT, Server A sends the packet, and Server B gets it at 04:38:00.145 GMT. The difference between these two periods is the amount of lag on this path: 0.145 seconds or 145 milliseconds.
Throughput: The performance of tasks by a computing service or device over a given time period is referred to as throughput. It is a metric that compares the amount of work accomplished to the length of time it takes to complete it, and it can be used to assess the performance of a processor, memory, and/or network communications.
availability: The amount of time a system is available to reply to queries is determined by this. System Uptime / (System Uptime+Downtime) is how it's computed.

#3. What do you mean by sharding?
Sharding is a database architectural style that is linked to horizontal partitioning, which is the process of dividing a table's rows into numerous partitions. Each partition has the same schema and columns as the others, but unique rows. Similarly, the data in each partition is distinct and unrelated to the data in other partitions.

#4. What is Scalability?
Scalability is sometimes used interchangeably with growth. Scalability, on the other hand, is defined as the ability to fulfil demand. From a business aspect, scalability refers to the ability to serve clients with ease, even when demand fluctuates dramatically.

#5. What do you mean by types of Scaling?
A system can be scaled in one of two ways:
1. Vertical Scaling
2. Horizontal Scaling

Horizontal scaling (also known as scaling out) is the process of adding more nodes or machines to your infrastructure to meet increased demand. If you're hosting an application on a server that no longer has the capacity or capability to handle traffic, adding another server can be the answer.

It's similar to delegating tasks to multiple staff rather than just one. However, the extra intricacy of your operation may be a disadvantage. You'll need to figure out which machine does what and how your new machines will interact with your old ones.

Vertical scaling (also known as scaling up) is the process of adding more resources to a system in order to fulfil demand. What distinguishes this from horizontal scaling?
Horizontal scaling refers to adding new nodes, whereas vertical scaling refers to increasing the power of your current machines. Vertical scaling, for example, would imply upgrading the CPUs if your server required additional processing power. You may also scale memory, storage, and network speed vertically.


#6. What are the advantages and disadvantages of horizontal scaling?
From a hardware standpoint, scaling is simpler: all you have to do is add more machines to your current pool to scale horizontally. It removes the requirement to figure out which system specifications need to be upgraded.
Increased fault tolerance and resilience - Relying on a single node for all of your data and processes puts you at danger of losing everything if it goes down. You can avoid losing it entirely by distributing it over multiple nodes.

Increased performance - When you use horizontal scaling to manage your network traffic, you may connect more endpoints because the load is distributed over numerous workstations.

Disadvantages of horizontal scaling:

Increased maintenance and operation complexity – Maintaining several servers is more difficult than maintaining a single server. You'll also need load balancing software and maybe virtualization software. It's also possible that backing up your equipment will become more difficult. You'll need to make sure the nodes are synchronised and communicating properly.

Initial costs are higher - Adding new servers is far more expensive than upgrading existing ones.

#7. What are the advantages and disadvantages of vertical scaling?
Upgrading a pre-existing server is less expensive than purchasing a new one. When scaling vertically, you're also less likely to add new backup and virtualization software. It's also possible that maintenance prices will stay the same.

Less complicated process communication – When a single node manages all of your services' layers, it won't need to synchronise or connect with other machines to function. This could lead to quicker responses.

Disadvantages of vertical scaling

Higher risk of downtime - Unless you have a backup server that can manage operations and requests, upgrading your machine will require significant downtime.

Single point of failure - If all of your activities are run on a single server, you run the risk of losing all of your data if there is a hardware or software failure.

#8. What do you mean by UML?
UML stands for Unified Modeling Language, and it's used to model a software system's Object-Oriented Analysis. UML is a method of visualising and documenting a software system through a set of diagrams that aid engineers, businesspeople, and system architects in comprehending the behaviour and structure of the system under development.

#9. What do you mean by a tier?
A tier can be thought of as a logical and physical separation of components of an application or service. This is a component-level separation, not a code-level split.

Types of tier:
One tier, Two tier, Three tier, N-tier

#10. What do you mean Web Architecture?
The process of planning, developing, and deploying an internet-based computer software is known as web architecture. These programmes are frequently websites that provide users with important information, and web developers may create these programmes for a specific purpose, company, or brand. Web architecture encompasses all aspects of an application and aids web designers in creating designs that improve the user's experience.

#11. What is Client-Server Architecture
Client server architecture is a computing model in which the server hosts, provides, and controls the majority of the client's resources and services. Because all requests and services are given across a network, it is also known as the networking computing model or client server network.
Client-Server Architecture examples are Mail servers, File Servers, Web Servers

#12. What is Server-side rendering?
An application's capacity to transform HTML files on the server into a fully rendered HTML page for the client is known as server-side rendering (SSR). The server receives a request for information from the web browser and immediately responds by delivering the client a completely rendered page.

#13. What do you mean by load balancing?
To spread traffic among various servers in a server farm, load balancing is a fundamental networking technique. Load balancers enhance the responsiveness and availability of applications while preventing server overload. Each load balancer is positioned in-between client devices and the backend servers. It receives incoming requests and then distributes them to any server that is accessible and able to process them.


#14. How do Load balancer work?
A device or programme known as a load balancer handles load balancing. A load balancer may be based on software or hardware. Software-based load balancers can run on a server, in the cloud, or on a virtual machine; hardware load balancers require the installation of a separate load balancing device. Load balancing functions are frequently seen in content delivery networks (CDN).

The load balancer distributes each request made by a user to a certain server when it comes in, and this procedure is repeated for each request. Based on a variety of different methods, load balancers choose the server that will handle each request. These algorithms can be divided into static and dynamic groups.

#15. What is a CDN?
A geographically dispersed group of servers known as a content delivery network (CDN) collaborates to deliver Internet material quickly. A CDN enables the rapid transfer of resources such as HTML pages, javascript files, stylesheets, pictures, and videos that are required for Internet content to load. Today, the bulk of web traffic, including traffic from well-known websites like Facebook, Netflix, and Amazon, is served through CDNs thanks to the services' rising popularity.

#16. What is a DNS?
For computers, services, or any resource connected to the Internet or a private network, Domain Name System (DNS) is a hierarchical naming system based on a distributed database. The ability to discover and connect devices everywhere in the globe is made possible, most importantly, by the translation of human readable domain names into the numerical identifiers associated with networking hardware. Similar to a network "phone book," DNS enables a browser to translate a domain name, such as "facebook.com," into the server's real IP address, which houses the data the browser is looking for.

#17.  What is difference between sharding and partitioning?
Both sharding and partitioning involve dividing a huge data source into more manageable portions. Sharding means that the data is distributed over several computers, whereas partitioning does not. Within a single database instance, data subsets are grouped through the process of partitioning. When "horizontal" and "vertical" are added before the terms "sharding" and "partitioning," they are frequently used interchangeably. As a result, the terms "horizontal sharding" and "horizontal partitioning" can be used interchangeably.

#18. What do you mean by monolithic architecture?
A unified model of software application development is monolithic architecture. It comprises three components:
Client side
Server side application
Data interface
A single database is used by all three components. This paradigm allows software to be operated with just one base of code. As a result, stakeholders always use the same set of code when they want to update or modify something. The performance on the user side may be impacted by this.

#19.  How do scalability and performance relate to one another?
If an improvement in performance is proportional to the addition of resources, a system is said to be scalable. Serving more work units is typically what is meant by performance improvement in terms of scalability. But as datasets expand, this may also imply the capacity for handling greater task units. If there is a performance problem in the application, then the system will be slow only for a single user. However, if there is a scalability issue, the system may be quick for a single user but slow down when there are many users using the application at once.

#20. What is NoSQL DataBase?
Instead of the columns and rows used by relational databases, NoSQL database technology stores data in JSON documents. To be precise, NoSQL does not mean "no SQL," but rather "not only SQL." This indicates that a NoSQL JSON database can essentially "use no SQL" to store and retrieve data. Or, for the best of both worlds, you can combine the adaptability of JSON with the strength of SQL. Because of this, NoSQL databases are designed to be adaptable, scalable, and quick to meet the data management needs of contemporary enterprises.

⌚ What is the difference between NoSQL and SQL database?
SQL	NoSQL
SQL databases are primarily called RDBMS or Relational Databases	NoSQL databases are primarily called as Non-relational or distributed database.
Traditional RDBMS analyse and obtain the data for further insights using SQL syntax and queries. For OLAP systems, they are employed.	Different types of database technology make into a NoSQL database system. In response to the requirements put forth for the creation of the current application, these databases were created.
Table-based databases are SQL databases.	Document-based, key-value pair, and graph databases are all types of NoSQL databases.
The best solution for resolving ACID issues is an RDBMS database.	Problems with data availability are best solved with NoSQL.


⌚ What is Caching?
Data caching is the technique of keeping several copies of data or files in a temporary storage space—also known as a cache—so they may be retrieved more quickly. In order to ensure consumers do not need to download information each time they access a website or application to speed up site loading, it saves data for software programmes, servers, and web browsers.

Multimedia items including photos, files, and scripts that are automatically stored on a device the first time a user runs an application or accesses a website are examples of cached data. Every time a user opens or accesses the programme or website after that, this is used to swiftly load the necessary information. The von Neumann bottleneck, which considers strategies to better provide speedier memory access, can be solved by caching.

⌚ What Consistency design patterns are there for different systems?
Every read request should receive the most recent data written, according to consistency from the CAP theorem. When there are several versions of the same data, it becomes difficult to synchronise them so that the clients always receive up-to-date information. The available consistency patterns are as follows:

Weak Consistency: The read request may or may not be successful in obtaining the new data after a write operation. Real-time use cases like VoIP, video chat, and online gaming all benefit from this kind of stability. For instance, if we experience a brief network outage while on a phone call, we will lose all information that was said during that time.

Eventual Consistency: Following a data write, reads will finally receive the most recent data in a matter of milliseconds. Here, asynchronous replication of the data is used. DNS and email systems both use them. In highly accessible systems, this works well.

Strong Consistency: Subsequent reads will always access the most recent data after a write. Here, synchronous replication of the data is used. This is seen in RDBMS and file systems, which are appropriate for systems needing data transfers.

⌚ What do you mean by CAP theorm?
A distributed system cannot ensure C, A, and P simultaneously, according to the CAP(Consistency-Availability-Partition Tolerance) theorem. It can only offer two of the three assurances at most. Let's use a distributed database system to help us comprehend this.

Consistency. The most recent write or an error are sent to all reads.
Availability. Although data is present in every read, it might not be the most recent.
Partition of tolerance:Even when the network fails, the system still functions (ie; dropped partitions, slow network connections, or unavailable network connections between nodes.)

⌚ What is a message queue?
A message queue is a tool used in software engineering to facilitate communication between threads running in the same process or between processes. By holding messages in a queue until the recipient collects them, message queues offer an asynchronous communication protocol that eliminates the need for simultaneous interaction between the sender and the recipient.

Programs can communicate with one another using message queues in operating systems or apps. Messages can be passed between computer systems using them as well.

⌚ What are a few of the distributed systems design problems?
These are a few of the problems with distributed systems:
Heterogeneity: The Internet enables the execution of applications across a diverse range of computers and networks. Since common Internet protocols are used for communication between networks, the distinctions between them are concealed. When developing distributed applications, this becomes a This becomes problematic when developing distributed applications.

Openness: The degree of openness in a system indicates how easily it may be modified and re-implemented. It describes in distributed systems the extent to which new sharing services can be implemented and made accessible to clients.

Security: Given its importance to users, the data stored in distributed systems needs to be protected. Maintaining the confidentiality, availability, and integrity of distributed systems can be difficult at times.

Scalability: A system is scalable if it continues to function well even when the volume of requests and available resources significantly rise. When designing a distributed system, it is important to consider how well the system can be scaled to handle a range of user loads.

Failure handling: In a distributed system, there can be partial failures, which means that even if certain components stop working, others will still work. The identification of the correct components where the problems occur makes it difficult to handle these failures.

⌚ How do you design parking lot system?
let's talk about functional requirements:

There are numerous points of entry and departure into the parking lot.
There are numerous parking levels available.
There are several rows of parking spaces on each floor.
Buses, cars, motorbikes, and other types of vehicles can all be parked in the parking lot.
There are large, small, and motorcycle parking spaces available in the lot.
A parking ticket can be obtained from the entry point, and the consumer can pay the parking price at any of the exit points.

Non Functional Requirement

Scale the system to a parking lot with 5,000 spaces.
Ten thousand parkings per day should be no problem for the system.
The system should be able to save ticketing data for, say, ten years.

Potential advices:

Consider an algorithm that would place a vehicle in the proper parking space.
Consider the many entities needed for system design.

⌚ How do you design netflix?
Netflix has 3 main components which we are going to discuss in an interview
OC or netflix CDN
backend
client

Gather Requirements:

The following requirements should be supported by the application.
At a predetermined time, the content creator should be able to post new content.
The video is accessible to viewers on a variety of platforms (TV, mobile-app, etc.).
Users ought to have the option of searching for videos based on their titles.
The programme should be able to play videos with subtitles.

How do you respond to interview questions about system design?
Ask the interviewer for clarity by asking questions: It is advisable to ask the interviewer pertinent questions given that the questions are meant to be intentionally vague in order to make sure that you and the interviewer are on the same page. By posing inquiries, you can demonstrate your interest in the needs of your customers.

Gather Requirement: List all the features that are necessary, the most frequent issues, and the performance standards that the system is anticipated to meet. This process enables the interviewer to assess your ability to prepare, anticipate difficulties, and provide solutions for each of them. When developing a system, each decision counts. There must be at least one list of advantages and disadvantages for each option.

Create a design: For each of the chosen needs, create a high-level design and a low-level design solution. Talk about the design's benefits and drawbacks. Talk about their advantages for the company as well.

Systems Design Fundamentals::
-----------------------------
#1. Introduction::
----------------
     Where the coding interview serves primarily as an assessment of your problem-solving ability, the systems design interview is a test of your engineering knowledge veiled behind the facade of an open-ended design question.
    
      Welcome to the crucible of modern software.
  
    ------------------------------------------------------------
    
#2. What Are Design Fundamentals?
-----------------------------------
       Building scalable, production-ready applications is both art and science. Science, in that it requires knowledge of many topics in computer engineering; art, in that it demands an eye for making smart design choices and piecing together the right technologies.
       
      -> Master both disciplines and you, too, can become a Systems Expert.  
-----------------------------------------------------------------------
#3. Client—Server Model
------------------------
    A client is a thing that talks to servers. A server is a thing that talks to clients. The client—server model is a thing made up of a bunch of clients and servers talking to one another.
    
    And that, kids, is how the Internet works!
    
    -> 5 Key Terms

------------------------------------------------------
#4. Network Protocols
---------------------
    -> IP packets. TCP headers. HTTP requests.
    
    ->  As daunting as they may seem, these low-level networking concepts are essential to understanding how machines in a system communicate with one another. And as we all know, proper communication is key for thriving relationships!
    
    ->  3 Prerequisites
    ->  4 Key Terms
-----------------------------------------------------------

#5. Storage
------------------------------------------------------
     -> An entire video dedicated just to the storage of data?
    
     -> Yes! Multiple videos, in fact, as you'll see later on when we discuss databases. As it turns out, information storage is an incredibly complex topic that is of vital importance to systems design. 
    Don't even think of skipping this lesson!
    
    -> 4 Key Terms
----------------------------------------------------------------------
#6. Latency And Throughput
---------------------------
    -> If you've ever experienced lag in a video game, it was most likely due to a combination of high latency and low throughput. And lag sucks.
    
    -> It is therefore your Call of Duty to master these two concepts and to join the crusade against high ping.
    
    -> 2 Prerequisites
    -> 2 Key Terms
    
-----------------------------------------------------------------------
#7. Availability
------------------------------
    -> Oops! This content is unavailable right now. Please try again later.
    
    
    
    
    -> Just kidding! SystemsExpert is a highly available system.
    
    -> 3 Prerequisites
    -> 6 Key Terms
---------------------------------------------------------------

#8.Caching
------------------------------------------------
     ->  What do a punching bag and a cache have in common?
       .
       .    
       .
       .    
       They can both take a hit!    ( ͡° ͜ʖ ͡°)
       
      -> 3 Prerequisites
      -> 5 Key Terms
 ----------------------------------------------
 
#9. Proxies
------------
    Often used by nefarious hackers to conceal their identity and obfuscate their location, these special intermediary servers boast many important real-life applications within the context of caching, access control, and censorship bypassing, amongst other things.
    
    -> 2 Prerequisites
    ------------------
     ->1. client
     -----------A machine or process that requests data or service from a server.
     -> Note that a single machine or piece of sofware can be both a client and a server at the same time. for instance, a single machine could act as a server for end users and as a client for a database.
     
     ->2. server
     -----------A machine or process that provides data or service for a client, usually by listening for incoming network calls.
     -> Note that a single machine or piece of software can be both a client and a server at the same time. for instance, a single machine could act as a server for end users and as a client for a database.
     
    ->3 Key Terms
      -----------
      1. Forward Proxy
      ---------------- A server that sits between a client and servers and acts on behalf of the client, typically used to mask the client's identity(IP address). Note that forward proxies are often referred to as just proxies.
      
      2. Reverse Proxy
      ----------------A server that sits between clients and servers and acts on behalf of the servers, typically used for logging, load balancing, or caching.
      
      3.Nginx
      ----------Nginx is a very popular webserver that's often used as a reverse proxy and load balancer. 
      
      
#10. Load Balancers
--------------------
    Relentlessly distributing network requests across multiple servers, these digital traffic cops act as watchful guardians for your system, ensuring that it operates at peak performance day and night.
    
    -> 1 Prerequisite
    -> 4 Key Terms

 --------------------------------------------------------------------------------------------
#11. Hashing
-------------
     Hashing? Like from hash tables? Should be simple enough, right?
     
     The good news is that, yes, hashing like from hash tables.
     
     The bad news is that, no, not simple enough. The video duration and thumbnail should be ominously indicative.
     
    -> 2 Prerequisites
    -> 3 Key Terms
---------------------------------

#12. Relational Databases
-------------------------
     Tables and ACID.
     
     No, we're not describing a drug lord's desk, but rather referring to key properties of relational databases. There's a lot of material to cover here, so hit the play button, kick back, and get ready to store tons of knowledge in the biggest database of them all: your brain.
     
     -> 3 Prerequisites
     -> 10 Key Terms
-----------------------------------------------

#13. Key-Value Stores
-----------------------
     One of the most commonly used NoSQL paradigms today, the key-value store bases its data model on the associative array data type.
     
     The result? A fast, flexible storage machine that resembles a hash table. That's right folks, our favorite friendly neighborhood data structure strikes again!
     
     -> 2 Prerequisites
     -> 4 Key Terms
------------------------------------------------------

     
#14. Replication And Sharding
------------------------------
     A system's performance is often only as good as its database's; optimize the latter, and watch as the former improves in tandem!
     
     On that note, in this video we'll examine how data redundancy and data partitioning techniques can be used to enhance a system's fault tolerance, throughput, and overall reliability.
     
     -> 6 Prerequisites
     -> 3 Key Terms
-------------------------------------
#15. Leader Election
---------------------
     Citizens in a society typically elect a leader by voting for their preferred candidate. But how do servers in a distributed system choose a master node? Via algorithms of course!
     
     This form of algorithmic democracy is known as "leader election", though we personally think "algorithmocracy" sounds way cooler.
     
     -> 5 Prerequisites
     -> 5 Key Terms
----------------------------------

#16. Peer-To-Peer Networks
     Equality for all.
     Sharing is caring.
     Unity makes strength.
     The more the merrier.
     Teamwork makes the dream work.
     Welcome to peer-to-peer networks!
     
     -> 2 Prerequisites
     -> 2 Key Terms
-----------------------------------------

#17. Polling And Streaming
---------------------------
     You can think of polling and streaming kind of like a classroom; sometimes students ask the teacher lots of questions, and other times they quiet down and listen attentively to the teacher's lecture.
     
     Now fire up the video and get ready to stream; you won't be able to poll here. Class is in session!
     
    -> 2 Prerequisites
    -> 2 Key Terms
---------------------------------------------------------------------------------------------

#18. Configuration
     The config file is like the genome of a computer application; it stores parameters that define your system's critical settings, much like your DNA stores the genes that define your physical characteristics.
     
     Unlike its biological counterpart though, the config file is easily editable. No gene therapy needed!
     
     -> 3 Prerequisites
     -> 1 Key Term
-------------------------------------------------------------------

#19. Rate Limiting
-------------------------
     *Poke*
     *Poke*
     *Poke*
     *Po——*
     
     Too many pokes! You just got rate limited.
     
    -> 2 Prerequisites
    -> 4 Key Terms
------------------------------------------------------------

#20. Logging And Monitoring
------------------------------
     In order to properly understand and diagnose issues that crop up within a system, it’s critical to have mechanisms in place that create audit trails of various events that occur within said system.
     
     So go ahead, unleash your inner Orwell and go full Big Brother on your application.
     
    -> 1 Prerequisite
    ->  3 Key Terms
-----------------------------------------------------------------


#21. Publish/Subscribe Pattern
------------------------------
     Publish/Subscribe. Press/Tug. Produce/Consume. Push/Pull. Send/Receive. Throw/Catch. Thrust/Retrieve.
     
     Three of these can be used interchangeably in the context of systems design. The others cannot.
     
     -> 3 Prerequisites
     -> 4 Key Terms
------------------------------

This course is in continuous development. In the coming few months, we'll be adding some more advanced topics, including MapReduce, the Publish–Subscribe Pattern, and API Design.


===========================================================
Course Content

  Lecture - 1
What exactly is a System Design Interview?

Expectations from Interviewee

Breadth Vs Depth

Should you know everything about everything?

Types of Jobs to target from the market

System Design Process ( Motivating Example: Design UBER.)

Common Mistakes

Chaotic Approach

Systematic Approach

Design problem focussed on Requirement Analysis and Data Modelling

  Lecture - 2
Trade-offs in a large scale system(Motivating Example: Design TWITTER.)

Performance Vs Scalability

Latency Vs Throughput

Availability Vs Consistency(CAP Theorem)

Design problem focussed on Requirement Analysis and Data Modelling

  Lecture - 3
Components of a large scale system

Queue

SNS (Lab Demo)

SQS (Lab Demo)

Design problem based on event-driven system

  Lecture - 4
Components of a large scale system

Databases/Storage Layer
Details on B-Tree, LSM Tree, storage techniques, and resolving bottlenecks

Design Problem focussed on intricacies of storage systems

  Lecture - 5
Components of a large scale system

Databases/Storage Layer

Indexes (Primary, Secondary, single key, multi-key)

Design Problem focusing on the depth of data modeling and indexes
  Lecture - 6
Components of a large scale system

Databases/Storage Laye
Consistent Hashing
Replication and sharding
Cache
Caching Policies: Write - Through/Around/Back
  Lecture - 7
Patterns of Enterprise Application Architecture

MicroService Vs Monolith

API Gateway

  Lecture - 8
Components of a large scale system
 

DNS
CDN
Applying what you’ve learned: End-to-End System Design Problem Solving
  Lecture - 9
Components of a large scale system

Load Balancer

Aspects of Security

Encryption

DDoS

Man in the middle attacks

Isolation

Applying what you’ve learned: End-to-End System Design Problem Solving

  Lecture - 10
Some useful stuff that should be on tips of the interviewee

Common numbers to remember for the back of the envelope calculation

Tips on Schema Design

Applying what you’ve learned: End-to-End System Design Problem Solving(FB messenger, Youtube/Netflix, Dropbox)