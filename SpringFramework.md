-----------------
#Spring Framework ::
-----------------
IoC Container
The IoC container is responsible to instantiate, configure and assemble the objects. The IoC container gets informations from the XML file and works accordingly. The main tasks performed by IoC container are:

to instantiate the application class
to configure the object
to assemble the dependencies between the objects
There are two types of IoC containers. They are:
BeanFactory
ApplicationContext

Dependency Injection (DI) is a design pattern that removes the dependency from the programming code so that it can be easy to manage and test the application. Dependency Injection makes our programming code loosely coupled.

Spring framework provides two ways to inject dependency

By Constructor
By Setter method

MVC is a Java framework which is used to build web applications. It follows the Model-View-Controller design pattern. It implements all the basic features of a core spring framework like Inversion of Control, Dependency Injection.

Model - A model contains the data of the application. A data can be a single object or a collection of objects.
Controller - A controller contains the business logic of an application. Here, the @Controller annotation is used to mark the class as the controller.
View - A view represents the provided information in a particular format. Generally, JSP+JSTL is used to create a view page. Although spring also supports other view technologies such as Apache Velocity, Thymeleaf and FreeMarker.
Front Controller - In Spring Web MVC, the DispatcherServlet class works as the front controller. It is responsible to manage the flow of the Spring MVC application.

As displayed in the figure, all the incoming request is intercepted by the DispatcherServlet that works as the front controller.
The DispatcherServlet gets an entry of handler mapping from the XML file and forwards the request to the controller.
The controller returns an object of ModelAndView.
The DispatcherServlet checks the entry of view resolver in the XML file and invokes the specified view component.




@Controller
->@Controller is used to mark classes as Spring MVC Controller.
->It is a specialized version of @Component annotation.
->In @Controller, we can return a view in Spring Web MVC.
->@Controller annotation indicates that the class is a “controller” like a web controller.
->In @Controller, we need to use @ResponseBody on every handler method.
->It was added to Spring 2.5 version.

@RestController
->@RestController annotation is a special controller used in RESTful Web services, and it’s the combination of @Controller and @ResponseBody annotation.
->It is a specialized version of @Controller annotation.
->In @RestController, we can not return a view.
->@RestController annotation indicates that class is a controller where @RequestMapping methods assume @ResponseBody semantics by default.
->In @RestController, we don’t need to use @ResponseBody on every handler method.
->It was added to Spring 4.0 version.


Aspect Oriented Programming (AOP) compliments OOPs in the sense that it also provides modularity. But the key unit of modularity is aspect than class.
AOP breaks the program logic into distinct parts (called concerns). It is used to increase modularity by cross-cutting concerns.
A cross-cutting concern is a concern that can affect the whole application and should be centralized in one location in code as possible, such as transaction management, authentication, logging, security etc.


Join point
Join point is any point in your program such as method execution, exception handling, field access etc. Spring supports only method execution join point.

Pointcut
It is an expression language of AOP that matches join points.

Advice
Advice represents an action taken by an aspect at a particular join point. There are different types of advices:

Before Advice: it executes before a join point.
After Returning Advice: it executes after a joint point completes normally.
After Throwing Advice: it executes if method exits by throwing an exception.
After (finally) Advice: it executes after a join point regardless of join point exit whether normally or exceptional return.
Around Advice: It executes before and after a join point.




1.What’s the difference between @Component, @Controller, @Repository & @Service annotations in Spring?
-----------------------------------------------------------------------------------------------------
     @Component: This marks a java class as a bean. It is a generic stereotype for any Spring-managed component. The component-scanning mechanism of spring now can pick it up and pull it into the application context.
    -> @Component is a generic stereotype for any Spring-managed component.
    -> It is a basic auto component scan annotation which indicates that an annotated class is an auto scan component.
    
                          Component
                          /  |   \
                         /   |    \
               controller  Service Repository
    
    @Controller: This marks a class as a Spring Web MVC controller. Beans marked with it are automatically imported into the Dependency Injection container.
    
    @Service: This annotation is a specialization of the component annotation. It doesn’t provide any additional behavior over the @Component annotation.
    You can use @Service over @Component in service-layer classes as it specifies intent in a better way.
    -> It indicates that an annotated class is a Service component of the business layer.
    ->Service annotates classes at the service layer
    
    @Repository: This annotation is a specialization of the @Component annotation with similar use and functionality. It provides additional benefits specifically for DAOs.
    It imports the DAOs into the DI container and makes the unchecked exceptions eligible for translation into Spring DataAccessException.
    -> You need to use this annotation within the persistence layer that acts like database repository.
    -> Repository annotates classes at the persistence layer, which will act as a database repository
--------------------------------------------------------------------------------------------------------
1.Spring Core:::
-----------------
Q1. What Is Spring Framework?
----------------------------
->Spring is the most broadly used framework for the development of Java Enterprise Edition applications. The core features of Spring can be used in developing any Java application.
->We can use its extensions for building various web applications on top of the Jakarta EE platform, or we may just use its dependency injection provisions in simple standalone applications.
------------------------------------------------------------------------------------------------------------
Q2. What Are the Benefits of Using Spring?
-----------------------------------------------
    Spring targets to make Jakarta EE development easier. Here are the advantages of using it:
    
->    Lightweight: there is a slight overhead of using the framework in development
->    Inversion of Control (IoC): Spring container takes care of wiring dependencies of various objects, instead of creating or looking for dependent objects
->    Aspect Oriented Programming (AOP): Spring supports AOP to separate business logic from system services
->    IoC container: it manages Spring Bean life cycle and project specific configurations
->    MVC framework: that is used to create web applications or RESTful web services, capable of returning XML/JSON responses
->    Transaction management: reduces the amount of boiler-plate code in JDBC operations, file uploading, etc., either by using Java annotations or by Spring Bean XML configuration file
->    Exception Handling: Spring provides a convenient API for translating technology-specific exceptions into unchecked exceptions
-------------------------------------------------------------------------------------------------------------
Q3. What Spring Sub-Projects Do You Know? Describe Them Briefly.
----------------------------------------------------------------------------------------------------------
Core – a key module that provides fundamental parts of the framework, like IoC or DI
JDBC – this module enables a JDBC-abstraction layer that removes the need to do JDBC coding for specific vendor databases
ORM integration – provides integration layers for popular object-relational mapping APIs, such as JPA, JDO, and Hibernate
Web – a web-oriented integration module, providing multipart file upload, Servlet listeners, and web-oriented application context functionalities
MVC framework – a web module implementing the Model View Controller design pattern
AOP module – aspect-oriented programming implementation allowing the definition of clean method-interceptors and pointcuts
------------------------------------------------------------------------------------------------------------------
What Is Dependency Injection?
------------------------------
Dependency Injection, an aspect of Inversion of Control (IoC), is a general concept stating that you do not create your objects manually but instead describe how they should be created. An IoC container will instantiate required classes if needed.

------------------------------------------------------------------------------------------------------------------
Q5. How Can We Inject Beans in Spring?
--------------------------------------
A few different options exist:

Setter Injection
Constructor Injection
Field Injection
The configuration can be done using XML files or annotations.
-----------------------------------------------------------------------------------------------------------
Q6. Which Is the Best Way of Injecting Beans and Why?
-----------------------------------------------------
The recommended approach is to use constructor arguments for mandatory dependencies and setters for optional ones. Constructor injection allows injecting values to immutable fields and makes testing easier.

-----------------------------------------------------------------------------------------------------------------
Q7. What Is the Difference Between Beanfactory and Applicationcontext?
-------------------------------------------------------------------
BeanFactory is an interface representing a container that provides and manages bean instances. The default implementation instantiates beans lazily when getBean() is called.

ApplicationContext is an interface representing a container holding all information, metadata, and beans in the application. It also extends the BeanFactory interface but the default implementation instantiates beans eagerly when the application starts. This behavior can be overridden for individual beans.

----------------------------------------------------------------------------------------------------------------------------------------------
Q8. What Is a Spring Bean?
---------------------------
The Spring Beans are Java Objects that are initialized by the Spring IoC container.

Spring Bean Annotations
-----------------------
@ComponentScan::- pring can automatically scan a package for beans if component scanning is enabled.@ComponentScan configures which packages to scan for classes with annotation configuration. We can specify the base package names directly with one of the basePackages or value arguments (value is an alias for basePackages):
ex:-
@Configuration
@ComponentScan(basePackages = "com.baeldung.annotations")
class VehicleFactoryConfig {}

@Component::- is a class level annotation. During the component scan, Spring Framework automatically detects classes annotated with @Component:

-------------------------------------------------------------------------------------------------------------------------------------------
Q9. What Is the Default Bean Scope in Spring Framework?
---------------------------------------------------------
By default, a Spring Bean is initialized as a singleton.

-------------------------------------------------------------------------------------------------------------------------------------
Q10. How to Define the Scope of a Bean?
--------------------------------------------------------------------------------------------------------------------------------------
To set Spring Bean's scope, we can use @Scope annotation or “scope” attribute in XML configuration files. There are five supported scopes:

1.singleton:-(Default) Scopes a single bean definition to a single object instance for each Spring IoC container.

2.prototype :- Scopes a single bean definition to any number of object instances.

3.request :- Scopes a single bean definition to the lifecycle of a single HTTP request. That is, each HTTP request has its own instance of a bean created off the back of a single bean definition. Only valid in the context of a web-aware Spring ApplicationContext.

4.session :- Scopes a single bean definition to the lifecycle of an HTTP Session. Only valid in the context of a web-aware Spring ApplicationContext.

5.application :-Scopes a single bean definition to the lifecycle of a ServletContext. Only valid in the context of a web-aware Spring ApplicationContext.

6.websocket :-Scopes a single bean definition to the lifecycle of a WebSocket. Only valid in the context of a web-aware Spring ApplicationContext.

-------------------------------------------------------------------------------------------------------------------------------------------
Q11. Are Singleton Beans Thread-Safe?
------------------------------------------------------------------------------------------------------------------------------------------
No, singleton beans are not thread-safe, as thread safety is about execution, whereas the singleton is a design pattern focusing on creation. Thread safety depends only on the bean implementation itself.

---------------------------------------------------------------------------------------------------------------------------------------------
Q12. What Does the Spring Bean Lifecycle Look Like?
----------------------------------------------------
First, a Spring bean needs to be instantiated, based on Java or XML bean definition. It may also be required to perform some initialization to get it into a usable state. After that, when the bean is no longer required, it will be removed from the IoC container.

-----------------------------------------------------------------------------------------------------------------------------------------
Q13. What Is the Spring Java-Based Configuration?
--------------------------------------------------
It's one of the ways of configuring Spring-based applications in a type-safe manner. It's an alternative to the XML-based configuration.

--------------------------------------------------------------------------------------------------------------------------------------
Q14. Can We Have Multiple Spring Configuration Files in One Project?
-----------------------------------------------------------------------
Yes, in large projects, having multiple Spring configurations is recommended to increase maintainability and modularity.

You can load multiple Java-based configuration files:

@Configuration
@Import({MainConfig.class, SchedulerConfig.class})
public class AppConfig {
Or load one XML file that will contain all other configs:

ApplicationContext context = new ClassPathXmlApplicationContext("spring-all.xml");
And inside this XML file you'll have:

<import resource="main.xml"/>
<import resource="scheduler.xml"/>

---------------------------------------------------------------------------------------
Q15. What Is Spring Security?
Spring Security is a separate module of the Spring framework that focuses on providing authentication and authorization methods in Java applications. It also takes care of most of the common security vulnerabilities such as CSRF attacks.

To use Spring Security in web applications, you can get started with a simple annotation: @EnableWebSecurity.

----------------------------------------------------------------------------------------------------------
2.AOP:::
-------------

Spring AOP Overview
Most of the enterprise applications have some common crosscutting concerns that are applicable to different types of Objects and modules. Some of the common crosscutting concerns are logging, transaction management, data validation, etc. In Object Oriented Programming, modularity of application is achieved by Classes whereas in Aspect Oriented Programming application modularity is achieved by Aspects and they are configured to cut across different classes. Spring AOP takes out the direct dependency of crosscutting tasks from classes that we can’t achieve through normal object oriented programming model. For example, we can have a separate class for logging but again the functional classes will have to call these methods to achieve logging across the application.

Aspect Oriented Programming Core Concepts
Before we dive into the implementation of Spring AOP implementation, we should understand the core concepts of AOP.

Aspect: An aspect is a class that implements enterprise application concerns that cut across multiple classes, such as transaction management. Aspects can be a normal class configured through Spring XML configuration or we can use Spring AspectJ integration to define a class as Aspect using @Aspect annotation.
Join Point: A join point is a specific point in the application such as method execution, exception handling, changing object variable values, etc. In Spring AOP a join point is always the execution of a method.
Advice: Advices are actions taken for a particular join point. In terms of programming, they are methods that get executed when a certain join point with matching pointcut is reached in the application. You can think of Advices as Struts2 interceptors or Servlet Filters.
Pointcut: Pointcut is expressions that are matched with join points to determine whether advice needs to be executed or not. Pointcut uses different kinds of expressions that are matched with the join points and Spring framework uses the AspectJ pointcut expression language.
Target Object: They are the object on which advices are applied. Spring AOP is implemented using runtime proxies so this object is always a proxied object. What is means is that a subclass is created at runtime where the target method is overridden and advice are included based on their configuration.
AOP proxy: Spring AOP implementation uses JDK dynamic proxy to create the Proxy classes with target classes and advice invocations, these are called AOP proxy classes. We can also use CGLIB proxy by adding it as the dependency in the Spring AOP project.
Weaving: It is the process of linking aspects with other objects to create the advised proxy objects. This can be done at compile time, load time or at runtime. Spring AOP performs weaving at the runtime.
AOP Advice Types
Based on the execution strategy of advice, they are of the following types.

Before Advice: These advices runs before the execution of join point methods. We can use @Before annotation to mark an advice type as Before advice.
After (finally) Advice: An advice that gets executed after the join point method finishes executing, whether normally or by throwing an exception. We can create after advice using @After annotation.
After Returning Advice: Sometimes we want advice methods to execute only if the join point method executes normally. We can use @AfterReturning annotation to mark a method as after returning advice.
After Throwing Advice: This advice gets executed only when join point method throws exception, we can use it to rollback the transaction declaratively. We use @AfterThrowing annotation for this type of advice.
Around Advice: This is the most important and powerful advice. This advice surrounds the join point method and we can also choose whether to execute the join point method or not. We can write advice code that gets executed before and after the execution of the join point method. It is the responsibility of around advice to invoke the join point method and return values if the method is returning something. We use @Around annotation to create around advice methods.


---------------------
3.Spring Thread Pooling:::
--------------------------

---------------------------
4.Scheduling:::::
---------------------------

---------------------------
5.Spring MVC:::
--------------------

----------------------
5.Spring WebFlux:::
-------------------

---------------------
6.Transaction management:::
---------------------------

---------------------------
7.JDBC templates:::
---------------------------

---------------------------
8.Spring Cloud ::::
------------------------------------------------------------------------------------------------
ELK Stack
Eureka
Ribbon
Netflix Sevice Discovery
Consul Service Discovery
Hystrix Circuit Breaker
Cloud Foudry
Zuul API Gateway
Zipkin
Sleuth
-----------------
#Spring Cloud::
--------------

Spring Cloud Config
Centralized external configuration management backed by a git repository. The configuration resources map directly to Spring Environment but could be used by non-Spring applications if desired.

Spring Cloud Netflix
Integration with various Netflix OSS components (Eureka, Hystrix, Zuul, Archaius, etc.).

Spring Cloud Bus
An event bus for linking services and service instances together with distributed messaging. Useful for propagating state changes across a cluster (e.g. config change events).

Spring Cloud Cloudfoundry
Integrates your application with Pivotal Cloud Foundry. Provides a service discovery implementation and also makes it easy to implement SSO and OAuth2 protected resources.

Spring Cloud Open Service Broker
Provides a starting point for building a service broker that implements the Open Service Broker API.

Spring Cloud Cluster
Leadership election and common stateful patterns with an abstraction and implementation for Zookeeper, Redis, Hazelcast, Consul.

Spring Cloud Consul
Service discovery and configuration management with Hashicorp Consul.

Spring Cloud Security
Provides support for load-balanced OAuth2 rest client and authentication header relays in a Zuul proxy.

Spring Cloud Sleuth
Distributed tracing for Spring Cloud applications, compatible with Zipkin, HTrace and log-based (e.g. ELK) tracing.

Spring Cloud Data Flow
A cloud-native orchestration service for composable microservice applications on modern runtimes. Easy-to-use DSL, drag-and-drop GUI, and REST-APIs together simplifies the overall orchestration of microservice based data pipelines.

Spring Cloud Stream
A lightweight event-driven microservices framework to quickly build applications that can connect to external systems. Simple declarative model to send and receive messages using Apache Kafka or RabbitMQ between Spring Boot apps.

Spring Cloud Stream App Starters
Spring Cloud Stream App Starters are Spring Boot based Spring Integration applications that provide integration with external systems.

Spring Cloud Task
A short-lived microservices framework to quickly build applications that perform finite amounts of data processing. Simple declarative for adding both functional and non-functional features to Spring Boot apps.

Spring Cloud Task App Starters
Spring Cloud Task App Starters are Spring Boot applications that may be any process including Spring Batch jobs that do not run forever, and they end/stop after a finite period of data processing.

Spring Cloud Zookeeper
Service discovery and configuration management with Apache Zookeeper.

Spring Cloud AWS
Easy integration with hosted Amazon Web Services. It offers a convenient way to interact with AWS provided services using well-known Spring idioms and APIs, such as the messaging or caching API. Developers can build their application around the hosted services without having to care about infrastructure or maintenance.

Spring Cloud Connectors
Makes it easy for PaaS applications in a variety of platforms to connect to backend services like databases and message brokers (the project formerly known as "Spring Cloud").

Spring Cloud Starters
Spring Boot-style starter projects to ease dependency management for consumers of Spring Cloud. (Discontinued as a project and merged with the other projects after Angel.SR2.)

Spring Cloud CLI
Spring Boot CLI plugin for creating Spring Cloud component applications quickly in Groovy

Spring Cloud Contract
Spring Cloud Contract is an umbrella project holding solutions that help users in successfully implementing the Consumer Driven Contracts approach.

Spring Cloud Gateway
Spring Cloud Gateway is an intelligent and programmable router based on Project Reactor.

Spring Cloud OpenFeign
Spring Cloud OpenFeign provides integrations for Spring Boot apps through autoconfiguration and binding to the Spring Environment and other Spring programming model idioms.

Spring Cloud Pipelines
Spring Cloud Pipelines provides an opinionated deployment pipeline with steps to ensure that your application can be deployed in zero downtime fashion and easilly rolled back of something goes wrong.

Spring Cloud Function
Spring Cloud Function promotes the implementation of business logic via functions. It supports a uniform programming model across serverless providers, as well as the ability to run standalone (locally or in a PaaS).



=====================================================================================================================================
1.What's the difference between EnableEurekaClient and EnableDiscoveryClient?
-----------------------------------------------------------------------------
There are multiple implementations of "Discovery Service" (eureka, consul, zookeeper).
@EnableDiscoveryClient lives in spring-cloud-commons and picks the implementation on the classpath.
@EnableEurekaClient lives in spring-cloud-netflix and only works for eureka. If eureka is on your classpath, they are effectively the same.


@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableEurekaClient
@RestController
---------------------------------------------------------------------------
1.



===========================================================================================================================================================================
#Spring Data:::
---------------
Spring Data Commons - Core Spring concepts underpinning every Spring Data module.

Spring Data JDBC - Spring Data repository support for JDBC.

Spring Data JDBC Ext - Support for database specific extensions to standard JDBC including support for Oracle RAC fast connection failover, AQ JMS support and support for using advanced data types.

Spring Data JPA - Spring Data repository support for JPA.

Spring Data KeyValue - Map based repositories and SPIs to easily build a Spring Data module for key-value stores.

Spring Data LDAP - Spring Data repository support for Spring LDAP.

Spring Data MongoDB - Spring based, object-document support and repositories for MongoDB.

Spring Data Redis - Easy configuration and access to Redis from Spring applications.

Spring Data REST - Exports Spring Data repositories as hypermedia-driven RESTful resources.

Spring Data for Apache Cassandra - Easy configuration and access to Apache Cassandra or large scale, highly available, data oriented Spring applications.

Spring Data for Apache Geode - Easy configuration and access to Apache Geode for highly consistent, low latency, data oriented Spring applications.

Spring Data for Apache Solr - Easy configuration and access to Apache Solr for your search oriented Spring applications.

Spring Data for Pivotal GemFire - Easy configuration and access to Pivotal GemFire for your highly consistent, low latency/high through-put, data oriented Spring applications.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------

#Spring Security:::  
                     1. https://backstage.forgerock.com/docs/am/5.5/oauth2-guide/
                     2. https://www.baeldung.com/security-spring
                     3. https://tools.ietf.org/html/rfc6749
---------------------------------------------------------------------------------------------
1. OAuth 2.0::-
----------------


#The Registration Series
The Basic Registration Process
Registration – Activate a New Account by Email
Resend the Verification Email
Registration – Password Encoding
Reset Your Password

#The Authentication Series
Basic Form Login (popular)
Form Login – Error Handling and Localization
Logout
Redirect to Different Pages after Login
Remember Me and Persistent Remember Me
Roles and Privileges
Prevent Brute Force Authentication Attempts
Spring Security Login Page with React

#Core Spring Security
Spring Security with Maven
Retrieve User Information in Spring Security (popular)
Spring Security Authentication Provider
Spring Security Expressions - hasRole Example
Unable to locate Spring NamespaceHandler for XML schema namespace
No bean named ‘springSecurityFilterChain’ is defined
Spring Security - security none, filters none, access permitAll
Session Management (popular)
Intro to Spring Security LDAP
How to Manually Authenticate User with Spring Security
Extra Login Fields with Spring Security
​Introduction to Spring Method Security​​​
Spring Boot Security Auto-Configuration (popular)
Default Password Encoder in Spring Security 5
Spring Security Custom AuthenticationFailureHandler
Find the Registered Spring Security Filters
Introduction to Spring Securit​​y Taglibs

#OAuth2 with Spring Security
Spring REST API + OAuth2 + Angular (popular)
Using JWT with Spring Security OAuth (popular)
Simple Single Sign-On with Spring Security OAuth2 (popular)
Spring Security and OpenID Connect
Spring Security 5 – OAuth2 Login
Testing an OAuth Secured API with Spring MVC
Spring Security OAuth2 – Simple Token Revocation
​Logout in an OAuth Secured Application
​Spring Security 5 – OAuth2 Login
Extracting Principal and Authorities using Spring Security OAuth
OAuth2 – @EnableResourceServer vs @EnableOAuth2Sso
Customizing Authorization and Token Requests with Spring Security 5.1 Client

#Spring Security with REST
Securing a REST Service with Spring Security (popular)
Spring Security – Basic Authentication
Spring Security – Digest Authentication
Basic and Digest Authentication for a REST API
Authentication against a REST API with Spring Security
RestTemplate – Basic Authentication
RestTemplate – Digest Authentication

#Other Spring Tutorials
The REST with Spring Tutorial
How to build REST Services with Spring
Persistence with Spring Tutorial
How to Build the Persistence Layer of an application with Spring and Hibernate, JPA, Spring Data, etc
Spring Exceptions Tutorial
Common Exceptions in Spring with examples – why they occur and how to solve them quickly

-------------------------------------------------------------------------------------------------------------------------------------------
#Spring Session::
--------------------
Spring Session provides an API and implementations for managing a user’s session information.

Features
---------
Spring Session makes it trivial to support clustered sessions without being tied to an application container specific solution. It also provides transparent integration with:

-> HttpSession - allows replacing the HttpSession in an application container (i.e. Tomcat) neutral way, with support for providing session IDs in headers to work with RESTful APIs

-> WebSocket - provides the ability to keep the HttpSession alive when receiving WebSocket messages

-> WebSession - allows replacing the Spring WebFlux’s WebSession in an application container neutral way

Modules
--------
Spring Session consists of the following modules:

->Spring Session Core - provides core Spring Session functionalities and APIs

->Spring Session Data Redis - provides SessionRepository and ReactiveSessionRepository implementation backed by Redis and configuration support

->Spring Session JDBC - provides SessionRepository implementation backed by a relational database and configuration support

->Spring Session Hazelcast - provides SessionRepository implementation backed by Hazelcast and configuration support

1. Spring Session MongoDB:::
----------------------------
Spring Session MongoDB provides an API and implementations for managing a user’s session information stored in MongoDB by leveraging Spring Data MongoDB.

Features
--------
Spring Session MongoDB provides the following features:

-> API and implementations for managing a user’s session

-> HttpSession - allows replacing the HttpSession in an application container (i.e. Tomcat) neutral way

-> Clustered Sessions - Spring Session makes it trivial to support clustered sessions without being tied to an application container specific solution.

-> Multiple Browser Sessions - Spring Session supports managing multiple users' sessions in a single browser instance (i.e. multiple authenticated accounts similar to Google).

-> RESTful APIs - Spring Session allows providing session ids in headers to work with RESTful APIs WebSocket - provides the ability to keep the HttpSession alive when receiving WebSocket messages


----------------------------------------------------------------------------------------------------------------------------------------------
#Spring Cloud Data Flow
---------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------
#Spring Integration::::- 1. https://docs.spring.io/spring-integration/docs/5.1.7.RELEASE/reference/html/
--------------------------------
Introduction
Using the Spring Framework encourages developers to code using interfaces and use dependency injection (DI) to provide a Plain Old Java Object (POJO) with the dependencies it needs to perform its tasks. Spring Integration takes this concept one step further, where POJOs are wired together using a messaging paradigm and individual components may not be aware of other components in the application. 
Such an application is built by assembling fine-grained reusable components to form a higher level of functionality. WIth careful design, these flows can be modularized and also reused at an even higher level.
In addition to wiring together fine-grained components, Spring Integration provides a wide selection of channel adapters and gateways to communicate with external systems. 
Channel Adapters are used for one-way integration (send or receive); gateways are used for request/reply scenarios (inbound or outbound). For a full list of adapters and gateways, refer to the reference documentation.
The Spring Cloud Stream project builds on Spring Integration, where Spring Integration is used as an engine for message-driven microservices.

Features
---------
Implementation of most of the Enterprise Integration Patterns
Endpoint
Channel (Point-to-point and Publish/Subscribe)
Aggregator
Filter
Transformer
Control Bus
…
Integration with External Systems
ReST/HTTP
FTP/SFTP
Twitter
WebServices (SOAP and ReST)
TCP/UDP
JMS
RabbitMQ
Email
…
The framework has extensive JMX support
Exposing framework components as MBeans
Adapters to obtain attributes from MBeans, invoke operations, send/receive notifications

----------------------------------------------------------------------------------------
#Spring HATEOAS
----------------------------------------------------------------------------------------
Spring HATEOAS provides some APIs to ease creating REST representations that follow the HATEOAS principle when working with Spring and especially Spring MVC. The core problem it tries to address is link creation and representation assembly.

Features
--------
->Model classes for link, resource representation models

->Link builder API to create links pointing to Spring MVC controller methods

->Support for hypermedia formats like HAL

Spring Boot Config
-------------------
->Spring Boot will do the following:
->HAL support
->Register support for entity links
->Wire up message converter support.
----------------------------------------------------------------------------------------
#Spring REST Docs
----------------------------------------------------------------------------------------
Spring REST Docs helps you to document RESTful services.

It combines hand-written documentation written with Asciidoctor and auto-generated snippets produced with Spring MVC Test. This approach frees you from the limitations of the documentation produced by tools like Swagger.

It helps you to produce documentation that is accurate, concise, and well-structured. This documentation then allows your users to get the information they need with a minimum of fuss.

Spring Boot Config
---------------
Spring Boot provides an @AutoConfigureRestDocs annotation to leverage Spring REST Docs in your tests.

-----------------------------------------------------------------------------------------
#Spring Web Flow
#Spring Web Services
-----------------------

------------------------
#Spring for Apache Kafka :::->  
                               1. https://docs.spring.io/spring-kafka/docs/2.2.8.RELEASE/reference/html/
-------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------
#Spring Batch
#Spring IO Platform
#Spring AMQP
Spring for Android
Spring Cloud Skipper
Spring CredHub
Spring Flo

Spring LDAP
Spring Mobile
Spring Roo
Spring Shell
Spring Statemachine
Spring Test HtmlUnit
Spring Vault



What is Inversion Of Control (IoC)?
Inversion Of Control (IoC) is a concept of software engineering, where the control of objects or a block of a program is transferred to a container or framework. IoC allows a container to take control of the flow of a program and make calls to our custom code rather than making calls to a library. To implement IoC, the Spring Dependency Injection concept is used.

What is Spring Container?
In the Spring framework, the interface ‘ApplicationContext’ represents the Spring container. The Spring container takes responsibility of instantiating, configuring and assembling objects (Spring beans), and managing their life cycles as well. In order to create beans, the Spring container takes help of configuration metadata, which can be in the form of either XML configuration or annotations. Spring Container is the short form of  ‘Spring IoC container’ and they are used interchangeably. For further reading on Spring Container, kindly visit Spring Official website (spring.io).

What is a Spring Bean?

In the context of Spring Framework, an object which is managed (instantiated, configured & assembled) by the Spring container is known as Spring bean.

What is Dependency in Spring?
Before learning Spring Dependency Injection, let’s discuss what is ‘Dependency In Spring’ first. Let’s consider a simple core Java class ‘Invoice’ as shown below.


public class Invoice {

   String name;
   Double amount;
   List<String> items;
   Vendor vendor;
   TaxDetails taxDetails;
}

class Vendor { 

}

interface TaxDetails {

}
As shown in the code snippet above, Invoice is a class which has various instance variables: name, amount, items, vendor, and taxDetails. If we want to create an object of Invoice, we need all instance variables. In other words, instantiation of class Invoice depends on all the instance variables inside the class. In the context of Spring, our Invoice class/bean has six dependencies: name, amount, items, vendor, and taxDetails. Now, let’s talk about different types of dependencies that Spring Container can inject.

What is a dependent bean in Spring?

When we talk about Spring Dependency Injection, it becomes important to know about dependent bean in Spring. Let’s look into the previous example of Invoice class. When we define the Invoice class as a bean in spring, since it has dependencies, it will be called as dependent bean.

Which type of dependencies can a Spring Container Inject?
Spring container can inject dependencies if the dependent variable’s data type is any one of the following.

1) Primitive Type  2) Collection Type  3) Reference Type (User defined Type)

Based on the data type of the instance variables, we have three types of dependencies.

Primitive Type Dependency 
If a variable has its data type from the below list, then the dependency is Primitive Type Dependency. Please note that String is also considered as a primitive type in Spring.

byte, short, int, long, float, double, boolean, char, String



Collection Type Dependency 
If a variable has its data type from the below list, then the dependency is Collection Type Dependency. Please note that the Properties is a class, whereas all others are interface. They come under java.util package.

List, Set, Map, Properties


Reference Type Dependency 
If a variable is created using a class or an interface as its data type, then the dependency is Reference Type Dependency. In real time Reference Type Dependency is the most commonly used term for dependency. In our example above, variables vendor and taxDetails come under this dependency.

What is an Object Dependency?
When an object depends on another object(s), we call it object dependency. In fact, this is same as Reference Type Dependency as described above. In the context of Spring framework, when a bean depends on another bean(s), we also call it bean dependency. Moreover, the parent bean is called the dependent bean and the instance variables inside the bean are called dependencies.



What is Injection in the term Spring Dependency Injection? 
As the word ‘injection’ suggests, inserting something into another thing. In fact, injection is the process of inserting values to variables while creating an object. If we talk about objects, the process of inserting one object into another object is called injection. In this case variable will be a reference type variable.

What is Spring Dependency Injection (DI)?
Spring Dependency Injection (DI) is the feature of the core module of the Spring Framework, where the Spring Container injects other objects into an object. Here, other objects are nothing but dependencies (instance variables) which are declared in the class (Spring Bean).


How many ways to implement Spring Dependency Injection?
There are two ways to implement dependency injection:

1) XML Based

2) Java Annotation Based

In this article, we will focus on Java Annotation-based Configuration, which is the most advanced, easy to implement and used widely across the software industry.


What is Autowiring?
Spring Container detects the relationship between the beans either by reading the XML Configuration file or by scanning the Java annotations at the time of booting up the application. Further, it will create the objects & wire the dependencies. Since Spring Container does this process automatically, it is referred to as Autowiring i.e., Automatic Wiring. Developer only needs to provide @Autowired annotation at a specific place.

Autowire Disambiguation
If more than one bean of the same type is available in the container, the framework will throw a NoUniqueBeanDefinitionException, indicating that more than one bean is available for autowiring. This is referred to as autowire Disambiguation. To resolve this conflict, we need to tell Spring explicitly which bean we want to inject. In order to handle this situation, we make use of @Primary or @Qualifier annotations along with @Autowired annotation. The complete detail is provided in a separate article on How to use @Qualifier and @Primary Annotations?.

We can use autowiring on properties, setters, and constructors. Hence, there are three types of dependency injection.

What are the types of Spring Dependency Injection? 
Setter Based Injection
When Spring Container provides values to variables using setter method, we call it Setter Injection. In this case, Spring Container uses the default constructor to create the object. When the annotation @Autowired is used on top of the class’s setter method, it is known as Setter based Dependency Injection.

@Component 
public class Vendor{
   int code; 
   String name;
} 

public class Invoice{
   
   private Vendor vendor;

   @Autowired
   public void setVendor(Vendor vendor){
      this.vendor=vendor;
   }
}
As shown in the example above, Vendor class has been created using @Component annotation. Hence, Spring container will automatically detect it while starting up the application and will create a bean object for Vendor class. Further, when Spring container detects that the Vendor bean object has been Autowired (using @Autowired) into Invoice class setVendor() method, the bean object created for Vendor will be injected into Invoice class via setter method.

Constructor Based Injection 
When Spring Container provides values to variables using parameterized constructor, we call it Constructor Injection. Please note that Spring Container uses parameterized constructor to create the object in this case. When the annotation @Autowired is used on top of the class constructor, it is known as Constructor-based Dependency Injection.


@Component 
public class Vendor{  
   int code;   
   String name;
} 

public class Invoice{

   private Vendor vendor;

   @Autowired
   public Invoice(Vendor vendor){
      this.vendor=vendor;
   } 
}

As shown in the example above, Vendor class has been created using @Component annotation. Hence, Spring container will automatically detect it while starting up the application and will create a bean object for Vendor class. Further, when Spring container detects that the Vendor bean object has been Autowired (using @Autowired) into Invoice class constructor, the bean object created for Vendor will be injected into Invoice class via constructor.

Field or Property Based Injection 
When the annotation @Autowired is used on top of the field or property in the class, it is known as Field-based Dependency Injection.

@Component 
public class Vendor{  
   int code;   
   String name;
} 

public class Invoice{

  @Autowired
  private Vendor vendor;
}

Similar to above both Dependency Injection, here the Autowiring takes place by means of the field ‘vendor’. The bean object created for Vendor class will be Autowired & injected via the field, ‘vendor’ in Invoice class.

In this case, while creating the Invoice object, if there’s no constructor or setter method is available to inject the Vendor bean, the container will use reflection to inject Vendor into Invoice.

When to use @Autowired(required = false) ?
Suppose a bean is not registered with Spring Container and we try to apply the @Autowired annotation on it, then Spring will throw an exception because the required dependency is missing. In this case, we can use @Autowired(required = false) to tell Spring Container that this dependency is optional. On doing so, Spring will not throw any exception of the missing dependency.

How to inject Java Collections as a Dependency in Spring?
As aforementioned, we can use List, Set, Map and Properties as an example of the dependency injection.

Let’s create an example bean class:

public class ListCollectionBean { 

   @Autowired 
   private List<String> listOfLanguages; 
   public void printLanguageList() {
       System.out.println(listOfLanguages); 
   } 
}

Now, let’s register the ListCollectionBean in the configuration setup class:

@Configuration 
public class ListCollectionConfig { 
   
   @Bean 
   public ListCollectionBean getListCollectionBean() {
      return new ListCollectionBean(); 
   }
   
   @Bean 
   public List<String> listOfLanguages() {
      return List.of("Java", "Scala", "Python"); 
   } 
}
Similarly, we can inject any type of Collection as a dependency.

How to inject bean/object references as an element of a Collection in Spring?
In the above example, we have seen the injection of a Collection where elements are String. Here in this section we will look at a collection where elements of the collection will be bean/object references.

Let’s create a bean:

public class MyTestBean { 

    private String name; 
 
    public MyTestBean(String Name){
       this.name=name;
    } 
}
And add a List of MyTestBean as a property to the MyCollectionBean class as below:

public class MyCollectionBean {
   
   @Autowired(required = false) 
   private List<MyTestBean> beanList; 
   
   public void printBeanList() { 
      System.out.println(beanList); 
   } 
}
Now, let’s create a configuration class and create some beans of type MyTestBean as below:

@Configuration 
public class CollectionConfig { 
   
   @Bean 
   public MyTestBean getFirstObject() {
      return new MyTestBean("bean1"); 
   } 
   
   @Bean 
   public MyTestBean getSecondObject() {
      return new MyTestBean("bean2"); 
   } 
  
   @Bean 
   public MyTestBean getThirdObject() {
      return new MyTestBean("bean3"); 
   } 
   // other methods 
}
Here, Spring Container will inject the specific beans of the MyTestBean type into the collection. Further, on invoking the printBeanList() method, the output will show the bean names as list elements: [bean1, bean2, bean3].

If you want Spring container to inject the beans in a particular order, you can use @Order annotation with index. For example, below code demonstrates the concept. In order to have complete understanding of @Order annotation, kindly visit a separate article on the @Order annotation of Spring Annotations.

@Configuration 
public class CollectionConfig { 
 
   @Bean 
   @Order(1)
   public MyTestBean getFirstObject() {
      return new MyTestBean("bean1"); 
   } 
 
   @Bean 
   @Order(0)
   public MyTestBean getSecondObject() {
      return new MyTestBean("bean2"); 
   } 
 
   @Bean  
   @Order(2)  
   public MyTestBean getThirdObject() {
      return new MyTestBean("bean3"); 
   } 
}

As per the @Order annotation shown above, the injection order will be: bean2, bean1, bean3

What are the benefits of Dependency Injection?
1) Most importantly, the dependency Injection helps in achieving loose coupling between objects.

2) As the dependency between objects becomes loosely coupled, it helps the developers to test the module by injecting the dependent Mock Objects (for example, making use of Spring Mockito).

3) When the objects are independent of each other & can be injected, the scope of code reusability increases.

4) It makes code more readable. We don’t have to look through all the code to see what dependencies we need to satisfy for a given component. They are all visible in the interface.

What are the disadvantages of Dependency Injection?
1) As the Dependency Injection offers loose coupling, it concludes increasing the number of classes & interfaces.

2) While implementing the Dependency Injection, Spring container takes the control rather than the developer. Hence, it becomes difficult for the developers to learn how things work in the background & also to have customization.