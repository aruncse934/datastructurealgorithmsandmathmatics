Q1: What is Caching?  
Add to PDF Entry 
Q2: Is Redis just a cache?  Related To: Redis
Add to PDF Junior 
Q3: What is Resultset Caching?  
Add to PDF Junior 
Q4: What is Cache Invalidation?  
Add to PDF Junior 
Q5: What usually should be cached?  
Add to PDF Junior 
Q6: Name some Cache Writing Strategies  
Add to PDF Junior 
Q7: What are some alternatives to Cache Invalidation?  
 Add to PDF Mid 
Q8: Name some Cache Invalidation methods  
 Add to PDF Mid 
Q9: Compare caching at Business Layer vs Caching at Data Layer  
 Add to PDF Senior 
Q10: What are Cache Replacement (or Eviction Policy) algorithms?  
 Add to PDF Senior 
Q11: What are some disadvantages of Cache Invalidation?   
 Add to PDF Senior 
Q12: Why is Cache Invalidation considered difficult?  
 Add to PDF Senior 
Q13: What is the difference between Cache replacement vs Cache invalidation?  
 Add to PDF Senior 
Q14: Explain what is Cache Stampede  Related To: Software Architecture
 Add to PDF Senior 
Q15: When to use LRU vs LFU Cache Replacement algorithms?  
 Add to PDF Expert 
Q16: What are best practices for caching paginated results whose ordering/properties can change?  Related To: Software Architecture
 Add to PDF Expert 
Q17: Cache miss-storm: Dealing with concurrency when caching invalidates for high-traffic sites  Related To: Software Architecture
 Add to PDF Expert 
Q18: Name some Cache Stampede mitigation techniques  