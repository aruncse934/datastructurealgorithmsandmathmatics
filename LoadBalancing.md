Q1: What Is Load Balancing?  Related To: Software Architecture
Add to PDF Entry 
Q2: What Is Round-Robin Load Balancing?  
Add to PDF Junior 
Q3: Name some advantages of Round-Robin Load Balancing  
Add to PDF Junior 
Q4: What Is Sticky Session Load Balancing? What Do You Mean By "Session Affinity"?  Related To: Software Architecture
 Add to PDF Mid 
Q5: What Is Load Balancing Fail Over?  Related To: Software Architecture
 Add to PDF Mid 
Q6: Why should we use Load Balancer (except preventing overloading)?  
 Add to PDF Mid 
Q7: What is the difference between Session Affinity and Sticky Session?  
 Add to PDF Mid 
Q8: What are some variants of Round-Robin Load Balancing algorithm?  
 Add to PDF Mid 
Q9: What is the Difference Between Weighted Load Balancing vs Round Robin Load Balancing?  
 Add to PDF Mid 
Q10: What Is a TCP Load Balancer?  
 Add to PDF Mid 
Q11: Explain what is Reverse Proxy Server?  Related To: Networking
 Add to PDF Mid 
Q12: What Is IP Address Affinity Technique For Load Balancing?  Related To: Software Architecture
 Add to PDF Senior 
Q13: Name some metrics for traffic routing  
 Add to PDF Senior 
Q14: What is the different between Layer7 vs Layer4 load balancing?  
 Add to PDF Senior 
Q15: What are the Pros and Cons of the "sticky session" load balancing strategy?  
 Add to PDF Senior 
Q16: What affect does SSL have on the way load balancing works?  
 Add to PDF Senior 
Q17: When to choose Round-Robin load‑balancing method?  
 Add to PDF Senior 
Q18: What are some Load Balancing Algorithms you know?  
 Add to PDF Senior 
Q19: What Are The Issues With Sticky Session?  
 Add to PDF Senior 
Q20: What Is a UDP Load Balancer?  
 Add to PDF Senior 
Q21: Explain what is “Power of Two Random Choices” Load Balancing?  
 Add to PDF Expert 
Q22: Compare UDP Load Balancer vs TCP Load Balancer  