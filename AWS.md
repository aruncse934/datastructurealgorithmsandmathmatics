AWS Architect::-
-------------------------------------

1. Fundamental Building Blocks of AWS
--------------------------------------
 #. An Overview of AWS
 ---------------------------
  An Overview of AWS
This section identifies the core AWS areas.

When you think about Amazon Web Services (AWS) think IaaS & PaaS. AWS offers both.

IaaS allows organizations to utilize AWS instead of owning and operating your own datacenter. You can simply rent VMs or physical servers from AWS.

The Platform as a Service (PaaS) on the other hand removes the need for your organization to manage the underlying platforms like a database, streaming services, etc.

This allows you to focus on the deployment and management of your core applications and not worry about the IaaS and PaaS layers.

It, in turn, gets organizations to be more efficient and focused as you don’t need to worry about resource procurement, capacity planning, software maintenance, patching, or any of the other undifferentiated heavy lifting involved in running your application.

The Amazon Web Services Infrastructure consists of 4 primary areas which are a combination of IaaS & PaaS :-

 1.Compute (EC2, LightSail, ECS, Lambda, Batch)
 2.Storage (EBS, EFS, S3, Glacier, Storage Gateway, Storage Migration Services)
 3.Database (RDS, Redshift)
 4.Network (CloudFront, VPC, Direct Connect, Load Balancing, Route 53)
In this course we will go into the details of the various services that would fall into one of these 4 main buckets.

Compute


Compute(EC2): This is where you create/deploy your own virtual machine. At AWS you have a wide variety of compute instances you can choose from. This ranges from the type of operating system you would choose to the RAM or CPU you would want your compute instance to have.



Elastic Container Services(ECS): Are used to run and manage your Docker containers. Think about this as something like a managed Kubernetes service.



Light Sail (VPS Service – Virtual Private service): AWS Lightsail launches virtual private servers, which are VMs with individual operating systems but restricted access to physical server resources.



Lambda: Is where you upload a function on to AWS and you pay every time the function is executed or called.

You do not need to think about managing the OS or the VM. Lambda does it all for you.

e.g. Think about a Tax calculator. In the traditional model, you would write the code. Procure a virtual machine to deploy your code to the VM. Then you need to maintain, manage your deployment. However, with Lambda you don’t need to pay or manage the VM, Amazon does this for you.



Batch: Batch computing is used for Batch processing. AWS Batch dynamically provisions the optimal quantity and type of compute resources (e.g., CPU vs memory optimized instances) based on the volume and specific resource requirements of the batch jobs submitted. It also manages the batch process, i.e re-starting jobs that fail, scheduling jobs, etc,

Storage


Simple Storage Service (S3). It is an Object / Bucket type of storage.



Network Attached Storage (EFS): In the Elastic file System you upload your files to an EFS and then mount that on to multiple virtual machines



Glacier: Is a storage service used for Data archival. Primarily used to store data that you do not need to use right away. The cost of storage on Glacier is significantly lower.

It takes 3 – 5 hours to restore from Glacier. Costs - $0.01 per gigabyte, per month.



Snow Ball: Is used to transport a large amount of data on to AWS or to take it out of AWS and move it to your data center.

AWS ships you a hardware device that you plug into your data center and then upload your encrypted data on to the snowball (Hardware device). You will then ship it to AWS and they would upload your data on to AWS. This way you do not need to move your data through the internet as it might take months to move petabytes of data on to AWS.



Storage Gateway: VM you install in your data center and this replicates data Back into S3. This is used when you have an on-premises data-center and would like to replicate the data on to AWS. Once you have a Storage gateway setup you can replicate to S3 on AWS.

Database:-
1.RDS – Relational Database Service
2.Aurora
3.DynamoDB
4.Neptune
5.ElasticCache


Relational Database Service (RDS): Provides cost-efficient and resizable capacity while managing time-consuming database administration tasks, freeing you to focus on your applications and business.

Amazon RDS gives you access to several familiar database engines, including Amazon Aurora, MySQL, PostgreSQL, MariaDB, Oracle, and SQL Server. This means that the code, applications, and tools you already use with your existing databases can be used with RDS.

RDS automatically patches the database software and backs up your database, storing the backups for a user-defined retention period and enabling point-in-time recovery.

Aurora is an AWS proprietary relational Database that is compatible with MySQL and PostgreSQL it combines the performance and availability of high-end commercial databases with the simplicity and cost-effectiveness of open source databases.



DynamoDB is a nonrelational database i.e a NoSQL DB. It is a Managed Service, i.e you do not need to tune or manage it in any way, AWS does this for you.

Neptune is a fully managed graph database. Relationships are first-class citizens in graph databases, and most of the value of graph databases is derived from these relationships. Graph databases use nodes to store data entities, and edges to store relationships between entities

A graph in a graph database can be traversed along specific edge types or across the entire graph. In graph databases, traversing the joins or relationships is very fast because the relationships between nodes are not calculated at query times but are persisted in the database. Graph databases have advantages for use cases such as social networking, recommendation engines, and fraud detection, when you need to create relationships between data and quickly query these relationships.



ElasticCache offers fully managed Redis and Memcached. Seamlessly deploy, operate, and scale popular open source compatible in-memory data stores.

Login to save progress


#Security, Identity & Compliance
#Application Integration
#Availability Zones & Regions
----------------------------------------------------------
2. AWS: Miscellaneous Services
---------------------------------------------------------
#Media Services
#Machine Learning Services
#Management Tools
#Migration Services
#Customer Engagement & Business Productivity Services
#Mobile Services
#Developer Tools
----------------------------------------------------------------
##3.AWS: Identity & Access Management (IAM)
-----------------------------------------------------------------
 1. Identity Access Management (IAM)
 -----------------------------------
 
IAM Essentials

IAM Policies

IAM Users

IAM Groups

IAM Roles

Security Token Service

IAM API Keys

AWS Key Management Service (KMS)

AWS Inspector

Cognito Essentials

Identity Federation and Amazon Cognito











------------------------------------------------------------------
4.AWS: Compute Offerings
------------------------------------------------------------------
#EC2 - Elastic Compute Cloud
------------------------------
EC2 Fundamentals

EC2 Purchasing Options

EC2 Instance Configuration

EC2 Storage Basics

EC2 Key Pairs

Elastic Load Balancers and Session State

EC2 API Actions/Errors and the AWS Shared Responsibility Model


#Elastic Load Balancers (ELB)
#Lambda - Serverless Architecture
#Elastic Container Service (ECS)
---------------------------------------------------------------------
5. AWS: Storage
---------------------------------------------------------------------
#S3 - Simple Storage Service
--------------------------------------------------------------------
S3 - Simple Storage Service
AWS S3 - Simple Storage Service what is it? and where would you use it? Also take a look at an architecture using S3.

Fundamentally there are two types of storage:-

1.Object-Based Storage
2.Block Based Storage

Simple Storage Service (S3) - Object-Based Storage
Provides developers and IT Teams with secure durable, highly scalable object storage. Easy to use simple web interface to store and retire any amount of date from anywhere on the web.

It is a place to store your files on the AWS cloud Dropbox was born. By simplifying the User interface of S3. Think about Dropbox as a layer built on top of S3.

Data is spread across multiple devices and facilities

Think about S3 to store your photos or files.

1.Object Base Storage
2.Unlimited Storage
3.Files are Stored in Buckets/Folders
4.Names must be unique globally
5.Every time you have a successful upload you get a http 200 code back

S3 Is primarily used for:-

1.Store and Backup
2.Application File Hosting
3.Media Hosting
4.Software Delivery
5.Storing AMI’s and Snapshots

Data consistency Model – S3
Read after write consistency for PUTS of new objects Eventual consistency for overwrite PUTS and deletes (i.e. takes time to propagate)

Objects Consists of the following

1.Key – this is simply the file name of the object
2.Value – the data and it is made up of a sequence of bytes.
3.Versioning – which version of the object is this
4.Meta Data: Data about data, the data about the data file you are storing.

Think, if you are storing a music track/song. This would have metadata like the information of the singer, the year it was released, the name of the album etc.

Sub resources –

1.Access Control list – this determines was can access the file on S3. This can be done at the file level or at the Bucket level.
2.Torrent – supports the Bit torrent protocol
3.Built for 99.99% availability of the S3 Amazon SLA 99.9% platform
4.Durability guarantee – 99.9… (11.9s) (more)?
5.Tiered storage Availability
6.Lifecycle management
7.Versioning
8.Encryption
9.Secure the data using Access control lists and Bucket policies

S3 – IA (Infrequently Accessed) For data that is accessed less frequently but requires rapid access when needed. This here costs lesser than S3 but you are charged for the retrieval of the data.

S3 – RRS (Reduced Redundancy Storage) Basically less durability with the same level of availability.

e.g. This about data you could potentially regenerate like a tax calculation or a payslip. This is cheaper than the original SS. Suppose you create thumbnails for all your pictures. If you lose a thumbnail you could always regenerate it.

When deciding which storage to use think about the various storage options, their advantages vs disadvantages. Are you optimizing for durability, the frequency of retrieval or availability?

Image comparing the various types of S3
widget
![alt text](https://www.educative.io/api/collection/5081119082938368/5748755743637504/page/5737979670691840/image/5651874166341632.png)


Charging Model

1.Storage
2.Number of requests
3.Storage Management Pricing
4.Add metadata to see usage metrics.

Transfer Acceleration - Enables fast, easy and secure transfers of your files over long distances between your end users and an S3 bucket.

Transfer acceleration takes advantages of Amazon cloud front’s globally distributed edge locations. As the data arrives at an edge location, the data is routed to Amazon S3 over an optimized network path.

Think of transfer acceleration as a combination of S3 + CDN natively supported by this Service. Basically, every user ends up going through the closest possible edge location which in turn talks to the actual S3 bucket.

Recap - S3
S3 Storage Classes

1.S3 (Durable, immediately available and frequently accessed)
2.S3 – IA (durable, immediately available, infrequently accessed.
3.S3 Reduced Redundancy Storage (Used for data that is easily reproducible, such thumbnails)

Core fundamentals of S3 objects

1.Key: Name of the object These are store in alphabetic order
2.Value: The data itself Version ID: The version of the object
3.Meta Data: The various attributes of the data

Sub resources

1.ACL: Access control lists
2.Torrent: bit Torrent protocol

Cross region Replication

This basically means that if you have this turned on then for a bucket AWS will automatically make a bucket available across 2 or more regions.

Example of an Amazon S3 Hosted Website Architecture

![alt text](https://www.educative.io/api/collection/5081119082938368/5748755743637504/page/5737979670691840/image/5769928858664960.png)


Securing your S3 Buckets

1.By default, all buckets are private
2.You can set up access control to your using
3.Bucket Policies
4.Access control lists (ACL)
5.S3 buckets can be configured to create access logs

Encryption for S3

1.In-transit
2.SSL/TLS (using HTTPS)
3.At Rest
4.Server Side Encryption
5.S3 Managed keys – SSE-S3
6.Server side Encryption
7.Key Management Service – Managed Key
8.SST – KMS
9.Client Side Encryption

---------------------------------------------------------------------
#Elastic Block Storage (EBS)
#Elastic File System (EFS)
#Block Storage vs File storage
#Storage Gateway
#Snowball
---------------------------------------------------------------------
6. AWS: Database Offerings
---------------------------------------------------------------------
#Generic Database 101
#AWS Database Types
#RDS
#Dynamo DB
----------------------------------------------------------------------
Dynamo DB
Dynamo DB where would you use it and how it works.

Dynamo DB
Is a fast and flexible NoSQL DB for applications that need consistent single-digit millisecond intently at any scale. It is a fully managed database and supports both document and key value data models.

Dynamo DB Characteristics

1.Stored on SSD
2.Spread Across 3 geographically distinct data centers
3.Eventual read consistency by default
4.Strongly consistent reads

Eventual Consistent Reads : Consistency across all copies of data is usually reached within a second. Performing a read after a short duration should return updated data.

Strongly Consistent Reads: A strongly consistent read returns as a result that reflects all writes that received a successful response prior to that read.

Dynamo DS pricing:
1.Write through put $0.0065 per hour for every 10 units.

2.Read throughput $0.0065 per hour for every 50 units.

3.Storage cost of $0.25 GB per month

DynamoDB Data Model

![alt text](https://www.educative.io/api/collection/5081119082938368/5748755743637504/page/5118147170402304/image/5728757302165504.png)

![alt text](https://www.educative.io/api/collection/5081119082938368/5748755743637504/page/5118147170402304/image/5083670394175488.png)


Simple DynamoDB Architecture

![alt text](https://www.educative.io/api/collection/5081119082938368/5748755743637504/page/5118147170402304/image/5696459148099584.png)

----------------------------------------------------------------------
#Red Shift
#Aurora
#ElastiCache
#Neptune
-----------------------------------------------------------------------
7. AWS: Routing
-----------------------------------------------------------------------
#DNS Basics
#Route 53
#Direct Connect
#CloudFront: Content Delivery Network (CDN)
-----------------------------------------------------------------------
8. AWS: Virtual Private Cloud (VPC)
-----------------------------------------------------------------------
Virtual Private Cloud (VPC)
VPC- Peering
-----------------------------------------------------------------------
9. AWS: Application Services
-----------------------------------------------------------------------
#Simple Queue Service (SQS)
#Simple Workflow Service (SWF)
------------------------------------------------------------------------
10. AWS Well Architected Framework
------------------------------------------------------------------------
#General Design Principles
#Well Architected Framework: An Overview
#Well Architected Framework: Security
#Well Architected Framework: Cost Optimization
#Well Architected Framework: Reliability
#Well Architected Framework: Performance Efficiency
#Well Architected Framework: Operational Excellence
--------------------------------------------
1: What Is Amazon EC2?
EC2 is short for Elastic Compute Cloud, and it provides scalable computing capacity. Using Amazon EC2 eliminates the need to invest in hardware, leading to faster development and deployment of applications. You can use Amazon EC2 to launch as many or as few virtual servers as needed, configure security and networking, and manage storage. It can scale up or down to handle changes in requirements, reducing the need to forecast traffic. EC2 provides virtual computing environments called “instances.”

2: What Are Some of the Security Best Practices for Amazon EC2?
Security best practices for Amazon EC2 include using Identity and Access Management (IAM) to control access to AWS resources; restricting access by only allowing trusted hosts or networks to access ports on an instance; only opening up those permissions you require, and disabling password-based logins for instances launched from your AMI.

3: What Is Amazon S3? 
S3 is short for Simple Storage Service, and Amazon S3 is the most supported storage platform available. S3 is object storage that can store and retrieve any amount of data from anywhere. Despite that versatility, it is practically unlimited as well as cost-effective because it is storage available on demand. In addition to these benefits, it offers unprecedented levels of durability and availability. Amazon S3 helps to manage data for cost optimization, access control, and compliance. 

4: Can S3 Be Used with EC2 Instances, and If Yes, How?
Amazon S3 can be used for instances with root devices backed by local instance storage. That way, developers have access to the same highly scalable, reliable, fast, inexpensive data storage infrastructure that Amazon uses to run its own global network of websites. To execute systems in the Amazon EC2 environment, developers load Amazon Machine Images (AMIs) into Amazon S3 and then move them between Amazon S3 and Amazon EC2.
Amazon EC2 and Amazon S3 are two of the best-known web services that make up AWS.



5: What Is Identity Access Management (IAM) and How Is It Used?
Identity Access Management (IAM) is a web service for securely controlling access to AWS services. IAM lets you manage users, security credentials such as access keys, and permissions that control which AWS resources users and applications can access.

6: What Is Amazon Virtual Private Cloud (VPC) and Why Is It Used?
A VPC is the best way of connecting to your cloud resources from your own data center. Once you connect your datacenter to the VPC in which your instances are present, each instance is assigned a private IP address that can be accessed from your data center. That way, you can access your public cloud resources as if they were on your own private network.

AWS Solutions Architect Certification

7: What Is Amazon Route 53?
Amazon Route 53 is a scalable and highly available Domain Name System (DNS). The name refers to TCP or UDP port 53, where DNS server requests are addressed.

8: What Is Cloudtrail and How Do Cloudtrail and Route 53 Work Together? 
CloudTrail is a service that captures information about every request sent to the Amazon Route 53 API by an AWS account, including requests that are sent by IAM users. CloudTrail saves log files of these requests to an Amazon S3 bucket. CloudTrail captures information about all requests. You can use information in the CloudTrail log files to determine which requests were sent to Amazon Route 53, the IP address that the request was sent from, who sent the request, when it was sent, and more.

9: When Would You Prefer Provisioned IOPS over Standard Rds Storage?
You would use Provisioned IOPS when you have batch-oriented workloads. Provisioned IOPS delivers high IO rates, but it is also expensive. However, batch processing workloads do not require manual intervention. 

10: How Do Amazon Rds, Dynamodb and Redshift Differ from Each Other?
Amazon RDS is a database management service for relational databases. It manages patching, upgrading and data backups automatically. It’s a database management service for structured data only. On the other hand, DynamoDB is a NoSQL database service for dealing with unstructured data. Redshift is a data warehouse product used in data analysis.

11: What Are the Benefits of AWS’s Disaster Recovery?
Businesses use cloud computing in part to enable faster disaster recovery of critical IT systems without the cost of a second physical site. The AWS cloud supports many popular disaster recovery architectures ranging from small customer workload data center failures to environments that enable rapid failover at scale. With data centers all over the world, AWS provides a set of cloud-based disaster recovery services that enable rapid recovery of your IT infrastructure and data.



----------------------------------------------------------------------------------------------------
#Cloud Architecture: A Guide To Design & Architect Your Cloud
-------------------------------------------------------------
1. Introduction
-------------------------------------------------------------
#Why Move to the Cloud Now ?
----------------------------
Why Move to the Cloud Now ?
This section of the course will go into the fundamentals of why there is a need to move to the cloud for software development manager (SDM), Technical Program Manager (TPM), IT-Manager, directors.

We are at an interesting point in time where the way we use and think about infrastructure is changing. The move to the cloud has been talked about for the last 10 years or so but I believe we are at an interesting tipping point. The reason for this is that there has been a rapid growth in the maturity of cloud infrastructure available and of course the cost of being on the cloud.

When we talk here about infrastructure it is important to understand that IaaS is made up of hardware like switches and servers but also there is a layer of software that helps users to deploy, manage and maintain the infrastructure that is built but the cloud provider. Though the primary capabilities are similar, advanced capabilities differ from provider to provider i.e AWS vs Azure.

Over the last couple of years the core components of infrastructure have advanced in terms of capabilities and from an operational perspective teams have learnt how to operate in an Agile way. This has in turn made leaders within the IT and technology space to take a second look at moving to the cloud.

If you take a step back and look at what organizations have been doing; they have been procuring hardware, racking servers, managing and maintaining their hardware infrastructure. There is a tremendous amount of manpower required and manpower translates to cost. Also the cost of the acquired hardware translates into hardware depreciating over the predetermined life of the hardware.

The cost of procurement and maintenance aspect can be minimized when you move from an on-premises datacenter setup to using the cloud. This also helps companies focus more on their core business rather than spending time and resources working on managing their infrastructure that does not add a lot of business value to its customers. On the other hand moving to the cloud helps organizations build reliable, scalable, and durable applications.

One of the fundamental benefits of the cloud is the opportunity to swap upfront capital infrastructure cost with low variable costs that scale with growth. With the cloud, you no longer need to plan and procure servers and other IT infrastructure equipment weeks or months in advance. Instead, infrastructure can be instantly spun up in minutes. This is something that can no longer be overlooked. Think about the retail industry that has certain days in the year like black friday or boxing day where traffic to websites go up by 5x. If you are a retail company like Target or Best Buy you would need to buy 5x more hardware just to accommodate the spike after which that hardware is no longer necessary.

Moving to the cloud is not all rosy, it comes with it own issues like the initial cost of moving to the cloud, having the right type of expertise to run your applications in the cloud and your security posture in general. Sometimes customers who move to the cloud feel that a lot of work has been put into moving to the cloud but the outcome of moving to the cloud is not very tangible from a end user perspective.

In this course you will learn about the fundamentals of why there is the need now more than ever to move to the cloud. The reason why moving to the cloud makes sense, how do legacy applications move to the cloud and the various types of approaches to move to the cloud. You will learn terminologies that you would need to fundamentally understand when we talk about the cloud. We will move on to talking about what cloud native designs i.e building applications for the cloud. As we learn about Cloud native architecture we will get you a quick primer on microservices and their importance and also touch a little on Kubernetes and understand the part it plays.

You will get an overview of the various types of cloud deployment strategies keeping in mind Operations, Security, Redundancy and Scaling.

We will be using the AWS cloud of most of the examples in this course. The reason for choosing Amazon AWS for this course is that AWS is probably the more mature than most of its competition. It also has a wider range of adoption in the industry and is easier to access for you as an individual.

Who is this course for ?
This course is primarily designed for managers and leaders in IT or Tech organizations. You could be a development manager (SDM), Technical Program Manager (TPM), IT-Manager, director or you could be someone in tech who just wants to learn about the cloud.

The course aims to give you a good primer on cloud design patterns, cloud terminology, cloud components cloud best practices practices and operating on the cloud. This is to ensure that you get to know all you need to know when your organization moves to the cloud or when you start interacting with a cloud team. It will also help people who are looking to work for cloud providers like AWS or Azure or any other SaaS service ask the right type of questions.

If you are a leader in the tech industry we are sure you are seeing the need to know of the fundamentals of the cloud now more than ever. More than 60% jobs require some sort of cloud exposure. Some places don’t even call this out anymore as they believe that most of the talent already know of the fundamentals. The job market is evolving driven by the evolution of organizations.

Why Cloud ?
If you think about every successful tech company - Facebook, Amazon, Netflix, Google have been able to grow and scale the way they have only because of the scalability and agility that cloud offers. By choosing to run your infrastructure on the cloud you are able to provide value to your customer at a quicker pace. Your teams do not need to wait for provisioning i.e ordering, waiting and racking servers which in most places takes a couple of weeks at the very least. An idea today could be deployed as soon as possible with less time spent waiting for infrastructure.

Also consider the rapid growth of startups. The startup space has exploded over the last 8 years because there is no longer a large cost to acquire infrastructure to build and launch your applications. And supposing an app have viral growth then the fact that you are on the cloud automatically takes care of the infrastructure aspects of scale.
-----------------------------------------------------------------------

#Cloud Computing Models & Their Evolution
----------------------------------------------------------------
2. Cloud Principles
-------------------------------------------------------------------
#Let's Start With Some Basics
#Understanding Amazon Web Services (AWS)
#Introduction to Cloud Native Principles
----------------------------------------------------------------------
#Horizontal Scaling vs Vertical Scaling & Multi-Tenancy
--------------------------------------------------------
Horizontal Scaling vs Vertical Scaling & Multi-Tenancy
In this section we will learn about various Cloud Concepts:- a) Scaling on the cloud b) Degradation of Services c) Availability Vs Durability on the Cloud d) Single & Multi-tenancy Applications e) Types of Cloud Deployments

In this section we will learn various Cloud Concepts.

1.Scaling on the cloud
2.Degradation of Services
3.Availability Vs Durability on the Cloud
4.Single & Multi-tenancy Applications
5.Types of Cloud Deployments

Scaling Out / Horizontal Scaling vs Scaling up/ Vertical Scaling
Horizontal scaling means that you scale by adding more machines into your pool of resources. Eg. As traffic goes up you add more web servers to take on the traffic.

Vertical scaling means that you scale by adding more power (CPU, RAM) to an existing machine.

On the Cloud you Scale horizontally !

Graceful Degradation of Services
Applications need to have a way to handle excessive load and handle failure gracefully no matter if it’s the application or a dependent service under load.

The Site Reliability Engineering handbook describes graceful degradation in applications as offering “responses that are not as accurate as or that contain less data than normal responses, but that are easier to compute” when under excessive load.

Modern applications are build with breakers that do not overload other microservices if they malfunction. They also need to have sufficient telemetry that helps in troubleshooting and help in identifying what is causing a problem.

If you take a moment to think of all the apps on your phone that you use when was the last time it had an outage ? The probability of an outage is so less likely because the system design and architecture of modern applications take into account every single point of failure from an application, database, Load balancer and even the infrastructure.

Graceful failure is about making sure that at the event of failure at any level that there is a backup that takes the responsibility of the component that failed and at the end there is no perceived customer impact. Note the word ‘perceived’, Lets take for example that a large ecommerce / retail website calculates the tax for each shipment before you place the order. If the tax microservice goes down then maybe you do not charge the customer tax for that one transaction the cost will be covered by the organization.

Availability Vs Durability on the Cloud
Availability
Availability and durability are two very different aspects of data accessibility. Most cloud services offer a 99.9999% availability. This varies by provider and by each service that is offered by the provider.

Availability refers to system uptime, i.e. the storage system is operational and can deliver data upon request. Historically, this has been achieved through hardware redundancy so that if any component fails, access to data will prevail.

Durability
Durability, on the other hand, refers to long-term data protection, i.e. the stored data does not suffer from bit rot, degradation or other corruption. Rather than focusing on hardware redundancy, it is concerned with data redundancy so that data is never lost or compromised.

Availability and durability serve different objectives. For data centers, availability/uptime is a key metric for operations as any minute of downtime is costly. The measurement focuses on storage system availability. But what happens when a component, system or even the data center goes down? Will your data be intact when the fault is corrected?

This illustrates the equal importance of data durability. When an availability fault is corrected, it is essential that access to uncorrupted data is restored. With the explosion of data created, the potential of mining, and growing needs for longer retention rates (for everything) you can imagine how this is paramount for business success.

Consider the potential competitive, financial or even legal impact of not being able to retrieve the archived master/reference copy of data. Hence, both data availability and data durability are essential for short- and long-term business success.

Single vs Multi Tenancy Cloud Deployment
Single tenancy or multi-tenancy is in context with SaaS applications. A SaaS application can either choose to be one to the other. Most modern applications choose to be multi-tenant applications.

Multi-tenancy
Multi-tenancy means that a single instance of the software and all of the supporting infrastructure serve multiple end-customers. Each customer shares the software application and also shares a single database. The data is tagged in the database as belonging to one customer or another, and the software is smart enough to know who the data belongs to.

![alt text](https://www.educative.io/api/collection/5081119082938368/5750085036015616/page/5700735861784576/image/5651442522128384.png)

Single tenancy
In a Single tenancy approach a single instance of the software and all of the supporting infrastructure serves a single customer. With single tenancy, each customer has their own independent database and instance of the application software as well. With this option, there’s essentially no sharing going on. Everyone has their own, separate from everyone else.

Think about a SaaS application that is serving customers like coke and pepsi, sometimes these companies would care that they are not sharing the database tier in particular so that there is no possibility for information to leak. If this was a concern then they would go for a Single tenancy rather than a multi tenancy approach.

With single-tenant architectures, the marginal cost of adding new customers never goes down. Maintenance With single-tenant, maintenance is incremental. If you botch it, you typically only take down a single team. Instead of deploying a single app update, you’re deploying N app updates. Instead of migrating one database, you’re migrating N databases. On the surface level it sounds like this would create more work for you, but since the systems are isolated and identical most of that work can be automated. All you need is a bit of tooling to orchestrate the updates.

Resilience
Single-tenant creates resilience at the system level. Outside of DDOS attacks at the DNS level there are very few ways to take down the entire system. A team may experience problems, but it’s unlikely those problems will extend beyond that single instance.

Types of Cloud Deployments
Hybrid cloud is an environment that uses a mix of on-premises, private cloud and /or third-party, public cloud services with orchestration between the two platforms.

Multi-cloud
A multi-cloud strategy is the use of two or more cloud services. While a multi-cloud deployment can refer to any implementation of multiple software as a service (SaaS) or platform as a service (PaaS) cloud offerings, today, it generally refers to a mix of public infrastructure as a service (IaaS) environments, such as Amazon Web Services, Microsoft Azure, Oracle Cloud Infrastructure.

Multi-cloud was, and still is, seen as a way to prevent data loss or downtime due to a localized component failure in the cloud. The ability to avoid vendor lock-in was also an early driver of multi-cloud adoption. A multi-cloud strategy also offers the ability to select different cloud services or features from different providers.

The pros and cons of a multi-cloud strategy
There are several commonly cited advantages to multi-cloud computing, such as the ability to avoid vendor lock-in, the ability to find the optimal cloud service for a particular business or technical need and increased redundancy.

However, there are some potential drawbacks. For example, most public cloud providers offer volume discounts, where prices are reduced as customers buy more of a particular service. It becomes more difficult for an organization to qualify for those discounts when it doesn’t concentrate its business with a single cloud provider.

In addition, a multi-cloud deployment requires an IT staff to have multiple kinds of cloud platform or provider expertise. Workload or application management in multi-cloud computing can also be a challenge, as information moves from one cloud platform to another.

Multi-cloud computing vs. Hybrid cloud computing
Multi-cloud and hybrid cloud computing are similar, but different IT infrastructure models. In general, hybrid cloud refers to a cloud computing environment that uses a mix of an on-premises, private cloud and a third-party, public cloud, with orchestration between the two. An enterprise often adopts hybrid cloud to achieve a specific task, such as the ability to run workloads in house, and then burst into the public cloud when compute demands spike.

However, multi-cloud doesn’t preclude hybrid cloud, and a hybrid cloud could be part of a multi-cloud deployment. The two models are not an either/or situation; it simply depends on what a business hopes to achieve.

![alt text](https://www.educative.io/api/collection/5081119082938368/5750085036015616/page/5700735861784576/image/5170986005561344.png)

The basic benefits of multi-tenant are:
Resilience
Both architectures offer their own form of resilience. Multi-tenant creates resilience at the team level. Each team is serviced by multiple instances, spread across multiple regions/zones, and hosted behind load balancers. A team is unlikely to experience issues unless there is a system-wide outage.

Maintenance
With multi-tenant, deploys are typically all or nothing. Maintenance on multi-tenant systems can be scary. You push out a single update, and every customer is immediately on the new system. If you botch it you take down the entire system.

Easy Upgrades
Instead of a vendor needing to update every instance of their software across a large number of servers, they are able to update a single, central application or codebase and have the changes instantly available to all users. With a multi-tenant application the process for spinning up a new cloud and application on behalf of a new customer is incredibly easy and can be done very quickly.

Easy Customizations
With multi-tenancy based applications you can provide an additional layer to allow for customizations while still maintaining an underlying codebase which remains constant for all users, including of course, all new customers.

Ongoing Cost Savings
Multi-tenancy speeds up upgrades, saves time (and also cost) but in addition the server / cloud requirements for a multi-tenancy application cost much less. The opportunity to save money takes many forms and becomes greater as the application scales up. This reduces the cost of doing business for the vendor and savings can be passed onto customers.

--------------------------------------------------------
#Application Migration Strategies
-----------------------------------------------------------------

3.Operating on the cloud
------------------------------------------------------------------
#What is DevOps ? And why is it important on the Cloud ?
#Operational Excellence on the Cloud
#Continuous Integration & Continuous Delivery
------------------------------------------------------------------
4.Security on the Cloud
------------------------------------------------------------------
#Security In a Multi-tenant Environment
------------------------------------------------------------------
#Security On the Cloud - Design Principles
------------------------------------------------------------------
Security On the Cloud - Design Principles
Learn about the five best practice areas for security in the cloud: a) Identity and Access Management b) Detective Controls c) Infrastructure Protection d) Data Protection e) Incident Response

The security pillar includes the ability to protect information, systems, and assets while delivering business value through risk assessments and mitigation strategies. The security pillar provides an overview of design principles, best practices, and questions

Design Principles
There are six design principles for security in the cloud:

Implement a strong identity foundation:
Implement the principle of least privilege and enforce separation of duties with appropriate authorization for each interaction with your AWS resources. Centralize privilege management and reduce or even eliminate reliance on long term credentials.

Enable traceability:
Monitor, alert, and audit actions and changes to your environment in real time. Integrate logs and metrics with systems to automatically respond and take action.

Apply security at all layers:
Rather than just focusing on protecting a single outer layer, apply a defense-in-depth approach with other security controls. Apply to all layers, for example, edge network, virtual private cloud (VPC), subnet, load balancer, every instance, operating system, and application.

Automate security best practices:
Automated software-based security mechanisms improve your ability to securely scale more rapidly and cost effectively. Create secure architectures, including the implementation of controls that are defined and managed as code in version-controlled templates.

Protect data in transit and at rest:
Classify your data into sensitivity levels and use mechanisms, such as encryption and tokenization where appropriate. Reduce or eliminate direct human access to data to reduce risk of loss or modification.

Prepare for security events:
Prepare for an incident by having an incident management process that aligns to your organizational requirements. Run incident response simulations and use tools with automation to increase your speed for detection, investigation, and recovery.

Definition There are five best practice areas for security in the cloud:
1.Identity and Access Management
2.Detective Controls
3.Infrastructure Protection
4.Data Protection
5.Incident Response

Before you architect any system, you need to put in place practices that influence security. You will want to control who can do what. In addition, you want to be able to identify security incidents, protect your systems and services, and maintain the confidentiality and integrity of data through data protection.

You should have a well-defined and practiced process for responding to security incidents. These tools and techniques are important because they support objectives such as preventing financial loss or complying with regulatory obligations.

The AWS Shared Responsibility Model enables organizations that adopt the cloud to achieve their security and compliance goals. Because AWS physically secures the infrastructure that supports our cloud services, as an AWS customer you can focus on using services to accomplish your goals.

Best Practices Identity and Access Management
Identity and access management are key parts of an information security program, ensuring that only authorized and authenticated users are able to access your resources, and only in a manner that is intended. For example, you should define principals (users, groups, services, and roles that take action in your account), build out policies aligned with these principals, and implement strong credential management.

These privilege-management elements form the core concepts of authentication and authorization. In AWS, privilege management is primarily supported by the AWS Identity and Access Management (IAM) service, which allows you to control user access to AWS services and resources. You should apply granular policies, which assign permissions to a user, group, role, or resource. You also have the ability to require strong password practices, such as complexity level, avoiding re-use, and using multi-factor authentication (MFA).

You can use federation with your existing directory service. For workloads that require systems to have access to AWS, IAM enables secure access through instance profiles, identity federation, and temporary credentials. The following questions focus on identity and access management considerations for security

SEC 1: How are you protecting access to and use of the AWS account root user credentials?

SEC 2: How are you defining roles and responsibilities of system users to control human access to the AWS Management Console and API?

SEC 3: How are you limiting automated access to AWS resources (for example, applications, scripts, and/or third-party tools or services)?

It is critical to keep root user credentials protected, and to this end AWS recommends attaching MFA to the root user and locking the credentials with the MFA in a physically secured location. IAM allows you to create and manage other non-root user permissions, as well as establish access levels to resources.

Detective Controls:
You can use detective controls to identify a potential security incident. These controls are an essential part of governance frameworks and can be used to support a quality process, a legal or compliance obligation, and for threat identification and response efforts. There are different types of detective controls.

For example, conducting an inventory of assets and their detailed attributes promotes more effective decision making (and lifecycle controls) to help establish operational baselines. Or you can use internal auditing, an examination of controls related to information systems, to ensure that practices meet policies and requirements and that you have set the correct automated alerting notifications based on defined conditions. These controls are important reactive factors that can help your organization identify and understand the scope of anomalous activity.

In AWS, you can implement detective controls by processing logs, events, and monitoring that allows for auditing, automated analysis, and alarming. CloudTrail logs, AWS API calls, and CloudWatch provide monitoring of metrics with alarming, and AWS Config provides configuration history. Service-level logs are also available, for example, you can use Amazon Simple Storage Service (Amazon S3) to log access requests. Finally, Amazon Glacier provides a vault lock feature to preserve mission-critical data with compliance controls designed to support auditable long-term retention.

The following question focuses on detective controls considerations for security.

SEC 4: How are you capturing and analyzing logs? Log management is important to a well-architected design for reasons ranging from security or forensics to regulatory or legal requirements.

It is critical that you analyze logs and respond to them so that you can identify potential security incidents. AWS provides functionality that makes log management easier to implement by giving you the ability to define a data-retention lifecycle or define where data will be preserved, archived, or eventually deleted. This makes predictable and reliable data handling simpler and more cost effective.

Infrastructure Protection
Infrastructure protection includes control methodologies, such as defense in depth and MFA, which are necessary to meet best practices and industry or regulatory obligations. Use of these methodologies is critical for successful ongoing operations either in the cloud or on-premises. In AWS, you can implement stateful and stateless packet inspection, either by using AWS-native technologies or by using partner products and services available through the AWS Marketplace.

You should use Amazon Virtual Private Cloud (Amazon VPC) to create a private, secured, and scalable environment in which you can define your topology—including gateways, routing tables, and public and private subnets. The following questions focus on infrastructure protection considerations for security.

Infrastructure protection includes control methodologies, such as defense in depth and MFA, which are necessary to meet best practices and industry or regulatory obligations. Use of these methodologies is critical for successful ongoing operations either in the cloud or on-premises. In AWS, you can implement stateful and stateless packet inspection, either by using AWS-native technologies or by using partner products and services available through the AWS Marketplace.

You should use Amazon Virtual Private Cloud (Amazon VPC) to create a private, secured, and scalable environment in which you can define your topology—including gateways, routing tables, and public and private subnets. The following questions focus on infrastructure protection considerations for security.

SEC 5: How are you enforcing network and host-level boundary protection?

SEC 6: How are you leveraging AWS service-level security features?

SEC 7: How are you protecting the integrity of the operating system?

Multiple layers of defense are advisable in any type of environment. In the case of infrastructure protection, many of the concepts and methods are valid across cloud and on-premises models. Enforcing boundary protection, monitoring points of ingress and egress, and comprehensive logging, monitoring, and alerting are all essential to an effective information security plan.

AWS customers are able to tailor, or harden, the configuration of an Amazon Elastic Compute Cloud (Amazon EC2), Amazon EC2 Container Service (Amazon ECS) container, or AWS Elastic Beanstalk instance, and persist this configuration to an immutable Amazon Machine Image (AMI). Then, whether triggered by Auto Scaling or launched manually, all new virtual servers (instances) launched with this AMI receive the hardened configuration.

Data Protection
Before architecting any system, foundational practices that influence security should be in place. For example, data classification provides a way to categorize organizational data based on levels of sensitivity, and encryption protects data by rendering it unintelligible to unauthorized access. These tools and techniques are important because they support objectives such as preventing financial loss or complying with regulatory obligations.

In AWS, the following practices facilitate protection of data:
As an AWS customer you maintain full control over your data.

AWS makes it easier for you to encrypt your data and manage keys, including regular key rotation, which can be easily automated by AWS or maintained by you. Detailed logging that contains important content, such as file access and changes, is available. AWS has designed storage systems for exceptional resiliency. For example, Amazon S3 is designed for 11 nines of durability. (For example, if you store 10,000 objects with Amazon S3, you can on average expect to incur a loss of a single object once every 10,000,000 years.)

Versioning, which can be part of a larger data lifecycle management process, can protect against accidental overwrites, deletes, and similar harm. AWS never initiates the movement of data between Regions. Content placed in a Region will remain in that Region unless you explicitly enable a feature or leverage a service that provides that functionality. The following questions focus on data protection considerations for security.

SEC 8: How are you classifying your data?

SEC 9: How are you encrypting and protecting your data at rest?

SEC 10: How are you managing keys?

SEC 11: How are you encrypting and protecting your data in transit?

AWS provides multiple means for encrypting data at rest and in transit. AWS build features into our services that make it easier to encrypt your data. For example, AWS has implemented server-side encryption (SSE) for Amazon S3 to make it easier for you to store your data in an encrypted form. You can also arrange for the entire HTTPS encryption and decryption process (generally known as SSL termination) to be handled by Elastic Load Balancing (ELB).

Incident Response
Even with extremely mature preventive and detective controls, your organization should still put processes in place to respond to and mitigate the potential impact of security incidents. The architecture of your workload will strongly affect the ability of your teams to operate effectively during an incident to isolate or contain systems and to restore operations to a known-good state.

Putting in place the tools and access ahead of a security incident, then routinely practicing incident response, will make sure the architecture is updated to accommodate timely investigation and recovery. In AWS, the following practices facilitate effective incident response:

Detailed logging is available that contains important content, such as file access and changes. Events can be automatically processed and trigger scripts that automate runbooks through the use of AWS APIs.

You can pre-provision tooling and a “clean room” using AWS CloudFormation. This allows you to carry out forensics in a safe, isolated environment. The following question focuses on incident response considerations for security

SEC 12: How do you ensure that you have the appropriate incident response?

Ensure that you have a way to quickly grant access for your InfoSec team, and automate the isolation of instances as well at the capturing of data and state for forensics.

Key AWS Services
The AWS service that is essential to security is IAM, which allows you to securely control access to AWS services and resources for your users. The following services and features support the five areas in security:

Identity and Access Management:
IAM enables you to securely control access to AWS services and resources. MFA adds an extra layer of protection on top of your user name and password.

Detective Controls:
AWS CloudTrail records AWS API calls, AWS Config provides a detailed inventory of your AWS resources and configuration, and Amazon CloudWatch is a monitoring service for AWS resources.

Infrastructure Protection:
Amazon VPC lets you provision a private, isolated section of the AWS Cloud where you can launch AWS resources in a virtual network.

Data Protection:
Services such as ELB, Amazon Elastic Block Store (Amazon EBS), Amazon S3, and Amazon Relational Database Service (Amazon RDS) include encryption capabilities to protect your data in transit and at rest. Amazon Macie automatically discovers, classifies, and protects sensitive data, while AWS Key Management Service (AWS KMS) makes it easy for you to create and control keys used for encryption.

Incident Response: IAM should be used to grant appropriate authorization to incident response teams. AWS CloudFormation can be used to create a trusted environment for conducting investigations.

-----------------------------------------------------------------
#Basics of Securing Your Cloud
------------------------------------------------------------------
5.Understanding the Cloud Architecture
------------------------------------------------------------------
#The Well Architected Framework
#Best Practices for the Cloud
#Scaling on the cloud
#Scale Your Web Application — One Step at a Time
#Reliability on The Cloud
#Understanding Elasticity on the Cloud
#Performance Efficiency on the Cloud

-----------------------------------------------------------------------------------------------------

Introduction To AWS
Learning Objectives: In this module, you will learn about the different services provided by AWS. You will be provided with an overview of important resources required for architecting an application.

Topics:
Cloud Computing
Cloud deployment and service models
AWS Global Infrastructure and its benefits
AWS Services
Ways to access AWS Services

Hands-on:
Sign-up for AWS free-tier account
Create an S3 bucket through Console
Create an S3 bucket through AWS CLI
Launch an EC2 instance

Security Management In AWS
Learning Objectives: In this module, you will learn about security management in AWS using Identity Access Management (IAM) and Key Management Service.

Topics:
User management through Identity Access Management (IAM)
Various access policies across AWS Services
API keys service access
Best practices for IAM
Key Management Service
Access billing and create alerts on billing

Hands-on:
Create new users who can login to AWS console
Create role for an application to access S3
Create policies for new user to have either admin or limited privileges
Credential rotation for IAM users
Login to AWS console via MFA
Create API keys for accessing AWS Services
Create Budget

Object Storage Options
Learning Objectives: In this module, you will learn about the different Object Storage Services offered by AWS, identify when to use a specific service, how to store/transfer data using
these services and optimize the storage cost.

Topics:
S3 bucket - Creation, Version Control, Security, Replication, Transfer Acceleration
Storage classes in S3
Life cycle policy in S3
Cost optimization for S3
CloudFront – Create and configure with S3
Snowball
Storage Gateway and its types

Hands-on:
Hosting a Static Website on Amazon S3
Versioning in AWS S3
Replicating data across regions
S3 Transfer acceleration
Transfer and retrieve data from Glacier through lifecycle policy
Upload a file to AWS S3 through a Website
Accessing a static website through Cloud Front

Amazon EC2
Learning Objectives: EC2 (Elastic Compute Cloud) is the backbone of AWS. In this module, you will learn about the concepts associated with an EC2 instance and their usage. This module covers different Amazon AMIs, a demo on launching an AWS EC2 instance, ways to connect with an instance and how to host a website on AWS EC2 instance.

Topics:
Start, stop and terminate an EC2 Instance
Security Group
AMI
VPC, ENI, Public and Private IP
Storage services
EBS and its types
EFS
Cost optimization

Hands-on:
Host your website inside EC2
Create an AMI
Create an Elastic IP
Attaching an EBS volume externally
To create a snapshot
Mount EFS volumes

Load Balancing, Auto-Scaling And Route 53
Learning Objectives: In this module, you will learn the concepts of Load Balancing, Auto-Scaling and Route 53 to manage traffic.

Topics:
Elastic Load Balancer and its types
Comparison of Classic, Network and Application Load Balancer
Auto-Scaling
Components of Auto-Scaling
Lifecycle of Auto-Scaling
Auto-Scaling policy
Working of Route 53
Various Routing Policies

Hands-on:
Create a Classic Load Balancer
Create a Network Load Balancer
Work with Application Load Balancer and Auto-Scaling
Auto-Scaling and Scaling policy
Point a sub-domain to EC2 box in Route 53

Database Services And Analytics
Learning Objectives: In this module, you will learn about the different database services offered by AWS to handle structured and unstructured data. Also, learn how to analyze your data.

Topics:
Amazon RDS and its benefits
Amazon Aurora
Amazon DynamoDB
ElastiCache
Amazon RedShift
AWS Kinesis

Hands-on:
Storing an application data in MySQL DB using Relational Database Service (RDS)
Creating Tables, loading sample data and running queries
Redis Cache
Visualize the web traffic using Kinesis Data Stream

Networking And Monitoring Services
Learning Objectives: This module introduces you to the Amazon Virtual Private Cloud. You will learn to implement networking using public and private subnets with VPC. Also, this module demonstrates how to monitor your services.

Topics:
VPC – Benefits and Components
CIDR Notations
Network Access Control List v/s Security Groups
NAT – Network Address Translation
VPC peering
AWS CloudWatch
AWS CloudTrail
Trusted Advisor

Hands-on:
Create a Non-default VPC and attach it to an EC2 instance
Access Internet Inside Private Subnet Using NAT Gateway
Connect two instances in different VPCs using VPC peering
Monitor an EC2 instance using CloudWatch
Enable CloudTrail and Store Logs in S3
Explore the Trusted Advisor

Applications Services And AWS Lambda
Learning Objectives: In this module, you will learn about the different Application services offered by AWS that are used for sending e-mails, notifications, and processing message queues. This module also deals with the latest trend of Serverless architecture using AWS Lambda.

Topics:
AWS Simple Email Service (SES)
AWS Simple Notification Service (SNS)
AWS Simple Queue Service (SQS)
AWS Simple Work Flow (SWF)
AWS Lambda

Hands-on:
Send an email through AWS SES
Send a notification through SNS
Send an e-mail through Lambda when an object is added to S3
Send a notification through Lambda when a message is sent to SQS

Configuration Management and Automation
Learning Objectives: This module helps you learn various AWS services and tools used for configuration management and Automation.

Topics:
AWS CloudFormation
AWS OpsWorks - OpsWorks for Chef Automate, OpsWorks for Stack, OpsWorks for Puppet Enterprises
AWS Elastic Beanstalk
Differentiate between CloudFormation, OpsWorks, and Beanstalk

Hands-on:
Installation of LAMP server in EC2 through CloudFormation
AWS OpsWorks Stack
Deploy a Web Application with DynamoDB using Beanstalk

WS Architectural Designs – I
Learning Objectives: This module gives you an idea about the importance of AWS guidelines for Well Architected Framework. You will also learn about the Resilient and Performant architecture designs.

Topics:
Determine how to design high-availability and fault-tolerant architectures
Choose reliable/resilient storage
Determine how to design decoupling mechanisms using AWS services
Determine how to design a multi-tier architecture solution
Disaster Recovery Solution
Choose performant storage
Apply caching to improve performance
Design solutions for elasticity and scalability

AWS Architectural Designs – II (Self-paced)
Learning Objectives: Adding to Module 10, this module covers the remaining three concepts behind AWS Well-Architected Framework – Securing Applications and Architectures, Designing Cost-Optimized Architectures, Defining Operationally Excellent Architectures.

Topics:
Well-Architected Framework
Specify Secure Applications and Architectures
Design Cost-Optimized Architectures
Define Operationally-Excellent Architectures

------------------------------------------------------------------------------------------------------