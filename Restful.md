#RestFul::
-------------------------------------
0.What are web services ?
------------------------------------
Web services are client and server applications that communicate over the World Wide Web’s (WWW) HyperText Transfer Protocol (HTTP).
Web services provide a standard means of inter operating between software applications running on a variety of platforms and frameworks.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1.What is REST?What does REST stand for?
-------------------------------------------------------------
->REST is an architectural style of developing web services which take advantage of ubiquity of HTTP protocol and leverages HTTP method to define actions.
 REST stands for REpresntational State Transfer.
->REST stands for REpresentational State Transfer, which uses HTTP protocol to send data from client to server e.g. a book in the server can be delivered to the client using JSON or XML.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2.What is RESTFul Web Service?
---------------------------------------------------
-> There are two popular way to develop web services, using SOAP (Simple Object Access Protocol) which is XML based way to expose web services and second REST based web services
   which uses HTTP protocol. Web services developed using REST style is also known as RESTful Web Services.
-> REST is a stateless client-server architecture where web services are resources and can be identified by their URIs. Client applications can use HTTP GET/POST methods to
   invoke Restful web services.
REST doesn’t specify any specific protocol to use, but in almost all cases it’s used over HTTP/HTTPS. When compared to SOAP web services, these are lightweight and doesn’t follow
 any standard.
We can use XML, JSON, text or any other type of data for request and response.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
3.What is a resource?
----------------------------------
A resource is how data is represented in REST architecture. By exposing entities as the resource it allows a client to read, write, modify, and create resources using HTTP methods
e.g. GET, POST, PUT, DELETE etc.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4.What are safe REST operations?
---------------------------------------------------
REST API uses HTTP methods to perform operations. Some of the HTTP operations which doesn't modify the resource at the server is known as safe operations e.g. GET and HEAD.
On the other hand, PUT, POST, and DELETE are unsafe because they modify the resource on the server.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5.Can you tell me which API can be used to develop RESTFul web service in Java?
-----------------------------------------------------------------------------------------------------------------
-> There are many framework and libraries out there which helps to develop RESTful web services in Java including JAX-RS which is standard way to develop REST web services.
Jersey is one of the popular implementation of JAX-RS which also offers more than specification recommends. Then you also have RESTEasy, RESTlet and Apache CFX.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
6.Have you used Jersey API to develop RESTful services in Java?
-------------------------------------------------------------------------------------------
-> Jersey is one of the most popular framework and API to develop REST based web services in Java. Since many organization uses Jersey they check if candidate has used it before or not.
It's simple to answer, say Yes if you have really used and No, if you have not. In case of No, you should also mention which framework you have used for developing RESTful
web services e.g. Apache CFX, Play or Restlet.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7.What do you understand by payload in RESTFul?
-------------------------------------------------------------------------
   -> Payload means data which passed inside request body also payload is not request parameters. So only you can do payload in POST  and not in GET and DELTE method

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8.Can you do payload in GET method?
--------------------------------------
-> No, payload can only be passed using POST method.

-------------------------------------

9.Can you do payload in HTTP DELETE?
----------------------------------------------------------
-> No. You can only pass payload using HTTP POST method.

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10.How much maximum pay load you could do in POST method?
---------------------------------------------------------------------------------------------
-> If you remember difference between GET and POST request then you know that unlike GET which passes data on URL and thus limited by maximum URL length, POST has no such limit.
 So, theoretically you can pass unlimited data as payload to POST method but you need to take practical things into account e.g. sending POST with large payload will consume more
 bandwidth, take more time and present performance challenge to your server.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11.What is difference between SOAP and RESTFul web services?
----------------------------------------------------------------------------------------
SOAP is a standard protocol for creating web services.	REST is an architectural style to create web services.
SOAP is acronym for Simple Object Access Protocol.	REST is acronym for REpresentational State Transfer.
SOAP uses WSDL to expose supported methods and technical details.	REST exposes methods through URIs, there are no technical details.
SOAP web services and client programs are bind with WSDL contract	REST doesn’t have any contract defined between server and client
SOAP web services and client are tightly coupled with contract.	REST web services are loosely coupled.
SOAP learning curve is hard, requires us to learn about WSDL generation, client stubs creation etc.	REST learning curve is simple, POJO classes can be generated easily and works on simple HTTP methods.
SOAP supports XML data format only	REST supports any data type such as XML, JSON, image etc.
SOAP web services are hard to maintain, any change in WSDL contract requires us to create client stubs again and then make changes to client code.	REST web services are easy to maintain when compared to SOAP, a new method can be added without any change at client side for existing resources.
SOAP web services can be tested through programs or software such as Soap UI.	REST can be easily tested through CURL command, Browsers and extensions such as Chrome Postman.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12.If you have to develop web services which one you will choose SOAP OR RESTful and why?
-----------------------------------------------------------------------------------------------------------------------------------
 its easy to develop RESTful web services than SOAP based web services but later comes with some in-built security features.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13.What framework you had used to develop RESTFul services?
-------------------------------------------------------------------------------------
-> If you have used Jersey to develop RESTFul web services then answer as Jersey but expect some follow-up question on Jersey. Similarly if you have used Apache CFX or Restlet then answer them accordingly.

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14.What are idempotent operations? Why is idempotency important?
------------------------------------------------------------------------------------------
There are some HTTP methods e.g. GET which produce same response no matter how many times you use them e.g. sending multiple GET request to the same URI will result in same response without any side-effect hence it is known as idempotent.

On the other hand, the POST is not idempotent because if you send multiple POST request, it will result in multiple resource creation on the server, but again, PUT is idempotent if you are using it to update the resource.
multiple PUT request to update a resource on a server will give same end result. You can further take HTTP Fundamentals course by Pluralsight to learn more about idempotent methods of HTTP protocol and HTTP in general.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15.Is REST scalable and/or interoperable?
-------------------------------------------------------
  Yes, REST is Scalable and interoperable. It doesn't mandate a specific choice of technology either at client or server end. You can use Java, C++, Python or JavaScript to create RESTful Web Services and Consume them at the client end.

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16.What are the advantages of the RestTemplate?
-----------------------------------------------------------------
The RestTemplate class is an implementation of Template method pattern in Spring framework. Similar to other popular template classes e.g. JdbcTemplate or JmsTempalte, it also simplifies the interaction with RESTful Web Services on the client side

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17.Which HTTP methods does REST use?
----------------------------------------------------------
REST can use any HTTP methods but the most popular ones are GET for retrieving a resource, POST for creating a resource, PUt for updating resource and DELETE for removing a resource from the server.

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18.What is an HttpMessageConverter in Spring REST?
-------------------------------------------------------------------------
An HttpMessageConverter is a Strategy interface that specifies a converter that can convert from and to HTTP requests and responses. Spring REST uses this interface to convert HTTP response to various formats e.g. JSON or XML.

Each HttpMessageConverter implementation has one or several MIME Types associated with it. Spring uses the "Accept" header to determine the content type client is expecting.

It will then try to find a registered HTTPMessageConverter that is capable of handling that specific content-type and use it to convert the response into that format before sending to the client.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19.How to create a custom implementation of HttpMessageConverter to support a new type of request/responses?
---------------------------------------------------------------------------------------------------------------------------------------------------------
You just need to create an implementation of AbstractHttpMessageConverter and register it using the WebMvcConfigurerAdapter#extendMessageConverters() method with the classes which generate a new type of request/response.

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20.Is REST normally stateless?
-----------------------------------------
Yes, REST API should be stateless because it is based on HTTP which is also stateless. A Request in REST API should contain all the details required it to process i.e. it should not rely on previous or next request or some data maintained at the server end e.g.
Sessions. REST specification put a constraint to make it stateless and you should keep that in mind while designing your REST API.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21.What does @RequestMapping annotation do?
---------------------------------------------------------------
The @RequestMapping annotation is used to map web requests to Spring Controller methods. You can map request based upon HTTP methods  e.g. GET and POST and various other parameters. For examples,
if you are developing RESTful Web Service using Spring then you can use produces and consumes property along with media type annotation to indicate that this method is only used to produce or consumers JSON
as shown below:

@RequestMapping (method = RequestMethod.POST, consumes="application/json")
public Book save(@RequestBody Book aBook) {
   return bookRepository.save(aBook);
}
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22.Is @Controller a stereotype? Is @RestController a stereotype?
---------------------------------------------------------------------------------------
Yes, both @Controller and @RestController are stereotypes. The @Controller is actually a specialization of Spring's @Component stereotype annotation. This means that class annotated with @Controller will also be
automatically be detected by Spring container as part of container's component scanning process.

And, @RestController is a specialization of @Controller for RESTful web service. It not only combines @ResponseBody and @Controller annotation but also gives more meaning to your controller class to clearly indicate
that it deals with RESTful requests.

Spring Framework may also use this annotation to provide some more useful features related to REST API development in future.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
23.What are HTTP methods that can be used in Restful web services?
----------------------------------------------------------------------------------------------------
RESTful web services use HTTP protocol methods for the operations they perform.
Some important Methods are:
GET : It defines a reading access of the resource without side-effects.This operation is idempotent i.e.they can be applied multiple times without changing the result
PUT : It is generally used for updating resource. It must also be idempotent.
DELETE : It removes the resources. The operations are idempotent i.e. they can get repeated without leading to different results.
POST : It is used for creating a new resource. It is not idempotent.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24.What are differences between Post and Put Http methods?
------------------------------------------------------------------------------------------------
POST :It is used for creating a new resource. It is not idempotent.
PUT : It is generally used for updating resource. It is idempotent.
Idempotent means result of multiple successful request will not change state of resource after initial application
-----------------------------------------------------------------------------------------------------------------------------------------------------------
25.The difference between PUT and PATCH is that:
--------------------------------------------------------------------------
PUT is required to be idempotent. In order to achieve that, you have to put the entire complete resource in the request body.
PATCH can be non-idempotent. Which implies it can also be idempotent in some cases, such as the cases you described.
-------------------------------------------------------------------------------------------------------------------------------------------
26.What is HTTP Basic Authentication and how it works?
-------------------------------------------------------

Question 5: How do you configure the RESTFul web service?

Question 6: How you apply security in RESTFul web services?

Question 7: Have you used securing RESTful APIs with HTTP Basic Authentication

Question 8: How you maintain sessions in RESTful services?
Question 10: What is WADL in RESTFul?


------------------------------
What are Safe Methods in HTTP?
------------------------------
These are HTTP methods that don't change the resource on the server-side. For example, using a GET or a HEAD request on a resource URL should NEVER change the resource. Safe methods can be cached and prefetched without any repercussions or side-effect to the resource . Here is an example of safe method
GET /order/123 HTTP/1.1

------------------------------------------------------------------------------------------------------------------------------

What are Idempotent Methods in HTTP?
------------------------------------
These are methods which are safe from multiple calls i.e. they produce same result irrespective of how many times you call them. They change 
the resource in Server every time you call them but the end result is always same. Maths is good place to explain idempotent methods, consider 
the following example:

int i = 30; // idempotent

i++; // not idempotent

--------------------------------------------------------------------------------------------------------------------------------