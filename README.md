# DataStructureAlgorithmsAndMathematics

Data Structure Algorithms and Mathematics in Java:- Easy, Medium, Hard, Very Hard, and  Extremely Hard. or All Topics


#Comman QA:::
--------------
JAR vs WAR vs EAR :::
---------------------
In J2EE application, modules are packaged as EAR, JAR and WAR based on their functionality
JAR: EJB modules which contain enterprise java beans (class files) and EJB deployment descriptor are packed as JAR files with .jar extenstion
WAR: Web modules which contain Servlet class files, JSP Files, supporting files, GIF and HTML files are packaged as JAR file with .war (web archive) extension
EAR: All above files (.jar and .war) are packaged as JAR file with .ear (enterprise archive) extension and deployed into Application Server.
---------------------------------------------------------------------------------------------------------------------------------------------------------
#Difference between BufferedReader and Scanner are following::::
---------------------------------------------------------------
BufferedReader is synchronized but Scanner is not synchronized.
BufferedReader is thread safe but Scanner is not thread safe.
BufferedReader has larger buffer memory but Scanner has smaller buffer memory.
BufferedReader is faster but Scanner is slower in execution.
Code to read a line from console:
BufferedReader:
----------------
 InputStreamReader isr=new InputStreamReader(System.in);
 BufferedReader br= new BufferedReader(isr);
 String st= br.readLine();
 
Scanner:
--------
Scanner sc= new Scanner(System.in);
String st= sc.nextLine();

----------------
#Core Java:::
------------
Basics::
--------
0.Explain JDK, JRE and JVM?
---------------------------
JDK::-> It stands for Java Development Kit.
-> It is the tool necessary to compile,document and package Java programs.
->Along with JRE, it includes an interpreter/loader, a compiler (javac), an archiver (jar), a
documentation generator (javadoc) and other tools needed in Java development. In short, it contains JRE + development tools.

JRE:::->It stands for Java Runtime Environment.
-> JRE refers to a runtime environment in which java bytecode can be executed.
-> It implements the JVM (Java Virtual Machine) and provides all the class libraries and other support files that JVM uses at runtime. So JRE is a software package that contains what is required to run a Java program.
Basically, it’s an implementation of the JVM which physically exists.

JVM::: ->It stands for Java Virtual Machine.
-> It is an abstract machine. It is a specification that provides run-time environment in which java bytecode can be executed.
->JVM follows three notations: Specification(document that describes the implementation of the Java virtual machine),Implementation​ (program that meets the requirements of JVM specification) and ​ Runtime Instance ​ (instance of JVM is created whenever you write a java command on the command prompt and run class).
----------------------------------------------------------------------------------------
1.What is the difference between public, protected, package-private and private in Java?
----------------------------------------------------------------------------------------
The official tutorial may be of some use to you.
_____________________________________________________________
            │ Class │ Package │ Subclass │ Subclass │ World
            │       │         │(same pkg)│(diff pkg)│ 
────────────┼───────┼─────────┼──────────┼──────────┼────────
public      │   +   │    +    │    +     │     +    │   + 
────────────┼───────┼─────────┼──────────┼──────────┼────────
protected   │   +   │    +    │    +     │     +    │ 
────────────┼───────┼─────────┼──────────┼──────────┼────────
no modifier │   +   │    +    │    +     │          │ 
────────────┼───────┼─────────┼──────────┼──────────┼────────
private     │   +   │         │          │          │ 
____________________________________________________________
 + : accessible         blank : not accessible
--------------------------------------------------------------
#2.Explain public static void main(String args[]).
---------------------------------------------------------
public::: Public is an access modifier, which is used to specify who can access this method. Public means that this Method will be accessible by any Class.
static::​: It is a keyword in java which identifies it is class based i.e it can be accessed without creating the instance of a Class.
void​ ::: It is the return type of the method. Void defines the method which will not return any value.
main​ ::: It is the name of the method​ ​ which is searched by JVM as a starting point for an application with a particular signature only. It is the method where the main execution occurs.
String args[]​ ::: It is the parameter passed to the main method.
-----------------------------------------------------------------------------------------------------
#3. Why Java is platform independent?
---------------------------------------
Platform independent practically means “write once run anywhere”. Java is called so because of its byte codes which can run on any system irrespective of its underlying operating system.
--------------------------------------------------------------------------------------------------
#4. Why java is not 100% Object-oriented?
-------------------------------------------
Java is not 100% Object-oriented because it makes use of eight primitive datatypes such as boolean, byte, char, int,float, double, long, short which are ​ not objects.
---------------------------------------------------------------------------------------------------
#5. Why java is not 100% Object-oriented?
--------------------------------------------
Java is not 100% Object-oriented because it makes use of eight primitive datatypes such as boolean, byte, char, int,float, double, long, short which are ​ not objects.
---------------------------------------------------------------------------------------------------
#6. What are wrapper classes?
Wrapper classes converts the java primitives into the reference types (objects). Every primitive data type has a class dedicated to it. These are known as wrapper classes because they “wrap” the primitive data type into an object of that class. Refer to the below image which displays different primitive type, wrapper class and constructor argument.
--------------------------------------------------------------------------------------------------
#OOPS::
--------
#Encapsulation in Java
--------------------------------
-> Encapsulation is data hiding.
-> Encapsulation in Java is a process of wrapping code and data together into a single unit.
-> We can create a fully encapsulated class in Java by making all the data members of the class private. Now we can use setter and getter methods to set and get the data in it.
-> The Java Bean class is the example of a fully encapsulated class.

Advantage of Encapsulation in Java
--------------------------------------------------
-> By providing only a setter or getter method, you can make the class read-only or write-only. In other words, you can skip the getter or setter methods.
-> It provides you the control over the data. Suppose you want to set the value of id which should be greater than 100 only, you can write the logic inside the setter method. You can write the logic not to store the negative numbers in the setter methods.
-> It is a way to achieve data hiding in Java because other class will not be able to access the data through the private data members.
-> The encapsulate class is easy to test. So, it is better for unit testing.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Abstraction in Java
---------------------------
-> Abstraction is detail hiding(implementation hiding).
-> Abstraction is a process of hiding the implementation details and showing only functionality to the user.
-> Another way, it shows only essential things to the user and hides the internal details, for example, sending SMS where you type the text and send the message. You don't know the internal processing about the message delivery.
-> Abstraction lets you focus on what the object does instead of how it does it.

Ways to achieve Abstraction
----------------------------------------
There are two ways to achieve abstraction in java

-> Abstract class (0 to 100%)
-> Interface (100%)
---------------------------
Difference between abstract class and interface
-------------------------------------------------------------------
-> Abstract class and interface both are used to achieve abstraction where we can declare the abstract methods. Abstract class and interface both can't be instantiated.

But there are many differences between abstract class and interface that are given below.

Abstract class                    Interface
-------------------- --------------------------                               
1) Abstract class can have abstract and non-abstract methods.-> Interface can have only abstract methods. Since Java 8, it can have default and static methods also.
2) Abstract class doesn't support multiple inheritance.                ->	Interface supports multiple inheritance.
3) Abstract class can have final, non-final, static and non-static variables. ->	Interface has only static and final variables.
4) Abstract class can provide the implementation of interface. ->	Interface can't provide the implementation of abstract class.
5) The abstract keyword is used to declare abstract class.	 -> The interface keyword is used to declare interface.
6) An abstract class can extend another Java class and implement multiple Java interfaces.	 -> An interface can extend another Java interface only.
7) An abstract class can be extended using keyword "extends". ->	An interface class can be implemented using keyword "implements".
8) A Java abstract class can have class members like private, protected, etc.	->  Members of a Java interface are public by default.
9)Example:
public abstract class Shape{
public abstract void draw();
}	

Example:
public interface Drawable{
void draw();
}

Polyporphism
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--------------------
#Exception Handling:::
----------------------
0.Exception Handling in Java
-----------------------------
Exception Handling in Java is one of the powerful mechanism to handle the runtime errors so that normal flow of the application can be maintained.
->The java.lang.Throwable class is the root class of Java Exception hierarchy which is inherited by two subclasses: Exception and Error.
->There are mainly two types of exceptions: checked and unchecked. Here, an error is considered as the unchecked exception. 
------------------------------------------------------------------------------------------------------
1.Difference between Checked and Unchecked Exceptions
--------------------------------------------------------------------------------------------------
Checked Exception:::
--------------------
-> The classes which directly inherit Throwable class except RuntimeException and Error are known as checked exceptions e.g. IOException, SQLException etc. Checked exceptions are checked at compile-time.

Unchecked Exception:::
----------------------
-> The classes which inherit RuntimeException are known as unchecked exceptions e.g. ArithmeticException, NullPointerException, ArrayIndexOutOfBoundsException etc. Unchecked exceptions are not checked at compile-time, but they are checked at runtime.

Error:::
--------
-> Error is irrecoverable e.g. OutOfMemoryError, VirtualMachineError, AssertionError etc.
------------------------------------------------------------------------------------------------------
2.There are 5 keywords which are used in handling exceptions in Java.
---------------------------------------------------------------------
try:::
------   ->The "try" keyword is used to specify a block where we should place exception code. The try block must be followed by either catch or finally. It means, we can't use try block alone.
         
catch:::	
 ------- -> The "catch" block is used to handle the exception. It must be preceded by try block which means we can't use catch block alone. It can be followed by finally block later.
        
finally:::
---------  -> The "finally" block is used to execute the important code of the program. It is executed whether an exception is handled or not.
          
throw::: 
-------- 	-> The "throw" keyword is used to throw an exception.
throws:::
---------   ->The "throws" keyword is used to declare exceptions. It doesn't throw an exception. It specifies that there may occur an exception in the method. It is always used with method signature.
---------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
1. Difference between throw and throws in Java.
------------------------------------------------
         throw	                             throws
        --------                          ---------------------
1)Java throw keyword is used to explicitly throw an exception.-> Java throws keyword is used to declare  an exception.
2)Checked exception cannot be propagated using throw only.-> Checked exception can be propagated with throws.
3)Throw is followed by an instance. -> Throws is followed by class.
4)Throw is used within the method. -> Throws is used with the method signature.
5)You cannot throw multiple exceptions.-> You can declare multiple exceptions e.g.
                                          public void method()throws IOException,SQLException.
-------------------------------------------------------------------------------------------------
2.Difference between final, finally and finalize.
---------------------------------------------------
final:::
-------		
-> Final is used to apply restrictions on class, method and variable. Final class can't be inherited, final method can't be overridden and final variable value can't be changed.
->Final is a keyword.

finally:::
----------	
->Finally is used to place important code, it will be executed whether exception is handled or not.	
->Finally is a block.

finalize:::
----------
->Finalize is used to perform clean up processing just before object is garbage collected.
->Finalize is a method.
-----------------------------------------------------------------------------------------------------
3.What is Custom Exception.
----------------------------
-> If you are creating your own Exception that is known as custom exception or user-defined exception.     Java custom exceptions are used to customize the exception according to user need.
-> By the help of custom exception, you can have your own exception and message.  
->Ex. class InvalidAgeException extends Exception{ }
---------------------------------------------------------------------------------------------------

--------------------
#Collections::
--------------
1.Convert HashMap To ArrayList In Java.
---------------------------------------
Map<Integer,String>map = new HashMap<>();
map.put(1,"mark");
map.put(2,"arun");
List<String> list = new ArrayList<>(map.values());
for(String s : list){
Sopln(s);
}
----------------------------------------------------
2.Differences between HashMap and Hashtable?
--------------------------------------------
HashMap and Hashtable both are used to store data in key and value form. Both are using hashing technique to store unique keys. But there are many differences between HashMap and Hashtable classes that are given below.

HashMap::::
-------------
HashMap is non synchronized. It is not-thread safe and can't be shared between many threads without proper synchronization code.
HashMap allows one null key and multiple null values.
HashMap is a new class introduced in JDK 1.2.
HashMap is fast.
We can make the HashMap as synchronized by calling this code
Map m = Collections.synchronizedMap(HashMap);
HashMap is traversed by Iterator.
Iterator in HashMap is fail-fast.
HashMap inherits AbstractMap class.

Hashtable:::
------------
Hashtable is synchronized. It is thread-safe and can be shared with many threads.
Hashtable doesn't allow any null key or value.
Hashtable is a legacy class.
Hashtable is slow.
Hashtable is internally synchronized and can't be unsynchronized.
Hashtable is traversed by Enumerator and Iterator.
Enumerator in Hashtable is not fail-fast.
Hashtable inherits Dictionary class.
--------------------------------------------------------------------------------------
3.What's the difference between ConcurrentHashMap and Collections.synchronizedMap(Map)?
--------------------------------------------------------------------------------------
->ConcurrentHashMap does not allow null keys or values. So they are NOT equal alternatives of a synchronized map. 
For your needs, use ConcurrentHashMap. It allows concurrent modification of the Map from several threads without the need to block them. Collections.synchronizedMap(map) creates a blocking Map which will degrade performance, albeit ensure consistency (if used properly).
Use the second option if you need to ensure data consistency, and each thread needs to have an up-to-date view of the map. Use the first if performance is critical, and each thread only inserts data to the map, with reads happening less frequently.
╔═══════════════╦═══════════════════╦═══════════════════╦═════════════════════╗
║   Property    ║     HashMap       ║    Hashtable      ║  ConcurrentHashMap  ║
╠═══════════════╬═══════════════════╬═══════════════════╩═════════════════════╣ 
║      Null     ║     allowed       ║              not allowed                ║
║  values/keys  ║                   ║                                         ║
╠═══════════════╬═══════════════════╬═════════════════════════════════════════╣
║Is thread-safe ║       no          ║                  yes                    ║
╠═══════════════╬═══════════════════╬═══════════════════╦═════════════════════╣
║     Lock      ║       not         ║ locks the whole   ║ locks the portion   ║ 
║  mechanism    ║    applicable     ║       map         ║                     ║ 
╠═══════════════╬═══════════════════╩═══════════════════╬═════════════════════╣
║   Iterator    ║               fail-fast               ║ weakly consistent   ║ 
╚═══════════════╩═══════════════════════════════════════╩═════════════════════╝
Regarding locking mechanism: Hashtable locks the object, while ConcurrentHashMap locks only the bucket.
---------------------------------------------------------------------------------------------------------
ArrayList:- Java ArrayList class uses a dynamic array for storing the elements. It inherits AbstractList class and implements List interface.
-------------
Java ArrayList class can contain duplicate elements.
Java ArrayList class maintains insertion order.
Java ArrayList class is non synchronized.
Java ArrayList allows random access because array works at the index basis.
In Java ArrayList class, manipulation is slow because a lot of shifting needs to occur if any element is removed from the array list.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Difference between ArrayList and Vector
---------------------------------------------------------
ArrayList and Vector both implements List interface and maintains insertion order.
ArrayList
-------------
1) ArrayList is not synchronized.
2) ArrayList increments 50% of current array size if the number of elements exceeds from its capacity.
3) ArrayList is not a legacy class. It is introduced in JDK 1.2.
4) ArrayList is fast because it is non-synchronized.
5) ArrayList uses the Iterator interface to traverse the elements.
---------------------------------------------------------------------------------------------------------------
Vector
-----------------------------------------------------------------------------------------------------
Vector is synchronized.
Vector increments 100% means doubles the array size if the total number of elements exceeds than its capacity.
Vector is a legacy class.
Vector is slow because it is synchronized, i.e., in a multithreading environment, it holds the other threads in runnable or non-runnable state until current thread releases the lock of the object.
A Vector can use the Iterator interface or Enumeration interface to traverse the elements.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
LinkedList::- Java LinkedList class uses a doubly linked list to store the elements. It provides a linked-list data structure. It inherits the AbstractList class and implements List and Deque interfaces.

Java LinkedList class can contain duplicate elements.
Java LinkedList class maintains insertion order.
Java LinkedList class is non synchronized.
In Java LinkedList class, manipulation is fast because no shifting needs to occur.
Java LinkedList class can be used as a list, stack or queue.
----------------------------------------------------------------------------------------------------------------------
Difference between ArrayList and LinkedList
---------------------------------------------------------------
ArrayList and LinkedList both implements List interface and maintains insertion order. Both are non synchronized classes.

ArrayList	LinkedList
1) ArrayList internally uses a dynamic array to store the elements.	
->   LinkedList internally uses a doubly linked list to store the elements.
2) Manipulation with ArrayList is slow because it internally uses an array. If any element is removed from the array, all the bits are shifted in memory.
	-> Manipulation with LinkedList is faster than ArrayList because it uses a doubly linked list, so no bit shifting is required in memory.
3) An ArrayList class can act as a list only because it implements List only.	
 - -> LinkedList class can act as a list and queue both because it implements List and Deque interfaces.
4) ArrayList is better for storing and accessing data.
-->	LinkedList is better for manipulating data.
----------------------------------------------------------------------------
Java LinkedHashMap::-Java LinkedHashMap class is Hashtable and Linked list implementation of the Map interface, with predictable iteration order. It inherits HashMap class and implements the Map interface.

Java LinkedHashMap contains values based on the key.
Java LinkedHashMap contains unique elements.
Java LinkedHashMap may have one null key and multiple null values.
Java LinkedHashMap is non synchronized.
Java LinkedHashMap maintains insertion order.
The initial default capacity of Java HashMap class is 16 with a load factor of 0.75.
--------------------------------------------------------------------------------------------------------------------
HashMap: - Java HashMap class implements the map interface by using a hash table. It inherits AbstractMap class and implements Map interface.

Java HashMap class contains values based on the key.
Java HashMap class contains only unique keys.
Java HashMap class may have one null key and multiple null values.
Java HashMap class is non synchronized.
Java HashMap class maintains no order.
The initial default capacity of Java HashMap class is 16 with a load factor of 0.75.
----------------------------------------------------------------------------------------
Difference between HashMap and Hashtable
------------------------------------------
HashMap and Hashtable both are used to store data in key and value form. Both are using hashing technique to store unique keys.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Hashtable:-Java Hashtable class implements a hashtable, which maps keys to values. It inherits Dictionary class and implements the Map interface.

A Hashtable is an array of a list. Each list is known as a bucket. The position of the bucket is identified by calling the hashcode() method. A Hashtable contains values based on the key.
Java Hashtable class contains unique elements.
Java Hashtable class doesn't allow null key or value.
Java Hashtable class is synchronized.
The initial default capacity of Hashtable class is 11 whereas loadFactor is 0.75.
--------------------------------------------------------------------------------------------------------------
HashMap	Hashtable
1) HashMap is non synchronized. It is not-thread safe and can't be shared between many threads without proper synchronization code.	
 --->Hashtable is synchronized. It is thread-safe and can be shared with many threads.
2) HashMap allows one null key and multiple null values.
--->	Hashtable doesn't allow any null key or value.
3) HashMap is a new class introduced in JDK 1.2.	
---> Hashtable is a legacy class.
4) HashMap is fast.	
 --->Hashtable is slow.
5) We can make the HashMap as synchronized by calling this code  Map m = Collections.synchronizedMap(hashMap);	
  ---> Hashtable is internally synchronized and can't be unsynchronized.
6) HashMap is traversed by Iterator.	
--->Hashtable is traversed by Enumerator and Iterator.
7) Iterator in HashMap is fail-fast.	
 ---->Enumerator in Hashtable is not fail-fast.
8) HashMap inherits AbstractMap class.	
---> Hashtable inherits Dictionary class.
-----------------------------------------------------------------------------------------------------------------------------------
Java TreeMap class hierarchy
-----------------------------
Java TreeMap class is a red-black tree based implementation. It provides an efficient means of storing key-value pairs in sorted order.

Java TreeMap contains values based on the key. It implements the NavigableMap interface and extends AbstractMap class.
Java TreeMap contains only unique elements.
Java TreeMap cannot have a null key but can have multiple null values.
Java TreeMap is non synchronized.
Java TreeMap maintains ascending order.
---------------------------------------------------------------------------------
ConcurrentHashMap ::--ConcurrentHashMap ConcurrentHashMap class is introduced in JDK 1.5, which implements ConcurrentMap as well as Serializable interface also. ConcureentHashMap is enhancement of HashMap as we know that while dealing with Threads in our application HashMap is not a good choice because performance wise HashMap is not upto the mark.

* The underlined data structure for ConcurrentHashMap is Hashtable.
* ConcurrentHashMap class is thread-safe i.e. multiple thread can operate on a single object without any complications.
* At a time any number of threads are applicable for read operation without locking the ConcurrentHashMap object which is not there in HashMap.
* In ConcurrentHashMap, the Object is divided into number of segments according to the concurrency level.
* Default concurrency-level of ConcurrentHashMap is 16.
* In ConcurrentHashMap, at a time any number of threads can perform retrieval operation but for updation in object, thread must lock the particular segment in which thread want to * operate.This type of locking mechanism is known as Segment locking or bucket locking.Hence at a time 16 updation operations can be performed by threads.
* null insertion is not possible in ConcurrentHashMap as key or value.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
WeakHashMap :: - WeakHashMap is the Hash table based implementation of the Map interface, with weak keys. An entry in a WeakHashMap will automatically be removed when its key is no longer in ordinary use. More precisely, the presence of a mapping for a given key will not prevent the key from being discarded by the garbage collector, that is, made finalizable, finalized, and then reclaimed. When a key has been discarded its entry is effectively removed from the map, so this class behaves somewhat differently from other Map implementations. Few important features of a weak hashmap are:

1. Both null values and null keys are supported in WeakHashMap.
2. It is not synchronised.
3. This class is intended primarily for use with key objects whose equals methods test for object identity using the == operator.
------------------------------------------------------------------------
Difference between Comparable and Comparator
---------------------------------------------------
Comparable and Comparator both are interfaces and can be used to sort collection elements.

Comparable	Comparator
1) Comparable provides a single sorting sequence. In other words, we can sort the collection on the basis of a single element such as id, name, and price.	
--->The Comparator provides multiple sorting sequences. In other words, we can sort the collection on the basis of multiple elements such as id, name, and price etc.
2) Comparable affects the original class, i.e., the actual class is modified.
--->	Comparator doesn't affect the original class, i.e., the actual class is not modified.
3) Comparable provides compareTo() method to sort elements.
--->	Comparator provides compare() method to sort elements.
4) Comparable is present in java.lang package.
-->	A Comparator is present in the java.util package.
5) We can sort the list elements of Comparable type by Collections.sort(List) method.	
----> We can sort the list elements of Comparator type by Collections.sort(List, Comparator) method.
------------------------------------------------------------------------------------------------------------------------------------------
HashSet::- Java HashSet class is used to create a collection that uses a hash table for storage. It inherits the AbstractSet class and implements Set interface.

HashSet stores the elements by using a mechanism called hashing.
HashSet contains unique elements only.
HashSet allows null value.
HashSet class is non synchronized.
HashSet doesn't maintain the insertion order. Here, elements are inserted on the basis of their hashcode.
HashSet is the best approach for search operations.
The initial default capacity of HashSet is 16, and the load factor is 0.75.
----------------------------------------------------------------------------------------------------
TreeSet :-- Java TreeSet class implements the Set interface that uses a tree for storage. It inherits AbstractSet class and implements the NavigableSet interface. The objects of the TreeSet class are stored in ascending order.

Java TreeSet class contains unique elements only like HashSet.
Java TreeSet class access and retrieval times are quiet fast.
Java TreeSet class doesn't allow null element.
Java TreeSet class is non synchronized.
Java TreeSet class maintains ascending order.
------------------------------------------------------------------------------------------------------------
#String Handling::
-----------------
1.Difference between StringBuilder and StringBuffer?
----------------------------------------------------
->StringBuffer is synchronized, StringBuilder is not.
->String is an immutable.
StringBuffer is a mutable and synchronized.
StringBuilder is also mutable but its not synchronized.

-----------------
#Multithreading::
-----------------
1.What does 'synchronized' mean?
--------------------------------
-> The synchronized keyword is all about different threads reading and writing to the same variables, objects and resources. 
This is not a trivial topic in Java, but here is a quote from Sun:synchronized methods enable a simple strategy for preventing 
thread interference and memory consistency errors: if an object is visible to more than one thread, all reads or writes to that object's variables are done through synchronized methods.
->synchronized means that in a multi threaded environment, an object having  synchronized method(s)/block(s) does not let two threads to access the synchronized method(s)/block(s) of code at the same time.
 This means that one thread can't read while another thread updates it.
-----------------------------------------------------------------------------------------------------
2.Differences between Start And Run Methods In Java Threads?
-------------------------------------------------------------
 start()                            	                       run()
New thread is created.	                                 No new thread is created.
Newly created thread executes task kept in run() method. Calling thread itself executes task kept in run() method.                                                                              
It is a member of java.lang.Thread class.	             It is a member of java.lang.Runnable interface.
You can’t call start() method more than once.	         You can call run() method multiple times.
Use of multi-threaded programming concept.	             No use of multi-threaded programming concept.
-----------------------------------------
3. Difference Between BLOCKED Vs WAITING States In Java?
----------------------------------------------------------
WAITING
--------                                                                  	
The thread will be in this state when it calls wait() or join() method. The thread will remain in WAITING state until any other thread calls notify() or notifyAll().
The WAITING thread is waiting for notification from other threads.
The WAITING thread can be interrupted.	

BLOCKED
---------
The thread will be in this state when it is notified by other thread but has not got the object lock yet.
The BLOCKED thread is waiting for other thread to release the lock.
The BLOCKED thread can’t be interrupted.

-----------------
Servlet::(J2EE)
-----------------
1.Classloading,2. Memory Management, 3.Transaction management...

-----------------
MySQL::
-----------------

-----------------
Oracle:::
-----------------
Oracle Joins
------------
Join is a query that is used to combine rows from two or more tables, views, or materialized views. It retrieves data from multiple tables and creates a new table.

Join Conditions
---------------
There may be at least one join condition either in the FROM clause or in the WHERE clause for joining two tables. It compares two columns from different tables and combines pair of rows, each containing one row from each table, for which join condition is true.

Types of Joins
--------------
	Inner Joins (Simple Join)
	Outer Joins
	-----------
			Left Outer Join (Left Join)
			Right Outer Join (Right Join)
			Full Outer Join (Full Join)
	Equijoins
	Self Joins
	Cross Joins (Cartesian Products)
	Antijoins
	Semijoins:::
	------------
	Semi-join is introduced in Oracle 8.0. It provides an efficient method of performing a WHERE EXISTS sub-query.
	A semi-join returns one copy of each row in first table for which at least one match is found.
	Semi-joins are written using the EXISTS construct.
	---------------------------------------------------------------------------------------------------------------
	Oracle Semi Join Example
	Let's take two tables "departments" and "customer"
	Departments table

	CREATE TABLE  "DEPARTMENTS"   
   (    "DEPARTMENT_ID" NUMBER(10,0) NOT NULL ENABLE,   
    "DEPARTMENT_NAME" VARCHAR2(50) NOT NULL ENABLE,   
     CONSTRAINT "DEPARTMENTS_PK" PRIMARY KEY ("DEPARTMENT_ID") ENABLE  
   );  

	Customer table

	CREATE TABLE  "CUSTOMER"   
	   (    "CUSTOMER_ID" NUMBER,   
		"FIRST_NAME" VARCHAR2(4000),   
		"LAST_NAME" VARCHAR2(4000),   
		"DEPARTMENT_ID" NUMBER  
	   );  
	   
	   Execute this query

	SELECT   departments.department_id, departments.department_name  
			FROM     departments  
			WHERE    EXISTS  
					 (  
					 SELECT 1  
					 FROM   customer  
					 WHERE customer.department_id = departments.department_id  
					 )  
			ORDER BY departments.department_id;  
--------------------------------------------------
#SOAP::
--------------------------------------------------
1. JAX-WS annotations:::-
-------------------------
 a.@WebService
 -------------
This JAX-WS annotation can be used in 2 ways. If we are annotating this over a class, it means that we are trying to mark the class as the implementing the Web Service, 
in other words Service Implementation Bean (SIB).Or we are marking this over an interface, it means that we are defining a Web Service Interface (SEI), in other words Service Endpoint Interface.
Now lets see the java program demonstrating both of the mentioned ways:

WSAnnotationWebServiceI.java
----------------------------
package com.javacodegeeks.examples.jaxWsAnnotations.webservice;
 
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
 
@WebService
@SOAPBinding(style=Style.RPC)
public interface WSAnnotationWebServiceI {
    @WebMethod
    float celsiusToFarhenheit(float celsius);
}

In the above program we can see that we haven’t provided any optional element along with the @WebService annotation. 
And here it is used to define SEI. Regarding the other annotations used in the above program, we shall see their description a little ahead.

WsAnnotationsWebServiceImpl.java
---------------------------------
package com.javacodegeeks.examples.jaxWsAnnotations.webservice;
 
import javax.jws.WebService;
 
@WebService(endpointInterface="com.javacodegeeks.examples.jaxWsAnnotations.webservice.WSAnnotationWebServiceI")
public class WsAnnotationsWebServiceImpl implements WSAnnotationWebServiceI {
    @Override
    public float celsiusToFarhenheit(float celsius) {
        return ((celsius - 32)*5)/9;
    }
}
In the above program we can see that we have provided an optional element endpointInterface along with the @WebService annotation. And here it is used to define SIB. endpointInterface optional element describes the SEI that the said SIB is implementing.

While implementing a web service as in above example, it is not mandatory for WsAnnotationsWebServiceImpl to implement WSAnnotationWebServiceI, this just serves as a check. Also, it is not mandatory to use an SEI, however, as a basic design principle “We should program to interface”, hence we have adapted this methodology in above program.

Other optional elements to @WebService can be like wsdlLocation that defines location of pre-defined wsdl defining the web service, name that defines name of the web service etc.

---------------------------------------------------------------------------------------
2 @SOAPBinding
Demonstration of @SOAPBinding JAX-WS annotation has already been shown in first program in 1.1. This annotation is used to specify the SOAP messaging style which can either be RPC or DOCUMENT. This style represents the encoding style of message sent to and fro while using the web service.

With RPC style a web service is capable of only using simple data types like integer or string. However, DOCUMENT style is capable of richer data types for a class let’s say Person, which can have attributes like String name, Address address etc.

Document style indicates that in the underlying web service, underlying message shall contain full XML documents, whereas in the RPC style, the underlying message contains parameters and return values in request and response message respectively. By default the style is Document.

The other important optional attribute is use. It represents the formatting style of the web service message. Its value can either be literal or encoded.

Example usage of @SOAPBinding:

@SOAPBinding(style=Style.DOCUMENT, use=Use.LITERAL)

1.3 @WebMethod
@WebMethod JAX-WS annotation can be applied over a method only. This specified that the method represents a web service operation. For its demonstration, please refer to first program in 1.1.

1.4 @WebResult
To understand this JAX-WS annotation, let’s write SEI & SIB again:

WSAnnotationsWebResultI.java

package com.javacodegeeks.examples.jaxWsAnnotations.webresult;
 
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
 
@WebService
@SOAPBinding(style = Style.RPC)
public interface WSAnnotationsWebResultI {
    @WebMethod
    @WebResult(partName="farhenheitResponse")
    float celsiusToFarhenheit(float celsius);
}
WSAnnotationsWebResultImpl.java

package com.javacodegeeks.examples.jaxWsAnnotations.webresult;
 
import javax.jws.WebService;
 
@WebService(endpointInterface="com.javacodegeeks.examples.jaxWsAnnotations.webresult.WSAnnotationsWebResultI")
public class WSAnnotationsWebResultImpl implements WSAnnotationsWebResultI {
    @Override
    public float celsiusToFarhenheit(float celsius) {
        return ((celsius - 32)*5)/9;
    }
}
Now let’s publish this endpoint:

WSPublisher.java

package com.javacodegeeks.examples.jaxWsAnnotations.webresult;
 
import javax.xml.ws.Endpoint;
 
public class WSPublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://127.0.0.1:9999/ctf", new WSAnnotationsWebResultImpl());
    }
}
On publishing the generated WSDL (at URL: http://127.0.0.1:9999/ctf?wsdl) would be like:

<definitions
    xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
    xmlns:wsp="http://www.w3.org/ns/ws-policy" xmlns:wsp1_2="http://schemas.xmlsoap.org/ws/2004/09/policy"
    xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:tns="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://schemas.xmlsoap.org/wsdl/"
    targetNamespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/"
    name="WSAnnotationsWebResultImplService">
    <types />
    <message name="celsiusToFarhenheit">
        <part name="arg0" type="xsd:float" />
    </message>
    <message name="celsiusToFarhenheitResponse">
        <part name="farhenheitResponse" type="xsd:float" />
    </message>
    <portType name="WSAnnotationsWebResultI">
        <operation name="celsiusToFarhenheit">
            <input
                wsam:Action="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/WSAnnotationsWebResultI/celsiusToFarhenheitRequest"
                message="tns:celsiusToFarhenheit" />
            <output
                wsam:Action="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/WSAnnotationsWebResultI/celsiusToFarhenheitResponse"
                message="tns:celsiusToFarhenheitResponse" />
        </operation>
    </portType>
    <binding name="WSAnnotationsWebResultImplPortBinding" type="tns:WSAnnotationsWebResultI">
        <soap:binding transport="http://schemas.xmlsoap.org/soap/http"
            style="rpc" />
        <operation name="celsiusToFarhenheit">
            <soap:operation soapAction="" />
            <input>
                <soap:body use="literal"
                    namespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/" />
            </input>
            <output>
                <soap:body use="literal"
                    namespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/" />
            </output>
        </operation>
    </binding>
    <service name="WSAnnotationsWebResultImplService">
        <port name="WSAnnotationsWebResultImplPort" binding="tns:WSAnnotationsWebResultImplPortBinding">
            <soap:address location="http://127.0.0.1:9999/ctf" />
        </port>
    </service>
</definitions>
Refer to the highlighted line in above wsdl, the part name has been changed to “farhenheitResponse” as was defined using @WebResult. What interest here is that @WebResult can be used to determine what the generated WSDL shall look like.

If we comment out the @WebResult, the WSDL (at URL: http://127.0.0.1:9999/ctf?wsdl) shall be like:

<definitions
    xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
    xmlns:wsp="http://www.w3.org/ns/ws-policy" xmlns:wsp1_2="http://schemas.xmlsoap.org/ws/2004/09/policy"
    xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:tns="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://schemas.xmlsoap.org/wsdl/"
    targetNamespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/"
    name="WSAnnotationsWebResultImplService">
    <types />
    <message name="celsiusToFarhenheit">
        <part name="arg0" type="xsd:float" />
    </message>
    <message name="celsiusToFarhenheitResponse">
        <part name="return" type="xsd:float" />
    </message>
    <portType name="WSAnnotationsWebResultI">
        <operation name="celsiusToFarhenheit">
            <input
                wsam:Action="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/WSAnnotationsWebResultI/celsiusToFarhenheitRequest"
                message="tns:celsiusToFarhenheit" />
            <output
                wsam:Action="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/WSAnnotationsWebResultI/celsiusToFarhenheitResponse"
                message="tns:celsiusToFarhenheitResponse" />
        </operation>
    </portType>
    <binding name="WSAnnotationsWebResultImplPortBinding" type="tns:WSAnnotationsWebResultI">
        <soap:binding transport="http://schemas.xmlsoap.org/soap/http"
            style="rpc" />
        <operation name="celsiusToFarhenheit">
            <soap:operation soapAction="" />
            <input>
                <soap:body use="literal"
                    namespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/" />
            </input>
            <output>
                <soap:body use="literal"
                    namespace="http://webresult.jaxWsAnnotations.examples.javacodegeeks.com/" />
            </output>
        </operation>
    </binding>
    <service name="WSAnnotationsWebResultImplService">
        <port name="WSAnnotationsWebResultImplPort" binding="tns:WSAnnotationsWebResultImplPortBinding">
            <soap:address location="http://127.0.0.1:9999/ctf" />
        </port>
    </service>
</definitions>
In the above program, part name has value return.

Other elements to @WebResult are WebParam.Mode that defines the direction in which parameter is flowing, targetNamespace to define XML namespace for the parameter.

1.5 @WebServiceClient
To understand @WebServiceClient JAX-WS annotation, let us first publish the endpoint written in 1.1 using below program:

WSPublisher.java

package com.javacodegeeks.examples.jaxWsAnnotations.webservice;
 
import javax.xml.ws.Endpoint;
 
public class WSPublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://127.0.0.1:9999/ctf", new WsAnnotationsWebServiceImpl());
    }
}
Before moving further we should understand wsimport utility provided by Java that eases the task of writing client for a SOAP based web service. We won’t go into much detail here about wsimport, instead let’s first save the wsdl file with name ‘ctf.wsdl’ and then write following command on the command prompt:

wsimport -keep -p client ctf.wsdl

The generated code shall have following classes generated:

Client files generated
Client files generated

Now let’s open WsAnnotationsWebServiceImplService.java. It shall be like:

WsAnnotationsWebServiceImplService.java

package client;
 
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
 
/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "WsAnnotationsWebServiceImplService", targetNamespace = "http://webservice.jaxWsAnnotations.examples.javacodegeeks.com/", wsdlLocation = "file:/Users/saurabharora123/Downloads/ctf.wsdl")
public class WsAnnotationsWebServiceImplService extends Service {
 
    private final static URL WSANNOTATIONSWEBSERVICEIMPLSERVICE_WSDL_LOCATION;
    private final static WebServiceException WSANNOTATIONSWEBSERVICEIMPLSERVICE_EXCEPTION;
    private final static QName WSANNOTATIONSWEBSERVICEIMPLSERVICE_QNAME = new QName(
            "http://webservice.jaxWsAnnotations.examples.javacodegeeks.com/", "WsAnnotationsWebServiceImplService");
 
    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/Users/saurabharora123/Downloads/ctf.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WSANNOTATIONSWEBSERVICEIMPLSERVICE_WSDL_LOCATION = url;
        WSANNOTATIONSWEBSERVICEIMPLSERVICE_EXCEPTION = e;
    }
 
    public WsAnnotationsWebServiceImplService() {
        super(__getWsdlLocation(), WSANNOTATIONSWEBSERVICEIMPLSERVICE_QNAME);
    }
 
    public WsAnnotationsWebServiceImplService(WebServiceFeature... features) {
        super(__getWsdlLocation(), WSANNOTATIONSWEBSERVICEIMPLSERVICE_QNAME, features);
    }
 
    public WsAnnotationsWebServiceImplService(URL wsdlLocation) {
        super(wsdlLocation, WSANNOTATIONSWEBSERVICEIMPLSERVICE_QNAME);
    }
 
    public WsAnnotationsWebServiceImplService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, WSANNOTATIONSWEBSERVICEIMPLSERVICE_QNAME, features);
    }
 
    public WsAnnotationsWebServiceImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }
 
    public WsAnnotationsWebServiceImplService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }
 
    /**
     * 
     * @return returns WSAnnotationWebServiceI
     */
    @WebEndpoint(name = "WsAnnotationsWebServiceImplPort")
    public WSAnnotationWebServiceI getWsAnnotationsWebServiceImplPort() {
        return super.getPort(new QName("http://webservice.jaxWsAnnotations.examples.javacodegeeks.com/",
                "WsAnnotationsWebServiceImplPort"), WSAnnotationWebServiceI.class);
    }
 
    /**
     * 
     * @param features
     *            A list of {@link javax.xml.ws.WebServiceFeature} to configure
     *            on the proxy. Supported features not in the
     *            <code>features</code> parameter will have their default
     *            values.
     * @return returns WSAnnotationWebServiceI
     */
    @WebEndpoint(name = "WsAnnotationsWebServiceImplPort")
    public WSAnnotationWebServiceI getWsAnnotationsWebServiceImplPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://webservice.jaxWsAnnotations.examples.javacodegeeks.com/",
                "WsAnnotationsWebServiceImplPort"), WSAnnotationWebServiceI.class, features);
    }
 
    private static URL __getWsdlLocation() {
        if (WSANNOTATIONSWEBSERVICEIMPLSERVICE_EXCEPTION != null) {
            throw WSANNOTATIONSWEBSERVICEIMPLSERVICE_EXCEPTION;
        }
        return WSANNOTATIONSWEBSERVICEIMPLSERVICE_WSDL_LOCATION;
    }
 
}
Refer to the highlighted line in the above generated program that has the annotation @WebServiceClient. The information specified in this annotation helps in identifying a wsdl:service element inside a WSDL document. This element represents the Web service for which the generated service interface provides a client view.

1.6 @RequestWrapper
@RequestWrapper JAX-WS annotation is used to annotate methods in the Service Endpoint Interface with the request wrapper bean to be used at runtime. It has 4 optional elements; className that represents the request wrapper bean name, localName that represents element’s local name, partName that represent the part name of the wrapper part in the generated WSDL file, and targetNamespace that represents the element’s namespace.

Example usage: WSRequestWrapperInterface.java

package com.javacodegeeks.examples.jaxWsAnnotations.wrapper;
 
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.RequestWrapper;
 
@WebService
@SOAPBinding(style=Style.RPC)
public interface WSRequestWrapperInterface {
    @WebMethod
    @RequestWrapper(localName="CTF", 
    targetNamespace="http://javacodegeeks.com/tempUtil", 
    className="com.javacodegeeks.examples.jaxWsAnnotations.webservice.CTF")
    float celsiusToFarhenheit(float celsius);
}
1.7 @ResponseWrapper
@ResponseWrapper JAX-WS annotation is used to annotate methods in the Service Endpoint Interface with the response wrapper bean to be used at runtime. It has 4 optional elements; className that represents the response wrapper bean name, localName that represents element’s local name, partName that represent the part name of the wrapper part in the generated WSDL file, and targetNamespace that represents the element’s namespace.

Example usage: WSRequestWrapperInterface.java

package com.javacodegeeks.examples.jaxWsAnnotations.wrapper;
 
import javax.jws.WebMethod;
import javax.xml.ws.ResponseWrapper;
 
public interface WSResponseWrapperInterfaceI {
    @WebMethod
    @ResponseWrapper(localName="CTFResponse", 
    targetNamespace="http://javacodegeeks.com/tempUtil", 
    className="com.javacodegeeks.examples.jaxWsAnnotations.webservice.CTFResponse")
    float celsiusToFarhenheit(float celsius);
}
1.8 @Oneway
@Oneway JAX-WS annotation is applied to WebMethod which means that method will have only input and no output. When a @Oneway method is called, control is returned to calling method even before the actual operation is performed. It means that nothing will escape method neither response neither exception.

Example usage: WSAnnotationsOnewayI.java

package com.javacodegeeks.examples.jaxWsAnnotations.oneway;
 
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
 
@WebService
@SOAPBinding(style = Style.RPC)
public interface WSAnnotationsOnewayI {
    @WebMethod
    @Oneway
    void sayHello();
}
1.9 @HandlerChain
Web Services and their clients may need to access the SOAP message for additional processing of the message request or response. A SOAP message handler provides a mechanism for intercepting the SOAP message during request and response.

A handler at server side can be a validator. Let’s say we want to validate the temperature before the actual service method is called. To do this our validator class shall implement interface SOAPHandler

TemperatureValidator.java

package com.javacodegeeks.examples.jaxWsAnnotations.handler;
 
import java.util.Set;
 
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
 
public class TemperatureValidator implements SOAPHandler {
 
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        // TODO Auto-generated method stub
        return false;
    }
 
    @Override
    public boolean handleFault(SOAPMessageContext context) {
        // TODO Auto-generated method stub
        return false;
    }
 
    @Override
    public void close(MessageContext context) {
        // TODO Auto-generated method stub
         
    }
 
    @Override
    public Set getHeaders() {
        // TODO Auto-generated method stub
        return null;
    }
 
}
Next we shall implement SOAP handler xml file that may also contain sequence of handlers.

soap-handler.xml

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<javaee:handler-chains xmlns:javaee="http://java.sun.com/xml/ns/javaee"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <javaee:handler-chain>
        <javaee:handler>
            <javaee:handler-class>com.javacodegeeks.examples.jaxWsAnnotations.handler.TemperatureValidator
            </javaee:handler-class>
        </javaee:handler>
    </javaee:handler-chain>
</javaee:handler-chains>
After this we shall configure @HandlerChain JAX-WS annotation in SEI:

WSAnnotationsHandlerChainI.java

package com.javacodegeeks.examples.jaxWsAnnotations.handler;
 
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
 
@WebService
@SOAPBinding(style = Style.RPC)
public interface WSAnnotationsHandlerChainI {
    @HandlerChain(file = "soap-handler.xml")
    @WebMethod
    float celsiusToFarhenheit(float celsius);
}


-----------------
#RestFul::
-------------------------------------
0. What are web services ?
------------------------------------
Web services are client and server applications that communicate over the World Wide Web’s (WWW) HyperText Transfer Protocol (HTTP). Web services provide a standard means of inter operating between software applications running on a variety of platforms and frameworks.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1. What is REST?What does REST stand for?
------------------------------------------------------------- 
->REST is an architectural style of developing web services which take advantage of ubiquity of HTTP protocol and leverages HTTP method to define actions. REST stands for REpresntational State Transfer.
->REST stands for REpresentational State Transfer, which uses HTTP protocol to send data from client to server e.g. a book in the server can be delivered to the client using JSON or XML.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2. What is RESTFul Web Service?
---------------------------------------------------
-> There are two popular way to develop web services, using SOAP (Simple Object Access Protocol) which is XML based way to expose web services and second REST based web services which uses HTTP protocol. Web services developed using REST style is also known as RESTful Web Services.
-> REST is a stateless client-server architecture where web services are resources and can be identified by their URIs. Client applications can use HTTP GET/POST methods to invoke Restful web services. REST doesn’t specify any specific protocol to use, but in almost all cases it’s used over HTTP/HTTPS. When compared to SOAP web services, these are lightweight and doesn’t follow any standard. We can use XML, JSON, text or any other type of data for request and response.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
3.What is a resource? 
----------------------------------
A resource is how data is represented in REST architecture. By exposing entities as the resource it allows a client to read, write, modify, and create resources using HTTP methods e.g. GET, POST, PUT, DELETE etc.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4. What are safe REST operations?
---------------------------------------------------
REST API uses HTTP methods to perform operations. Some of the HTTP operations which doesn't modify the resource at the server is known as safe operations e.g. GET and HEAD. On the other hand, PUT, POST, and DELETE are unsafe because they modify the resource on the server.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5.Can you tell me which API can be used to develop RESTFul web service in Java?
-----------------------------------------------------------------------------------------------------------------
-> There are many framework and libraries out there which helps to develop RESTful web services in Java including JAX-RS which is standard way to develop REST web services. Jersey is one of the popular implementation of JAX-RS which also offers more than specification recommends. Then you also have RESTEasy, RESTlet and Apache CFX.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
6.Have you used Jersey API to develop RESTful services in Java?
-------------------------------------------------------------------------------------------
-> Jersey is one of the most popular framework and API to develop REST based web services in Java. Since many organization uses Jersey they check if candidate has used it before or not. It's simple to answer, say Yes if you have really used and No, if you have not. In case of No, you should also mention which framework you have used for developing RESTful web services e.g. Apache CFX, Play or Restlet.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7.What do you understand by payload in RESTFul?
-------------------------------------------------------------------------
-> Payload means data which passed inside request body also payload is not request parameters. So only you can do payload in POST  and not in GET and DELTE method
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8. Can you do payload in GET method?
-> No, payload can only be passed using POST method.
-------------------------------------
9. Can you do payload in HTTP DELETE?
----------------------------------------------------------
-> No. You can only pass payload using HTTP POST method.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10. How much maximum pay load you could do in POST method?
---------------------------------------------------------------------------------------------
-> If you remember difference between GET and POST request then you know that unlike GET which passes data on URL and thus limited by maximum URL length, POST has no such limit. So, theoretically you can pass unlimited data as payload to POST method but you need to take practical things into account e.g. sending POST with large payload will consume more bandwidth, take more time and present performance challenge to your server.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11. What is difference between SOAP and RESTFul web services?
----------------------------------------------------------------------------------------
SOAP is a standard protocol for creating web services.	REST is an architectural style to create web services.
SOAP is acronym for Simple Object Access Protocol.	REST is acronym for REpresentational State Transfer.
SOAP uses WSDL to expose supported methods and technical details.	REST exposes methods through URIs, there are no technical details.
SOAP web services and client programs are bind with WSDL contract	REST doesn’t have any contract defined between server and client
SOAP web services and client are tightly coupled with contract.	REST web services are loosely coupled.
SOAP learning curve is hard, requires us to learn about WSDL generation, client stubs creation etc.	REST learning curve is simple, POJO classes can be generated easily and works on simple HTTP methods.
SOAP supports XML data format only	REST supports any data type such as XML, JSON, image etc.
SOAP web services are hard to maintain, any change in WSDL contract requires us to create client stubs again and then make changes to client code.	REST web services are easy to maintain when compared to SOAP, a new method can be added without any change at client side for existing resources.
SOAP web services can be tested through programs or software such as Soap UI.	REST can be easily tested through CURL command, Browsers and extensions such as Chrome Postman.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12. If you have to develop web services which one you will choose SOAP OR RESTful and why?
-----------------------------------------------------------------------------------------------------------------------------------
 its easy to develop RESTful web services than SOAP based web services but later comes with some in-built security features.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13.What framework you had used to develop RESTFul services?
-------------------------------------------------------------------------------------
-> This is really experience based question. If you have used Jersey to develop RESTFul web services then answer as Jersey but expect some follow-up question on Jersey. Similarly if you have used Apache CFX or Restlet then answer them accordingly.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14. What are idempotent operations? Why is idempotency important?
------------------------------------------------------------------------------------------
There are some HTTP methods e.g. GET which produce same response no matter how many times you use them e.g. sending multiple GET request to the same URI will result in same response without any side-effect hence it is known as idempotent.

On the other hand, the POST is not idempotent because if you send multiple POST request, it will result in multiple resource creation on the server, but again, PUT is idempotent if you are using it to update the resource.
multiple PUT request to update a resource on a server will give same end result. You can further take HTTP Fundamentals course by Pluralsight to learn more about idempotent methods of HTTP protocol and HTTP in general.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15. Is REST scalable and/or interoperable?
-------------------------------------------------------
Yes, REST is Scalable and interoperable. It doesn't mandate a specific choice of technology either at client or server end. You can use Java, C++, Python or JavaScript to create RESTful Web Services and Consume them at the client end. 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16. What are the advantages of the RestTemplate?
-----------------------------------------------------------------
The RestTemplate class is an implementation of Template method pattern in Spring framework. Similar to other popular template classes e.g. JdbcTemplate or JmsTempalte, it also simplifies the interaction with RESTful Web Services on the client side
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17. Which HTTP methods does REST use?
----------------------------------------------------------
REST can use any HTTP methods but the most popular ones are GET for retrieving a resource, POST for creating a resource, PUt for updating resource and DELETE for removing a resource from the server.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18. What is an HttpMessageConverter in Spring REST? 
-------------------------------------------------------------------------
An HttpMessageConverter is a Strategy interface that specifies a converter that can convert from and to HTTP requests and responses. Spring REST uses this interface to convert HTTP response to various formats e.g. JSON or XML.

Each HttpMessageConverter implementation has one or several MIME Types associated with it. Spring uses the "Accept" header to determine the content type client is expecting.

It will then try to find a registered HTTPMessageConverter that is capable of handling that specific content-type and use it to convert the response into that format before sending to the client.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19. How to create a custom implementation of HttpMessageConverter to support a new type of request/responses? 
---------------------------------------------------------------------------------------------------------------------------------------------------------
You just need to create an implementation of AbstractHttpMessageConverter and register it using the WebMvcConfigurerAdapter#extendMessageConverters() method with the classes which generate a new type of request/response.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20. Is REST normally stateless?
-----------------------------------------
Yes, REST API should be stateless because it is based on HTTP which is also stateless. A Request in REST API should contain all the details required it to process i.e. it should not rely on previous or next request or some data maintained at the server end e.g. Sessions. REST specification put a constraint to make it stateless and you should keep that in mind while designing your REST API.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21. What does @RequestMapping annotation do?
---------------------------------------------------------------
The @RequestMapping annotation is used to map web requests to Spring Controller methods. You can map request based upon HTTP methods  e.g. GET and POST and various other parameters. For examples, if you are developing RESTful Web Service using Spring then you can use produces and consumes property along with media type annotation to indicate that this method is only used to produce or consumers JSON as shown below:

@RequestMapping (method = RequestMethod.POST, consumes="application/json")
public Book save(@RequestBody Book aBook) {
   return bookRepository.save(aBook);
}
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22. Is @Controller a stereotype? Is @RestController a stereotype? 
---------------------------------------------------------------------------------------
Yes, both @Controller and @RestController are stereotypes. The @Controller is actually a specialization of Spring's @Component stereotype annotation. This means that class annotated with @Controller will also be automatically be detected by Spring container as part of container's component scanning process.

And, @RestController is a specialization of @Controller for RESTful web service. It not only combines @ResponseBody and @Controller annotation but also gives more meaning to your controller class to clearly indicate that it deals with RESTful requests.

Spring Framework may also use this annotation to provide some more useful features related to REST API development in future.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
23. . What are HTTP methods that can be used in Restful web services?
----------------------------------------------------------------------------------------------------
RESTful web services use HTTP protocol methods for the operations they perform.
Some important Methods are:
GET : It defines a reading access of the resource without side-effects.This operation is idempotent i.e.they can be applied multiple times without changing the result
PUT : It is generally used for updating resource. It must also be idempotent.
DELETE : It removes the resources. The operations are idempotent i.e. they can get repeated without leading to different results.
POST : It is used for creating a new resource. It is not idempotent.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24. What are differences between Post and Put Http methods?
------------------------------------------------------------------------------------------------
POST :It is used for creating a new resource. It is not idempotent.
PUT : It is generally used for updating resource. It is idempotent.
Idempotent means result of multiple successful request will not change state of resource after initial application
-----------------------------------------------------------------------------------------------------------------------------------------------------------
25. The difference between PUT and PATCH is that:
--------------------------------------------------------------------------
PUT is required to be idempotent. In order to achieve that, you have to put the entire complete resource in the request body.
PATCH can be non-idempotent. Which implies it can also be idempotent in some cases, such as the cases you described.
-------------------------------------------------------------------------------------------------------------------------------------------

-----------------
#Hibernate::
-----------------

@Entity ::-
------------
@Table ::-
------------
@AttributeOverride::-
----------------------
@Column ::-
-------------
@ManyToOne(fetch = FetchType.LAZY)::-
----------------------------------
@JoinColumn ::-
--------------
@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL},mappedBy = "account")::-
------------------------------------------------------------------------------------
@Cascade({org.hibernate.annotations.CascadeType.DELETE_ORPHAN})::-
---------------------------------------------------------------
@Temporal(TemporalType.DATE)::
---------------------------------
 @Transient::
 ---------------
 @LicenceClone
 

-----------------
#Spring ::
-----------------
1.What’s the difference between @Component, @Controller, @Repository & @Service annotations in Spring?
-----------------------------------------------------------------------------------------------------
 @Component: This marks a java class as a bean. It is a generic stereotype for any Spring-managed component. The component-scanning mechanism of spring now can pick it up and pull it into the application context.
-> @Component is a generic stereotype for any Spring-managed component.
-> It is a basic auto component scan annotation which indicates that an annotated class is an auto scan component.

                      Component
                      /  |   \
                     /   |    \
           controller  Service Repository
           
@Controller: This marks a class as a Spring Web MVC controller. Beans marked with it are automatically imported into the Dependency Injection container.

@Service: This annotation is a specialization of the component annotation. It doesn’t provide any additional behavior over the @Component annotation. You can use @Service over @Component in service-layer classes as it specifies intent in a better way.
-> It indicates that an annotated class is a Service component of the business layer.
->Service annotates classes at the service layer

@Repository: This annotation is a specialization of the @Component annotation with similar use and functionality. It provides additional benefits specifically for DAOs. It imports the DAOs into the DI container and makes the unchecked exceptions eligible for translation into Spring DataAccessException.
-> You need to use this annotation within the persistence layer that acts like database repository.
-> Repository annotates classes at the persistence layer, which will act as a database repository
--------------------------------------------------------------------------------------------------------
2.AOP:::
-------------

---------------------
3.Spring Thread Pooling:::
--------------------------

---------------------------
4.Scheduling:::::
---------------------------

---------------------------
5.Spring MVC:::
--------------------

---------------------
6.Transaction management:::
---------------------------

---------------------------
7.JDBC templates:::
---------------------------

---------------------------


-----------------
#Spring Boot::
-----------------
-------------------
1. What is Spring Boot? Why should you use it?
-----------------------------------------------------------------
#Spring Boot Framework is Auto-Dependency Resolution, Auto-Configuration, Management EndPoints, Embedded HTTP Servers(Jetty/Tomcat etc.) and Spring Boot CLI.
#Spring Boot = Auto-Dependency Resolution + Auto-Configuration + Management EndPoints + Embedded HTTP Servers(Jetty/Tomcat).
#Spring Boot Framework is Spring Boot Starter, Spring Boot Auto-Configurator, Spring Boot Actuator, Embedded HTTP Servers, and Groovy.
---Use-- Spring boot makes it easier for you to create Spring application, it can save a lot of time and efforts.
#Spring Boot not only provides a lot of convenience by auto-configuration a lot of things for you but also improves the productivity because it lets you focus only on writing your business logic.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2.What are some important features of using Spring Boot?(Components)
--------------------------------------------------------------------------------------------------
 a.)Spring Boot Starter::: -> Spring Boot Starters are just JAR Files. They are used by Spring Boot Framework to provide “Auto-Dependency Resolution”.
 b.)Spring Boot AutoConfigurator::: -> Spring Boot AutoConfigurator is used by Spring Boot Framework to provide “Auto-Configuration”.
 c.)Spring Boot Actuator::: -> Spring Boot Actuator is used by Spring Boot Framework to provide “Management EndPoints” to see Application Internals, Metrics etc.
  ->  Spring Boot provides actuator to monitor and manage our application. Actuator is a tool which has HTTP endpoints. when application is pushed to production, you can choose to manage and monitor your application using HTTP endpoints.
 d.)Spring Boot CLI::: ->  Spring Boot CLI is Auto Dependency Resolution, Auto-Configuration, Management EndPoints, Embedded HTTP Servers(Jetty, Tomcat etc.) and (Groovy, Auto-Imports).
 -> Spring Boot CLI is Spring Boot Starter, Spring Boot Auto-Configurator, Spring Boot Actuator, Embedded HTTP Servers, and Groovy.
 -> With Spring Boot CLI::: No Semicolons, No Public and private access modifiers, No Imports(Most), No “return” statement, No setters and getters, No Application class with main() method(It takes care by SpringApplication class)., No Gradle/Maven builds. , No separate HTTP Servers.
 e.)Spring Boot Initilizr::: ->  Spring Boot Initilizr is a Spring Boot tool to bootstrap Spring Boot or Spring Applications very easily.
Spring Boot Initilizr comes in the following forms:

Spring Boot Initilizr With Web Interface
Spring Boot Initilizr With IDEs/IDE Plugins
Spring Boot Initilizr With Spring Boot CLI
Spring Boot Initilizr With ThirdParty Tools
---------------------------------------------------------------------------------------------------------------------------------------
3. What is auto-configuration in Spring boot? how does it help? Why Spring Boot is called opinionated?
---------------------------------------------------------------------------------------------------------------------------------------------
->Spring Boot is its ability to automatically configure our application based the jar dependencies we are adding to our classpath. we will be covering Spring Boot Auto Configuration features with an understanding of how this can help in the development lifecycle.
->the point is auto-configuration does a lot of work for you with respect to configuring beans, controllers, view resolvers etc, hence it helps a lot in creating a Java application.
->Spring Boot also provides ways to override auto-configuration settings.
It's also disabled by default and you need to use either @SpringBootApplication or @EnableAutoConfiguration annotations on the Main class to enable the auto-configuration feature.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5. What is the difference between @SpringBootApplication and @EnableAutoConfiguration annotation?
---------------------------------------------------------------------------------------------------------------------------------------------------
-> Even though both are essential Spring Boot application and used in the Main class or Bootstrap class there is a subtle difference between them. The @EnableAutoConfiguration is used to enable auto-configuration but @SpringBootApplication does a lot more than that.
-> It also combines @Configuration and @ComponentScan annotations to enable Java-based configuration and component scanning in your project.
->The @SpringBootApplication is in fact combination of @Configuration, @ComponentScan and @EnableAutoConfiguration annotations. 
-> Many Spring Boot developers like their apps to use auto-configuration, component scan and be able to define extra configuration on their "application class". A single @SpringBootApplication annotation can be used to enable those three features, that is:

@EnableAutoConfiguration: enable Spring Boot’s auto-configuration mechanism
@ComponentScan: enable @Component scan on the package where the application is located 
@Configuration: allow to register extra beans in the context or import additional configuration classes
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
6. What is Spring Initializer? why should you use it?
----------------------------------------------------------------------
One of the difficult things to start with a framework is initial setup, particularly if you are starting from scratch and you don't have a reference setup or project. Spring Initializer addresses this problem in Spring Boot.

It's nothing but a web application which helps you to create initial Spring boot project structure and provides Maven or Gradle build file to build your code.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7. What is actuator in Spring boot?
------------------------------------------------------------------------
Spring boot actuator is one of the most important features of Spring boot. It is used to access current state of running application in production environment. There are various metrics which you can use to check current state of the application.

Spring boot actuator provides restful web services end points which you can simply use and check various metrics.
For example:
/metrics : This restful end point will show you metrics such as free memory, processors, uptime and many more properties,

Spring boot actuator will help you to monitor your application in production environment.Restful end points can be sensitive, it means it will have restricted access and will be shown only to authenticated users. You can change this property by overriding it in application.properties.

Since Spring Boot is all about auto-configuration it makes debugging difficult and at some point in time, you want to know which beans are created in Spring's Application Context and how Controllers are mapped. Spring Actuator provides all that information.

It provides several endpoints e.g. a REST endpoint to retrieve this kind of information over the web. It also provides a lot of insight and metrics about application health e.g. CPU and memory usage, number of threads etc.
------------------------------------------------------------------------------------------------------------------------------------------------------------------
8. What is Spring Boot CLI? What are its benefits?
Spring Boot CLI is a command line interface which allows you to create Spring-based Java application using Groovy. Since it's used Groovy, it allows you to create Spring Boot application from the command line without ceremony e.g. you don't need to define getter and setter method, or access modifiers, return statements etc.
It's also very powerful and can auto-include a lot of library in Groovy's default package if you happen to use it.
For example, if you use JdbcTempalte, it can automatically load that for you.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
9. Where do you define properties in Spring Boot application?
---------------------------------------------------------------------------------------------------
You can define both application and Spring boot related properties into a file called application.properties. You can create this file manually or you can use Spring Initializer to create this file, albeit empty.

You don't need to do any special configuration to instruct Spring Boot load this file. If it exists in classpath then Spring Boot automatically loads it and configure itself and application code according.
For example, you can use to define a property to change the embedded server port in Spring Boot,
-----------------------------------------------------------------------------------------------------------------------------------------------
10. Can you change the port of Embedded Tomcat server in Spring boot? If Yes, How?
-----------------------------------------------------------------------------------------------------------------------------
Yes, we can change the port of Embedded Tomcat Server in Spring Boot by adding a property called server.port in the application.properties file.
this property file is automatically loaded by Spring Boot and can be used to configure both Spring Boot as well as application code.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11. What is the difference between an embedded container and a WAR?
The main difference between an embedded container and a WAR file is that you can Spring Boot application as a JAR from the command prompt without setting up a web server. But to run a WAR file, you need to first set up a web server like Tomcat which has Servlet container and then you need to deploy WAR there.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12. What embedded containers does Spring Boot support?
----------------------------------------------------------------------------------
Spring Boot support three embedded containers: Tomcat, Jetty, and Undertow. By default, it uses Tomcat as embedded containers but you can change it to Jetty or Undertow.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13. What are some common Spring Boot annotations?
---------------------------------------------------------------------------------------
Some of the most common Spring Boot annotations are @EnableAutoConfiguration, @SpringBootApplication, @SpringBootConfiguration, and @SpringBootTest.

The @EnableAutoConfiguration is used to enable auto-configuration on Spring Boot application, while @SpringBootApplication is used on the Main class to allow it to run a JAR file. @SpringBootTest is used to run unit test on Spring Boot environment.
------------------------------------------------------------------------------------------------------------------------------------------------------
14. Can you name some common Spring Boot Starter POMs?
-------------------------------------------------------------------------------------------
Some of the most common Spring Boot Start dependencies or POMs are spring-boot-starter, spring-boot-starter-web, spring-boot-starter-test. You can use spring-boot-starter-web to enable Spring MVC in Spring Boot application.
------------------------------------------------------------------------------------------------------------------------------
15. Can you control logging with Spring Boot? How?
----------------------------------------------------------------------------
Yes, we can control logging with Spring Boot by specifying log levels on application.properties file. Spring Boot loads this file when it exists in the classpath and it can be used to configure both Spring Boot and application code.

Spring Boot uses Commons Logging for all internal logging and you can change log levels by adding following lines in the application.properties file:
logging.level.org.springframework=DEBUG
logging.level.com.demo=INFO
------------------------------------------------------------------------------------------------------------------------------------------------------
16. What is DevTools in Spring boot?
------------------------------------------------------
Spring boot comes with DevTools which is introduced to increase the productivity of developer. You don’t need to redeploy your application every time you make the changes.Developer can simply reload the changes without restart of the server. It avoids pain of redeploying application every time when you make any change. This module will be disabled in production environment.
-----------------------------------------------------------------------------------------------------------
17. How can you implement Spring security in Spring boot application?
-------------------------------------------------------------------------------------------------
Implementation of Spring security in Spring boot application requires very little configuration. You need to add spring-boot-starter-security starter in pom.xml.You need to create Spring config class which will extend WebSecurityConfigurerAdapter and override required method to achieve security in Spring boot application
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18.How can you configure logging in Spring boot application?
---------------------------------------------------------------------------------------
Spring Boot comes with support for Java Util Logging, Log4J2 and Logback and it will be pre-configured as Console output.
Hence,You can simply specify logging.level in application.properties.
logging.level.spring.framework=Debug
----------------------------------------------------------------------------------------------------------------------------------
19. Advantages of Spring Boot:
--------------------------------------------------------
It is very easy to develop Spring Based applications with Java or Groovy.
It reduces lots of development time and increases productivity.
It avoids writing lots of boilerplate Code, Annotations and XML Configuration.
It is very easy to integrate Spring Boot Application with its Spring Ecosystem like Spring JDBC, Spring ORM, Spring Data, Spring Security etc.
It follows “Opinionated Defaults Configuration” Approach to reduce Developer effort
It provides Embedded HTTP servers like Tomcat, Jetty etc. to develop and test our web applications very easily.
It provides CLI (Command Line Interface) tool to develop and test Spring Boot(Java or Groovy) Applications from command prompt very easily and quickly.
It provides lots of plugins to develop and test Spring Boot Applications very easily using Build Tools like Maven and Gradle
It provides lots of plugins to work with embedded and in-memory Databases very easily.

What are disadvantages of Spring boot?
If you want to convert your old spring application to Spring boot application, it may not be straight forward and can be time consuming.
----------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------
#Design Pattern::
--------------------
1.Singleton class in java.
-----------------------------

------------------------------
2.Factory method :::
----------------------

--------------------
3.Abstract Factory Pattern::
-----------------------------

-----------------------------
4.Facede Design Pattern::
---------------------------

--------------------------------------------------------------------------------------------------------
#JUnit/TestNG::: Unit Testing :::: Integration Testing ::: Regression testing:::: DEBUGGING::: BUG FIX:::
--------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
#JSON:::
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
#GIT:::  #GitLab::: #Putty:::  #LOG4J::: #Maven::: #Gradle::: #Tomcat::: #JBOSS:::
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
#Linux (Shell Script):::
--------------------------------------------------------------------------------------------------------

#Data Structure & Algorithms ::
--------------------------------------------------------------------------------------------------------
Arrays::-
-----
Array is a collection of homogeneous data elements. It is a very simple data structure. The elements of an array are stored in successive memory location. Array is refered by a name and index number. Array is nice because of their simplicity and well suited for situations where the number is known. Array operation :

Traverse
Insert
Delete
Sort
Search  


There are two types of array. One dimensional array and multi dimensional array.One dimensional array This type of array of array represent and strore in linear form. Array index start with zero.

Declaration : datatype arrayname[size];
                        int arr[10];

Input array : for(int i=0; i<10; i++)  cin>>arr[i];

We can use store integer type of data to the array arr using above segment.

----------

Traverse : Traversing can easy in linera array. Algorithm:


C++ implement :
void traverse(int arr[])
{
       for(int i=0; i<10; i++)   cout<<arr[i];
}

----------

Insertion : Inserting an element at the end of a linear array can be easily done provided the memory space space allocated for the array is large enough to accommodate the additional element. Inserting an element in the middle . . Algorithm : Insertion(arr[], n, k, item) here arr is a linear array with n elements and k is index we item insert. This algorithm inserts an item to kth in index in arr.

Step 1:Start
Step 2: Repeat for i=n-1 down to k(index)
   Shift the element dawn by one position] arr[i+1]=arr[i];
  [End of the loop]
Step 3: set arr[k] = item
Step 4: n++; Step 5 : Exit.


C++ implement :
void insert(int arr[], int n, int k, int item)
{
   for(int i=n-1; i>=k; i--) 
      {
         arr[i]=arr[i+1];
      }
      arr[k] = item;
      n++;
}

----------

Deletion : Deletion is very easy on linear array.

Algorithm : Deletion(arr, n, k) Here arr is a linear array with n number of items. K is position of elememt which can be deleted.
Step 1:Start
Step 2: Repeat for i=k upto n
       [Move the element upword]  arr[i]=arr[i+1];
      [End of the loop]
Step 3: n--;
Step 4 : Exit.


C++ implementation :
 void deletion(int arr[], int n, int k)
 { 
      for(int i=k; i<n; i++) 
       { 
            arr[i] = arr[i+1]; 
       } 
      n--; 
  }

----------

Searching : Searching means find out a particular element in linear array. Linear seach and binary search are common algorithm for linear array. We discuss linear search and binary search.

Linear search Algorithm : Linear search is a simple search algorithm that checks every record until it finds the target value
Algorithm: LinearSeach(arr, n, item)
Step 1:Start.
Step 2: Initialize loc=0;
Step 3: Repeat for i=0 upto n-1 if(arr[i]==item) loc++; [End of the loop]
Step 4: if loc is not zero then print found otherwise print not found.
Step 5 : Exit.

C++ implementation :
  void linear search(int arr[], int n, item)
   {
      for(int i=0; i<n-1; i++)  
       {
         if(arr[i]==item) loc++;
       }

      if(loc) cout<<"Found"<<endl;
      else cout<<"Not found"<<endl 
   }
   
   -------------------------------------------------------------------
			Two Sum II - Input array is sorted
			----------------------------------
			Input: numbers = [2,7,11,15], target = 9
			Output: [1,2]
			Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
		
Implementation::::--
		
			public int[] twoSum(int[] nums, int target) {
				int left=0;
				int right = nums.length-1;
				while(nums[left] + nums[right] != target){
				if(nums[left] + nums[right] >target)
					right--;
				else
					left++;
				}
				return new int[]{left+1,right+1};
			}
   
		

----------

Binary search : Binary search is available for sorted array. It compares the target value to the middle element of the array; if they are unequal, the half in which the target cannot lie is eliminated and the search continues on the remaining half until it is successful.

Algorithm : BinarySeach(arr, n, item)
Step 1:Start
Step 2: Initialize low = 0 and high = n-1;
Step 3: While loop low<=high
    mid = (low + high)/2;  
    if (a[mid] == item) return mid;
    else if (a[mid] < item) low = mid + 1;
     else high = mid - 1;
Step 4: If item is not found in array return -1. Step 5: End.


C++ implementation : 
 int binarySearch(int[] a, int n, int item)
 {  
    int low = 0;
    int high = n - 1;
    while(low<=high){
    {
      int mid = (low + high)/2;
      if (a[mid] == item) return mid;
      else if (a[mid] < item) low = mid + 1;
      else high = mid - 1;
    } 
    return -1;
 }


--------------------
Sorting : There are various sorting algorithm in linear array. We discuss bubble sort and quick sort in this post.
Bubble Sort: Bubble sort is a example of sorting algorithm . In this method we at first compare the data element in the first position with the second position and arrange them in desired order.Then we compare the data element with with third data element and arrange them in desired order. The same process continuous until the data element at second last and last position.

Algorithm : BubbleSort(arr,n)
Step 1:Start
Step 2: Repeats i=0 to n      
Step 3: Repeats j=0 to n       
         if(arr[j]>arr[j+1]) then interchange arr[j] and arr[j+1]
         [End of inner loop]
        [End of outer loop]
Step 4: Exit.

C++ implement :
 void BubbleSort(int arr, int n)
 {
    for(int i=0; i<n-1; i++)
    {
        for(int j=0; j<n-1; j++)
        {
           if(arr[j]>arr[j+1])     swap(arr[j],arr[j+1]);
        }
     }
 }

Quick Sort:
 Quick sort is a divide and conquer paradism. In this paradism one element is to be chosen as partitining element . 
We divide the whole list array into two parts with respect to the partitiong elemnt . The data which are similar than or equal to the partitining element remain in
 the first part and data data which are greater than the partitioning element
 remain in the second part. If we find any data which is greater than the partitioning value that will be transfered to the second part., If we find any data whichis smaller than the partitioning element that will be transferred to first part. 
Transferring the data have been done by exchanging the position of the the data 
found in first and second part. By repeating this process , 
we can sort the whole list of data.

Algorithm: QUICKSORT(arr, l, h) 
if  l<h then pi ← PARTITION(A, l, h) 
QUICKSORT(A, l, pi–1) 
QUICKSORT(A, pi+1, h)

C++ implementation :

 int partition(int arr[], int start, int end)
 {
    int pivotValue = arr[start];
    int pivotPosition = start;
     for (int i=start+1; i<=end; i++)  
     {
        if (pivotValue > arr[i])
       {
          swap(arr[pivotPosition+1], arr[i]);
          swap(arr[pivotPosition] , arr[pivotPosition+1]);
          pivotPosition++;
       }
     }
    return pivotPosition;
  }


 void quickSort(int arr[], int low, int high)
 {
   if (low < high)
   {
      int pi = partition(arr, low, high);
      quickSort(arr, low, pi - 1);
      quickSort(arr, pi + 1, high);
   }
 }






C++ example for simple sorting program with stl function:

 #include <bits/stdc++.h>
 using namespace std;

 int main()
 {
   int  n, arr[100];
   cin >> n;
   for(int i=0; i<n; i++) 
   {
     cin>>arr[i];
   }

   sort(arr, arr + n);

   for (int i=0; i<n; i++) 
   {
      cout<<arr[i]<<" ";
   }
   cout<<endl;

   return 0;
 }
 
 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1.How To Find Duplicates In Array In Java?
----------------------------------------
public static void findDuplicate(String[] str){
Set<String> set = new HashSet<String>();
for(String s : str){
if(set.add(s) ==false){
    Sopln(s);
  }
 }
}
------------------------------------------------------------------------------------------------------
Sorting:::-
-------------
1. Merge Sort::- Merge sort is a “divide and conquer” algorithm where in we first divide the problem into subproblems. 
Time Complexity::-O(nLogn). Space Complexity:: O(n).
---------------------------------------------------
class MergeSort 
{ 
	void merge(int arr[], int l, int m, int r) { 
		int n1 = m - l + 1; 
		int n2 = r - m; 
		int L[] = new int [n1]; 
		int R[] = new int [n2]; 

		for (int i=0; i<n1; ++i) 
			L[i] = arr[l + i]; 
		for (int j=0; j<n2; ++j) 
			R[j] = arr[m + 1+ j]; 

		int i = 0, j = 0; 
		int k = l; 
		while (i < n1 && j < n2) 
		{ 
			if (L[i] <= R[j]) 
			{ 
				arr[k] = L[i]; 
				i++; 
			} 
			else
			{ 
				arr[k] = R[j]; 
				j++; 
			} 
			k++; 
		} 
		while (i < n1) 
		{ 
			arr[k] = L[i]; 
			i++; 
			k++; 
		} 
		while (j < n2) 
		{ 
			arr[k] = R[j]; 
			j++; 
			k++; 
		} 
	} 
	void sort(int arr[], int l, int r) 
	{ 
		if (l < r) 
		{ 
			int m = (l+r)/2; 
			sort(arr, l, m); 
			sort(arr , m+1, r); 
			merge(arr, l, m, r); 
		} 
	} 
	static void printArray(int arr[]) 
	{ 
		int n = arr.length; 
		for (int i=0; i<n; ++i) 
			System.out.print(arr[i] + " "); 
		System.out.println(); 
	} 
	public static void main(String args[]) 
	{ 
		int arr[] = {12, 11, 13, 5, 6, 7}; 

		System.out.println("Given Array"); 
		printArray(arr); 

		MergeSort ob = new MergeSort(); 
		ob.sort(arr, 0, arr.length-1); 

		System.out.println("\nSorted array"); 
		printArray(arr); 
	} 
} 



---------------------------------------------------
2.Quick Sort :::- is Divide and Conquer Algorithms
--------------------------------------------------


--------------------------------------------------

-------------
Strings:::-
------------

------------
Searching:::
------------

------------
Dictionaries and HashMaps:::
----------------------------

----------------------------
Greedy Algorithms:::
----------------------

-----------------------
Dynamic Programming:::
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Easy::
------
Maximum Subarray::-Largest Sum Contiguous SubarrayS
------------------------------------------------------
Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6
---------------
public static int maxSubArray(int[] A){
    int maxCurr = A[0];
    int maxEnd = A[0];
	for(int i = 1;i<A.length;i++){
	maxEnd = Math.max(maxEnd+A[i],A[i]);
	maxCurr = Math.max(maxCurr,maxEnd);
	}
	return maxCurr;
}
--------------------------------------------------------------------------
Minimum Size Subarray Sum::
------------------------------------------------------------------------
Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under the problem constraint.
------------------------------------------------------------------------------------
public int minSubArrayLen(int s, int[] a) {
  if (a == null || a.length == 0)
    return 0;
  
  int i = 0, j = 0, sum = 0, min = Integer.MAX_VALUE;
  
  while (j < a.length) {
    sum += a[j++];
    
    while (sum >= s) {
      min = Math.min(min, j - i);
      sum -= a[i++];
    }
  }
  
  return min == Integer.MAX_VALUE ? 0 : min;
}
------------------------------------------------------------------------------------------------------------------------------------
Smallest subarray with sum greater than a given value:::
--------------------------------------------------------------
arr[] = {1, 4, 45, 6, 0, 19}
   x  =  51
Output: 3
Minimum length subarray is {4, 45, 6}
--------------------------------------------------------------------------------------------------------------------------------
   static int smallestSubWithSum(int arr[], int n, int x){ 
        int curr_sum = 0, min_len = n + 1; 
        int start = 0, end = 0; 
        while (end < n){ 
            while (curr_sum <= x && end < n) 
                curr_sum += arr[end++]; 
            while (curr_sum > x && start < n)  
            { 
                if (end - start < min_len) 
                    min_len = end - start; 
                curr_sum -= arr[start++]; 
            } 
        } 
        return min_len; 
    } 
----------------------------------------------------------------------------------------------------------------------------------
 Decode Ways::::
 ------------------------------------
 Input: "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).
----------------------------------------------------------------------------------
 public int numDecodings(String s) {
        int n = s.length();
        if (n == 0) return 0;
        
        int[] memo = new int[n+1];
        memo[n]  = 1;
        memo[n-1] = s.charAt(n-1) != '0' ? 1 : 0;
        
        for (int i = n - 2; i >= 0; i--)
            if (s.charAt(i) == '0') continue;
            else memo[i] = (Integer.parseInt(s.substring(i,i+2))<=26) ? memo[i+1]+memo[i+2] : memo[i+1];
        
        return memo[0];
    }
-----------------------------------------------------------------------------------------------------------
Decode Ways II :::
-----------------------------------------
Input: "*"
Output: 9
Explanation: The encoded message can be decoded to the string: "A", "B", "C", "D", "E", "F", "G", "H", "I".
Example 2:
Input: "1*"
Output: 9 + 9 = 18
-----------------------------------------------------------------------------------------------------------------
class Solution {
    int M = 1000000007;
    public int numDecodings(String s) {
        long first = 1, second = s.charAt(0) == '*' ? 9 : s.charAt(0) == '0' ? 0 : 1;
        for (int i = 1; i < s.length(); i++) {
            long temp = second;
            if (s.charAt(i) == '*') {
                second = 9 * second;
                if (s.charAt(i - 1) == '1')
                    second = (second + 9 * first) % M;
                else if (s.charAt(i - 1) == '2')
                    second = (second + 6 * first) % M;
                else if (s.charAt(i - 1) == '*')
                    second = (second + 15 * first) % M;
            } else {
                second = s.charAt(i) != '0' ? second : 0;
                if (s.charAt(i - 1) == '1')
                    second = (second + first) % M;
                else if (s.charAt(i - 1) == '2' && s.charAt(i) <= '6')
                    second = (second + first) % M;
                else if (s.charAt(i - 1) == '*')
                    second = (second + (s.charAt(i) <= '6' ? 2 : 1) * first) % M;
            }
            first = temp;
        }
        return (int) second;
    }

}
------------------------------------------------------------------------------------------------------------------------------
Maximum Product Subarray:::
--------------------------------------------------
Input: [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
--
Input: [-2,0,-1]
Output: 0
Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
---------------------------------------------------------------------------------
public int maxProduct(int[] A) {
    if (A.length == 0) {
        return 0;
    }
    
    int maxherepre = A[0];
    int minherepre = A[0];
    int maxsofar = A[0];
    int maxhere, minhere;
    
    for (int i = 1; i < A.length; i++) {
        maxhere = Math.max(Math.max(maxherepre * A[i], minherepre * A[i]), A[i]);
        minhere = Math.min(Math.min(maxherepre * A[i], minherepre * A[i]), A[i]);
        maxsofar = Math.max(maxhere, maxsofar);
        maxherepre = maxhere;
        minherepre = minhere;
    }
    return maxsofar;
}
------------------------------------------------------------------------------------------------------------------------------------
Longest Palindromic Substring :::
----------------------------------
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.

Input: "cbbd"
Output: "bb"
---------------------------------------
class Solution {
    private int lo, maxLen;
public String longestPalindrome(String s) {
	int len = s.length();
	if (len < 2)
		return s;
	
    for (int i = 0; i < len-1; i++) {
     	extendPalindrome(s, i, i);
     	extendPalindrome(s, i, i+1);
    }
    return s.substring(lo, lo + maxLen);
}
private void extendPalindrome(String s, int j, int k) {
	while (j >= 0 && k < s.length() && s.charAt(j) == s.charAt(k)) {
		j--;
		k++;
	}
	if (maxLen < k - j - 1) {
		lo = j + 1;
		maxLen = k - j - 1;
	}
}
}
----------------------------------------------------------------------------------------------
Shortest Palindrome:::
-----------------------
Given a string s, you are allowed to convert it to a palindrome by adding characters in front of it. Find and return the shortest palindrome you can find by performing this transformation.

Input: "aacecaaa"
Output: "aaacecaaa"

Input: "abcd"
Output: "dcbabcd"
-----------------------------------------------------
 public String shortestPalindrome(String s) {
     int j = 0;
    for (int i = s.length() - 1; i >= 0; i--) {
        if (s.charAt(i) == s.charAt(j)) { j += 1; }
    }
    if (j == s.length()) { return s; }
    String suffix = s.substring(j);
    return new StringBuffer(suffix).reverse().toString() + shortestPalindrome(s.substring(0, j)) + suffix;   
    }
------------------------------------------------------------------------------------------------------------


--------------------
Tries:::
------------------

------------------
Graphs::::
-------------------------

-------------------------
Trees:::
------------------------

-----------------------
Linked List:::::
-----------------------

-----------------------
Recursion  Backtracking::
------------------------

--------------
Stack and Queue:::
--------------------

-------------------
Heaps:::
-----------------

----------------
Divide and Conquer:::
---------------------

----------------------
Bit Manipulation:::
-------------------

--------------------------------------------------------------------------------------------------------
#Mathmatics::::
---------------------------------------------------------------------------------------------------------
Fundamentals:::
----------------------

----------------------
Basic math operations (addition, subtraction, multiplication, division, exponentiation)
---------------------------------------------------------------------------------------
Fast Modulo Multiplication (Exponential Squaring)
-------------------------------------------------
-> Fast Modulo Multiplication (also known as Exponential Squaring or Repeated Squaring).
   This is a very useful technique to have under your arsenal as a competitive programmer, especially because such technique often appears on Maths related problems and, sometimes,
   it might be the difference between having AC veredict or TLE veredict, so, for someone who still doesn't know about it, I hope this tutorial will help :)

Main reason why the usage of repeated squaring is useful
--------------------
This technique might seem a bit too complicated at a first glance for a newbie, after all, say, we want to compute the number 310. We can simply write the following code and do a simple for loop:

#include <iostream>
using namespace std;

int main()
{
    int base = 3;
    int exp = 10;
    int ans = 1;
    for(int i = 1; i <= exp; i++)
    {
        ans *= base;
    }
    cout << ans;
    return 0;
}

The above code will correctly compute the desired number, 59049. However, after we analyze it carefully, we will obtain an insight which will be the key to develop a faster method.

Apart from being correct, the main issue with the above method is the number of multiplications that are being executed.

We see that this method executes exactly exp-1 multiplications to compute the number n^exp, which is not desirable when the value of exp is very large.

Usually, on contest problems, the idea of computing large powers of a number appears coupled with the existence of a modulus value,
i.e., as the values being computed can get very large, very fast, the value we are looking to compute is usually something of the form:

n^exp % M ,

where M is usually a large prime number (typically, 10^9 + 7).

Note that we could still use the modulus in our naive way of computing a large power: we simply use modulus on all the intermediate steps and take modulus at the end, to ensure every calculation is kept within the limit of "safe" data-types.

The fast-exponentiation method: an implementation in C++
---------------------------------------------------
It is possible to find several formulas and several ways of performing fast exponentiation by searching over the internet, but, there's nothing like implementing them on our own to get a better feel about what we are doing :)

I will describe here the most naive method of performing repeated squaring. As found on Wikipedia, the main formula is:

	  { x(x^2)^n-1/2, if n is odd
x^n = { (x^2)^n/2 , if n is even.
      {
A brief analysis of this formula (an intuitive analysis if you prefer), based both on its recursive formulation and on its implementation allows us to see that the formula uses only O(log2n) squarings and O(log2n) multiplications!

This is a major improvement over the most naive method described in the beginning of this text, where we used much more multiplication operations.

Below, I provide a code which computes base^exp % 1000000007, based on wikipedia formula:

long long int fast_exp(int base, int exp)
{
    if(exp==1)
    return base;
    else
    {
        if(exp%2 == 0)
        {
            long long int base1 = pow(fast_exp(base, exp/2),2);
            if(base1 >= 1000000007)
            return base1%1000000007;
            else
            return base1;
        }
        else
        {
            long long int ans = (base*  pow(fast_exp(base,(exp-1)/2),2));
            if(ans >= 1000000007)
            return ans%1000000007;
            else
            return ans;
        }
    }
}
The 2^k-ary method for repeated squaring
Besides the recursive method detailed above, we can use yet another insight which allows us to compute the value of the desired power.

The main idea is that we can expand the exponent in base 2 (or more generally, in base 2^k, with k >= 1) and use this expansion to achieve the same result as above, but, this time, using less memory.


ll fast_exp(int base, int exp) {
    ll res=1;
    while(exp>0) {
       if(exp%2==1) res=(res*base)%MOD;
       base=(base*base)%MOD;
       exp/=2;
    }
    return res%MOD;
}
These are the two most common ways of performing repeated squaring on a live contest and I hope you have enjoyed this text and have found it useful :)

-------------------------------------------------------------------------------------------
------------------------
Number Theory :::
-----------------------

--------------------------
Permutaion & Combinatorics
---------------------------

-------------------------------
Algebra:::
-------------------------------

------------------------------
Geometry:::
---------------------------------------------------------

--------------------------------------------------------------------------------------------------------
Probability::::
-------------------------------------------

-------------------------------------------
Linear Algebra Foundations:::
--------------------------------

---------------------------------
Statistics:::
-----------------

-----------------

-------------------------------------------------------------------------------------------------------
#JavaScript:::
------------------------------------------------------------------------------------------------------
#Security(Functions, Terminology and Concepts, Cryptography)::
--------------------------------------------------------------
#Microservices Architecture::#Microservices Course
----------------------------------------------------
1.Evolution of Microservices
-----------------------------
Learning Objectives: In this Module, you will learn how Microservices have evolved over time and how different is Microservices from SOA.
In addition, you will get to know about different architectures and where does Microservices architecture fit. 

Topics:
Monolithic Architecture
Distributed Architecture
Service oriented Architecture
Microservice and API Ecosystem
Microservices in nutshell
Point of considerations
SOA vs. Microservice
Microservice & API

Skills:
Architecture styles
Advantages of different architecture styles
Limitations of Architectures
What is Microservices
------------------------------------------------
2.Microservices Architecture
--------------------------------------------------
Learning Objectives: Learn the various principles of REST, the various characteristics of Microservices, the importance of messaging in Microservices architecture, and the concept of distributed transactions. 

Topics:
REST Architecture principles
Microservice Characteristics
Inter-Process Communications
Microservice Transaction Management

Skills:
Considerations while building microservices  
How the services communicate with each other
How the transaction management is done in microservice.
-----------------------------------------------------------
3.Microservices - Design
-------------------------
Learning Objectives: This Module gives you an insight into Domain Driven Design, the approach called Big Ball of Mud, the approaches and their strategies that can be used while moving from Monolithic to Microservices. 

 Topics:
Domain Driven Design
Big Mud Ball to Sweet Gems
Untangling the Ball of MUD
Kill the MUD Ball growth
Repackaging/Refactoring
Decouple the User interface and Backend Business Logic
MUD Ball to Services
Microservice Design Patterns
Microservice Architecture Decisions

Hands-on:
Setting up the root project
Spring Boot - Hello World
Returning json entity as response
Spring Boot dev tools
Intro to Lombok
Adding Items to Mongo DB
Querying Mongo
Accessing an SQL database
Spring Data Rest and HATEOAS
Connecting to an Elasticsearch Server
Searching our Elasticsearch Server

Skills:
Architecture Decisions
Monolithic to Microservices redesign.
Learn to identify and design microservices.
-----------------------------------------------
4.Microservices - Security
------------------------------------------------
Learning Objectives: Know why security is an important factor to be considered in Microservices. Learn what are the various best practices in Microservice security design, and what techniques can be used to implement security. 

Topics:
Why is Security important? 
Microservice Security Principles
Microservice Security techniques
Access Tokens
Oauth 2.0
How to secure a Microservice using OAuth 2.0

Hands-on:
Spring Boot Security Setup
Basic Spring security
Moving to Oauth2
Implementing Single Sign On
Implementing Authorization Server
Implementing Resource Server

Skills:
Oauth 2.0
Security tokens
Secure by design
------------------------------------------------------
5.Microservices - Testing
Learning Objectives: Learn the different testing strategies that can be implemented in Microservices, how Spring Boot features help in testing Microservices, and the various testing tools that are available to be used.


Topics:
Testing scenarios and strategy
Test at Different Levels
Testing Best Practice for Microservices

Skills:
Testing methodology
How to test Microservices
------------------------------------------------
6. Microservices Reference Architecture
Learning Objectives: Get an insight into Microservices reference architecture, what are the key Microservice enablers and how do DevOps and Microservice go hand in hand. In addition, know what features an API system provide to Microservices, and how Netflix has benefited by implementing Microservices. 

Topics:
Reference Architecture
Microservice Enablerc
Microservices @ Netflix

Hands-on:
Reading properties in various ways
Implementing config server
Setting up Discovery Server
Setting up Discovery Client
Overview of Actuator Endpoints
API Gateway and Dynamic Routing
IDeclarative Rest Client
Hystrix Fault Tolerance
Distributed Caching
Distributed Sessions
Need for Event Driven Systems
Building Event Driven Systems
Implementing Distributed Tracing
Understanding Metrics
Monitoring Microservices
Spring Boot Admin

Skills:
Scalable Architecture
How Netflix uses Microservices
How cloud and DevOps enables Microservice architecture
----------------------------------------------------------------------------------------


