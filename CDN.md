CDN::-
-----------

Q1: What is a CDN?->Entry
--------------------------
A CDN (content delivery network) is essentially a group of servers that are strategically placed across the globe with 
the purpose of accelerating the delivery of your static web content.

For website owners who have visitors in multiple geographic locations, content will be delivered faster to these users 
as there is less distance to travel. CDN users also benefit from the ability to easily scale up and down much more easily 
due to traffic spikes. On average, 80% of a website consist of static resources therefore when using a CDN, there is much 
less load on the origin server.

Q2: Why use a CDN (Content Delivery Network)? -> Junior 
----------------------------------------------------------
CDNs are very useful for a multiple reasons:

It simply Decrease Server Load - On average, 80% of a website consist of static resources therefore when using a CDN, there is much less load on the origin server.
Make Faster Content Delivery - For website owners who have visitors in multiple geographic locations, content will be delivered faster to these users as there is less distance to travel.
100 Percent Availability - CDN users also benefit from the ability to easily scale up and down much more easily due to traffic spikes.


Q3: Name some advantages of using CDN for static JS files and assets? -> Junior 
--------------------------------------------------------------------------------
It increases the parallelism available. (Most browsers will only download 3 or 4 files at a time from any given site.)

It increases the chance that there will be a cache-hit. (As more sites follow this practice, more users already have the file ready.)

It ensures that the payload will be as small as possible. (Google can pre-compress the file in a wide array of formats (like GZIP or DEFLATE). This makes the time-to-download very small, because it is super compressed and it isn't compressed on the fly.)

It reduces the amount of bandwidth used by your server. (Google is basically offering free bandwidth.)

It ensures that the user will get a geographically close response. (Google has servers all over the world, further decreasing the latency.)

(Optional) They will automatically keep your scripts up to date. (If you like to "fly by the seat of your pants," you can always use the latest version of any script that they offer. These could fix security holes, but generally just break your stuff.)



Q4: What is a CDN origin server? -> Junior 
-------------------------------------------
The origin server is the primary source of your website's data and where your website files are hosted.

For example, if you are using DigitalOcean to host your site's files and have chosen data centre in San Francisco, then your origin server would be based in San Francisco.

Being limited to one server to deliver files from can be quite inefficient as distance is a contributing factor to latency.





Q5: What are CDN edge servers? -> Mid 
---------------------------------------


Q6: Why and how to use Cache Busting? -> Mid 
Q7: What Is Cache Busting? -> Mid 
Q8: How does CDN caching work? -> Senior 
Q9: Explain why CDN (in)availability may be a problem for using WebSockets? -> Expert 
