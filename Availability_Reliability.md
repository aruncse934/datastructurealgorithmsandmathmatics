Q1: What is Availability?  
Add to PDF Entry 
Q2: What is Reliability?  
Add to PDF Junior 
Q3: What is Back-Pressure?  Related To: Software Architecture
 Add to PDF Mid 
Q4: How Do You Update A Live Heavy Traffic Site With Minimum Or Zero Down Time?  Related To: Software Architecture
 Add to PDF Mid 
Q5: What Do You Mean By High Availability (HA)?  Related To: Software Architecture
 Add to PDF Mid 
Q6: What does it mean "System Shall Be Resilient"?  Related To: Software Architecture
 Add to PDF Mid 
Q7: What is Fail-over?  
 Add to PDF Mid 
Q8: Explain Failure in Contrast to Error  Related To: Software Architecture
 Add to PDF Senior 
Q9: How to choose between CP (consistency) and AP (availability)?  Related To: CAP Theorem
 Add to PDF Senior 
Q10: Explain how does Active-Passive Fail-over work?  
 Add to PDF Senior 
Q11: What is Active-Active Fail-over?  
 Add to PDF Senior 
Q12: Compare "Fail Fast" vs "Robust" approaches of building software  Related To: Software Architecture
 Add to PDF Senior 
Q13: Explain how to calculate Availability of multiple system components  
 Add to PDF Expert 
Q14: What is a crashloop?  