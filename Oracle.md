Oracle:::
-----------------
Oracle Joins
------------
Join is a query that is used to combine rows from two or more tables, views, or materialized views. It retrieves data from multiple tables and creates a new table.

Join Conditions
---------------
There may be at least one join condition either in the FROM clause or in the WHERE clause for joining two tables. It compares two columns from different tables and combines pair of rows, each containing one row from each table, for which join condition is true.

Types of Joins
--------------
	Inner Joins (Simple Join)
	Outer Joins
	-----------
			Left Outer Join (Left Join)
			Right Outer Join (Right Join)
			Full Outer Join (Full Join)
	Equijoins
	Self Joins
	Cross Joins (Cartesian Products)
	Antijoins
	Semijoins:::
	------------
	Semi-join is introduced in Oracle 8.0. It provides an efficient method of performing a WHERE EXISTS sub-query.
	A semi-join returns one copy of each row in first table for which at least one match is found.
	Semi-joins are written using the EXISTS construct.
	---------------------------------------------------------------------------------------------------------------
	Oracle Semi Join Example
	Let's take two tables "departments" and "customer"
	Departments table

	CREATE TABLE  "DEPARTMENTS"
   (    "DEPARTMENT_ID" NUMBER(10,0) NOT NULL ENABLE,
    "DEPARTMENT_NAME" VARCHAR2(50) NOT NULL ENABLE,
     CONSTRAINT "DEPARTMENTS_PK" PRIMARY KEY ("DEPARTMENT_ID") ENABLE
   );

	Customer table

	CREATE TABLE  "CUSTOMER"
	   (    "CUSTOMER_ID" NUMBER,
		"FIRST_NAME" VARCHAR2(4000),
		"LAST_NAME" VARCHAR2(4000),
		"DEPARTMENT_ID" NUMBER
	   );

	   Execute this query

	SELECT   departments.department_id, departments.department_name
			FROM     departments
			WHERE    EXISTS
					 (
					 SELECT 1
					 FROM   customer
					 WHERE customer.department_id = departments.department_id
					 )
			ORDER BY departments.department_id;
--------------------------------------------------