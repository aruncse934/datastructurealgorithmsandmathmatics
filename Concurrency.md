Q1: What is a Deadlock?  
Add to PDF Junior 
Q2: Explain the difference between Asynchronous and Parallel programming?  
Add to PDF Junior 
Q3: What is a Mutex?  
Add to PDF Junior 
Q4: Is there any difference between a Binary Semaphore and Mutex?  
 Add to PDF Mid 
Q5: What is a Race Condition?  
 Add to PDF Mid 
Q6: Write a function that guarantees to never return the same value twice  Related To: Brain Teasers
 Add to PDF Mid 
Q7: Explain Deadlock to 5 years old  
 Add to PDF Mid 
Q8: What is the meaning of the term “Thread-Safe”?  
 Add to PDF Mid 
Q9: How much work should I place inside a lock statement?  
 Add to PDF Mid 
Q10: What is the difference between Concurrency and Parallelism?  Related To: Software Architecture
 Add to PDF Mid 
Q11: What's the difference between Deadlock and Livelock?  
 Add to PDF Senior 
Q12: What are some advantages of Lockless Concurrency?  
 Add to PDF Senior 
Q13: What is Green Thread?  
 Add to PDF Senior 
Q14: What is a Data Race?  
 Add to PDF Senior 
Q15: What is Starvation?  
 Add to PDF Senior 
Q16: Explain what is a Race Condition to 5 years old  
 Add to PDF Senior 
Q17: Provide some real-live examples of Livelock  
 Add to PDF Senior 
Q18: Two customers add a product to the basket in the same time whose the stock was only one (1). What will you do?   Related To: Software Architecture
 Add to PDF Senior 
Q19: Compare Actor Model with Threading Model for concurrency  
 Add to PDF Senior 
Q20: What happens if you have a "race condition" on the lock itself?  
 Add to PDF Expert 
Q21: What is the difference between Race Condition and Data Races? Are they the same?  