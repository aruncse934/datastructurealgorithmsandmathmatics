Cloud Design Patterns::-
----------------------
These design patterns are useful for building reliable, scalable, secure applications in the cloud.

CHALLENGES IN CLOUD DEVELOPMENT ::-(Categories)
---------------------------------------------------
Availability, Data management, Design and implementation, Management and monitoring, Messaging,
 Performance and scalability, Resiliency, Security.

Availability ::-
---------------
Availability is the proportion of time that the system is functional and working, usually measured as a percentage of uptime. It can be affected by system errors, infrastructure problems, malicious attacks, and system load. Cloud applications typically provide users with a service level agreement (SLA), so applications must be designed to maximize availability.


Data Management::-
-------------------
Data management is the key element of cloud applications, and influences most of the quality attributes. Data is typically hosted in different locations and across multiple servers for reasons such as performance, scalability or availability, and this can present a range of challenges. For example, data consistency must be maintained, and data will typically need to be synchronized across different locations.


Design and Implementation::-
-----------------------------
Good design encompasses factors such as consistency and coherence in component design and deployment, maintainability to simplify administration and development, and reusability to allow components and subsystems to be used in other applications and in other scenarios. Decisions made during the design and implementation phase have a huge impact on the quality and the total cost of ownership of cloud hosted applications and services.

Messaging::-
--------------
The distributed nature of cloud applications requires a messaging infrastructure that connects the components and services, ideally in a loosely coupled manner in order to maximize scalability. Asynchronous messaging is widely used, and provides many benefits, but also brings challenges such as the ordering of messages, poison message management, idempotency, and more.

Management and Monitoring::-
-----------------------------
Cloud applications run in a remote datacenter where you do not have full control of the infrastructure or, in some cases, the operating system. This can make management and monitoring more difficult than an on-premises deployment. Applications must expose runtime information that administrators and operators can use to manage and monitor the system, as well as supporting changing business requirements and customization without requiring the application to be stopped or redeployed.

Performance and Scalability::-
-------------------------------
Performance is an indication of the responsiveness of a system to execute any action within a given time interval, while scalability is ability of a system either to handle increases in load without impact on performance or for the available resources to be readily increased. Cloud applications typically encounter variable workloads and peaks in activity. Predicting these, especially in a multitenant scenario, is almost impossible. Instead, applications should be able to scale out within limits to meet peaks in demand, and scale in when demand decreases. Scalability concerns not just compute instances, but other elements such as data storage, messaging infrastructure, and more.

Resiliency::-
--------------
Resiliency is the ability of a system to gracefully handle and recover from failures. The nature of cloud hosting, where applications are often multitenant, use shared platform services, compete for resources and bandwidth, communicate over the Internet, and run on commodity hardware means there is an increased likelihood that both transient and more permanent faults will arise. Detecting failures, and recovering quickly and efficiently, is necessary to maintain resiliency.

Security::-
-------------
Security provides confidentiality, integrity, and availability assurances against malicious attacks on information systems (and safety assurances for attacks on operational technology systems). Losing these assurances can negatively impact your business operations and revenue, as well as your organization’s reputation in the marketplace. Maintaining security requires following well-established practices (security hygiene) and being vigilant to detect and rapidly remediate vulnerabilities and active attacks.


Patterns::-
-----------

API GATEWAY
AGGREGATOR MICROSERVICES
CACHING
COMMANDER
POISON PILL
SERVERLESS
Ambassador
Command and Query Responsibility Segregation (CQRS)
Circuit Breaker
Event Sourcing
Leader Election
Priority Queue
Queue-Based Load Leveling
Retry
Saga
Sharding
Throttling
Strangler Fig

--------------------------------------------------------
Anti-corruption Layer
Asynchronous Request-Reply
Backends for Frontends
Bulkhead
Cache-Aside
Choreography
Claim Check
Compensating Transaction
Competing Consumers
Compute Resource Consolidation
Deployment Stamps
External Configuration Store
Federated Identity
Gatekeeper
Gateway Aggregation
Gateway Offloading
Gateway Routing
Geodes
Health Endpoint Monitoring
Index Table
Materialized View
Pipes and Filters
Publisher/Subscriber
Scheduler Agent Supervisor
Sequential Convoy
Sidecar
Static Content Hosting
Valet Key
